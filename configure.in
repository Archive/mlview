dnl **************************************************************
dnl Process this file with autoconf to produce a configure script.
dnl **************************************************************
AC_INIT(mlview,0.9.0)
AC_PREREQ(2.59)
AC_CONFIG_SRCDIR(src/main.cc)
AC_CANONICAL_TARGET
AM_MAINTAINER_MODE
AM_INIT_AUTOMAKE(AC_PACKAGE_NAME, AC_PACKAGE_VERSION)
AM_CONFIG_HEADER(config.h)

MLVIEW_MAJOR_VERSION=0
MLVIEW_MINOR_VERSION=10
MLVIEW_MICRO_VERSION=0


dnl ************************************************************
dnl First, Here goes the list of the version of the librairies
dnl we depend on.
dnl ************************************************************
LIBGNOMEUI2_VERSION=2.2.0
LIBXML2_VERSION=2.6.11
LIBXSLT_VERSION=1.1.8
GLIB2_VERSION=2.6.0
GTK2_VERSION=2.6.0
LIBGLADE2_VERSION=2.4.0
LIBGNOME2_VERSION=2.8.1
LIBGLADEMM_VERSION=2.4.0
LIBGNOME2_VERSION=2.4.1
GCONF2_VERSION=2.6.2
SEWFOX_VERSION=0.0.1
GTKSOURCEVIEW_VERSION=1.0
VTE_VERSION=0.11.12
DBUS_VERSION=0.22
GTKMM2_VERSION=2.4.0
LIBGNOMEVFS_VERSION=2.6.0

AC_SUBST(LIBXML2_VERSION)
AC_SUBST(LIBXSLT_VERSION)
AC_SUBST(LIBGNOMEUI2_VERSION)
AC_SUBST(LIBGLADE2_VERSION)
AC_SUBST(LIBGLADEMM_VERSION)
AC_SUBST(GLIB2_VERSION)
AC_SUBST(GTK2_VERSION)
AC_SUBST(LIBGNOME2_VERSION)
AC_SUBST(GCONF2_VERSION)
AC_SUBST(DBUS_VERSION)
AC_SUBST(VTE_VERSION)
AC_SUBST(LIBGNOMEVFS_VERSION)

dnl ****************************
dnl libmlview version stuffs
dnl ***************************
LIBMLVIEW_VERSION_INFO=10:0:0

LIBMLVIEW_MAJOR_VERSION=1
LIBMLVIEW_MINOR_VERSION=1
LIBMLVIEW_MICRO_VERSION=1

LIBMLVIEW_VERSION=$LIBMLVIEW_MAJOR_VERSION.\
$LIBMLVIEW_MINOR_VERSION.$LIBMLVIEW_MICRO_VERSION

LIBMLVIEW_VERSION_NUMBER=\
`expr $LIBMLVIEW_MAJOR_VERSION \
\* 10000 + $LIBMLVIEW_MINOR_VERSION\
 \* 100 + $LIBMLVIEW_MICRO_VERSION`


MLVIEW_VERSION=$MLVIEW_MAJOR_VERSION.\
$MLVIEW_MINOR_VERSION.\
$MLVIEW_MICRO_VERSION$MLVIEW_RC_NUMBER

MLVIEW_VERSION_NUMBER=`expr $MLVIEW_MAJOR_VERSION \
\* 10000 + $MLVIEW_MINOR_VERSION \
\* 100 + $MLVIEW_MICRO_VERSION`

MLVIEW_EXE=$prefix/bin/mlv
AC_SUBST(MLVIEW_VERSION_NUMBER)
AC_SUBST(MLVIEW_VERSION)
AC_SUBST(LIBMLVIEW_VERSION_NUMBER)
AC_SUBST(LIBMLVIEW_VERSION_INFO)
AC_SUBST(LIBMLVIEW_VERSION)
AC_SUBST(MLVIEW_EXE)

dnl *********************
dnl Checks for programs.
dnl *********************
AC_PROG_CXX
AC_GNU_SOURCE
#AC_HEADER_STDC

AC_PROG_INSTALL
AC_PROG_LIBTOOL

AC_LANG_CPLUSPLUS
AC_LANG_COMPILER_REQUIRE

dnl ********************************************************
dnl languages
dnl ********************************************************
ALL_LINGUAS="ar az ca cs de dz en_CA en_GB es fi fr hr it ja ml ms nb ne nl oc pl pt pt_BR ru rw sr sr@Latn sv tr uk vi zh_CN"

dnl ********************************************************
dnl triggers gettext
dnl ********************************************************
AM_GLIB_GNU_GETTEXT
AC_PROG_INTLTOOL([0.33])
GETTEXT_PACKAGE=mlview
AC_SUBST(GETTEXT_PACKAGE)
AC_DEFINE_UNQUOTED(GETTEXT_PACKAGE,"$GETTEXT_PACKAGE",[fix])

dnl ***************************
dnl a list of enablexxx options
dnl ***************************
ENABLE_DBUS=no
AC_ARG_ENABLE(dbus,
              AC_HELP_STRING([--enable-dbus=yes|no],
	                     [enable use of dbus (default is no)]),
	      ENABLE_DBUS=$enableval,
	      ENABLE_DBUS=no)
if test x$ENABLE_DBUS = xyes ; then
	AC_DEFINE([MLVIEW_WITH_DBUS],1,[enable dbus usage])
else
	ENABLE_DBUS=no
fi
AC_SUBST(ENABLE_DBUS)

ENABLE_STYLE=no
AC_ARG_ENABLE(style,
	      AC_HELP_STRING([--enable-style=yes|no],
	                    [enable the CSS based styled editing view (default is no)]),
	      ENABLE_STYLE=$enableval,
	      ENABLE_STYLE=no)
if test $ENABLE_STYLE = yes ; then
	AC_DEFINE([MLVIEW_WITH_STYLE],1,
	[enable the style based editing view])
else
	ENABLE_STYLE=no
fi

ENABLE_DEBUG=yes
AC_ARG_ENABLE(debug,
	      AC_HELP_STRING([--enable-debug=yes|no],
	                     [enable a lot of debug message dumps(default is yes)]),
	      ENABLE_DEBUG=$enableval,
	      ENABLE_DEBUG=yes)
if test $ENABLE_DEBUG = yes ; then
	AC_DEFINE([MLVIEW_DEBUG], 1,
	[enable a lot of messages dumps in the terminal])
else
	ENABLE_DEBUG=no
fi


ENABLE_MLVIEW_CELL_RENDERER=yes
AC_ARG_ENABLE(custom-cell-renderer,
	      AC_HELP_STRING([--enable-custom-cell-renderer=yes|no],
	                    [enable a custom cell renderer in the tree editing widget (default is yes)]),
	      ENABLE_MLVIEW_CELL_RENDERER=$enableval,
	      ENABLE_MLVIEW_CELL_RENDERER=yes)
if test $ENABLE_MLVIEW_CELL_RENDERER = yes ; then
	AC_DEFINE([MLVIEW_WITH_CUSTOM_CELL_RENDERER],1,
	[enable the custom MlView Cell Renderer in the tree editing widget])
else
	ENABLE_MLVIEW_CELL_RENDERER=no
fi

ENABLE_LIBXML_EXPERIMENTAL_COMPLETION=no
AC_ARG_ENABLE(libxml-experimental-completion,
	      AC_HELP_STRING([--enable-libxml-experimental-completion=yes|no],
	                    [enable the experimental libxml completion stuffs (default is no)]),
	      ENABLE_LIBXML_EXPERIMENTAL_COMPLETION=$enableval,
	      ENABLE_LIBXML_EXPERIMENTAL_COMPLETION=no)
if test $ENABLE_LIBXML_EXPERIMENTAL_COMPLETION = yes ; then
	AC_DEFINE([MLVIEW_WITH_LIBXML_EXPERIMENTAL_COMPLETION],1,
	[enable the highly experimental libxml regexp based completion system])
else
	ENABLE_LIBXML_EXPERIMENTAL_COMPLETION=no
fi

ENABLE_GTK_FILE_CHOOSER=yes
AC_ARG_ENABLE(gtk-file-chooser,
	      AC_HELP_STRING([--enable-gtk-file-chooser=yes|no],
	                    [enable the new gtk file chooser (default is yes)]),
	      ENABLE_GTK_FILE_CHOOSER=$enableval,
	      ENABLE_GTK_FILE_CHOOSER=yes)
if test $ENABLE_GTK_FILE_CHOOSER = yes ; then
	AC_DEFINE([MLVIEW_WITH_GTK_FILE_CHOOSER],1,
	[enable the new gtk file chooser])
else
	ENABLE_LIBXML_EXPERIMENTAL_COMPLETION=no
fi

dnl *******************************
dnl Checks for header files.
dnl *******************************
AC_STDC_HEADERS

dnl *******************************
dnl check for zlib
dnl *******************************
AC_CHECK_HEADERS(zlib.h, AC_CHECK_LIB(z, gzopen))

dnl *******************************
dnl checking for libxml2
dnl *******************************
PKG_CHECK_MODULES(LIBXML2,[libxml-2.0 >= $LIBXML2_VERSION])

dnl
dnl ***********************************
dnl detect the actual libxml2 version.
dnl and define a variable to test
dnl ***********************************
use_libxml2_above_2_6=no
LIBXML2_ACTUAL_VERSION=`$PKG_CONFIG --modversion libxml-2.0`
two_six_series_version=20600
major=`echo $LIBXML2_ACTUAL_VERSION | awk -F '.' '{print $1}'`
minor=`echo $LIBXML2_ACTUAL_VERSION | awk -F '.' '{print $2}'`
micro=`echo $LIBXML2_ACTUAL_VERSION | awk -F '.' '{print $3}'`
version=`expr $major \* 10000`
version=`expr $version + $minor \* 100`
version=`expr $version + $micro`
AC_MSG_RESULT([libxml2 version is $version])
if test $version -ge $two_six_series_version ; then	
    AC_DEFINE([LIBXML_2_6_SERIES_OR_ABOVE], 1,
	[libxml2 belongs to the 2.6 series (or above)])
    use_libxml2_above_2_6=yes
fi

dnl *******************************
dnl checking for libxslt
dnl *******************************
PKG_CHECK_MODULES(LIBXSLT,[libxslt >= $LIBXSLT_VERSION])

dnl *******************************
dnl checking for glib
dnl *******************************
PKG_CHECK_MODULES(GLIB2,[glib-2.0 >= $GLIB2_VERSION])

dnl *******************************
dnl checking for gtk+
dnl *******************************
PKG_CHECK_MODULES(GTK2,[gtk+-2.0 >= $GTK2_VERSION])

dnl *******************************
dnl checking for gtkmm2.0
dnl *******************************
PKG_CHECK_MODULES(GTKMM2, [gtkmm-2.4 >= $GTKMM2_VERSION])

use_gtk2_above_2_6=no
AC_MSG_CHECKING([the exact version of gtk+-2.0 we have])
GTK2_ACTUAL_VERSION=`$PKG_CONFIG --modversion gtk+-2.0`
major=`echo $GTK2_ACTUAL_VERSION | awk -F '.' '{print $1}'`
minor=`echo $GTK2_ACTUAL_VERSION | awk -F '.' '{print $2}'`
micro=`echo $GTK2_ACTUAL_VERSION | awk -F '.' '{print $3}'`
version=`expr $major \* 10000`
version=`expr $version + $minor \* 100`
version=`expr $version + $micro`
AC_MSG_RESULT([gtk+2 version is $version])
if test $version -ge 20600 ; then	
    AC_DEFINE([GTK_2_6_SERIE_OR_ABOVE], 1,
	[gtk+-2.0 to the 2.6 series (or above)])
    use_gtk2_above_2_6=yes
fi

dnl *******************************
dnl checking for libglade
dnl *******************************
PKG_CHECK_MODULES(LIBGLADE2,[libglade-2.0 >= $LIBGLADE2_VERSION])

dnl *******************************
dnl checking for libglademm
dnl *******************************
PKG_CHECK_MODULES(LIBGLADEMM,[libglademm-2.4 >= $LIBGLADEMM_VERSION])

dnl *******************************
dnl checking for libgnome
dnl *******************************
PKG_CHECK_MODULES(LIBGNOME2,[libgnome-2.0 >= $LIBGNOME2_VERSION])

dnl *******************************
dnl checking for libgnomevfs
dnl *******************************
PKG_CHECK_MODULES(LIBGNOMEVFS,[gnome-vfs-2.0 >= $LIBGNOMEVFS_VERSION])

dnl *******************************
dnl checking for libgnomeui
dnl *******************************
PKG_CHECK_MODULES(LIBGNOMEUI2,[libgnomeui-2.0 >= $LIBGNOMEUI2_VERSION])

dnl *******************************
dnl checking for gconf2
dnl *******************************
PKG_CHECK_MODULES(GCONF2,[gconf-2.0 >= $GCONF2_VERSION])

dnl *******************
dnl gconf
dnl *******************
AC_PATH_PROG(GCONFTOOL, gconftool-2)
AM_GCONF_SOURCE_2
AC_SUBST(GCONFTOOL)


dnl **********************
dnl libgtksourceview
dnl **********************
PKG_CHECK_MODULES(GTKSOURCEVIEW, gtksourceview-1.0 >= $GTKSOURCEVIEW_VERSION)

dnl **********************
dnl vte
dnl **********************
PKG_CHECK_MODULES(VTE, vte >= $VTE_VERSION)

dnl *******************
dnl checking for SEWFOX
dnl *******************
if test $ENABLE_STYLE = yes ; then
	PKG_CHECK_MODULES(SEWFOX,[sewfox = $SEWFOX_VERSION])
fi

dnl *******************
dnl checking for DBUS
dnl *******************
if test x$ENABLE_DBUS = xyes ; then
	PKG_CHECK_MODULES(DBUS,[dbus-glib-1 >= $DBUS_VERSION])
fi


#MLVIEW_LIBS="$LIBXML2_LIBS $LIBXSLT_LIBS $LIBGLADE2_LIBS $LIBGNOMEUI2_LIBS $SEWFOX_LIBS"
MLVIEW_LIBS="$LIBXML2_LIBS $LIBXSLT_LIBS $LIBGLADE2_LIBS $LIBGLADEMM_LIBS $SEWFOX_LIBS $GTKSOURCEVIEW_LIBS $DBUS_LIBS  $GTKMM2_LIBS $VTE_LIBS $LIBGNOMEVFS_LIBS $LIBGNOMEUI2_LIBS"
#MLVIEW_CFLAGS="$LIBXML2_CFLAGS $LIBXSLT_CFLAGS $LIBGLADE2_CFLAGS $LIBGNOMEUI2_CFLAGS $SEWFOX_CFLAGS"
MLVIEW_CFLAGS="$LIBXML2_CFLAGS $LIBXSLT_CFLAGS $LIBGLADE2_CFLAGS $LIBGLADEMM_CFLAGS $SEWFOX_CFLAGS $GTKSOURCEVIEW_CFLAGS $DBUS_CFLAGS $GTKMM2_CFLAGS $VTE_CFLAGS $LIBGNOMEVFS_CFLAGS $LIBGNOMEUI2_CFLAGS"

AC_SUBST(MLVIEW_LIBS)
AC_SUBST(MLVIEW_CFLAGS)


dnl ***************************
dnl Set Dodji's devel environment.
dnl ***************************

DODJI_FLAGS="-g -Wall -Wundef -Wpointer-arith -Wcast-align \
            -Wwrite-strings   -Wsign-compare \
	    -Waggregate-return -Wmissing-prototypes  -Wmissing-declarations \
	    -Wmissing-format-attribute  -Wredundant-decls -Wnested-externs \
	    -Werror"

if test  "x$MLVIEW_DEVEL" = "xon" ; then
	CFLAGS=$DODJI_FLAGS
	CXXFLAGS="-Wall -g -Werror"
	MLVIEW_DEBUG=yes ;
fi
if test x$MLVIEW_DEBUG != x ; then
	AC_DEFINE([MLVIEW_DEBUG],1,[enable debug messages])
fi

AC_SUBST(LDFLAGS)
AC_SUBST(mlview)

AC_PROG_MAKE_SET
AC_OUTPUT([mlview-mdk.spec
mlview.desktop
Doxyfile
Makefile
pixmaps/Makefile
po/Makefile.in
src/Makefile
src/recent-files/Makefile
ui/Makefile
menu-descriptions/Makefile
tests/Makefile
schemas/Makefile
mime/Makefile
application-registry/Makefile
])

dnl ******************************************************************************************
echo "
	============================================================
            GNOME-MLVIEW, A SIMPLE XML EDITOR FOR GNOME $VERSION
        ============================================================
        Here is the configuration of the package:

	Prefix				: ${prefix}
	Source code location		: ${srcdir}
	C Compiler			: ${CC}
	C++ Compiler			: ${CXX}
	MLVIEW_DEVEL env var            : ${MLVIEW_DEVEL}
	CFLAGS				: ${CFLAGS}
	CXXFLAGS			: ${CXXFLAGS}
	CUSTOM_CFLAGS                   : ${CUSTOM_CFLAGS}
	Experimental cell renderer      : ${ENABLE_MLVIEW_CELL_RENDERER}
	Experimental styled view        : ${ENABLE_STYLE}
	Experimental libxml completion  : ${ENABLE_LIBXML_EXPERIMENTAL_COMPLETION}
	Use the new gtk file chooser    : ${ENABLE_GTK_FILE_CHOOSER}
	Using DBUS			: ${ENABLE_DBUS} 
	Enable verbose debug messages   : ${ENABLE_DEBUG}
	Use a libxml2 version above 2.6 : ${use_libxml2_above_2_6}
	Maintainer mode			: ${USER_MAINTAINER_MODE}
	Languages	      		: ${ALL_LINGUAS}

Oh, whenever you find a bug in MlView, please go to
http://bugzilla.gnome.org/enter_bug.cgi?product=mlview to report it.
Otherwise, if we are not aware of the bug, we will not be able to fix it.
	
Now, Type 'make' to compile MlView
We hope you will enjoy it.

--The MlView developers.
"
