/* -*- Mode: C++; indent-tabs-mode: true; c-basic-offset: 8; -*- */

/*
 *This file is part of MlView
 *
 *MlView is free software; you can redistribute it 
 *and/or modify it under the terms of 
 *the GNU General Public License as published
 * by the Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *MlView is distributed in the hope that it will be useful, 
 *but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of MERCHANTABILITY or 
 *FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License 
 *along with MlView; 
 *see the file COPYING. If not, write to the 
 *Free Software Foundation, Inc., 59 Temple Place - Suite 330, 
 *Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright informations.
 */

#include <iostream>

#include "mlview-plugin.h"

using namespace std ;

extern "C"
{
	bool first_plugin_load (mlview::Plugin *plugin)
	{
		cout << "First plugin loaded." << endl ;
		
		return true ;
	}
	
	bool first_plugin_unload (mlview::Plugin *plugin)
	{
		return true ;
	}
}
