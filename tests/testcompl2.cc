#include <iostream>
#include <libxml/xmlregexp.h>
#include <libxml/xmlautomata.h>
#include <libxml/valid.h>
#include <libxml/parser.h>

#include "mlview-utils.h"
#include "mlview-ustring.h"
#include "mlview-exception.h"

using namespace std ;
using namespace mlview ;

enum InsertionScheme {
	NO_SCHEME = 0,
	INSERT_PREV,
	INSERT_NEXT,
	APPEND_CHILD,
	PREPEND_CHILD,
	CURRENT_ELEMENT,
} ;

struct Options {
	bool display_usage ;
	bool compute_completion ;
	UString dtd_path ;
	UString instance_path ;
	UString node_path ;
	enum InsertionScheme insertion_scheme ;

	Options ():
		display_usage (false),
		compute_completion (false),
		insertion_scheme (NO_SCHEME)
	{}
} ;

static void
display_usage (const UString &a_progname)
{
	cout << "usage: " << a_progname << "[--help | [options]\n"
		 "where options are:\n"
		 "--dtd <path-to-dtd> provides a dtd\n"
		 "--nodepath <a-node-path-expression> specify a ref xml node for insertion completion\n"
		 "--insert-prev display a completion list for previous node insertion\n"
		 "--insert-next display a completion list for next node insertion\n"
		 "--append-child display a completion list for child node addition\n"
		 "--current-element display the currently selected element\n" ;
}

static Options 
parse_command_line (int a_argc, char **a_argv)
{
	Options options ;
	int i = 0 ;
	
	if (a_argc < 2)
		options.display_usage = true ;
	for (i = 1 ; i < a_argc ;i++) {
		if (a_argv[i][0] != '-')
			break ;
		if (!strcmp (a_argv[i], "--help") ||
		    !strcmp (a_argv[i], "-h")) {
		} else if (!strcmp (a_argv[i], "--dtd")) {
			if (i + 1 >= a_argc || a_argv[i + 1][0] == '-') {
				options.display_usage = true ;
				break ;
			}
			options.dtd_path = a_argv[i +1] ;
			i++ ;
		}else if (!strcmp (a_argv[i], "--instance")) {
			if (i + 1 >= a_argc || a_argv[i + 1][0] == '-') {
				options.display_usage = true ;
				break ;
			}
			options.instance_path = a_argv[i + 1] ;
			i++ ;
		} else if (!strcmp (a_argv[i], "--nodepath")) {
			if (i + 1 >= a_argc || a_argv[i + 1][0] == '-') {
				options.display_usage = true ;
				break ;
			}
			options.node_path = a_argv[i + 1] ;
			i++ ;
			options.compute_completion = true ;
		} else if (!strcmp (a_argv[i], "--insert-prev")) {
			options.insertion_scheme = INSERT_PREV ;
			options.compute_completion = true ;
		} else if (!strcmp (a_argv[i], "--insert-next")) {
			options.insertion_scheme = INSERT_NEXT ;
			options.compute_completion = true ;
		} else if (!strcmp (a_argv[i], "--append-child")) {
			options.insertion_scheme = APPEND_CHILD ;
			options.compute_completion = true ;
		} else if (!strcmp (a_argv[i], "--prepend-child")) {
			options.insertion_scheme = PREPEND_CHILD ;
			options.compute_completion = true ;
		}else if (!strcmp (a_argv[i], "--current-element")) {
			options.insertion_scheme = CURRENT_ELEMENT ;
			options.compute_completion = true ;
		}else {
			options.display_usage = true ;
		}
	}
	return options ;
}

int
main (int argc, char **argv)
{
	Options options = parse_command_line (argc, argv) ;
	if (options.display_usage) {
		display_usage (argv[0]) ;
		return -1 ;
	}
	if (options.dtd_path == "") {
		cout << "please, specify a dtd path" << endl ;
		display_usage (argv[0]) ;
		return -1 ;
	}
	if (options.node_path == "") {
		cout << "please, specify a node path" << endl ;
		display_usage (argv[0]) ;
		return -1 ;
	}
}

