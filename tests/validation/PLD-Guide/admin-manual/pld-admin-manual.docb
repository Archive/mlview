    <chapter>
      <title>System configuration</title>
      <section>
	<title>Kernel modules</title>
	<para>
	  Kernel modules configuration is stored in <filename>/etc/modules.conf</filename>.
	  Some common examples:</para>
	<para>
	  Use kernel module <literal>3c509</literal> (3Com 509/509b adapter) to handle first ethernet 
	  device (eth0):
	  <programlisting>alias eth0 3c509</programlisting>
	  
	  Use kernel module <literal>aic7xxx</literal> to handle SCSI hostadapter:
	  <programlisting>alias scsi_hostadapter aic7xxx</programlisting>
	</para>
	<para>	  
	  See <citerefentry><refentrytitle>modules.conf</refentrytitle></citerefentry>for details.
	</para>
      </section>
      <section>
	<title>Disks</title>
	<para>
	</para>
	<literallayout>
	  disks 
	  removable media: CD, diskettes etc
	  user access
	  automounter
	</literallayout>
      </section>
      <section>
	<title>Basic system configuration: <filename>/etc/sysconfig</filename></title>
	<section>
	  <title>Intro</title>
	  <para>
	    Directory <filename>/etc/sysconfig</filename> contains several files
	    that are used by rc-scripts (system startup scripts located in 
	    /etc/rc.d). Those files defines basic properties of the system.
	  </para>
	</section>
	<section>
	  <title>console</title>
	  <para>
	    File <filename>/etc/sysconfig/console</filename>
	    contains basic console settings.
	  </para>
	  <para>
	    After modyfying config file, issue 
	    <programlisting>/etc/rc.d/init.d/console restart</programlisting>
	    if you want the changes to take effect.
	  </para>
	</section>
	<section>
	  <title>mouse</title>
	  <para>
	    File <filename>/etc/sysconfig/mouse</filename> contains
	    mouse settings for service gpm (General Purpose Mouse).
	    This service provides mouse support for console based
	    applications and may be also used by X Window Server.
	    <!--  (see <xref linkend="x-repeater"/>) -->
	  </para>
	  <para>
	    After modyfying config file, issue 
	    <programlisting>/etc/rc.d/init.d/gpm restart</programlisting>
	    if you want the changes to take effect.
	  </para>
	</section>

	<section>
	  <title>clock</title>
	  <para>
	    File <filename>/etc/sysconfig/clock</filename> contains
	    clock settings.
	    Its content is used by /etc/rc.d/rc.sysinit script
	    at very early stage of system bootup.
	  </para>
	</section>
	<section>
	  <title>system</title>
	  <para>
	    Its content is used by rc-scripts at very early stage of system bootup.
	  </para>
	</section>

      </section>
      <section>
	<title>Runlevels and /etc/inittab</title>
	<para>
	  
	</para>
      </section>
      <section>
	<title>Printing</title>
	<para></para>
      </section>
      <section>
	<title>X Window</title>
	<para>
	</para>
      </section>
    </chapter>
    <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
    <chapter>
      <title>Network configuration</title>
      <section>
	<title>Interfaces</title>
	<para>ethernet, dial-ups</para>
      </section>
      <section>
	<title>Basic configuration</title>
	<para>resolver, local network, hostname, default router</para>
      </section>
      <section>
	<title>Internet access</title>
	<para>cable, dial-up, etc.</para>
      </section>
      <section>
	<title>Services</title>
	<section>
	  <title>Intro</title>
	  <para>/etc/services, ...</para>
	</section>
	<section>
	  <title>Inetd services</title>
	  <para>rc-inetd overview, </para>
	</section>
	<section>
	  <title>Standalone services</title>
	  <para>supported MTA's, httpd, ftpd, samba, etc ...</para>
	</section>
      </section>
    </chapter>
    <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
    <chapter>
      <title>System administration</title>
      <section>
	<title>RPM package management</title>
	<section>
	  <title>Using RPM</title>
	  <para>configuring rpm (limit languages, etc)</para>
	</section>
	<section>
	  <title>Managing packages with <application>apt</application></title>
	  <para>
	    Apt is powerfull package installer/updater originally
	    written for Debian and its .deb package system, adapted for
	    RPM based systems by Conectiva people. Apt can be used to install
	    packages from various sources (filesystem, cdrom, nfs, http).
	    Here the net method will be described (ftp or http).
	  </para>
	  <para>
	    To use apt first make sure you have
	    <application>apt</application> rpm package installed. Then
	    you usually need to edit
	    <filename>/etc/apt/sources.list</filename> (there are some
	    examples in this file, in most cases it is enough to
	    uncomment one of them). Example entry for i586 system:
	    <programlisting>rpm ftp://ftp.pld.org.pl/apt PLD-1.0/i586 base supported</programlisting>
	    You might also check some setup
	    values in <filename>/etc/apt/apt.conf</filename>
	    See 
	    <citerefentry>
	      <refentrytitle>sources.list</refentrytitle>
	      <manvolnum>5</manvolnum>
	    </citerefentry>
	    and
	    <citerefentry>
	      <refentrytitle>apt.conf</refentrytitle>
	      <manvolnum>5</manvolnum>
	    </citerefentry>
	    for details.
	  </para>
	  <para>
	    Apt is driven by <command>apt-get</command> command. First
	    time you run apt, issue <command>apt-get update</command>
	    (this command will only download list of packages from
	    server).  Then you can install/upgrade particular packages
	    (and all packages they depend on) with 
	    <command>apt-get install package1 package2 ...</command> 
	    or upgrade whole system with
	    <command>apt-get upgrade</command>. 
	    See
	    <citerefentry>
	      <refentrytitle>apt-get</refentrytitle>
	      <manvolnum>8</manvolnum>
	    </citerefentry>.
	  </para>
	  <note>
	    <para>
	      Apt is very strict about dependencies.
	      If you ever issued --force and --nodeps options to rpm program,
	      then you may get a lot of messages about broken dependecies
	      when you run apt first time. That means your rpm database is messy.
	      In that case it is wise to run 
	      <command>apt-get check</command> to see more details and then
	      fix your database by hand, installing, removing or updating
	      packages that cause problems.
	    </para>
	  </note>
	</section>
	<section>
	  <title>Managing packages with <application>poldek</application></title>
	  <para>
	    Poldek is batch-mode rpm package installer/udpater written
	    by Pawel Gajda as a part of PLD installer.
	    It is software in beta stage.
	  </para>
	  <para>
	    To use poldek you need <application>poldek</application> rpm package.
	    Poldek can be used in full batch mode - every option can be passed 
	    in command line - or it can use config file, typically located
	    in <filename>~/.poldekrc</filename>. Poldek is prepared to be run by 
	    ordinary user, installation of packages in that case can be done with 
	    <command>sudo</command>. Most people would choose to use it from root, however,
	    then establishing ~root/.poldekrc in would be convenient.
	  </para>
	  <para>
	    Poldek supports various sources of packages: filesystem, ftp, http, rsync.
	    Example <filename>poldekrc</filename> for ftp source:
	    <programlisting>
source = ftp://ftp.pld.org.pl/PLD-1.0/i686/PLD/RPMS/
	    </programlisting>
	    See examples of <filename>poldekrc</filename> in poldek doc
	    directory.
	  </para>
	  <para>
	    You may install/upgrade selected packages with 
	    <command>poldek -i package1 package2 ...</command>
	    or upgrade whole system with <command>poldek --upgrade-dist</command>.
	    Poldek can also be run in interactive mode with 
	    <command>poldek --shell</command> (with completion and history available).
	  </para>
	  <para>
	    If you user poldek remotely, it may be sometimes necessary to 
	    issue command <command>poldek --update</command>
	    to reload poldek index files from server.
	  </para>
	  <para>
	    See extensive man page for details:
	    <citerefentry>
	      <refentrytitle>poldek</refentrytitle>
	      <manvolnum>8</manvolnum>
	    </citerefentry>.
	  </para>
	</section>
	<section>
	  <title>Managing packages with <application>wuch</application></title>
	  <para>
	    Wuch is an interactive package manager written by Pawel Kolodziej.
	    It has also some batch mode. The software is in beta stage.
	  </para>
	  <para>
	    To run wuch you need <application>wuch</application> package.
	    Wuch stores its configuration in <filename>/etc/wuch.conf</filename>.
	    Example wuch.conf:
	    <programlisting>
root_dir=/
source=Primary PLD ftp server,ftp,ftp.pld.org.pl,/PLD-1.0/i586/PLD/,RPMS,inst</programlisting>
	    For more details see comments in initial wuch.conf.
	  </para>
	  <para>	  
	    Unfortunatelly, program is not very well documented (you may 
	    find some docs in program sources), nevertheless
	    it has very simple and intuitive interface.
	    Simply type <command>wuch</command> and enjoy.
	  </para>
	</section>
      </section>
      <section>
	<title>Managing users</title>
	<para></para>
      </section>
      <section>
	<title>Viewing logfiles</title>
	<para></para>
      </section>
      <section>
	<title>Backup</title>
	<para></para>
      </section>
      <section>
	<title>Rescue procedure</title>
	<para></para>
      </section>
    </chapter>
    <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
    <chapter>
      <title>Security and access control</title>
      <section>
	<title>Overview</title>
	<para>PAM, tcp wrappers, su, sudo ...</para>
      </section>
      <section>
	<title>PAM</title>
	<para></para>
      </section>
    </chapter>
    <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
    <chapter>
      <title>Overview of PLD system</title>
      <section>
	<title>Directory structure - FHS</title>
	<para></para>
      </section>
      <section>
	<title>Overview of rc-scripts</title>
	<para></para>
      </section>
      <section>
	<title>Devices and links in /dev</title>
	<para></para>
      </section>
    </chapter>

 
  </book>

<!-- Keep this comment at the end of the file
Local variables:
mode: xml
sgml-omittag:nil
sgml-shorttag:nil
sgml-minimize-attributes:nil
sgml-always-quote-attributes:t
sgml-indent-step:2
sgml-indent-data:t
sgml-default-dtd-file:"../dbxbook4.1.2.ced"
sgml-exposed-tags:nil
sgml-local-catalogs:nil
sgml-local-ecat-files:nil
sgml-declaration:nil
sgml-validate-command:"rxp -sxV %s %s"
sgml-parent-document:("PLD-Guide.xml" "book" "section")
End:
-->
