<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!DOCTYPE article PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN" "http://www.oasis-open.org/docbook/xml/4.1.2/docbookx.dtd" [
<!ENTITY appversion "0.7.0">
<!ENTITY manrevision "0.0.1">
<!ENTITY date "August 2004">
<!ENTITY app "<application>mlview</application>">
<!-- Information about the entities
       The legal.xml file contains legal information, there is no need to edit the file. 
       Use the appversion entity to specify the version of the application.
       Use the manrevision entity to specify the revision number of this manual.
       Use the date entity to specify the release date of this manual.
       Use the app entity to specify the name of the application. -->]>
<!-- 
      (Do not remove this comment block.)
  Maintained by the GNOME Documentation Project
  http://developer.gnome.org/projects/gdp
  Template version: 2.0 beta
  Template last modified Apr 11, 2002
-->
<!-- =============Document Header ============================= -->
<article id="index" lang="en">
<!-- please do not change the id; for translations, change lang to -->
<!-- appropriate code -->
  <articleinfo>
    <title>&app; Manual V&manrevision;</title>
    <copyright>
      <year>2004</year>
      <holder>Baptiste Mille-Mathias</holder>
    </copyright>
    <publisher>
      <publishername> GNOME Documentation Project </publishername>
    </publisher>
<!--&legal;-->
<!-- This file  contains link to license for the documentation (GNU FDL), and 
        other legal stuff such as "NO WARRANTY" statement. Please do not change 
	any of this. -->
    <authorgroup>
      <author>
        <firstname>Baptiste</firstname>
        <surname>Mille-Mathias</surname>
        <affiliation>
          <orgname>GNOME Documentation Project</orgname>
          <address>
            <email>bmm80@free.fr</email>
          </address>
        </affiliation>
      </author>
    </authorgroup>
<!-- According to GNU FDL, revision history is mandatory if you are -->
<!-- modifying/reusing someone else's document.  If not, you can omit it. -->
<!-- Remember to remove the &manrevision; entity from the revision entries other
-->
<!-- than the current revision. -->
<!-- The revision numbering system for GNOME manuals is as follows: -->
<!-- * the revision number consists of two components -->
<!-- * the first component of the revision number reflects the release version of the GNOME desktop. -->
<!-- * the second component of the revision number is a decimal unit that is incremented with each revision of the manual. -->
<!-- For example, if the GNOME desktop release is V2.x, the first version of the manual that -->
<!-- is written in that desktop timeframe is V2.0, the second version of the manual is V2.1, etc. -->
<!-- When the desktop release version changes to V3.x, the revision number of the manual changes -->
<!-- to V3.0, and so on. -->
    <revhistory>
      <revision>
        <revnumber>Mlview manual</revnumber>
        <date>August 2004</date>
        <revdescription>
          <para role="author">Baptiste Mille-Mathias    	<email>bmm80@free.fr</email>
	  		</para>
          <para role="publisher">GNOME Documentation Project</para>
        </revdescription>
      </revision>
    </revhistory>
    <releaseinfo>This manual describes version &appversion; of mlview</releaseinfo>
    <legalnotice>
      <title>Feedback</title>
      <para>To report a bug or make a suggestion regarding the &app; application or
      this manual, follow the directions in the <ulink url="ghelp:gnome-feedback" type="help">GNOME Feedback Page</ulink>. 
      </para>
    </legalnotice>
  </articleinfo>
  <indexterm zone="index">
    <primary>MLVIEW</primary>
  </indexterm>
  <indexterm zone="index">
    <primary>mlview</primary>
  </indexterm>
<!-- ============= Document Body ============================= -->
<!-- ============= Introduction ============================== -->
<!-- Use the Introduction section to give a brief overview of what
     the application is and what it does. -->
  <sect1 id="mlview-introduction">
    <title>Introduction</title>
    <para>The &app; application enables you to create and edit xml files easily. &app; provides the following features:</para>
    <itemizedlist>
      <listitem>
        <para>Support for the completion of the markup when a DTD is provided</para>
      </listitem>
      <listitem>
        <para>Document edition with keybindings (ala vim or emacs) to ease the writing of documents</para>
      </listitem>
      <listitem>
        <para>Support validation and error reporting.</para>
      </listitem>
    </itemizedlist>
  </sect1>
<!-- =========== Getting Started ============================== -->
<!-- Use the Getting Started section to describe the steps required
     to start the application and to describe the user interface components
     of the application. If there is other information that it is important
     for readers to know before they start using the application, you should
     also include this information here. 
     If the information about how to get started is very short, you can 
     include it in the Introduction and omit this section. -->
  <sect1 id="mlview-getting-started">
    <title>Getting Started</title>
    <sect2 id="mlview-start">
      <title>To Start &app;</title>
      <para>You can start  in the following ways:
    </para>
      <variablelist>
        <varlistentry>
          <term><guimenu>Applications</guimenu> menu</term>
          <listitem>
            <para>Choose 
    		<menuchoice><guisubmenu>Office </guisubmenu><guimenuitem>MlView XML Editor</guimenuitem></menuchoice>. </para>
          </listitem>
        </varlistentry>
        <varlistentry>
          <term>Command line</term>
          <listitem>
            <para>To start <application>&app;</application> from a command line, type the following command, 
    		then press <keycap>Return</keycap>:</para>
            <para> 
			<command>mlv</command> <replaceable>filename.xml</replaceable></para>
            <para>where 
			<replaceable>filename.xml</replaceable> is the name of the file you
			want to open.
    		</para>
            <para>You can use the switch --dtd to provide a <acronym>DTD</acronym> with the xml file in the command line.</para>
          </listitem>
        </varlistentry>
      </variablelist>
    </sect2>
    <sect2 id="mlview-when-start">
      <title>When You Start &app;</title>
      <para>When you start <application>&app;</application>, the following window is displayed.</para>
<!-- ==== Figure ==== -->
      <figure id="mainwindow-fig">
        <title>&app; Start Up Window</title>
        <screenshot>
          <mediaobject>
            <imageobject>
              <imagedata fileref="figures/mlview_start_window.png" format="PNG"/>
            </imageobject>
            <textobject>
              <phrase>Shows &app; main window. Contains titlebar, menubar, toolbar, display area, and
	      scrollbars. Menubar contains File, View, Settings, and Help menus. </phrase>
            </textobject>
          </mediaobject>
        </screenshot>
      </figure>
<!-- ==== End of Figure ==== -->
<!-- Include any descriptions of the GUI immediately after the screenshot of the main UI, -->
<!-- for example, the items on the menubar and on the toolbar. This section is optional. -->
      <para>The &app; window contains the following elements:
    </para>
      <variablelist>
        <varlistentry>
          <term>Menubar. </term>
          <listitem>
            <para>The menus on the menubar contain all of the commands you need to work with files in 
	<application>&app;</application>.</para>
          </listitem>
        </varlistentry>
        <varlistentry>
          <term>Toolbar. </term>
          <listitem>
            <para> The toolbar contains a subset of the commands that you can access from the menubar.</para>
          </listitem>
        </varlistentry>
        <varlistentry>
          <term>Statusbar. </term>
          <listitem>
            <para>The statusbar displays information about current <application>&app;</application> 
	activity and contextual information about the menu items. </para>
          </listitem>
        </varlistentry>
      </variablelist>
    </sect2>
  </sect1>
<!-- ================ Usage ================================ -->
<!-- Use this section to describe how to use the application to perform the tasks for 
  which the application is designed. -->
  <sect1 id="mlview-usage">
    <title>Usage</title>
<!-- ================ Usage Subsection ================================ -->
    <sect2 id="mlview-new-doc">
      <title>Create a new document</title>
      <para>To create a new document, choose <menuchoice><guimenu>File</guimenu><guimenuitem>New</guimenuitem></menuchoice>. The 
	<guilabel>Name of the root element</guilabel> dialog is displayed. Enter the root element name, then click 
	<guibutton>OK</guibutton>. Now, the file selector appears, and you are prompted to enter the path of the <acronym>DTD</acronym>. A <acronym>DTD</acronym> is a document which provides the list of authorized elements, mlview uses it to autocomplete the xml markups.<note><para>If you cancel, when you're asked for a DTD, mlview will cannot provide validation and completion.</para></note></para>
      <para>Now, mlview displays a xml tree with one element, the root element name you provided earlier. You can start to edit the document.</para>
    </sect2>
    <sect2 id="mlview-open-doc">
      <title>Open document</title>
      <para>To open a document, choose <menuchoice><guimenu>File</guimenu><guimenuitem>Open</guimenuitem></menuchoice>, and now mlview should display the source of the xml file.</para>
    </sect2>
<!-- ================ Usage Subsection ================================ -->
  </sect1>
  <sect1 id="quick-reference">
    <title>Keyboard Shortcuts</title>
    <sect2>
      <title>Shortcuts</title>
      <informaltable frame="none">
        <tgroup cols="2" colsep="0" rowsep="0">
<!--<colspec colname="COLSPEC0" colwidth="30*">
      <colspec colname="COLSPEC1" colwidth="70*">-->
          <tbody>
            <row>
              <entry>
              <shortcut><keycombo><keycap>Ctrl</keycap><keycap>N</keycap></keycombo></shortcut>
            </entry>
              <entry>
              <para>New Document</para>
            </entry>
            </row>
            <row>
              <entry>
              <shortcut><keycombo><keycap>Ctrl</keycap><keycap>O</keycap></keycombo></shortcut>
            </entry>
              <entry>
              <para>Open Document</para>
            </entry>
            </row>
            <row>
              <entry>
              <shortcut><keycombo><keycap>Ctrl</keycap><keycap>S</keycap></keycombo></shortcut>
            </entry>
              <entry>
              <para>Save</para>
            </entry>
            </row>
            <row>
              <entry>
              <shortcut><keycombo><keycap>Ctrl</keycap><keycap>Shift</keycap><keycap>S</keycap></keycombo></shortcut>
            </entry>
              <entry>
              <para>Save as</para>
            </entry>
            </row>
            <row>
              <entry>
              <shortcut><keycombo><keycap>Ctrl</keycap><keycap>W</keycap></keycombo></shortcut>
            </entry>
              <entry>
              <para>Close current document</para>
            </entry>
            </row>
          </tbody>
        </tgroup>
      </informaltable>
    </sect2>
    <sect2>
      <title>Keybindings for Edition</title>
      <para>Mlview provides a powerful way to edit an xml document only with the keyboard. </para>
      <informaltable frame="none">
        <tgroup cols="2" colsep="0" rowsep="0">
<!--<colspec colname="COLSPEC0" colwidth="30*">
      <colspec colname="COLSPEC1" colwidth="70*">-->
          <tbody>
            <row>
              <entry>
                <keycap>y</keycap>
              </entry>
              <entry>
                <para>Copy node</para>
              </entry>
            </row>
            <row>
              <entry>
                <keycap>&lt;</keycap>
              </entry>
              <entry>
                <para>Add next element</para>
              </entry>
            </row>
            <row>
              <entry>
                <keycap>&gt;</keycap>
              </entry>
              <entry>
                <para>Add previous element</para>
              </entry>
            </row>
            <row>
              <entry>
                <keycap>t</keycap>
              </entry>
              <entry>
                <para>Add child text node</para>
              </entry>
            </row>
            <row>
              <entry>
                <keycap>f</keycap>
              </entry>
              <entry>
                <para>fold/unfold node</para>
              </entry>
            </row>
            <row>
              <entry>
                <keycap>del</keycap>
              </entry>
              <entry>
                <para>Cut node</para>
              </entry>
            </row>
            <row>
              <entry>
                <keycap>pp</keycap>
              </entry>
              <entry>
                <para>Paste to previous</para>
              </entry>
            </row>
            <row>
              <entry>
                <keycap>pn</keycap>
              </entry>
              <entry>
                <para>Paste to next</para>
              </entry>
            </row>
            <row>
              <entry>
                <keycap>o</keycap>
              </entry>
              <entry>
                <para>Switch to the text editor on a text node</para>
              </entry>
            </row>
            <row>
              <entry>
                <keycap>esc o</keycap>
              </entry>
              <entry>
                <para>Switch back to the xml tree on the text node</para>
              </entry>
            </row>
          </tbody>
        </tgroup>
      </informaltable>
    </sect2>
  </sect1>
<!-- ============= Bugs ================================== -->
<!-- This section is optional and is commented out by default. 
     You can use it to describe known bugs and limitations of the 
	  program if there are any - please be frank and list all
     problems you know of. 
  
  <sect1 id="mayapp-bugs">
  <title>Known Bugs and Limitations</title>
  <para> </para>
 </sect1>
-->
<!-- ============= About ================================== -->
<!-- This section contains info about the program (not docs), such as
      author's name(s), web page, license, feedback address. This
      section is optional: primary place for this info is "About.." box of
      the program. However, if you do wish to include this info in the
      manual, this is the place to put it. Alternatively, you can put this information in the title page.-->
  <sect1 id="mlview-about">
    <title>About &app;</title>
    <para> &app; was written by Dodji Seketeli
      (<email>dodji@gnome.org</email>) and Nicolas Centa (<email>happypeng@free.fr</email>). To find more information about &app;, please visit the 
      <ulink url="http://www.mlview.org" type="http">mlview Web page</ulink>. </para>
    <para>
      To report a bug or make a suggestion regarding this application or
      this manual, follow the directions in this 
      <ulink url="ghelp:gnome-feedback" type="help">document</ulink>.
    </para>
    <para> This program is distributed under the terms of the GNU
      General Public license as published by the Free Software
      Foundation; either version 2 of the License, or (at your option)
      any later version. A copy of this license can be found at this
      <ulink url="ghelp:gpl" type="help">link</ulink>, or in the file
      COPYING included with the source code of this program. </para>
  </sect1>
</article>
