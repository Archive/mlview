#############################
#This script parses programs and replaces gtk-doc comments by
#doxygen/javadoc style comments.
#
#Author: Dodji Seketeli.
#############################

$debug="off" ;


sub log {
    my $string = shift ;

    if ($debug eq "on") {
	print $string ;
    }
}

sub print_usage {
    print "usage: $0 <input file>\n" ;
}


sub set_state {
    my ($state_ref, $state_name) = @_ ;

    pop @$state_ref ;
    push @$state_ref, $state_name ;
}

sub unset_state {
    my ($state_ref) = @_ ;

    pop @$state_ref ;
}

sub format_comment_text {
    my($text) = shift ;

    
    $text =~ s/\s*\n+\s*/\n/gs ;
    $text =~s/s*\n+\s*$/ /gs ;
    $text =~ s/\n([^\*])/\n \*$1/gs ;

    return $text ;
}

sub clear_function_comment {
    my ($function_comment_ref) = shift ;
    $$function_comment_ref{"comment_text"} = "" ;
    $param_hash_ref = $$function_comment_ref{"params"} ;

    foreach $name (keys %$param_hash_ref) {
	delete $$param_hash_ref{$name} ;
    }

    $$function_comment_ref{"return_comment_text"} = "" ;
    $$function_comment_ref{"title"} = "" ;
}


sub print_function_comment {

    my ($OUTPUT_FILE, $function_comment_ref) = @_ ;

    my ($params_hashref) = $$function_comment_ref{"params"} ;
    my (@param_names) = keys %$params_hash_ref ;
    my ($function_comment_text) = &format_comment_text ($$function_comment_ref{"comment_text"}) ;
    my ($return_comment_text) =  &format_comment_text ($$function_comment_ref{"return_comment_text"}) ;

    print $OUTPUT_FILE "/**\n" ;

    
    if ($function_comment_text ne "" ) {
	print $OUTPUT_FILE " *$function_comment_text\n" ;
    } ;

    if ($#param_names > -1) {
	########################
	#There are some parameters
	########################
	print $OUTPUT_FILE " *\n" ;

	foreach $param_name (@param_names) {
	    $param_desc = &format_comment_text ($$params_hash_ref{$param_name}) ;
	    print $OUTPUT_FILE " *\@param $param_name $param_desc\n" ;
	}
	print $OUTPUT_FILE " *\n" ;
    }


    print $OUTPUT_FILE " *\n" ;

    
    if ($return_comment_text) {
	print $OUTPUT_FILE " *\@return $return_comment_text\n" ;
    }
    print $OUTPUT_FILE " */\n" ;

}

sub print_function_comment_debug {
    my ($function_comment_ref) = shift ;

    &log ("============= function comment report===========================\n") ;
    &log ("function title: ===>:\n" . $$function_comment_ref{"title"} . "\n") ;
    &log ("function comment text: ===>:\n" . $$function_comment_ref{"comment_text"} . "\n") ;
    &log ("function return comment text: ===>:" . $$function_comment_ref{"return_comment_text"} . "\n") ;
    &log ("function params: ===>\n") ;

    $param_names_hash_ref = $$function_comment_ref{"params"} ;

    @param_names = keys %$param_names_hash_ref ;
    &log ("nb of params: " + $#param_names . "\n") ;

    foreach $param_name (@param_names) {
	$param_value = $$param_names_hash_ref{$param_name} ;
	&log ("$param_name= $param_value\n") ;
    }

    &log ("============= End of Function comment report===========================\n") ;
}

#============================================
#Todo: add state management to 
#support multiline param and result comments.
#============================================
sub translate_file {
    my $input_file_name = shift ;
    my @comment_stack,$line_content, @state, @function_comments, %function_comment, %function_params ;

    %function_params = ("" => "") ;

    %function_comment = ("comment_text" =>"",
			 "params" => \%function_params,
			 "return_comment_text" => "",
			 "title" => "",
			 ) ;

    open INPUT_FILE, $input_file_name or die "could not open file $input_file_name: $!\n" ;
    $output_file_name = $input_file_name . ".doxy" ;
    open OUTPUT_FILE, ">$output_file_name" or die "could not open file $output_file_name: $!\n" ;

    $line = 0 ;
    $line_content = "" ;
    while (<INPUT_FILE>) 
    {
	$line ++ ;
	$line_content = $_ ;

	&log ("current_line: $line_content\n") ;
	&log ("current state: ") ;
	&log ($state[$#state] . "\n ") ;
	&log ("current state#: ") ;
	&log ($#state . "\n") ;

	if (/\/\*\*\s*$/) {
	    &log ("start of comments\n") ;
	    if ($#comment_stack != -1) {
		&log ("Argh !! There are nested comments at line $line. Skiped.\n") ;
		print OUTPUT_FILE $line_content ;
		next ;
	    }
	    push @comment_stack, \%function_comment ;
	}
	
	&log ("#comment stack: $#comment_stack\n") ;
	if ($#comment_stack == 0) {
	    ########################
	    #We are parsing a comment
	    ########################

	    if (/\*(\w+):/) {
		
		$function_comment_ref = $comment_stack[$#comment_stack] ;
		$$function_comment_ref{"title"} = $1 ;

		&log ("---function title---\n") ;
		&print_function_comment_debug ($function_comment_ref) ;
		
	    } elsif (/\*@(\w+):\s*(.*)/) {
		#######################
		#start of parameter desc
		########################

		$param_name = $1;
		$param_comment = $2 ;
		$param_names_hash_ref = $$function_comment_ref{"params"} ;
		
		$$param_names_hash_ref{$param_name} = $param_comment ;
		&log ("----------param name----------\n") ;
		&print_function_comment_debug ($function_comment_ref) ;
		&set_state (\@state, "param:$param_name") ;

	    } elsif (/[r|R]eturns?:?\s+(([^\s]+\s*)+$)/) {
		######################
		#start of return comment.
		######################

		$return_comment = $1 ;
		$$function_comment_ref{"return_comment_text"} = $return_comment ;

		&log ("---------return comment---------\n") ;
		&print_function_comment_debug ($function_comment_ref) ;
		&set_state (\@state, "return") ;

	    } elsif ( /^\s*\*([^\\].+?\s*)$/ ) {
		###########################
		#Next part of a param comment
		##########################
		$comment = $1 ;
		&log ("next part of comment. state content" . $state[$#state] . "\n") ;
		if ($state[$#state] =~ /(param:([a-zA-Z0-9_\.-]+))/) {
		    $param_name = $2 ;
		    $params_hash_ref = $$function_comment_ref{"params"} ;
		    
		    $$params_hash_ref{$param_name} =
			$$params_hash_ref{$param_name} . $comment . " " ;

		    &log ("-----param comment: suite of $param_name---------\n")  ;
		    &print_function_comment_debug ($function_comment_ref) ;

		} elsif ($state[$#state] eq "return") {

		    $$function_comment_ref{"return_comment"} = 
			$$function_comment_ref{"return_comment"} . $comment . " " ;
		    &log ("-----return comment, suite-------\n") ;
		    &print_function_comment_debug ($function_comment_ref) ;

		} else {
		    $$function_comment_ref{"comment_text"} = 
			$$function_comment_ref{"comment_text"} . $comment . " " ;
		    &set_state (\@state, "comment") ;

		    &log ("-----function comment, suite-------\n") ;
		    &print_function_comment_debug ($function_comment_ref) ;
		}

	    } elsif (/^(\s*\*\s*$)/) {
		$comment = $1 ;
		&log ("-------empty comment----\n") ;
		&unset_state (\@state) ;
		&print_function_comment_debug ($function_comment_ref) ;
		
	    }

	}

	if (/\*\//) {
	    ######################
	    #End of comment
	    ######################

	    if ($#comment_stack == 0) {
		&log ("end of comments\n") ;
		&print_function_comment_debug ($function_comment_ref) ;
		&print_function_comment (OUTPUT_FILE, $function_comment_ref) ;
		&clear_function_comment ($function_comment_ref) ;
		pop @comment_stack ;
	    } else {
		print OUTPUT_FILE $line_content ;
	    }
	    
	} elsif ($#comment_stack == -1) {

	    #####################
	    #No comment stack.
	    #####################
	    &log ("No comment: $line_content") ;
	    print OUTPUT_FILE $line_content ;
	}
	
    }

    if ($#comment_stack != -1) {
	&log ("Some comment are not closed !!\n") ;
    }

    close INPUT_FILE ;
    close OUTPUT_FILE ;
    
}


if ($#ARGV == -1) {
    &print_usage ;
    exit (0) ;
}

@files = /w+\.c/ ;
&translate_file ($ARGV[0]) ;

