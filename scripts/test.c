/**
 *foo_function:
 *@a_first_param: the first parameter.
 *next part of the first param comment
 *@a_second_param: the second parameter.
 *next part of the second param comment.
 *
 *mqkfdsqjfkdsqmfdskf
 *qsfjdsfmdsjfkdsmkdsf
 *
 *Returns the good value
 *span of return
 */
gint
foo_function (char * a_first_param, char *a_second_param) ;

/*
 *
 *You should have received a copy of the GNU General Public License along with MlView; 
 *see the file COPYING. If not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 *Copyright 2001-2002 Dodji Seketeli
 */

#include <libxml/uri.h>
#include <libxml/tree.h>

#include "mlview-editor.h"
#include "mlview-xml-doc-tree-view.h"
#include "mlview-file-descriptor.h"
#include "mlview-settings-builder.h"
#include "mlview-global-settings.h"
#include "mlview-xml-document.h"

#include "messages.h"
#include "utils.h"

enum
{
	DOCUMENT_CHANGED,
	SIGNAL_NUM
};

enum ContextualMenuEntry {
	CONTEXTUAL_MENU_ENTRY_START_BOUND,
	OPEN_FILE,
	CLOSE_FILE,
	SAVE_FILE,
	SEARCH,
	LAST_CONTEXTUAL_MENU_ENTRY_END_BOUND
} ;

/*======================================================
 *Some private (static) data structures used by
 *MlViewEditor widget.
 *=====================================================*/

struct _MlViewEditorPrivate
{
	/*
	 *The set of MlViewXMLDocument
	 *opened in the editor. The key is the pointer to the instance of MlViewXMLDocument opened in the editor
	 *The value is a an hashtable that contains the views that holds the key MlViewXMLDocument.
	 */
	GHashTable *mlview_xml_docs ;
	
	/*
	 *A hash table where the keys are the instances of
	 *MlViewXMLDocumentViews and the values are their
	 *associated document.
	 */
	GHashTable *mlview_xml_doc_views ;

	/*
	 *The notebook that holds all the notebook pages (document views)
	 */
	GtkNotebook *notebook ;

	/*
	 *The current document being viewed by the user
	 */
	MlViewXMLDocumentView * current_xml_doc_view ;

	/*a hash table which keys are the base name of the files already opened. 
	 *The associated data is the number of times the file name
	 *has been opened. When destroying this hash table, do not destroy the referenced data (base names) because
	 *the base names are substring of file_paths. And file names are hold by the instances of MlViewXMLDocument.
	 */
	GHashTable *opened_file_base_names ;

	/*a hash table wich keys are the file paths of the file files already opened.
	 *the associated MlViewXMLDocument. When destroying this hashtable do not destroy the referenced data (file path)
	 *because file paths are hold by instances of MlViewXMLDocument and destroyed by them.
	*/
	GHashTable *opened_file_paths ;
	
	/**/
	GHashTable *opened_document_label_names ;

	/*mlview wide settings hash table*/
	GHashTable *global_settings ;

	/*Number of untitled document opened*/
	guint untitled_docs_num ;
	
	/*total number of docs opened*/
	guint opened_docs_num ;

	/*settings manager*/
	MlViewSettingsManager *settings_manager ;
	MlViewSettingsBuilder *settings_builder ;

	MlViewAppContext * app_context ;

	/*the editor contextual menu*/
	GtkMenu * contextual_menu ;
} ;

#define PRIVATE(editor) (editor->private)

static void 
mlview_editor_class_init                         (MlViewEditorClass *a_klass) ;

static void 
mlview_editor_init                               (MlViewEditor *a_editor) ;

static void 
mlview_editor_destroy (GtkObject *a_object) ;

/*signal callbacks and default handlers*/
static void 
mlview_editor_switch_notebook_page_cb            (GtkNotebook *a_notebook,GtkNotebookPage *a_page,
						  gint a_page_num, MlViewEditor *a_editor) ;

static gboolean 
mlview_editor_event_cb (GtkWidget * a_widget, GdkEvent *a_event,
					gpointer a_user_data) ;

static void 
menu_open_local_file_activate_cb (GtkMenuItem * a_menu_item,
					      gpointer *a_user_data) ;

static void 
menu_close_file_activate_cb (GtkMenuItem * a_menu_item,
			     gpointer *a_user_data) ;

static void 
menu_save_file_activate_cb (GtkMenuItem * a_menu_item,
			    gpointer *a_user_data) ;

static void 
menu_search_activate_cb (GtkMenuItem * a_menu_item,
			 gpointer *a_user_data) ;

static void 
menu_new_document_activate_cb (GtkMenuItem * a_menu_item,
			       gpointer *a_user_data) ;

static void 
view_name_changed_cb (MlViewXMLDocumentView *a_view,
		      gpointer a_editor) ;

static GList *
build_view_list_from_hashtable (GHashTable *a_views) ;

static void
add_hash_key_to_list (gpointer a_key,
		      gpointer a_value,
		      GList ** a_list) ;

static GtkVBoxClass *parent_class = NULL ;
static guint mlview_editor_signals[SIGNAL_NUM] = {0} ;



/*============================================================
 *private method required by the GTK typing system
 *=============================================================*/
static void
mlview_editor_class_init (MlViewEditorClass * a_klass)
{
	GtkObjectClass *object_class = GTK_OBJECT_CLASS (a_klass) ;
  
	parent_class = gtk_type_class (GTK_TYPE_VBOX) ;
  
	object_class->destroy = mlview_editor_destroy; /*overload the destroy method of GtkObject*/
  
	/*define the signals*/
	mlview_editor_signals[DOCUMENT_CHANGED] = gtk_signal_new ("document-changed",
								  GTK_RUN_FIRST,
								  object_class->type,
								  GTK_SIGNAL_OFFSET(MlViewEditorClass,document_changed),
								  gtk_marshal_NONE__NONE,
								  GTK_TYPE_NONE,
								  0);
	gtk_object_class_add_signals (object_class, mlview_editor_signals, SIGNAL_NUM) ;
	a_klass->document_changed = NULL ;
}


/**
 *mlview_editor_init: 
 *@a_editor: the current instance of MlViewEditor to initialyze
 *
 *the MlViewEditor instance initialyzer.
 *
 *FIXME: connect the "element-changed" signal of a_editor->node_editor to a callback.
 *This callback must retrieve the visual_node associated to the modified xml node, update the string representation
 *of the tag, and reshow the node in the MlViewTreeEditor.
 */
static void
mlview_editor_init (MlViewEditor *a_editor)
{
	g_assert (a_editor != NULL);
	PRIVATE (a_editor) = g_malloc0(sizeof(MlViewEditorPrivate));

	PRIVATE (a_editor)->notebook = GTK_NOTEBOOK(gtk_notebook_new()) ;
	gtk_signal_connect (GTK_OBJECT(PRIVATE(a_editor)->notebook),
			    "switch-page",
			    GTK_SIGNAL_FUNC(mlview_editor_switch_notebook_page_cb),
			    a_editor) ;
	gtk_box_pack_start (GTK_BOX(a_editor),
			    GTK_WIDGET(PRIVATE(a_editor)->notebook),
			    TRUE,TRUE,0) ;
	
	PRIVATE (a_editor)->opened_file_base_names = g_hash_table_new(g_str_hash,g_str_equal) ;
	PRIVATE (a_editor)->opened_file_paths = g_hash_table_new(g_str_hash, g_str_equal) ;
	PRIVATE (a_editor)->opened_document_label_names = g_hash_table_new(g_str_hash, g_str_equal) ;
	PRIVATE (a_editor)->mlview_xml_docs = g_hash_table_new(g_direct_hash, g_direct_equal) ;
	PRIVATE (a_editor)->mlview_xml_doc_views = g_hash_table_new(g_direct_hash, g_direct_equal) ;
	PRIVATE (a_editor)->untitled_docs_num = 0 ;
	PRIVATE (a_editor)->opened_docs_num = 0 ;
}


/*================================================
 *private helper functions
 *
 *=================================================*/

/**
 *free_mlview_xml_doc_views_hash_elements:
 *@a_list_elem:
 *@a_user_data:
 *
 *Called on each element of a GHashTable. Frees the element of the list. Note that this
 *function expects the list element to be a MlViewXMLDocument *
 */
static void
free_mlview_xml_doc_views_hash_elements (gpointer a_key, gpointer a_value, gpointer a_user_data)
{
	MlViewXMLDocumentView *mlview_xml_doc_view ;
	g_return_if_fail (a_key) ;

	mlview_xml_doc_view = (MlViewXMLDocumentView *) a_key ;
	mlview_xml_document_view_destroy (a_key) ;
}


static void
mlview_editor_build_contextual_menu (MlViewEditor * a_editor)
{
	//xmlNode * current_xml_node = NULL ;
	GtkWidget * menu_item = NULL ;

	g_return_if_fail (a_editor != NULL) ;
	g_return_if_fail (MLVIEW_IS_EDITOR (a_editor)) ;
	g_return_if_fail (PRIVATE (a_editor) != NULL) ;

	if (PRIVATE (a_editor)->contextual_menu) {
		gtk_widget_destroy (GTK_WIDGET (PRIVATE (a_editor)->contextual_menu)) ;
		PRIVATE (a_editor)->contextual_menu = NULL ;
	}

	PRIVATE (a_editor)->contextual_menu = GTK_MENU (gtk_menu_new ()) ;

	menu_item = gtk_menu_item_new_with_label (_("Open File")) ;
	gtk_signal_connect (GTK_OBJECT (menu_item),"activate",
			    GTK_SIGNAL_FUNC (menu_open_local_file_activate_cb),
			    a_editor) ;
	gtk_menu_append (GTK_MENU (PRIVATE (a_editor)->contextual_menu), 
			 menu_item) ;
	gtk_widget_show (menu_item) ;


	menu_item = gtk_menu_item_new_with_label (_("Close File")) ;
	gtk_signal_connect (GTK_OBJECT (menu_item),"activate",
			    GTK_SIGNAL_FUNC (menu_close_file_activate_cb), 
			    a_editor) ;
	gtk_menu_append (GTK_MENU (PRIVATE (a_editor)->contextual_menu), 
			 menu_item) ;
	gtk_widget_show (menu_item) ;

	menu_item = gtk_menu_item_new_with_label (_("Save File")) ;
	gtk_signal_connect (GTK_OBJECT (menu_item),"activate",
			    GTK_SIGNAL_FUNC (menu_save_file_activate_cb),
			    a_editor) ;
	gtk_menu_append (GTK_MENU (PRIVATE (a_editor)->contextual_menu), 
			 menu_item) ;
	gtk_widget_show (menu_item) ;

	/*separator*/
	menu_item = gtk_menu_item_new () ;
	gtk_menu_append (GTK_MENU (PRIVATE (a_editor)->contextual_menu), 
			 menu_item) ;
	gtk_widget_show (menu_item) ;

	menu_item = gtk_menu_item_new_with_label (_("Search")) ;
	gtk_signal_connect (GTK_OBJECT (menu_item),"activate",
			    GTK_SIGNAL_FUNC (menu_search_activate_cb),
			    a_editor) ;
	gtk_menu_append (GTK_MENU (PRIVATE (a_editor)->contextual_menu), 
			 menu_item) ;
	gtk_widget_show (menu_item) ;

	/*separator*/
	menu_item = gtk_menu_item_new () ;
	gtk_menu_append (GTK_MENU (PRIVATE (a_editor)->contextual_menu), 
			 menu_item) ;
	gtk_widget_show (menu_item) ;

	menu_item = gtk_menu_item_new_with_label (_("New Document")) ;
	gtk_signal_connect (GTK_OBJECT (menu_item),"activate",
			    GTK_SIGNAL_FUNC (menu_new_document_activate_cb),
			    a_editor) ;
	gtk_menu_append (GTK_MENU (PRIVATE (a_editor)->contextual_menu), 
			 menu_item) ;
	gtk_widget_show (menu_item) ;

	if (PRIVATE (a_editor)->current_xml_doc_view) {
		mlview_xml_doc_tree_view_update_contextual_menu (MLVIEW_XML_DOC_TREE_VIEW (PRIVATE (a_editor)->current_xml_doc_view),
								 &PRIVATE (a_editor)->contextual_menu) ;
	}
}

/*================================================
 *signal callbacks and default handlers
 *
 *=================================================*/



/**
 *
 */
static void
mlview_editor_switch_notebook_page_cb (GtkNotebook *a_notebook,GtkNotebookPage *a_page,
				      gint a_page_num, MlViewEditor *a_editor)
{
	MlViewXMLDocumentView *doc_view ;

	g_return_if_fail (a_editor != NULL) ;
	g_return_if_fail (MLVIEW_IS_EDITOR (a_editor)) ;
	g_return_if_fail (PRIVATE (a_editor) != NULL) ;

	doc_view = MLVIEW_XML_DOCUMENT_VIEW (a_page->child) ;
	g_return_if_fail (doc_view != NULL) ;
	PRIVATE (a_editor)->current_xml_doc_view = doc_view ;
}


static gboolean
mlview_editor_event_cb (GtkWidget * a_widget,
			GdkEvent *a_event,
			gpointer a_user_data) {

	MlViewEditor * editor = NULL ;

	g_return_val_if_fail (a_widget != NULL, FALSE) ;
	g_return_val_if_fail (GTK_IS_WIDGET (a_widget), FALSE) ;
	g_return_val_if_fail (a_user_data != NULL, FALSE) ;
	g_return_val_if_fail (MLVIEW_IS_EDITOR (a_user_data), FALSE) ;
	g_return_val_if_fail (a_event != NULL, FALSE) ;

	editor = MLVIEW_EDITOR (a_user_data) ;
	g_return_val_if_fail (editor != NULL, FALSE) ;
	g_return_val_if_fail (PRIVATE (editor), FALSE) ;
	
	switch (a_event->type) {
	case GDK_BUTTON_PRESS :
		if (a_event->button.button == 3) { /*user pressed the right mouse button*/
			mlview_editor_build_contextual_menu (editor) ;
			g_return_val_if_fail (PRIVATE (editor)->contextual_menu != NULL, FALSE) ;

			gtk_menu_popup (PRIVATE (editor)->contextual_menu, NULL, NULL, NULL, 
					editor, a_event->button.button, a_event->button.time) ;
		}
		break ;
	default :
		break ;
	}
	return FALSE ;
}

/**
 *menu_open_file_activate_cb:
 *
 *
 *Called when the user clicks on the
 *"open file" menu.
 */
static void
menu_open_local_file_activate_cb (GtkMenuItem * a_menu_item,
				  gpointer *a_user_data)
{
	MlViewEditor * editor = NULL ;
	g_return_if_fail (a_menu_item != NULL) ;
	g_return_if_fail (GTK_IS_MENU_ITEM (a_menu_item)) ;
	g_return_if_fail (a_user_data != NULL) ;
	g_return_if_fail (MLVIEW_IS_EDITOR (a_user_data)) ;

	editor = MLVIEW_EDITOR (a_user_data) ;
	mlview_editor_open_local_xml_document_interactive (editor) ;
}

/**
 *menu_close_file_activate_cb:
 *
 *
 *Called when the user clicks on the
 *"close file" menu.
 */
static void
menu_close_file_activate_cb (GtkMenuItem * a_menu_item,
			     gpointer *a_user_data)
{
	MlViewEditor * editor = NULL ;

	g_return_if_fail (a_menu_item != NULL) ;
	g_return_if_fail (GTK_IS_MENU_ITEM (a_menu_item)) ;
	g_return_if_fail (a_user_data != NULL) ;
	g_return_if_fail (MLVIEW_IS_EDITOR (a_user_data)) ;

	editor = MLVIEW_EDITOR (a_user_data) ;
	mlview_editor_close_xml_document_interactive (editor) ;
}

/**
 *menu_save_file_activate_cb:
 *
 *
 *Called when the user clicks on the
 *"close file" menu.
 */
static void
menu_save_file_activate_cb (GtkMenuItem * a_menu_item,
			    gpointer *a_user_data)
{
	MlViewEditor * editor = NULL ;

	g_return_if_fail (a_menu_item != NULL) ;
	g_return_if_fail (GTK_IS_MENU_ITEM (a_menu_item)) ;
	g_return_if_fail (a_user_data != NULL) ;
	g_return_if_fail (MLVIEW_IS_EDITOR (a_user_data)) ;

	editor = MLVIEW_EDITOR (a_user_data) ;
	mlview_editor_save_xml_document (editor) ;
}


/**
 *menu_search_activate_cb:
 *
 *
 *Called when the user clicks on the
 *"close file" menu.
 */
static void
menu_search_activate_cb (GtkMenuItem * a_menu_item,
			 gpointer *a_user_data)
{
	MlViewEditor * editor = NULL ;

	g_return_if_fail (a_menu_item != NULL) ;
	g_return_if_fail (GTK_IS_MENU_ITEM (a_menu_item)) ;
	g_return_if_fail (a_user_data != NULL) ;
	g_return_if_fail (MLVIEW_IS_EDITOR (a_user_data)) ;

	editor = MLVIEW_EDITOR (a_user_data) ;
	mlview_editor_find_xml_node_that_contains_str_interactive (editor) ;
}

/**
 *menu_new_document_activate_cb:
 *
 *
 *Called when the user clicks on the
 *"close file" menu.
 */
static void
menu_new_document_activate_cb (GtkMenuItem * a_menu_item,
			       gpointer *a_user_data)
{
	MlViewEditor * editor = NULL ;

	g_return_if_fail (a_menu_item != NULL) ;
	g_return_if_fail (GTK_IS_MENU_ITEM (a_menu_item)) ;
	g_return_if_fail (a_user_data != NULL) ;
	g_return_if_fail (MLVIEW_IS_EDITOR (a_user_data)) ;

	editor = MLVIEW_EDITOR (a_user_data) ;
	mlview_editor_create_new_xml_document (editor) ;
}

static void 
view_name_changed_cb (MlViewXMLDocumentView *a_view,
		      gpointer a_editor) {

	MlViewEditor *editor = NULL ;
	gchar * new_view_name = NULL ;

	g_return_if_fail (a_view != NULL) ;
	g_return_if_fail (MLVIEW_XML_DOCUMENT_VIEW (a_view)) ;
	g_return_if_fail (a_editor != NULL) ;
	g_return_if_fail (MLVIEW_IS_EDITOR (a_editor)) ;
	
	editor = MLVIEW_EDITOR (a_editor) ;
	g_return_if_fail (PRIVATE (editor) != NULL) ;
	g_return_if_fail (PRIVATE (editor)->notebook != NULL) ;
	
	new_view_name = 
		mlview_xml_document_view_get_name (a_view) ;

	gtk_notebook_set_tab_label_text (PRIVATE (editor)->notebook,
					 GTK_WIDGET (a_view),
					 new_view_name) ;

}

static GList *
build_view_list_from_hashtable (GHashTable *a_views)
{
	GList * result = NULL ;
	g_hash_table_foreach (a_views, 
			      (GHFunc) add_hash_key_to_list,
			      &result) ;
	return result ;
}


static void
add_hash_key_to_list (gpointer a_key,
			gpointer a_value,
			GList ** a_list)
{

	g_return_if_fail (a_list != NULL) ;
	*a_list  = g_list_append (*a_list, a_key) ;
}

/*=================================================
 *public methods
 *=================================================*/

/**
 *mlview_editor_get_type:
 *
 *the standard type builder of MlViewEditor
 */
guint
mlview_editor_get_type (void)
{
	static guint mlview_editor_type = 0;
	if (!mlview_editor_type) {
		static const GtkTypeInfo mlview_editor_type_info = {
			"MlViewEditor",
			sizeof(MlViewEditor),
			sizeof(MlViewEditorClass),
			(GtkClassInitFunc) mlview_editor_class_init,
			(GtkObjectInitFunc) mlview_editor_init,
			NULL,
			NULL,
			(GtkClassInitFunc) NULL
		};
		mlview_editor_type = gtk_type_unique(gtk_vbox_get_type(), &mlview_editor_type_info);
	}
	return mlview_editor_type ;
}


/**
 *mlview_editor_new:
 *@a_title: the title of the editor.
 *
 *Instanciates a MlViewEditor.
 */
GtkWidget*
mlview_editor_new (gchar * a_title, MlViewAppContext * a_context)
{
	MlViewEditor *editor = NULL ;
	gpointer tmp = NULL ;	
	editor = gtk_type_new (MLVIEW_TYPE_EDITOR) ;

	PRIVATE(editor)->app_context = a_context ;
	g_return_val_if_fail (a_context != NULL, NULL) ;

	/*handles mlview editor settings stufs. Therefore, instanciate the mlview settings management*/	
	PRIVATE (editor)->settings_builder = mlview_settings_builder_new (PRIVATE (editor)->app_context) ;
	g_return_val_if_fail (PRIVATE (editor)->settings_builder != NULL, NULL) ;

	mlview_settings_builder_build_settings (PRIVATE (editor)->settings_builder) ;

	PRIVATE (editor)->settings_manager = mlview_settings_builder_get_settings_manager (PRIVATE (editor)->settings_builder) ;

	tmp = mlview_app_context_get_element (a_context, "GnomeApp") ;
	if (tmp && GTK_IS_WIDGET (tmp)) {
		GtkWidget * parent_app = NULL ;

		parent_app = GTK_WIDGET (tmp) ;
		gtk_widget_add_events (parent_app, GDK_BUTTON3_MOTION_MASK) ;
		gtk_signal_connect (GTK_OBJECT (parent_app), "button_press_event",
				    GTK_SIGNAL_FUNC (mlview_editor_event_cb),
				    editor) ;
	}

	return GTK_WIDGET(editor) ;
}


/**
 *mlview_editor_set_app_context:
 *@a_editor: the current mlview editor.
 *@a_context: the current application context.
 *
 *
 */
void 
mlview_editor_set_app_context (MlViewEditor * a_editor, 
			       MlViewAppContext *a_context)
{
	g_return_if_fail (a_editor != NULL) ;
	g_return_if_fail (MLVIEW_IS_EDITOR (a_editor)) ;
	g_return_if_fail (PRIVATE (a_editor) != NULL) ;
	g_return_if_fail (a_context != NULL) ;
	g_return_if_fail (MLVIEW_IS_APP_CONTEXT (a_context)) ;

	PRIVATE (a_editor)->app_context = a_context ;	
}


/**
 *mlview_editor_get_app_context:
 *@a_editor: the current instance of MlView Editor.
 *
 *
 */
MlViewAppContext *
mlview_editor_get_app_context (MlViewEditor *a_editor)
{
	g_return_val_if_fail (a_editor != NULL, NULL) ;
	g_return_val_if_fail (MLVIEW_IS_EDITOR (a_editor), NULL) ;
	g_return_val_if_fail (PRIVATE (a_editor) != NULL, NULL) ;

	return PRIVATE (a_editor)->app_context ;
}


/**
 *mlview_editor_load_xml_file:
 *@a_editor: the current instance of MlViewEditor.
 *@a_file_path: the file path.
 *
 *Opens the xml file a_file_path as an MlViewXMLDocument and adds it to the editor.
 *
 */
void
mlview_editor_load_xml_file (MlViewEditor *a_editor, gchar * a_file_path)
{
	MlViewXMLDocument * mlview_xml_document = NULL ;

	g_return_if_fail (a_editor != NULL) ;
	g_return_if_fail (MLVIEW_IS_EDITOR (a_editor)) ;
	g_return_if_fail (PRIVATE (a_editor)) ;

	if (a_file_path == NULL)
		return ;

	mlview_app_context_sbar_push_message (PRIVATE (a_editor)->app_context, _("Opening file %s ..."), a_file_path) ;
	
	mlview_xml_document =
		mlview_xml_document_open (a_file_path, PRIVATE (a_editor)->app_context) ;

	if (mlview_xml_document) {
		MlViewXMLDocumentView * new_view = NULL ;
		GtkWidget * parent_window = NULL ;

		new_view =
			MLVIEW_XML_DOCUMENT_VIEW (mlview_xml_doc_tree_view_new (mlview_xml_document, a_file_path,
										PRIVATE (a_editor)->app_context)) ;
		g_return_if_fail (new_view != NULL) ;
		PRIVATE(a_editor)->current_xml_doc_view =  new_view ;

		parent_window = gtk_widget_get_toplevel (GTK_WIDGET (a_editor)) ;

		gtk_widget_add_events (parent_window, GDK_BUTTON3_MOTION_MASK) ;

		gtk_signal_connect (GTK_OBJECT (parent_window), "button_press_event",
				    GTK_SIGNAL_FUNC (mlview_editor_event_cb),
				    a_editor) ;

		mlview_editor_add_xml_document_view (a_editor,
						     MLVIEW_XML_DOCUMENT_VIEW (PRIVATE(a_editor)->current_xml_doc_view)) ;
	}

	/*FIXME: add support for exception thrown by mlview_xml_document_open in case 
	 *of error and display that error => BIG DEAL
	 */

	mlview_app_context_sbar_pop_message (PRIVATE (a_editor)->app_context) ;
}


/**
 *mlview_editor_open_local_xml_document_interactive:
 *@editor:  the current instance of MlViewEditor.
 *
 *let the user choose and open a local xml document.
 *
 */
void
mlview_editor_open_local_xml_document_interactive (MlViewEditor * a_editor)
{
	MlViewFileSelection * file_selector ;
	enum MLVIEW_SELECTED_BUTTON button ;
	gchar * file_name = NULL ;

	g_return_if_fail (a_editor != NULL) ;
	g_return_if_fail (MLVIEW_IS_EDITOR (a_editor)) ;
	g_return_if_fail (PRIVATE (a_editor) != NULL) ;
	g_return_if_fail (PRIVATE (a_editor)->app_context != NULL) ;

	file_selector = 
		mlview_app_context_get_file_selector (PRIVATE (a_editor)->app_context, 
						      _("Open xml document")) ;
	g_return_if_fail (file_selector != NULL) ;

	mlview_app_context_sbar_push_message (PRIVATE (a_editor)->app_context,
					      _("Choose the xml file to open")) ;

	gtk_widget_realize (GTK_WIDGET (file_selector)) ;
	mlview_app_context_set_window_icon (PRIVATE (a_editor)->app_context, GTK_WIDGET (file_selector)) ;
	button = mlview_file_selection_run (MLVIEW_FILE_SELECTION (file_selector),
					    TRUE) ;
	switch ( button ) {
	case OK_BUTTON:
		file_name = gtk_file_selection_get_filename (GTK_FILE_SELECTION (file_selector)) ;
		if ( file_name  &&  strcmp (file_name, "") )
			mlview_editor_load_xml_file (a_editor, file_name) ;
	case CANCEL_BUTTON:
	case WINDOW_CLOSED:
	default :
		break ;
	}

	mlview_app_context_sbar_pop_message (PRIVATE (a_editor)->app_context) ;	
}


MlViewXMLDocumentView *
mlview_editor_create_new_view_on_document (MlViewEditor *a_editor,
					   MlViewXMLDocument *a_xml_doc,
					   enum MLVIEW_XML_DOC_VIEW_TYPE a_view_type)

{
	MlViewXMLDocumentView * result = NULL ;

	g_return_val_if_fail (a_editor != NULL, NULL) ;
	g_return_val_if_fail (MLVIEW_IS_EDITOR (a_editor), NULL) ;
	g_return_val_if_fail (PRIVATE (a_editor) != NULL, NULL) ;
	g_return_val_if_fail (a_xml_doc != NULL, NULL) ;
	g_return_val_if_fail (MLVIEW_IS_XML_DOCUMENT (a_xml_doc), NULL) ;

	switch (a_view_type) {
	case MLVIEW_XML_DOC_TREE_VIEW:
		result = 
			MLVIEW_XML_DOCUMENT_VIEW (mlview_xml_doc_tree_view_new (a_xml_doc, NULL, 
										PRIVATE (a_editor)->app_context)) ;
		mlview_editor_add_xml_document_view (a_editor, result) ;
		break ;
	default :
		break ;
	}

	return result ;
}


MlViewXMLDocumentView *
mlview_editor_create_new_view_on_current_document_interactive (MlViewEditor *a_editor)
{
	MlViewXMLDocument * xml_document = NULL ;

	g_return_val_if_fail (a_editor != NULL, NULL) ;
	g_return_val_if_fail (MLVIEW_IS_EDITOR (a_editor), NULL) ;
	g_return_val_if_fail (PRIVATE (a_editor) != NULL, NULL) ;
	
	
	if (PRIVATE (a_editor)->current_xml_doc_view == NULL)
		return NULL ;

	xml_document = 
		mlview_xml_document_view_get_document (PRIVATE (a_editor)->current_xml_doc_view) ;

	return mlview_editor_create_new_view_on_document (a_editor, xml_document, 
							  MLVIEW_XML_DOC_TREE_VIEW) ;

}

void
mlview_editor_set_current_view_name (MlViewEditor *a_editor, 
				     gchar * a_name)
{
	g_return_if_fail (a_editor != NULL) ;
	g_return_if_fail (MLVIEW_IS_EDITOR (a_editor)) ;
	g_return_if_fail (PRIVATE (a_editor) != NULL) ;

	if (PRIVATE (a_editor)->current_xml_doc_view == NULL)
		return ;

	mlview_xml_document_view_set_name (PRIVATE (a_editor)->current_xml_doc_view,
					   a_name) ;
}

void
mlview_editor_set_current_view_name_interactive (MlViewEditor *a_editor)
{
	g_return_if_fail (a_editor != NULL) ;
	g_return_if_fail (MLVIEW_IS_EDITOR (a_editor)) ;
	g_return_if_fail (PRIVATE (a_editor) != NULL) ;

	if (PRIVATE (a_editor)->current_xml_doc_view == NULL)
		return ;

	mlview_xml_document_view_set_name_interactive (PRIVATE (a_editor)->current_xml_doc_view) ;
}


/**
 *mlview_editor_add_xml_document_view:
 *@a_editor: the current instance of MlViewEditor
 *@a_xml_doc: the mlview xml document to add to the editor.
 *
 *Adds a document to the editor. If a_xml_doc is a new document, opens adds new notebook page to the MlViewEditor notebook.
 *The base name of the document is shown in the notebook page tab. If a_xml_doc has the same base name as a document already opened,
 *the notebook page tab string will be "basename<nb>" where nb is the number of docs that have the same name in the editor (like in emacs).
 *If a_xml_doc is already loaded in MlViewEditor, the old instance of a_xml_doc is destroyed (as well as the notebook page that contains it)
 *and the new instance of a_xml_doc is added to the editor.
 *
 */

void
mlview_editor_add_xml_document_view (MlViewEditor * a_editor,
				     MlViewXMLDocumentView * a_xml_doc_view)
{
	  MlViewFileDescriptor * file_desc = NULL ;
	  gchar *file_path = NULL, *base_name = NULL ;
	  MlViewXMLDocument * mlview_xml_document = NULL ;
	  GHashTable *views_associated_to_document = NULL ;
	  gpointer ptr = NULL ;
	  gboolean is_new_document = TRUE ;
	  GtkWidget *label = NULL ;

	  g_return_if_fail (a_editor != NULL) ;
	  g_return_if_fail (MLVIEW_IS_EDITOR (a_editor)) ;
	  g_return_if_fail (PRIVATE (a_editor) != NULL) ;
	  g_return_if_fail (PRIVATE (a_editor)->notebook != NULL) ;
	  g_return_if_fail (a_xml_doc_view != NULL) ;

	  g_return_if_fail (MLVIEW_IS_XML_DOCUMENT_VIEW (a_xml_doc_view)) ;
	  g_return_if_fail (PRIVATE (a_editor)->mlview_xml_docs != NULL) ;
	  g_return_if_fail (PRIVATE (a_editor)->mlview_xml_doc_views != NULL) ;

	  if (PRIVATE (a_editor)->current_xml_doc_view == NULL)
		  PRIVATE (a_editor)->current_xml_doc_view = a_xml_doc_view ;

	  file_desc = mlview_xml_document_view_get_file_descriptor (a_xml_doc_view) ;
	  if (file_desc)
		  file_path = mlview_file_descriptor_get_file_path (file_desc) ;
	  mlview_xml_document = mlview_xml_document_view_get_document (a_xml_doc_view) ;
	  g_return_if_fail (mlview_xml_document != NULL) ;

	  /*check if a view on the same document has been added to
	   *the editor already.
	   */
	  views_associated_to_document = 
		  g_hash_table_lookup (PRIVATE (a_editor)->mlview_xml_docs, 
				       mlview_xml_document) ;

	  if (views_associated_to_document) {
		  is_new_document = FALSE ;
	  }

	  if (file_path == NULL) {
		  gchar *tmp_str = NULL, *label_str = NULL ;
		  
		  if (is_new_document == TRUE)
			  PRIVATE (a_editor)->untitled_docs_num ++ ;
		  tmp_str = g_strdup_printf ("%d", PRIVATE (a_editor)->untitled_docs_num) ;
		  label_str = g_strconcat ("untitled", tmp_str, NULL) ;
		  label = gtk_label_new (label_str) ;
		  g_free (label_str) ;
		  g_free (tmp_str) ;

	  } else {

		  gint base_name_nb = 0 ;
		
		  gboolean file_is_already_opened = FALSE ;
		  base_name = g_basename (file_path) ;

		  
		  if (is_new_document
		      && (ptr = g_hash_table_lookup (PRIVATE (a_editor)->opened_file_paths, file_path)) != NULL) {
			  /*This document is already opened. So, reopen it.
			   *that is, remove it previous instance and add
			   *the new one.
			   */
			  GtkWidget *old_label = NULL ;
			  gchar *old_label_str_tmp = NULL, *old_label_str = NULL ;

			  /*get the old label string because this document
			   *must have the same label as the one alreay opened
			   */
			  old_label = gtk_notebook_get_tab_label (PRIVATE (a_editor)->notebook,
								  GTK_WIDGET ((MlViewXMLDocumentView *)ptr)) ;
			  g_assert (old_label != NULL) ;
			  gtk_label_get (GTK_LABEL (old_label), &old_label_str_tmp) ;

			  /*make a copy of the label string because
			   *mlview_editor_remove_xml_document ()
			   *will destroy this label.
			   */

			  /*old_label_str_tmp belongs to label,
			   *and will be freed by him
			   */
			  old_label_str = g_strdup (old_label_str_tmp) ;

			  mlview_editor_remove_xml_document_view (a_editor,
								  (MlViewXMLDocumentView *)ptr) ;

			  /*create the label of this document notebook page*/
			  label = gtk_label_new (old_label_str) ;
			  g_free (old_label_str) ;/*old_label_str has been strduped*/
			  gtk_label_get (GTK_LABEL (label),
					 &old_label_str) ;

			  /*old_label_str belongs to label and will be freed by him
			   *so we can use as key in a hash table without any need of
			   *freeing it.
			   */
			  g_hash_table_insert (PRIVATE(a_editor)->opened_document_label_names,
					       g_strdup (old_label_str), a_xml_doc_view) ;
			  file_is_already_opened = TRUE ;

		  } else if ((ptr = g_hash_table_lookup (PRIVATE (a_editor)->opened_file_base_names, base_name)) == NULL) {
			  /*It is the first time a document with this basename is opened*/
			  base_name_nb = 1 ;

		  } else if (ptr != NULL) {
			  /*some documents with the this basename are already opened*/
			  base_name_nb = GPOINTER_TO_INT (ptr) ;
			  if (!is_new_document)
				  base_name_nb ++ ;
		  } 
		  
		  g_hash_table_insert (PRIVATE (a_editor)->opened_file_base_names, 
				       base_name, GINT_TO_POINTER (base_name_nb)) ;

		  g_hash_table_insert (PRIVATE (a_editor)->opened_file_paths,
				       file_path, a_xml_doc_view) ;

		  if (base_name_nb > 1) {
			  gchar * tmp_str = NULL, *label_str = NULL ;

			  while (1) {

				  tmp_str = g_strdup_printf ("%d", base_name_nb) ;
				  label_str = g_strconcat (base_name,
							   "<", tmp_str,">",
							   NULL) ;

				  if (g_hash_table_lookup (PRIVATE (a_editor)->opened_document_label_names, 
							   label_str)) {
					  base_name_nb++ ;
					  g_free(tmp_str) ;
					  continue ;
				  }
				  break ;
			  }
			  mlview_xml_document_view_set_name (a_xml_doc_view, label_str) ;

			  label = gtk_label_new (label_str) ;
			  g_hash_table_insert (PRIVATE (a_editor)->opened_document_label_names,
					       g_strdup (label_str), a_xml_doc_view) ;
			  g_free (tmp_str) ;
			  g_free (label_str) ;

		  } else if (file_is_already_opened == FALSE) {

			  label = gtk_label_new (base_name) ;
			  g_hash_table_insert (PRIVATE (a_editor)->opened_document_label_names,
					       g_strdup (base_name), a_xml_doc_view) ;
		  }
	  }

	  /*update the view->document index*/
	  g_hash_table_insert (PRIVATE (a_editor)->mlview_xml_doc_views,
			       a_xml_doc_view, mlview_xml_document) ;

	  /*update the document->views index*/
	  views_associated_to_document = g_hash_table_lookup (PRIVATE (a_editor)->mlview_xml_docs, mlview_xml_document) ;
	  if (!views_associated_to_document) {

		  views_associated_to_document = g_hash_table_new (g_direct_hash, g_direct_equal) ;
		  g_assert (views_associated_to_document != NULL) ;
		  
		  g_hash_table_insert (PRIVATE (a_editor)->mlview_xml_docs, 
				       mlview_xml_document, 
				       views_associated_to_document) ;
	  }

	  g_hash_table_insert (views_associated_to_document, 
			       a_xml_doc_view, mlview_xml_document) ;

	  if (is_new_document == TRUE)
		  PRIVATE (a_editor)->opened_docs_num ++ ;

	  /*now, visually add the view*/
	  gtk_notebook_append_page (PRIVATE (a_editor)->notebook,
				    GTK_WIDGET (a_xml_doc_view),
				    label) ;

	  gtk_signal_connect (GTK_OBJECT (a_xml_doc_view),
			      "name-changed",
			      GTK_SIGNAL_FUNC (view_name_changed_cb),
			      a_editor) ;

	  gtk_widget_show_all (GTK_WIDGET (a_editor)) ;

	  mlview_xml_doc_tree_view_set_all_paned_proportions (MLVIEW_XML_DOC_TREE_VIEW (a_xml_doc_view),
							      30, 20) ;
}


/**
 *mlview_editor_remove_xml_document_view:
 *@a_editor: the current instance of MlViewEditor
 *@a_xml_doc_view: the instance of MlViewXMLDocument to remove.
 *
 *removes the document view a_xml_doc_view from the editor.
 *It does not save the document.
 */
void
mlview_editor_remove_xml_document_view (MlViewEditor * a_editor, 
					MlViewXMLDocumentView * a_xml_doc_view)
{
	GtkWidget *label = NULL ;
	gpointer *ptr = NULL ;
	gint notebook_page_num ;
	MlViewFileDescriptor *file_desc = NULL ;
	MlViewXMLDocument * mlview_xml_doc = NULL ;
	GHashTable *views_related_to_document = NULL ;
	gboolean doc_closed = FALSE ;
	gchar *file_path = NULL, *base_name=NULL, *label_str ;

	g_return_if_fail (a_editor != NULL) ;
	g_return_if_fail (MLVIEW_IS_EDITOR (a_editor)) ;
	g_return_if_fail (PRIVATE(a_editor) != NULL) ;
	g_return_if_fail (PRIVATE (a_editor)->mlview_xml_doc_views != NULL) ;

	g_return_if_fail (a_xml_doc_view != NULL) ;
	g_return_if_fail (MLVIEW_IS_XML_DOCUMENT_VIEW (a_xml_doc_view)) ;
	
	file_desc = mlview_xml_document_view_get_file_descriptor (a_xml_doc_view) ;
	if (file_desc)
		file_path  = mlview_file_descriptor_get_file_path (file_desc) ;

	if (file_path != NULL)
		base_name = g_basename (file_path) ;

	mlview_xml_doc = mlview_xml_document_view_get_document (a_xml_doc_view) ;
	g_return_if_fail (mlview_xml_doc != NULL) ;

	label = gtk_notebook_get_tab_label (PRIVATE(a_editor)->notebook,
						    GTK_WIDGET (a_xml_doc_view)) ;
	gtk_label_get (GTK_LABEL(label), &label_str) ;
	label_str = g_strdup (label_str) ;

	g_return_if_fail (label != NULL) ;

	/*check if the document view a_xml_doc_view
	 *is opened in this instance of MlViewEditor.
	 */
	ptr = g_hash_table_lookup (PRIVATE (a_editor)->mlview_xml_doc_views, a_xml_doc_view) ;
	g_return_if_fail (ptr != NULL) ;

	/*removes a_xml_doc_view from the hashtable of the opened document views.
	 *and from the opened_file_paths hash_table.
	 */
	g_hash_table_remove (PRIVATE (a_editor)->mlview_xml_doc_views, a_xml_doc_view) ;

	views_related_to_document = 
		g_hash_table_lookup (PRIVATE (a_editor)->mlview_xml_docs, mlview_xml_doc) ;
	g_return_if_fail (views_related_to_document != NULL) ;
	
	ptr = g_hash_table_lookup (views_related_to_document, a_xml_doc_view) ;
	g_return_if_fail (ptr != NULL) ;
	
	g_hash_table_remove (views_related_to_document, a_xml_doc_view) ;

	/*removes the notebook page that contains the view*/
	notebook_page_num = gtk_notebook_page_num (PRIVATE(a_editor)->notebook, 
						   GTK_WIDGET (a_xml_doc_view)) ;

	g_return_if_fail (notebook_page_num != -1) ;
	gtk_notebook_remove_page ( PRIVATE(a_editor)->notebook, notebook_page_num) ;
	
	if (g_hash_table_size (views_related_to_document) == 0 ) {
		/*no views are opened on the current doc anymore=>close the doc*/
		g_hash_table_remove (PRIVATE (a_editor)->mlview_xml_docs, mlview_xml_doc) ;
		gtk_object_destroy (GTK_OBJECT (mlview_xml_doc)) ;
		mlview_xml_doc = NULL ;
		doc_closed = TRUE ;

		if (file_path != NULL) {
			g_hash_table_remove (PRIVATE (a_editor)->opened_file_paths, file_path) ;
		}
		
		PRIVATE (a_editor)->opened_docs_num-- ;
	}

	if (doc_closed && label_str) {
		/*remove the entry in the opened_document_label_names hash table*/
		g_hash_table_remove (PRIVATE (a_editor)->opened_document_label_names, label_str) ;
		g_free (label_str) ;
		label_str = NULL ;
	}

	/*if there are several docs that have the save base name as this one,
	 *decrement their number, and if the number reaches 0, remove the entry matching this base name
	 *from the hash table.
	 */
	if (doc_closed == TRUE && file_path != NULL) {
		gint tmp_int ;
		ptr = g_hash_table_lookup (PRIVATE (a_editor)->opened_file_base_names, base_name)  ;
		tmp_int = GPOINTER_TO_INT (ptr) ;
		tmp_int-- ;
		if (tmp_int == 0)
			g_hash_table_remove( PRIVATE (a_editor)->opened_file_base_names, base_name) ;
		else {
			ptr = GINT_TO_POINTER (tmp_int) ;
			g_hash_table_insert (PRIVATE (a_editor)->opened_file_base_names, base_name, ptr) ;
		}
	} else if (doc_closed == TRUE && !file_path) {
		PRIVATE (a_editor)->untitled_docs_num -- ;
	}
}

/**
 *mlview_editor_create_new_xml_document:
 *@a_editor:
 *
 *
 */
void
mlview_editor_create_new_xml_document (MlViewEditor * a_editor)
{
	MlViewXMLDocument * mlview_doc ;
	GtkWidget * label,*entry,* table ;
	gint res ;
	GtkWidget *dialog ;
	gchar *element_name ;
	xmlDocPtr xml_doc=NULL ;
	xmlNodePtr xml_node=NULL ;
	gchar * validation_is_on = NULL ;
	
	g_return_if_fail (a_editor != NULL) ;
	g_return_if_fail (MLVIEW_IS_EDITOR (a_editor)) ;
	g_return_if_fail (PRIVATE (a_editor) != NULL) ;
	g_return_if_fail (PRIVATE (a_editor)->app_context != NULL) ;

	validation_is_on = mlview_app_context_get_settings_value (PRIVATE (a_editor)->app_context, 
								  MLVIEW_STG_K_IS_VALIDATION_ON) ;

	dialog = gnome_dialog_new (_("Name of the root element"),
				   GNOME_STOCK_BUTTON_YES,
				   GNOME_STOCK_BUTTON_CANCEL,
				   NULL) ;
	
	gtk_window_set_wmclass (GTK_WINDOW (dialog), "root-element-dialog", "MlView") ;

	gnome_dialog_close_hides (GNOME_DIALOG (dialog), FALSE) ;
	
	label = gtk_label_new (_("Root element name:")) ;
	entry = gtk_entry_new () ;
	table = gtk_table_new (1,2,TRUE) ;

	gtk_table_attach_defaults (GTK_TABLE(table), label,
				   0,1,0,1) ;
	gtk_table_attach_defaults (GTK_TABLE(table), entry,
				   1,2,0,1) ;
	gtk_box_pack_start (GTK_BOX (GNOME_DIALOG (dialog)->vbox),table,TRUE,TRUE,0) ;
	gtk_widget_show_all (table) ;
	gtk_widget_grab_focus (entry) ;
	gnome_dialog_set_default (GNOME_DIALOG(dialog),0) ;

	gtk_widget_realize (GTK_WIDGET (dialog)) ;
	mlview_app_context_set_window_icon (PRIVATE (a_editor)->app_context, GTK_WIDGET (dialog)) ;
	res = gnome_dialog_run (GNOME_DIALOG (dialog)) ;

	if (res == 0) {/*pressed ok*/
		element_name = gtk_entry_get_text (GTK_ENTRY(entry)) ;
		if (!utils_is_white_string (element_name)) {
			xml_node = xmlNewNode (NULL, g_strdup(element_name)) ;
			xml_doc = xmlNewDoc ("1.0") ;
			xml_doc->name = g_strdup ("untitled") ;
			mlview_doc = mlview_xml_document_new (xml_doc, PRIVATE(a_editor)->app_context) ;
			
			if (mlview_doc != NULL
			    && (validation_is_on != NULL)
			    && (strcmp (validation_is_on, MLVIEW_STG_V_YES) == 0)) {
				mlview_xml_document_associate_dtd_interactive (mlview_doc) ;
			}

			xml_node = 
				mlview_xml_document_add_child_node (mlview_doc, (xmlNode*)xml_doc,
								    xml_node, TRUE) ;

			g_return_if_fail (xml_node != NULL) ;
			xmlDocSetRootElement (xml_doc, xml_node) ;
			gnome_dialog_close (GNOME_DIALOG(dialog)) ;
						

			if (mlview_doc != NULL) {
				MlViewXMLDocumentView *view = NULL ;
				view = 
					MLVIEW_XML_DOCUMENT_VIEW (mlview_xml_doc_tree_view_new (mlview_doc,"", 
												PRIVATE (a_editor)->app_context)) ;
				mlview_editor_add_xml_document_view(a_editor, view) ;
			}
		}
	} else if (res == 1) {/*pressed cancel button*/
		gnome_dialog_close (GNOME_DIALOG (dialog)) ;
	} else if (res == -1) {
		gnome_dialog_close (GNOME_DIALOG (dialog)) ;
	}
}


/**
 *mlview_editor_edit_xml_document:
 *@a_editor: the current instance of MlViewEditor.
 *@a_doc: the xml document to edit
 *@a_doc_name: an alternative name to xml doc in case the document has no name.
 *
 *Edits the xml document given in argument. For the moment, does not support validation.
 *
 */
void
mlview_editor_edit_xml_document (MlViewEditor *a_editor, xmlDocPtr a_doc, gchar * a_doc_name)
{
	MlViewXMLDocumentView * doc_view = NULL ;
	MlViewXMLDocument * mlview_xml_doc = NULL ;

	g_return_if_fail (a_editor != NULL) ;
	g_return_if_fail (MLVIEW_IS_EDITOR (a_editor)) ;
	g_return_if_fail (PRIVATE(a_editor) != NULL) ;
	g_return_if_fail(a_doc != NULL) ;

	mlview_xml_doc = mlview_xml_document_new (a_doc, PRIVATE (a_editor)->app_context) ;
	doc_view = MLVIEW_XML_DOCUMENT_VIEW (mlview_xml_doc_tree_view_new (mlview_xml_doc, "",
									   PRIVATE (a_editor)->app_context)) ;
	mlview_editor_add_xml_document_view (a_editor, doc_view) ;
}


/**
 *mlview_editor_add_child_node_interactive:
 *@a_editor: the current instance of MlViewEditor
 *
 *Adds a child node to the currently selected node. It displays a node type picker to let
 *the user choose the type of node he wants.
 *
 *
 */
void
mlview_editor_add_child_node_interactive (MlViewEditor *a_editor)
{	
	g_return_if_fail (a_editor != NULL) ;
	g_return_if_fail (MLVIEW_IS_EDITOR (a_editor)) ;
	g_return_if_fail (PRIVATE(a_editor) != NULL) ;

	if(PRIVATE (a_editor)->current_xml_doc_view)
		mlview_xml_doc_tree_view_add_child_node_interactive (MLVIEW_XML_DOC_TREE_VIEW (PRIVATE (a_editor)->current_xml_doc_view)) ;
}


/**
 *mlview_editor_insert_prev_sibing_node_interactive:
 *@a_editor:
 *
 */
void
mlview_editor_insert_prev_sibling_node_interactive (MlViewEditor *a_editor)
{
	g_return_if_fail (a_editor != NULL) ;
	g_return_if_fail (MLVIEW_IS_EDITOR (a_editor)) ;
	g_return_if_fail (PRIVATE (a_editor) != NULL) ;
	
	if (PRIVATE(a_editor)->current_xml_doc_view)
		mlview_xml_doc_tree_view_insert_prev_sibling_node_interactive (MLVIEW_XML_DOC_TREE_VIEW (PRIVATE (a_editor)->current_xml_doc_view)) ;
}


/**
 *mlview_editor_insert_next_sibing_node_interactive:
 *@a_editor:
 *
 */
void
mlview_editor_insert_next_sibling_node_interactive (MlViewEditor *a_editor)
{
	g_return_if_fail(a_editor != NULL) ;
	g_return_if_fail (MLVIEW_IS_EDITOR (a_editor)) ;
	g_return_if_fail(PRIVATE(a_editor) != NULL) ;
	
	if (PRIVATE (a_editor)->current_xml_doc_view)
		mlview_xml_doc_tree_view_insert_next_sibling_node_interactive (MLVIEW_XML_DOC_TREE_VIEW (PRIVATE (a_editor)->current_xml_doc_view)) ;
}


/**
 *mlview_editor_cut_node:
 *@a_editor:
 *
 *Cuts the current selected node and it children
 */
void
mlview_editor_cut_node (MlViewEditor *a_editor)
{
	g_return_if_fail (a_editor != NULL) ;
	g_return_if_fail (MLVIEW_IS_EDITOR (a_editor)) ;
	g_return_if_fail (PRIVATE(a_editor) != NULL) ;

	if (PRIVATE (a_editor)->current_xml_doc_view)
		mlview_xml_doc_tree_view_cut_node (MLVIEW_XML_DOC_TREE_VIEW (PRIVATE (a_editor)->current_xml_doc_view)) ;
}


/**
 *mlview_editor_copy_node:
 *@a_editor:
 *
 *Copy the current selected node and it children.
 */
void
mlview_editor_copy_node (MlViewEditor *a_editor)
{
	g_return_if_fail (a_editor != NULL) ;
	g_return_if_fail (MLVIEW_IS_EDITOR (a_editor)) ;
	g_return_if_fail (PRIVATE(a_editor) != NULL) ;

	if (PRIVATE (a_editor)->current_xml_doc_view)
		mlview_xml_doc_tree_view_copy_node (MLVIEW_XML_DOC_TREE_VIEW (PRIVATE (a_editor)->current_xml_doc_view)) ;
}


/**
 *mlview_editor_paste_node_as_child:
 *@a_editor:
 *
 */
void 
mlview_editor_paste_node_as_child (MlViewEditor *a_editor)
{
	g_return_if_fail (a_editor != NULL) ;
	g_return_if_fail (MLVIEW_IS_EDITOR (a_editor)) ;
	g_return_if_fail (PRIVATE (a_editor) != NULL) ;
	
	mlview_xml_doc_tree_view_paste_node_as_child (MLVIEW_XML_DOC_TREE_VIEW (PRIVATE (a_editor)->current_xml_doc_view)) ;
}

/**
 *mlview_editor_paste_node_as_prev_sibling:
 *@a_editor:
 *
 */
void 
mlview_editor_paste_node_as_prev_sibling (MlViewEditor *a_editor)
{
	g_return_if_fail (a_editor != NULL) ;
	g_return_if_fail (MLVIEW_IS_EDITOR (a_editor)) ;
	g_return_if_fail (PRIVATE (a_editor)) ;

	mlview_xml_doc_tree_view_paste_node_as_prev_sibling (MLVIEW_XML_DOC_TREE_VIEW (PRIVATE (a_editor)->current_xml_doc_view)) ;
}


/**
 *mlview_editor_paste_node_as_next_sibling:
 *@a_editor:
 *
 */
void 
mlview_editor_paste_node_as_next_sibling (MlViewEditor *a_editor)
{
	g_return_if_fail (a_editor != NULL) ;
	g_return_if_fail (MLVIEW_IS_EDITOR (a_editor)) ;
	g_return_if_fail (PRIVATE (a_editor) != NULL) ;
	
	mlview_xml_doc_tree_view_paste_node_as_next_sibling (MLVIEW_XML_DOC_TREE_VIEW (PRIVATE (a_editor)->current_xml_doc_view)) ;
}

void 
mlview_editor_expand_tree_to_depth_interactive (MlViewEditor *a_editor)
{
	g_return_if_fail (a_editor != NULL) ;
	g_return_if_fail (MLVIEW_IS_EDITOR (a_editor)) ;
	g_return_if_fail (PRIVATE (a_editor) != NULL) ;
	
	if (PRIVATE (a_editor)->current_xml_doc_view == NULL
	    || MLVIEW_IS_XML_DOC_TREE_VIEW (PRIVATE (a_editor)->current_xml_doc_view) == FALSE)
		return ;

	mlview_xml_doc_tree_view_expand_tree_to_depth_interactive (MLVIEW_XML_DOC_TREE_VIEW (PRIVATE (a_editor)->current_xml_doc_view)) ;
}

/**
 *mlview_editor_get_current_xml_doc_file_path:
 *@a_editor:
 *
 *
 */
gchar *
mlview_editor_get_current_xml_doc_file_path(MlViewEditor *a_editor)
{
	MlViewXMLDocument * doc = NULL ;

	g_return_val_if_fail (a_editor != NULL, NULL) ;
	g_return_val_if_fail (MLVIEW_IS_EDITOR (a_editor), NULL) ;
	g_return_val_if_fail (PRIVATE (a_editor) != NULL, NULL) ;

	doc = mlview_xml_document_view_get_document (PRIVATE (a_editor)->current_xml_doc_view) ;
	g_return_val_if_fail (doc != NULL, NULL) ;

	return mlview_xml_document_get_file_path (doc) ;
}


/**
 *mlview_editor_save_xml_document:
 *@a_editor: the current instance of MlViewEditor
 *
 *
 */
void
mlview_editor_save_xml_document (MlViewEditor *a_editor)
{
	MlViewXMLDocument *xml_doc = NULL ;
	gchar *file_path = NULL ;

	g_return_if_fail (a_editor != NULL) ;
	g_return_if_fail (MLVIEW_IS_EDITOR (a_editor)) ;
	g_return_if_fail (PRIVATE (a_editor) != NULL) ;
	g_return_if_fail (PRIVATE (a_editor)->current_xml_doc_view != NULL) ;

	xml_doc = mlview_xml_document_view_get_document (PRIVATE (a_editor)->current_xml_doc_view) ;

	if ( xml_doc == NULL )
		return ;

	file_path = mlview_editor_get_current_xml_doc_file_path (a_editor) ;

	if(file_path == NULL)
		mlview_editor_save_xml_document_as_interactive (a_editor) ;
	else
		mlview_xml_document_save (xml_doc) ;
}

/**
 *mlview_editor_save_xml_document_as:
 *
 */
void
mlview_editor_save_xml_document_as (MlViewEditor *a_editor,
				    gchar *a_file_path)
{
	MlViewXMLDocument * mlview_xml_document = NULL ;
	gchar *base_name = NULL ;
	gboolean file_was_untitled = FALSE ;

	g_return_if_fail (a_editor != NULL) ;
	g_return_if_fail (MLVIEW_IS_EDITOR (a_editor)) ;
	g_return_if_fail (PRIVATE (a_editor) != NULL) ;
	g_return_if_fail (PRIVATE (a_editor)->current_xml_doc_view != NULL) ;
	g_return_if_fail (a_file_path != NULL) ;

	mlview_xml_document =
		mlview_xml_document_view_get_document (PRIVATE (a_editor)->current_xml_doc_view) ;

	mlview_app_context_sbar_push_message (PRIVATE (a_editor)->app_context,
					      _("Saving xml document as file %s ..."), a_file_path) ;

	file_was_untitled = 
		( mlview_xml_document_view_get_file_descriptor (PRIVATE (a_editor)->current_xml_doc_view) 
		  == 
		  NULL ) ;

	mlview_xml_document_save_as (mlview_xml_document, 
				     a_file_path) ;

	g_return_if_fail (PRIVATE (a_editor)->notebook != NULL) ;

	if (file_was_untitled) {
		base_name = g_basename (a_file_path) ;
		mlview_xml_document_view_set_name (PRIVATE (a_editor)->current_xml_doc_view,
						   base_name) ;
	}

	mlview_app_context_sbar_pop_message (PRIVATE (a_editor)->app_context) ;
}


/**
 *mlview_editor_save_xml_document_as_interactive:
 *@a_editor:
 *
 */
void 
mlview_editor_save_xml_document_as_interactive (MlViewEditor *a_editor)
{
	MlViewFileSelection * file_sel ;
	enum MLVIEW_SELECTED_BUTTON button ;
	gchar * file_name=NULL ;

	g_return_if_fail (a_editor != NULL) ;	
	g_return_if_fail (MLVIEW_IS_EDITOR (a_editor)) ;
	g_return_if_fail (PRIVATE (a_editor) != NULL) ;
	g_return_if_fail (PRIVATE (a_editor)->app_context != NULL) ;

	file_sel = mlview_app_context_get_file_selector(PRIVATE (a_editor)->app_context,
							_("Choose a xml document")) ;

	g_return_if_fail (file_sel) ;
	gtk_widget_realize (GTK_WIDGET (file_sel)) ;
	mlview_app_context_set_window_icon (PRIVATE (a_editor)->app_context, GTK_WIDGET (file_sel)) ;
	button = mlview_file_selection_run (file_sel,TRUE) ;

	switch ( button ) {
	case OK_BUTTON:
		file_name = gtk_file_selection_get_filename (GTK_FILE_SELECTION (file_sel)) ;
		if ( file_name  &&  strcmp(file_name, "") ) {
			mlview_editor_save_xml_document_as (a_editor,file_name) ;
		}
		break ;
	case CANCEL_BUTTON:
	case WINDOW_CLOSED:
	default :
		break ;
	}
}

/**
 *mlview_editor_close_xml_document_without_saving:
 *@a_editor: the current instance of MlViewEditor.
 *
 *FIXME: actually, this function is over bugged. It only save the document and
 *closes the current view.
 */
void 
mlview_editor_close_xml_document_without_saving (MlViewEditor * a_editor)
{
	g_return_if_fail(a_editor != NULL) ;
	g_return_if_fail (MLVIEW_IS_EDITOR (a_editor)) ;
	g_return_if_fail (PRIVATE (a_editor) != NULL) ;

	mlview_editor_remove_xml_document_view (a_editor, PRIVATE (a_editor)->current_xml_doc_view) ;
}


/**
 *mlview_editor_save_and_close_xml_document:
 *@a_editor: the current instance of MlViewEditor.
 *
 *
 */
void 
mlview_editor_save_and_close_xml_document (MlViewEditor *a_editor)
{
	MlViewFileSelection * file_sel = NULL ;
	MlViewXMLDocument * mlview_xml_document = NULL ;
	enum MLVIEW_SELECTED_BUTTON button ;
	gchar * file_name = NULL ;

	g_return_if_fail (a_editor != NULL) ;
	g_return_if_fail (MLVIEW_IS_EDITOR (a_editor)) ;
	g_return_if_fail (PRIVATE (a_editor) != NULL) ;
	g_return_if_fail (PRIVATE (a_editor)->current_xml_doc_view != NULL) ;
	g_return_if_fail (PRIVATE (a_editor)->app_context != NULL) ;

	file_sel = mlview_app_context_get_file_selector (PRIVATE (a_editor)->app_context,
							_("Choose a xml document")) ;
	g_return_if_fail (file_sel) ;

	mlview_xml_document = mlview_xml_document_view_get_document (PRIVATE (a_editor)->current_xml_doc_view) ;
	g_return_if_fail (mlview_xml_document != NULL) ;

	gtk_widget_realize (GTK_WIDGET (file_sel)) ;
	mlview_app_context_set_window_icon (PRIVATE (a_editor)->app_context, GTK_WIDGET (file_sel)) ;
	button = mlview_file_selection_run (file_sel, TRUE) ;

	switch (button) {
	case OK_BUTTON:
		file_name = gtk_file_selection_get_filename (GTK_FILE_SELECTION(file_sel)) ;
		if ( file_name  &&  strcmp(file_name, "") ) {
			mlview_xml_document_save_as (mlview_xml_document, file_name) ;
			mlview_editor_close_xml_document_without_saving (a_editor) ;
		}
		break ;
	case CANCEL_BUTTON:
	case WINDOW_CLOSED:
		break ;
	}
}

/**
 *mlview_editor_close_all_xml_document_interactive:
 *@a_editor: the current instance of MlViewEditor.
 *
 */
void
mlview_editor_close_all_xml_documents_interactive (MlViewEditor *a_editor)
{
	GList * views = NULL, *mobile_view_ptr = NULL ;

	g_return_if_fail (a_editor != NULL) ;
	g_return_if_fail (MLVIEW_EDITOR (a_editor)) ;
	g_return_if_fail (PRIVATE (a_editor) != NULL) ;
	g_return_if_fail (PRIVATE (a_editor)->mlview_xml_doc_views != NULL) ;

	views = 
		build_view_list_from_hashtable (PRIVATE (a_editor)->mlview_xml_doc_views) ;

	if (views == NULL) return ;

	for (mobile_view_ptr = views ; 
	     mobile_view_ptr ;
	     mobile_view_ptr = mobile_view_ptr->next) {
		PRIVATE (a_editor)->current_xml_doc_view = (MlViewXMLDocumentView*) mobile_view_ptr->data ;
		mlview_editor_close_xml_document_interactive (a_editor) ;
		
	}
	
}


/**
 *
 *
 */
void
mlview_editor_close_xml_document_interactive (MlViewEditor * a_editor)
{
	MlViewXMLDocument *doc ;
	GtkWidget * dialog, *label ;
	guint go = 1 ;

	g_return_if_fail (a_editor != NULL) ;
	g_return_if_fail (MLVIEW_IS_EDITOR (a_editor)) ;
	g_return_if_fail (PRIVATE(a_editor)) ;

	
	doc = mlview_xml_document_view_get_document (PRIVATE (a_editor)->current_xml_doc_view) ;
	g_return_if_fail (doc != NULL) ;

	if(mlview_xml_document_needs_saving (doc) == FALSE) {
		mlview_editor_close_xml_document_without_saving (a_editor) ;
		return ;
	}
	
	dialog = gnome_dialog_new (_("Document has changed"),
				   _("Save before closing"), _("Close without saving"),
				   NULL) ;
	gtk_window_set_wmclass (GTK_WINDOW (dialog), "document-has-changed-dialog", "MlView") ;

	label = gtk_label_new (_("The document has been modifed. Should I save it before closing it ?")) ;
	gtk_widget_show_all (label) ;
	gtk_box_pack_start (GTK_BOX (GNOME_DIALOG (dialog)->vbox), 
			    label,TRUE,TRUE,0) ;
	gnome_dialog_close_hides (GNOME_DIALOG (dialog), TRUE) ;
	
	while (go) {
		int res ;
		gtk_widget_realize (GTK_WIDGET (dialog)) ;
		mlview_app_context_set_window_icon (PRIVATE (a_editor)->app_context, GTK_WIDGET (dialog)) ;
		res = gnome_dialog_run (GNOME_DIALOG (dialog)) ;
		if (res == 0) {
			gtk_widget_destroy (dialog) ;
			mlview_editor_save_and_close_xml_document (a_editor) ;
			go = 0 ;
		} else if (res == 1) {
			gtk_widget_destroy (dialog) ;
			mlview_editor_close_xml_document_without_saving (a_editor) ;
			go = 0 ;
		}
	}
}


/**
 *mlview_editor_find_xml_node_that_contains_str_interactive:
 *@a_editor: the current instance of MlViewEditor.
 *
 *
 */
GtkCTreeNode *
mlview_editor_find_xml_node_that_contains_str_interactive(MlViewEditor *a_editor)
{
	g_return_val_if_fail(a_editor != NULL, NULL) ;
	g_return_val_if_fail (MLVIEW_IS_EDITOR (a_editor), NULL) ;
	g_return_val_if_fail(PRIVATE(a_editor) != NULL, NULL) ;

	if(PRIVATE(a_editor)->current_xml_doc_view == NULL)
		return NULL ;
	return mlview_xml_doc_tree_view_find_xml_node_that_contains_str_interactive (MLVIEW_XML_DOC_TREE_VIEW (PRIVATE (a_editor)->current_xml_doc_view)) ;
}

/**
 *mlview_editor_edit_settings_interactive:
 *@a_editor: the current instance of MlViewEditor.
 *
 *Edits the settings.
 */
void
mlview_editor_edit_settings_interactive (MlViewEditor *a_editor)
{
	g_return_if_fail(a_editor != NULL) ;
	g_return_if_fail(MLVIEW_IS_EDITOR(a_editor)) ;
	g_return_if_fail(PRIVATE(a_editor) != NULL) ;
	g_return_if_fail(PRIVATE(a_editor)->settings_manager != NULL) ;

	mlview_settings_manager_edit_settings_interactive (PRIVATE (a_editor)->settings_manager) ;
}


/**
 *mlview_editor_associate_dtd_interactive:
 *
 *Associates a dtd to the current mlview xml document.
 *
 *Returns 0 if association worked, 1 if not xml document is opened,
 *,2 if association did not succeed (due to error during dtd parsing for example),
 *and -1 if the function contains bad parameter.
 *
 */
gint
mlview_editor_associate_dtd_interactive ( MlViewEditor *a_editor ) {
	
	MlViewXMLDocument * mlview_xml_doc = NULL ;
	g_return_val_if_fail (a_editor != NULL, -1) ;
	g_return_val_if_fail (MLVIEW_IS_EDITOR (a_editor), -1) ;
	g_return_val_if_fail (PRIVATE (a_editor) != NULL, -1) ;

	if ( ! PRIVATE(a_editor)->current_xml_doc_view )
		return 1 ;

	mlview_xml_doc = mlview_xml_document_view_get_document (PRIVATE (a_editor)->current_xml_doc_view) ;
	if (mlview_xml_doc == NULL)
		return 1 ;

	if ( mlview_xml_document_associate_dtd_interactive (mlview_xml_doc))
		return 0 ;

	return 2 ;
}

/**
 *mlview_editor_validate:
 *@a_editor: the current instance of MlViewEditor.
 *
 *validate the current selected document against the dtd it is associated to.
 *If there is no dtd associated to the document, the functions asks the user
 *to associate a dtd to it and then, does the validation
 */
gint
mlview_editor_validate (MlViewEditor * a_editor) {

	MlViewXMLDocument * doc = NULL ;

	g_return_val_if_fail(a_editor != NULL, -1) ;
	g_return_val_if_fail (MLVIEW_IS_EDITOR (a_editor), -1) ;
	g_return_val_if_fail(PRIVATE(a_editor) != NULL, -1) ;

	if (!PRIVATE(a_editor)->current_xml_doc_view)
		return 1 ;
	doc = mlview_xml_document_view_get_document (PRIVATE(a_editor)->current_xml_doc_view) ;

	if (doc == NULL)
		return 1 ;

	return mlview_xml_document_validate (doc) ;
}


/**
 *mlview_editor_destroy:
 *@a_object: the destroyer of the MlViewEditor instances.
 */
void
mlview_editor_destroy (GtkObject *a_object)
{
	MlViewEditor *editor ;
  
	g_return_if_fail (a_object != NULL) ;
	g_return_if_fail (MLVIEW_IS_EDITOR(a_object));

	editor = MLVIEW_EDITOR (a_object) ;

	/*destroy the allocated widgets and other objects*/
	if (PRIVATE(editor) != NULL) {		
		if (PRIVATE(editor)->mlview_xml_docs){
			g_hash_table_foreach (PRIVATE(editor)->mlview_xml_doc_views,
					      free_mlview_xml_doc_views_hash_elements,
					      NULL) ;
			g_hash_table_destroy (PRIVATE (editor)->mlview_xml_docs) ;
			PRIVATE (editor)->mlview_xml_docs = NULL ;
		}
		PRIVATE (editor)->current_xml_doc_view = NULL ;/*do not free this element because it has been freed as an element of the link list above*/
		if (PRIVATE (editor)->notebook != NULL) {
			gtk_widget_destroy (GTK_WIDGET (PRIVATE (editor)->notebook)) ;
			PRIVATE (editor)->notebook = NULL ;
		}
		if (PRIVATE (editor)->opened_file_base_names != NULL) {
			g_hash_table_destroy (PRIVATE (editor)->opened_file_base_names);
			PRIVATE (editor)->opened_file_base_names = NULL ;
		}
		if (PRIVATE(editor)->opened_file_paths) {
			g_hash_table_destroy (PRIVATE (editor)->opened_file_paths) ;
			PRIVATE (editor)->opened_file_paths = NULL ;
		}
		if (PRIVATE (editor)->opened_document_label_names) {
			g_hash_table_destroy(PRIVATE(editor)->opened_document_label_names) ;
			PRIVATE(editor)->opened_file_paths = NULL ;
		}		
		if (PRIVATE (editor)->settings_builder) {
			/*remember: this frees also the settings manager.*/
			mlview_settings_builder_destroy (GTK_OBJECT (PRIVATE (editor)->settings_builder)) ;
			PRIVATE (editor)->settings_builder = NULL ;
		}
	}
	/*call the destroy method of the parent*/
	if (GTK_OBJECT_CLASS (parent_class)->destroy)
		(*GTK_OBJECT_CLASS (parent_class)->destroy) (a_object) ;
	
}

