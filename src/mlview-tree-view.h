/* -*- Mode: C++; indent-tabs-mode:true ; c-basic-offset: 4-*- */
/*
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute 
 *it and/or modify it under the terms of 
 *the GNU General Public License as published by the 
 *Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */

#ifndef __MLVIEW_TREE_VIEW_H__
#define __MLVIEW_TREE_VIEW_H__

#include <libxml/tree.h>
#include "mlview-tree-editor.h"
#include "mlview-node-editor.h"
#include "mlview-app-context.h"
#include "mlview-view-adapter.h"
#include "mlview-action.h"
#include "mlview-completion-table.h"

/**
 *@file
 *The declaration of the #MlViewTreeView class.
 */

namespace mlview {

struct TreeViewPriv ;
class TreeView : public ViewAdapter  {
friend struct TreeViewPriv ;

	struct TreeViewPriv *m_priv ;

	//forbid copy/assignation
	TreeView (TreeView const&) ;
	TreeView& operator= (TreeView const&) ;

	public:
	TreeView (MlViewXMLDocument *a_doc, const UString &a_name) ;

	virtual ~TreeView () ;

	bool get_must_rebuild_upon_document_reload () ;

	MlViewTreeEditor * get_current_tree_editor ();

	MlViewNodeEditor * get_node_editor ();

	MlViewCompletionTable* get_completion_widget () ;

	Gtk::Notebook * get_tree_editors_notebook () ;

	enum MlViewStatus create_internal_subset_node_interactive () ;

	void add_child_element_node (const UString &a_element_name) ;

	void add_child_text_node (const UString &a_text) ;

	void add_child_node_interactive ();

	void insert_sibling_node_interactive ();

	void insert_prev_sibling_node_interactive ();

	void insert_prev_sibling_text_node (const UString &a_text) ;

	void insert_next_sibling_text_node (const UString &a_text) ;

	void insert_prev_sibling_element_node (const UString &a_element_name) ;

	void insert_next_sibling_element_node (const UString &a_element_name) ;

	void insert_next_sibling_node_interactive ();

	void cut_node ();

	void comment_current_node () ;

	void uncomment_current_node () ;

	void copy_node ();

	void paste_node_as_child ();

	void paste_node_as_prev_sibling ();

	void paste_node_as_next_sibling ();

	void set_upper_paned1_proportions (const guint a_percentage);

	void find_xml_node_that_contains_str_interactive ()  ;

	void set_main_paned_proportions (const guint a_percentage);

	void set_all_paned_proportions (const guint a_main_paned_percentage,
	                                const guint a_node_editor_paned_percentage);

	void set_xml_document_path (const UString &a_file_path);

	void expand_tree_to_depth_interactive ();

	enum MlViewStatus reload_xml_document () ;

	enum MlViewStatus get_contextual_menu (GtkWidget **a_menu_ptr) ;

	enum MlViewStatus get_edit_menu_for_application (GtkWidget **a_menu_ptr) ;

	void update_contextual_menu (GtkMenu ** a_menu_ptr);

	enum MlViewStatus focus_on_node_editor () ;

	enum MlViewStatus focus_on_tree_editor () ;

	void select_parent_node () ;

	void select_prev_sibling_node () ;

	void select_next_sibling_node () ;

	GtkUIManager * get_ui_manager () ;

	enum MlViewStatus execute_action (MlViewAction *a_action) ;
	enum MlViewStatus undo () ;
	enum MlViewStatus redo () ;
	bool can_undo () ;
	bool can_redo () ;

	enum MlViewStatus handle_contextual_menu_request
	(GtkWidget *a_source_widget,
	 GdkEvent *a_event) ;

	//************
	// signals
	//*************
	sigc::signal0<void>& signal_document_changed () ;

protected:
	//*************
	//callbacks
	//*************
	void on_tree_editor_selected (GtkNotebookPage *a_page,
	                              guint a_page_num) ;
	void on_realized () ;

	void on_is_swapped_in () ;
	void on_is_swapped_out () ;
	void on_application_menu_populating_requested (gpointer a_user_data) ;
	void on_contextual_menu_requested (GtkWidget *a_source_widget,
									   GdkEvent *a_event);
	void on_view_swapped (mlview::Object *a_old_view, 
						  mlview::Object* a_new_view) ;

	//******************
	//protected methods
	//******************
	const UString build_edit_menu_root_path (bool a_popup) ;

	enum MlViewStatus build_edit_menu_body (const UString &a_menu_root_path) ;

	enum MlViewStatus build_contextual_menu2 () ;

	enum MlViewStatus build_app_edit_menu () ;

	GtkDialog * get_expand_tree_dialog ()  ;

	void clear_completion_popup_submenus () ;

	void update_completion_popup_submenu2 
			 (const UString &a_menu_root_path,
			  xmlNodePtr a_node,
			  enum NODE_INSERTION_SCHEME a_insertion_scheme) ;



	enum MlViewStatus connect_to_doc (MlViewXMLDocument *a_doc) ;

	enum MlViewStatus disconnect_from_doc (MlViewXMLDocument *a_doc) ;

	enum MlViewStatus activate_or_deactivate_proper_menu_items2
	(const gchar *a_menu_root_path) ;

}
;//class TreeView

}//namespace mlview

#endif
