
#ifndef __mlview_marshal_MARSHAL_H__
#define __mlview_marshal_MARSHAL_H__

#include	<glib-object.h>

G_BEGIN_DECLS

/* VOID:VOID (mlview-marshal.list:26) */
#define mlview_marshal_VOID__VOID	g_cclosure_marshal_VOID__VOID

/* VOID:POINTER,POINTER (mlview-marshal.list:27) */
extern void mlview_marshal_VOID__POINTER_POINTER (GClosure     *closure,
	        GValue       *return_value,
	        guint         n_param_values,
	        const GValue *param_values,
	        gpointer      invocation_hint,
	        gpointer      marshal_data);

/* VOID:POINTER,POINTER,POINTER (mlview-marshal.list:28) */
extern void mlview_marshal_VOID__POINTER_POINTER_POINTER (GClosure     *closure,
	        GValue       *return_value,
	        guint         n_param_values,
	        const GValue *param_values,
	        gpointer      invocation_hint,
	        gpointer      marshal_data);

/* VOID:POINTER (mlview-marshal.list:29) */
#define mlview_marshal_VOID__POINTER	g_cclosure_marshal_VOID__POINTER

/* BOOLEAN:POINTER (mlview-marshal.list:30) */
extern void mlview_marshal_BOOLEAN__POINTER (GClosure     *closure,
	        GValue       *return_value,
	        guint         n_param_values,
	        const GValue *param_values,
	        gpointer      invocation_hint,
	        gpointer      marshal_data);

/* VOID:INT (mlview-marshal.list:31) */
#define mlview_marshal_VOID__INT	g_cclosure_marshal_VOID__INT

/* VOID:STRING,STRING (mlview-marshal.list:32) */
extern void mlview_marshal_VOID__STRING_STRING (GClosure     *closure,
	        GValue       *return_value,
	        guint         n_param_values,
	        const GValue *param_values,
	        gpointer      invocation_hint,
	        gpointer      marshal_data);

/* VOID:POINTER,STRING,STRING,BOOLEAN,INT,INT,INT (mlview-marshal.list:33) */
extern void mlview_marshal_VOID__POINTER_STRING_STRING_BOOLEAN_INT_INT_INT (GClosure     *closure,
	        GValue       *return_value,
	        guint         n_param_values,
	        const GValue *param_values,
	        gpointer      invocation_hint,
	        gpointer      marshal_data);

G_END_DECLS

#endif /* __mlview_marshal_MARSHAL_H__ */

