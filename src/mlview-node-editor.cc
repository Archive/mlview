/* -*- Mode: C++; indent-tabs-mode:nil; c-basic-offset: 4-*- */
/*
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute 
 *it and/or modify it under the terms of 
 *the GNU General Public License as published by the 
 *Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */

/**
 *TODO: apply the editing transaction scheme initiated with
 *the text node view to all the types of views of the node editor.
 *This is urgent !!!!!!
 */

/**
 *@file
 *The definition of the #MlViewNodeEditor widget.
 *
 *This widget edits a given xml node.
 *Some of it methods take an xmlNodePtr 
 *in parameter and allow the user to
 *interactively edit the xml node. 
 *It emits signal whenever the xml 
 *node being edited changes.
 *The user of this widget can thus 
 *catch and handle the emitted signals.
 *An xml node can have several types
 *(element node, text node, processing instruction node, comment node).
 *Each type of node is edited using what I
 *call a view. As they are several types of nodes, they are several
 *types of view; one for each type of node. 
 *A particular view is a widget that can 
 *edit a particular type of xml node
 *Each view is abstracted by a structure 
 *that holds the view widget.
 *The #MlViewElementWidget contains all the
 *possible views it can handle. 
 *Whenever an xml node is to be edited, the proper view
 *is then selected and used to edit the node.
 *The selection of the right view is done in 
 *the method mlview_node_editor_edit_xml_node(). 
 *
 */
#include <ctype.h>
#include <string.h>
#include <libxml/tree.h>
#include <glade/glade.h>
#include <gdk/gdkkeysyms.h>
#include "mlview-utils.h"
#include "mlview-node-editor.h"
#include "mlview-xml-document.h"
#include "mlview-attrs-editor.h"
#include "mlview-ns-editor.h"
#include "mlview-kb-eng.h"
#include "mlview-exception.h"

typedef struct _XMLElementNodeView XMLElementNodeView;
typedef struct _XMLCommentNodeView XMLCommentNodeView;
typedef struct _XMLCDataSectionNodeView XMLCDataSectionNodeView;
typedef struct _XMLTextNodeView XMLTextNodeView;
typedef struct _XMLPINodeView XMLPINodeView;
typedef struct _XMLDocNodeView XMLDocNodeView;

/*===========================================================
 *Some structure used in the private data part of MlViewElementEditor.
 *===========================================================*/

/**
 *This structure represents the element node view 
 */
struct _XMLElementNodeView
{
	GtkVBox *vbox;
	/*a GtkEntry: contains the current element name */
	GtkEntry *name;
	/*the widget that does attributes edition*/
	MlViewAttrsEditor *attrs_editor ;
	/*The namespaces editor */
	MlViewNSEditor *ns_editor ;
	guint name_changed_handler_id;
	gboolean started_editing_transaction ;
	xmlNode *transaction_node ;
};


/**
 *The text node view
 */
struct _XMLTextNodeView
{
	GtkVBox *vbox ;
	GtkTextView *widget ;
	gboolean started_editing_transaction ;
	xmlNode *transaction_node ;
} ;

/**
 *The comment node view. 
 *
 */
struct _XMLCommentNodeView
{
	GtkVBox *vbox;
	GtkTextView *widget ;
	gboolean started_editing_transaction ;
	xmlNode *transaction_node ;
};

/**
 *The cdata section node view. 
 *
 */
struct _XMLCDataSectionNodeView
{
	GtkVBox *vbox;
	GtkTextView *widget ;
	gboolean started_editing_transaction ;
	xmlNode *transaction_node ;
};

/**
 *The processing instruction node view
 */
struct _XMLPINodeView
{
	GtkVBox *vbox;
	GtkEntry *name;
	GtkTextView *widget ;
	guint name_changed_handler_id;
};

/**
 *The document node view.
 */
struct _XMLDocNodeView
{
	GtkVBox *vbox;

	/*the name of this document ... not mandatory */
	GtkEntry *name;

	guint name_changed_handler_id;

	/*wether this document is mandatory or not */
	GtkCheckButton *standalone;

	/*the xml version */
	GtkEntry *xml_version;

	/*the encoding of the on disk file */
	GtkComboBox *external_encoding;

	/*the external id of the external subset */
	GtkEntry *ext_subset_external_id;

	/*the system id of the external subset */
	struct
	{
		GtkListStore *store;
		GHashTable *references;
		GtkComboBox *combo;
	}
	ext_subset_system_id;
};


/*******************************************************
 *The private part of the MlViewNodeEditor structure.
 *******************************************************/

struct _MlViewNodeEditorPrivate
{
	/*the left part of the MlViewNodeEditor widget */
	GtkVBox *left_margin;

	/*Right part of element editor.
	 *This widget shows a view appropriated
	 *to the type of xml element
	 *being edited. Let's say the xml node 
	 *being edited is a text node, the view (widget) used to edit it
	 *will be a GtkText. If the xml node 
	 *being edited is an element node, the view used to edit it
	 *will be more complex: it will be a widget 
	 *that enable the user to edit the xml element name and
	 *it attributes ...
	 */
	GtkNotebook *node_view;

	/*the current xml node being edited */
	xmlNode *curr_xml_node;

	MlViewXMLDocument *curr_xml_document;

	/*the view used to edit an xml element node */
	XMLElementNodeView *element_node_view;

	/*the view used to edit an xml text node */
	XMLTextNodeView *text_node_view;

	/*the view used to edit an xml comment node */
	XMLCommentNodeView *comment_node_view;

	/*the view used to edit an xml cdata section node */
	XMLCDataSectionNodeView *cdata_section_node_view;

	/*the view used to edit an xml pi node */
	XMLPINodeView *pi_node_view;

	/*the view used to edit an xml doc node */
	XMLDocNodeView *doc_node_view;

	GtkWidget *cur_focusable_widget ;

	/*Keybinding engine. It is responsible of parsing
	 *the keyboard inputs to detect a combination predefined as
	 *"keyboard shortcut, aka keybinding". 
	 *Upon detection of a given keybinding sequence, 
	 *the keybinding engine can call a given editing function.
	 *Grep the file a bit to see how kb_eng is used to learn about
	 *it.
	 */
	MlViewKBEng *kb_eng ;

	guint xml_node_type_changed_handler_id;

	guint left_right_percentage;

	gboolean dispose_has_run ;
};

/*
 *This enum defines the signals emited by MlViewNodeEditor.
 *It is used as the offset of the signal in 
 *the private p_table mlview_node_editor_signals.
 */
enum {
    ELEMENT_CHANGED,
    EDIT_STATE_CHANGED,
    ELEMENT_NAME_CHANGED,
    ELEMENT_ATTRIBUTE_CHANGED,
    ELEMENT_CONTENT_CHANGED,
    UNGRAB_FOCUS_REQUESTED,
    NUMBER_OF_SIGNALS
};

/*
 *This enum defines the notebook pages that hold
 *the possibles views of an xml node. It is used
 *as the offset of matching notebook pages.
 *Make sure that the order of the construction of 
 *the views (of the notebook pages)
 *(at the end of mlview_node_editor_init()) 
 *matches the order defined by this enum.
 */
enum {
    ELEMENT_NODE_VIEW_PAGE,
    TEXT_NODE_VIEW_PAGE,
    COMMENT_NODE_VIEW_PAGE,
    CDATA_SECTION_VIEW_PAGE,
    PI_NODE_VIEW_PAGE,
    DOC_NODE_VIEW_PAGE
};

/*the macro used to access the private part of the MlViewNodeEditor*/
#define PRIVATE(node_editor) ((node_editor)->priv)

#define ELEMENT_NODE_VIEW(node_editor) (PRIVATE(node_editor)->element_node_view) /*acess the element node view ... */
#define TEXT_NODE_VIEW(node_editor)  (PRIVATE(node_editor)->text_node_view)
#define COMMENT_NODE_VIEW(node_editor) (PRIVATE(node_editor)->comment_node_view)
#define CDATA_SECTION_NODE_VIEW(node_editor) (PRIVATE(node_editor)->cdata_section_node_view)
#define PI_NODE_VIEW(node_editor) (PRIVATE(node_editor)->pi_node_view)
#define DOC_NODE_VIEW(node_editor) (PRIVATE(node_editor)->doc_node_view)
#define CURRENT_XML_NODE(node_editor) (PRIVATE(node_editor)->curr_xml_node)
#define DEFAULT_LEFT_RIGHT_PERCENTAGE 20


/*private functions declarations*/
static void mlview_node_editor_build_xml_element_node_view (MlViewNodeEditor * a_node_editor);

static void mlview_node_editor_build_xml_text_node_view (MlViewNodeEditor * a_node_editor);

static void mlview_node_editor_build_xml_comment_node_view (MlViewNodeEditor * a_node_editor);

static void mlview_node_editor_build_xml_cdata_section_node_view (MlViewNodeEditor * a_node_editor);

static void mlview_node_editor_build_xml_pi_node_view (MlViewNodeEditor * a_node_editor);

static void mlview_node_editor_build_xml_doc_node_view (MlViewNodeEditor * a_node_editor);

static void mlview_node_editor_clear_xml_element_node_view (MlViewNodeEditor * a_node_editor);

static void mlview_node_editor_clear_xml_text_node_view (MlViewNodeEditor * a_node_editor);

static void mlview_node_editor_clear_xml_comment_node_view (MlViewNodeEditor * a_node_editor);

static void mlview_node_editor_clear_xml_cdata_section_node_view (MlViewNodeEditor * a_node_editor);

static void mlview_node_editor_clear_xml_pi_node_view (MlViewNodeEditor * a_node_editor);

static void mlview_node_editor_xml_element_node_view_edit_xml_node (MlViewNodeEditor * a_editor,
        MlViewXMLDocument * a_xml_doc,
        xmlNode * a_node);

static void mlview_node_editor_xml_element_node_view_commit_edit_trans (MlViewNodeEditor *a_this) ;

static void mlview_node_editor_xml_text_node_view_edit_xml_node (MlViewNodeEditor * a_editor,
        MlViewXMLDocument * a_xml_doc, xmlNode * a_node);

static void mlview_node_editor_xml_text_node_view_commit_edit_trans (MlViewNodeEditor *a_this) ;

static void mlview_node_editor_xml_comment_node_view_edit_xml_node (MlViewNodeEditor * a_editor,
        MlViewXMLDocument * a_xml_doc,
        xmlNode * a_node);

static void mlview_node_editor_xml_comment_node_view_commit_edit_trans (MlViewNodeEditor *a_this) ;

static void mlview_node_editor_xml_cdata_section_node_view_commit_edit_trans (MlViewNodeEditor *a_this) ;

static void mlview_node_editor_xml_cdata_section_node_view_edit_xml_node (MlViewNodeEditor * a_editor,
        MlViewXMLDocument * a_xml_doc,
        xmlNode * a_node);

static void
mlview_node_editor_xml_pi_node_view_edit_xml_node (MlViewNodeEditor * a_editor,
												   MlViewXMLDocument * a_xml_doc,
												   xmlNode * a_node);

static void mlview_node_editor_xml_doc_node_view_edit_xml_node (MlViewNodeEditor * a_editor,
        MlViewXMLDocument * a_xml_doc,
        xmlNode * a_node);


static void mlview_node_editor_class_init (MlViewNodeEditorClass * a_klass);

static void mlview_node_editor_init (MlViewNodeEditor * a_node_editor);

static void mlview_node_editor_construct (MlViewNodeEditor * a_node_editor);

static void mlview_node_editor_commit_editing_transaction (MlViewNodeEditor *a_this) ;

static void mlview_node_editor_name_changed_cb (GtkWidget * a_entry,
												GdkEventFocus *a_event,
												MlViewNodeEditor * a_editor) ;

static void mlview_node_editor_attribute_changed_cb (MlViewAttrsEditor * a_attr_editor,
        gpointer a_editor);

static gboolean mlview_node_editor_content_changed_cb (GtkTextView * a_view,
        GdkEventFocus *a_event,
        MlViewNodeEditor *a_editor) ;

static void external_encoding_changed_cb (GtkComboBox * a_encoding_combo,
        MlViewNodeEditor * a_editor);

static void text_inserted_in_text_node_view_cb (GtkTextBuffer *a_text_buffer,
        GtkTextIter *a_iter,
        gchar *a_text,
        gint a_len,
        gpointer a_user_data) ;

static void text_range_deleted_in_text_node_view_cb (GtkTextBuffer *a_text_buffer,
        GtkTextIter *a_start,
        GtkTextIter *a_end,
        gpointer a_user_data) ;

static void text_inserted_in_element_name_cb (GtkEditable *a_editable,
        MlViewNodeEditor *a_this) ;

static void  text_inserted_in_comment_node_view_cb (GtkTextBuffer *a_text_buffer,
        GtkTextIter *a_iter,
        gchar *a_text,
        gint a_len,
        gpointer a_user_data) ;

static void  text_inserted_in_cdata_node_view_cb (GtkTextBuffer *a_text_buffer,
        GtkTextIter *a_iter,
        gchar *a_text,
        gint a_len,
        gpointer a_user_data) ;

static void ext_subset_system_id_combo_changed_cb (GtkComboBox *a_combo,
        MlViewNodeEditor *a_editor);

static void standalone_checkbtn_clicked_cb (GtkCheckButton *a_standalone_checkbtn,
        MlViewNodeEditor *a_this);

static void realize_cb (GtkWidget *a_this,
                        MlViewNodeEditor *a_editor) ;

static gboolean key_press_event_cb (GtkWidget *a_this,
                                    GdkEvent *a_event,
                                    gpointer a_user_data) ;

static void mlview_node_editor_dispose (GObject *a_this) ;

static void mlview_node_editor_finalize (GObject *a_this) ;

static void xml_doc_node_commented_cb (MlViewXMLDocument *a_xml_doc,
                                       xmlNode *a_node,
                                       xmlNode *a_new_node,
                                       gpointer a_user_data) ;

static void xml_doc_node_uncommented_cb (MlViewXMLDocument *a_xml_doc,
        xmlNode *a_node,
        xmlNode *a_new_node,
        gpointer a_user_data) ;

static void xml_doc_replace_node_cb (MlViewXMLDocument *a_xml_doc,
                                     xmlNode *a_old_node,
                                     xmlNode *a_new_node,
                                     gpointer a_user_data) ;

static void ext_subset_changed_cb (MlViewXMLDocument *a_doc,
                                   MlViewNodeEditor *a_editor) ;
/*private static data*/
static GtkHPanedClass *gv_parent_class;
static guint gv_mlview_node_editor_signals[NUMBER_OF_SIGNALS] ={ 0 };


static struct MlViewKBDef gv_keybindings [] =
    {
	    {
		    /*One entry of the gv_keybindings table*/
		    {
			    /*the key_inputs field of the MlViewKBDef*/
			    {GDK_Escape, (GdkModifierType) 0},
			    {GDK_o, (GdkModifierType) 0}
		    },
		    /*the key_input_s_len field of the MlViewKBDef*/
		    2,
		    /*the editing action of the MlViewrKBDef*/
		    (MlViewKBEngAction) mlview_node_editor_request_ungrab_focus,
		    "request-ungrab-focus"
	    },
	    {
	        /*One entry of the gv_keybindings table*/
	        {
	            /*the key_inputs field of the MlViewKBDef*/
	            {GDK_Escape, (GdkModifierType) 0},
	            {GDK_exclam, (GdkModifierType) 0}
	        },
	        /*the key_input_s_len field of the MlViewKBDef*/
	        2,
	        /*the editing action of the MlViewrKBDef*/
	        (MlViewKBEngAction) mlview_node_editor_commit_editing_transaction,
	        "commit-editing-transaction"
	    }
    } ;

/*================================================================
 *private helper functions
 *===============================================================*/

static void
mlview_node_editor_build_xml_element_node_view (MlViewNodeEditor* a_this)
{
	XMLElementNodeView *view = NULL;
	MlViewNodeEditorPrivate *private_data = NULL;
	gchar *gfile = NULL;
	GladeXML *gxml = NULL;
	GtkWidget *attribute_box = NULL,
	                           *namespace_box = NULL;

	const gchar *attributes_names_title =
	    N_("Attribute names");
	const gchar *attributes_values_title =
	    N_("Attribute values");
	THROW_IF_FAIL (a_this != NULL);

	if (PRIVATE (a_this) == NULL) {
		PRIVATE (a_this) = (MlViewNodeEditorPrivate *)
		                   g_try_malloc
		                   (sizeof (MlViewNodeEditorPrivate));
		if (!PRIVATE (a_this)) {
			mlview_utils_trace_debug ("g_try_malloc failed") ;
			return ;
		}
		memset (PRIVATE (a_this), 0,
		        sizeof (MlViewNodeEditorPrivate)) ;
	}
	if (ELEMENT_NODE_VIEW (a_this) == NULL) {
		ELEMENT_NODE_VIEW (a_this) = (XMLElementNodeView *)
		                             g_try_malloc (sizeof (XMLElementNodeView));
		if (!ELEMENT_NODE_VIEW (a_this)) {
			mlview_utils_trace_debug ("g_try_malloc failed") ;
			return ;
		}
		memset (ELEMENT_NODE_VIEW (a_this), 0,
		        sizeof (XMLElementNodeView)) ;
	} else {
		if (ELEMENT_NODE_VIEW (a_this)->vbox !=
		        NULL) {
			gtk_widget_destroy
			(GTK_WIDGET
			 (TEXT_NODE_VIEW
			  (a_this)->vbox));
		}
	}
	view = ELEMENT_NODE_VIEW (a_this);
	private_data = PRIVATE (a_this);

	gfile = gnome_program_locate_file
	        (NULL,
	         GNOME_FILE_DOMAIN_APP_DATADIR,
	         PACKAGE "/mlview-node-editor.glade",
	         TRUE,
	         NULL);

	if (!gfile)
		return;

	gxml = glade_xml_new (gfile, "ElementNodeBox", NULL);

	g_free (gfile);
	gfile = NULL;

	if (!gxml)
		return;


	view->vbox = GTK_VBOX (glade_xml_get_widget (gxml, "ElementNodeBox"));
	view->name = GTK_ENTRY (glade_xml_get_widget (gxml, "NameEntry"));
	view->name_changed_handler_id =
	    g_signal_connect (G_OBJECT (view->name),
	                      "focus-out-event",
	                      G_CALLBACK (mlview_node_editor_name_changed_cb),
	                      a_this);
	g_signal_connect (G_OBJECT (view->name),
	                  "changed",
	                  G_CALLBACK (text_inserted_in_element_name_cb),
	                  a_this) ;

	/*init the attributes and the namespace edition areas */
	view->attrs_editor = MLVIEW_ATTRS_EDITOR
	                     (mlview_attrs_editor_new
	                      ((gchar*)attributes_names_title,
	                       (gchar*)attributes_values_title)) ;

	g_signal_connect (G_OBJECT (view->attrs_editor),
	                  "attribute-changed",
	                  G_CALLBACK (mlview_node_editor_attribute_changed_cb),
	                  a_this);

	attribute_box = glade_xml_get_widget (gxml, "AttributesBox");
	gtk_box_pack_start (
	    GTK_BOX (attribute_box),
	    GTK_WIDGET (view->attrs_editor),
	    TRUE,
	    TRUE,
	    0);

	view->ns_editor = MLVIEW_NS_EDITOR
	                  (mlview_ns_editor_new
	                   (PRIVATE (a_this)->curr_xml_document)) ;

	namespace_box = glade_xml_get_widget (gxml, "NamespaceBox");
	gtk_box_pack_start (
	    GTK_BOX (namespace_box),
	    GTK_WIDGET (view->ns_editor),
	    TRUE,
	    TRUE,
	    0);

	/*FIXME: add signals on the namespaces editor */

	gtk_widget_show_all (GTK_WIDGET (view->vbox));

	gtk_notebook_append_page (private_data->node_view,
	                          GTK_WIDGET (view->vbox), NULL);
}

/**
 *Creates the text node view. This view will be used to edit text elements. 
 */
static void
mlview_node_editor_build_xml_text_node_view (MlViewNodeEditor * a_this)
{
	XMLTextNodeView *view = NULL;
	MlViewNodeEditorPrivate *private_data = NULL;
	GladeXML *gxml = NULL;
	gchar *gfile = NULL;
	GtkWidget *frame = NULL;
	GtkTextBuffer *text_buffer = NULL ;

	THROW_IF_FAIL (a_this != NULL);

	if (PRIVATE (a_this) == NULL)
		PRIVATE (a_this) = (MlViewNodeEditorPrivate *)
		                   g_malloc0 (sizeof
		                              (MlViewNodeEditorPrivate));

	private_data = PRIVATE (a_this);

	if (TEXT_NODE_VIEW (a_this) == NULL)
		TEXT_NODE_VIEW (a_this) = (XMLTextNodeView *)
		                          g_malloc0 (sizeof (XMLTextNodeView));
	else if (TEXT_NODE_VIEW (a_this)->vbox != NULL) {
		gtk_widget_destroy
		(GTK_WIDGET
		 (TEXT_NODE_VIEW (a_this)->vbox));
	}

	view = TEXT_NODE_VIEW (a_this);

	gfile = gnome_program_locate_file
	        (NULL,
	         GNOME_FILE_DOMAIN_APP_DATADIR,
	         PACKAGE "/mlview-node-editor.glade",
	         TRUE, NULL);

	if (!gfile)
		return;

	gxml = glade_xml_new (gfile, "TextnodeBox", NULL);

	g_free (gfile);
	gfile = NULL;

	if (!gxml)
		return;

	frame = glade_xml_get_widget (gxml, "TextnodeFrame");
	view->vbox = GTK_VBOX (glade_xml_get_widget (gxml, "TextnodeBox"));
	view->widget = GTK_TEXT_VIEW (glade_xml_get_widget (gxml, "TextnodeTextview"));
	if (!(view->widget && GTK_IS_TEXT_VIEW (view->widget)))
		return;

	g_signal_connect (G_OBJECT (view->widget),
	                  "focus-out-event",
	                  G_CALLBACK (mlview_node_editor_content_changed_cb),
	                  a_this) ;
	text_buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (view->widget)) ;
	THROW_IF_FAIL (text_buffer) ;
	g_signal_connect (G_OBJECT (text_buffer),
	                  "insert-text",
	                  G_CALLBACK (text_inserted_in_text_node_view_cb),
	                  a_this) ;
	g_signal_connect (G_OBJECT (text_buffer),
	                  "delete-range",
	                  G_CALLBACK (text_range_deleted_in_text_node_view_cb),
	                  a_this) ;

	g_signal_connect (G_OBJECT (view->widget),
	                  "realize",
	                  G_CALLBACK (realize_cb),
	                  a_this) ;
	gtk_widget_show_all (GTK_WIDGET (view->vbox));
	gtk_notebook_append_page (private_data->node_view,
	                          GTK_WIDGET (view->vbox), NULL);
}

/**
 *creates the xml comment node view. 
 *This view will be used by 
 *MlViewElement editor to
 *edit xml comment node. 
 */
static void
mlview_node_editor_build_xml_comment_node_view (MlViewNodeEditor * a_this)
{
	MlViewNodeEditorPrivate *private_data = NULL;
	XMLCommentNodeView *view = NULL;
	GtkTextBuffer *text_buffer = NULL ;
	GladeXML *gxml = NULL;
	gchar *gfile = NULL;

	THROW_IF_FAIL (a_this != NULL);

	if (PRIVATE (a_this) == NULL)
		PRIVATE (a_this) = (MlViewNodeEditorPrivate *)
		                   g_malloc0 (sizeof
		                              (MlViewNodeEditorPrivate));

	private_data = PRIVATE (a_this);

	if (COMMENT_NODE_VIEW (a_this) == NULL) {
		COMMENT_NODE_VIEW (a_this) = (XMLCommentNodeView *)
		                             g_malloc0 (sizeof (XMLCommentNodeView));
	} else {
		if (COMMENT_NODE_VIEW (a_this)->vbox !=
		        NULL) {
			gtk_widget_destroy (GTK_WIDGET
			                    (COMMENT_NODE_VIEW
			                     (a_this)->
			                     vbox));
		}
	}

	view = COMMENT_NODE_VIEW (a_this);
	gfile = gnome_program_locate_file
	        (NULL,
	         GNOME_FILE_DOMAIN_APP_DATADIR,
	         PACKAGE "/mlview-node-editor.glade",
	         TRUE,
	         NULL);

	if (!gfile)
		return;

	gxml = glade_xml_new (gfile, "CommentNodeBox", NULL);

	g_free (gfile);
	gfile = NULL;

	if (!gxml)
		return;

	view->vbox = GTK_VBOX (glade_xml_get_widget (gxml, "CommentNodeBox"));
	view->widget = GTK_TEXT_VIEW (glade_xml_get_widget (gxml, "CommentTextview"));
	text_buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (view->widget)) ;
	THROW_IF_FAIL (text_buffer) ;
	g_signal_connect (G_OBJECT (view->widget),
	                  "focus-out-event",
	                  G_CALLBACK (mlview_node_editor_content_changed_cb),
	                  a_this);
	g_signal_connect (G_OBJECT (text_buffer),
	                  "insert-text",
	                  G_CALLBACK (text_inserted_in_comment_node_view_cb),
	                  a_this) ;
	gtk_widget_show_all (GTK_WIDGET (view->vbox));
	gtk_notebook_append_page (private_data->node_view,
	                          GTK_WIDGET (view->vbox), NULL);
}


/**
 *Creates the xml cdata section view. 
 *This view will be used by 
 *MlViewNodeEditor to edit cdata section node.
 *
 */
static void
mlview_node_editor_build_xml_cdata_section_node_view (MlViewNodeEditor * a_this)
{
	MlViewNodeEditorPrivate *private_data = NULL;
	XMLCDataSectionNodeView *view = NULL;
	GtkTextBuffer *text_buffer = NULL ;
	GladeXML *gxml = NULL;
	gchar *gfile = NULL;

	THROW_IF_FAIL (a_this != NULL);

	if (PRIVATE (a_this) == NULL)
		PRIVATE (a_this) = (MlViewNodeEditorPrivate *)
		                   g_malloc0 (sizeof
		                              (MlViewNodeEditorPrivate));

	private_data = PRIVATE (a_this);
	if (CDATA_SECTION_NODE_VIEW (a_this) == NULL) {
		CDATA_SECTION_NODE_VIEW (a_this) = (XMLCDataSectionNodeView *)
		                                   g_malloc0 (sizeof
		                                              (XMLCDataSectionNodeView));
	} else {
		if (CDATA_SECTION_NODE_VIEW (a_this)->
		        vbox != NULL) {
			gtk_widget_destroy (GTK_WIDGET
			                    (CDATA_SECTION_NODE_VIEW
			                     (a_this)->
			                     vbox));
		}
	}

	view = CDATA_SECTION_NODE_VIEW (a_this);

	gfile = gnome_program_locate_file
	        (NULL,
	         GNOME_FILE_DOMAIN_APP_DATADIR,
	         PACKAGE "/mlview-node-editor.glade",
	         TRUE,
	         NULL);

	if (!gfile)
		return;

	gxml = glade_xml_new (gfile, "CDataNodeBox", NULL);

	g_free (gfile);
	gfile = NULL;

	if (!gxml)
		return;

	view->vbox = GTK_VBOX (glade_xml_get_widget (gxml, "CDataNodeBox"));
	view->widget = GTK_TEXT_VIEW (glade_xml_get_widget (gxml, "CDataTextview"));
	text_buffer = gtk_text_view_get_buffer (view->widget) ;
	g_signal_connect (G_OBJECT (view->widget),
	                  "focus-out-event",
	                  G_CALLBACK (mlview_node_editor_content_changed_cb),
	                  a_this);
	g_signal_connect (G_OBJECT (text_buffer),
	                  "insert-text",
	                  G_CALLBACK (text_inserted_in_cdata_node_view_cb),
	                  a_this) ;
	gtk_widget_show_all (GTK_WIDGET (view->vbox));
	gtk_notebook_append_page (private_data->node_view,
	                          GTK_WIDGET (view->vbox), NULL);
}

/**
 *builds the xml processing instruction 
 *node view included in the current instance of a_this. 
 *
 */
static void
mlview_node_editor_build_xml_pi_node_view (MlViewNodeEditor * a_this)
{
	XMLPINodeView *view = NULL;
	MlViewNodeEditorPrivate *private_data = NULL;
	GtkTextBuffer *text_buffer = NULL ;
	GladeXML *gxml = NULL;
	gchar *gfile = NULL;

	THROW_IF_FAIL (a_this != NULL);

	if (PRIVATE (a_this) == NULL)
		PRIVATE (a_this) = (MlViewNodeEditorPrivate *)
		                   g_malloc0 (sizeof
		                              (MlViewNodeEditorPrivate));
	private_data = PRIVATE (a_this);

	if (PI_NODE_VIEW (a_this) == NULL) {
		PI_NODE_VIEW (a_this) = (XMLPINodeView *)
		                        g_malloc0 (sizeof (XMLPINodeView));
	} else {
		if (PI_NODE_VIEW (a_this)->vbox != NULL) {
			gtk_widget_destroy
			(GTK_WIDGET
			 (PI_NODE_VIEW (a_this)->
			  vbox));
		}
	}

	view = PI_NODE_VIEW (a_this);

	gfile = gnome_program_locate_file
	        (NULL,
	         GNOME_FILE_DOMAIN_APP_DATADIR,
	         PACKAGE "/mlview-node-editor.glade",
	         TRUE,
	         NULL);

	if (!gfile)
		return;

	gxml = glade_xml_new (gfile, "PiNodeBox", NULL);

	g_free (gfile);
	gfile = NULL;

	if (!gxml)
		return;

	view->vbox = GTK_VBOX (glade_xml_get_widget (gxml, "PiNodeBox"));

	view->name = GTK_ENTRY (glade_xml_get_widget (gxml, "PiNodeNameEntry"));
	view->name_changed_handler_id =
	    g_signal_connect (G_OBJECT (view->name),
	                      "focus-out-event",
	                      G_CALLBACK (mlview_node_editor_name_changed_cb),
	                      a_this) ;

	view->widget = GTK_TEXT_VIEW (glade_xml_get_widget (gxml, "PiNodeTextview"));
	text_buffer = gtk_text_view_get_buffer (view->widget) ;
	g_signal_connect (G_OBJECT (view->widget),
	                  "focus-out-event",
	                  G_CALLBACK (mlview_node_editor_content_changed_cb),
	                  a_this);

	gtk_widget_show_all (GTK_WIDGET (view->vbox));

	gtk_notebook_append_page (private_data->node_view,
	                          GTK_WIDGET (view->vbox), NULL);
}


/**
 *Creates the view that can edit an xml doc node. 
 *TODO: verify the cleanup if something goes wrong.
 *
 */
static void
mlview_node_editor_build_xml_doc_node_view (MlViewNodeEditor * a_this)
{
	XMLDocNodeView *view = NULL;
	MlViewNodeEditorPrivate *private_data = NULL;
	GList *available_encodings = NULL, *p;
	guint curencoding = 0 ;
	GtkCellRenderer *renderer = NULL;
	GladeXML *gxml = NULL;
	gchar *gfile = NULL;

	THROW_IF_FAIL (a_this != NULL);

	if (PRIVATE (a_this) == NULL) {
		PRIVATE (a_this) = (MlViewNodeEditorPrivate *) g_malloc0
		                   (sizeof (MlViewNodeEditorPrivate));
	}

	private_data = PRIVATE (a_this);

	if (DOC_NODE_VIEW (a_this) == NULL) {
		DOC_NODE_VIEW (a_this) = (XMLDocNodeView *)
		                         g_malloc0 (sizeof (XMLDocNodeView));
	} else {
		if (DOC_NODE_VIEW (a_this)->vbox != NULL) {
			gtk_widget_destroy
			(GTK_WIDGET
			 (DOC_NODE_VIEW (a_this)->
			  vbox));
		}
	}

	view = DOC_NODE_VIEW (a_this);

	gfile = gnome_program_locate_file
	        (NULL,
	         GNOME_FILE_DOMAIN_APP_DATADIR,
	         PACKAGE "/mlview-node-editor.glade",
	         TRUE,
	         NULL);

	if (!gfile)
		return;

	gxml = glade_xml_new (gfile, "DocNodeBox", NULL);

	g_free (gfile);
	gfile = NULL;

	if (!gxml)
		return;

	view->vbox = GTK_VBOX (glade_xml_get_widget (gxml, "DocNodeBox"));
	gtk_widget_show (GTK_WIDGET (view->vbox));

	/* Document URI */
	view->name = GTK_ENTRY (glade_xml_get_widget (gxml, "UriEntry"));
	gtk_widget_show (GTK_WIDGET (view->name));

	view->name_changed_handler_id = g_signal_connect (
	                                    G_OBJECT (view->name),
	                                    "focus-out-event",
	                                    G_CALLBACK (mlview_node_editor_name_changed_cb),
	                                    a_this);

	/* Standalone */
	view->standalone = GTK_CHECK_BUTTON (glade_xml_get_widget (gxml, "StandaloneCheckbutton"));
	gtk_widget_show (GTK_WIDGET (view->standalone));

	/* Check button if needed */
	if (mlview_xml_document_is_standalone (PRIVATE (a_this)->curr_xml_document))
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (view->standalone), TRUE);

	g_signal_connect (G_OBJECT (view->standalone),
	                  "clicked",
	                  G_CALLBACK (standalone_checkbtn_clicked_cb),
	                  a_this);

	/* XML Version */
	view->xml_version = GTK_ENTRY (glade_xml_get_widget (gxml, "VersionEntry"));
	gtk_widget_show (GTK_WIDGET (view->xml_version));

	/* XML Encoding */
	view->external_encoding = GTK_COMBO_BOX (glade_xml_get_widget (gxml, "EncodingCombo"));
	gtk_widget_show (GTK_WIDGET (view->external_encoding));
	THROW_IF_FAIL (view->external_encoding);


	g_signal_connect (G_OBJECT (view->external_encoding),
	                  "changed",
	                  G_CALLBACK (external_encoding_changed_cb),
	                  a_this);

	/* data */
	available_encodings = mlview_utils_get_available_encodings ();
	THROW_IF_FAIL (available_encodings);
	/*gtk_combo_set_popdown_strings (view->external_encoding, available_encodings);*/

	p = available_encodings;
	while (p != NULL) {
		gtk_combo_box_insert_text (
		    GTK_COMBO_BOX (view->external_encoding),
		    ++curencoding,
		    (const gchar*)p->data);

		p = g_list_next (p);
	}

	gtk_combo_box_set_active (GTK_COMBO_BOX (view->external_encoding),
	                          0);


	/* External ID */
	view->ext_subset_external_id = GTK_ENTRY (glade_xml_get_widget (gxml, "ExtIdEntry"));
	gtk_widget_show (GTK_WIDGET (view->ext_subset_external_id));

	/* System ID */
	view->ext_subset_system_id.store = gtk_list_store_new (1, G_TYPE_STRING);
	THROW_IF_FAIL (view->ext_subset_system_id.store);
	view->ext_subset_system_id.combo = GTK_COMBO_BOX (glade_xml_get_widget (gxml, "SysIdCombo"));
	gtk_combo_box_set_model (
	    GTK_COMBO_BOX (view->ext_subset_system_id.combo),
	    GTK_TREE_MODEL (view->ext_subset_system_id.store));
	THROW_IF_FAIL (view->ext_subset_system_id.combo);
	g_object_unref (G_OBJECT (view->ext_subset_system_id.store));
	g_signal_connect (
	    G_OBJECT (view->ext_subset_system_id.combo),
	    "changed",
	    G_CALLBACK (ext_subset_system_id_combo_changed_cb),
	    a_this);
	renderer = gtk_cell_renderer_text_new ();
	THROW_IF_FAIL (renderer);
	gtk_cell_layout_pack_start (
	    GTK_CELL_LAYOUT (view->ext_subset_system_id.combo),
	    renderer,
	    TRUE);
	gtk_cell_layout_set_attributes (
	    GTK_CELL_LAYOUT (view->ext_subset_system_id.combo),
	    renderer,
	    "text", 0, NULL);
	view->ext_subset_system_id.references = g_hash_table_new_full (g_str_hash, g_str_equal, NULL,
	                                        (GDestroyNotify) gtk_tree_row_reference_free);

	gtk_widget_show (GTK_WIDGET (view->ext_subset_system_id.combo));



	/* Pack and show all */
	/*Show everything and attach it to the views */
	/*gtk_widget_show_all (GTK_WIDGET (view->vbox));*/

	gtk_notebook_append_page (private_data->node_view,
	                          GTK_WIDGET (view->vbox), NULL);
}


/**
 *Visually clears the xml element node view. 
 *
 */
static void
mlview_node_editor_clear_xml_element_node_view (MlViewNodeEditor * a_this)
{
	XMLElementNodeView *view = NULL;

	THROW_IF_FAIL (a_this != NULL);
	view = ELEMENT_NODE_VIEW (a_this);
	THROW_IF_FAIL (view != NULL);

	g_signal_handlers_block_by_func
	(G_OBJECT (view->name),
	 (void *) (mlview_node_editor_name_changed_cb),
	 a_this) ;
	gtk_entry_set_text (view->name, "");
	mlview_attrs_editor_clear (view->attrs_editor) ;

	g_signal_handlers_unblock_by_func
	(G_OBJECT (view->name),
	 (void *) (mlview_node_editor_name_changed_cb),
	 a_this) ;
}


/**
 *Clears visually (only) the text node view of the MlViewNodeEditor widget. 
 */
static void
mlview_node_editor_clear_xml_text_node_view (MlViewNodeEditor * a_this)
{
	XMLTextNodeView *view = NULL ;
	GtkTextBuffer *text_buffer = NULL ;
	GtkTextIter iter1 = {0}, iter2 = {0} ;

	THROW_IF_FAIL (a_this != NULL);

	view = TEXT_NODE_VIEW (a_this);
	THROW_IF_FAIL (view != NULL);

	/*
	 *FIXME: remember to block the
	 *"text-changed" signal (if used)
	 *before clearing the text node view.
	 */
	g_signal_handlers_block_by_func
	(G_OBJECT (text_buffer),
	 (void *) (mlview_node_editor_content_changed_cb),
	 a_this) ;
	text_buffer = gtk_text_view_get_buffer (view->widget) ;
	gtk_text_buffer_get_iter_at_offset (text_buffer,
	                                    &iter1, 0) ;
	gtk_text_buffer_get_iter_at_offset (text_buffer,
	                                    &iter2, -1) ;
	gtk_text_buffer_delete (text_buffer, &iter1, &iter2) ;
	g_signal_handlers_unblock_by_func
	(G_OBJECT (text_buffer),
	 (void *) (mlview_node_editor_content_changed_cb),
	 a_this) ;
}


/**
 *Clears visually (only) the comment node view of MlViewNodeEditor. 
 *
 */
static void
mlview_node_editor_clear_xml_comment_node_view (MlViewNodeEditor* a_this)
{
	XMLCommentNodeView *view = NULL;
	GtkTextBuffer *text_buffer = NULL ;
	GtkTextIter iter1 = {0}, iter2 = {0} ;

	THROW_IF_FAIL (a_this != NULL);

	view = COMMENT_NODE_VIEW (a_this);

	THROW_IF_FAIL (view != NULL);

	text_buffer = gtk_text_view_get_buffer (view->widget) ;
	g_signal_handlers_block_by_func
	(G_OBJECT (text_buffer),
	 (void *) (mlview_node_editor_content_changed_cb),
	 a_this) ;
	gtk_text_buffer_get_iter_at_offset (text_buffer,
	                                    &iter1, 0) ;
	gtk_text_buffer_get_iter_at_offset (text_buffer,
	                                    &iter2, -1) ;
	gtk_text_buffer_delete (text_buffer, &iter1, &iter2) ;
	g_signal_handlers_unblock_by_func
	(G_OBJECT (text_buffer),
	 (void *) (mlview_node_editor_content_changed_cb),
	 a_this) ;
}

static void
mlview_node_editor_clear_xml_cdata_section_node_view (MlViewNodeEditor * a_this)
{
	XMLCDataSectionNodeView *view = NULL;
	GtkTextBuffer *text_buffer = NULL ;
	GtkTextIter iter1 = {0}, iter2 = {0} ;

	THROW_IF_FAIL (a_this != NULL);
	THROW_IF_FAIL (PRIVATE (a_this) != NULL);

	view = CDATA_SECTION_NODE_VIEW (a_this);

	THROW_IF_FAIL (view != NULL);

	text_buffer = gtk_text_view_get_buffer (view->widget) ;
	g_signal_handlers_block_by_func
	(G_OBJECT (text_buffer),
	 (void *) (mlview_node_editor_content_changed_cb),
	 a_this) ;
	gtk_text_buffer_get_iter_at_offset (text_buffer,
	                                    &iter1, 0) ;
	gtk_text_buffer_get_iter_at_offset (text_buffer,
	                                    &iter2, 0) ;
	gtk_text_buffer_delete (text_buffer, &iter1, &iter2) ;
	g_signal_handlers_unblock_by_func
	(G_OBJECT (text_buffer),
	 (void *) (mlview_node_editor_content_changed_cb),
	 a_this) ;
}

/**
 *Visually clears the current pi node view 
 *included in the current instance of MlViewNodeEditor. 
 *
 */
static void
mlview_node_editor_clear_xml_pi_node_view (MlViewNodeEditor *a_this)
{
	XMLPINodeView *view = NULL;
	GtkTextBuffer *text_buffer = NULL ;
	GtkTextIter iter1 = {0}, iter2 = {0} ;

	THROW_IF_FAIL (a_this != NULL);
	THROW_IF_FAIL (PRIVATE (a_this) != NULL);

	view = PI_NODE_VIEW (a_this);

	THROW_IF_FAIL (view != NULL);

	/*clear the pi node content editor */
	text_buffer = gtk_text_view_get_buffer (view->widget) ;
	g_signal_handlers_block_by_func
	(G_OBJECT (text_buffer),
	 (void *) (mlview_node_editor_content_changed_cb),
	 a_this) ;
	gtk_text_buffer_get_iter_at_offset (text_buffer,
	                                    &iter1, 0) ;
	gtk_text_buffer_get_iter_at_offset (text_buffer,
	                                    &iter2, -1) ;
	gtk_text_buffer_delete (text_buffer, &iter1, &iter2) ;

	/*clear the name */
	gtk_editable_delete_text (GTK_EDITABLE (view->name), 0,
	                          -1);
	g_signal_handlers_unblock_by_func
	(G_OBJECT (text_buffer),
	 (void *) (mlview_node_editor_content_changed_cb),
	 a_this) ;
}


/**
 *Edits the xml node 
 *(which is supposed to be an xml element node) 
 *using the xml element view contained in 
 *the current instance of MlViewNodeEditor.
 *Note that if the xml node is not 
 *an element node, this function does nothing.
 *@param a_editor the "this" pointer of the current
 *instance of #MlViewNodeEditor.
 *@a_xml_doc the instance of #MlViewXMLDocument currently
 *being edited.
 *@a_node the node of a_xml_doc to edit.
 */
static void
mlview_node_editor_xml_element_node_view_edit_xml_node (MlViewNodeEditor * a_editor,
        MlViewXMLDocument * a_xml_doc,
        xmlNode * a_node)
{
	MlViewNodeEditorPrivate *editor_private = NULL;
	XMLElementNodeView *editor_view = NULL;
	gchar *full_name = NULL;
	enum MlViewStatus status = MLVIEW_OK;

	THROW_IF_FAIL (a_editor != NULL);
	THROW_IF_FAIL (MLVIEW_IS_NODE_EDITOR (a_editor));
	THROW_IF_FAIL (a_xml_doc != NULL);
	THROW_IF_FAIL (MLVIEW_IS_XML_DOCUMENT (a_xml_doc));
	THROW_IF_FAIL (a_node != NULL);
	THROW_IF_FAIL (PRIVATE (a_editor) != NULL);

	editor_private = PRIVATE (a_editor);
	editor_private->curr_xml_node = a_node;
	editor_private->curr_xml_document = a_xml_doc;
	if (a_node->type != XML_ELEMENT_NODE)
		return;
	editor_view = ELEMENT_NODE_VIEW (a_editor);
	THROW_IF_FAIL (editor_view != NULL);
	status = mlview_xml_document_node_get_fqn
	         (a_node, UTF8, &full_name);
	THROW_IF_FAIL (status == MLVIEW_OK);

	/*set the name of the element */
	g_signal_handler_block (G_OBJECT (editor_view->name),
	                        editor_view->name_changed_handler_id);
	g_signal_handlers_block_by_func (G_OBJECT (editor_view->name),
	                                 (void *) (text_inserted_in_element_name_cb),
	                                 a_editor) ;
	gtk_entry_set_text (editor_view->name, "");
	gtk_entry_set_text (GTK_ENTRY (editor_view->name),
	                    full_name);
	if (full_name != NULL) {
		g_free (full_name);
		full_name = NULL;
	}
	g_signal_handlers_unblock_by_func (G_OBJECT (editor_view->name),
	                                   (void *) (text_inserted_in_element_name_cb),
	                                   a_editor) ;
	g_signal_handler_unblock (G_OBJECT (editor_view->name),
	                          editor_view->name_changed_handler_id);

	/*edit the attributes */
	mlview_attrs_editor_clear (editor_view->attrs_editor) ;
	mlview_attrs_editor_edit_xml_attributes
	(editor_view->attrs_editor,
	 a_xml_doc, a_node) ;
	/*edit the namespaces */
	mlview_ns_editor_clear (editor_view->ns_editor) ;
	mlview_ns_editor_edit_node_visible_namespaces
	(editor_view->ns_editor, a_node) ;
	gtk_notebook_set_page (editor_private->node_view,
	                       ELEMENT_NODE_VIEW_PAGE);
	PRIVATE (a_editor)->cur_focusable_widget =
	    GTK_WIDGET (editor_view->name) ;
}


static void
mlview_node_editor_xml_text_node_view_edit_xml_node (MlViewNodeEditor * a_this,
        MlViewXMLDocument * a_xml_doc, xmlNode * a_node)
{
	MlViewNodeEditorPrivate *editor_private = NULL;
	XMLTextNodeView *editor_view = NULL;
	gchar *utf8_content = NULL ;
	GtkTextBuffer *text_buffer = NULL ;
	GtkTextIter iter1 = {0}, iter2 = {0} ;
	enum MlViewStatus status = MLVIEW_OK;
	gint len = 0;

	THROW_IF_FAIL (a_this
	                  && MLVIEW_IS_NODE_EDITOR (a_this)
	                  && a_xml_doc
	                  && MLVIEW_IS_XML_DOCUMENT (a_xml_doc)
	                  && a_node
	                  && PRIVATE (a_this));

	if (a_node->type != XML_TEXT_NODE)
		return;

	editor_view = TEXT_NODE_VIEW (a_this);
	THROW_IF_FAIL (editor_view != NULL);

	editor_private = PRIVATE (a_this);
	editor_private->curr_xml_node = a_node;
	editor_private->curr_xml_document = a_xml_doc;

	status = mlview_xml_document_node_get_content (a_node, UTF8,
	         &utf8_content);
	THROW_IF_FAIL (status == MLVIEW_OK);

	text_buffer = gtk_text_view_get_buffer (editor_view->widget) ;
	g_signal_handlers_block_by_func
	(G_OBJECT (text_buffer),
	 (void *) (mlview_node_editor_content_changed_cb),
	 a_this) ;
	g_signal_handlers_block_by_func (G_OBJECT (text_buffer),
	                                 (void *) (text_inserted_in_text_node_view_cb),
	                                 a_this) ;
	gtk_text_buffer_get_iter_at_offset (text_buffer,
	                                    &iter1, 0) ;
	gtk_text_buffer_get_iter_at_offset (text_buffer,
	                                    &iter2, -1) ;
	gtk_text_buffer_delete (text_buffer, &iter1, &iter2) ;

	if (utf8_content) {
		gtk_text_buffer_get_iter_at_offset
		(text_buffer, &iter1, 0) ;
		len = strlen (utf8_content);
		gtk_text_buffer_insert (text_buffer, &iter1,
		                        utf8_content, len) ;
	}
	gtk_notebook_set_page (editor_private->node_view,
	                       TEXT_NODE_VIEW_PAGE);

	g_signal_handlers_unblock_by_func (G_OBJECT (text_buffer),
	                                   (void *) (text_inserted_in_text_node_view_cb),
	                                   a_this) ;
	g_signal_handlers_unblock_by_func
	(text_buffer,
	 (void *) (mlview_node_editor_content_changed_cb),
	 a_this) ;
	PRIVATE (a_this)->cur_focusable_widget = GTK_WIDGET (editor_view->widget);

	/* release_resources:*/
	if (utf8_content) {
		g_free (utf8_content);
		utf8_content = NULL;
	}
}


/**
 *static void mlview_node_editor_xml_comment_node_view_edit_xml_node:
 *Edits an xml comment node using a specific xml comment node view. If the xml node given in argument
 *is not an xml comment node, does nothing. 
 *
 */
static void
mlview_node_editor_xml_comment_node_view_edit_xml_node (MlViewNodeEditor * a_this,
        MlViewXMLDocument * a_xml_doc,
        xmlNode * a_node)
{
	MlViewNodeEditorPrivate *editor_private = NULL;
	XMLCommentNodeView *editor_view = NULL;
	GtkTextBuffer *text_buffer = NULL ;
	GtkTextIter iter1 = {0}, iter2 = {0} ;
	gchar *utf8_content = NULL;
	enum MlViewStatus status = MLVIEW_OK;
	gint len = 0;

	THROW_IF_FAIL (a_this != NULL);
	THROW_IF_FAIL (MLVIEW_IS_NODE_EDITOR (a_this));
	THROW_IF_FAIL (a_xml_doc != NULL);
	THROW_IF_FAIL (MLVIEW_IS_XML_DOCUMENT (a_xml_doc));
	THROW_IF_FAIL (a_node != NULL);
	THROW_IF_FAIL (PRIVATE (a_this) != NULL);

	if (a_node->type != XML_COMMENT_NODE)
		return;

	editor_view = COMMENT_NODE_VIEW (a_this);
	THROW_IF_FAIL (editor_view != NULL);
	editor_private = PRIVATE (a_this);
	editor_private->curr_xml_node = a_node;
	editor_private->curr_xml_document = a_xml_doc;

	/*content = xmlNodeGetContent (a_node) ; */

	status = mlview_xml_document_node_get_content (a_node, UTF8,
	         &utf8_content);

	THROW_IF_FAIL (status == MLVIEW_OK);

	text_buffer = gtk_text_view_get_buffer (editor_view->widget) ;
	g_signal_handlers_block_by_func
	(G_OBJECT (text_buffer),
	 (void *) (mlview_node_editor_content_changed_cb),
	 a_this) ;
	g_signal_handlers_block_by_func (G_OBJECT (text_buffer),
	                                 (void *) (text_inserted_in_comment_node_view_cb),
	                                 a_this) ;

	gtk_text_buffer_get_iter_at_offset (text_buffer,
	                                    &iter1, 0) ;
	gtk_text_buffer_get_iter_at_offset (text_buffer,
	                                    &iter2, -1) ;
	gtk_text_buffer_delete (text_buffer, &iter1, &iter2) ;

	if (utf8_content) {
		len = strlen (utf8_content);
		gtk_text_buffer_get_iter_at_offset
		(text_buffer, &iter1, 0) ;
		gtk_text_buffer_insert (text_buffer, &iter1,
		                        utf8_content, len) ;
	}
	gtk_notebook_set_page (editor_private->node_view,
	                       COMMENT_NODE_VIEW_PAGE);
	g_signal_handlers_unblock_by_func (G_OBJECT (text_buffer),
	                                   (void *) (text_inserted_in_comment_node_view_cb),
	                                   a_this) ;
	g_signal_handlers_unblock_by_func
	(G_OBJECT (text_buffer),
	 (void *) (mlview_node_editor_content_changed_cb),
	 a_this) ;
	PRIVATE (a_this)->cur_focusable_widget = GTK_WIDGET (editor_view->widget) ;

	/* release_resources:*/

	if (utf8_content) {
		g_free (utf8_content);
		utf8_content = NULL;
	}
}

/**
 *Edits a xml cdata section node using the a specific xml cdata section view.
 *If the xml node given in argument is node a cdata section node, does nothing. 
 *
 */
static void
mlview_node_editor_xml_cdata_section_node_view_edit_xml_node (MlViewNodeEditor * a_this,
        MlViewXMLDocument * a_xml_doc,
        xmlNode * a_node)
{
	MlViewNodeEditorPrivate *editor_private = NULL;
	XMLCDataSectionNodeView *editor_view = NULL;
	GtkTextBuffer *text_buffer = NULL ;
	GtkTextIter iter1 = {0}, iter2 = {0} ;
	gchar *utf8_content = NULL;
	enum MlViewStatus status = MLVIEW_OK;
	gint len = 0;

	THROW_IF_FAIL (a_this
	                  && MLVIEW_IS_NODE_EDITOR (a_this)
	                  && a_xml_doc
	                  && MLVIEW_IS_XML_DOCUMENT (a_xml_doc)
	                  && a_node
	                  && PRIVATE (a_this));

	editor_view = CDATA_SECTION_NODE_VIEW (a_this);
	THROW_IF_FAIL (editor_view);

	editor_private = PRIVATE (a_this);
	editor_private->curr_xml_node = a_node;
	editor_private->curr_xml_document = a_xml_doc;

	/*content = xmlNodeGetContent (a_node) ; */
	status = mlview_xml_document_node_get_content (a_node, UTF8,
	         &utf8_content);
	THROW_IF_FAIL (status == MLVIEW_OK);

	text_buffer = gtk_text_view_get_buffer
	              (editor_view->widget) ;
	g_signal_handlers_block_by_func
	(G_OBJECT (text_buffer),
	 (void *) (mlview_node_editor_content_changed_cb),
	 a_this) ;
	g_signal_handlers_block_by_func (G_OBJECT (text_buffer),
	                                 (void *) (text_inserted_in_cdata_node_view_cb),
	                                 a_this) ;
	gtk_text_buffer_get_iter_at_offset (text_buffer,
	                                    &iter1, 0) ;
	gtk_text_buffer_get_iter_at_offset (text_buffer,
	                                    &iter2, -1) ;
	gtk_text_buffer_delete (text_buffer, &iter1, &iter2) ;
	if (utf8_content) {
		len = strlen (utf8_content);
		gtk_text_buffer_get_iter_at_offset (text_buffer,
		                                    &iter1, 0) ;
		gtk_text_buffer_insert (text_buffer, &iter1,
		                        utf8_content, len) ;
	}
	gtk_notebook_set_page (editor_private->node_view,
	                       CDATA_SECTION_VIEW_PAGE);
	g_signal_handlers_unblock_by_func (G_OBJECT (text_buffer),
	                                   (void *) (text_inserted_in_cdata_node_view_cb),
	                                   a_this) ;
	g_signal_handlers_unblock_by_func
	(G_OBJECT (text_buffer),
	 (void *) (mlview_node_editor_content_changed_cb),
	 a_this) ;

	PRIVATE (a_this)->cur_focusable_widget =
	    GTK_WIDGET (editor_view->widget) ;

	/* release_resources:*/
	if (utf8_content) {
		g_free (utf8_content);
		utf8_content = NULL;
	}
}


/**
 *Edits the xml pi node given in argument 
 *using a specific xml pi node view. If he xml node
 *given in argument is not a pi node, does nothing. 
 *
 */
static void
mlview_node_editor_xml_pi_node_view_edit_xml_node (MlViewNodeEditor * a_this,
        MlViewXMLDocument * a_xml_doc,
        xmlNode * a_node)
{
	MlViewNodeEditorPrivate *editor_private = NULL;
	XMLPINodeView *editor_view = NULL;
	GtkTextBuffer *text_buffer = NULL ;
	GtkTextIter iter1 = {0}, iter2 = {0} ;
	gchar *content = NULL, *full_name = NULL;
	enum MlViewStatus status = MLVIEW_OK;
	gint len = 0;

	THROW_IF_FAIL (a_this != NULL);
	THROW_IF_FAIL (MLVIEW_IS_NODE_EDITOR (a_this));
	THROW_IF_FAIL (a_xml_doc != NULL);
	THROW_IF_FAIL (MLVIEW_IS_XML_DOCUMENT (a_xml_doc));
	THROW_IF_FAIL (a_node != NULL);
	THROW_IF_FAIL (PRIVATE (a_this) != NULL);

	editor_view = PI_NODE_VIEW (a_this);
	THROW_IF_FAIL (editor_view != NULL);
	editor_private = PRIVATE (a_this);
	editor_private->curr_xml_node = a_node;
	editor_private->curr_xml_document = a_xml_doc;

	status = mlview_xml_document_node_get_name (a_node, UTF8,
	         &full_name);
	THROW_IF_FAIL (status == MLVIEW_OK);

	/*get the name of the element. */
	g_signal_handler_block (G_OBJECT (editor_view->name),
	                        editor_view->name_changed_handler_id);

	gtk_entry_set_text (editor_view->name, "");
	gtk_entry_set_text (GTK_ENTRY (editor_view->name),
	                    full_name);

	if (full_name)
		g_free (full_name);

	g_signal_handler_unblock (G_OBJECT (editor_view->name),
	                          editor_view->
	                          name_changed_handler_id);

	/*edit the content */
	/*content = xmlNodeGetContent (a_node) ; */

	status = mlview_xml_document_node_get_content (a_node, UTF8,
	         &content);
	THROW_IF_FAIL (status == MLVIEW_OK);

	text_buffer = gtk_text_view_get_buffer (editor_view->widget) ;
	g_signal_handlers_block_by_func
	(G_OBJECT (text_buffer),
	 (void *) (mlview_node_editor_content_changed_cb),
	 a_this) ;
	gtk_text_buffer_get_iter_at_offset (text_buffer,
	                                    &iter1, 0) ;
	gtk_text_buffer_get_iter_at_offset (text_buffer,
	                                    &iter2, -1) ;
	gtk_text_buffer_delete (text_buffer, &iter1, &iter2) ;

	if (content) {
		len = strlen (content);
		gtk_text_buffer_get_iter_at_offset (text_buffer,
		                                    &iter1, 0) ;
		gtk_text_buffer_insert (text_buffer, &iter1,
		                        content, len) ;
	}
	gtk_notebook_set_page (editor_private->node_view,
	                       PI_NODE_VIEW_PAGE) ;
	g_signal_handlers_unblock_by_func
	(G_OBJECT (text_buffer),
	 (void *) (mlview_node_editor_content_changed_cb),
	 a_this) ;
	PRIVATE (a_this)->cur_focusable_widget =
	    GTK_WIDGET (editor_view->name) ;

	if (content) {
		g_free (content);
		content = NULL;
	}
}

/**
 *DTD selector-related functions.
 */

static void
select_dtd_in_ext_subset_id_list (MlViewNodeEditor *a_this,
                                  MlViewXMLDocument *a_doc)
{
	xmlDocPtr doc = NULL;
	GtkTreeRowReference *ref = NULL;
	XMLDocNodeView *editor_view = NULL;
	GtkTreePath *path = NULL;
	GtkTreeIter iter = { 0 };
	gboolean res = FALSE;
	gchar *url = NULL;

	THROW_IF_FAIL (a_this && MLVIEW_IS_NODE_EDITOR (a_this));
	THROW_IF_FAIL (a_doc && MLVIEW_IS_XML_DOCUMENT (a_doc));

	editor_view = DOC_NODE_VIEW (a_this);

	THROW_IF_FAIL (editor_view);
	THROW_IF_FAIL (editor_view->ext_subset_system_id.store);
	THROW_IF_FAIL (editor_view->ext_subset_system_id.references);
	THROW_IF_FAIL (editor_view->ext_subset_system_id.combo);

	doc = mlview_xml_document_get_native_document (a_doc);

	THROW_IF_FAIL (doc);

	if (doc->extSubset) {
		THROW_IF_FAIL (doc->extSubset->SystemID);

		url = (gchar*) doc->extSubset->SystemID;
	} else
		url = _("None");

	THROW_IF_FAIL (url);

	ref = (GtkTreeRowReference *) g_hash_table_lookup
	      (editor_view->ext_subset_system_id.references,
	       url);

	if (!ref) {
		mlview_utils_trace_debug ("ref is NULL for url: (see below)") ;
		mlview_utils_trace_debug (url) ;
		return ;
	}

	path = gtk_tree_row_reference_get_path (ref);

	THROW_IF_FAIL (path);

	res = gtk_tree_model_get_iter
	      (GTK_TREE_MODEL (editor_view->ext_subset_system_id.store),
	       &iter, path);

	gtk_tree_path_free (path);
	path = NULL;

	if (!res)
		return;

	g_signal_handlers_block_by_func
	(G_OBJECT (editor_view->ext_subset_system_id.combo),
	 (void *) (ext_subset_system_id_combo_changed_cb),
	 a_this) ;
	gtk_combo_box_set_active_iter (editor_view->ext_subset_system_id.combo,
	                               &iter);
	g_signal_handlers_unblock_by_func
	(G_OBJECT (editor_view->ext_subset_system_id.combo),
	 (void *) (ext_subset_system_id_combo_changed_cb),
	 a_this) ;
}

static void
add_schema_to_ext_subset_id_list (MlViewSchema *a_schema,
                                  XMLDocNodeView *editor_view)
{
	gchar *url = NULL;
	GtkTreeIter iter = { 0 };
	GtkTreePath *path = NULL;
	GtkTreeRowReference *ref = NULL;
	enum MlViewSchemaType schema_type = SCHEMA_TYPE_UNDEF ;
	THROW_IF_FAIL (a_schema);
	THROW_IF_FAIL (editor_view);
	THROW_IF_FAIL (editor_view->ext_subset_system_id.store);
	THROW_IF_FAIL (editor_view->ext_subset_system_id.references);

	mlview_schema_get_type (a_schema, &schema_type) ;
	THROW_IF_FAIL (schema_type != SCHEMA_TYPE_UNDEF) ;

	if (schema_type == SCHEMA_TYPE_DTD) {
		url = mlview_schema_get_url (a_schema);

		THROW_IF_FAIL (url);

		gtk_list_store_append (editor_view->ext_subset_system_id.store,
		                       &iter);

		gtk_list_store_set (editor_view->ext_subset_system_id.store,
		                    &iter, 0, url, -1);

		path = gtk_tree_model_get_path
		       (GTK_TREE_MODEL (editor_view->ext_subset_system_id.store),
		        &iter);

		if (!path) {
			gtk_list_store_remove
			(editor_view->ext_subset_system_id.store, &iter);

			return;
		}

		ref = gtk_tree_row_reference_new
		      (GTK_TREE_MODEL (editor_view->ext_subset_system_id.store),
		       path);

		if (ref)
			g_hash_table_insert
			(editor_view->ext_subset_system_id.references, url, ref);
		else
			gtk_list_store_remove
			(editor_view->ext_subset_system_id.store, &iter);

		gtk_tree_path_free (path);
		path = NULL;
	}
}

static void
build_ext_subset_id_list (MlViewNodeEditor *a_this,
                          MlViewXMLDocument *a_doc)
{
	MlViewSchemaList *list = NULL;
	XMLDocNodeView *editor_view = NULL;
	GtkTreeRowReference *ref = NULL;
	GtkTreePath *path = NULL;
	GtkTreeIter iter = { 0 };

	THROW_IF_FAIL (a_this && MLVIEW_IS_NODE_EDITOR (a_this));
	THROW_IF_FAIL (a_doc && MLVIEW_IS_XML_DOCUMENT (a_doc));

	list = mlview_xml_document_get_schema_list (a_doc);

	THROW_IF_FAIL (list && MLVIEW_IS_SCHEMA_LIST (list));

	editor_view = DOC_NODE_VIEW (a_this);

	THROW_IF_FAIL (editor_view);
	THROW_IF_FAIL (editor_view->ext_subset_system_id.store);
	THROW_IF_FAIL (editor_view->ext_subset_system_id.references);

	g_hash_table_foreach_remove (editor_view->ext_subset_system_id.references,
	                             (GHRFunc) gtk_true, NULL);

	g_signal_handlers_block_by_func (G_OBJECT (a_doc),
	                                 (void *) (ext_subset_changed_cb),
	                                 a_this) ;
	g_signal_handlers_block_by_func
	(G_OBJECT (editor_view->ext_subset_system_id.combo),
	 (void *) (ext_subset_system_id_combo_changed_cb),
	 a_this) ;
	gtk_list_store_clear (editor_view->ext_subset_system_id.store);

	gtk_list_store_append (editor_view->ext_subset_system_id.store, &iter);

	gtk_list_store_set (editor_view->ext_subset_system_id.store, &iter,
	                    0, _("None"), -1);

	path = gtk_tree_model_get_path
	       (GTK_TREE_MODEL (editor_view->ext_subset_system_id.store), &iter);

	if (!path) {
		gtk_list_store_remove
		(editor_view->ext_subset_system_id.store, &iter);

		return;
	}

	ref = gtk_tree_row_reference_new
	      (GTK_TREE_MODEL (editor_view->ext_subset_system_id.store), path);

	gtk_tree_path_free (path);
	path = NULL;

	if (!ref) {
		gtk_list_store_remove
		(editor_view->ext_subset_system_id.store, &iter);

		return;
	}
	g_hash_table_insert (editor_view->ext_subset_system_id.references,
	                     _("None"),
	                     ref);
	mlview_schema_list_foreach
	(list,
	 (MlViewSchemaListFunc) add_schema_to_ext_subset_id_list,
	 editor_view);

	g_signal_handlers_unblock_by_func
	(G_OBJECT (editor_view->ext_subset_system_id.combo),
	 (void *) (ext_subset_system_id_combo_changed_cb),
	 a_this) ;
	g_signal_handlers_unblock_by_func (G_OBJECT (a_doc),
	                                   (void *) (ext_subset_changed_cb),
	                                   a_this) ;
}

static void
ext_subset_changed_cb (MlViewXMLDocument *a_doc, MlViewNodeEditor *a_editor)
{
	xmlDocPtr doc = NULL;
	XMLDocNodeView *editor_view = NULL;

	THROW_IF_FAIL (a_doc && MLVIEW_IS_XML_DOCUMENT (a_doc));
	THROW_IF_FAIL (a_editor && MLVIEW_IS_NODE_EDITOR (a_editor));

	select_dtd_in_ext_subset_id_list (a_editor, a_doc);

	doc = mlview_xml_document_get_native_document (a_doc);

	THROW_IF_FAIL (doc);

	editor_view = DOC_NODE_VIEW (a_editor);

	THROW_IF_FAIL (editor_view);
	THROW_IF_FAIL (editor_view->ext_subset_external_id);

	if (doc->extSubset && doc->extSubset->ExternalID)
		gtk_entry_set_text (editor_view->ext_subset_external_id,
		                    (const gchar *) doc->extSubset->ExternalID);
	else
		gtk_entry_set_text (editor_view->ext_subset_external_id,
		                    "");
}

static void
standalone_checkbtn_clicked_cb (GtkCheckButton *a_btn, MlViewNodeEditor *a_this)
{
	MlViewXMLDocument *doc = PRIVATE (a_this)->curr_xml_document;
	gboolean active = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (a_btn));

	mlview_xml_document_set_standalone (doc, active);
}

static void
schema_associated_cb (MlViewSchemaList *a_list, MlViewSchema *a_schema,
                      MlViewNodeEditor *a_editor)
{
	XMLDocNodeView *editor_view = NULL;
	MlViewXMLDocument *doc = NULL;

	THROW_IF_FAIL (a_schema);
	THROW_IF_FAIL (a_editor && MLVIEW_IS_NODE_EDITOR (a_editor));

	editor_view = DOC_NODE_VIEW (a_editor);

	add_schema_to_ext_subset_id_list (a_schema, editor_view);

	THROW_IF_FAIL (PRIVATE (a_editor));

	doc = PRIVATE (a_editor)->curr_xml_document;

	THROW_IF_FAIL (doc);

	select_dtd_in_ext_subset_id_list (a_editor, doc);
}

static void
schema_unassociated_cb (MlViewSchemaList *a_list, MlViewSchema *a_schema,
                        MlViewNodeEditor *a_editor)
{
	XMLDocNodeView *editor_view = NULL;
	GtkTreeRowReference *ref = NULL;
	GtkTreePath *path = NULL;
	GtkTreeIter iter = { 0 };
	gchar *url = NULL;
	gboolean res = FALSE;
	MlViewXMLDocument *doc = NULL;

	THROW_IF_FAIL (a_schema);
	THROW_IF_FAIL (a_editor && MLVIEW_IS_NODE_EDITOR (a_editor));

	editor_view = DOC_NODE_VIEW (a_editor);

	THROW_IF_FAIL (editor_view);
	THROW_IF_FAIL (editor_view->ext_subset_system_id.references);
	THROW_IF_FAIL (editor_view->ext_subset_system_id.store);

	url = mlview_schema_get_url (a_schema);

	THROW_IF_FAIL (url);

	ref = (GtkTreeRowReference *) g_hash_table_lookup
	      (editor_view->ext_subset_system_id.references, url);

	THROW_IF_FAIL (ref);

	path = gtk_tree_row_reference_get_path (ref);

	THROW_IF_FAIL (path);

	res = gtk_tree_model_get_iter
	      (GTK_TREE_MODEL (editor_view->ext_subset_system_id.store),
	       &iter, path);

	gtk_tree_path_free (path);
	path = NULL;

	THROW_IF_FAIL (res);

	res = gtk_list_store_remove
	      (editor_view->ext_subset_system_id.store, &iter);

	g_hash_table_remove
	(editor_view->ext_subset_system_id.references, url);

	url = NULL;
	ref = NULL;

	THROW_IF_FAIL (PRIVATE (a_editor));

	doc = PRIVATE (a_editor)->curr_xml_document;

	THROW_IF_FAIL (doc);

	select_dtd_in_ext_subset_id_list (a_editor, doc);
}

static void
ext_subset_system_id_combo_changed_cb (GtkComboBox *a_combo,
                                       MlViewNodeEditor *a_editor)
{
	MlViewXMLDocument *doc = NULL;
	mlview::AppContext *ctxt = NULL;
	XMLDocNodeView *editor_view = NULL;
	GtkTreeIter iter = { 0 };
	gboolean res = FALSE;
	gchar *url = NULL;

	THROW_IF_FAIL (a_combo && GTK_IS_COMBO_BOX (a_combo));
	THROW_IF_FAIL (a_editor && MLVIEW_IS_NODE_EDITOR (a_editor));
	THROW_IF_FAIL (PRIVATE (a_editor));

	doc = PRIVATE (a_editor)->curr_xml_document;

	THROW_IF_FAIL (doc);

	ctxt = mlview::AppContext::get_instance () ;
	THROW_IF_FAIL (ctxt);

	editor_view = DOC_NODE_VIEW (a_editor);

	THROW_IF_FAIL (editor_view);
	THROW_IF_FAIL (editor_view->ext_subset_system_id.store);

	res = gtk_combo_box_get_active_iter (a_combo, &iter);
	if (!res)
		return;

	gtk_tree_model_get
	(GTK_TREE_MODEL (editor_view->ext_subset_system_id.store),
	 &iter, 0, &url, -1);

	THROW_IF_FAIL (url);

	if (!strcmp (url, _("None")))
		mlview_xml_document_set_ext_subset_with_url (doc, NULL);
	else
		mlview_xml_document_set_ext_subset_with_url (doc, url);
}

/**
 *Edits the xml document given in argument using a specific view. If the xml node given in argument is not
 *a xml document node, does nothing. 
 *
 */
static void
mlview_node_editor_xml_doc_node_view_edit_xml_node (MlViewNodeEditor * a_this,
        MlViewXMLDocument * a_xml_doc,
        xmlNode * a_node)
{
	MlViewNodeEditorPrivate *editor_private = NULL;
	XMLDocNodeView *editor_view = NULL;
	gchar *name = NULL;
	enum MlViewStatus status = MLVIEW_OK;

	THROW_IF_FAIL (a_this != NULL);
	THROW_IF_FAIL (MLVIEW_IS_NODE_EDITOR (a_this));
	THROW_IF_FAIL (a_xml_doc != NULL);
	THROW_IF_FAIL (MLVIEW_IS_XML_DOCUMENT (a_xml_doc));
	THROW_IF_FAIL (a_node != NULL);
	THROW_IF_FAIL (PRIVATE (a_this) != NULL);

	if (a_node->type != XML_DOCUMENT_NODE)
		return;

	editor_view = DOC_NODE_VIEW (a_this);
	THROW_IF_FAIL (editor_view != NULL);
	editor_private = PRIVATE (a_this);
	editor_private->curr_xml_node = a_node;
	editor_private->curr_xml_document = a_xml_doc;

	/*get the name of the element. */
	status = mlview_xml_document_node_get_name (a_node,
	         UTF8,
	         &name);
	THROW_IF_FAIL (status == MLVIEW_OK);

	g_signal_handler_block (G_OBJECT (editor_view->name),
	                        editor_view->
	                        name_changed_handler_id);
	gtk_entry_set_text (editor_view->name, "");

	if (name != NULL)
		gtk_entry_set_text (GTK_ENTRY
		                    (editor_view->name), name);
	else
		gtk_entry_set_text (GTK_ENTRY
		                    (editor_view->name), "");

	if (name) {
		g_free (name);
		name = NULL;
	}

	g_signal_handler_unblock (G_OBJECT (editor_view->name),
	                          editor_view->
	                          name_changed_handler_id);

	/*get the xml version */
	if (((xmlDocPtr) a_node)->version)
		gtk_entry_set_text (editor_view->xml_version,
		                    (const gchar *) ((xmlDocPtr) a_node)->version);
	else
		gtk_entry_set_text (editor_view->xml_version,
		                    "1.0");

	/*get the external encoding */


	if (((xmlDocPtr) a_node)->encoding) {
		enum MlViewStatus status = MLVIEW_OK;

		/*
		 *If the encoding is supported
		 *by mlview and if it is not
		 *in the default list of supported encodings
		 *add it to that list.
		 */
		status = mlview_utils_add_supported_encoding
		         ((gchar *) ((xmlDocPtr) a_node)->
		          encoding);

		if (status ==
		        MLVIEW_ENCODING_NOT_SUPPORTED_ERROR) {

			/*
			 *If the document has no encoding specified,
			 *set the default document encoding to UTF-8.
			 */
			((xmlDocPtr) a_node)->encoding = (const xmlChar *)
			                                 xmlMemStrdup ("UTF-8");

		}

	} else {

		((xmlDocPtr) a_node)->encoding = (const xmlChar *)
		                                 xmlMemStrdup ("UTF-8");
	}

	THROW_IF_FAIL (editor_view->external_encoding);
	/*
	  && editor_view->external_encoding->
	  entry);
	  
	  gtk_signal_handler_block_by_data
	  (GTK_OBJECT
	  (editor_view->external_encoding->entry),
	  a_this);
	 
	  gtk_entry_set_text (GTK_ENTRY
	  (editor_view->external_encoding->
	  entry),
	  ((xmlDocPtr) a_node)->encoding);
	  
	  gtk_signal_handler_unblock_by_data
	  (GTK_OBJECT
	                 (editor_view->external_encoding->entry),
	                 a_this);
	*/
	/*get stuffs about the external subset and standalone*/
	gtk_notebook_set_page (editor_private->node_view,
	                       DOC_NODE_VIEW_PAGE);

	PRIVATE (a_this)->cur_focusable_widget = GTK_WIDGET (editor_view->name);

	build_ext_subset_id_list (a_this, a_xml_doc);

	ext_subset_changed_cb (a_xml_doc, a_this);
}

static gboolean
mlview_node_editor_has_an_editing_transaction_started (MlViewNodeEditor *a_this)
{
	g_return_val_if_fail (a_this && MLVIEW_IS_NODE_EDITOR (a_this),
	                      FALSE) ;

	if (PRIVATE (a_this)->element_node_view->started_editing_transaction == TRUE
	        || PRIVATE (a_this)->text_node_view->started_editing_transaction == TRUE
	        || PRIVATE (a_this)->comment_node_view->started_editing_transaction == TRUE
	        || PRIVATE (a_this)->cdata_section_node_view->started_editing_transaction == TRUE) {
		return TRUE ;
	}
	return FALSE ;
}

static void
mlview_node_editor_xml_text_node_view_commit_edit_trans (MlViewNodeEditor *a_this)
{
	GtkTextBuffer * text_buffer = NULL ;
	GtkTextIter iter1 = {0}, iter2 = {0} ;
	gchar *content = NULL;
	xmlNode *node = NULL ;
	gchar *node_path = NULL ;

	THROW_IF_FAIL (a_this
	                  && MLVIEW_IS_NODE_EDITOR (a_this)
	                  && PRIVATE (a_this)->curr_xml_node
	                  && PRIVATE (a_this)->text_node_view
	                  && PRIVATE (a_this)->text_node_view->transaction_node) ;

	if (PRIVATE (a_this)->text_node_view
	        && PRIVATE (a_this)->text_node_view->started_editing_transaction == FALSE)
		return ;
	node = PRIVATE (a_this)->text_node_view->transaction_node ;
	PRIVATE (a_this)->text_node_view->transaction_node = NULL ;
	text_buffer = gtk_text_view_get_buffer (PRIVATE (a_this)->text_node_view->widget) ;
	THROW_IF_FAIL (text_buffer) ;
	gtk_text_buffer_get_iter_at_offset
	(text_buffer, &iter1, 0) ;
	gtk_text_buffer_get_iter_at_offset
	(text_buffer, &iter2, -1) ;
	content =
	    gtk_text_buffer_get_text (text_buffer, &iter1,
	                              &iter2, FALSE) ;
	mlview_xml_document_get_node_path (PRIVATE (a_this)->curr_xml_document,
	                                   node,
	                                   &node_path) ;

	g_signal_handlers_block_by_func (G_OBJECT (text_buffer),
	                                 (void *) (text_inserted_in_text_node_view_cb),
	                                 a_this) ;
	g_signal_handlers_block_by_func (G_OBJECT (text_buffer),
	                                 (void *) (text_range_deleted_in_text_node_view_cb),
	                                 a_this) ;
	mlview_xml_document_set_node_content
	(PRIVATE (a_this)->curr_xml_document,
	 node_path, content, TRUE);
	if (node_path) {
		g_free (node_path) ;
		node_path = NULL ;
	}

	g_signal_emit (G_OBJECT (a_this),
	               gv_mlview_node_editor_signals
	               [ELEMENT_CONTENT_CHANGED], 0, content);

	g_signal_emit (G_OBJECT (a_this),
	               gv_mlview_node_editor_signals
	               [ELEMENT_CHANGED], 0, content);
	g_free (content) ;

	PRIVATE (a_this)->text_node_view->started_editing_transaction = FALSE ;

	g_signal_handlers_unblock_by_func (G_OBJECT (text_buffer),
	                                   (void *) (text_inserted_in_text_node_view_cb),
	                                   a_this) ;
	g_signal_handlers_unblock_by_func (G_OBJECT (text_buffer),
	                                   (void *) (text_range_deleted_in_text_node_view_cb),
	                                   a_this) ;
}

static void
mlview_node_editor_xml_element_node_view_commit_edit_trans (MlViewNodeEditor *a_this)
{
	gchar *full_name = NULL,
	                   *local_name = NULL;
	xmlNs *ns = NULL;
	xmlNode *node = NULL ;
	GtkEntry *entry = NULL ;
	gchar *node_path = NULL ;

	THROW_IF_FAIL (a_this && MLVIEW_NODE_EDITOR (a_this)
	                  && PRIVATE (a_this)
	                  && PRIVATE (a_this)->curr_xml_document) ;

	THROW_IF_FAIL (PRIVATE (a_this)->element_node_view) ;
	entry = PRIVATE (a_this)->element_node_view->name ;
	if (PRIVATE (a_this)->element_node_view->started_editing_transaction == FALSE)
		return ;
	THROW_IF_FAIL (PRIVATE (a_this)->element_node_view->transaction_node) ;
	THROW_IF_FAIL (PRIVATE (a_this)->element_node_view->transaction_node->type == XML_ELEMENT_NODE
	                  || PRIVATE (a_this)->element_node_view->transaction_node->type == XML_PI_NODE) ;

	node = PRIVATE (a_this)->element_node_view->transaction_node ;

	full_name = (gchar *)
	            gtk_entry_get_text (GTK_ENTRY (entry));
	if (full_name) {
		mlview_utils_parse_full_name
		(node, full_name, &ns,
		 &local_name);
	}
	if (ns != NULL) {
		xmlSetNs (node, ns);
	} else {
		node->ns = NULL;
	}
	PRIVATE (a_this)->element_node_view->started_editing_transaction = FALSE ;
	PRIVATE (a_this)->element_node_view->transaction_node = NULL ;

	mlview_xml_document_get_node_path (PRIVATE (a_this)->curr_xml_document,
	                                   node,
	                                   &node_path) ;
	if (!node_path) {
		mlview_utils_trace_debug ("could not get node path") ;
		goto cleanup ;
	}

	mlview_xml_document_set_node_name
	(PRIVATE (a_this)->curr_xml_document,
	 node_path, local_name, TRUE);
	gtk_signal_emit (GTK_OBJECT (a_this),
	                 gv_mlview_node_editor_signals
	                 [ELEMENT_CHANGED]);
cleanup:
	if (local_name) {
		g_free (local_name);
		local_name = NULL;
	}
	if (node_path) {
		g_free (node_path) ;
		node_path = NULL ;
	}

}

static void
mlview_node_editor_xml_comment_node_view_commit_edit_trans (MlViewNodeEditor *a_this)
{
	GtkTextBuffer * text_buffer = NULL ;
	GtkTextIter iter1 = {0}, iter2 = {0} ;
	gchar *content = NULL;
	xmlNode *node = NULL ;
	gchar *node_path = NULL ;

	THROW_IF_FAIL (a_this
	                  && MLVIEW_IS_NODE_EDITOR (a_this)
	                  && PRIVATE (a_this)->curr_xml_node
	                  && PRIVATE (a_this)->comment_node_view
	                  && PRIVATE (a_this)->comment_node_view->transaction_node) ;

	THROW_IF_FAIL (a_this
	                  && MLVIEW_IS_NODE_EDITOR (a_this)
	                  && PRIVATE (a_this)->curr_xml_node
	                  && PRIVATE (a_this)->comment_node_view
	                  && PRIVATE (a_this)->comment_node_view->transaction_node) ;

	if (PRIVATE (a_this)->comment_node_view
	        && PRIVATE (a_this)->comment_node_view->started_editing_transaction == FALSE)
		return ;
	PRIVATE (a_this)->comment_node_view->started_editing_transaction = FALSE ;
	node = PRIVATE (a_this)->comment_node_view->transaction_node ;
	PRIVATE (a_this)->comment_node_view->transaction_node = NULL ;
	text_buffer = gtk_text_view_get_buffer (PRIVATE (a_this)->comment_node_view->widget) ;
	THROW_IF_FAIL (text_buffer) ;
	gtk_text_buffer_get_iter_at_offset
	(text_buffer, &iter1, 0) ;
	gtk_text_buffer_get_iter_at_offset
	(text_buffer, &iter2, -1) ;
	content =
	    gtk_text_buffer_get_text (text_buffer, &iter1,
	                              &iter2, FALSE) ;
	mlview_xml_document_get_node_path (PRIVATE (a_this)->curr_xml_document,
	                                   node,
	                                   &node_path) ;
	if (!node_path) {
		mlview_utils_trace_debug ("Could not get node path") ;
		goto cleanup ;
	}
	mlview_xml_document_set_node_content
	(PRIVATE (a_this)->curr_xml_document,
	 node_path, content, TRUE);
	if (node_path) {
		g_free (node_path) ;
		node_path = NULL ;
	}
	g_signal_emit (G_OBJECT (a_this),
	               gv_mlview_node_editor_signals
	               [ELEMENT_CONTENT_CHANGED], 0, content);

	g_signal_emit (G_OBJECT (a_this),
	               gv_mlview_node_editor_signals
	               [ELEMENT_CHANGED], 0, content);
cleanup:
	if (content) {
		g_free (content) ;
		content = NULL ;
	}
}

static void
mlview_node_editor_xml_cdata_section_node_view_commit_edit_trans (MlViewNodeEditor *a_this)
{
	GtkTextBuffer * text_buffer = NULL ;
	GtkTextIter iter1 = {0}, iter2 = {0} ;
	gchar *content = NULL;
	xmlNode *node = NULL ;
	gchar *node_path = NULL ;

	THROW_IF_FAIL (a_this
	                  && MLVIEW_IS_NODE_EDITOR (a_this)
	                  && PRIVATE (a_this)->curr_xml_node
	                  && PRIVATE (a_this)->cdata_section_node_view
	                  && PRIVATE (a_this)->cdata_section_node_view->transaction_node) ;

	THROW_IF_FAIL (a_this
	                  && MLVIEW_IS_NODE_EDITOR (a_this)
	                  && PRIVATE (a_this)->curr_xml_node
	                  && PRIVATE (a_this)->cdata_section_node_view
	                  && PRIVATE (a_this)->cdata_section_node_view->transaction_node) ;

	if (PRIVATE (a_this)->cdata_section_node_view
	        && PRIVATE (a_this)->cdata_section_node_view->started_editing_transaction == FALSE)
		return ;
	PRIVATE (a_this)->cdata_section_node_view->started_editing_transaction = FALSE ;
	node = PRIVATE (a_this)->cdata_section_node_view->transaction_node ;
	PRIVATE (a_this)->cdata_section_node_view->transaction_node = NULL ;
	text_buffer = gtk_text_view_get_buffer (PRIVATE (a_this)->cdata_section_node_view->widget) ;
	THROW_IF_FAIL (text_buffer) ;
	gtk_text_buffer_get_iter_at_offset
	(text_buffer, &iter1, 0) ;
	gtk_text_buffer_get_iter_at_offset
	(text_buffer, &iter2, -1) ;
	content =
	    gtk_text_buffer_get_text (text_buffer, &iter1,
	                              &iter2, FALSE) ;

	mlview_xml_document_get_node_path (PRIVATE (a_this)->curr_xml_document,
	                                   node,
	                                   &node_path) ;
	if (!node_path) {
		mlview_utils_trace_debug ("Could not get path") ;
		goto cleanup ;
	}
	mlview_xml_document_set_node_content
	(PRIVATE (a_this)->curr_xml_document,
	 node_path, content, TRUE);
	if (node_path) {
		g_free (node_path) ;
		node_path = NULL ;
	}
	g_signal_emit (G_OBJECT (a_this),
	               gv_mlview_node_editor_signals
	               [ELEMENT_CONTENT_CHANGED], 0, content);

	g_signal_emit (G_OBJECT (a_this),
	               gv_mlview_node_editor_signals
	               [ELEMENT_CHANGED], 0, content);
cleanup:
	if (content) {
		g_free (content);
	}
}

static void
mlview_node_editor_commit_editing_transaction (MlViewNodeEditor *a_this)
{
	THROW_IF_FAIL (a_this && MLVIEW_IS_NODE_EDITOR (a_this)
	                  && PRIVATE (a_this)) ;
	if (PRIVATE (a_this)->text_node_view
	        && PRIVATE (a_this)->text_node_view->started_editing_transaction == TRUE) {
		mlview_node_editor_xml_text_node_view_commit_edit_trans
		(a_this) ;
	}
	if (PRIVATE (a_this)->element_node_view
	        && PRIVATE (a_this)->element_node_view->started_editing_transaction == TRUE) {
		mlview_node_editor_xml_element_node_view_commit_edit_trans (a_this) ;
	}
	if (PRIVATE (a_this)->comment_node_view
	        && PRIVATE (a_this)->comment_node_view->started_editing_transaction == TRUE) {
		mlview_node_editor_xml_comment_node_view_commit_edit_trans (a_this) ;
	}
	if (PRIVATE (a_this)->cdata_section_node_view
	        && PRIVATE (a_this)->cdata_section_node_view->started_editing_transaction == TRUE) {
		mlview_node_editor_xml_cdata_section_node_view_commit_edit_trans (a_this) ;
	}
}

/***********************************************
 *private signal handlers
 *FIXME: recode everything below to 
 *support the private field in MlViewNodeEditor 
 *and the different views ...
 ************************************************/


/**
 *Called back when the element name entry changes.
 *Applies the change to the attached xml node and emits the
 *signal "element_name_changed" followed by the signal
 *"element_changed" if
 *and only if the element is an xml element node. 
 *
 */
static void
mlview_node_editor_name_changed_cb (GtkWidget * a_entry,
                                    GdkEventFocus *a_event,
                                    MlViewNodeEditor * a_editor)
{

	/*TODO: handle the PI node case !!*/
	THROW_IF_FAIL (a_entry
	                  && GTK_IS_ENTRY (a_entry)
	                  && a_event
	                  && a_editor
	                  && MLVIEW_IS_NODE_EDITOR (a_editor)
	                  && PRIVATE (a_editor)
	                  && PRIVATE (a_editor)->curr_xml_node
	                  && ELEMENT_NODE_VIEW (a_editor));
	if (ELEMENT_NODE_VIEW (a_editor)->started_editing_transaction == TRUE) {
		mlview_node_editor_xml_element_node_view_commit_edit_trans (a_editor);
	}
}

static void
mlview_node_editor_attribute_changed_cb (MlViewAttrsEditor *a_attrs_editor,
        gpointer a_this)
{
	MlViewNodeEditor *node_editor = NULL;
	THROW_IF_FAIL (a_attrs_editor
	                  && MLVIEW_IS_ATTRS_EDITOR (a_attrs_editor));
	THROW_IF_FAIL (a_this);

	node_editor = MLVIEW_NODE_EDITOR (a_this);
	gtk_signal_emit (GTK_OBJECT (node_editor),
	                 gv_mlview_node_editor_signals
	                 [ELEMENT_CHANGED]);
}

static void
xml_doc_node_unselected_cb (MlViewXMLDocument *a_doc,
                            xmlNode *a_node,
                            gpointer a_user_data)
{
	/*
	        GtkTextIter iter1 = {0}, iter2 = {0} ;        
	        GtkTextBuffer *text_buffer = NULL ;
	        gchar *content = NULL ;
	*/
	MlViewNodeEditor *editor = NULL ;

	if (a_node->type != XML_TEXT_NODE)
		return ;
	THROW_IF_FAIL (a_doc && MLVIEW_IS_XML_DOCUMENT (a_doc)
	                  && a_node && a_user_data
	                  && MLVIEW_IS_NODE_EDITOR (a_user_data)) ;

	editor = MLVIEW_NODE_EDITOR (a_user_data) ;
	THROW_IF_FAIL (editor && PRIVATE (editor)
	                  && PRIVATE (editor)->text_node_view
	                  && PRIVATE (editor)->text_node_view->widget) ;

	if (PRIVATE (editor)->text_node_view->started_editing_transaction == TRUE) {
		mlview_node_editor_xml_text_node_view_commit_edit_trans (editor) ;
	}

	/*
	text_buffer = gtk_text_view_get_buffer 
	        (GTK_TEXT_VIEW 
	         (PRIVATE (editor)->text_node_view->widget)) ;
	THROW_IF_FAIL (text_buffer) ;
	gtk_text_buffer_get_iter_at_offset 
	        (text_buffer, &iter1, 0) ;
	gtk_text_buffer_get_iter_at_offset 
	        (text_buffer, &iter2, -1) ;
	content =
	        gtk_text_buffer_get_text (text_buffer, &iter1,
	                                  &iter2, FALSE) ;        
	mlview_xml_document_set_node_content
	        (a_doc,a_node,
	         content, UTF8, TRUE);
	if (content) {
	        g_free (content) ;
	        content = NULL ;
	}
	*/
}

/**
 *A callback called when the user changes the content of the element.
 *Emits a signal "element_content_changed" of the MlViewNodeEditor class. 
 *
 */
static gboolean
mlview_node_editor_content_changed_cb (GtkTextView * a_view,
                                       GdkEventFocus *a_event,
                                       MlViewNodeEditor *a_editor)
{
	g_return_val_if_fail (GTK_IS_TEXT_VIEW (a_view)
	                      && a_editor
	                      && MLVIEW_IS_NODE_EDITOR (a_editor)
	                      && PRIVATE (a_editor),
	                      FALSE);

	if (mlview_node_editor_has_an_editing_transaction_started (a_editor) == FALSE) {
		return FALSE ;
	}
	mlview_node_editor_commit_editing_transaction (a_editor) ;
	return FALSE ;
}

static void
external_encoding_changed_cb (GtkComboBox * a_encoding_combo,
                              MlViewNodeEditor * a_editor)
{
	gchar *encoding = NULL;

	THROW_IF_FAIL (a_encoding_combo
	                  && GTK_IS_COMBO_BOX (a_encoding_combo));
	THROW_IF_FAIL (a_editor
	                  && MLVIEW_IS_NODE_EDITOR (a_editor)
	                  && PRIVATE (a_editor));

	if (PRIVATE (a_editor)->curr_xml_node == NULL
	        || PRIVATE (a_editor)->curr_xml_node->doc == NULL)
		return;

#ifdef GTK_2_6_SERIE_OR_ABOVE

	encoding = (gchar*)g_strdup (gtk_combo_box_get_active_text (a_encoding_combo));
#else

	encoding = (gchar*)g_strdup
	           (mlview_utils_combo_box_get_active_text (a_encoding_combo));

#endif

	if (encoding == NULL)
		return;

	if (mlview_utils_is_encoding_supported (encoding) ==
	        TRUE) {

		if (PRIVATE (a_editor)->curr_xml_node->doc->
		        encoding) {
			xmlFree ((xmlChar *) PRIVATE (a_editor)->
			         curr_xml_node->doc->encoding);
		}

		PRIVATE (a_editor)->curr_xml_node->doc->
		encoding = (const xmlChar *) xmlMemStrdup (encoding);
	} else {
		/*
		 *Libxml2 does not support the new encoding so ...
		 *why not setting the encoding to UTF-8 as default ?
		 *
		 * toonsy 11/11/2004: OK Let's do it :)
		 */
		PRIVATE (a_editor)->curr_xml_node->doc->
		encoding = xmlCharStrdup ("UTF-8");
	}

	if (encoding) {
		g_free (encoding);
		encoding = NULL;
	}
}

static void
xml_doc_node_changed_cb (MlViewXMLDocument *a_this,
                         xmlNode *a_node,
                         MlViewNodeEditor *a_editor)
{
	THROW_IF_FAIL (a_this
	                  && MLVIEW_IS_XML_DOCUMENT (a_this)
	                  && a_node
	                  && MLVIEW_IS_NODE_EDITOR (a_editor)
	                  && PRIVATE (a_editor)) ;


	if (PRIVATE (a_editor)->curr_xml_node != a_node)
		return ;
	mlview_node_editor_edit_xml_node (a_editor, a_this, a_node) ;
}

static void
xml_doc_node_selected_cb (MlViewXMLDocument *a_doc,
                          xmlNode *a_node,
                          MlViewNodeEditor *a_editor)
{
	THROW_IF_FAIL (a_doc && MLVIEW_IS_XML_DOCUMENT (a_doc)
	                  && a_node && a_editor
	                  && MLVIEW_IS_NODE_EDITOR (a_editor)) ;

	mlview_node_editor_edit_xml_node (a_editor, a_doc,
	                                  a_node) ;
}

static void
text_inserted_in_text_node_view_cb (GtkTextBuffer *a_text_buffer,
                                    GtkTextIter *a_iter,
                                    gchar *a_text,
                                    gint a_len,
                                    gpointer a_user_data)
{
	MlViewNodeEditor *thiz = NULL ;

	THROW_IF_FAIL (a_text_buffer
	                  && GTK_IS_TEXT_BUFFER (a_text_buffer)
	                  && a_iter
	                  && a_user_data) ;

	thiz = MLVIEW_NODE_EDITOR (a_user_data) ;
	THROW_IF_FAIL (thiz && PRIVATE (thiz)) ;

	if (a_text && a_len) {
		if (PRIVATE (thiz)->text_node_view) {
			if (PRIVATE (thiz)->text_node_view->started_editing_transaction == FALSE) {
				PRIVATE (thiz)->text_node_view->transaction_node = PRIVATE (thiz)->curr_xml_node ;
				PRIVATE (thiz)->text_node_view->started_editing_transaction = TRUE ;
			} else {
				THROW_IF_FAIL (PRIVATE (thiz)->text_node_view->transaction_node
				                  == PRIVATE (thiz)->curr_xml_node) ;
			}
		}
	}
}

static void
text_range_deleted_in_text_node_view_cb (GtkTextBuffer *a_text_buffer,
        GtkTextIter *a_start,
        GtkTextIter *a_end,
        gpointer a_user_data)
{
	MlViewNodeEditor *thiz = NULL ;

	THROW_IF_FAIL (a_text_buffer
	                  && GTK_IS_TEXT_BUFFER (a_text_buffer)
	                  && a_start && a_end
	                  && a_user_data) ;

	thiz = MLVIEW_NODE_EDITOR (a_user_data) ;
	THROW_IF_FAIL (thiz && PRIVATE (thiz)) ;

	if (PRIVATE (thiz)->text_node_view) {
		if (PRIVATE (thiz)->text_node_view->started_editing_transaction == FALSE) {
			PRIVATE (thiz)->text_node_view->transaction_node = PRIVATE (thiz)->curr_xml_node ;
			PRIVATE (thiz)->text_node_view->started_editing_transaction = TRUE ;
		} else {
			THROW_IF_FAIL (PRIVATE (thiz)->text_node_view->transaction_node
			                  == PRIVATE (thiz)->curr_xml_node) ;
		}
	}
}

static void
text_inserted_in_element_name_cb (GtkEditable *a_editable,
                                  MlViewNodeEditor *a_this)
{
	THROW_IF_FAIL (a_editable && GTK_IS_ENTRY (a_editable)) ;
	THROW_IF_FAIL (a_this && MLVIEW_IS_NODE_EDITOR (a_this)
	                  && PRIVATE (a_this)
	                  && PRIVATE (a_this)->element_node_view
	                  && PRIVATE (a_this)->curr_xml_node) ;

	if (PRIVATE (a_this)->element_node_view->started_editing_transaction == TRUE)
		return ;

	PRIVATE (a_this)->element_node_view->started_editing_transaction = TRUE ;
	PRIVATE (a_this)->element_node_view->transaction_node =
	    PRIVATE (a_this)->curr_xml_node ;
}

static void
text_inserted_in_comment_node_view_cb (GtkTextBuffer *a_text_buffer,
                                       GtkTextIter *a_iter,
                                       gchar *a_text,
                                       gint a_len,
                                       gpointer a_user_data)
{
	MlViewNodeEditor *thiz = NULL ;

	THROW_IF_FAIL (a_text_buffer
	                  && GTK_IS_TEXT_BUFFER (a_text_buffer)
	                  && a_iter
	                  && a_user_data) ;

	thiz = MLVIEW_NODE_EDITOR (a_user_data) ;
	THROW_IF_FAIL (thiz && PRIVATE (thiz)) ;

	if (a_text && a_len) {
		if (PRIVATE (thiz)->text_node_view) {
			if (PRIVATE (thiz)->comment_node_view->started_editing_transaction == FALSE) {
				PRIVATE (thiz)->comment_node_view->transaction_node = PRIVATE (thiz)->curr_xml_node ;
				PRIVATE (thiz)->comment_node_view->started_editing_transaction = TRUE ;
			} else {
				THROW_IF_FAIL (PRIVATE (thiz)->comment_node_view->transaction_node
				                  == PRIVATE (thiz)->curr_xml_node) ;
			}
		}
	}
}

static void
text_inserted_in_cdata_node_view_cb (GtkTextBuffer *a_text_buffer,
                                     GtkTextIter *a_iter,
                                     gchar *a_text,
                                     gint a_len,
                                     gpointer a_user_data)
{
	MlViewNodeEditor *thiz = NULL ;

	THROW_IF_FAIL (a_text_buffer
	                  && GTK_IS_TEXT_BUFFER (a_text_buffer)
	                  && a_iter
	                  && a_user_data) ;

	thiz = MLVIEW_NODE_EDITOR (a_user_data) ;
	THROW_IF_FAIL (thiz && PRIVATE (thiz)) ;

	if (a_text && a_len) {
		if (PRIVATE (thiz)->cdata_section_node_view) {
			if (PRIVATE (thiz)->cdata_section_node_view->started_editing_transaction == FALSE) {
				PRIVATE (thiz)->cdata_section_node_view->transaction_node = PRIVATE (thiz)->curr_xml_node ;
				PRIVATE (thiz)->cdata_section_node_view->started_editing_transaction = TRUE ;
			} else {
				THROW_IF_FAIL (PRIVATE (thiz)->cdata_section_node_view->transaction_node
				                  == PRIVATE (thiz)->curr_xml_node) ;
			}
		}
	}
}

static gboolean
key_press_event_cb (GtkWidget *a_this,
                    GdkEvent *a_event,
                    gpointer a_user_data)
{
	MlViewNodeEditor *thiz = NULL ;
	enum MlViewStatus status = MLVIEW_OK ;
	struct MlViewKBDef *keybinding = NULL ;
	gboolean result = FALSE ;

	thiz = MLVIEW_NODE_EDITOR (a_user_data) ;
	g_return_val_if_fail (thiz && PRIVATE (thiz)
	                      && PRIVATE (thiz)->kb_eng,
	                      FALSE) ;


	status = mlview_kb_lookup_key_binding_from_key_press
	         (PRIVATE (thiz)->kb_eng,
	          (GdkEventKey*)a_event,
	          &keybinding) ;

	if (status == MLVIEW_OK && keybinding) {
		if (keybinding->action) {
			keybinding->action (thiz) ;
		}
		result = TRUE ;
	} else if (status == MLVIEW_KEY_SEQUENCE_TOO_SHORT_ERROR) {
		result = TRUE ;
	} else {
		result = FALSE ;
	}
	return result ;
}


static void
realize_cb (GtkWidget *a_this,
            MlViewNodeEditor *a_editor)
{
	THROW_IF_FAIL (a_this
	                  && GTK_IS_WIDGET (a_this)) ;
	THROW_IF_FAIL (a_editor
	                  && MLVIEW_IS_NODE_EDITOR (a_editor)) ;

	g_signal_connect (G_OBJECT (a_this),
	                  "key-press-event",
	                  G_CALLBACK (key_press_event_cb),
	                  a_editor) ;
}

static void
xml_doc_replace_node_cb (MlViewXMLDocument *a_xml_doc,
                         xmlNode *a_old_node,
                         xmlNode *a_new_node,
                         gpointer a_user_data)
{
	MlViewNodeEditor *thiz = NULL ;

	THROW_IF_FAIL (a_xml_doc && MLVIEW_IS_XML_DOCUMENT (a_xml_doc)
	                  && a_old_node && a_new_node && a_user_data
	                  && MLVIEW_IS_NODE_EDITOR (a_user_data)) ;

	thiz = MLVIEW_NODE_EDITOR (a_user_data) ;
	THROW_IF_FAIL (thiz && PRIVATE (thiz)) ;

	/*commit all the pending editing transactions*/
	mlview_node_editor_commit_editing_transaction (thiz) ;

	/* if the current node is the one that is going to be replaced, then
	 * set the current node pointer to NULL. This prevents from touching
	 * dirty memory when the replace node is freed.
	 */
	PRIVATE (thiz)->curr_xml_node = NULL ;
}

static void
xml_doc_node_commented_cb (MlViewXMLDocument *a_xml_doc,
                           xmlNode *a_node,
                           xmlNode *a_new_node,
                           gpointer a_user_data)
{
	MlViewNodeEditor *thiz = NULL ;

	THROW_IF_FAIL (a_xml_doc && MLVIEW_IS_XML_DOCUMENT (a_xml_doc)
	                  && a_user_data && MLVIEW_IS_NODE_EDITOR (a_user_data)
	                  && a_node && a_new_node) ;
	THROW_IF_FAIL (a_node->type != XML_COMMENT_NODE
	                  && a_new_node->type == XML_COMMENT_NODE) ;

	thiz = MLVIEW_NODE_EDITOR (a_user_data) ;
	THROW_IF_FAIL (thiz && PRIVATE (thiz)) ;

	PRIVATE (thiz)->curr_xml_node = a_new_node;
	mlview_node_editor_edit_xml_node (thiz, a_xml_doc, a_new_node) ;
}

static void
xml_doc_node_uncommented_cb (MlViewXMLDocument *a_xml_doc,
                             xmlNode *a_node,
                             xmlNode *a_new_node,
                             gpointer a_user_data)
{
	MlViewNodeEditor *thiz = NULL ;

	THROW_IF_FAIL (a_xml_doc && MLVIEW_IS_XML_DOCUMENT (a_xml_doc)
	                  && a_user_data && MLVIEW_IS_NODE_EDITOR (a_user_data)
	                  && a_node && a_new_node) ;
	THROW_IF_FAIL (a_node->type == XML_COMMENT_NODE
	                  && a_new_node->type != XML_COMMENT_NODE) ;

	thiz = MLVIEW_NODE_EDITOR (a_user_data) ;
	THROW_IF_FAIL (thiz && PRIVATE (thiz)) ;

	PRIVATE (thiz)->curr_xml_node = a_new_node;
	mlview_node_editor_edit_xml_node (thiz, a_xml_doc, a_new_node) ;
}

/******************************************
 *private gtk object framework methods
 ******************************************/

/**
 *Classical dynamic type allocator.
 */
guint
mlview_node_editor_get_type (void)
{

	static guint mlview_node_editor_type = 0;

	if (!mlview_node_editor_type) {
		static const GTypeInfo
		mlview_node_editor_type_info = {
		                                   sizeof (MlViewNodeEditorClass),
		                                   NULL,   /* base_init */
		                                   NULL,   /* base_finalize */
		                                   (GClassInitFunc)
		                                   mlview_node_editor_class_init,
		                                   NULL,   /* class_finalize */
		                                   NULL,   /* class_data */
		                                   sizeof (MlViewNodeEditor),
		                                   0,
		                                   (GInstanceInitFunc)
		                                   mlview_node_editor_init
		                               };

		mlview_node_editor_type =
		    g_type_register_static (GTK_TYPE_HPANED,
		                            "MlViewNodeEditor",
		                            &mlview_node_editor_type_info,
		                            (GTypeFlags) 0);
	}
	return mlview_node_editor_type;
}

/**
 *classical class struct initialyzer. This code overrides the
 *destroy method of the GtkObjectClass with mlview_node_editor_destroy. 
 *It defines
 *also the signal which offsets are 
 *strored in the static gv_mlview_node_editor_signals. 
 *
 */
static void
mlview_node_editor_class_init (MlViewNodeEditorClass * a_klass)
{
	GObjectClass *gobject_class = NULL ;
	gv_parent_class = (GtkHPanedClass *)
	                  gtk_type_class (gtk_hpaned_get_type ());

	gobject_class = G_OBJECT_CLASS (a_klass) ;
	THROW_IF_FAIL (gobject_class) ;

	/*overload the destroy method of object */
	gobject_class->dispose = mlview_node_editor_dispose ;
	gobject_class->finalize = mlview_node_editor_finalize ;

	/*signal definitions */
	gv_mlview_node_editor_signals[ELEMENT_CHANGED] =
	    g_signal_new ("element-changed",
	                  G_TYPE_FROM_CLASS (gobject_class),
	                  G_SIGNAL_RUN_FIRST,
	                  GTK_SIGNAL_OFFSET
	                  (MlViewNodeEditorClass,
	                   element_changed), NULL, NULL,
	                  gtk_marshal_NONE__NONE,
	                  G_TYPE_NONE,
	                  0,
	                  NULL);

	gv_mlview_node_editor_signals[ELEMENT_NAME_CHANGED] =
	    g_signal_new ("element-name-changed",
	                  G_TYPE_FROM_CLASS (gobject_class),
	                  G_SIGNAL_RUN_FIRST,
	                  GTK_SIGNAL_OFFSET
	                  (MlViewNodeEditorClass,
	                   element_name_changed), NULL, NULL,
	                  gtk_marshal_NONE__NONE,
	                  G_TYPE_NONE, 0, NULL);

	gv_mlview_node_editor_signals[ELEMENT_ATTRIBUTE_CHANGED]
	=
	    g_signal_new ("element-attribute-changed",
	                  G_TYPE_FROM_CLASS (gobject_class),
	                  G_SIGNAL_RUN_FIRST,
	                  GTK_SIGNAL_OFFSET
	                  (MlViewNodeEditorClass,
	                   element_attribute_changed), NULL,
	                  NULL, gtk_marshal_NONE__NONE,
	                  G_TYPE_NONE, 0, NULL);

	gv_mlview_node_editor_signals[ELEMENT_CONTENT_CHANGED] =
	    g_signal_new ("element-content-changed",
	                  G_TYPE_FROM_CLASS (gobject_class),
	                  G_SIGNAL_RUN_FIRST,
	                  GTK_SIGNAL_OFFSET
	                  (MlViewNodeEditorClass,
	                   element_content_changed), NULL,
	                  NULL, gtk_marshal_NONE__NONE,
	                  G_TYPE_NONE, 0, NULL);

	gv_mlview_node_editor_signals[EDIT_STATE_CHANGED] =
	    g_signal_new ("edit-state-changed",
	                  G_TYPE_FROM_CLASS (gobject_class),
	                  G_SIGNAL_RUN_FIRST,
	                  GTK_SIGNAL_OFFSET
	                  (MlViewNodeEditorClass,
	                   edit_state_changed), NULL, NULL,
	                  gtk_marshal_NONE__NONE,
	                  G_TYPE_NONE, 0, NULL);

	gv_mlview_node_editor_signals[UNGRAB_FOCUS_REQUESTED] =
	    g_signal_new ("ungrab-focus-requested",
	                  G_TYPE_FROM_CLASS (gobject_class),
	                  G_SIGNAL_RUN_FIRST,
	                  GTK_SIGNAL_OFFSET
	                  (MlViewNodeEditorClass, ungrab_focus_requested),
	                  NULL, NULL,
	                  gtk_marshal_NONE__NONE,
	                  G_TYPE_NONE, 0, NULL);

	/*no default signal handler for signal "document-changed" */
	a_klass->element_changed = NULL;
	a_klass->element_name_changed = NULL;
	a_klass->element_attribute_changed = NULL;
	a_klass->element_content_changed = NULL;
	a_klass->edit_state_changed = NULL;
}

static void
mlview_node_editor_dispose (GObject *a_this)
{
	MlViewNodeEditor *editor = NULL ;

	THROW_IF_FAIL (a_this
	                  && MLVIEW_IS_NODE_EDITOR (a_this)) ;
	editor = MLVIEW_NODE_EDITOR (a_this) ;
	THROW_IF_FAIL (PRIVATE (editor)) ;
	if (PRIVATE (editor)->dispose_has_run == TRUE) {
		return ;
	}
	if (PRIVATE (editor)->element_node_view) {
		g_free (PRIVATE (editor)->element_node_view) ;
		PRIVATE (editor)->element_node_view = NULL ;
	}
	if (PRIVATE (editor)->text_node_view) {
		g_free (PRIVATE (editor)->text_node_view) ;
		PRIVATE (editor)->text_node_view = NULL ;
	}
	if (PRIVATE (editor)->comment_node_view) {
		g_free (PRIVATE (editor)->comment_node_view) ;
		PRIVATE (editor)->comment_node_view = NULL ;
	}
	if (PRIVATE (editor)->cdata_section_node_view) {
		g_free (PRIVATE (editor)->cdata_section_node_view) ;
		PRIVATE (editor)->cdata_section_node_view = NULL ;
	}
	if (PRIVATE (editor)->pi_node_view) {
		g_free (PRIVATE (editor)->pi_node_view) ;
		PRIVATE (editor)->pi_node_view = NULL ;
	}
	if (PRIVATE (editor)->doc_node_view) {
		if (PRIVATE
		        (editor)->doc_node_view->ext_subset_system_id.references)
			g_hash_table_destroy
			(PRIVATE
			 (editor)->doc_node_view->ext_subset_system_id.references);

		g_free (PRIVATE (editor)->doc_node_view) ;
		PRIVATE (editor)->doc_node_view = NULL ;
	}
	PRIVATE (editor)->dispose_has_run = TRUE ;

	if (gv_parent_class
	        && G_OBJECT_CLASS (gv_parent_class)->dispose) {
		G_OBJECT_CLASS (gv_parent_class)->dispose (a_this) ;
	}
}

static void
mlview_node_editor_finalize (GObject *a_this)
{
	MlViewNodeEditor *editor = NULL ;

	THROW_IF_FAIL (a_this && MLVIEW_NODE_EDITOR (a_this)) ;
	editor = MLVIEW_NODE_EDITOR (a_this) ;
	THROW_IF_FAIL (PRIVATE (editor)) ;
	g_free (PRIVATE (editor)) ;
	PRIVATE (editor) = NULL ;

	if (gv_parent_class
	        && G_OBJECT_CLASS (gv_parent_class)->finalize) {
		G_OBJECT_CLASS (gv_parent_class)->finalize (a_this) ;
	}
}

static void
mlview_node_editor_construct (MlViewNodeEditor * a_this)
{
	enum MlViewStatus status = MLVIEW_OK ;
	MlViewNodeEditorPrivate *private_data = NULL;

	THROW_IF_FAIL (a_this != NULL);
	THROW_IF_FAIL (MLVIEW_IS_NODE_EDITOR (a_this));
	THROW_IF_FAIL (PRIVATE (a_this) != NULL);

	private_data = PRIVATE (a_this);
	private_data->curr_xml_node = NULL;
	private_data->node_view =
	    GTK_NOTEBOOK (gtk_notebook_new ());

	gtk_notebook_set_show_tabs (private_data->node_view,
	                            FALSE);

	gtk_notebook_popup_disable (private_data->node_view);
	gtk_paned_add1 (GTK_PANED (a_this),
	                GTK_WIDGET (private_data->node_view));

	/*
	 *now that the mlview app context context has been 
	 *passed to this object, can build
	 *sub components that need that context.
	 */
	mlview_node_editor_build_xml_element_node_view
	(a_this);
	mlview_node_editor_build_xml_text_node_view
	(a_this);
	mlview_node_editor_build_xml_comment_node_view
	(a_this);
	mlview_node_editor_build_xml_cdata_section_node_view
	(a_this);
	mlview_node_editor_build_xml_pi_node_view
	(a_this);
	mlview_node_editor_build_xml_doc_node_view
	(a_this);

	/*
	 *Instanciate the keybindings engine and register
	 *our keybindings.
	 */
	PRIVATE (a_this)->kb_eng = mlview_kb_eng_new () ;
	if (!PRIVATE (a_this)->kb_eng) {
		mlview_utils_trace_debug ("System may be out of memory") ;
	}
	status = mlview_kb_eng_register_key_bindings (PRIVATE (a_this)->kb_eng,
	         gv_keybindings,
	         sizeof (gv_keybindings) / sizeof (struct MlViewKBDef)) ;
	if (status != MLVIEW_OK)
		mlview_utils_trace_debug ("Keybindings registration failed") ;
}


/**
 *Classical instance initialyser.Creates the widget. 
 *
 */
static void
mlview_node_editor_init (MlViewNodeEditor * a_this)
{
	if (PRIVATE (a_this) == NULL)
		PRIVATE (a_this) = (MlViewNodeEditorPrivate *)
		                   g_malloc0 (sizeof
		                              (MlViewNodeEditorPrivate));

}

/*************************
 *public exported methods
 *************************/

/**
 *Default constructor.
 *@param a_app_context the application context.
 *@param a_doc #MlViewXMLDocument the document object model we
 *are editing.
 *@return the newly built widget (node editor) or NULL in case of
 *error.
 */
GtkWidget *
mlview_node_editor_new (MlViewXMLDocument *a_doc)
{
	MlViewNodeEditor *editor = NULL;

	editor = (MlViewNodeEditor *) g_object_new (MLVIEW_TYPE_NODE_EDITOR, NULL);
	PRIVATE (editor)->curr_xml_document = a_doc ;
	mlview_node_editor_construct (editor);

	return GTK_WIDGET (editor);
}


/**
 *Sets a new application context the node editor.
 *@param a_this the current instance of #MlViewNodeEditor.
 */
void
mlview_node_editor_set_application_context (MlViewNodeEditor *a_this)
{
	THROW_IF_FAIL (a_this != NULL);
	THROW_IF_FAIL (MLVIEW_IS_NODE_EDITOR (a_this));
	THROW_IF_FAIL (PRIVATE (a_this) != NULL);
}


/**
 *Gets the current xml node being edited by the node editor.
 *@return a pointer on the xmlNode being edited. 
 */
xmlNodePtr
mlview_node_editor_get_current_xml_node (MlViewNodeEditor *a_editor)
{
	g_return_val_if_fail (a_editor != NULL, NULL);
	return PRIVATE (a_editor)->curr_xml_node;
}


/**
 *Edits an xml node. This is basically what this
 *widget is for.
 *@param a_editor the current instance of #MlViewNodeEditor.
 *@param a_xml_doc the document object model the node belongs to.
 *@param a_node xml node to edit.
 */
void
mlview_node_editor_edit_xml_node (MlViewNodeEditor * a_editor,
                                  MlViewXMLDocument * a_xml_doc,
                                  xmlNode * a_node)
{
	/*some sanity checks */
	THROW_IF_FAIL (a_editor != NULL
	                  && MLVIEW_IS_NODE_EDITOR (a_editor));
	THROW_IF_FAIL (MLVIEW_IS_NODE_EDITOR (a_editor));
	THROW_IF_FAIL (PRIVATE (a_editor) != NULL);
	THROW_IF_FAIL (a_xml_doc != NULL);
	THROW_IF_FAIL (MLVIEW_IS_XML_DOCUMENT (a_xml_doc));

	if (PRIVATE (a_editor)->curr_xml_node) {
		/*
		 *If there are pending editing transactions,
		 *get out ! Pending transactions must be flushed
		 *out before calling us here.
		 */
		if (mlview_node_editor_has_an_editing_transaction_started (a_editor) == TRUE) {
			return ;
		}
	}
	PRIVATE (a_editor)->curr_xml_node = a_node;
	THROW_IF_FAIL (PRIVATE (a_editor)->curr_xml_node !=
	                  NULL);

	switch (a_node->type) {
	case XML_ELEMENT_NODE:
		/*
		 *edit the element node using the 
		 *XMLElementNodeView of the element editor
		 */
		mlview_node_editor_xml_element_node_view_edit_xml_node
		(a_editor, a_xml_doc, a_node);
		break;

	case XML_TEXT_NODE:
		/*
		 *show the text content using the 
		 *XMLTextNodeView of the element editor
		 */
		mlview_node_editor_xml_text_node_view_edit_xml_node
		(a_editor, a_xml_doc, a_node);
		break;

	case XML_CDATA_SECTION_NODE:
		/*
		 *show the cdata using the 
		 *XMLCDataSectionNodeView of the element editor
		 */
		mlview_node_editor_xml_cdata_section_node_view_edit_xml_node
		(a_editor, a_xml_doc, a_node);
		break;

	case XML_COMMENT_NODE:
		mlview_node_editor_xml_comment_node_view_edit_xml_node
		(a_editor, a_xml_doc, a_node);
		break;

	case XML_DOCUMENT_NODE:
		mlview_node_editor_xml_doc_node_view_edit_xml_node
		(a_editor, a_xml_doc, a_node);
		break;

	case XML_PI_NODE:
		mlview_node_editor_xml_pi_node_view_edit_xml_node
		(a_editor, a_xml_doc, a_node);
		break;

	default:
		break;
	}
	/*FIXME: remove the provious children of right side of a_editor */
	gtk_widget_show_all (GTK_WIDGET
	                     (PRIVATE (a_editor)->node_view));

}                               /*end mlview_node_editor_edit_xml_node */


static gboolean
grab_focus (MlViewNodeEditor *a_this)
{
	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_NODE_EDITOR (a_this)
	                      && PRIVATE (a_this)
	                      && PRIVATE (a_this)->node_view,
	                      FALSE) ;

	if (PRIVATE (a_this)->cur_focusable_widget
	        && GTK_WIDGET_CAN_FOCUS (PRIVATE (a_this)->cur_focusable_widget)) {
#ifdef MLVIEW_VERBOSE
		g_print ("grabing the focus on the current focusable widget of the node editor\n") ;
		gtk_widget_grab_focus (PRIVATE (a_this)->cur_focusable_widget) ;
#endif

	}
#ifdef MLVIEW_VERBOSE
	else {
		g_print ("no focusable widget found\n") ;
	}
#endif
	return FALSE ;
}

enum MlViewStatus
mlview_node_editor_grab_focus (MlViewNodeEditor *a_this)
{
	g_return_val_if_fail (a_this && MLVIEW_IS_NODE_EDITOR (a_this)
	                      && PRIVATE (a_this)
	                      && PRIVATE (a_this)->node_view,
	                      MLVIEW_BAD_PARAM_ERROR) ;

	if (GTK_WIDGET_HAS_GRAB (a_this)) {
#ifdef MLVIEW_VERBOSE
		g_print ("in mlview_node_editor_grab_focus(): "
		         "widget already is in the grab stack\n") ;
#endif

		return MLVIEW_OK ;
	} else {
#ifdef MLVIEW_VERBOSE
		g_print ("in mlview_node_editor_grab_focus(): "
		         "grabing the focus.\n") ;
#endif

		g_idle_add ((GSourceFunc)grab_focus,
		            a_this) ;
	}
	return MLVIEW_OK ;
}


enum MlViewStatus
mlview_node_editor_request_ungrab_focus (MlViewNodeEditor *a_this)
{
	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_NODE_EDITOR (a_this)
	                      && PRIVATE (a_this),
	                      MLVIEW_BAD_PARAM_ERROR) ;

#ifdef MLVIEW_VERBOSE

	g_print ("mlview_node_editor_request_ungrab_focus()\n") ;
#endif

	g_signal_emit (G_OBJECT (a_this),
	               gv_mlview_node_editor_signals[UNGRAB_FOCUS_REQUESTED], 0) ;

	return MLVIEW_OK ;
}


/**
 *Visualy (only) Clears the element editor. 
 *Does not free anything. 
 *@param a_editor the current instance of #MlViewNodeEditor.
 */
void
mlview_node_editor_clear (MlViewNodeEditor * a_editor)
{
	THROW_IF_FAIL (a_editor != NULL
	               && MLVIEW_IS_NODE_EDITOR (a_editor));

	/*clear all the views */
	mlview_node_editor_clear_xml_element_node_view (a_editor);
	mlview_node_editor_clear_xml_text_node_view (a_editor);
	mlview_node_editor_clear_xml_comment_node_view (a_editor);
	mlview_node_editor_clear_xml_cdata_section_node_view (a_editor);
	mlview_node_editor_clear_xml_pi_node_view (a_editor);

}

/**
 *Connects the signals of the document object model.
 *@param a_this the current instance of #MlViewNodeEditor
 *@param a_doc the documet object model to connect to.
 *@return MLVIEW_OK upon successful completion, an error code 
 *otherwise.
 *@return MLVIEW_OK upon successful completion, an error code otherwise.
 */
enum MlViewStatus
mlview_node_editor_connect_to_doc (MlViewNodeEditor *a_this,
                                   MlViewXMLDocument *a_doc)
{
	MlViewSchemaList *list = NULL;

	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_NODE_EDITOR (a_this)
	                      && PRIVATE (a_this)
	                      && a_doc
	                      && MLVIEW_IS_XML_DOCUMENT (a_doc),
	                      MLVIEW_BAD_PARAM_ERROR) ;

	g_signal_connect (G_OBJECT (a_doc),
	                  "node-selected",
	                  G_CALLBACK (xml_doc_node_selected_cb),
	                  a_this) ;

	g_signal_connect (G_OBJECT (a_doc),
	                  "node-unselected",
	                  G_CALLBACK (xml_doc_node_unselected_cb),
	                  a_this) ;

	g_signal_connect (G_OBJECT (a_doc),
	                  "node-changed",
	                  G_CALLBACK (xml_doc_node_changed_cb),
	                  a_this) ;

	g_signal_connect (G_OBJECT (a_doc),
	                  "ext-subset-changed",
	                  G_CALLBACK (ext_subset_changed_cb),
	                  a_this);

	g_signal_connect (G_OBJECT (a_doc),
	                  "replace-node",
	                  G_CALLBACK (xml_doc_replace_node_cb),
	                  a_this) ;

	g_signal_connect (G_OBJECT (a_doc),
	                  "node-commented",
	                  G_CALLBACK (xml_doc_node_commented_cb),
	                  a_this) ;

	g_signal_connect (G_OBJECT (a_doc),
	                  "node-uncommented",
	                  G_CALLBACK (xml_doc_node_uncommented_cb),
	                  a_this) ;
	if (PRIVATE (a_this)->element_node_view) {
		g_object_ref (G_OBJECT
		              (PRIVATE (a_this)->element_node_view->attrs_editor)) ;
		mlview_attrs_editor_connect_to_doc
		(PRIVATE (a_this)->element_node_view->attrs_editor,
		 a_doc) ;
	}

	g_object_ref (G_OBJECT (PRIVATE (a_this)->element_node_view->ns_editor)) ;
	mlview_ns_editor_connect_to_doc
	(PRIVATE (a_this)->element_node_view->ns_editor,
	 a_doc) ;


	list = mlview_xml_document_get_schema_list (a_doc);

	g_return_val_if_fail (list && MLVIEW_IS_SCHEMA_LIST (list),
	                      MLVIEW_BAD_PARAM_ERROR);

	g_signal_connect (G_OBJECT (list),
	                  "schema-associated",
	                  G_CALLBACK (schema_associated_cb),
	                  a_this);

	g_signal_connect (G_OBJECT (list),
	                  "schema-unassociated",
	                  G_CALLBACK (schema_unassociated_cb),
	                  a_this);


	return MLVIEW_OK ;
}

/**
 *Disconnect from the document object model we connected to using
 *mlview_node_editor_connect_to_doc().
 *@param a_this the current instance of #MlViewNodeEditor
 *@param a_doc the document object model to connect to.
 *@return MLVIEW_OK upon successful completion, an error code
 *otherwise.
 */
enum MlViewStatus
mlview_node_editor_disconnect_from_doc (MlViewNodeEditor *a_this,
                                        MlViewXMLDocument *a_doc)
{
	MlViewSchemaList *list = NULL;

	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_NODE_EDITOR (a_this)
	                      && PRIVATE (a_this)
	                      && a_doc
	                      && MLVIEW_IS_XML_DOCUMENT (a_doc)
	                      && MLVIEW_IS_XML_DOCUMENT (a_doc),
	                      MLVIEW_BAD_PARAM_ERROR) ;

	g_signal_handlers_disconnect_by_func
	(G_OBJECT (a_doc),
	 (void *) (xml_doc_node_changed_cb),
	 a_this) ;
	g_signal_handlers_disconnect_by_func
	(G_OBJECT (a_doc),
	 (void *) (xml_doc_node_selected_cb),
	 a_this) ;
	g_signal_handlers_disconnect_by_func
	(G_OBJECT (a_doc),
	 (void *) (xml_doc_node_unselected_cb),
	 a_this) ;
	g_signal_handlers_disconnect_by_func
	(G_OBJECT (a_doc),
	 (void *) (ext_subset_changed_cb),
	 a_this);
	g_signal_handlers_disconnect_by_func
	(G_OBJECT (a_doc),
	 (void *) (xml_doc_replace_node_cb),
	 a_this) ;
	g_signal_handlers_disconnect_by_func
	(G_OBJECT (a_doc),
	 (void *) (xml_doc_node_commented_cb),
	 a_this) ;
	g_signal_handlers_disconnect_by_func
	(G_OBJECT (a_doc),
	 (void *) (xml_doc_node_uncommented_cb),
	 a_this) ;


	if (PRIVATE (a_this)->element_node_view) {
		mlview_attrs_editor_disconnect_from_doc
		(PRIVATE (a_this)->element_node_view->attrs_editor,
		 a_doc) ;
		g_object_unref
		(G_OBJECT
		 (PRIVATE (a_this)->element_node_view->attrs_editor)) ;

		mlview_ns_editor_disconnect_from_doc
		(PRIVATE (a_this)->element_node_view->ns_editor,
		 a_doc) ;
		g_object_unref
		(G_OBJECT
		 (PRIVATE (a_this)->element_node_view->ns_editor)) ;
	}

	list = mlview_xml_document_get_schema_list (a_doc);

	g_return_val_if_fail (list && MLVIEW_IS_SCHEMA_LIST (list),
	                      MLVIEW_BAD_PARAM_ERROR);

	g_signal_handlers_disconnect_by_func
	(G_OBJECT (list), (void *) (schema_associated_cb),
	 a_this);

	g_signal_handlers_disconnect_by_func
	(G_OBJECT (list), (void *) (schema_unassociated_cb),
	 a_this);

	return MLVIEW_OK ;
}
