/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 8-*- */

/*
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute 
 *it and/or modify it under the terms of 
 *the GNU General Public License as published by the 
 *Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY;
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file copyright information.
 */

#ifndef __MLVIEW_DOC_MUTATION_H__
#define __MLVIEW_DOC_MUTATION_H__

#include "mlview-utils.h"
#include "mlview-xml-document.h"

/**
 *@file
 *#MlViewDocMutation class declaration file
 */

G_BEGIN_DECLS

#define MLVIEW_TYPE_DOC_MUTATION (mlview_doc_mutation_get_type())
#define MLVIEW_DOC_MUTATION(object) (G_TYPE_CHECK_INSTANCE_CAST((object), MLVIEW_TYPE_DOC_MUTATION, MlViewDocMutation))
#define MLVIEW_DOC_MUTATION_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST((klass), MLVIEW_TYPE_DOC_MUTATION, MlViewDocMutationClass))
#define MLVIEW_IS_DOC_MUTATION(object) (G_TYPE_CHECK_INSTANCE_TYPE((object), MLVIEW_TYPE_DOC_MUTATION))
#define MLVIEW_IS_DOC_MUTATION_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass), MLVIEW_TYPE_DOC_MUTATION))

typedef struct _MlViewDocMutation MlViewDocMutation ;
typedef struct _MlViewDocMutationClass MlViewDocMutationClass ;
typedef struct _MlViewDocMutationPrivate MlViewDocMutationPrivate ;

typedef enum MlViewStatus (*MlViewDoMutationFunc) (MlViewDocMutation *a_this,
        gpointer a_user_data) ;

typedef enum MlViewStatus (*MlViewUndoMutationFunc) (MlViewDocMutation *a_this,
        gpointer a_user_data) ;

/**
 *The #MlViewDocMutation class.
 *This class abstracts a mutation that can be performed 
 *on the MlViewXMLDocument. Mutations are central to the
 *undo/redo machinery.
 *A mutation is a modification of the #MlViewXMLDocument.
 *Basically, every editing action that modifies an instance
 *of #MlViewXMLDocument is a mutation.
 */
struct _MlViewDocMutation
{
	GObject parent_object ;
	MlViewDocMutationPrivate *priv ;
} ;

struct _MlViewDocMutationClass
{
	GObjectClass parent_class ;

} ;

GType mlview_doc_mutation_get_type (void) ;

MlViewDocMutation * mlview_doc_mutation_new (MlViewXMLDocument *a_mlview_xml_doc,
        MlViewDoMutationFunc a_do_mutation_func,
        MlViewUndoMutationFunc an_undo_mutation_func,
        const gchar *a_mutation_name) ;

enum MlViewStatus mlview_doc_mutation_do_mutation (MlViewDocMutation *a_this,
        gpointer a_user_data) ;

enum MlViewStatus mlview_doc_mutation_undo_mutation (MlViewDocMutation *a_this,
        gpointer a_user_data) ;

MlViewXMLDocument *mlview_doc_mutation_get_doc (MlViewDocMutation *a_this) ;

enum MlViewStatus mlview_doc_mutation_ref (MlViewDocMutation *a_this) ;

enum MlViewStatus mlview_doc_mutation_unref (MlViewDocMutation *a_this) ;

enum MlViewStatus mlview_doc_mutation_destroy (MlViewDocMutation *a_this) ;

G_END_DECLS

#endif /*__MLVIEW_DOC_MUTATION_H__*/
