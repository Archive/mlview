/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 8-*- */
/*
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute 
 *it and/or modify it under the terms of 
 *the GNU General Public License as published by the 
 *Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */

#ifndef __MLVIEW_PARSING_UTILS_H__
#define __MLVIEW_PARSING_UTILS_H__

/**
 *@file  
 *Declaration of various helper functions used during xml
 *parsing and validation.
 */

#include <libxml/tree.h>
#include <libxml/relaxng.h>
#include <libxml/xmlschemas.h>
#include "mlview-xml-document.h"

G_BEGIN_DECLS

enum NODE_INSERTION_SCHEME {
        /*Insert node before current selected node */
        INSERT_BEFORE,

        /*insert node after current selected node */
        INSERT_AFTER,

        /*add a child node */
        ADD_CHILD,
        
        /*Change the name of the current element node*/
        CHANGE_CUR_ELEMENT_NAME
};

enum MLVIEW_PARSING_UTILS_STATUS {
        OK = 0,
        NOK,
        GENERIC_ASSERTION_ERROR,
        /*GENERIC VALUES */

        BAD_PARAMETER,
        APP_SETTINGS_NOT_AVAILABLE,
        VALIDATION_IS_OFF,
        DOCUMENT_HAS_NO_DTD,
        /*function specific values */

        NO_SUBTREE_REQUIRED_BY_SCHEMA,
        ELEMENT_DESC_NOT_FOUND,
        ATTRIBUTE_DESC_NOT_FOUND,
        NULL_ELEMENT_CONTENT
};


typedef struct _MlViewExtSubsDef MlViewExtSubsDef ;

/*a data structure to define an external subset.*/
struct _MlViewExtSubsDef {
        gchar *external_id;     /*the entity public id */
        gchar *system_id;
        gchar *root_element_name;
};

MlViewExtSubsDef * mlview_ext_subs_def_new (const gchar * a_root_element_name,
                                            const gchar * a_external_id,
                                            const gchar * a_system_id) ;

void mlview_ext_subs_def_destroy (MlViewExtSubsDef * a_def);

MlViewExtSubsDef * mlview_utils_get_a_copy_of_last_ext_subs_def (void);

xmlDocPtr mlview_parsing_utils_load_xml_file_with_dtd_interactive 
						(const gchar * a_file_name) ;

xmlDocPtr mlview_parsing_utils_load_xml_file_with_dtd (const gchar *a_file_name,
                                                       const gchar *a_dtd_name);

xmlDtd *mlview_parsing_utils_load_a_dtd (MlViewExtSubsDef * a_subset_def) ;

gint mlview_parsing_utils_validate_dtd (xmlDoc * a_doc, 
                                        xmlDtd * a_dtd);


MlViewExtSubsDef * mlview_parsing_utils_let_user_choose_a_dtd (gchar * a_title) ;

gint mlview_parsing_utils_build_element_name_completion_list (enum NODE_INSERTION_SCHEME a_insertion_scheme,
                                                              xmlNode * a_current_xml_node,
                                                              GList ** a_feasible_names_ptr);

gint mlview_parsing_utils_build_attribute_name_completion_list 
					(xmlNode * a_current_xml_node, 
					 GList ** a_attr_names_compl_list,
					 gboolean a_required_attributes_only);

enum MLVIEW_PARSING_UTILS_STATUS mlview_parsing_utils_get_element_content_table 
(xmlElementContent * a_element_content, GHashTable ** a_element_content_table);

enum MLVIEW_PARSING_UTILS_STATUS mlview_parsing_utils_build_required_children_tree
												(xmlNode ** a_node);

enum MLVIEW_PARSING_UTILS_STATUS mlview_parsing_utils_build_required_attributes_list (xmlNode * a_node);

GList *mlview_parsing_utils_build_attribute_value_set
(xmlAttribute * a_attribute_desc, gint * a_last_id);

GList *mlview_parsing_utils_build_graphical_attr_values
(xmlAttribute * a_attribute_desc, gint * a_last_id);

xmlDtdPtr mlview_parsing_utils_load_dtd (const gchar *a_url);

xmlRelaxNGPtr mlview_parsing_utils_load_rng (const gchar *a_url);

xmlSchemaPtr mlview_parsing_utils_load_xsd (const gchar *a_url);

void mlview_parsing_utils_clean_dtd (xmlDtdPtr a_dtd);

enum MlViewStatus mlview_parsing_utils_parse_fragment (const xmlDoc *a_doc,
                                                       const xmlChar *a_buf,
                                                       xmlNode **a_result_node) ;

enum MlViewStatus mlview_parsing_utils_serialize_node_to_buf (const xmlNode *a_node,
                                                              gchar **out_buf) ;

enum MlViewStatus mlview_parsing_utils_do_comment_node (const xmlNode *a_node,
                                                        xmlNode **a_comment_node) ;


enum MlViewStatus
mlview_parsing_utils_uncomment_node (xmlDoc *a_doc,
		                     const xmlNode *comment_node,
		                     xmlNode **a_result_node) ;
G_END_DECLS

#endif
