/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 8-*- */

/*
 *This file is part of MlView
 *
 *MlView is free software; you can redistribute it and/or 
 *modify it under the terms of 
 *the GNU General Public License as published 
 *by the Free Software Foundation; either version 2
 *or (at your option) any later version.
 *
 *MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView.
 *see the file COPYING. 
 *If not, write to the 
 *Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */

#ifndef __MLVIEW_DBUS_MANAGER_H__
#define __MLVIEW_DBUS_MANAGER_H__

#include "mlview-app.h"

/**
 * These constants define the Singleton service
 * and the object path & name to make method calls on the service
 */
#define MLVIEW_DBUS_INSTANCE_SERVICE_NAME	"org.mlview.MlViewInstanceService"
#define MLVIEW_DBUS_INSTANCE_OBJECT_PATH	"/org/mlview/MlViewInstance"
#define MLVIEW_DBUS_INSTANCE_OBJECT_NAME	"org.mlview.MlViewInstance"

/**
 * Initialize MlView DBus facility
 * If an instance is running and there are URIs specified on the cmdline
 * they will be opened by the currently running instance,
 * then the program will exit
 *
 * @param app the current MlViewApp instance
 * @param args uri strings of documents to open
 *
 * Warning: the program will abort on errors
 */
void mlview_dbus_manager_init (MlViewApp *app, gchar **args);

/**
 * Shutdown MlView DBus facility
 */
void mlview_dbus_manager_shutdown (void);

#endif

