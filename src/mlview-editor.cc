/* -*- Mode: C++; indent-tabs-mode: true; c-basic-offset: 4-*- */

/**
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute it 
 *and/or modify it under the terms of 
 *the GNU General Public License as published 
 *by the Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *MlView is distributed in the hope that it will be useful, 
 *but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. If not, write to the Free Software Foundation, Inc., 
 *59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */

#include <string.h>
#include <map>
#include <libxml/uri.h>
#include <libxml/tree.h>
#include <glade/glade.h>
#include <gnome.h>
#include "recent-files/egg-recent.h"
#include "mlview-exception.h"
#include "mlview-editor.h"
#include "mlview-view-manager.h"
#include "mlview-view-factory.h"
#include "mlview-file-descriptor.h"
#include "mlview-xml-document.h"
#include "mlview-utils.h"
#include "mlview-xslt-utils.h"
#include "mlview-schemas-window.h"
#include "mlview-validator-window.h"
#include "mlview-safe-ptr-utils.h"
#include "mlview-prefs.h"
#include "mlview-prefs-category-general.h"

using namespace std ;
#define BEGIN_NAMESPACE_MLVIEW namespace mlview {
#define END_NAMESPACE_MLVIEW }

/**
 *@file
 *The definition of the methods of #MlViewEditor class.
 */
BEGIN_NAMESPACE_MLVIEW

enum {
    DOCUMENT_CHANGED,
    LAST_VIEW_REMOVED,
    FIRST_VIEW_ADDED,
    SIGNAL_NUM
};

struct DocumentWindowData
{
	Editor *editor;
	MlViewXMLDocument *document;
	GtkWidget *window;
};

/**
 * The Editor class private member type
 */
typedef map<IView*, MlViewXMLDocument*> ViewToDocMap ;
typedef map<UString, IView*> StringToViewMap ;
typedef map<UString, gint> StringToIntMap ;
typedef map<MlViewXMLDocument*, struct DocumentWindowData*> DocToWindowDataMap ;
typedef list<IView*> ListOfViews ;
typedef map<MlViewXMLDocument*, ListOfViews*> DocToViewsMap ;

struct EditorPriv
{

	/**
	 * A hash table where the keys are the instances
	 * of MlViewXMLDocument and the values are their
	 * matching DocumentWindowData, associated to the schema
	 * management window for each document.
	 */
	DocToWindowDataMap doc_to_schema_window_data_map ;

	/**
	 * A hash table where the keys are the instances of
	 * MlViewXMLDocument and the values are their matching
	 * DocumentWindowData, associated with the validation
	 * management window for each document.
	 */
	DocToWindowDataMap doc_to_validation_window_data_map ;

	/**
	*The notebook that holds all the notebook pages 
	*Each notebook page contains one editing view.
	*/
	Gtk::Notebook *notebook;
	sigc::connection notebook_signal_conn0 ;

	/**
	*a hash table which keys are the base name of 
	*the files already opened. 
	*The associated data is the number 
	*of times the file name
	*has been opened.
	*/
	StringToIntMap base_name_to_occurence_map ;

	/**
	*a hash table wich keys are the file paths of 
	*the files already opened.
	*When destroying this hashtable do not 
	*destroy the referenced data (file path)
	*because file paths are hold by instances 
	*of MlViewXMLDocument and destroyed by them.
	*/
	StringToViewMap uri_to_view_map ;

	/**
	*An hash table that associates the opened document label names to
	*the matching opened view.
	*/
	StringToViewMap tab_label_to_view_map ;

	/**
	 *Number of untitled document opened.
	 */
	guint untitled_docs_num;

	/**
	 *total number of docs opened
	 */
	guint opened_docs_num;

	SafePtr<ViewManager, ObjectRef, ObjectUnref> view_manager_ptr ;

	/*
	 *the editor contextual menu 
	 */
	GtkWidget *contextual_menu;

	gboolean dispose_has_run ;

	sigc::signal0<void> signal_document_changed ;

	EditorPriv () :
			notebook (NULL),
			untitled_docs_num (0),
			opened_docs_num (0),
			contextual_menu (NULL)
	{}

	~EditorPriv ()
	{
		if (view_manager_ptr) {
			list<IView*> views = view_manager_ptr->get_all_views () ;
			for (list<IView*>::iterator cur_view = views.begin ();
					cur_view != views.end ();
					++cur_view) {
				view_manager_ptr->remove_view (*cur_view) ;
			}
		}

		view_manager_ptr->set_cur_view (NULL);
	}

	static void schemas_window_destroy_cb
	(GtkWidget *a_widget, struct DocumentWindowData *a_win) ;

	static void validation_window_destroy_cb
	(GtkWidget *a_widget,
	 struct DocumentWindowData *a_win) ;

private:
	//forbid copy/assignment
	EditorPriv (EditorPriv const &) ;
	EditorPriv& operator= (EditorPriv const &) ;
} ;


//************************************************
// static functions
// **********************************************


//**********************************
//signal handlers
//***********************************


void
EditorPriv::schemas_window_destroy_cb (GtkWidget *a_widget,
                                       struct DocumentWindowData *a_win)
{
	THROW_IF_FAIL (a_win);
	THROW_IF_FAIL (a_win->document);
	THROW_IF_FAIL (a_win->editor);
	THROW_IF_FAIL (a_win->editor->m_priv) ;

	a_win->editor->m_priv->doc_to_schema_window_data_map.erase
	(a_win->document) ;

	g_free (a_win);
	a_win = NULL;
}



void
EditorPriv::validation_window_destroy_cb (GtkWidget *a_widget,
        struct DocumentWindowData *a_win)
{
	THROW_IF_FAIL (a_win);
	THROW_IF_FAIL (a_win->editor);
	THROW_IF_FAIL (a_win->document);
	THROW_IF_FAIL (a_win->editor->m_priv);

	a_win->editor->m_priv->doc_to_validation_window_data_map.erase
	(a_win->document) ;

	g_free (a_win);
	a_win = NULL;
}

//**************************************
//private methods
//****************************************

bool
Editor::confirm_close ()
{
	enum MlViewStatus status = MLVIEW_OK ;
	GtkWidget *dialog;
	gint ret;
	UString name = NULL;
	bool result = false ;

	THROW_IF_FAIL (m_priv) ;

	name = get_cur_view ()->get_view_name () ;

	THROW_IF_FAIL (status == MLVIEW_OK) ;

	dialog = gtk_message_dialog_new (NULL,
	                                 GTK_DIALOG_MODAL,
	                                 GTK_MESSAGE_QUESTION,
	                                 GTK_BUTTONS_NONE,
	                                 _("The document \"%s\" has been modifed.\n"
	                                   "Should I save it before closing it?"),
	                                 name.c_str ());

	gtk_dialog_add_buttons (GTK_DIALOG (dialog),
	                        _("_Close without Saving"), GTK_RESPONSE_NO,
	                        GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
	                        GTK_STOCK_SAVE, GTK_RESPONSE_YES, NULL);

	gtk_dialog_set_default_response (GTK_DIALOG (dialog), GTK_RESPONSE_YES);

	ret = gtk_dialog_run (GTK_DIALOG (dialog));
	switch (ret) {
	case GTK_RESPONSE_YES:
		save_and_close_xml_document () ;
		break;
	case GTK_RESPONSE_NO:
		close_xml_document_without_saving () ;
		break;
	case GTK_RESPONSE_CANCEL:
	case GTK_RESPONSE_DELETE_EVENT:
		result = true ;
		break ;
	default:
		g_assert_not_reached ();
	}

	gtk_widget_destroy (dialog);
	return result ;
}

bool
Editor::is_view_added_to_editor (IView *a_view)
{
	THROW_IF_FAIL (m_priv) ;

	return m_priv->view_manager_ptr->view_exists (a_view) ;
}

void
Editor::connect_to_app_context (AppContext *a_context)
{
	THROW_IF_FAIL (m_priv && a_context) ;
}

void
Editor::disconnect_from_app_context (AppContext *a_context)
{
	THROW_IF_FAIL (a_context) ;
}

GtkWidget *
Editor::build_reload_file_confirmation_dialog (void)
{
	GtkWidget *dialog=NULL, *hbox=NULL, *image=NULL,
	                              *label=NULL;

	dialog = gtk_dialog_new_with_buttons
	         (_("File already opened"), NULL,
	          GTK_DIALOG_MODAL,
	          GTK_STOCK_NO,GTK_RESPONSE_CANCEL,
	          GTK_STOCK_YES, GTK_RESPONSE_OK,
	          NULL) ;

	gtk_dialog_set_has_separator (GTK_DIALOG (dialog), FALSE);
	gtk_dialog_set_default_response (GTK_DIALOG (dialog),
	                                 GTK_RESPONSE_OK);

	hbox = gtk_hbox_new (FALSE, 6);
	image = gtk_image_new_from_stock (GTK_STOCK_DIALOG_QUESTION,
	                                  GTK_ICON_SIZE_DIALOG);
	gtk_misc_set_alignment (GTK_MISC (image),
	                        (gdouble)0.5, (gdouble)0.0);
	label = gtk_label_new
	        (_("This document is already open in the editor.\n"
	           " Do you want to reload it ?"));
	gtk_box_pack_start (GTK_BOX (hbox), image, FALSE, FALSE, 0);
	gtk_box_pack_start (GTK_BOX (hbox), label, TRUE, TRUE, 0);
	gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->vbox),
	                    hbox, FALSE, FALSE, 0);

	gtk_widget_show_all (dialog);
	return dialog ;
}

ViewDescriptor const *
Editor::select_view_to_open (void)
{
	GtkWidget *dialog = NULL ;
	GtkWidget *dialog_vbox = NULL ;
	GtkWidget *hbox1 = NULL ;
	GtkWidget *label1 = NULL ;
	GtkWidget *option_menu = NULL ;
	GtkWidget *menu = NULL ;
	GtkWidget *menu_item = NULL ;
	GtkWidget *dialog_action_area1 = NULL ;
	GtkWidget *button5 = NULL ;
	GtkWidget *button6 = NULL ;
	GtkWidget *sel_menu_item = NULL ;

	int button;
	ViewDescriptor const *result = NULL;
	ViewDescriptor const *view_desc_ptr = NULL ;
	gchar *base_name = NULL;
	guint nr_view_desc = 0 ;

	nr_view_desc = ViewFactory::get_number_of_view_desc () ;
	THROW_IF_FAIL (nr_view_desc) ;

	/*
	 *If there is only one type of view registered
	 *in the system, get its view descriptor and use that
	 *one.
	 */
	if (nr_view_desc == 1) {
		result = ViewFactory::get_view_descriptor_at (0) ;
		THROW_IF_FAIL (result) ;
		return result ;
	}

	dialog = gtk_dialog_new ();
	gtk_window_set_title (GTK_WINDOW (dialog), _("Select View"));

	dialog_vbox = GTK_DIALOG (dialog)->vbox;
	gtk_widget_show (dialog_vbox);

	hbox1 = gtk_hbox_new (FALSE, 0);
	gtk_widget_show (hbox1);
	gtk_box_pack_start (GTK_BOX (dialog_vbox), hbox1, TRUE, TRUE, 0);
	label1 = gtk_label_new (_("Select view to open"));
	gtk_widget_show (label1);
	gtk_box_pack_start (GTK_BOX (hbox1), label1, FALSE, FALSE, 10);

	/* build select view menu */
	option_menu = gtk_option_menu_new();
	menu = gtk_menu_new();
	gtk_option_menu_set_menu (GTK_OPTION_MENU(option_menu), menu);

	gtk_widget_show (menu);
	gtk_widget_show (option_menu);

	gtk_box_pack_start (GTK_BOX (hbox1), option_menu, TRUE, TRUE, 0);
	for (view_desc_ptr = ViewFactory::get_view_descriptors ();
	        view_desc_ptr && view_desc_ptr->view_type_name;
	        view_desc_ptr ++) {
		base_name = view_desc_ptr->view_type_name;
		menu_item = gtk_menu_item_new_with_label(base_name);
		gtk_menu_shell_append (GTK_MENU_SHELL(menu),  menu_item);
		gtk_widget_show(menu_item);
		g_object_set_data (G_OBJECT(menu_item), "mlview_view_desc", (gpointer)view_desc_ptr);
	}
	gtk_option_menu_set_history(GTK_OPTION_MENU(option_menu), 0);

	/* dialog box buttons */
	dialog_action_area1 = GTK_DIALOG (dialog)->action_area;
	gtk_widget_show (dialog_action_area1);
	gtk_button_box_set_layout (GTK_BUTTON_BOX (dialog_action_area1), GTK_BUTTONBOX_END);

	button5 = gtk_button_new_from_stock ("gtk-cancel");
	gtk_widget_show (button5);
	gtk_dialog_add_action_widget (GTK_DIALOG (dialog), button5, GTK_RESPONSE_CANCEL);
	GTK_WIDGET_SET_FLAGS (button5, GTK_CAN_DEFAULT);

	button6 = gtk_button_new_from_stock ("gtk-ok");
	gtk_widget_show (button6);
	gtk_dialog_add_action_widget (GTK_DIALOG (dialog), button6, GTK_RESPONSE_OK);
	GTK_WIDGET_SET_FLAGS (button6, GTK_CAN_DEFAULT);

	/* show dialog */
	button = gtk_dialog_run (GTK_DIALOG (dialog));


	switch (button) {
	case GTK_RESPONSE_OK:
		sel_menu_item = gtk_menu_get_active(GTK_MENU(menu));

		result = (ViewDescriptor *)g_object_get_data
		         (G_OBJECT(sel_menu_item), "mlview_view_desc");
		break;

	default:
		result = NULL ;
		break;
	}
	gtk_widget_destroy (dialog);
	return result;
}
//******************************************
// public methods
//******************************************


//********************************************************
//porting MlViewEditor to mlview::Editor in progress ....
//******************************************************

Editor::Editor (const UString &a_title)
{
	m_priv = new EditorPriv () ;

	m_priv->view_manager_ptr = new ViewManager ("OldGVC") ;

	Gtk::Widget *graphical_container =
	    m_priv->view_manager_ptr->get_embeddable_container_widget () ;
	THROW_IF_FAIL (graphical_container) ;

	this->pack_start (*graphical_container, true, true, 0);

	m_priv->untitled_docs_num = 0;

	m_priv->opened_docs_num = 0;

	AppContext *context = AppContext::get_instance () ;
	if (context) {
		connect_to_app_context (context) ;
	}
}

Editor::~Editor ()
{
	THROW_IF_FAIL (m_priv)  ;
	AppContext *context = AppContext::get_instance () ;
	if (context) {
		disconnect_from_app_context (context) ;
	}
	delete m_priv ;
	m_priv = NULL ;
}

IView *
Editor::create_new_view_on_document (MlViewXMLDocument *a_xml_doc)
{
	THROW_IF_FAIL (m_priv != NULL);
	THROW_IF_FAIL (a_xml_doc != NULL);
	THROW_IF_FAIL (MLVIEW_IS_XML_DOCUMENT (a_xml_doc));

	return create_new_view_on_document (a_xml_doc, NULL) ;
}

IView *
Editor::create_new_view_on_document (MlViewXMLDocument *a_doc,
                                     const UString &a_view_desc_type_name)
{
	IView *result = NULL;
	ViewDescriptor const *view_descriptor = NULL ;

	THROW_IF_FAIL (m_priv != NULL);
	THROW_IF_FAIL (a_doc != NULL);
	THROW_IF_FAIL (MLVIEW_IS_XML_DOCUMENT (a_doc));

	AppContext *context = AppContext::get_instance () ;
	THROW_IF_FAIL (context) ;

	if (a_view_desc_type_name != "") {
		view_descriptor = ViewFactory::peek_editing_view_descriptor
		                  (a_view_desc_type_name.c_str ()) ;
	}
	if (!view_descriptor) {
		view_descriptor = select_view_to_open ();
	}

	if (!view_descriptor) {
		LOG_TO_ERROR_STREAM ("Unknown view type name !"
		                     << endl
		                     << " This may be caused by a gconfd "
		                     "problem or a bad mlview default "
		                     "view type name gconf key\n"
		                     "First, try to killall gconfd and restart it\n"
		                     "If you still have the problem, send a mail to"
		                     "mlview-list@gnome.org to ask for help\n") ;
		return NULL ;
	}
	result = ViewFactory::create_view (a_doc,
	                                   view_descriptor->view_type_name,
	                                   NULL) ;

	THROW_IF_FAIL (result) ;

	return result;
}

IView *
Editor::create_new_view_on_document (MlViewXMLDocument *a_doc,
                                     ViewDescriptor *a_desc)
{
	IView *result = NULL ;
	THROW_IF_FAIL (a_doc && MLVIEW_IS_XML_DOCUMENT (a_doc) && a_desc) ;

	result = create_new_view_on_document (a_doc, a_desc->view_type_name) ;
	return result ;
}

IView *
Editor::create_new_view_on_current_document (ViewDescriptor* a_desc)
{
	MlViewXMLDocument *doc = NULL ;
	IView *result = NULL ;

	THROW_IF_FAIL (a_desc) ;
	doc = get_current_document () ;
	if (!doc) {
		mlview_utils_trace_debug ("Could not get the "
		                          "current doc being edited") ;
		return NULL ;
	}
	result = create_new_view_on_document (doc, a_desc) ;
	return result ;
}

/**
 *Interactively creates a new view on an existing
 *xml document.
 *The view will be created if and only if an non empty document is
 *currently selected in the editor.
 *@return the newly created view or NULL if it could not be created.
 */
IView *
Editor::create_new_view_on_current_document_interactive ()
{
	MlViewXMLDocument *xml_document = NULL;
	IView *result = NULL ;

	THROW_IF_FAIL (m_priv != NULL);


	if (get_cur_view () == NULL)
		return NULL;

	xml_document = get_cur_view ()->get_document () ;

	result = create_new_view_on_document (xml_document);
	m_priv->view_manager_ptr->insert_view (result) ;
	return result ;
}

IView *
Editor::get_cur_view () const
{
	THROW_IF_FAIL (m_priv);

	return m_priv->view_manager_ptr->get_cur_view ();
}

void
Editor::set_cur_view (IView *a_view)
{
        THROW_IF_FAIL (m_priv) ;
        THROW_IF_FAIL (m_priv->view_manager_ptr) ;

        m_priv->view_manager_ptr->set_cur_view (a_view) ;
}

ViewManager*
Editor::get_view_manager () const
{
        return m_priv->view_manager_ptr ;
}

/**
 *Getter of the current selected edition
 *view.
 *@return the current MlViewXMLDocumentView or NULL.
 */
MlViewXMLDocument *
Editor::get_current_document () const
{
	MlViewXMLDocument *doc = NULL ;

	THROW_IF_FAIL (m_priv);

	if (get_cur_view ()) {
		doc = get_cur_view ()->get_document ();
	}
	return doc ;
}

/**
 *Opens the xml file a_file_path, loads it, build a view to edit it and
 *adds this new view to the editor.
 *
 *@param a_file_path the path of the file to open.
 */
void
Editor::load_xml_file (const UString &a_file_path,
                       bool a_interactive)
{
	load_xml_file_with_dtd (a_file_path, "", a_interactive) ;
}

/**
 *Opens the xml file a_file_path, loads it, build a view to edit it and
 *adds this new view to the editor.
 *The file is validated with the DTD if provided or specified in the 
 *document / by the user at the prompt.
 *If a DTD is specified the user won't be prompted for DTD informations.
 *
 *@param a_file_path the path of the file to open.
 *@param a_dtd_path the path of the dtd to validate against.
 */
void
Editor::load_xml_file_with_dtd (const UString &a_file_path,
                                const UString &a_dtd_path,
                                bool a_disable_interaction)
{
	MlViewXMLDocument *mlview_xml_document = NULL;
	enum MlViewStatus status = MLVIEW_OK;
	gboolean is_relative = FALSE;
	gchar *absolute_path = NULL, *cur_dir = NULL;
	gchar *vfs_uri = NULL;

	THROW_IF_FAIL (m_priv);
	AppContext *context = AppContext::get_instance () ;
	THROW_IF_FAIL (context) ;

	if (a_file_path == "")
		return;

	status = mlview_utils_uri_is_relative (a_file_path.c_str (),
	                                       &is_relative);
	if (status != MLVIEW_OK) {
		THROW_IF_FAIL (context) ;
		context->error (_("The following URI is not well formed: %s"),
		                a_file_path.c_str ()) ;
		return;
	}

	if (is_relative == TRUE) {
		cur_dir = g_get_current_dir ();

		THROW_IF_FAIL (cur_dir);

		mlview_utils_relative_uri_to_absolute_uri (a_file_path.c_str (),
		        cur_dir,
		        &absolute_path);

		g_free (cur_dir);
		cur_dir = NULL;
	} else
		absolute_path = g_strdup (a_file_path.c_str ());

	THROW_IF_FAIL (absolute_path);

	if (strstr(absolute_path, "://"))
		vfs_uri = g_strdup (absolute_path);
	else
		vfs_uri = gnome_vfs_get_uri_from_local_path (absolute_path);
	THROW_IF_FAIL (vfs_uri);

	context->sbar_push_message (_("Opening file %s..."), absolute_path);

	if ((a_dtd_path != "")
	        || a_disable_interaction)
		mlview_xml_document =
		    mlview_xml_document_open_with_dtd (vfs_uri, a_dtd_path.c_str ()) ;
	else
		mlview_xml_document =
		    mlview_xml_document_open_with_dtd_interactive (vfs_uri);

	if (mlview_xml_document) {
		ViewDescriptor *view_descriptor = NULL ;
		IView *new_view = NULL ;

		//**********************************************
		//get the descriptor of the default editing view
		//so that we can instanciate a view of the type
		//of the default editing view defined in gconf
		//*********************************************
		view_descriptor =
		    ViewFactory::get_default_view_descriptor () ;

		if (!view_descriptor) {
			//***************************************
			//if we could not get the descriptor
			//of the default editing view, use the
			//source editing view as the default view
			//***************************************
			new_view = ViewFactory::create_view (mlview_xml_document,
			                                     "source-view",
			                                     vfs_uri );
			THROW_IF_FAIL (new_view != NULL) ;
		} else {
			new_view = ViewFactory::create_view
			           (mlview_xml_document,
			            view_descriptor->view_type_name,
			            vfs_uri) ;
			THROW_IF_FAIL (new_view != NULL) ;
		}
		THROW_IF_FAIL (new_view != NULL);
		m_priv->view_manager_ptr->insert_view (new_view) ;
	}

	g_free (absolute_path);
	g_free (vfs_uri);
	absolute_path = NULL;
	vfs_uri = NULL;

	/*
	 *FIXME: add support for exception thrown by 
	 *mlview_xml_document_open in case 
	 *of error and display that error => BIG DEAL
	 */

	context->sbar_pop_message ();
}

/**
 * Reloads the document being edited, that is, loads the file from the disk
 * or from where it has been loaded from. All the changes that have not been
 * saved before calling this method will then be lost.
 * @param a_this the current instance of #MlViewEditor
 * @return MLVIEW_OK upon successful completion, an error code otherwise
 */
enum MlViewStatus
Editor::reload_document (bool a_interactive)
{
	MlViewXMLDocument *doc = NULL ;
	gchar *file_path = NULL ;

	THROW_IF_FAIL (m_priv) ;

	doc = get_current_document () ;
	if (!doc) {
		/*Hugh ? there is no current doc ? So how did the UI
		 *let the user hit a button that lets her end up here then ?
		 *man, go fix the UI, so that when there is no document
		 *opened, grey out the buttons that let the user endup here.
		 */
		mlview_utils_trace_debug ("You asked for the current doc, but"
		                          "but there is no current doc.\n"
		                          "Maybe the UI shouldn't have let the user "
		                          "Hit a button that make her ask for the current "
		                          "doc when there is no current doc loaded ?\n"
		                          "Homeboy, go fix the damn UI.") ;
	}

	file_path = mlview_xml_document_get_file_path (doc) ;
	if (!file_path) {
		/*seems like this doc is new and has never been saved yet.
		 *We can then not reload it.
		 */
		return MLVIEW_CANT_RELOAD_ERROR ;
	}
	load_xml_file (file_path, a_interactive) ;

	if (file_path) {
		g_free (file_path) ;
		file_path = NULL ;
	}
	return MLVIEW_OK ;
}

void
Editor::open_xml_document_interactive ()
{
	GladeXML *glade_xml=NULL;
	gchar *path=NULL, *uri=NULL;
	gint response = 0;
	GtkDialog *uri_location_dialog = NULL;
	GnomeEntry *uri_location_entry = NULL;

	try {
		path = gnome_program_locate_file (NULL, GNOME_FILE_DOMAIN_APP_DATADIR,
		                                  "mlview/mlview-uri-dialog.glade",
		                                  TRUE, NULL) ;
		if (!path) {
			THROW ("Couldn't find mlview-uri-dialog.glade");
		}
		glade_xml = glade_xml_new (path, "URIDialog", NULL);
		if (!glade_xml) {
			THROW ("Couldn't extract dialog from glade file");
		}
		uri_location_dialog =  GTK_DIALOG (glade_xml_get_widget
		                                   (glade_xml, "URIDialog")) ;
		if (!uri_location_dialog) {
			THROW ("Couldn't get dialog widget from glade file");
		}
		gtk_dialog_set_has_separator (GTK_DIALOG (uri_location_dialog), FALSE);

		uri_location_entry = GNOME_ENTRY (glade_xml_get_widget
		                                  (glade_xml, "URILocationEntry"));
		if (!uri_location_entry) {
			THROW ("Couldn't get the location entry widget from glade file");
		}

		/* run dialog */
		response = gtk_dialog_run (uri_location_dialog);

		switch (response) {
		case GTK_RESPONSE_OK:
			uri = g_strdup (gtk_entry_get_text
			                (GTK_ENTRY (gnome_entry_gtk_entry
			                            (uri_location_entry))));

			/*
			 *if the document is already opened in the editor
			 *ask the user if she wants to reload it.
			 */
			if (uri && strcmp (uri, "")) {
				if (is_document_opened_in_editor (uri) == true) {
					GtkWidget *dialog = NULL ;
					gint res =  0 ;
					dialog = build_reload_file_confirmation_dialog () ;
					if (dialog) {
						res = gtk_dialog_run
						      (GTK_DIALOG (dialog)) ;
						switch (res) {
						case GTK_RESPONSE_OK:
							gnome_entry_prepend_history
							(uri_location_entry,
							 TRUE, uri);
							load_xml_file (uri, TRUE);
							break ;
						case GTK_RESPONSE_CANCEL:
							break;
						default:
							g_assert_not_reached () ;
						}
						gtk_widget_destroy (dialog) ;
						dialog = NULL ;
					}
				} else {
					gnome_entry_prepend_history
					(uri_location_entry, TRUE, uri);
					load_xml_file  (uri, TRUE);
				}
			}
			break;
		case GTK_RESPONSE_CANCEL:
		default:
			break;
		}

		AppContext *context = AppContext::get_instance () ;
		THROW_IF_FAIL (context) ;
		if (uri) {
			EggRecentModel *model = NULL;

			model = (EggRecentModel*) context->get_element ("MlViewRecentModel");
			THROW_IF_FAIL (model && EGG_IS_RECENT_MODEL (model)) ;
			egg_recent_model_add (model, uri);
		}
	} catch (Exception &e) {
		TRACE_EXCEPTION (e) ;
	} catch (exception &e) {
		TRACE_EXCEPTION (e) ;
	} catch (...) {
		LOG_TO_ERROR_STREAM ("unknow exception was caught") ;
	}

//cleanup:
	if (uri_location_dialog) {
		gtk_widget_destroy (GTK_WIDGET (uri_location_dialog));
		uri_location_dialog = NULL ;
	}
	if (uri) {
		g_free (uri);
		uri = NULL;
	}
	if (path) {
		g_free (path) ;
		path = NULL ;
	}
	if (glade_xml) {
		g_object_unref (glade_xml) ;
		glade_xml = NULL ;
	}
	return;
}

/**
 *interactively open/edit a local xml file name.
 *@param a_this the current mlview editor.
 */
void
Editor::open_local_xml_document_interactive ()
{
	gchar *cstr = NULL;
	gint response = 0;

	THROW_IF_FAIL (m_priv) ;

	AppContext *context = AppContext::get_instance () ;
	THROW_IF_FAIL (context) ;
	GtkWidget *file_dialog = GTK_WIDGET (context->get_file_chooser
	                                     (_("Open xml document"), 
										  MLVIEW_FILE_CHOOSER_OPEN_MODE)) ;

	THROW_IF_FAIL (file_dialog != NULL);

	context->sbar_push_message (_("Choose the xml file to open"));

	response = gtk_dialog_run (GTK_DIALOG(file_dialog));
	gtk_widget_hide (file_dialog);

	if (response == GTK_RESPONSE_OK) {
		UString uri, file_name ;
		cstr = gtk_file_chooser_get_filename
		       (GTK_FILE_CHOOSER (file_dialog));
		file_name = (cstr)? cstr : "" ;
		if (cstr) {
			g_free (cstr);
			cstr = NULL;
		}

		cstr = gnome_vfs_get_uri_from_local_path (file_name.c_str ());
		uri = (cstr)? cstr : "" ;
		if (cstr) {
			g_free (cstr);
			cstr = NULL;
		}

		if (file_name != "") {
			if (is_document_opened_in_editor (uri)) {
				GtkWidget *dialog = NULL ;
				gint res =  0 ;
				dialog = build_reload_file_confirmation_dialog () ;
				if (dialog) {
					res = gtk_dialog_run
					      (GTK_DIALOG (dialog)) ;
					switch (res) {
					case GTK_RESPONSE_OK:
						load_xml_file (uri, TRUE);
						break ;
					case GTK_RESPONSE_CANCEL:
						break;
					default:
						g_assert_not_reached () ;
					}
					gtk_widget_destroy (dialog) ;
					dialog = NULL ;
				}
			} else {
				load_xml_file (uri, TRUE) ;
			}
		}

		if (uri != "") {
			EggRecentModel *model = NULL;
			model = (EggRecentModel*)
			        context->get_element ("MlViewRecentModel") ;
			THROW_IF_FAIL (model) ;
			egg_recent_model_add (model, uri.c_str ());
		}
	}
	context->sbar_pop_message () ;
}

/**
 *Setter of the name of the currently selected view.
 *
 *@param a_name the new name of the view.
 */
void
Editor::set_current_view_name (const UString &a_name)
{
	THROW_IF_FAIL (m_priv);

	if (!get_cur_view ())
		return;

	get_cur_view ()->set_view_name (a_name.c_str ());
}

/**
 *Interactively sets the name of the view currently
 *selected in the mlview editor.
 *
 *@param a_this the current mlview editor.
 */
void
Editor::set_current_view_name_interactive ()
{
	THROW_IF_FAIL (m_priv);

	if (!get_cur_view ())
		return;

	get_cur_view ()->set_name_interactive ();
}



/**
 *Edits the xml document given in argument.
 *Actually, this method creates a view on this document and
 *adds the view to the editor.
 *@param a_doc the xml document to edit. This is an
 *object of the libxml2
 *@param a_doc_name the name of the xml document to edit.
 */
void
Editor::edit_xml_document (xmlDocPtr a_doc, const UString &a_doc_name)
{
	IView *doc_view = NULL ;
	MlViewXMLDocument *mlview_xml_doc = NULL;
	ViewDescriptor *view_desc_ptr = NULL ;

	THROW_IF_FAIL (m_priv != NULL);
	THROW_IF_FAIL (a_doc != NULL);

	AppContext *context = AppContext::get_instance () ;
	THROW_IF_FAIL (context) ;

	mlview::PrefsCategoryGeneral *prefs =
	    dynamic_cast<mlview::PrefsCategoryGeneral*> (
		mlview::Preferences::get_instance ()
		->get_category_by_id (mlview::PrefsCategoryGeneral::CATEGORY_ID));
	THROW_IF_FAIL (prefs);

	view_desc_ptr =
	    ViewFactory::peek_editing_view_descriptor
            (prefs->get_default_edition_view ());
	THROW_IF_FAIL (view_desc_ptr) ;

	mlview_xml_doc =
	    mlview_xml_document_new (a_doc) ;

	doc_view = ViewFactory::create_view (mlview_xml_doc,
	                                     view_desc_ptr->view_type_name,
	                                     a_doc_name) ;
	m_priv->view_manager_ptr->insert_view (doc_view) ;
}

/**
 *Interactively saves the underlying xml document of the 
 *currently selected document view.
 *If the document has an associated file name,
 *save it in that file or else, asks the user where to
 *save it.
 */
void
Editor::save_xml_document ()
{
	MlViewXMLDocument *xml_doc = NULL;

	THROW_IF_FAIL (m_priv != NULL);
	if (!get_cur_view ())
		return;

	xml_doc = get_cur_view ()->get_document () ;
	if (xml_doc == NULL)
		return;

	UString file_path =
	    get_current_xml_doc_file_path ();

	if (file_path == "")
		save_xml_document_as_interactive ();
	else
		save_xml_document_as (file_path);

}

/**
 *Classical "save as" functionnality.
 *Saves the underlying document of the currently selected view
 *into the file denoted by the file path a_file_path.
 *
 *@param a_file_path file path where to save the document.
 */
void
Editor::save_xml_document_as (const UString &a_file_path)
{
	MlViewXMLDocument *mlview_xml_document = NULL;
	gboolean file_was_untitled = FALSE;
	UString prev_file_path ;

	THROW_IF_FAIL (m_priv);
	THROW_IF_FAIL (get_cur_view ());
	THROW_IF_FAIL (a_file_path != "") ;

	mlview_xml_document = get_current_document () ;
	THROW_IF_FAIL (mlview_xml_document) ;

	AppContext *context = AppContext::get_instance () ;
	THROW_IF_FAIL (context) ;
	context->sbar_push_message (_("Saving xml document as file %s..."),
	                            a_file_path.c_str ());

	file_was_untitled =
	    (mlview_xml_document_get_file_descriptor (mlview_xml_document)
	     == NULL);

	prev_file_path = mlview_xml_document_get_file_path (mlview_xml_document);

	/*really save the document now */
	if (mlview_xml_document_save (mlview_xml_document,
	                              a_file_path.c_str (), TRUE) > 0
	        && (prev_file_path != ""
	            || a_file_path != prev_file_path)) {
		UString new_file_path ;

		/*
		 *The save was OK and the new file path of this doc
		 * is different from the previous file path of this doc.
		 */

		/*
		 *remove the reference to the previous file path 
		 *of this document.
		 */
		if (prev_file_path != "") {
			m_priv->uri_to_view_map.erase (prev_file_path) ;
		}

		/*
		 *Now, reference the new path of this doc.
		 *We must make sure that the string we put in the
		 *hash table belongs to mlview_xml_document.
		 *That way, one can destroy the hash table without
		 *any fear to leak the memory hold by the string.
		 */
		new_file_path = mlview_xml_document_get_file_path
		                (mlview_xml_document);

		if (new_file_path != "") {
			m_priv->uri_to_view_map.insert
			(StringToViewMap::value_type (new_file_path,
			                              get_cur_view ())) ;
		}
	}

	context->sbar_pop_message () ;
}

/**
 *Interactively saves the underlying document of the currently
 *selected view. Graphically asks the user where to save the
 *document and saves it.
 */
void
Editor::save_xml_document_as_interactive ()
{
	UString file_name ;
	gint response = 0;
	GtkWidget *file_dialog = NULL ;
	;

	AppContext *context = AppContext::get_instance () ;
	THROW_IF_FAIL (context) ;
	file_dialog = GTK_WIDGET (context->get_file_chooser
	                          (_("Save xml document"),
	                           MLVIEW_FILE_CHOOSER_SAVE_MODE)) ;
	THROW_IF_FAIL (file_dialog != NULL);

	context->sbar_push_message (_("Choose where to save the xml file"));

	response = gtk_dialog_run (GTK_DIALOG(file_dialog));
	gtk_window_set_modal (GTK_WINDOW (file_dialog), FALSE);
	gtk_widget_hide (GTK_WIDGET (file_dialog));

	if (response == GTK_RESPONSE_OK) {
		file_name = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (file_dialog));

		if (file_name != "") {
			save_xml_document_as (file_name);
		}

	}
	context->sbar_pop_message () ;
}

/**
 *Getter of the file path of the document 
 *associated to the currently selected view.
 *@param a_this the current mlview editor.
 *@return the file path.
 */
UString
Editor::get_current_xml_doc_file_path () const
{
	MlViewXMLDocument *doc = NULL;
	gchar *cstr = NULL ;
	UString file_path ;

	THROW_IF_FAIL (m_priv) ;

	doc = get_cur_view ()->get_document () ;
	THROW_IF_FAIL (doc) ;
	cstr = mlview_xml_document_get_file_path (doc) ;
	file_path = cstr ;
	if (cstr) {
		g_free (cstr) ;
		cstr = NULL ;
	}
	return file_path ;
}

/**
 *closes the current view without saving the underlying document.
 *@param a_this the current mlview editor.
 */
void
Editor::close_xml_document_without_saving ()
{
	THROW_IF_FAIL (m_priv != NULL);

	m_priv->view_manager_ptr->remove_view (get_cur_view ());

	if (m_priv->view_manager_ptr->get_number_of_open_documents () == 0)
		set_cur_view (NULL) ;
}

/**
 *Saves the underlying document of the currently selected view
 *and closes the view.
 *
 *@param a_this the current mlview editor.
 */
void
Editor::save_and_close_xml_document ()
{
	MlViewFileDescriptor *file_desc = NULL ;
	GtkWidget *file_dialog ;
	gint response = 0 ;
	MlViewXMLDocument *mlview_xml_document = NULL;
	gchar *file_name = NULL, *tmp_str = NULL;

	THROW_IF_FAIL (m_priv != NULL);
	if (!get_cur_view ())
		return;

	mlview_xml_document = get_cur_view ()->get_document () ;
	THROW_IF_FAIL (mlview_xml_document != NULL);
	AppContext *context = AppContext::get_instance () ;
	THROW_IF_FAIL (context) ;

	file_desc = mlview_xml_document_get_file_descriptor
	            (mlview_xml_document) ;
	if (!file_desc) {
		file_dialog = GTK_WIDGET (context->get_file_chooser
		                          (_("Save xml document"), MLVIEW_FILE_CHOOSER_SAVE_MODE)) ;
		THROW_IF_FAIL (file_dialog != NULL);

		context->sbar_push_message (_("Choose where to save the xml file"));

		response = gtk_dialog_run (GTK_DIALOG(file_dialog));

		gtk_window_set_modal (GTK_WINDOW (file_dialog), FALSE);
		gtk_widget_hide (GTK_WIDGET (file_dialog));

		if (response == GTK_RESPONSE_OK) {
			file_name = gtk_file_chooser_get_filename
			            (GTK_FILE_CHOOSER (file_dialog));
		}
		context->sbar_pop_message ();
	} else {
		/*The document already has*/
		tmp_str = mlview_file_descriptor_get_file_path (file_desc);
		THROW_IF_FAIL (tmp_str);
		file_name = g_strdup (tmp_str);
	}
	if (file_name && strcmp (file_name, "")) {
		mlview_xml_document_save (mlview_xml_document, file_name, TRUE);
		close_xml_document_without_saving ();
	}
	if (file_name) {
		g_free (file_name);
		file_name = NULL;
	}
}

bool
Editor::show_new_document_dialog (MlViewNewDocumentDialogData **data)
{
	gint result = -1;
	gboolean status = FALSE;
	GladeXML *gxml   = NULL;
	gchar *gfile  = NULL;
	GtkWidget *docentry = NULL;
	GtkWidget *dialog = NULL;
	GtkWidget *schema_file_entry = NULL;
	GtkWidget *schema_uri_entry = NULL;
	GtkWidget *schema_type_combo = NULL;
	GtkWidget *encoding_combo = NULL;
	GtkWidget *xmlversion_entry = NULL;
	GList *available_encodings = NULL, *p = NULL;
	guint curencoding = 0 ;
	GtkListStore *text = NULL;
	GtkTreeIter iter = { 0 };
	GtkCellRenderer *renderer  = NULL;
	gchar *schema_uri = NULL;
	gint schema_type_index = -1;
	enum MlViewSchemaType schema_type = SCHEMA_TYPE_UNDEF ;

	THROW_IF_FAIL (*data != NULL);

	gfile = gnome_program_locate_file
	        (NULL,
	         GNOME_FILE_DOMAIN_APP_DATADIR,
	         PACKAGE "/mlview-new-document.glade",
	         TRUE, NULL);

	if (!gfile)
		return FALSE;

	gxml = glade_xml_new (gfile, NULL, "NewDocumentDialog");

	g_free (gfile);
	gfile = NULL;

	if (!gxml)
		goto cleanup;

	dialog = glade_xml_get_widget (gxml, "NewDocumentDialog");

	if (!(dialog && GTK_IS_DIALOG (dialog)))
		goto cleanup;

	docentry = glade_xml_get_widget (gxml, "RootNodeNameEntry");

	if (!(docentry && GTK_IS_ENTRY (docentry)))
		goto cleanup;

	schema_file_entry = glade_xml_get_widget (gxml, "SchemaFileEntry");

	if (!(schema_file_entry && GNOME_IS_FILE_ENTRY (schema_file_entry)))
		goto cleanup;

	schema_uri_entry = glade_xml_get_widget (gxml, "SchemaUriEntry");

	if (!(schema_uri_entry && GTK_IS_ENTRY (schema_uri_entry)))
		goto cleanup;

	schema_type_combo = glade_xml_get_widget (gxml, "SchemaTypeCombo");

	if (!(schema_type_combo && GTK_IS_COMBO_BOX (schema_type_combo)))
		goto cleanup;

	text = gtk_list_store_new (1, G_TYPE_STRING);

	if (!text)
		goto cleanup;

	gtk_list_store_append (text, &iter);
	gtk_list_store_set (text, &iter, 0, "Document Type Definition (DTD)", -1);
	gtk_list_store_append (text, &iter);
	gtk_list_store_set (text, &iter, 0, "Relax-NG Schema (RNG)", -1);
	gtk_list_store_append (text, &iter);
	gtk_list_store_set (text, &iter, 0, "XML Schema Definition (XSD)", -1);

	renderer = gtk_cell_renderer_text_new ();

	gtk_cell_layout_pack_start (GTK_CELL_LAYOUT (schema_type_combo),
	                            renderer,
	                            TRUE);

	gtk_cell_layout_set_attributes (GTK_CELL_LAYOUT (schema_type_combo),
	                                renderer,
	                                "text",
	                                0,
	                                NULL);

	gtk_combo_box_set_model (GTK_COMBO_BOX (schema_type_combo),
	                         GTK_TREE_MODEL (text));

	gtk_combo_box_set_active (GTK_COMBO_BOX (schema_type_combo),
	                          0);

	g_object_unref (G_OBJECT (text));
	text = NULL;

	xmlversion_entry = glade_xml_get_widget (gxml, "XMLVersionEntry");

	if (!(xmlversion_entry && GTK_IS_ENTRY (xmlversion_entry)))
		goto cleanup;

	encoding_combo = glade_xml_get_widget (gxml, "EncodingCombo");

	if (!(encoding_combo && GTK_IS_COMBO_BOX (encoding_combo)))
		goto cleanup;

	/* fill encoding combo data */
	available_encodings = mlview_utils_get_available_encodings ();

	THROW_IF_FAIL (available_encodings);

	p = available_encodings;
	while (p != NULL) {
		gtk_combo_box_insert_text (
		    GTK_COMBO_BOX (encoding_combo),
		    ++curencoding,
		    (const gchar*)p->data);

		p = g_list_next (p);
	}

	gtk_combo_box_set_active (GTK_COMBO_BOX (encoding_combo),
	                          0);


	result = gtk_dialog_run (GTK_DIALOG (dialog));
	switch (result) {
	case GTK_RESPONSE_ACCEPT:
		/* fill data */
		(*data)->root_node_name = (gchar*)g_strdup (
		                              gtk_entry_get_text (GTK_ENTRY (docentry)));
		(*data)->xml_version = (gchar*)g_strdup (
		                           gtk_entry_get_text (GTK_ENTRY (xmlversion_entry)));

#ifdef GTK_2_6_SERIE_OR_ABOVE

		(*data)->encoding = (gchar*)g_strdup (
		                        gtk_combo_box_get_active_text
		                        (GTK_COMBO_BOX (encoding_combo)));
#else

		(*data)->encoding = (gchar*)g_strdup (
		                        mlview_utils_combo_box_get_active_text
		                        (GTK_COMBO_BOX (encoding_combo)));
#endif


		schema_type_index = gtk_combo_box_get_active
		                    (GTK_COMBO_BOX (schema_type_combo));
		schema_uri = (gchar*) gtk_entry_get_text
		             (GTK_ENTRY (schema_uri_entry));

		if (schema_uri && (strcmp (schema_uri,"") != 0)) {
			g_warning ("SCHEMA SPECIFIED");
			if (schema_type_index == -1)
				return FALSE;

			switch (schema_type_index) {
			case 0:
				schema_type = SCHEMA_TYPE_DTD;
				break;
			case 1:
				schema_type = SCHEMA_TYPE_RNG;
				break;
			case 2:
				schema_type = SCHEMA_TYPE_XSD;
				break;
			}

			(*data)->schema = mlview_schema_load_from_file (schema_uri,
			                  schema_type);

			/* add schema to combo history */
			if ((*data)->schema)
				gnome_entry_prepend_history (
				    GNOME_ENTRY
				    (gnome_file_entry_gnome_entry (
				         GNOME_FILE_ENTRY (schema_file_entry))),
				    TRUE,
				    schema_uri);
		}

		status = TRUE;
		break;
	default:
		status = FALSE;
		break;
	}


cleanup:
	if (gxml)
		g_object_unref (gxml);

	if (dialog)
		gtk_widget_destroy (GTK_WIDGET (dialog));

	return status;
}
/**
 *Interactively create a new document.
 *Asks the user for info like 
 *root element, dtd (if validation is switched on) ...etc.
 */
void
Editor::create_new_xml_document ()
{
	MlViewXMLDocument *mlview_doc = NULL;
	xmlDocPtr xml_doc = NULL;
	xmlNodePtr xml_node = NULL;
	IView *view = NULL ;
	ViewDescriptor *view_desc_ptr = NULL ;

	gboolean loop = TRUE;
	gchar *utf8_elname = NULL;
	MlViewNewDocumentDialogData *newdoc_dialog_data = NULL;

	THROW_IF_FAIL (m_priv != NULL);

	AppContext *context = AppContext::get_instance () ;
	THROW_IF_FAIL (context) ;

	mlview::PrefsCategoryGeneral *prefs =
	    dynamic_cast<mlview::PrefsCategoryGeneral*> (
		mlview::Preferences::get_instance ()
		->get_category_by_id (mlview::PrefsCategoryGeneral::CATEGORY_ID));
	THROW_IF_FAIL (prefs);

	view_desc_ptr =
	    ViewFactory::peek_editing_view_descriptor
	    (prefs->get_default_edition_view ());

	if (!view_desc_ptr) {
		view_desc_ptr =
		ViewFactory::peek_editing_view_descriptor ("source-view") ;
	}
	THROW_IF_FAIL (view_desc_ptr) ;

	newdoc_dialog_data =
	    (MlViewNewDocumentDialogData*)g_try_malloc
	    (sizeof(MlViewNewDocumentDialogData));

	newdoc_dialog_data = (MlViewNewDocumentDialogData*)memset(
	    (void*)newdoc_dialog_data,
	    0,
	    sizeof(MlViewNewDocumentDialogData));

	THROW_IF_FAIL (newdoc_dialog_data);

	while (loop) {
		gboolean res = FALSE;
		enum MlViewStatus status = MLVIEW_OK;
		gchar *name_end = NULL, *parsed_name = NULL;
		gulong len = 0 ;

		res = show_new_document_dialog (&newdoc_dialog_data);

		utf8_elname = newdoc_dialog_data->root_node_name;

		/*the user hit cancel */
		if (!res)
			break;
		/*empty root name: show the dialog again */
		if (!newdoc_dialog_data->root_node_name)
			continue;

		if (mlview_utils_is_white_string ((gchar*)utf8_elname) == TRUE)
			continue;

		/*check if is a valid element name */
		status = mlview_utils_parse_element_name
		         ((gchar*)utf8_elname, &name_end);
		if (status != MLVIEW_OK || !name_end) {
			context->error (_("The string entered is "
			                  "not a well formed element name!"));
			continue;
		}
		len = (gulong)name_end - (gulong)utf8_elname + 1;
		parsed_name = (gchar*)g_strndup (utf8_elname, len) ;
		if (!parsed_name) {
			context->error (_("The string entered is "
			                  "not a well formed element name!"));
			continue ;
		}
		xml_node = xmlNewNode (NULL, (xmlChar*)parsed_name);
		if (parsed_name) {
			g_free (parsed_name) ;
			parsed_name = NULL ;
		}
		xml_doc = xmlNewDoc ((const xmlChar*)"1.0");
		xml_doc->name = g_strdup ("Untitled Document");
		xmlDocSetRootElement (xml_doc, xml_node);
		xml_doc->version  = xmlCharStrdup
		                    ((const char*)newdoc_dialog_data->xml_version);
		xml_doc->encoding = xmlCharStrdup
		                    ((const char*)newdoc_dialog_data->encoding);

		mlview_doc = mlview_xml_document_new (xml_doc);
		THROW_IF_FAIL (mlview_doc != NULL);

		/* add the schema (if specified) to the document's schema list */
		if (newdoc_dialog_data->schema) {
			mlview_schema_list_add_schema (
			    mlview_xml_document_get_schema_list (mlview_doc),
			    newdoc_dialog_data->schema);
		}

		/*validation */
		if (prefs->use_validation () == TRUE) {
			if (xml_node->type == XML_ELEMENT_NODE) {
				mlview_parsing_utils_build_required_attributes_list (xml_node);
				mlview_parsing_utils_build_required_children_tree (&xml_node);
			}
		}
		view = ViewFactory::create_view
		       (mlview_doc, view_desc_ptr->view_type_name, "") ;
		if (!view) {
			LOG_TO_ERROR_STREAM ("view instanciation failed") ;
			return ;
		}
		m_priv->view_manager_ptr->insert_view (view);
		/*everything went fine */
		break;
	}
}

/**
 *closes the current view without saving the underlying document.
 *@param a_this the current mlview editor.
 *@return true if the close process has been canceled.
 */
bool
Editor::close_xml_document (gboolean a_interactive)
{
	THROW_IF_FAIL (m_priv != NULL);

	bool result = false ;
	if (!a_interactive) {
		m_priv->view_manager_ptr->remove_view (get_cur_view ());

		if (m_priv->view_manager_ptr->get_number_of_open_documents () == 0)
			m_priv->view_manager_ptr->set_cur_view (NULL) ;
	} else {
		MlViewXMLDocument *doc = NULL ;
		if (!get_cur_view ())
			return false;
		doc = get_cur_view ()->get_document () ;
		if (!doc) {
			mlview_utils_trace_debug ("The current view has no "
			                          "associated document. "
			                          "This is truly weird, "
			                          "something bad is happening") ;
			return false;
		}
		if (get_number_of_views_opened_with_doc (doc)> 1
		        || mlview_xml_document_needs_saving (doc) == FALSE) {
			close_xml_document_without_saving () ;
		} else {
			result = confirm_close () ;
		}
	}
	return result ;
}

/**
 *Closes all the views (and their underlying documents).
 *For document that needs to be saved, this function 
 *popups a dialog asking the user if she wants
 *to save the document before closing, closing without saving
 *or cancel. If she cancels, then the function just returns FALSE
 *and the processing is stopped.
 *
 *@return TRUE if all the document views have been closed, FALSE otherwise.
 */
bool
Editor::close_all_xml_documents (gboolean a_interactive)
{
	THROW_IF_FAIL (m_priv != NULL) ;

	list<IView*>::iterator view_iterator ;
	list<IView*> views =  m_priv->view_manager_ptr->get_all_views () ;
	for (view_iterator = views.begin () ;
		 view_iterator != views.end ();
		 ++view_iterator) {
		set_cur_view (*view_iterator) ;
		close_xml_document (a_interactive) ;
	}
	if (m_priv->view_manager_ptr->get_number_of_open_documents () == 0)
		return true ;
	else
		return false ;
}

/**
 *Interactively edits the editor settings.
 *
 *@param a_this the current mlview editor.
 */
void
Editor::edit_settings_interactive ()
{
	THROW_IF_FAIL (m_priv) ;
}

/**
 *Shows the tool used to manage the schemas associated with an
 *XML document.
 *
 *@param a_this the current mlview editor.
 *@return 0 if association worked, 1 if not xml document is opened, 
 */
gint
Editor::manage_associated_schemas ()
{
	MlViewXMLDocument *mlview_xml_doc = NULL;

	THROW_IF_FAIL (m_priv != NULL);

	if (!get_cur_view ())
		return 1;

	mlview_xml_doc = get_cur_view ()->get_document () ;
	if (mlview_xml_doc == NULL)
		return 1;

	show_schemas_window_for_doc (mlview_xml_doc);

	return 0;
}

/**
 *validate the current selected document against the dtd it is associated to.
 */
void
Editor::validate ()
{
	MlViewXMLDocument *doc = NULL;

	THROW_IF_FAIL (m_priv != NULL);

	if (!get_cur_view ())
		return;

	doc = get_cur_view ()->get_document () ;

	if (doc == NULL)
		return;

	show_validation_window_for_doc (doc);
}



/**
 *Execute an editing editing action.
 *@param a_this the current instance of #MlViewEditor
 *@param a_action the action to execute.
 *@return MLVIEW_OK upon successful completion an error code
 *otherwise.
 */
enum MlViewStatus
Editor::execute_action (MlViewAction *a_action)
{
	THROW_IF_FAIL (m_priv) ;

	if (get_cur_view ()) {
		get_cur_view ()->execute_action (*a_action) ;
		return MLVIEW_OK ;
	}
	return MLVIEW_ERROR ;
}

/**
 *
 *Shows a file chooser dialog to select an external stylesheet
 *Loads the file selected, and performs a xslt namespace check
 *@param a_this : mlview editor context
 *@return the document selected or NULL if user cancels or 
 *document is not an xslt stylesheet
 * 
 */
MlViewXMLDocument*
Editor::choose_and_open_stylesheet ()
{
	gchar *file_name = NULL;
	MlViewXMLDocument *mlv_xsl_doc = NULL;
	gint response = 0;
	GtkWidget *file_dialog = NULL;

	AppContext *app_context = AppContext::get_instance ();
	THROW_IF_FAIL (app_context) ;
	file_dialog = GTK_WIDGET
	              (app_context->get_file_chooser (_("Open an xslt stylesheet"),
												  MLVIEW_FILE_CHOOSER_OPEN_MODE));

	THROW_IF_FAIL (file_dialog != NULL);

	app_context->sbar_push_message (_("Choose the xslt file to open"));
	response = gtk_dialog_run (GTK_DIALOG(file_dialog)) ;
	gtk_window_set_modal (GTK_WINDOW (file_dialog), FALSE) ;
	gtk_widget_hide (GTK_WIDGET (file_dialog)) ;

	if (response == GTK_RESPONSE_OK) {
		file_name = gtk_file_chooser_get_filename
		            (GTK_FILE_CHOOSER (file_dialog));

		mlv_xsl_doc = mlview_xml_document_open_with_dtd_interactive (file_name);

		if (!mlv_xsl_doc
		        || !xslt_utils_is_xslt_doc(mlv_xsl_doc)) {
			/*display error box and close doc*/
			mlview_utils_display_error_dialog
			("%s",
			 _("document is not an XSLT Stylesheet"));
			if (mlv_xsl_doc) {
				mlview_xml_document_unref (mlv_xsl_doc) ;
				mlv_xsl_doc=NULL;
			}
		} else {
			mlview_xml_document_ref (mlv_xsl_doc) ;
		}
		if (file_name) {
			g_free (file_name);
			file_name = NULL;
		}
	}
	app_context->sbar_pop_message () ;

	return mlv_xsl_doc;
}

/**
 *Shows the xslt chooser dialog
 *@param a_editor : mlview editor context
 *@return the document selected or NULL if user cancels or 
 *document is not an xslt stylesheet
 */
MlViewXMLDocument*
Editor::select_xsl_doc ()
{
	GtkWidget *dialog1 = NULL, *dialog_vbox1 = NULL,
	                     *hbox1 = NULL, *label1 = NULL, *menu = NULL,
	                                                            *option_menu = NULL, *menu_item = NULL,
	                                                                                              *sel_menu_item = NULL,  *dialog_action_area1 = NULL,
	                                                                                                               *button4 = NULL,  *button5 = NULL, *button6 = NULL ;
	MlViewXMLDocument *mlv_xsl_doc=NULL;
	UString file_path, base_name ;
	gint result;

	THROW_IF_FAIL (m_priv);

	/*builds a GList of opened XSLT docs*/

	list<MlViewXMLDocument*>::iterator it ;
	list<MlViewXMLDocument*> xslt_docs ;
	list<MlViewXMLDocument*> open_docs =
	    m_priv->view_manager_ptr->get_list_of_open_documents ();
	for (it = open_docs.begin () ;
	        it != open_docs.end () ;
	        ++ it ) {
		if (xslt_utils_is_xslt_doc (*it)) {
			xslt_docs.push_back (*it) ;
		}
	}

	/* build dialog window */

	dialog1 = gtk_dialog_new ();
	gtk_window_set_title (GTK_WINDOW (dialog1), _("Select XSLT"));

	dialog_vbox1 = GTK_DIALOG (dialog1)->vbox;
	gtk_widget_show (dialog_vbox1);

	hbox1 = gtk_hbox_new (FALSE, 0);
	gtk_widget_show (hbox1);
	gtk_box_pack_start (GTK_BOX (dialog_vbox1), hbox1, TRUE, TRUE, 0);

	if (xslt_docs.size () != 0) {
		label1 = gtk_label_new (_("Select xslt stylesheet"));
	} else {
		label1 = gtk_label_new (_("No xslt stylesheet is open"));
	}
	gtk_widget_show (label1);
	gtk_box_pack_start (GTK_BOX (hbox1), label1, FALSE, FALSE, 10);

	/* build select xslt menu */

	if (xslt_docs.size () != 0) {
		option_menu = gtk_option_menu_new();
		menu = gtk_menu_new();
		gtk_option_menu_set_menu (GTK_OPTION_MENU(option_menu), menu);

		gtk_widget_show (menu);
		gtk_widget_show (option_menu);

		gtk_box_pack_start (GTK_BOX (hbox1), option_menu, TRUE, TRUE, 0);

		list<MlViewXMLDocument*>::iterator it2 ;
		for (it2 = xslt_docs.begin ();
		        it2 != xslt_docs.end () ;
		        ++it2 ) {
			file_path = mlview_xml_document_get_file_path
			            (*it2);
			base_name = (gchar *) g_basename
			            (file_path.c_str ());
			menu_item = gtk_menu_item_new_with_label
			            (base_name.c_str ());
			gtk_menu_shell_append (GTK_MENU_SHELL(menu),
			                       menu_item);
			gtk_widget_show (menu_item);
			g_object_set_data (G_OBJECT(menu_item),
			                   "mlview_doc", *it2);
		}
		gtk_option_menu_set_history (GTK_OPTION_MENU (option_menu), 0);
	}

	/** end menu **/

	dialog_action_area1 = GTK_DIALOG (dialog1)->action_area;
	gtk_widget_show (dialog_action_area1);
	gtk_button_box_set_layout (GTK_BUTTON_BOX (dialog_action_area1),
	                           GTK_BUTTONBOX_END);

	button4 = gtk_button_new_with_mnemonic (_("Browse..."));
	gtk_widget_show (button4);
	gtk_dialog_add_action_widget (GTK_DIALOG (dialog1),
	                              button4,
	                              MLVIEW_XSLT_UTILS_RESPONSE_BROWSE);
	GTK_WIDGET_SET_FLAGS (button4, GTK_CAN_DEFAULT);

	button5 = gtk_button_new_from_stock ("gtk-cancel");
	gtk_widget_show (button5);
	gtk_dialog_add_action_widget (GTK_DIALOG (dialog1),
	                              button5, GTK_RESPONSE_CANCEL);
	GTK_WIDGET_SET_FLAGS (button5,
	                      GTK_CAN_DEFAULT);

	if (xslt_docs.size () != 0) {
		button6 = gtk_button_new_from_stock ("gtk-ok");
		gtk_widget_show (button6);
		gtk_dialog_add_action_widget (GTK_DIALOG (dialog1),
		                              button6, GTK_RESPONSE_OK);
		GTK_WIDGET_SET_FLAGS (button6, GTK_CAN_DEFAULT);
	}

	/* runs the dialog box */
	result = gtk_dialog_run (GTK_DIALOG (dialog1));
	switch (result) {
	case GTK_RESPONSE_OK:
		sel_menu_item = gtk_menu_get_active(GTK_MENU(menu));
		mlv_xsl_doc = (MlViewXMLDocument*) g_object_get_data
		              (G_OBJECT(sel_menu_item), "mlview_doc");
		break;
	case GTK_RESPONSE_CANCEL:
		break;
	case MLVIEW_XSLT_UTILS_RESPONSE_BROWSE:
		mlv_xsl_doc = choose_and_open_stylesheet () ;
		break;
	}

	gtk_widget_destroy (dialog1);

	return mlv_xsl_doc;
}

/**
 *Applies a XSLT stylesheet to the current document
 *displaying a dialog for xslt selection and opens
 *the transformation resulting document in the editor
 *@param a_this the current instance of #MlViewEditor.
 */
void
Editor::xslt_transform_document_interactive ()
{
	MlViewXMLDocument *src_doc = NULL;
	MlViewXMLDocument *xsl_doc = NULL;
	MlViewXMLDocument *res_doc = NULL;
	IView *view = NULL ;

	THROW_IF_FAIL (m_priv != NULL);

	src_doc = get_current_document ();
	xsl_doc = select_xsl_doc ();
	if (xsl_doc != NULL) {
		res_doc = xslt_utils_transform_document (src_doc, xsl_doc);
		mlview_xml_document_unref (xsl_doc);
		if (res_doc != NULL)  {
			view = create_new_view_on_document (res_doc);
			if (view) {
				m_priv->view_manager_ptr->insert_view (view) ;
			}
		}
	}
}

/**
 *Returns the number of views opened with the document, or
 *-1 if an error occurs.
 *@param a_doc the XML document.
 *@return the corresponding number of views.
 */
gint
Editor::get_number_of_views_opened_with_doc (MlViewXMLDocument *a_doc) const
{
	THROW_IF_FAIL (m_priv) ;

	return m_priv->view_manager_ptr->get_number_of_views_opened_with_doc
	       (a_doc) ;

}

GtkWidget *
Editor::show_schemas_window_for_doc (MlViewXMLDocument *a_doc)
{
	GtkWidget *win = NULL;
	struct DocumentWindowData *data = NULL;

	THROW_IF_FAIL (a_doc && MLVIEW_IS_XML_DOCUMENT (a_doc));
	THROW_IF_FAIL (m_priv);

	DocToWindowDataMap::iterator it ;
	it = m_priv->doc_to_schema_window_data_map.find (a_doc) ;
	if (it != m_priv->doc_to_schema_window_data_map.end ())
		data = it->second ;

	if (data) {
		THROW_IF_FAIL (data->window);
		THROW_IF_FAIL (GTK_IS_WIDGET (data->window));

		gtk_widget_hide (data->window);
		gtk_widget_show (data->window);

		return data->window;
	}

	win = mlview_schemas_window_new_with_document (a_doc);

	data = (struct DocumentWindowData*)
	       g_try_malloc (sizeof (struct DocumentWindowData));

	if (!data) {
		gtk_widget_destroy (win);

		return NULL;
	}

	memset (data, 0, sizeof (struct DocumentWindowData));

	data->document = a_doc;
	data->editor = this;
	data->window = win;

	g_signal_connect (G_OBJECT (win), "destroy",
	                  G_CALLBACK (EditorPriv::schemas_window_destroy_cb),
	                  data);

	m_priv->doc_to_schema_window_data_map.erase (a_doc) ;
	m_priv->doc_to_schema_window_data_map.insert
	(DocToWindowDataMap::value_type (a_doc, data)) ;

	gtk_widget_show_all (win);

	return win;
}

GtkWidget *
Editor::show_validation_window_for_doc (MlViewXMLDocument *a_doc)
{
	GtkWidget *win = NULL;
	struct DocumentWindowData *data = NULL;
	DocToWindowDataMap::iterator it ;

	THROW_IF_FAIL (a_doc && MLVIEW_IS_XML_DOCUMENT (a_doc));
	THROW_IF_FAIL (m_priv);

	it = m_priv->doc_to_validation_window_data_map.find (a_doc) ;
	if (it != m_priv->doc_to_validation_window_data_map.end ())
		data = it->second ;

	if (data) {
		THROW_IF_FAIL (data->window);
		THROW_IF_FAIL (GTK_IS_WIDGET (data->window));

		gtk_widget_hide (data->window);
		gtk_widget_show (data->window);

		return data->window;
	}

	win = mlview_validator_window_new (a_doc);

	data = (struct DocumentWindowData*)g_try_malloc
	       (sizeof (struct DocumentWindowData));

	if (!data) {
		gtk_widget_destroy (win);

		return NULL;
	}

	memset (data, 0, sizeof (struct DocumentWindowData));

	data->document = a_doc;
	data->editor = this;
	data->window = win;

	g_signal_connect (G_OBJECT (win), "destroy",
	                  G_CALLBACK (EditorPriv::validation_window_destroy_cb),
	                  data);

	m_priv->doc_to_validation_window_data_map.erase (a_doc) ;
	m_priv->doc_to_validation_window_data_map.insert
	(DocToWindowDataMap::value_type (a_doc, data)) ;

	gtk_widget_show_all (win);

	return win;

}

enum MlViewStatus
Editor::make_current_view_populate_application_edit_menu ()
{
	IView *cur_view = NULL ;

	THROW_IF_FAIL (m_priv) ;

	cur_view = get_cur_view () ;
	if (!cur_view)
		return MLVIEW_OK ;
	cur_view->request_application_menu_populating () ;

	return MLVIEW_OK ;
}

bool
Editor::is_document_opened_in_editor (const UString &a_doc_uri) const
{
	THROW_IF_FAIL (m_priv) ;

	if (m_priv->uri_to_view_map.find (a_doc_uri)
	        != m_priv->uri_to_view_map.end ()) {
		return true ;
	} else {
		return false ;
	}
}

/**
 * Undo the last editing action performed on the current
 * editing view
 * @return MLVIEW_OK upon successful completion, an error code otherwise
 */
enum MlViewStatus
Editor::undo ()
{
	IView *cur_view = NULL ;

	THROW_IF_FAIL (m_priv) ;

	cur_view = get_cur_view () ;

	if (!cur_view) {
		mlview_utils_trace_debug ("No current selected view found") ;
		return MLVIEW_ERROR;
	}
	return cur_view->undo () ;
}

/**
 * Redo the last undo'ed editing action performed in the current
 * editing view
 * @return MLVIEW_OK upon successful completion, an error code otherwise
 */
enum MlViewStatus
Editor::redo ()
{
	IView *cur_view = NULL ;

	THROW_IF_FAIL (m_priv) ;

	cur_view = get_cur_view () ;

	if (!cur_view) {
		mlview_utils_trace_debug ("No current selected view found") ;
		return MLVIEW_ERROR;
	}

	return cur_view->redo ();
}

/**
 * Checks wether the last editing action performed on the current editing
 * view can be undo'ed
 * @return TRUE if the action can be undo'ed, FALSE otherwise
 */
bool
Editor::can_undo ()
{
	IView *cur_view = NULL ;

	THROW_IF_FAIL (m_priv) ;

	cur_view = get_cur_view () ;

	if (!cur_view) {
		return FALSE;
	}
	return cur_view->can_undo () ;
}

/**
 * Checks if the last editing action performed on the current editing
 * view can be undo'ed or not.
 * @return TRUE if the action can be undo'ed, FALSE otherwise
 */
bool
Editor::can_redo ()
{
	IView *cur_view = NULL ;

	THROW_IF_FAIL (m_priv) ;

	cur_view = get_cur_view () ;

	if (!cur_view) {
		return FALSE;
	}
	return cur_view->can_redo () ;
}

//****************
// signal handlers
//****************
sigc::signal0<void>
Editor::signal_document_changed ()
{
	THROW_IF_FAIL (m_priv) ;
	return m_priv->signal_document_changed ;
}

END_NAMESPACE_MLVIEW
