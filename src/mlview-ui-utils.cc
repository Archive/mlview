/*
  <one line to give the program's name and a brief idea of what it does.>
  Copyright (C) <year>  <name of author>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include <math.h>
#include "mlview-ui-utils.h"

namespace mlview
{

static unsigned int color_scale (int color);


///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

Glib::ustring
gdk_color_to_html_string (const Gdk::Color color)
{
    gchar colstr[12];

    g_snprintf (colstr, 11, "#%02X%02X%02X",
		color_scale (color.get_red ()),
		color_scale (color.get_green ()),
		color_scale (color.get_blue ()));

    return Glib::ustring (colstr);
}

Glib::ustring
gdk_color_to_html_string (const GdkColor color)
{
    gchar colstr[12];

    g_snprintf (colstr, 11, "#%02X%02X%02X",
		color_scale (color.red),
		color_scale (color.green),
		color_scale (color.blue));

    return Glib::ustring (colstr);
}

static unsigned int
color_scale (int color)
{
    const int factor = 255;
    double value = color / 65535.;

    value = floor (value * factor + 0.5);
    value = MAX (value, 0);
    value = MIN (value, factor);

    return static_cast<unsigned int>(value);
}

} // namespace mlview
