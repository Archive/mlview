/* -*- Mode: C; indent-tabs-mode:nil; c-basic-offset: 8-*- */

/*
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute 
 *it and/or modify it under the terms of 
 *the GNU General Public License as published by the 
 *Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */

#include <string.h>
#include "mlview-editor-dbo.h"
#include "mlview-editor.h"

#ifdef MLVIEW_WITH_DBUS

#define PRIVATE(obj) (obj)->priv

static void mlview_editor_dbo_class_init (MlViewEditorDBOClass *a_klass) ;

static void mlview_editor_dbo_init (MlViewEditorDBO *a_this) ;

static void mlview_editor_dbo_dispose (GObject *a_object) ;

static void mlview_editor_dbo_finalize (GObject *a_object) ;

static void mlview_editor_dbo_idbo_init (MlViewIDBO *a_this) ;

static DBusHandlerResult mlview_editor_iface_load_xml_file_with_dtd
(DBusConnection *a_conn,
 DBusMessage *a_message,
 MlViewEditorDBO *a_this) ;

static DBusHandlerResult message_handler (DBusConnection *a_connection,
        DBusMessage *a_message,
        void *a_data) ;

static void message_unregister_handler (DBusConnection *a_connection,
                                        void *a_data) ;

static enum MlViewStatus get_app_context (MlViewEditorDBO *a_this,
        MlViewAppContext **a_ctxt) ;

static enum MlViewStatus get_editor (MlViewEditorDBO *a_this,
                                     MlViewEditor **a_editor) ;

static enum MlViewStatus mlview_editor_dbo_construct (MlViewEditorDBO *a_this,
        MlViewAppContext *a_ctxt) ;
static GObjectClass *gv_parent_class = NULL ;

struct _MlViewEditorDBOPriv
{
	MlViewAppContext *app_context ;
	gboolean dispose_has_run ;
} ;

static void
mlview_editor_dbo_class_init (MlViewEditorDBOClass *a_klass)
{
	GObjectClass *gobject_class = NULL ;

	g_return_if_fail (a_klass != NULL) ;
	gv_parent_class = g_type_class_peek_parent (a_klass) ;
	g_return_if_fail (gv_parent_class) ;
	gobject_class = G_OBJECT_CLASS (a_klass) ;

	gobject_class->dispose = mlview_editor_dbo_dispose ;
	gobject_class->finalize = mlview_editor_dbo_finalize ;
}

static void
mlview_editor_dbo_init (MlViewEditorDBO *a_this)
{
	g_return_if_fail (a_this && MLVIEW_IS_EDITOR_DBO (a_this)) ;
	g_return_if_fail (MLVIEW_IS_IDBO (a_this)) ;

	PRIVATE (a_this) = g_try_malloc (sizeof (MlViewEditorDBOPriv)) ;
	if (!PRIVATE (a_this)) {
		mlview_utils_trace_debug ("malloc failed") ;
		return ;
	}
	memset (PRIVATE (a_this), 0, sizeof (MlViewEditorDBOPriv)) ;
}

static void
mlview_editor_dbo_idbo_init (MlViewIDBO *a_this)
{
	g_return_if_fail (a_this) ;

	a_this->message_handler = message_handler ;
	a_this->message_unregister_handler = message_unregister_handler ;
}

static void
mlview_editor_dbo_dispose (GObject *a_object)
{
	MlViewEditorDBO *thiz = NULL ;
	g_return_if_fail (a_object) ;

	thiz = MLVIEW_EDITOR_DBO (a_object) ;
	g_return_if_fail (thiz && PRIVATE (thiz)) ;

	if (PRIVATE (thiz)->dispose_has_run == TRUE) {
		return ;
	}

	if (gv_parent_class->dispose) {
		gv_parent_class->dispose (a_object) ;
	}
	PRIVATE (thiz)->dispose_has_run = TRUE ;
}

static void
mlview_editor_dbo_finalize (GObject *a_object)
{
	MlViewEditorDBO *thiz = NULL ;
	g_return_if_fail (a_object) ;

	thiz = MLVIEW_EDITOR_DBO (a_object) ;
	g_return_if_fail (thiz) ;

	if (PRIVATE (thiz)) {
		g_free (PRIVATE (thiz)) ;
		PRIVATE (thiz) = NULL;
	}

	if (gv_parent_class->finalize) {
		gv_parent_class->finalize (a_object) ;
	}
}

static DBusHandlerResult
mlview_editor_iface_load_xml_file_with_dtd (DBusConnection *a_conn,
        DBusMessage *a_message,
        MlViewEditorDBO *a_this)
{
	DBusHandlerResult dbus_result = DBUS_HANDLER_RESULT_NOT_YET_HANDLED ;
	DBusError dbus_error = {0} ;
	DBusMessage *reply = NULL ;
	gchar *doc_uri = NULL, *dtd_uri = NULL ;
	enum MlViewStatus status = MLVIEW_OK ;

	g_return_val_if_fail (a_conn && a_message && a_this,
	                      DBUS_HANDLER_RESULT_NOT_YET_HANDLED) ;

	dbus_error_init (&dbus_error) ;

	if (!dbus_message_get_args (a_message, &dbus_error,
	                            DBUS_TYPE_STRING, &doc_uri,
	                            DBUS_TYPE_STRING, &dtd_uri,
	                            DBUS_TYPE_INVALID))  {

		mlview_utils_trace_debug ("could not get message args") ;
		dbus_result = DBUS_HANDLER_RESULT_NOT_YET_HANDLED ;
		goto cleanup ;
	}

	status = mlview_editor_dbo_load_xml_file_with_dtd (a_this,
	         doc_uri,
	         dtd_uri) ;
	reply = dbus_message_new_method_return (a_message) ;
	if (!reply) {
		status = DBUS_HANDLER_RESULT_NOT_YET_HANDLED ;
		goto cleanup ;
	}

	dbus_message_append_args (reply,
	                          DBUS_TYPE_INT32, status,
	                          DBUS_TYPE_INVALID) ;

	if (!dbus_connection_send (a_conn, reply, NULL)) {
		mlview_utils_trace_debug ("send message failed") ;
		status = DBUS_HANDLER_RESULT_NOT_YET_HANDLED ;
		goto cleanup ;
	}

	status = DBUS_HANDLER_RESULT_HANDLED ;

cleanup:
	if (doc_uri) {
		dbus_free (doc_uri) ;
		doc_uri = NULL ;
	}
	if (dtd_uri) {
		dbus_free (dtd_uri) ;
		dtd_uri = NULL ;
	}
	if (reply) {
		dbus_message_unref (reply) ;
		reply = NULL ;
	}
	return status ;
}

static DBusHandlerResult
message_handler (DBusConnection *a_connection,
                 DBusMessage *a_message,
                 void *a_data)
{
	DBusHandlerResult dbus_result =
	    DBUS_HANDLER_RESULT_NOT_YET_HANDLED ;

	g_return_val_if_fail (a_connection && a_message,
	                      DBUS_HANDLER_RESULT_NOT_YET_HANDLED) ;

	mlview_utils_trace_debug
	("hit message handler MlViewEditorDBO::message_handler") ;

	if (dbus_message_is_method_call (a_message,
	                                 INTERFACE_ORG_MLVIEW_EDITOR_IFACE,
	                                 "load_xml_file_with_dtd")) {

		mlview_utils_trace_debug ("method \"load_xml_file_with_dtd\" "
		                          "of interface %s is called\n",
		                          INTERFACE_ORG_MLVIEW_EDITOR_IFACE) ;

		return mlview_editor_iface_load_xml_file_with_dtd
		       (a_connection, a_message, a_data) ;
	}
	return dbus_result ;
}

static void
message_unregister_handler (DBusConnection *a_connection,
                            void *a_data)
{
	g_return_if_fail (a_connection && a_data) ;

	mlview_utils_trace_debug ("Unregistered handler") ;
}


static enum MlViewStatus
get_app_context (MlViewEditorDBO *a_this,
                 MlViewAppContext **a_ctxt)
{
	g_return_val_if_fail (a_this && MLVIEW_IS_EDITOR_DBO (a_this)
	                      && PRIVATE (a_this),
	                      MLVIEW_BAD_PARAM_ERROR) ;

	*a_ctxt = PRIVATE (a_this)->app_context ;
	return MLVIEW_OK ;

}

static enum MlViewStatus
get_editor (MlViewEditorDBO *a_this,
            MlViewEditor **a_editor)
{
	enum MlViewStatus status = MLVIEW_OK ;
	MlViewAppContext *ctxt = NULL ;

	status = get_app_context (a_this, &ctxt) ;
	g_return_val_if_fail (status == MLVIEW_OK && ctxt,
	                      MLVIEW_ERROR) ;

	*a_editor = mlview_app_context_get_element (ctxt, "MlViewEditor") ;
	return MLVIEW_OK ;
}

static enum MlViewStatus
mlview_editor_dbo_construct (MlViewEditorDBO *a_this,
                             MlViewAppContext *a_ctxt)
{
	g_return_val_if_fail (a_this && MLVIEW_IS_EDITOR_DBO (a_this)
	                      && PRIVATE (a_this),
	                      MLVIEW_BAD_PARAM_ERROR) ;

	PRIVATE (a_this)->app_context = a_ctxt ;
	return MLVIEW_OK ;
}
/*************************
 * public methods
 ************************/

GType
mlview_editor_dbo_get_type (void)
{
	static GType type = 0 ;

	if (!type) {
		static const GTypeInfo type_info = {
		                                       sizeof (MlViewEditorDBOClass),
		                                       NULL, NULL,
		                                       (GClassInitFunc) mlview_editor_dbo_class_init,
		                                       NULL, NULL,
		                                       sizeof (MlViewEditorDBO),
		                                       0,
		                                       (GInstanceInitFunc) mlview_editor_dbo_init
		                                   } ;
		type = g_type_register_static (G_TYPE_OBJECT,
		                               "MlViewEditorDBO",
		                               &type_info, 0) ;

		static const GInterfaceInfo idbo_info = {
		                                            (GInterfaceInitFunc) mlview_editor_dbo_idbo_init,
		                                            NULL, NULL
		                                        } ;
		g_type_add_interface_static (type, MLVIEW_TYPE_IDBO,
		                             &idbo_info) ;
	}
	return type ;
}

MlViewEditorDBO *
mlview_editor_dbo_new (MlViewAppContext *a_ctxt)
{
	MlViewEditorDBO *editor = NULL ;

	editor = g_object_new (MLVIEW_TYPE_EDITOR_DBO, NULL) ;
	mlview_editor_dbo_construct (editor, a_ctxt) ;
	return editor ;
}

enum MlViewStatus
mlview_editor_dbo_load_xml_file_with_dtd (MlViewEditorDBO *a_this,
        const char *a_doc_uri,
        const char *a_dtd_uri)
{
	enum MlViewStatus status = MLVIEW_OK ;
	MlViewEditor *editor = NULL ;

	status = get_editor (a_this, &editor) ;
	g_return_val_if_fail (status == MLVIEW_OK && editor,
	                      MLVIEW_ERROR) ;

	mlview_editor_load_xml_file_with_dtd (editor,
	                                      a_doc_uri,
	                                      a_dtd_uri,
	                                      FALSE) ;
	return MLVIEW_OK ;
}
#endif /*MLVIEW_WITH_DBUS*/
