/* -*- Mode: C++; indent-tabs-mode:true ; c-basic-offset: 4 -*- */
/*
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute 
 *it and/or modify it under the terms of 
 *the GNU General Public License as published by the 
 *Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */

/**
 *@file  
 *Definition of various helper functions used during xml
 *parsing and validation.
 */

#include <string.h>
#include <libxml/parser.h>
#include <libxml/catalog.h>
#include <libxml/valid.h>
#include <libxml/parserInternals.h>
#include <libxml/xmlmemory.h>
#include <libxml/SAX.h>
#include <libxml/uri.h>
/*#include <libxml/hash.h>*/
#include <glade/glade.h>
#include <libgnome/libgnome.h>
#include <libgnomevfs/gnome-vfs.h>
#include <zlib.h>

#include "mlview-utils.h"
#include "config.h"
#include "mlview-parsing-utils.h"
#include "mlview-file-descriptor.h"
#include "mlview-prefs.h"
#include "mlview-prefs-category-general.h"

static MlViewExtSubsDef * gv_ext_subs_def = NULL;

static gboolean gv_store_ext_subs_def = FALSE;

static const gint MAX_NUMBER_OF_ELEMENT_NAMES_IN_CHOICE_LIST = 256;

static GtkWidget *gv_dtd_choice_dialog = NULL;

typedef struct _MlViewParserCtxt MlViewParserCtxt;

/**
 *A custom xmlParserContext where we can store 
 *the mlview app context also. That way, some lightly
 *modified versions of libxml2 functions are able to
 *get/put information from/into the MlViewAppContext.
 */
struct _MlViewParserCtxt
{
	mlview::AppContext *app_ctxt;
	xmlParserCtxt *xml_ctxt;
};

/*helper function declarations*/
static MlViewExtSubsDef * mlview_ext_subs_def_clone (MlViewExtSubsDef * a_ext_subs_def);

static xmlParserInput *mlview_external_entity_loader (const char *URL,
        const char *ID,
        xmlParserCtxt * a_context);

static void mlview_external_subset_sax_handler (void *a_ctx,
        const xmlChar * a_name,
        const xmlChar * a_external_id,
        const xmlChar * a_system_id);


static xmlParserInput *mlview_sax_resolve_entity (void *a_ctx,
        const xmlChar * a_public_id,
        const xmlChar * a_system_id);

static enum MlViewStatus load_xml_document_from_local_file
(const gchar * a_file_name,
 xmlParserCtxtPtr * a_parser_context,
 gboolean a_store_external_subset_info);

static xmlDtdPtr custom_xmlSAXParseDTD (mlview::AppContext *context,
										xmlSAXHandlerPtr sax,
                                        const xmlChar * ExternalID,
                                        const xmlChar * SystemID);


static gboolean mlview_parsing_utils_ask_for_DTD_change_and_validation 
		(const gchar * a_external_id,
         const gchar * a_system_id,
         const xmlDoc *a_doc,
         MlViewExtSubsDef ** a_ext_subs_def);

static xmlChar *mlview_resolve_external_entity (const xmlChar * a_external_id,
												const xmlChar * a_system_id);

static void mlview_parsing_utils_build_entities_list (void *a_hash_data,
        void *a_output_list,
        void *a_entity_name);

static void mlview_parsing_utils_scan_and_build_ids_list (void *a_hash_data, void
        *a_output_list,
        void *a_id_value);

static void  build_required_element_content (xmlElementContent * a_content,
											 xmlNode ** a_node);

static gint g_list_compare_string_elems (gchar * a_elem1, gchar * a_elem2);

static gboolean is_an_ancestor_node (xmlNode * a_ancestor, xmlNode * a_cur_node);

static void generic_error_func (mlview::AppContext *a_context,
								const char *msg, ...) ;

/**
 *creates a new MlViewExtSubsDef. 
 *
 */
MlViewExtSubsDef *
mlview_ext_subs_def_new (const gchar * a_root_element_name,
                         const gchar * a_external_id,
                         const gchar * a_system_id)
{
	MlViewExtSubsDef *result;

	result = (MlViewExtSubsDef*) g_malloc0 (sizeof (MlViewExtSubsDef));

	if (a_external_id != NULL)
		result->external_id = g_strdup (a_external_id);

	if (a_system_id != NULL)
		result->system_id = g_strdup (a_system_id);

	if (a_root_element_name != NULL)
		result->root_element_name =
		    g_strdup (a_root_element_name);

	return result;
}


/**
 *
 */
static MlViewExtSubsDef *
mlview_ext_subs_def_clone (MlViewExtSubsDef * a_ext_subs_def)
{
	THROW_IF_FAIL (a_ext_subs_def != NULL);
	return mlview_ext_subs_def_new
	       (a_ext_subs_def->root_element_name,
	        a_ext_subs_def->external_id,
	        a_ext_subs_def->system_id);
}


/**
 *destroys the MlViewExtSubsDef given in argument. 
 */
void
mlview_ext_subs_def_destroy (MlViewExtSubsDef * a_def)
{
	g_return_if_fail (a_def != NULL);

	if (a_def->external_id != NULL) {
		g_free (a_def->external_id);
		a_def->external_id = NULL;
	}

	if (a_def->system_id != NULL) {
		g_free (a_def->system_id);
		a_def->system_id = NULL;
	}

	if (a_def->root_element_name != NULL) {
		g_free (a_def->root_element_name);
		a_def->root_element_name = NULL;
	}

	g_free (a_def);
}

/**
 *loads the xml document that is in the file which name is
 *a_xml_file_name. 
 *This function creates a new context 
 *and sets this parameter to it. 
 *If the file to open is a gziped, this function knows how
 *to handle it.
 *
 *@param a_parser_context the xml parser 
 *context to refer to for the parsing. 
 *caller of the function must deallocate 
 *the variable a_parser_context. 
 *@param a_xml_file_name the name of the file that contains the xml document.
 *
 *@return 0 if OK, > 0 to report an xmlParserError or < 0 
 *to report  MLVIEW_ERRORS 
 */
static enum MlViewStatus
load_xml_document_from_local_file (const gchar * a_xml_file_uri,
                                   xmlParserCtxt ** a_parser_context,
                                   gboolean a_store_external_subset_info)
{
	GnomeVFSResult vfs_result = GNOME_VFS_OK;
	GnomeVFSHandle *vfs_handle;
	GnomeVFSFileSize  bytes = 1024, read_bytes;
	gchar buffer[bytes];
	int parse_status = MLVIEW_OK;
	xmlParserCtxt *xml_ctxt = NULL;
	mlview::AppContext *app_context = mlview::AppContext::get_instance () ;

	THROW_IF_FAIL (app_context) ;

	/*initialyze validation flag to "false"
	 *=> no validation.
	 *That way, libxml2 won't validate the 
	 *document at load time.
	 *We can then do post parsing validation.
	 */
	xmlDoValidityCheckingDefaultValue = 0;

	/*now, open the xml file... */
	THROW_IF_FAIL (a_xml_file_uri != NULL);

	vfs_result = gnome_vfs_open (&vfs_handle, a_xml_file_uri, GNOME_VFS_OPEN_READ);
	THROW_IF_FAIL (vfs_result == GNOME_VFS_OK);

	/* Read first four bytes to create the parser context */
	vfs_result = gnome_vfs_read (vfs_handle,
								 (gpointer)buffer,
								 (GnomeVFSFileSize)4,
								 &read_bytes);
	THROW_IF_FAIL (vfs_result == GNOME_VFS_OK);

	/*
	 *Force the parser to ignore ignorable white spaces so that
	 *indentation can be made properly at save time.
	 */
	xmlKeepBlanksDefault (0);
	xml_ctxt = xmlCreatePushParserCtxt (NULL, NULL, buffer,
									    (int)read_bytes, a_xml_file_uri);

	if (a_store_external_subset_info == TRUE) {
		/*
		 *overload the externalSubset sax handler with our custom one
		 *that stores the info about the external 
		 *subset declared in the xml doc
		 *so that we can do post parsing validation.
		 */

		/*overload the externalSubset sax handler */
		xml_ctxt->sax->externalSubset =
		    mlview_external_subset_sax_handler;

		xml_ctxt->sax->resolveEntity =
		    mlview_sax_resolve_entity;
		/*
		 *tell the custom sax handler to save the 
		 *external subset definition ... or not
		 */
		gv_store_ext_subs_def =
		    a_store_external_subset_info;
	}

	/*override the error messaging stream of libxml */
	xmlSetGenericErrorFunc (app_context, (xmlGenericErrorFunc)
	                        generic_error_func);

	while (vfs_result == GNOME_VFS_OK) {
		vfs_result = gnome_vfs_read (vfs_handle, buffer, bytes, &read_bytes);
		if (vfs_result != GNOME_VFS_OK)
			break;

		if (read_bytes > 0) {
			parse_status = xmlParseChunk (xml_ctxt, (char*)buffer, (int) read_bytes, 0);

			if (parse_status != XML_ERR_OK)
				break;
		}
	}

	/*
	 *call the xmlParseChunk once more 2 
	 *consume all the data stored in 
	 *the parser internal cache ...
	 */
	if (parse_status == XML_ERR_OK) {
		parse_status =
		    xmlParseChunk (xml_ctxt, buffer,(int)read_bytes, 1);
	}
	*a_parser_context = xml_ctxt;
	/*restore the error display stream of libxml */
	xmlSetGenericErrorFunc (NULL, NULL);
	if (app_context && !app_context->error_buffer_is_empty ()) {
		app_context->display_buffered_error ();
	} else {
		app_context->set_error_dialog_title (NULL);
	}

	/* close file */
	vfs_result = gnome_vfs_close (vfs_handle);

	/*
	 * If the file is compressed ... we set it inside the doc

	if (compressed) {
	        printf ("Compression detected\n");
	        xmlSetDocCompressMode ((**a_parser_context).
	                               myDoc, 9);
	}
	*/

	return (parse_status == XML_ERR_OK) ? MLVIEW_OK : (enum MlViewStatus)
	       parse_status;
}

/**
 *A custom entity loader that first fetches 
 *the entities by resolving their url reference
 *through system catalog and then 
 *by performing a lookup on using http or ftp.
 *http and ftp support still need to be added.
 */
static xmlParserInput *
mlview_external_entity_loader (const char *URL,
                               const char *ID,
                               xmlParserCtxt * a_xml_ctxt)
{
	xmlParserInput *result = NULL;
	xmlChar *resource = NULL;

	resource =
	    mlview_resolve_external_entity ((xmlChar*)ID, (xmlChar*)URL);
	if (!resource) {
		MlViewFileDescriptor *fd = NULL;
		int error = 0;
		gboolean is_local = FALSE;

		fd = mlview_file_descriptor_new (URL);
		THROW_IF_FAIL (fd);
		error = mlview_file_descriptor_is_local (fd,
		        &is_local);
		if (error) {
			if (fd) {
				mlview_file_descriptor_destroy
				(fd);
				fd = NULL;
			}
			return NULL;
		}
		/*now, try to load the local dtd file */
		resource = xmlBuildURI ((xmlChar*)URL, NULL);
	}
	result = xmlNewInputFromFile (a_xml_ctxt, (char*)resource);
	if (resource) {
		xmlFree (resource);
		resource = NULL;
	}
	return result;
}


static xmlParserInput *
mlview_sax_resolve_entity (void *a_ctx,
                           const xmlChar * a_public_id,
                           const xmlChar * a_system_id)
{
	xmlParserCtxt *xml_ctxt = (xmlParserCtxt *) a_ctx;
	xmlParserInput *result = NULL;
	xmlChar *URI = NULL;
	gchar *base = NULL;

	if (xml_ctxt == NULL) {
		/*
		 *build a parser context cause we need it to
		 *build a parser input.
		 */
		xml_ctxt = xmlNewParserCtxt ();
	}

	THROW_IF_FAIL (xml_ctxt);

	if (xml_ctxt && (xml_ctxt->input != NULL)) {
		base = (gchar *)
		       xml_ctxt->input->filename;
	}

	if (base == NULL && xml_ctxt) {
		base = xml_ctxt->directory;
	}

	URI = xmlBuildURI (a_system_id, NULL);
	result = mlview_external_entity_loader ((const char *)
	                                        URI,
	                                        (const char *)
	                                        a_public_id,
	                                        xml_ctxt);

	if (URI != NULL)
		xmlFree (URI);

	return result;
}


/**
 *Resolves the the external entity and returns an xmlChar *
 *that must be freed with xmlFree().
 *
 *@param a_context the libxml parsing context context.
 *@param a_pub_id the public ID of the external entity
 *@param a_sys_id the system ID of the external entity.
 *@return the URL of the external entity or NULL if not found.
 */
static xmlChar *
mlview_resolve_external_entity (const xmlChar * a_pub_id,
                                const xmlChar * a_sys_id)
{
	xmlChar *result = NULL;

	result = xmlCatalogResolve (a_pub_id, a_sys_id);
	if (!result && a_sys_id) {
		if (g_file_test ((gchar*)a_sys_id, G_FILE_TEST_EXISTS) == TRUE
		        && g_file_test ((gchar*)a_sys_id,
		                        G_FILE_TEST_IS_DIR) == FALSE) {
			result = (xmlChar*)g_strdup ((gchar*)a_sys_id) ;
		}
	}
	return result;
}

/**
 *This function is a replacement of the 
 *default "externalSubset" sax handler used by libxml
 *to parse xml documents.
 *If validation is on, it interacts 
 *with the user to know if she wants to use another DTD than the one
 *specified in the doctype. The user can even choose not to validate.
 *
 *@param a_external_id the external id
 *@param a_ctxt the user data (XML parser context)
 *@param a_system_id the SYSTEM ID (e.g filename or URL)
 *@param a_name the root element name
 */
static void
mlview_external_subset_sax_handler (void *a_ctxt,
                                    const xmlChar * a_name,
                                    const xmlChar *a_external_id,
                                    const xmlChar * a_system_id)
{
	xmlParserCtxt *ctxt = NULL;
	gchar *system_id = NULL ;
	THROW_IF_FAIL (a_ctxt);

	ctxt = (xmlParserCtxt*)a_ctxt;

	if (a_external_id == NULL && a_system_id == NULL)
		return;

	mlview::AppContext *app_context = mlview::AppContext::get_instance () ;
	THROW_IF_FAIL (app_context) ;

	mlview::PrefsCategoryGeneral *prefs =
	    dynamic_cast<mlview::PrefsCategoryGeneral*> (
		mlview::Preferences::get_instance ()
		->get_category_by_id (mlview::PrefsCategoryGeneral::CATEGORY_ID));
	THROW_IF_FAIL (prefs);

	if (prefs->use_validation () == TRUE && ctxt) {
		ctxt->validate = 1;
	} else {
		ctxt->validate = 0;
	}
	system_id = (gchar*)a_system_id ;

	/*
	 *If validation is on, ask the user if she wants 
	 *to choose another DTD but
	 *the one referenced by the document in the doctype.
	 */
	if (ctxt->validate == 1) {
		MlViewExtSubsDef * ext_subs_def = NULL;

		gboolean validate = mlview_parsing_utils_ask_for_DTD_change_and_validation
							((gchar*)a_external_id, (gchar*)system_id,
							 ctxt->myDoc, &ext_subs_def);

		if (validate == FALSE)
			ctxt->validate = 0;

		if (validate == TRUE && ext_subs_def) {
			a_external_id =
			    (xmlChar*)ext_subs_def->external_id;
			system_id =
			    ext_subs_def->system_id;
		}
	}

	if (ctxt->validate == 1) {
#ifdef LIBXML_2_6_SERIES_OR_ABOVE
		xmlSAX2ExternalSubset (ctxt,
		                       a_name, a_external_id,
		                       (xmlChar*)system_id) ;
#else

		externalSubset (ctxt,
		                a_name, a_external_id,
		                system_id);
#endif /*LIBXML_2_6_SERIES_OR_ABOVE*/

		if (ctxt->myDoc
		        && (ctxt->myDoc->extSubset == NULL)) {
			app_context->warning (_("The external DTD subset "
									"was not found. "
									"I couldn't validate "
									"the document."));
		}

		/*
		 *We will not do validation during the document parsing.
		 *We will only do post parsing validation 
		 *(if validation is required).
		 */
		ctxt->validate = 0;
	}
}

/**
 *This code has been copied and modified from libxml2.
 *GPL code is good for you.
 *
 */
static xmlDtdPtr
custom_xmlSAXParseDTD (mlview::AppContext *context,
					   xmlSAXHandlerPtr sax,
                       const xmlChar * ExternalID,
                       const xmlChar * SystemID)
{
	xmlDtdPtr ret = NULL;
	xmlParserCtxtPtr ctxt;
	xmlParserInputPtr input = NULL;
	xmlCharEncoding enc;

	if ((ExternalID == NULL) && (SystemID == NULL))
		return (NULL);

	ctxt = xmlNewParserCtxt ();
	if (ctxt == NULL) {
		return (NULL);
	}
	/*
	 * Set-up the SAX context
	 */
	if (sax != NULL) {
		if (ctxt->sax != NULL)
			xmlFree (ctxt->sax);
		ctxt->sax = sax;
		ctxt->userData = NULL;
	}
	ctxt->userData = ctxt;  /*usefull for libxml2 */

	/*
	 * Ask the Entity resolver to load the damn thing
	 */
	if (ctxt->sax != NULL
	        && ctxt->sax->resolveEntity != NULL) {
		input = ctxt->sax->resolveEntity (ctxt,
		                                  ExternalID,
		                                  SystemID);

	}

	if (input == NULL) {
		if (sax != NULL)
			ctxt->sax = NULL;

		xmlFreeParserCtxt (ctxt);
		return (NULL);
	}

	/*
	 * plug some encoding conversion routines here.
	 */
	xmlPushInput (ctxt, input);

	enc = xmlDetectCharEncoding (ctxt->input->cur, 4);

	xmlSwitchEncoding (ctxt, enc);

	if (input->filename == NULL) {
		input->filename = (char *) xmlStrdup (SystemID);
	}

	input->line = 1;
	input->col = 1;
	input->base = ctxt->input->cur;
	input->cur = ctxt->input->cur;
	input->free = NULL;

	/*
	 * let's parse that entity knowing it's an external subset.
	 */

	ctxt->inSubset = 2;

	ctxt->myDoc = xmlNewDoc (BAD_CAST "1.0");

	ctxt->myDoc->extSubset =
	    xmlNewDtd (ctxt->myDoc, BAD_CAST "none",
	               ExternalID, SystemID);

	xmlParseExternalSubset (ctxt, ExternalID, SystemID);

	if (ctxt->myDoc != NULL) {
		if (ctxt->wellFormed) {
			ret = ctxt->myDoc->extSubset;

			ctxt->myDoc->extSubset = NULL;
		} else {
			ret = NULL;
		}

		xmlFreeDoc (ctxt->myDoc);

		ctxt->myDoc = NULL;
	}

	if (sax != NULL)
		ctxt->sax = NULL;


	xmlFreeParserCtxt (ctxt);

	return (ret);
}

/**
 *Tries to resolve the external DTD subset entity defined by
 *the pair system ID/public ID .
 *Displays dialog to ask the user if she wants
 *to use the resolved external DTD subset entity that has been found (in case MlView found one), 
 *to change the DTD or not to validate at all.
 *@param a_external_id the public ID that identifies 
 *the DTD external subset to consider
 *@param a_system_id the system ID that identifies 
 *the DTD external subset to consider.
 *@param a_ext_subs_def out parameter. The definition of the external DTD
 *subset we resolved the system ID/public ID pair to, if any, otherwise
 * *a_ext_subs_def is set to NULL .
 *@return TRUE if the user choosed to validate, FALSE otherwise.
 */
static gboolean
mlview_parsing_utils_ask_for_DTD_change_and_validation 
		(const gchar * a_external_id,
         const gchar * a_system_id,
         const xmlDoc *a_doc,
         MlViewExtSubsDef ** a_ext_subs_def)
{
	GladeXML *gxml = NULL;
	gchar *gfile = NULL ;
	gint button = 0;
	gboolean result = TRUE, is_relative = FALSE;
	GtkWidget *label_public_id = NULL;
	GtkWidget *label_system_id = NULL;
	GtkWidget *label_resource = NULL;
	gchar *public_id = NULL;
	gchar *system_id = NULL;
	xmlChar *resource = NULL;
	gchar *base_uri = NULL;
	gchar *tmp_str = NULL;
	enum MlViewStatus status = MLVIEW_BAD_PARAM_ERROR;

	THROW_IF_FAIL (a_ext_subs_def != NULL);

	if (a_external_id != NULL)
		public_id = (gchar*)a_external_id ;
	else
		public_id = (gchar*)"\"\"";

	if (a_system_id != NULL)
		system_id = (gchar*)a_system_id ;
	else
		system_id = (gchar*)"\"\"";

	*a_ext_subs_def = NULL; /*sanity initialization */
	system_id = g_strdup (system_id) ;

	mlview_utils_uri_is_relative (system_id, &is_relative) ;

	if (is_relative == TRUE) {
		base_uri = mlview_utils_get_dir_name_from_uri ((gchar*)a_doc->URL);

		if (!base_uri) {
			mlview_utils_trace_debug ("Can't get base URI.");

			g_free (system_id);

			system_id = (gchar*)"\"\"";
		} else {
			status = mlview_utils_relative_uri_to_absolute_uri (system_id,
			         base_uri,
			         &tmp_str);

			g_free (base_uri);
			base_uri = NULL;

			if (!tmp_str || status != MLVIEW_OK) {
				mlview_utils_trace_debug ("Can't get absolute URI.");

				g_free (system_id);

				system_id = (gchar*)"\"\"";
			} else {
				g_free (system_id);

				system_id = tmp_str;

				tmp_str = NULL;
			}
		}
	}

	resource =
	    mlview_resolve_external_entity ((xmlChar*)a_external_id,
	                                    (xmlChar*)system_id);
	if (resource) {
		gfile = gnome_program_locate_file
		        (NULL, GNOME_FILE_DOMAIN_APP_DATADIR,
		         PACKAGE "/mlview-dtd-choice.glade", TRUE,
		         NULL) ;
		THROW_IF_FAIL (gfile) ;
		gxml = glade_xml_new (gfile,
		                      "mlview_dtd_choice",
		                      NULL);
		THROW_IF_FAIL (gxml != NULL);
		label_public_id =
		    glade_xml_get_widget (gxml,
		                          "dtd_public_id");
		label_system_id =
		    glade_xml_get_widget (gxml,
		                          "dtd_system_id");
		label_resource =
		    glade_xml_get_widget (gxml,
		                          "resource_catalog");
		gtk_label_set_text (GTK_LABEL (label_public_id),
		                    public_id);
		gtk_label_set_text (GTK_LABEL (label_system_id),
		                    system_id);
		gtk_label_set_text (GTK_LABEL (label_resource),
		                    (gchar*)resource);
		gv_dtd_choice_dialog =
		    glade_xml_get_widget (gxml,
		                          "mlview_dtd_choice");

	} else {
		gfile = gnome_program_locate_file
		        (NULL, GNOME_FILE_DOMAIN_APP_DATADIR,
		         PACKAGE "/mlview-dtd-choice-dtd-not-resolved.glade",
		         TRUE, NULL) ;
		THROW_IF_FAIL (gfile) ;
		gxml = glade_xml_new (gfile, "mlview_dtd_choice_dtd_not_resolved",
		                      NULL);
		THROW_IF_FAIL (gxml != NULL);
		label_public_id =
		    glade_xml_get_widget (gxml,
		                          "dtd_public_id");
		label_system_id =
		    glade_xml_get_widget (gxml,
		                          "dtd_system_id");
		gtk_label_set_text (GTK_LABEL (label_public_id),
		                    public_id);
		gtk_label_set_text (GTK_LABEL (label_system_id),
		                    system_id);
		gv_dtd_choice_dialog =
		    glade_xml_get_widget
		    (gxml,
		     "mlview_dtd_choice_dtd_not_resolved") ;
	}
	glade_xml_signal_autoconnect (gxml);

	button = gtk_dialog_run (GTK_DIALOG
	                         (gv_dtd_choice_dialog));
	if (resource) {
		switch (button) {
		case 0:
			/*user clicked the "Use that DTD" button */
			*a_ext_subs_def =
			    mlview_ext_subs_def_new
			    ("", public_id, system_id) ;
			if (! (*a_ext_subs_def)) {
				mlview_utils_trace_debug
				("Out of memory") ;
				goto cleanup ;
			}
			break;
		case 1:
			/*
			 *user clicked the 
			 *"Choose another DTD" button
			 */
			*a_ext_subs_def =
			    mlview_parsing_utils_let_user_choose_a_dtd (_("Choose a DTD"));

			result = (*a_ext_subs_def ==
			          NULL) ? FALSE : TRUE;
			break;
		case 2:
			/*
			 *user clicked the 
			 *"Do not validate" button
			 */
			result = FALSE; /*do not validate */
			break;
		case -1:

			/*
			 *user closed the window using 
			 *Window manager=>like case 0
			 */
		default:
			break;
		}
	} else {
		switch (button) {
		case 0:
			/*
			 *user clicked the
			 *"Choose another DTD" button
			 **/
			*a_ext_subs_def =
			    mlview_parsing_utils_let_user_choose_a_dtd ( _("Choose a DTD"));

			result = (*a_ext_subs_def ==
			          NULL) ? FALSE : TRUE;
			break;

		case 1:
			/*user clicked the "Do not validate" button */
			result = FALSE; /*do not validate */
			break;

		case -1:

			/*
			 *user closed the window using 
			 *Window manager=>like case 0
			 **/
		default:
			break;
		}
	}

cleanup:
	gtk_widget_hide (GTK_WIDGET (gv_dtd_choice_dialog));
	g_object_unref (gxml);
	g_free (gfile);
	if (system_id) {
		g_free (system_id) ;
	}
	return result;
}

/**
 *The callback of the libxml2 hash table scan method xmlHashScan () ;
 *Takes a_hash_data 
 *(the current value of the hash table) and a_entity_name
 *(the key of the current 
 *value of the hash table), this function adds the
 *entity name into a_output_list which is supposed to be a GList **. 
 *
 *@param a_entity_name the key of the 
 *hash table, here, the name of the entity
 *@param a_output_list out parameter. The result of this function.
 *@param a_hash_data the data contained in an entry of the hash table.
 *
 *
 */
static void
mlview_parsing_utils_build_entities_list (void *a_hash_data,
        void *a_output_list,
        void *a_entity_name)
{

	if (a_entity_name)
		*((GList **) a_output_list) =
		    (GList *) g_list_append
		    (*((GList **) a_output_list),
		     a_entity_name);
}


static void
mlview_parsing_utils_scan_and_build_ids_list (void *a_hash_data, void
        *a_output_list,
        void *a_id_value)
{

	if (a_id_value) {
		*((GList **) a_output_list) =
		    (GList *) g_list_append
		    (*((GList **) a_output_list),
		     a_id_value);
	}
}

static gboolean
is_an_ancestor_node (xmlNode * a_ancestor, xmlNode * a_cur_node)
{
	xmlNode *mobile_ptr = a_cur_node;

	THROW_IF_FAIL (a_cur_node != NULL);
	THROW_IF_FAIL (a_ancestor != NULL);

	while (mobile_ptr) {
		if (xmlStrEqual (mobile_ptr->name,
		                 a_ancestor->name))
			return TRUE;
		mobile_ptr = mobile_ptr->parent;
	}
	return FALSE;
}

/**
 *This function builds the required subtree of the 
 *node given in argument.
 *Required means "the minimum required to make 
 *the document validate against de element content
 *declaration passed in parameter in a_content. 
 *
 *@param a_content the element content declaration tree to refer to.
 *@param a_node the output xml node tree.
 */
static void
build_required_element_content (xmlElementContent * a_content,
                                xmlNode ** a_node)
{
	xmlElementContent *current_element_content = a_content;
	xmlNode *child_node = NULL,
	                      *dummy = NULL;
	xmlDict *dict = NULL ;

	THROW_IF_FAIL (a_node != NULL);
	THROW_IF_FAIL ((*a_node) != NULL);

	if (!current_element_content)
		return;

	if ((*a_node)->doc && (*a_node)->doc->dict) {
		dict = (*a_node)->doc->dict ;
	}
	dummy = xmlNewNode (NULL, (xmlChar*)"<!dummy>");
	xmlDictFreeMem (dict, (xmlChar *) dummy->name);
	dummy->name = NULL;

	switch (current_element_content->type) {

	case XML_ELEMENT_CONTENT_OR:

		/*
		 *this element decl is a 
		 *<!ELEMENT current (child1 | child2)> type.
		 *Note that childi may be an element or a SEQ or whatever.
		 */

		switch (current_element_content->ocur) {
		case XML_ELEMENT_CONTENT_ONCE:
		case XML_ELEMENT_CONTENT_PLUS:
			/*ex: <!ELEMENT current (child1 | child2)> or
			 * <!ELEMENT current (child1 | child2)+> decl type.
			 *Note that childi may be an 
			 *element or a SEQ or whatever.
			 *=> let's check wether if we will 
			 *insert child1 or child2
			 *(that will depend on child1 and child2 cardinality)
			 */
			if (current_element_content->type
			        ==
			        XML_ELEMENT_CONTENT_ELEMENT
			        && current_element_content->name) {

				child_node =
				    xmlNewChild
				    ((*a_node),
				     NULL,
				     current_element_content->
				     name, NULL);

				mlview_parsing_utils_build_required_children_tree (&child_node);

			} else if (current_element_content->c1
			           &&
			           ((current_element_content->
			             c1->ocur ==
			             XML_ELEMENT_CONTENT_ONCE)
			            || current_element_content->
			            c1->ocur ==
			            XML_ELEMENT_CONTENT_PLUS)) {
				/*ex: <!ELEMENT current (child1 | child2*)+>
				 *decl type or 
				 *even <!ELEMENT current (child1 | child2+)+>
				 *Note that childi may be an element or a 
				 *SEQ or whatever.
				 *=>we explore child1 and not child2. 
				 *=> we won't keep child2
				 */
				if (current_element_content->c1->
				        type ==
				        XML_ELEMENT_CONTENT_ELEMENT) {
					dummy->name =
					    current_element_content->
					    c1->name;
					if (is_an_ancestor_node
					        (dummy,
					         *a_node) == FALSE) {
						build_required_element_content
								(current_element_content-> c1, a_node);
					} else {
						build_required_element_content
							( current_element_content-> c2, a_node);
					}
				} else {
					build_required_element_content
							(current_element_content->c1, a_node);
				}
			} else {
				/*ex: <!ELEMENT current (child1* | child2+)+>
				 *decl type
				 *Note that childi may be an element or a 
				 *SEQ or whatever.
				 *=> we won't explore child1 => 
				 *we won't keep it.
				 */
				build_required_element_content (current_element_content->c2, 
												a_node);
			}
			break;
		default:
			/*break*/
			;
		}
		break;

	default:
		switch (current_element_content->ocur) {
		case XML_ELEMENT_CONTENT_ONCE:
		case XML_ELEMENT_CONTENT_PLUS:
			if (current_element_content->name) {
				child_node =
				    xmlNewChild
				    ((*a_node), NULL,
				     current_element_content->
				     name, NULL);

				mlview_parsing_utils_build_required_children_tree
														(&child_node);
			} else {
				build_required_element_content
								(current_element_content->c1, a_node);

				build_required_element_content
				(current_element_content->c2, a_node);
			}

			break;
		default:
			break;

		}
		break;
	}
	if (dummy) {
		dummy->name = NULL;
		xmlFreeNode (dummy);
		dummy = NULL;
	}
}


static gint
g_list_compare_string_elems (gchar * a_str1, gchar * a_str2)
{
	THROW_IF_FAIL (a_str1 != NULL);
	THROW_IF_FAIL (a_str2 != NULL);

	return strcmp (a_str1, a_str2);
}

static void
generic_error_func (mlview::AppContext *a_context,
					const char *a_msg_format, ...)
{
	THROW_IF_FAIL (a_context) ;
	va_list params;

	va_start (params, a_msg_format);
	a_context->bufferize_error (a_msg_format, params) ;
	va_end (params);
}

/******************************************
 *Public methods
 ******************************************/

/**
 *Loads an xml file located on the disk. The user not prompted
 *at all, all informations about any DTD is forgotten.
 *If no DTD name is specified or if there was an error while 
 *loading the specified DTD, the returned document's extSubset
 *will be NULL.
 *@param a_file_name the name of the file to open and parse.
 *If set to NULL, 
 *it is not taken in account.
 *@return the xmlDoc resulting from 
 *the parsing or NULL if the parsing failed. 
 */
xmlDocPtr
mlview_parsing_utils_load_xml_file_with_dtd (const gchar *a_file_name,
											 const gchar *a_dtd_name)
{
	xmlDoc *result = NULL;
	xmlParserCtxtPtr parser_context = NULL;
	xmlDtdPtr dtd = NULL;

	THROW_IF_FAIL (a_file_name != NULL);
	mlview::AppContext *context = mlview::AppContext::get_instance () ;
	THROW_IF_FAIL (context) ;

	if (strcmp (a_file_name, "")) {
		gint load_res;
		/*
		 *fixme: remember to add support of
		 *error handling while loading the document.
		 */
		load_res =
		    load_xml_document_from_local_file
						(a_file_name, &parser_context, FALSE);

		if (parser_context == NULL) {
			context->error (_("could not load xml document %s"), a_file_name);

			return NULL;
		}

		if (!load_res) {
			parser_context->myDoc->name = g_strdup (a_file_name); 
			result = parser_context->myDoc;
		}
	}

	if (result) {
		if (a_dtd_name && strcmp (a_dtd_name, "")) {
			dtd = mlview_parsing_utils_load_dtd (a_dtd_name);

			if (dtd) {
				result->standalone = TRUE;
				result->extSubset = dtd;

				dtd->doc = result;
			}
		}
	}

	if (parser_context != NULL) {
		xmlFreeParserCtxt (parser_context);
	}

	return result;
}

/**
 *Loads an xml file located on the disk. The user is prompted
 *for informations if a DTD is specified in the document.
 *Loads an xml file from an URI.
 *@param a_file_uri the URI of the file to open and parse.
 *@return the xmlDoc resulting from 
 *the parsing or NULL if the parsing failed. 
 */
xmlDocPtr
mlview_parsing_utils_load_xml_file_with_dtd_interactive (const gchar *a_file_uri)
{
	xmlDoc *result = NULL;
	xmlParserCtxtPtr parser_context = NULL;

	THROW_IF_FAIL (a_file_uri != NULL);

	mlview::AppContext *context =  mlview::AppContext::get_instance () ;
	THROW_IF_FAIL (context) ;

	if (strcmp (a_file_uri, "")) {
		gint load_res;
		/*
		 *fixme: remember to add support of 
		 *error handling while loading the document.
		 */
		load_res =
		    load_xml_document_from_local_file (a_file_uri, &parser_context,
											   TRUE);

		if (parser_context == NULL) {
			context->error (_ ("could not load xml document %s"), a_file_uri);
			return NULL;
		}
		if (!load_res) {
			parser_context->myDoc->name = g_strdup (a_file_uri);
			result = parser_context->myDoc;
		}
	}

	if (parser_context != NULL) {
		xmlFreeParserCtxt (parser_context);
	}
	return result;
}


/**
 *Loads an external DTD subset entity.
 *@param a_subset_def the definition of the external DTD subset to load.
 *@param a_app_context the mlview application context.
 *@return the loaded external DTD subset, NULL otherwise.
 */
xmlDtd *
mlview_parsing_utils_load_a_dtd (MlViewExtSubsDef * a_subset_def)
{
	xmlDtd *dtd = NULL;
	xmlSAXHandler *sax_handler = NULL;

	THROW_IF_FAIL (a_subset_def != NULL);

	mlview::AppContext *app_context = mlview::AppContext::get_instance () ;
	THROW_IF_FAIL (app_context) ;

	if (a_subset_def->system_id) {
		/*
		 *Replace this function by xmlSAXParseDTD. 
		 *=> that will allow overloading 
		 *of the resolveEntity handler.
		 */
		sax_handler =
		    (xmlSAXHandler *) xmlMalloc
		    (sizeof (xmlSAXHandler));

		THROW_IF_FAIL (sax_handler != NULL);

		memset (sax_handler, 0, sizeof (xmlSAXHandler));
#ifdef LIBXML_2_6_SERIES_OR_ABOVE

		xmlSAX2InitDefaultSAXHandler(sax_handler, 0) ;
#else

		initxmlDefaultSAXHandler (sax_handler, 0);
#endif

		THROW_IF_FAIL (sax_handler != NULL);
		/*override the libxml error printing routine */
		xmlSetGenericErrorFunc (app_context,
								(xmlGenericErrorFunc) generic_error_func);

		app_context->set_error_dialog_title (_("Some error(s) occured "
											 "during the parsing "
											 "of the dtd.\n\n"));

		/*overload the entity resolver. */
		sax_handler->resolveEntity =
		    mlview_sax_resolve_entity;
		dtd = custom_xmlSAXParseDTD (app_context,
		                             sax_handler,
		                             (xmlChar*)a_subset_def->external_id,
		                             (xmlChar*)a_subset_def->system_id);

		/*restore the libxml error printing routine
		 *FIXME: it would be better re-think the error 
		 *overidding/restoring process.
		 */
		xmlSetGenericErrorFunc (NULL, NULL);

		if (!app_context->error_buffer_is_empty ()) {
			app_context->display_buffered_error ();
		} else {
			app_context->set_error_dialog_title (NULL);
		}
	}

	xmlCleanupParser ();

	return dtd;
}


/**
 *Validate a document against a DTD.
 *@return 0 if document is valid, 1 if not, 2 if no DTD was given 
 *in parameter, and -1 if one of the parameters are bad. 
 *@param a_doc the to validate against the DTD.
 *@param a_dtd the dtd external subset against which we validate the doc.
 *@return 0 is the doc is valide, 1 otherwise and -1 if an error occured.
 */
gint
mlview_parsing_utils_validate_dtd (xmlDoc * a_doc,
                                   xmlDtd * a_dtd)
{
	gint result = 0;
	gint validity_status;
	xmlValidCtxt validation_context = {0};

	THROW_IF_FAIL (a_doc != NULL);

	mlview::AppContext *app_context = mlview::AppContext::get_instance () ;
	THROW_IF_FAIL (app_context) ;

	validation_context.userData = app_context ;
	validation_context.error = (xmlValidityErrorFunc) generic_error_func ;
	validation_context.warning = (xmlValidityWarningFunc) generic_error_func ;

	xmlSetGenericErrorFunc (app_context, (xmlGenericErrorFunc) generic_error_func);
	app_context->set_error_dialog_title (_("Some error(s) occured during "
										   "the validation of the document.\n\n"));

	validity_status = xmlValidateDtd (&validation_context, a_doc, a_dtd);

	if (validity_status == 1) {
		/*xmlValidate() returns 1 if doc is valid
		 *whereas we want to return 0 in that case ...*/
		result = 0;
	} else {
		result = 1;
	}

	if (!app_context->error_buffer_is_empty ()) {
		app_context->display_buffered_error ();
	} else {
		app_context->set_error_dialog_title (NULL);
	}

	/*restore the generic error handling routine*/
	xmlSetGenericErrorFunc (NULL, NULL) ;

	return result;
}


/**
 *found during the parsing of the last xml document if and only if 
 *the parsing has been triggered by calling the
 *function mlview_parsing_utils_load_xml_file_interactive_doctype.
 *
 *@return NULL. This function can also return NULL if no external 
 *subset has been declared in the 
 */
MlViewExtSubsDef *
mlview_utils_get_a_copy_of_last_ext_subs_def (void)
{
	MlViewExtSubsDef *result;

	if (gv_ext_subs_def == NULL)
		return NULL;
	result = mlview_ext_subs_def_clone
	         (gv_ext_subs_def);

	mlview_ext_subs_def_destroy
	(gv_ext_subs_def);

	gv_ext_subs_def = NULL;

	return result;
}

/**
 *Runs a file selector that lets the user a DTD external subset
 *on disk.
 *@param a_title the title of the file selector window.
 *@return MLVIEW_OK 
 */
MlViewExtSubsDef *
mlview_parsing_utils_let_user_choose_a_dtd (gchar * a_title)
{
	gchar *dtd = NULL;
	MlViewExtSubsDef *subset_def = NULL;
	gint response = 0;
	mlview::AppContext *app_context = mlview::AppContext::get_instance () ;
	GtkWidget *file_dialog = GTK_WIDGET (app_context->get_file_chooser
			(a_title, mlview::MLVIEW_FILE_CHOOSER_OPEN_MODE)) ;

	THROW_IF_FAIL (file_dialog != NULL);

	app_context->sbar_push_message (_("Choose a dtd file"));

	response = gtk_dialog_run (GTK_DIALOG(file_dialog));
	gtk_window_set_modal (GTK_WINDOW (file_dialog), FALSE);
	gtk_widget_hide (GTK_WIDGET (file_dialog));

	if (response == GTK_RESPONSE_OK) {
		dtd = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (file_dialog));

		if (dtd) {
			subset_def =
			    mlview_ext_subs_def_new (NULL, NULL, dtd);
		}
		if (dtd) {
			g_free (dtd);
			dtd = NULL;
		}
	}
	app_context->sbar_pop_message ();
	return subset_def;
}

/**
 *Builds the list of the feasible element name that can be 
 *inserted before/after/beneath/ the
 *current node.
 *and -2 if some bad parameter have been passed to this function. 
 *
 *@param a_current_xml_node the reference node.
 *@param a_feasible_names output parameter. 
 *A pointer to the output element completion name list.
 *@param a_insertion_scheme how you want to insert a node 
 *relatively to the @a_current_xml_node.
 */
#ifndef MLVIEW_WITH_LIBXML_EXPERIMENTAL_COMPLETION
gint
mlview_parsing_utils_build_element_name_completion_list 
		(enum NODE_INSERTION_SCHEME a_insertion_scheme,
         xmlNode * a_current_xml_node,
         GList ** a_feasible_names_ptr)
{
	const xmlChar *feasible_names
	[MAX_NUMBER_OF_ELEMENT_NAMES_IN_CHOICE_LIST];
	gint nb_of_names = 0;
	mlview::AppContext *app_context = mlview::AppContext::get_instance ();
	THROW_IF_FAIL (app_context) ;
	xmlGenericError = NULL ;
	THROW_IF_FAIL (a_current_xml_node != NULL);
	THROW_IF_FAIL (a_current_xml_node->type == XML_ELEMENT_NODE);
	THROW_IF_FAIL (feasible_names != NULL);

	memset (feasible_names, 0,
	        MAX_NUMBER_OF_ELEMENT_NAMES_IN_CHOICE_LIST
	        * sizeof (xmlChar *));


	mlview::PrefsCategoryGeneral *prefs =
	    dynamic_cast<mlview::PrefsCategoryGeneral*> (
		mlview::Preferences::get_instance ()
		->get_category_by_id (mlview::PrefsCategoryGeneral::CATEGORY_ID));
	THROW_IF_FAIL (prefs);

	if (prefs->use_validation () != TRUE)
		return -1; /*validation is not turned on !!! */

	if ((a_insertion_scheme == INSERT_BEFORE)
	        && (a_current_xml_node->type == XML_DOCUMENT_NODE)) {
		/*
		 *invocation of xmlValidGetValidElements 
		 *in this case makes libxml2-2.4.16 dump core.*/
		return 0;
	}

	if ((a_insertion_scheme == INSERT_BEFORE)
	        && a_current_xml_node->parent
	        && a_current_xml_node->parent->type
	        == XML_DOCUMENT_NODE) {
		/*
		 *in this case, 
		 *invocation of xmlValidGetValidElements 
		 *with a document that have NULL sigsevs.
		 */
		return 0;
	}

	if ((a_insertion_scheme == INSERT_AFTER)
	        && (a_current_xml_node->type == XML_DOCUMENT_NODE)) {
		/*
		 *invocation of xmlValidGetValidElements 
		 *in this case sigsevs libxml2-2.4.16
		 */
		return 0;
	}

	switch (a_insertion_scheme) {
	case INSERT_BEFORE:
		/*
		 *user wants to insert a node just 
		 *before the current selected node
		 */
		nb_of_names =
		    xmlValidGetValidElements
		    (a_current_xml_node->prev,
		     a_current_xml_node,
		     feasible_names,
		     MAX_NUMBER_OF_ELEMENT_NAMES_IN_CHOICE_LIST);
		break;
	case INSERT_AFTER:
		nb_of_names =
		    xmlValidGetValidElements
		    (a_current_xml_node,
		     a_current_xml_node->next,
		     feasible_names,
		     MAX_NUMBER_OF_ELEMENT_NAMES_IN_CHOICE_LIST);
		break;

	case ADD_CHILD:        /*append child */
		if (a_current_xml_node->children) {
			nb_of_names =
			    xmlValidGetValidElements
			    (a_current_xml_node->last,
			     NULL, feasible_names,
			     MAX_NUMBER_OF_ELEMENT_NAMES_IN_CHOICE_LIST);

		} else {
			nb_of_names =
			    xmlValidGetValidElementsChildren
			    (a_current_xml_node,
			     feasible_names,
			     MAX_NUMBER_OF_ELEMENT_NAMES_IN_CHOICE_LIST);
		}

		break;
	case CHANGE_CUR_ELEMENT_NAME:
		if (a_current_xml_node->prev && a_current_xml_node->next) {
			nb_of_names = xmlValidGetValidElements (a_current_xml_node->prev,
			                                        a_current_xml_node->next,
			                                        feasible_names,
			                                        MAX_NUMBER_OF_ELEMENT_NAMES_IN_CHOICE_LIST) ;
		}
		break ;
	default:
		break;
	}                       /*end switch */

	if (nb_of_names > 0) {  /*found some names to propose to the user */
		int i;
		GList *list_ptr = NULL;
		GHashTable *names_index =
		    g_hash_table_new (g_str_hash,
		                      g_str_equal);

		for (list_ptr = *a_feasible_names_ptr;
		        list_ptr; list_ptr = list_ptr->next) {

			if (list_ptr->data)
				g_hash_table_insert (names_index,
				                     list_ptr->
				                     data,
				                     list_ptr->
				                     data);
		}

		for (i = 0; i < nb_of_names; i++) {

			if (((xmlChar **) feasible_names)[i]
			        && !g_hash_table_lookup
			        (names_index,
			         ((xmlChar **) feasible_names)[i])) {

				(*a_feasible_names_ptr) =
				    g_list_append
				    (*a_feasible_names_ptr,
				     ((xmlChar **)
				      feasible_names)[i]);
			}
		}
		g_hash_table_destroy (names_index);

		*a_feasible_names_ptr =
		    g_list_sort (*a_feasible_names_ptr,
		                 (GCompareFunc)
		                 g_list_compare_string_elems);
	}

	xmlSetGenericErrorFunc (NULL, NULL) ;
	return nb_of_names;
}
#else /*MLVIEW_WITH_LIBXML_EXPERIMENTAL_COMPLETION*/
gint
mlview_parsing_utils_build_element_name_completion_list 
		(enum NODE_INSERTION_SCHEME a_insertion_scheme,
         xmlNode * a_current_xml_node,
         GList ** a_feasible_names_ptr)
{

	xmlCompletionListType  type  ;
	xmlChar *element_names[50] = {0} ;
	int ret = 0, len = 0 ;
	GList *feasible_element_names = NULL ;

	mlview::AppContext *app_context = mlview::AppContext::get_instance () ;
	THROW_IF_FAIL (app_context) ;

	switch (a_insertion_scheme) {
	case INSERT_BEFORE:
		type = XML_PREV_SIBLING_LIST_COMPLETION_TYPE ;
		break ;
	case INSERT_AFTER:
		type = XML_NEXT_SIBLING_LIST_COMPLETION_TYPE ;
		break ;
	case ADD_CHILD:
		type =  XML_APPEND_CHILD_LIST_COMPLETION_TYPE ;
		break ;
	case CHANGE_CUR_ELEMENT_NAME:
		type = XML_CURRENT_ELEMENT_LIST_COMPLETION_TYPE
		       break ;
	case CHANGE_CUR_ELEMENT_NAME:
		type = XML_CURRENT_ELEMENT_LIST_COMPLETION_TYPE ;
		break ;
	default :
		ret = -1 ;
		goto cleanup ;
		break ;
	}
	if (!ret)
		len = xmlValidGetElementNameCompletionList
		      (a_current_xml_node, NULL, type,
		       (const xmlChar**)element_names,
		       50) ;
	else
		goto cleanup ;

	if (len) {
		int i = 0 ;
		for (i = 0 ; i < len ; i++) {
			feasible_element_names = g_list_prepend (feasible_element_names,
			                         element_names[i]) ;
		}
		if (feasible_element_names) {
			g_list_reverse (feasible_element_names) ;
		}
	}
	if (!ret) {
		ret = len ;
		*a_feasible_names_ptr = g_list_concat (*a_feasible_names_ptr,
		                                       feasible_element_names) ;
		feasible_element_names = NULL ;
	}

cleanup:
	if (feasible_element_names) {
		g_list_free (feasible_element_names) ;
		feasible_element_names = NULL ;
	}
	return ret ;
}

#endif

/**
 *Builds the atttribute completion list of a given xml document.
 *@param a_current_xml_node the current xml node
 *@param a_attr_names_compl_list out parameter. 
 *The attribute names completion list.
 *@return -2 if bad parameter were given, -1 if validation is switched off,
 *otherwise, the number of elements in the attribute completion list.
 */
gint
mlview_parsing_utils_build_attribute_name_completion_list 
		(xmlNode * a_current_xml_node,
         GList ** a_attr_names_compl_list,
         gboolean a_required_attributes_only)
{
	xmlElement *element_desc = NULL ;
	gint result = 0;

	mlview::AppContext *app_context = mlview::AppContext::get_instance () ;
	THROW_IF_FAIL (app_context != NULL);

	THROW_IF_FAIL (a_current_xml_node != NULL);
	THROW_IF_FAIL (a_attr_names_compl_list != NULL);

	*a_attr_names_compl_list = NULL;

	mlview::PrefsCategoryGeneral *prefs =
	    dynamic_cast<mlview::PrefsCategoryGeneral*> (
		mlview::Preferences::get_instance ()
		->get_category_by_id (mlview::PrefsCategoryGeneral::CATEGORY_ID));
	THROW_IF_FAIL (prefs);

	if (prefs->use_validation () != TRUE )
		return -1;     /*validation is not turned on !!! */

	if (a_current_xml_node->doc->intSubset != NULL)
		element_desc = xmlGetDtdElementDesc
			(a_current_xml_node->doc->intSubset, a_current_xml_node->name);

	if (element_desc == NULL
	        && a_current_xml_node->doc->extSubset) {
		element_desc =
		    xmlGetDtdElementDesc (a_current_xml_node->doc->extSubset,
								  a_current_xml_node->name);
	}

	if (element_desc != NULL && element_desc->attributes != NULL) {
		/*build the list of attribute names */
		gboolean add_attribute;
		xmlAttribute *curr_attr =
		    element_desc->attributes;
		;

		while (curr_attr) {
			if (a_required_attributes_only == TRUE
				&& curr_attr->def != XML_ATTRIBUTE_REQUIRED) {
				add_attribute = FALSE;
			} else {
				add_attribute = TRUE;
			}

			if (add_attribute == TRUE) {
				*a_attr_names_compl_list = g_list_append
									(*a_attr_names_compl_list,
									 (gpointer) curr_attr->name);
				result++;
			}
			curr_attr = curr_attr->nexth;
		}
	}

	*a_attr_names_compl_list =
	    g_list_sort (*a_attr_names_compl_list,
					 (GCompareFunc) g_list_compare_string_elems);
	return result;
}


/**
 *Gets all the possible sub elements of a given xml element.
 *@param a_element_content the element declaration content from the dtd.
 *@param a_children_table out parameter. 
 *The children table. This parameter is valid if and only
 *@return OK if everthing went well, the error code if not. 
 */
enum MLVIEW_PARSING_UTILS_STATUS
mlview_parsing_utils_get_element_content_table
		(xmlElementContent * a_element_content,
         GHashTable ** a_element_content_table)
{
	enum MLVIEW_PARSING_UTILS_STATUS result = NOK;

	mlview::AppContext *app_context = mlview::AppContext::get_instance () ;
	THROW_IF_FAIL (app_context != NULL);

	if (a_element_content == NULL)
		return OK;

	mlview::PrefsCategoryGeneral *prefs =
	    dynamic_cast<mlview::PrefsCategoryGeneral*> (
		mlview::Preferences::get_instance ()
		->get_category_by_id (mlview::PrefsCategoryGeneral::CATEGORY_ID));
	THROW_IF_FAIL (prefs);

	if (prefs->use_validation () != TRUE) {
		return VALIDATION_IS_OFF;/*validation is not turned on !!! */
	}
	if (!*a_element_content_table) {
		*a_element_content_table = g_hash_table_new (g_str_hash, g_str_equal);
	}
	THROW_IF_FAIL (*a_element_content_table != NULL);

	switch (a_element_content->type) {
	case XML_ELEMENT_CONTENT_PCDATA:
		result = OK;
		break;
	case XML_ELEMENT_CONTENT_ELEMENT:

		if (a_element_content->name == NULL
		        ||
		        g_hash_table_lookup (*a_element_content_table,
									 a_element_content->name)) {
			break;
		}

		g_hash_table_insert (*a_element_content_table,
		                     (gpointer) a_element_content->name, 
							 a_element_content);

		result = OK;
		break;

	case XML_ELEMENT_CONTENT_SEQ:
	case XML_ELEMENT_CONTENT_OR:
		mlview_parsing_utils_get_element_content_table (a_element_content->c1,
														a_element_content_table);

		mlview_parsing_utils_get_element_content_table
						(a_element_content->c2, a_element_content_table);

		result = OK;
		break;

	default:
		result = NOK;
		break;

	}

	return result;
}


/**
 *Given an xml node (a_node), builds the required attributes list
 *of that node. 
 *
 *@param a_node the node to add the attributes to.
 *@param a_app_context the current application context.
 *@return OK upon successful completion, an error code otherwise.
 */
enum MLVIEW_PARSING_UTILS_STATUS
mlview_parsing_utils_build_required_attributes_list (xmlNode * a_node)
{
	GList *attributes_list = NULL, *list_ptr = NULL;
	gint nb_of_attributes = 0 ;
	mlview::AppContext *context = mlview::AppContext::get_instance () ;
	enum MLVIEW_PARSING_UTILS_STATUS result = NOK;

	THROW_IF_FAIL (context != NULL);
	THROW_IF_FAIL (a_node != NULL);

	mlview::PrefsCategoryGeneral *prefs =
	    dynamic_cast<mlview::PrefsCategoryGeneral*> (
		mlview::Preferences::get_instance ()
		->get_category_by_id (mlview::PrefsCategoryGeneral::CATEGORY_ID));
	THROW_IF_FAIL (prefs);

if (prefs->use_validation () != TRUE) {
return VALIDATION_IS_OFF;
	}

	nb_of_attributes =
	    mlview_parsing_utils_build_attribute_name_completion_list
	    (a_node, &attributes_list, TRUE);

	if (nb_of_attributes < 0) {
		return NOK;
	} else if (nb_of_attributes == 0) {
		return OK;
	}

	result = OK;

	for (list_ptr = attributes_list;
	        list_ptr; list_ptr = list_ptr->next) {
		if (!list_ptr->data) {
			continue;
		} else {
			GList *attr_value_set = NULL;
			xmlAttribute *attr_desc = NULL;
			gint *last_id_ptr = NULL;
			gchar *default_value = NULL;
			xmlAttr *attr = NULL;

			if (a_node->doc
			        && a_node->doc->intSubset)
				attr_desc =
				    xmlGetDtdAttrDesc (a_node->doc->intSubset, a_node->name,
									   (xmlChar *) list_ptr->data);

			if ((attr_desc == NULL && a_node->doc)
			        && a_node->doc->extSubset) {
				attr_desc = xmlGetDtdAttrDesc (a_node->doc->extSubset,
											   a_node->name,
											   (xmlChar *) list_ptr->data);
			}
			if (attr_desc == NULL) {
				/*attribute unknown by the dtd */
				continue;
			}
			attr_desc->doc = a_node->doc;
			last_id_ptr = context->get_last_id_ptr ();

			if (last_id_ptr == NULL)
				continue;

			attr_value_set =
			    mlview_parsing_utils_build_attribute_value_set (attr_desc,
																last_id_ptr);

			if (attr_value_set && attr_value_set->data)
				default_value = (gchar*) attr_value_set->data;
			else
				default_value = (gchar *) "defaultValue";

			if (!xmlGetProp (a_node, (xmlChar *) list_ptr->data)) {
				attr = xmlSetProp (a_node,
				                   (xmlChar *) list_ptr->data,
				                   (xmlChar*)default_value);
			}

			if (attr != NULL && attr_desc->atype
				== XML_ATTRIBUTE_ID && a_node->doc) {
				xmlID *id = NULL;

				if (a_node->doc->ids == NULL)
					a_node->doc->ids = xmlHashCreate (0);

				id = (xmlID*) xmlMalloc (sizeof (xmlID));

				THROW_IF_FAIL (id != NULL);

				id->value = (xmlChar*) g_strdup (default_value);
				id->attr = attr;

				xmlHashAddEntry ((xmlHashTable*)a_node->doc->ids,
								 (const xmlChar *) default_value, id);
			}

			g_list_free (attr_value_set);
			attr_value_set = NULL;
		}
	}

	return result;
}


/**
 *Given an xml node, this method builds the subtree of 
 *this node that is required by the schema (or by the DTD)
 *
 *@param a_node the node to consider.
 *@param a_app_context the current instance of application context.
 */
enum MLVIEW_PARSING_UTILS_STATUS
mlview_parsing_utils_build_required_children_tree (xmlNode ** a_node)
{
	xmlElement *element_desc = NULL;
	enum MLVIEW_PARSING_UTILS_STATUS status = NOK;

	mlview::AppContext *app_context = mlview::AppContext::get_instance () ;
	THROW_IF_FAIL (app_context != NULL);

	THROW_IF_FAIL (a_node != NULL);
	THROW_IF_FAIL (*a_node != NULL);
	THROW_IF_FAIL (((*a_node)->type == XML_ELEMENT_NODE)
				   || ((*a_node)->type == XML_ATTRIBUTE_NODE));

	mlview::PrefsCategoryGeneral *prefs =
	    dynamic_cast<mlview::PrefsCategoryGeneral*> (
		mlview::Preferences::get_instance ()
		->get_category_by_id (mlview::PrefsCategoryGeneral::CATEGORY_ID));
	THROW_IF_FAIL (prefs);

	if (prefs->use_validation () != TRUE) {
		return VALIDATION_IS_OFF;
	}

	THROW_IF_FAIL ((*a_node)->doc != NULL);

	if (((*a_node)->doc->intSubset == NULL)
	        && ((*a_node)->doc->extSubset == NULL)) {
		return DOCUMENT_HAS_NO_DTD;
	}

	THROW_IF_FAIL ((*a_node)->type == XML_ELEMENT_NODE);

	if ((*a_node)->doc->intSubset) {
		element_desc =
		    xmlGetDtdElementDesc
		    ((*a_node)->doc->intSubset,
		     (*a_node)->name);
	}

	if (!element_desc) {
		element_desc =
		    xmlGetDtdElementDesc
		    ((*a_node)->doc->extSubset,
		     (*a_node)->name);
	}

	if (element_desc == NULL) {
		return ELEMENT_DESC_NOT_FOUND;
	}

	if (!strcmp ((char*)element_desc->name, "#PCDATA")) {
		xmlNodeSetContent (*a_node, (xmlChar*)"#PCDATA");
		return OK;
	}

	mlview_parsing_utils_build_required_attributes_list (*a_node);

	/*
	 *now, walk thru the element
	 *description tree to see what's
	 *going on there
	 */
	build_required_element_content (element_desc->content, a_node);

	return status;
}

/**
 *Returns the set of possible attribute values allowed 
 *for a given attribute.x
 *@param a_last_id a pointer to the number 
 *part of the last id generated. 
 *This is used only if the current 
 *attribute is an id or an idref. 
 *The function then updates the pointed integer to the
 *new last id value generated. 
 *@param a_attribute_desc the attribute description.
 *@param a_app_context the application context.
 *@return a list of GtkListItem to be put into the GtkList widget. 
 */
GList *
mlview_parsing_utils_build_attribute_value_set (xmlAttribute * a_attribute_desc,
												gint * a_last_id)
{
	GList *result = NULL;
	gchar *id_str = NULL;

	/*xmlNode *a_node ; */

	mlview::AppContext *app_context = mlview::AppContext::get_instance () ;
	THROW_IF_FAIL (app_context != NULL);
	THROW_IF_FAIL (a_attribute_desc != NULL);
	THROW_IF_FAIL (a_attribute_desc->name != NULL);
	THROW_IF_FAIL (a_attribute_desc->parent != NULL
				   && a_attribute_desc->parent->doc);

	THROW_IF_FAIL (a_last_id != NULL);

	switch (a_attribute_desc->atype) {

	case XML_ATTRIBUTE_CDATA:
		break;
	case XML_ATTRIBUTE_ID:

		if (a_attribute_desc->parent->doc->ids == NULL) {
			a_attribute_desc->parent->doc->ids =
			    xmlHashCreate (0);
		}

		id_str = g_strdup_printf ("_%d", *a_last_id);

		while (xmlHashLookup
		        ((xmlHashTable*)a_attribute_desc->parent->doc->ids,
		         (xmlChar*)id_str)) {

			(*a_last_id)++;
			id_str = g_strdup_printf ("_%d",
			                          *a_last_id);

		}

		result = g_list_append (result,
		                        (gpointer) id_str);
		break;
	case XML_ATTRIBUTE_IDREF:
	case XML_ATTRIBUTE_IDREFS:

		if (a_attribute_desc->parent->doc->ids == NULL) {
			a_attribute_desc->parent->doc->ids =
			    xmlHashCreate (0);
		}

		xmlHashScan
		((xmlHashTable*)a_attribute_desc->parent->doc->ids,
		 (xmlHashScanner)
		 mlview_parsing_utils_scan_and_build_ids_list,
		 &result);
		break;

	case XML_ATTRIBUTE_ENTITY:
	case XML_ATTRIBUTE_ENTITIES:

		if (a_attribute_desc->parent->doc->intSubset) {
			xmlHashScan
			((xmlHashTable*)a_attribute_desc->parent->doc->
			 intSubset->entities,
			 (xmlHashScanner)
			 mlview_parsing_utils_build_entities_list,
			 &result);
		}
		if (result == NULL
		        && a_attribute_desc->parent->doc->extSubset) {
			xmlHashScan
			((xmlHashTable*)a_attribute_desc->parent->entities,
			 (xmlHashScanner) mlview_parsing_utils_build_entities_list,
			 &result);
		}

		break;
	case XML_ATTRIBUTE_NMTOKEN:
	case XML_ATTRIBUTE_NMTOKENS:
		break;
	case XML_ATTRIBUTE_ENUMERATION:
		if (a_attribute_desc->tree
		        && a_attribute_desc->tree->name) {
			xmlEnumeration *curr =
			    a_attribute_desc->tree;

			while (curr) {
				if (curr->name) {
					result = g_list_append (result, (gpointer) curr->name);
				}

				curr = curr->next;
			}
		}

		break;

	case XML_ATTRIBUTE_NOTATION:
		break;

	default:
		break;
	}

	return result;
}


/**
 *Builds an returns a list of GtkListItem that represent the
 *list of possible allowed attibute values of a given attribute.
 *@param a_last_id a pointer to the number part of the last id generated. 
 *This is used only if the current attribute is an id or an idref. 
 *The function then updates the pointed integer to the
 *new last id value generated. 
 *@param a_attribute_desc the attribute description.
 *@param a_app_context the application context.
 *
 *@return a list of GtkListItem to be put into the GtkList widget. 
 */
GList *
mlview_parsing_utils_build_graphical_attr_values
(xmlAttribute * a_attribute_desc, gint * a_last_id)
{
	GList *result = NULL,
	                *value_set;

	THROW_IF_FAIL (a_attribute_desc != NULL);
	THROW_IF_FAIL (a_attribute_desc->name != NULL);
	THROW_IF_FAIL (a_attribute_desc->doc != NULL);
	THROW_IF_FAIL (a_last_id != NULL);

	value_set =
	    mlview_parsing_utils_build_attribute_value_set
	    (a_attribute_desc, a_last_id);

	if (value_set) {
		GList *list_ptr = NULL;

		for (list_ptr = value_set;
		        list_ptr; list_ptr = list_ptr->next) {

			result = g_list_append (result,
									(gpointer) gtk_list_item_new_with_label
									 ((gchar *) list_ptr->data));
		}
	}

	return result;
}

xmlDtdPtr
mlview_parsing_utils_load_dtd (const gchar *a_url)
{
	MlViewExtSubsDef *sd = NULL;
	xmlDtdPtr dtd = NULL;

	THROW_IF_FAIL (a_url);

	sd = mlview_ext_subs_def_new (NULL, NULL, a_url);

	if (!sd)
		goto cleanup;

	dtd = mlview_parsing_utils_load_a_dtd (sd);

	if (!dtd)
		goto cleanup;

	return dtd;

cleanup:
	if (sd) {
		mlview_ext_subs_def_destroy (sd);
		sd = NULL;
	}

	if (dtd) {
		xmlFreeDtd (dtd);
		dtd = NULL;
	}

	return NULL;
}

/* About the use of mlview_parsing_utils_load_xml_file_with_dtd (x, NULL, y) in
   functions which load schemas : I hope it works (I don't think there are DTD
   specified in schemas :-) but user should not be prompted for a DTD when a 
   schema is loaded, I think :-) */

xmlRelaxNGPtr
mlview_parsing_utils_load_rng (const gchar *a_url)
{
	xmlDocPtr doc = NULL;
	xmlRelaxNGPtr rng = NULL;
	xmlRelaxNGParserCtxtPtr ctx = NULL;

	THROW_IF_FAIL (a_url);

	doc = mlview_parsing_utils_load_xml_file_with_dtd ((gchar*)a_url, NULL);

	if (!doc)
		goto cleanup;

	ctx = xmlRelaxNGNewDocParserCtxt (doc);

	if (!ctx)
		goto cleanup;

	rng = xmlRelaxNGParse (ctx);

	if (!rng)
		goto cleanup;

	xmlFreeDoc (doc);
	doc = NULL;

	xmlRelaxNGFreeParserCtxt (ctx);
	ctx = NULL;

	return rng;

cleanup:
	if (doc) {
		xmlFreeDoc (doc);
		doc = NULL;
	}

	if (ctx) {
		xmlRelaxNGFreeParserCtxt (ctx);
		doc = NULL;
	}

	if (rng) {
		xmlRelaxNGFree (rng);
		rng = NULL;
	}

	return NULL;
}

xmlSchemaPtr
mlview_parsing_utils_load_xsd (const gchar *a_url)
{
	xmlDocPtr doc = NULL;
	xmlSchemaPtr xsd = NULL;
	xmlSchemaParserCtxtPtr ctx = NULL;

	THROW_IF_FAIL (a_url);
	doc = mlview_parsing_utils_load_xml_file_with_dtd ((gchar*)a_url, NULL);

	if (!doc)
		goto cleanup;

	ctx = xmlSchemaNewDocParserCtxt (doc);

	if (!ctx)
		goto cleanup;

	xsd = xmlSchemaParse (ctx);

	if (!xsd)
		goto cleanup;

	xmlFreeDoc (doc);
	doc = NULL;

	xmlSchemaFreeParserCtxt (ctx);
	ctx = NULL;

	return xsd;

cleanup:
	if (doc) {
		xmlFreeDoc (doc);
		doc = NULL;
	}

	if (ctx) {
		xmlSchemaFreeParserCtxt (ctx);
		doc = NULL;
	}

	if (xsd) {
		xmlSchemaFree (xsd);
		xsd = NULL;
	}

	return NULL;
}

void
mlview_parsing_utils_clean_dtd (xmlDtdPtr a_dtd)
{
	xmlNodePtr cur;

	THROW_IF_FAIL (a_dtd);

	a_dtd->doc = NULL;

	cur = a_dtd->children;

	while (cur) {
		if (cur->doc)
			cur->doc = NULL;

		cur = cur->next;
	}
}

enum MlViewStatus
mlview_parsing_utils_serialize_node_to_buf (const xmlNode *a_node,
											gchar **out_buf)
{
	enum MlViewStatus status = MLVIEW_OK ;
	xmlBuffer *xml_buffer = NULL ;
	xmlChar *content = NULL ;

	THROW_IF_FAIL (a_node) ;

	xml_buffer = xmlBufferCreate () ;
	if (!xmlNodeDump (xml_buffer, a_node->doc, (xmlNode*)a_node, 0, 0)) {
		mlview_utils_trace_debug ("No byte were writen during serialization") ;
		status = MLVIEW_ERROR ;
		goto cleanup ;
	}
	if (!xml_buffer->use) {
		status = MLVIEW_ERROR ;
		goto cleanup ;
	}
	content = (xmlChar*)g_strndup ((gchar*)xmlBufferContent (xml_buffer),
	                               xml_buffer->use) ;
	if (!content) {
		status = MLVIEW_OUT_OF_MEMORY_ERROR ;
		goto cleanup ;
	}
	*out_buf = (gchar*)content ;
	content = NULL ;

cleanup:
	if (xml_buffer) {
		xmlBufferFree (xml_buffer) ;
		xml_buffer = NULL ;
	}
	return status ;
}

enum MlViewStatus
mlview_parsing_utils_do_comment_node (const xmlNode *a_node,
                                      xmlNode **a_comment_node)
{
	enum MlViewStatus status = MLVIEW_OK ;
	xmlNode *comment_node = NULL ;
	gchar *buf = NULL ;
	xmlChar *encoded_buf = NULL ;

	THROW_IF_FAIL (a_node && a_node->doc && a_comment_node) ;

	status = mlview_parsing_utils_serialize_node_to_buf (a_node, &buf) ;

	if (status != MLVIEW_OK || !buf)
		return status ;
	encoded_buf = xmlEncodeEntitiesReentrant (a_node->doc, (xmlChar*)buf) ;
	g_free (buf) ;
	buf = NULL ;
	comment_node = xmlNewDocComment (a_node->doc, encoded_buf) ;
	if (!comment_node) {
		status = MLVIEW_OUT_OF_MEMORY_ERROR ;
		goto cleanup ;
	}

	*a_comment_node = comment_node ;
	comment_node = NULL ;

cleanup:
	if (buf) {
		xmlMemFree (buf) ;
		buf = NULL ;
	}
	if (comment_node) {
		xmlFreeNode (comment_node) ;
		comment_node = NULL ;
	}
	return status ;
}

enum MlViewStatus
mlview_parsing_utils_parse_fragment (const xmlDoc *a_doc,
                                     const xmlChar *a_buf,
                                     xmlNode **a_result_node)
{
	xmlDoc *doc = NULL ;
	xmlNode *node = NULL ;
	enum MlViewStatus status = MLVIEW_OK ;

	doc = const_cast<xmlDoc*>(a_doc) ;
	if (!doc)
		doc = xmlNewDoc ((xmlChar*)"1.0") ;
	if (!doc) {
		mlview_utils_trace_debug ("Got a NULL document") ;
		return MLVIEW_ERROR ;
	}
	if (xmlParseBalancedChunkMemory (doc, NULL, NULL, 0,
	                                 a_buf, &node)) {
		status = MLVIEW_ERROR ;
		goto cleanup ;
	}

	if (status == MLVIEW_OK) {
		*a_result_node = node ;
	}

cleanup:

	if (doc && doc != a_doc) {
		xmlFreeDoc (doc) ;
		doc = NULL ;
	}

	return status ;
}

enum MlViewStatus
mlview_parsing_utils_uncomment_node (xmlDoc *a_doc,
                                     const xmlNode *comment_node,
                                     xmlNode **a_result_node)
{
	enum MlViewStatus status = MLVIEW_OK ;
	xmlParserCtxt *parser_context = NULL ;
	xmlNode *uncommented_node = NULL ;
	gchar *content = NULL, *unescaped_content = NULL ;

	g_return_val_if_fail (a_doc && comment_node && a_result_node,
	                      MLVIEW_BAD_PARAM_ERROR) ;
	g_return_val_if_fail (comment_node->type == XML_COMMENT_NODE,
	                      MLVIEW_BAD_PARAM_ERROR) ;

	content = (gchar*) xmlNodeGetContent ((xmlNode*)comment_node) ;
	THROW_IF_FAIL (content) ;

	parser_context = xmlCreateMemoryParserCtxt (content, strlen (content)) ;
	THROW_IF_FAIL (parser_context) ;

	unescaped_content = (gchar*)xmlStringDecodeEntities (parser_context,
	                    (xmlChar*)content,
	                    XML_SUBSTITUTE_REF,
	                    0, 0, 0) ;

	THROW_IF_FAIL (unescaped_content) ;

	status = mlview_parsing_utils_parse_fragment (a_doc,
	         (xmlChar*)unescaped_content,
	         &uncommented_node) ;
	if (status != MLVIEW_OK) {
		uncommented_node = xmlNewText ((xmlChar*)content) ;
	}
	if (content) {
		g_free (content) ;
		content = NULL ;
	}
	if (parser_context) {
		xmlFreeParserCtxt (parser_context) ;
		parser_context = NULL ;
	}
	if (unescaped_content) {
		g_free (unescaped_content) ;
		unescaped_content = NULL ;
	}

	*a_result_node = uncommented_node ;
	return MLVIEW_OK ;
}
