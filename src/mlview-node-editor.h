/*
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute 
 *it and/or modify it under the terms of 
 *the GNU General Public License as published by the 
 *Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */

/**
 *changes that occur on the xmlNode. This signals could be catched by a higher level editor that would update a tree or something. This class
 *should provide facilities to attach objects to it, so that the higher level editors that use it could easily associates a visual tree node to
 *an xmlNode being edited. It would be then easier to update the matching visual node when an xmlNode is modified.
 *THAT WOULD BE FAR BETTER THAN NOW !!! 
 *
 */

#ifndef __MLVIEW_NODE_EDITOR_H__
#define __MLVIEW_NODE_EDITOR_H__

#include <libxml/tree.h>
#include "mlview-app-context.h"
#include "mlview-xml-document.h"

/**
 *@file
 *The declaration of the #MlViewNodeEditor widget.
 *
 *This widget edits a given xml node.
 *Some of it methods take an xmlNodePtr 
 *in parameter and allow the user to
 *interactively edit the xml node. 
 *It emits signal whenever the xml 
 *node being edited changes.
 *The user of this widget can thus 
 *catch and handle the emitted signals.
 *An xml node can have several types
 *(element node, text node, processing instruction node, comment node).
 *Each type of node is edited using what I
 *call a view. As they are several types of nodes, they are several
 *types of view; one for each type of node. 
 *A particular view is a widget that can 
 *edit a particular type of xml node
 *Each view is abstracted by a structure 
 *that holds the view widget.
 *The #MlViewElementWidget contains all the
 *possible views it can handle. 
 *Whenever an xml node is to be edited, the proper view
 *is then selected and used to edit the node.
 *The selection of the right view is done in 
 *the method mlview_node_editor_edit_xml_node(). 
 *
 */

G_BEGIN_DECLS

#define MLVIEW_TYPE_NODE_EDITOR (mlview_node_editor_get_type())
#define MLVIEW_NODE_EDITOR(object) (GTK_CHECK_CAST((object), MLVIEW_TYPE_NODE_EDITOR, MlViewNodeEditor))
#define MLVIEW_NODE_EDITOR_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass), MLVIEW_TYPE_NODE_EDITOR, MlViewNodeEditorClass))
#define MLVIEW_IS_NODE_EDITOR(object) (GTK_CHECK_TYPE((object), MLVIEW_TYPE_NODE_EDITOR))
#define MLVIEW_IS_NODE_EDITOR_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), MLVIEW_TYPE_NODE_EDITOR))

typedef struct _MlViewNodeEditorPrivate MlViewNodeEditorPrivate;
typedef struct _MlViewNodeEditor MlViewNodeEditor;
typedef struct _MlViewNodeEditorClass MlViewNodeEditorClass;

/***********************************************
 *The structure representing the MlViewNodeEditor 
 *************************************************/

struct _MlViewNodeEditor
{
	/*inherits a GtkHPaned */
	GtkHPaned hpaned;
	MlViewNodeEditorPrivate *priv ;
};

struct _MlViewNodeEditorClass
{
	GtkHPanedClass parent_class;

	/*signal default handlers */

	void (*element_changed) (MlViewNodeEditor *a_this,
	                         gpointer a_data);

	void (*element_name_changed) (MlViewNodeEditor * a_this,
	                              xmlNodePtr a_node);

	void (*element_attribute_changed) (MlViewNodeEditor * a_this,
	                                   xmlNodePtr a_node);

	void (*element_content_changed) (MlViewNodeEditor * a_this,
	                                 xmlNodePtr a_node);

	void (*edit_state_changed) (MlViewNodeEditor * a_this);

	void (*ungrab_focus_requested) (MlViewNodeEditor *a_this) ;

};

/*****************************************************
 *The exported methods of the MlViewNodeEditor object.
 *****************************************************/

guint mlview_node_editor_get_type (void);

GtkWidget *mlview_node_editor_new (MlViewXMLDocument *a_doc);

void
mlview_node_editor_set_application_context (MlViewNodeEditor * a_node_editor);

xmlNodePtr mlview_node_editor_get_current_xml_node (MlViewNodeEditor * a_editor);

void mlview_node_editor_edit_xml_node (MlViewNodeEditor * a_editor,
				       MlViewXMLDocument * a_xml_doc,
				       xmlNode * a_node);

void
mlview_node_editor_clear (MlViewNodeEditor * a_editor);

void
mlview_node_editor_set_editable (MlViewNodeEditor * a_editor,
				 gboolean a_editable);

gpointer
mlview_node_editor_get_attached_object (MlViewNodeEditor * a_editor, 
					gchar * a_key);

void mlview_node_editor_set_left_right_percentage (MlViewNodeEditor * a_node_editor,
						   const guint a_percentage);

enum MlViewStatus mlview_node_editor_connect_to_doc (MlViewNodeEditor *a_this,
						     MlViewXMLDocument *a_doc) ;

enum MlViewStatus mlview_node_editor_disconnect_from_doc (MlViewNodeEditor *a_this,
							  MlViewXMLDocument *a_doc) ;

enum MlViewStatus mlview_node_editor_grab_focus (MlViewNodeEditor *a_this) ;


enum MlViewStatus mlview_node_editor_request_ungrab_focus (MlViewNodeEditor *a_this) ;

G_END_DECLS
#endif /*__MLVIEW_NODE_EDITOR_H__*/
