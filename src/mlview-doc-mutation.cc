/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 8-*- */

/*
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute 
 *it and/or modify it under the terms of 
 *the GNU General Public License as published by the 
 *Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file copyright information.
 */

#include <string.h>
#include "mlview-doc-mutation.h"

/**
 *The private members of the #MlViewDocMutation
 *class.
 */
struct _MlViewDocMutationPrivate
{
	/**
	 *The instance of #MlViewXMLDocument
	 *this mutation refers to.
	 */
	MlViewXMLDocument *mlview_xml_doc ;

	/**
	 *The do_mutation() function pointer.
	 *This must actually perform the mutation.
	 *This function can retrieve its parameters
	 *from the instance of #MlViewMutation by invoking
	 *series of g_object_get_data().
	 */
	MlViewDoMutationFunc do_mutation ;

	/**
	 *the undo_mutation function pointer.
	 *This must must actually perform the converse
	 *of the do_mutation() function pointer.
	 *Issuing. Invoking undo_mutation() right
	 *after do_mutation() should let the #MlViewXMLDocument
	 *unchanged.
	 */
	MlViewUndoMutationFunc undo_mutation ;

	/**
	 *An arbitrary name given to the mutation.
	 */
	gchar *mutation_name ;

	/**
	 *is set to true if the mlview_doc_mutation_dispose() has
	 *been called.
	 */
	gboolean dispose_has_run ;

	guint ref_count ;
} ;

static GObjectClass *gv_parent_class = NULL ;

static void mlview_doc_mutation_dispose (GObject *a_this) ;

static void mlview_doc_mutation_finalize (GObject *a_this) ;

static enum MlViewStatus mlview_doc_mutation_construct (MlViewDocMutation *a_this,
        MlViewXMLDocument *a_mlview_xml_doc,
        MlViewDoMutationFunc a_do_mutation_func,
        MlViewUndoMutationFunc an_undo_mutation_func,
        const gchar * a_mutation_name) ;

#define PRIVATE(object) (object)->priv

static void
mlview_doc_mutation_class_init (MlViewDocMutation *a_klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (a_klass) ;

	gv_parent_class = (GObjectClass *) g_type_class_peek_parent (a_klass) ;
	g_return_if_fail (G_IS_OBJECT_CLASS (gv_parent_class)) ;
	object_class->dispose =  mlview_doc_mutation_dispose ;
	object_class->finalize = mlview_doc_mutation_finalize ;

	/*signal creation code goes here*/

}

static void
mlview_doc_mutation_init (MlViewDocMutation *a_this)
{
	g_return_if_fail (MLVIEW_IS_DOC_MUTATION (a_this)) ;

	if (PRIVATE (a_this))
		return ;
	PRIVATE (a_this) = (MlViewDocMutationPrivate *) g_try_malloc (sizeof (MlViewDocMutationPrivate)) ;
	if (!PRIVATE (a_this)) {
		mlview_utils_trace_debug ("System may be out of memory") ;
		return ;
	}
	memset (PRIVATE (a_this), 0, sizeof (MlViewDocMutationPrivate)) ;
}


static void
mlview_doc_mutation_dispose (GObject *a_this)
{
	MlViewDocMutation *thiz = NULL ;
	g_return_if_fail (a_this && MLVIEW_IS_DOC_MUTATION (a_this)) ;

	thiz = MLVIEW_DOC_MUTATION (a_this) ;
	g_return_if_fail (thiz) ;

	if (!PRIVATE (thiz) || PRIVATE (thiz)->dispose_has_run == TRUE)
		return ;

	if (PRIVATE (thiz)->mutation_name) {
		g_free (PRIVATE (thiz)->mutation_name) ;
		PRIVATE (thiz)->mutation_name = NULL ;
	}
	PRIVATE (thiz)->dispose_has_run = TRUE ;
}

static void
mlview_doc_mutation_finalize (GObject *a_this)
{
	MlViewDocMutation *thiz = NULL ;

	g_return_if_fail (a_this && MLVIEW_IS_DOC_MUTATION (a_this)) ;

	thiz = MLVIEW_DOC_MUTATION (a_this) ;
	g_return_if_fail (thiz) ;

	if (PRIVATE (thiz)) {
		g_free (PRIVATE (thiz)) ;
		PRIVATE (thiz) = NULL ;
	}
}

/******************************
 *Private methods and helpers
 ******************************/

static enum MlViewStatus
mlview_doc_mutation_construct (MlViewDocMutation *a_this,
                               MlViewXMLDocument *a_mlview_xml_doc,
                               MlViewDoMutationFunc a_do_mutation_func,
                               MlViewUndoMutationFunc an_undo_mutation_func,
                               const gchar * a_mutation_name)
{
	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_DOC_MUTATION (a_this)
	                      && PRIVATE (a_this)
	                      && a_do_mutation_func
	                      && an_undo_mutation_func,
	                      MLVIEW_BAD_PARAM_ERROR) ;

	PRIVATE (a_this)->mlview_xml_doc = a_mlview_xml_doc ;
	PRIVATE (a_this)->do_mutation = a_do_mutation_func ;
	PRIVATE (a_this)->undo_mutation = an_undo_mutation_func ;
	PRIVATE (a_this)->mutation_name = g_strdup (a_mutation_name) ;

	return MLVIEW_OK ;
}

/************************************************
 *Public methods of the #MlViewDocMutation class
 ************************************************/

/**
 *Type id builder of the #MlViewDocMutation class.
 *@return the new type id of the #MlViewDocMutation class.
 */
GType
mlview_doc_mutation_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo type_info = {
		                                       sizeof (MlViewDocMutationClass),
		                                       NULL, NULL,
		                                       (GClassInitFunc)
		                                       mlview_doc_mutation_class_init,
		                                       NULL, NULL,
		                                       sizeof (MlViewDocMutation),
		                                       0,
		                                       (GInstanceInitFunc)
		                                       mlview_doc_mutation_init
		                                   };
		type = g_type_register_static (G_TYPE_OBJECT,
		                               "MlViewDocMutation",
		                               &type_info, (GTypeFlags) 0);
	}
	return type;
}

/**
 *Instanciates a new #MlViewDocMutation.
 *@param a_mlview_xml_doc the instance of #MlViewXMLDocument
 *this mutation refers to. Must be non null.
 *@param a_do_mutation_func. A pointer to a function that actually performs
 *the mutation on the document @a_mlview_xml_doc.
 *@param a_undo_mutation_func. A pointer to a function that undoes what
 *@a_do_mutation_func has done.
 *@param a_mutation_name an arbitrary name given the the mutation.
 *Must be unique.
 *@return the newly built instance of #MlViewDocMutation, NULL if an error
 *occured.
 */
MlViewDocMutation *
mlview_doc_mutation_new (MlViewXMLDocument *a_mlview_xml_doc,
                         MlViewDoMutationFunc a_do_mutation_func,
                         MlViewUndoMutationFunc an_undo_mutation_func,
                         const gchar *a_mutation_name)

{
	enum MlViewStatus status = MLVIEW_OK ;
	MlViewDocMutation *result = NULL ;

	g_return_val_if_fail (a_do_mutation_func
	                      && an_undo_mutation_func
	                      && a_mutation_name,
	                      NULL) ;

	result = (MlViewDocMutation *) g_object_new (MLVIEW_TYPE_DOC_MUTATION, NULL) ;

	status = mlview_doc_mutation_construct (result,
	                                        a_mlview_xml_doc,
	                                        a_do_mutation_func,
	                                        an_undo_mutation_func,
	                                        a_mutation_name) ;
	if (status != MLVIEW_OK) {
		mlview_utils_trace_debug ("Construction of MlViewDocMutation failed") ;
		g_object_unref (G_OBJECT (result)) ;
		result = NULL ;
	}
	return result ;
}

/**
 *Invoke the do_mutation() function pointer defined at
 *#MlViewDocMutation instanciation time.
 *@param a_this the current instance of #MlViewDocMutation
 *@param a_user_data some custom user defined data.
 *@return MLVIEW_OK upon successful completion, an error
 *code otherwise.
 */
enum MlViewStatus
mlview_doc_mutation_do_mutation (MlViewDocMutation *a_this,
                                 gpointer a_user_data)
{
	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_DOC_MUTATION (a_this)
	                      && PRIVATE (a_this),
	                      MLVIEW_BAD_PARAM_ERROR) ;

	if (!PRIVATE (a_this)->do_mutation) {
		mlview_utils_trace_debug ("do_mutation() func pointer not defined !") ;
		return MLVIEW_IFACE_NOT_DEFINED_ERROR ;
	}
	return PRIVATE (a_this)->do_mutation (a_this, a_user_data) ;

}

/**
 *Invoke the undo_mutation() function pointer defined at
 *#MlViewDocMUtation instanciation time.
 *@param a_this the current instance of #MlViewDocMutation.
 *@param a_user_data a custom user defined data.
 *@return MLVIEW_OK upon successful completion, an error code otherwise.
 */
enum MlViewStatus
mlview_doc_mutation_undo_mutation (MlViewDocMutation *a_this,
                                   gpointer a_user_data)
{
	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_DOC_MUTATION (a_this)
	                      && PRIVATE (a_this),
	                      MLVIEW_BAD_PARAM_ERROR) ;

	if (!PRIVATE (a_this)->undo_mutation) {
		mlview_utils_trace_debug ("undo_mutation() func pointer not defined !") ;
		return MLVIEW_IFACE_NOT_DEFINED_ERROR ;
	}
	return PRIVATE (a_this)->undo_mutation (a_this, a_user_data) ;
}

/**
 *@return the instance of #MlViewXMLDocument associated to
 *@a_this
 *@param a_this the current instance of #MlViewDocMutation
 */
MlViewXMLDocument *
mlview_doc_mutation_get_doc (MlViewDocMutation *a_this)
{
	g_return_val_if_fail (a_this && MLVIEW_IS_DOC_MUTATION (a_this)
	                      && PRIVATE (a_this),
	                      NULL) ;

	return PRIVATE (a_this)->mlview_xml_doc ;
}

/**
 *Destroys the currrent instance of #MlViewDocMutation
 *@param a_this the current instance of #MlViewDocMutation to destroy.
 *@return MLVIEW_OK upon successful completion, an error code otherwise.
 */
enum MlViewStatus
mlview_doc_mutation_destroy (MlViewDocMutation *a_this)
{
	g_return_val_if_fail (a_this && MLVIEW_IS_DOC_MUTATION (a_this),
	                      MLVIEW_BAD_PARAM_ERROR) ;

	g_object_unref (a_this) ;
	return MLVIEW_OK ;
}


enum MlViewStatus
mlview_doc_mutation_ref (MlViewDocMutation *a_this)
{
	g_return_val_if_fail (a_this && MLVIEW_IS_DOC_MUTATION (a_this)
	                      && PRIVATE (a_this),
	                      MLVIEW_BAD_PARAM_ERROR) ;

	PRIVATE (a_this)->ref_count ++ ;
	return MLVIEW_OK ;
}


enum MlViewStatus
mlview_doc_mutation_unref (MlViewDocMutation *a_this)
{
	g_return_val_if_fail (a_this && MLVIEW_IS_DOC_MUTATION (a_this),
	                      MLVIEW_BAD_PARAM_ERROR) ;

	if (PRIVATE (a_this)->ref_count) {
		PRIVATE (a_this)->ref_count -- ;
	}
	if (PRIVATE (a_this)->ref_count == 0) {
		mlview_doc_mutation_destroy (a_this) ;
	}
	return MLVIEW_OK ;
}
