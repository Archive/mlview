/* -*- Mode: C++; indent-tabs-mode:nil; c-basic-offset: 4-*- */
/*
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute 
 *it and/or modify it under the terms of 
 *the GNU General Public License as published by the 
 *Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */

#ifndef __MLVIEW_ICON_TREE_H__
#define __MLVIEW_ICON_TREE_H__

#include "mlview-tree-editor.h"

/**
 *@file
 *The declaration of a tree editor that embedds nice icons (#MlViewIconTree)
 */

G_BEGIN_DECLS

#define MLVIEW_TYPE_ICON_TREE (mlview_icon_tree_get_type())
#define MLVIEW_ICON_TREE(object) (G_TYPE_CHECK_INSTANCE_CAST((object),MLVIEW_TYPE_ICON_TREE,MlViewIconTree))
#define MLVIEW_ICON_TREE_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST((klass),MLVIEW_TYPE_ICON_TREE,MlViewIconTreeClass))
#define MLVIEW_IS_ICON_TREE(object) (G_TYPE_CHECK_INSTANCE_TYPE((object),MLVIEW_TYPE_ICON_TREE))
#define MLVIEW_IS_ICON_TREE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass),MLVIEW_TYPE_ICON_TREE))

typedef struct _MlViewIconTree MlViewIconTree;
typedef struct _MlViewIconTreeClass MlViewIconTreeClass;
typedef struct _MlViewIconTreePrivate MlViewIconTreePrivate;

/**
 *The MlViewIconTree class.
 *This class abstracts a tree editor
 *that illustrates each type of xml node with 
 *nice icons. 
 *This class inherits the #MlViewTreeEditor2 class.
 */
struct _MlViewIconTree {
  MlViewTreeEditor tree_editor2;
};

GType mlview_icon_tree_get_type (void);
GtkWidget *mlview_icon_tree_new ();

struct _MlViewIconTreeClass {
        MlViewTreeEditorClass parent_class;

        struct 
        {
                GdkPixbuf *element;
                GdkPixbuf *open_element;
                GdkPixbuf *text;
                GdkPixbuf *root;
                GdkPixbuf *open_root;
                GdkPixbuf *comment;
                GdkPixbuf *pi;
                GdkPixbuf *entity_ref;
                guint refcount;
        } icons;
};

G_END_DECLS

#endif
