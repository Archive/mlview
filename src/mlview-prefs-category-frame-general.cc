/* -*- Mode: C++; indent-tabs-mode:true ; c-basic-offset: 4-*- */

/*
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute 
 *it and/or modify it under the terms of 
 *the GNU General Public License as published by the 
 *Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */
#include <iostream>
#include "mlview-prefs.h"
#include "mlview-prefs-category-general.h"
#include "mlview-prefs-category-frame-general.h"

namespace mlview
{
struct PrefsCategoryFrameGeneralPriv
{
    PrefsCategoryGeneral *m_prefs;
    Gtk::ComboBox    *m_default_view_combo;
    Gtk::CheckButton *m_use_validation_check_button;
    Gtk::Button      *m_reset_to_default_button;

    void setup_ui (Glib::RefPtr<Gnome::Glade::Xml> i_glade_xml);
    void setup_event_handlers ();

// Callbacks
    void default_view_combo_changed_callback ();
    void use_validation_check_button_toggled_callback ();
    void reset_to_default ();

	PrefsCategoryFrameGeneralPriv ():
		m_default_view_combo (NULL),
		m_use_validation_check_button (NULL),
		m_reset_to_default_button (NULL)
	{}
};

PrefsCategoryFrameGeneral::PrefsCategoryFrameGeneral ()
    : PrefsCategoryFrame ("prefs_category_box_general")
{
	THROW_IF_FAIL (m_priv) ;

    Glib::RefPtr<Gnome::Glade::Xml> l_refxml = this->get_gladexml_ref ();

    m_priv = new PrefsCategoryFrameGeneralPriv ();
    m_priv->m_prefs = dynamic_cast<PrefsCategoryGeneral*>
	(Preferences::get_instance ()->get_category_by_id ("general"));
	THROW_IF_FAIL (m_priv->m_prefs) ;

    m_priv->setup_ui (l_refxml);
    m_priv->setup_event_handlers ();

}

PrefsCategoryFrameGeneral::~PrefsCategoryFrameGeneral ()
{
    if (m_priv != NULL) {
		delete m_priv;
		m_priv = NULL;
    }
}

void
PrefsCategoryFrameGeneralPriv::setup_ui (
    Glib::RefPtr<Gnome::Glade::Xml> a_glade_xml_ref)
{
    a_glade_xml_ref->get_widget ("default_view_combo",
				 m_default_view_combo);
	THROW_IF_FAIL (m_default_view_combo) ;

    a_glade_xml_ref->get_widget ("use_validation_check_button",
				 m_use_validation_check_button);
	THROW_IF_FAIL (m_use_validation_check_button) ;

    a_glade_xml_ref->get_widget ("prefs_category_general_reset_button",
				 m_reset_to_default_button);
	THROW_IF_FAIL (m_reset_to_default_button) ;

    int idx = -1;
    if (m_prefs->get_default_edition_view () == "tree-view")
		idx = 0;
    else
		idx = 1;
    m_default_view_combo->set_active (idx);

    m_use_validation_check_button->set_active
	(m_prefs->use_validation ());
}

void
PrefsCategoryFrameGeneralPriv::setup_event_handlers ()
{
    m_default_view_combo->signal_changed ().connect
	(sigc::mem_fun (*this,
			&PrefsCategoryFrameGeneralPriv::
			default_view_combo_changed_callback));

    m_use_validation_check_button->signal_toggled ().connect
	(sigc::mem_fun (*this,
			&PrefsCategoryFrameGeneralPriv::
			use_validation_check_button_toggled_callback));

    m_reset_to_default_button->signal_clicked ().connect
	(sigc::mem_fun (*this,
			&PrefsCategoryFrameGeneralPriv::
			reset_to_default));

}

void
PrefsCategoryFrameGeneralPriv::default_view_combo_changed_callback()
{
    int active_row = m_default_view_combo->get_active_row_number ();
    switch (active_row) {
    case 0:
		m_prefs->set_default_edition_view ("tree-view");
	break;
    case 1:
		m_prefs->set_default_edition_view ("source-view");
	break;
    default:
	break;
    }
}

void
PrefsCategoryFrameGeneralPriv::use_validation_check_button_toggled_callback ()
{
    m_prefs->set_use_validation
	(m_use_validation_check_button->get_active ());
}

void
PrefsCategoryFrameGeneralPriv::reset_to_default ()
{
    int idx = -1;
    if (m_prefs->get_default_edition_view_default () == "tree-view")
		idx = 0;
    else
		idx = 1;
    m_default_view_combo->set_active (idx);

    m_use_validation_check_button->set_active
	(m_prefs->use_validation_default ());
}

} // namespace mlview
