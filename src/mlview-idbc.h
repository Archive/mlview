/* -*- Mode: C; indent-tabs-mode:nil; c-basic-offset: 8-*- */

/*
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute 
 *it and/or modify it under the terms of 
 *the GNU General Public License as published by the 
 *Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */

#ifndef __MLVIEW_IDBC_H__
#define __MLVIEW_IDBC_H__

#include "mlview-utils.h"
#ifdef MLVIEW_WITH_DBUS
#include <dbus/dbus.h>

G_BEGIN_DECLS

#define MLVIEW_TYPE_IDBC (mlview_idbc_get_type ())
#define MLVIEW_IDBC(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), MLVIEW_TYPE_IDBC, MlViewIDBC))
#define MLVIEW_IS_IDBC(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), MLVIEW_TYPE_IDBC))
#define MLVIEW_IDBC_GET_IFACE(obj) (G_TYPE_INSTANCE_GET_INTERFACE ((obj), MLVIEW_TYPE_IDBC, MlViewIDBC))

typedef struct _MlViewIDBC MlViewIDBC ;

struct _MlViewIDBC
{
	GTypeInterface parent_object ;

	/**
	 * Get the dbus session bus.
	 * No need to overload this method unless you know what
	 * you are doing. It works by default.
	 * @param a_this the current instance of MlViewIDBC
	 * @param a_dbus_connection the dbus connection to return
	 * @return MLVIEW_OK ipon successful completion, an error code otherwise.
	 */
	enum MlViewStatus (*get_session_bus) (MlViewIDBC *a_this,
	                                      DBusConnection **a_dbus_connection,
	                                      GError **a_error) ;
} ;

GType mlview_idbc_get_type (void) ;

enum MlViewStatus mlview_idbc_get_session_bus (MlViewIDBC *a_this,
        DBusConnection **a_con,
        GError **a_error) ;

G_END_DECLS
#endif /*MLVIEW_WITH_DBUS*/
#endif /*__MLVIEW_IDBC_H__*/

