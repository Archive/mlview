/* -*- Mode: C++; indent-tabs-mode: nil; c-basic-offset: 4-*- */

/**
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute it 
 *and/or modify it under the terms of 
 *the GNU General Public License as published 
 *by the Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *MlView is distributed in the hope that it will be useful, 
 *but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. If not, write to the Free Software Foundation, Inc., 
 *59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */
#include "mlview-view-factory.h"
#include "mlview-view-manager.h"
#include "mlview-tree-view.h"
#include "mlview-source-view.h"
#include "mlview-safe-ptr-utils.h"
#include "mlview-prefs.h"
#include "mlview-prefs-category-general.h"

namespace mlview
{

static struct ViewDescriptor gv_view_types[] =
    {
	    {
		    (gchar*)"tree-view",
		    (gchar*)N_ ("Tree view") ,
		    (gchar*)N_("An editing view well suited "
		               "for the tree oriented editing paradigm")
	    },
#ifdef MLVIEW_WITH_STYLE
	    {
	        (gchar*)"styled-view",
	        N_(Styled view),
	        (gchar*)N_("A highly experimental view to "
	                   "render/edit XML using a CSS")
	    },
#endif
	    {
	        (gchar*)"source-view",
	        (gchar*)N_ ("Source view"),
	        (gchar*)N_("An editing view well suited for the "
	                   "source oriented editing paradigm")
	    },
	    {0}
    } ;

IView*
ViewFactory::create_view (MlViewXMLDocument *a_doc,
                          const UString &a_view_type_name,
                          const UString &a_view_name)
{
	SafePtr<IView, ObjectRef, ObjectUnref> view_ptr ;

	if (a_view_type_name == "tree-view") {
		view_ptr = new TreeView (a_doc, a_view_name) ;
	} else if (a_view_type_name == "source-view") {
		view_ptr = new SourceView (a_doc, a_view_name) ;
	} else {
		view_ptr = NULL ;
	}
	if (view_ptr)
		view_ptr->set_desc_type_name (a_view_type_name) ;
	return view_ptr ;
}

ViewDescriptor *
ViewFactory::get_default_view_descriptor ()
{
    mlview::PrefsCategoryGeneral *prefs =
	    dynamic_cast<mlview::PrefsCategoryGeneral*> (
		mlview::Preferences::get_instance ()
		->get_category_by_id (mlview::PrefsCategoryGeneral::CATEGORY_ID));
    THROW_IF_FAIL (prefs);

	ViewDescriptor *view_desc = peek_editing_view_descriptor
	    (prefs->get_default_edition_view ());

	if (!view_desc) {
		view_desc = peek_editing_view_descriptor ("source-view") ;
		THROW_IF_FAIL (view_desc) ;
	}
	return view_desc ;
}

ViewDescriptor*
ViewFactory::peek_editing_view_descriptor
(const UString &a_view_type_name)
{
	ViewDescriptor *view_desc_ptr = NULL ;

	for (view_desc_ptr = gv_view_types ;
	        view_desc_ptr && view_desc_ptr->view_type_name;
	        view_desc_ptr ++) {
		if (view_desc_ptr->view_type_name
		        && (a_view_type_name == view_desc_ptr->view_type_name)) {
			return view_desc_ptr ;
		}
	}
	return NULL ;
}

guint
ViewFactory::get_number_of_view_desc (void)
{
	gint result = 0 ;
	ViewDescriptor *cur_view_desc = NULL ;

	for (cur_view_desc = gv_view_types ;
	        cur_view_desc && cur_view_desc->view_type_name;
	        cur_view_desc ++) {
		result ++ ;
	}
	return result ;
}

ViewDescriptor*
ViewFactory::get_view_descriptor_at (guint a_offset)
{
	guint nr_views = 0 ;

	nr_views = get_number_of_view_desc () ;
	if (a_offset >= nr_views)
		return NULL ;
	return &gv_view_types[a_offset] ;
}

ViewDescriptor const *
ViewFactory::get_view_descriptors ()
{
	return gv_view_types ;
}
}//namespace mlview

