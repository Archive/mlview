/* -*- Mode: C++; indent-tabs-mode: nil; c-basic-offset: 4-*- */

/*This file is part of MlView
 *
 *MlView is free software; you can redistribute it and/or modify it under the terms of 
 *the GNU General Public License as published by the Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *MlView is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License along with MlView; 
 *see the file COPYING. If not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */

#ifndef __MLVIEW_ATTRIBUTE_PICKER_H__
#  define __MLVIEW_ATTRIBUTE_PICKER_H__

#  include "mlview-app-context.h"

G_BEGIN_DECLS
/**
 *@file
 *The declaration of the class #MlViewAttributePicker.
 *
 */
#  define MLVIEW_TYPE_ATTRIBUTE_PICKER (mlview_attribute_picker_get_type ())
#  define MLVIEW_ATTRIBUTE_PICKER(object) (GTK_CHECK_CAST ((object), MLVIEW_TYPE_ATTRIBUTE_PICKER, MlViewAttributePicker))
#  define MLVIEW_ATTRIBUTE_PICKER_CLASS(klass) (GTK_CHECK_CLASS_CAST ((klass), MLVIEW_TYPE_ATTRIBUTE_PICKER, MlViewAttributePickerClass))
#  define MLVIEW_IS_ATTRIBUTE_PICKER(object) (GTK_CHECK_TYPE ((object), MLVIEW_TYPE_ATTRIBUTE_PICKER))
#  define MLVIEW_IS_ATTRIBUTE_PICKER_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), MLVIEW_TYPE_ATTRIBUTE_PICKER))
typedef struct _MlViewAttributePicker MlViewAttributePicker;
typedef struct _MlViewAttributePickerClass
			MlViewAttributePickerClass;
typedef struct _MlViewAttributePickerPrivate
			MlViewAttributePickerPrivate;

/**
 *The attribute picker of mlview.
 *This class is a business widget. It lets the user enter the name/value of
 *an attribute. It can for example perform an attribute name completion given
 *the xmlNode on which the attribute is to be defined.
 *This class inherits the GnomDialog class.
 */
struct _MlViewAttributePicker
{
	/**The parent class*/
	GtkDialog dialog;
	/**The private members of this class*/
	MlViewAttributePickerPrivate *priv;
};

gint mlview_attribute_picker_get_type (void);

GtkWidget *mlview_attribute_picker_new (gchar * a_title);

void
mlview_attribute_picker_set_attribute_completion (gboolean a_completion_on);

void
mlview_attribute_picker_grab_focus_to_name_entry (MlViewAttributePicker * a_picker);

void
mlview_attribute_picker_grab_focus_to_value_entry (MlViewAttributePicker * a_picker);

void
mlview_attribute_picker_select_attribute_value (MlViewAttributePicker * a_picker);

void
mlview_attribute_picker_select_attribute_name (MlViewAttributePicker * a_picker);

gchar *mlview_attribute_picker_get_attribute_name
(MlViewAttributePicker * a_picker);

gchar *mlview_attribute_picker_get_attribute_value
(MlViewAttributePicker * a_picker);

xmlAttributeType
mlview_attribute_picker_get_attribute_type (MlViewAttributePicker
        * a_picker);

void mlview_attribute_picker_build_attribute_name_choice_list
(MlViewAttributePicker * a_picker,
 xmlNode * a_xml_node);

void mlview_attribute_picker_set_current_xml_node
(MlViewAttributePicker * a_picker,
 xmlNode * a_xml_node);

void mlview_attribute_picker_set_app_context (MlViewAttributePicker * a_picker);

struct _MlViewAttributePickerClass
{
	GtkDialogClass parent_class;
};

G_END_DECLS
#endif /*__MLVIEW_ATTRIBUTE_PICKER_H__*/
