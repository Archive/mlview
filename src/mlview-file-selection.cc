/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 8 -*- */

/*
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute 
 *it and/or modify it under the terms of 
 *the GNU General Public License as published by the 
 *Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */

#include <string.h>
#include "mlview-file-selection.h"

/**
 *@file
 *The definition of the file selector class used
 *in mlview.
 */

typedef struct _MlViewFileSelectionRunInfo
			MlViewFileSelectionRunInfo;

/*I copied this struct from the GnomeDialogRunInfo in libgnomeui*/
struct _MlViewFileSelectionRunInfo
{
	/*The number of the button clicked */
	enum MLVIEW_SELECTED_BUTTON clicked_button;

	/*the signal connection id of the widget close callback */
	gint close_id;

	/*the signal connection id of the "ok button clicked" callback */
	gint ok_clicked_id;

	gint cancel_clicked_id;

	/*the signal connection id of the widget "destroy" callback */
	gint destroy_id;
	gboolean is_destroyed;
	gboolean is_closed;

	/*The id of the event mainloop */
	GMainLoop *mainloop;
};

static void mlview_file_selection_class_init
(MlViewFileSelectionClass * a_klass);

static void mlview_file_selection_init (MlViewFileSelection * a_file_sel);

static void mlview_file_selection_destroy (GtkObject * a_object);

static void mlview_file_selection_ok_clicked_callback (GtkButton * a_button,
        MlViewFileSelectionRunInfo * a_info);

static void mlview_file_selection_cancel_clicked_callback (GtkButton * a_button,
        MlViewFileSelectionRunInfo * a_info);

static gboolean mlview_file_selection_closed_callback (GtkWidget * a_filesel,
        GdkEvent * a_event,
        MlViewFileSelectionRunInfo * a_run_info);

static void mlview_file_selection_disconnect (MlViewFileSelection * a_filesel,
        MlViewFileSelectionRunInfo *
        a_run_info);

static void mlview_file_selection_event_loop_quit (MlViewFileSelectionRunInfo * a_user_data);

static gboolean mlview_file_selection_mark_destroy (GtkWidget * a_filesel,
        GdkEvent * a_event,
        MlViewFileSelectionRunInfo * a_run_info);

static enum MLVIEW_SELECTED_BUTTON mlview_file_selection_run_real (MlViewFileSelection * a_this,
        gboolean a_close_after);

GtkFileSelectionClass *parent_class = NULL;

/*===========================================
 *private methods.
 *==========================================*/

static void
mlview_file_selection_class_init (MlViewFileSelectionClass * a_klass)
{
	GtkObjectClass *object_class =
	    GTK_OBJECT_CLASS (a_klass);
	parent_class = (GtkFileSelectionClass*) gtk_type_class
	               (GTK_TYPE_FILE_SELECTION);

	/*overload the destroy method of GtkObject */
	object_class->destroy = mlview_file_selection_destroy;
}


static void
mlview_file_selection_init (MlViewFileSelection * a_file_sel)
{
	g_assert (a_file_sel != NULL);
	g_assert (MLVIEW_IS_FILE_SELECTION (a_file_sel));
	/*we do not have anything to do for the moment ... */
}

static void
mlview_file_selection_ok_clicked_callback (GtkButton * a_button,
        MlViewFileSelectionRunInfo * a_info)
{
	g_return_if_fail (a_button != NULL);
	g_return_if_fail (a_info != NULL);
	g_return_if_fail (GTK_IS_BUTTON (a_button));

	a_info->clicked_button = OK_BUTTON;
	mlview_file_selection_event_loop_quit (a_info);
}


static void
mlview_file_selection_cancel_clicked_callback (GtkButton * a_button,
        MlViewFileSelectionRunInfo * a_info)
{
	g_return_if_fail (a_button != NULL);
	g_return_if_fail (a_info != NULL);
	g_return_if_fail (GTK_IS_BUTTON (a_button));

	a_info->clicked_button = CANCEL_BUTTON;
	mlview_file_selection_event_loop_quit (a_info);
}

static gboolean
mlview_file_selection_closed_callback (GtkWidget * a_filesel,
                                       GdkEvent * a_event,
                                       MlViewFileSelectionRunInfo * a_run_info)
{
	g_return_val_if_fail (a_run_info != NULL, TRUE);

	a_run_info->clicked_button = WINDOW_CLOSED;
	mlview_file_selection_event_loop_quit (a_run_info);
	gtk_widget_hide (a_filesel);

	return TRUE;
}

static gboolean
mlview_file_selection_mark_destroy (GtkWidget * a_filesel,
                                    GdkEvent * a_event,
                                    MlViewFileSelectionRunInfo *a_run_info)
{
	g_return_val_if_fail (a_run_info != NULL, FALSE);
	a_run_info->clicked_button = (MLVIEW_SELECTED_BUTTON) 0;
	mlview_file_selection_event_loop_quit (a_run_info);

	return TRUE;
}


static void
mlview_file_selection_event_loop_quit (MlViewFileSelectionRunInfo * a_run_info)
{
	g_return_if_fail (a_run_info);

	if (a_run_info->mainloop && g_main_is_running
	        (a_run_info->mainloop)) {
		g_main_quit (a_run_info->mainloop);
	}
}


static void
mlview_file_selection_disconnect (MlViewFileSelection *a_filesel,
                                  MlViewFileSelectionRunInfo * a_run_info)
{
	g_return_if_fail (a_filesel != NULL);
	g_return_if_fail (a_run_info != NULL);

	if (a_run_info->is_destroyed != TRUE) {
		g_signal_handler_disconnect
		(G_OBJECT (GTK_FILE_SELECTION
		           (a_filesel)->ok_button),
		 a_run_info->ok_clicked_id);

		g_signal_handler_disconnect
		(G_OBJECT (GTK_FILE_SELECTION
		           (a_filesel)->cancel_button),
		 a_run_info->cancel_clicked_id);

		g_signal_handler_disconnect
		(G_OBJECT (a_filesel),
		 a_run_info->close_id);

		g_signal_handler_disconnect
		(G_OBJECT (a_filesel),
		 a_run_info->destroy_id);
	}

	a_run_info->is_closed = TRUE;
}


/**
 *Runs the file selector window and returns when
 *the user has performed a relevant action on it.
 *@param a_this the current instance of #MlViewFileSelection.
 *@param a_close_after if TRUE, closes the window just before
 *return.
 *@return the selected button.
 */
static enum MLVIEW_SELECTED_BUTTON
mlview_file_selection_run_real (MlViewFileSelection * a_this,
                                gboolean a_close_after)
{
	/*a boolean to store the initial modal state of the file_selection
	 *so that we can restore it when we get out.
	 */
	gboolean was_modal;
	MlViewFileSelectionRunInfo run_info;

	g_return_val_if_fail (a_this, NO_BUTTON_SELECTED);
	g_return_val_if_fail (MLVIEW_IS_FILE_SELECTION
	                      (a_this), NO_BUTTON_SELECTED);


	memset (&run_info, 0,
	        sizeof (MlViewFileSelectionRunInfo));

	/*let's connect to all
	 *the helpful signals: clicks on the buttons, 
	 *delete_event, close, destroy
	 */
	run_info.ok_clicked_id =
	    g_signal_connect
	    (G_OBJECT
	     (GTK_FILE_SELECTION (a_this)->
	      ok_button), "clicked",
	     G_CALLBACK
	     (mlview_file_selection_ok_clicked_callback),
	     &run_info);

	run_info.cancel_clicked_id =
	    g_signal_connect
	    (G_OBJECT (GTK_FILE_SELECTION
	               (a_this)->cancel_button),
	     "clicked",
	     G_CALLBACK
	     (mlview_file_selection_cancel_clicked_callback),
	     &run_info);

	run_info.close_id =
	    g_signal_connect
	    (G_OBJECT (a_this),
	     "delete_event",
	     G_CALLBACK
	     (mlview_file_selection_closed_callback),
	     &run_info);

	run_info.destroy_id =
	    g_signal_connect
	    (G_OBJECT (a_this),
	     "destroy_event",
	     G_CALLBACK (mlview_file_selection_mark_destroy),
	     &run_info);

	was_modal = GTK_WINDOW (a_this)->modal;

	/*we have to set the file selection as modal */
	if (!was_modal)
		gtk_window_set_modal (GTK_WINDOW
		                      (a_this), TRUE);

	if (!GTK_WIDGET_VISIBLE (GTK_WIDGET (a_this))) {
		gtk_widget_show_all (GTK_WIDGET
		                     (a_this));
	}

	run_info.mainloop = g_main_new (FALSE);
	g_main_run (run_info.mainloop);

	/*let's disconnect to all the signals we connected to */
	mlview_file_selection_disconnect (a_this,
	                                  &run_info);

	if (!run_info.is_destroyed) {
		if (!was_modal) {
			/*
			 *file selection was not 
			 *initially modal.
			 *Let's restore its state
			 **/
			gtk_window_set_modal (GTK_WINDOW
			                      (a_this),
			                      FALSE);
		}
	}

	if (run_info.mainloop)
		g_main_destroy (run_info.mainloop);

	if (a_close_after == TRUE)
		gtk_widget_hide (GTK_WIDGET (a_this));

	return run_info.clicked_button;
}


/*
 *Destroys the mlview_file_selection.
 *@a_object: the file selection to destroy.
 */
static void
mlview_file_selection_destroy (GtkObject * a_object)
{
	g_return_if_fail (a_object != NULL);
	g_return_if_fail (MLVIEW_IS_FILE_SELECTION (a_object));

	if (GTK_OBJECT_CLASS (parent_class)->destroy) {
		(GTK_OBJECT_CLASS (parent_class)->
		 destroy) (a_object);
	}
}


/*===========================================
 *Public methods.
 *==========================================*/
/**
 *The type getter/builder of the #MlViewFileSelection class.
 */
guint
mlview_file_selection_get_type (void)
{
	static guint mlview_file_selection_type = 0;

	if (!mlview_file_selection_type) {
		static const GTypeInfo type_info = {
		                                       sizeof (MlViewFileSelectionClass),
		                                       NULL,   /* base_init */
		                                       NULL,   /* base_finalize */
		                                       (GClassInitFunc)
		                                       mlview_file_selection_class_init,
		                                       NULL,   /* class_finalize */
		                                       NULL,   /* class_data */
		                                       sizeof (MlViewFileSelection),
		                                       0,
		                                       (GInstanceInitFunc)
		                                       mlview_file_selection_init
		                                   };

		mlview_file_selection_type =
		    g_type_register_static
		    (GTK_TYPE_FILE_SELECTION,
		     "MlViewFileSelection", &type_info, (GTypeFlags)0);

	}

	return mlview_file_selection_type;
}


/**
 *Default onstructor of the #MlViewFileSelection class.
 *@return the newly created MlViewFileSelection widget. 
 */
GtkWidget *
mlview_file_selection_new (void)
{
	MlViewFileSelection *filesel = NULL;

	filesel = (MlViewFileSelection*) gtk_type_new (MLVIEW_TYPE_FILE_SELECTION);

	g_assert (MLVIEW_IS_FILE_SELECTION (filesel));

	return GTK_WIDGET (filesel);
}


/**
 *runs the file selection widget and waits till the user has clicked
 *on the ok or cancel button, or on the close area. 
 *@param a_this the current instance of #MlViewFileSelection.
 *@param a_close_after if TRUE, the file selection window is closed
 *right after this function returns.
 */
gint
mlview_file_selection_run (MlViewFileSelection *a_this,
                           gboolean a_close_after)
{
	g_return_val_if_fail (a_this != NULL, -2);
	g_return_val_if_fail (MLVIEW_IS_FILE_SELECTION
	                      (a_this), -2);

	return mlview_file_selection_run_real
	       (a_this, a_close_after);
}
