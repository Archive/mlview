/* -*- Mode: C; indent-tabs-mode:nil; c-basic-offset: 8-*- */
/*
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute 
 *it and/or modify it under the terms of 
 *the GNU General Public License as published by the 
 *Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */

/**
 *This class is a custom GtkTreeView cell renderer used in the #MlViewTreeEditor
 *Widget. It provides "completion while typing" features.
 */


#include <string.h>
#include "mlview-marshal.h"
#include "mlview-cell-renderer.h"
#include "mlview-entry.h"

#ifdef MLVIEW_WITH_CUSTOM_CELL_RENDERER

#define PRIVATE(a_object) (a_object)->priv
#define MLVIEW_CELL_RENDERER_PATH "mlview-cell-renderer-path"

typedef void (*SetSelectionBoundsFunc) (GtkEditable *a_editable,
                                        gint a_start_pos,
                                        gint a_end_pos) ;

struct _MlViewCellRendererPrivate
{
	gboolean is_completion_on ;

	gchar *text;
	PangoFontDescription *font;
	gdouble font_scale;
	PangoColor foreground;
	PangoColor background;

	PangoAttrList *extra_attrs;

	PangoUnderline underline_style;

	gint rise;
	gint fixed_height_rows;

guint strikethrough :
	1;

guint editable  :
	1;

guint scale_set :
	1;

guint foreground_set :
	1;
guint background_set :
	1;

guint underline_set :
	1;

guint rise_set :
	1;

guint strikethrough_set :
	1;

guint editable_set :
	1;
guint calc_fixed_height :
	1;

	/*these are private in the gtkcellrenderertext implementation*/
guint single_paragraph :
	1;
guint language_set :
	1;
guint markup_set :
	1;

	gulong focus_out_id;
	PangoLanguage *language;
	SetSelectionBoundsFunc editable_set_selection_bounds_func_backup ;
	gboolean dispose_has_run ;
} ;

enum MlViewCellRendererProps {
    PROP_0 = 0,
    PROP_IS_COMPLETION_ON,

    PROP_TEXT,
    PROP_MARKUP,
    PROP_ATTRIBUTES,
    PROP_SINGLE_PARAGRAPH_MODE,

    /* Style args */
    PROP_BACKGROUND,
    PROP_FOREGROUND,
    PROP_BACKGROUND_GDK,
    PROP_FOREGROUND_GDK,
    PROP_FONT,
    PROP_FONT_DESC,
    PROP_FAMILY,
    PROP_STYLE,
    PROP_VARIANT,
    PROP_WEIGHT,
    PROP_STRETCH,
    PROP_SIZE,
    PROP_SIZE_POINTS,
    PROP_SCALE,
    PROP_EDITABLE,
    PROP_STRIKETHROUGH,
    PROP_UNDERLINE,
    PROP_RISE,
    PROP_LANGUAGE,

    /* Whether-a-style-arg-is-set args */
    PROP_BACKGROUND_SET,
    PROP_FOREGROUND_SET,
    PROP_FAMILY_SET,
    PROP_STYLE_SET,
    PROP_VARIANT_SET,
    PROP_WEIGHT_SET,
    PROP_STRETCH_SET,
    PROP_SIZE_SET,
    PROP_SCALE_SET,
    PROP_EDITABLE_SET,
    PROP_STRIKETHROUGH_SET,
    PROP_UNDERLINE_SET,
    PROP_RISE_SET,
    PROP_LANGUAGE_SET,
    MLVIEW_CELL_RENDERER_PROPS
} ;

enum MlViewCellRendererSignals {
    EDITED,
    WORD_CHANGED,
    EDITING_HAS_STARTED,
    SELECT_EDITABLE_REGION,
    NB_SIGNALS
} ;

static GtkCellRendererClass *gv_parent_class = NULL ;
static guint gv_signals [NB_SIGNALS] = {0};
static SetSelectionBoundsFunc gv_editable_set_selection_bounds_func_backup = 0 ;

static void mlview_cell_renderer_dispose (GObject *a_this) ;

static void mlview_cell_renderer_finalize (GObject *a_this) ;

static void mlview_cell_renderer_get_property (GObject *a_object,
        guint a_param_id,
        GValue *a_value,
        GParamSpec *a_param_spec) ;

static void mlview_cell_renderer_set_property (GObject *a_object,
        guint a_param_id,
        const GValue *a_value,
        GParamSpec *a_param_spec) ;

static PangoLayout* get_layout (MlViewCellRenderer  *a_this,
                                GtkWidget           *widget,
                                gboolean             will_render,
                                GtkCellRendererState flags) ;

static void add_attr (PangoAttrList  *a_attr_list,
                      PangoAttribute *a_attr) ;

static void  mlview_cell_renderer_get_size (GtkCellRenderer      *a_cell,
        GtkWidget            *a_widget,
        GdkRectangle         *a_cell_area,
        gint                 *a_x_offset,
        gint                 *a_y_offset,
        gint                 *a_width,
        gint                 *a_height) ;

static void mlview_cell_renderer_render (GtkCellRenderer      *a_this,
        GdkDrawable          *a_window,
        GtkWidget            *a_widget,
        GdkRectangle         *a_background_area,
        GdkRectangle         *a_cell_area,
        GdkRectangle         *a_expose_area,
        GtkCellRendererState  a_flags) ;

static GtkCellEditable * mlview_cell_renderer_text_start_editing (GtkCellRenderer *a_this,
        GdkEvent *a_event,
        GtkWidget *a_widget,
        const gchar *a_path,
        GdkRectangle *a_background_area,
        GdkRectangle *a_cell_area,
        GtkCellRendererState a_flags) ;

static PangoFontMask set_font_desc_fields (PangoFontDescription *a_desc,
        PangoFontMask         a_to_set) ;

static PangoFontMask get_property_font_set_mask (guint a_prop_id) ;

static void notify_fields_changed (GObject       *a_this,
                                   PangoFontMask  a_changed_mask) ;

static void notify_set_changed (GObject       *a_this,
                                PangoFontMask  a_changed_mask) ;

static void set_font_description (MlViewCellRenderer  *a_this,
                                  PangoFontDescription *a_font_desc) ;

static void set_bg_color (MlViewCellRenderer *a_this,
                          GdkColor            *a_color) ;

static void set_fg_color (MlViewCellRenderer *a_this,
                          GdkColor            *a_color) ;


static void mlview_cell_renderer_editing_done_cb (GtkCellEditable *a_entry,
        gpointer         a_data) ;

static gboolean mlview_cell_renderer_focus_out_event_cb (GtkWidget *a_entry,
        GdkEvent  *a_event,
        gpointer   a_data) ;


/*******************************************
 *Private gobject framework related methods
 ********************************************/

static void
mlview_cell_renderer_class_init (MlViewCellRendererClass *a_klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (a_klass);
	GtkCellRendererClass *cell_class = GTK_CELL_RENDERER_CLASS (a_klass) ;
	GParamSpec *param_spec = NULL ;

	g_return_if_fail (a_klass) ;
	g_return_if_fail (cell_class) ;

	gv_parent_class = (GtkCellRendererClass *) g_type_class_peek_parent (a_klass);
	g_return_if_fail (G_IS_OBJECT_CLASS (gv_parent_class));
	object_class->dispose = mlview_cell_renderer_dispose;
	object_class->finalize = mlview_cell_renderer_finalize;

	object_class->get_property = mlview_cell_renderer_get_property ;
	object_class->set_property = mlview_cell_renderer_set_property ;

	cell_class->get_size = mlview_cell_renderer_get_size ;
	cell_class->render = mlview_cell_renderer_render ;
	cell_class->start_editing = mlview_cell_renderer_text_start_editing ;

	param_spec = g_param_spec_boolean ("is-completion-on",
	                                   "Is completion on",
	                                   "Whether if completion is activated for this widget or not",
	                                   FALSE,
	                                   (GParamFlags) (G_PARAM_READABLE | G_PARAM_WRITABLE)) ;

	g_object_class_install_property (object_class,
	                                 PROP_IS_COMPLETION_ON,
	                                 param_spec) ;

	g_object_class_install_property (object_class,
	                                 PROP_TEXT,
	                                 g_param_spec_string ("text",
	                                                      "Text",
	                                                      "Text to render",
	                                                      NULL,
	                                                      (GParamFlags) G_PARAM_READWRITE));

	g_object_class_install_property (object_class,
	                                 PROP_MARKUP,
	                                 g_param_spec_string ("markup",
	                                                      "Markup",
	                                                      "Marked up text to render",
	                                                      NULL,
	                                                      G_PARAM_WRITABLE));

	g_object_class_install_property (object_class,
	                                 PROP_ATTRIBUTES,
	                                 g_param_spec_boxed ("attributes",
	                                                     "Attributes",
	                                                     "A list of style attributes to apply to the text of the renderer",
	                                                     PANGO_TYPE_ATTR_LIST,
	                                                     (GParamFlags) G_PARAM_READWRITE));

	g_object_class_install_property (object_class,
	                                 PROP_SINGLE_PARAGRAPH_MODE,
	                                 g_param_spec_boolean ("single_paragraph_mode",
	                                                       "Single Paragraph Mode",
	                                                       "Whether or not to keep all text in a single paragraph",
	                                                       FALSE,
	                                                       (GParamFlags) G_PARAM_READWRITE));


	g_object_class_install_property (object_class,
	                                 PROP_BACKGROUND,
	                                 g_param_spec_string ("background",
	                                                      "Background color name",
	                                                      "Background color as a string",
	                                                      NULL,
	                                                      (GParamFlags) G_PARAM_WRITABLE));

	g_object_class_install_property (object_class,
	                                 PROP_BACKGROUND_GDK,
	                                 g_param_spec_boxed ("background_gdk",
	                                                     "Background color",
	                                                     "Background color as a GdkColor",
	                                                     GDK_TYPE_COLOR,
	                                                     (GParamFlags) (G_PARAM_READABLE | G_PARAM_WRITABLE)));

	g_object_class_install_property (object_class,
	                                 PROP_FOREGROUND,
	                                 g_param_spec_string ("foreground",
	                                                      "Foreground color name",
	                                                      "Foreground color as a string",
	                                                      NULL,
	                                                      (GParamFlags) G_PARAM_WRITABLE));

	g_object_class_install_property (object_class,
	                                 PROP_FOREGROUND_GDK,
	                                 g_param_spec_boxed ("foreground_gdk",
	                                                     "Foreground color",
	                                                     "Foreground color as a GdkColor",
	                                                     GDK_TYPE_COLOR,
	                                                     (GParamFlags) (G_PARAM_READABLE | G_PARAM_WRITABLE)));


	g_object_class_install_property (object_class,
	                                 PROP_EDITABLE,
	                                 g_param_spec_boolean ("editable",
	                                                       "Editable",
	                                                       "Whether the text can be modified by the user",
	                                                       FALSE,
	                                                       (GParamFlags) (G_PARAM_READABLE | G_PARAM_WRITABLE)));

	g_object_class_install_property (object_class,
	                                 PROP_FONT,
	                                 g_param_spec_string ("font",
	                                                      "Font",
	                                                      "Font description as a string",
	                                                      NULL,
	                                                      (GParamFlags) (G_PARAM_READABLE | G_PARAM_WRITABLE)));

	g_object_class_install_property (object_class,
	                                 PROP_FONT_DESC,
	                                 g_param_spec_boxed ("font_desc",
	                                                     "Font",
	                                                     "Font description as a PangoFontDescription struct",
	                                                     PANGO_TYPE_FONT_DESCRIPTION,
	                                                     (GParamFlags) (G_PARAM_READABLE | G_PARAM_WRITABLE)));


	g_object_class_install_property (object_class,
	                                 PROP_FAMILY,
	                                 g_param_spec_string ("family",
	                                                      "Font family",
	                                                      "Name of the font family, e.g. Sans, Helvetica, Times, Monospace",
	                                                      NULL,
	                                                      (GParamFlags) (G_PARAM_READABLE | G_PARAM_WRITABLE)));

	g_object_class_install_property (object_class,
	                                 PROP_STYLE,
	                                 g_param_spec_enum ("style",
	                                                    "Font style",
	                                                    "Font style",
	                                                    PANGO_TYPE_STYLE,
	                                                    PANGO_STYLE_NORMAL,
	                                                    (GParamFlags) (G_PARAM_READABLE | G_PARAM_WRITABLE)));

	g_object_class_install_property (object_class,
	                                 PROP_VARIANT,
	                                 g_param_spec_enum ("variant",
	                                                    "Font variant",
	                                                    "Font variant",
	                                                    PANGO_TYPE_VARIANT,
	                                                    PANGO_VARIANT_NORMAL,
	                                                    (GParamFlags) (G_PARAM_READABLE | G_PARAM_WRITABLE)));

	g_object_class_install_property (object_class,
	                                 PROP_WEIGHT,
	                                 g_param_spec_int ("weight",
	                                                   "Font weight",
	                                                   "Font weight",
	                                                   0,
	                                                   G_MAXINT,
	                                                   PANGO_WEIGHT_NORMAL,
	                                                   (GParamFlags) (G_PARAM_READABLE | G_PARAM_WRITABLE)));

	g_object_class_install_property (object_class,
	                                 PROP_STRETCH,
	                                 g_param_spec_enum ("stretch",
	                                                    "Font stretch",
	                                                    "Font stretch",
	                                                    PANGO_TYPE_STRETCH,
	                                                    PANGO_STRETCH_NORMAL,
	                                                    (GParamFlags) (G_PARAM_READABLE | G_PARAM_WRITABLE)));

	g_object_class_install_property (object_class,
	                                 PROP_SIZE,
	                                 g_param_spec_int ("size",
	                                                   "Font size",
	                                                   "Font size",
	                                                   0,
	                                                   G_MAXINT,
	                                                   0,
	                                                   (GParamFlags) (G_PARAM_READABLE | G_PARAM_WRITABLE)));

	g_object_class_install_property (object_class,
	                                 PROP_SIZE_POINTS,
	                                 g_param_spec_double ("size_points",
	                                                      "Font points",
	                                                      "Font size in points",
	                                                      0.0,
	                                                      G_MAXDOUBLE,
	                                                      0.0,
	                                                      (GParamFlags) (G_PARAM_READABLE | G_PARAM_WRITABLE)));

	g_object_class_install_property (object_class,
	                                 PROP_SCALE,
	                                 g_param_spec_double ("scale",
	                                                      "Font scale",
	                                                      "Font scaling factor",
	                                                      0.0,
	                                                      G_MAXDOUBLE,
	                                                      1.0,
	                                                      (GParamFlags) (G_PARAM_READABLE | G_PARAM_WRITABLE)));

	g_object_class_install_property (object_class,
	                                 PROP_RISE,
	                                 g_param_spec_int ("rise",
	                                                   "Rise",
	                                                   "Offset of text above the baseline (below the baseline if rise is negative)",
	                                                   -G_MAXINT,
	                                                   G_MAXINT,
	                                                   0,
	                                                   (GParamFlags) (G_PARAM_READABLE | G_PARAM_WRITABLE)));


	g_object_class_install_property (object_class,
	                                 PROP_STRIKETHROUGH,
	                                 g_param_spec_boolean ("strikethrough",
	                                                       "Strikethrough",
	                                                       "Whether to strike through the text",
	                                                       FALSE,
	                                                       (GParamFlags) (G_PARAM_READABLE | G_PARAM_WRITABLE)));

	g_object_class_install_property (object_class,
	                                 PROP_UNDERLINE,
	                                 g_param_spec_enum ("underline",
	                                                    "Underline",
	                                                    "Style of underline for this text",
	                                                    PANGO_TYPE_UNDERLINE,
	                                                    PANGO_UNDERLINE_NONE,
	                                                    (GParamFlags) (G_PARAM_READABLE | G_PARAM_WRITABLE)));

	g_object_class_install_property (object_class,
	                                 PROP_LANGUAGE,
	                                 g_param_spec_string ("language",
	                                                      "Language",
	                                                      "The language this text is in, as an ISO code. Pango can use this as a hint when rendering the text. If you don't understand this parameter, you probably don't need it",
	                                                      NULL,
	                                                      (GParamFlags) G_PARAM_READWRITE));


	/* Style props are set or not */

#define ADD_SET_PROP(propname, propval, nick, blurb) g_object_class_install_property (object_class, propval, g_param_spec_boolean (propname, nick, blurb, FALSE, (GParamFlags) (G_PARAM_READABLE | G_PARAM_WRITABLE)))

	ADD_SET_PROP ("background_set", PROP_BACKGROUND_SET,
	              "Background set",
	              "Whether this tag affects the background color");

	ADD_SET_PROP ("foreground_set", PROP_FOREGROUND_SET,
	              "Foreground set",
	              "Whether this tag affects the foreground color");

	ADD_SET_PROP ("editable_set", PROP_EDITABLE_SET,
	              "Editability set",
	              "Whether this tag affects text editability");

	ADD_SET_PROP ("family_set", PROP_FAMILY_SET,
	              "Font family set",
	              "Whether this tag affects the font family");

	ADD_SET_PROP ("style_set", PROP_STYLE_SET,
	              "Font style set",
	              "Whether this tag affects the font style");

	ADD_SET_PROP ("variant_set", PROP_VARIANT_SET,
	              "Font variant set",
	              "Whether this tag affects the font variant");

	ADD_SET_PROP ("weight_set", PROP_WEIGHT_SET,
	              "Font weight set",
	              "Whether this tag affects the font weight");

	ADD_SET_PROP ("stretch_set", PROP_STRETCH_SET,
	              "Font stretch set",
	              "Whether this tag affects the font stretch");

	ADD_SET_PROP ("size_set", PROP_SIZE_SET,
	              "Font size set",
	              "Whether this tag affects the font size");

	ADD_SET_PROP ("scale_set", PROP_SCALE_SET,
	              "Font scale set",
	              "Whether this tag scales the font size by a factor");

	ADD_SET_PROP ("rise_set", PROP_RISE_SET,
	              "Rise set",
	              "Whether this tag affects the rise");

	ADD_SET_PROP ("strikethrough_set", PROP_STRIKETHROUGH_SET,
	              "Strikethrough set",
	              "Whether this tag affects strikethrough");

	ADD_SET_PROP ("underline_set", PROP_UNDERLINE_SET,
	              "Underline set",
	              "Whether this tag affects underlining");

	ADD_SET_PROP ("language_set", PROP_LANGUAGE_SET,
	              "Language set",
	              "Whether this tag affects the language the text is rendered as");

	gv_signals[EDITED] = g_signal_new ("edited",
	                                   G_OBJECT_CLASS_TYPE (object_class),
	                                   G_SIGNAL_RUN_LAST,
	                                   G_STRUCT_OFFSET (MlViewCellRendererClass, edited),
	                                   NULL, NULL,
	                                   mlview_marshal_VOID__STRING_STRING,
	                                   G_TYPE_NONE, 2,
	                                   G_TYPE_STRING,
	                                   G_TYPE_STRING);

	gv_signals[WORD_CHANGED] = g_signal_new ("word-changed",
	                           G_OBJECT_CLASS_TYPE (object_class),
	                           G_SIGNAL_RUN_LAST,
	                           G_STRUCT_OFFSET (MlViewCellRendererClass, word_changed),
	                           NULL, NULL,
	                           mlview_marshal_VOID__POINTER_STRING_STRING_BOOLEAN_INT_INT_INT,
	                           G_TYPE_NONE, 7,
	                           G_TYPE_POINTER,
	                           G_TYPE_STRING,
	                           G_TYPE_STRING,
	                           G_TYPE_BOOLEAN,
	                           G_TYPE_INT,
	                           G_TYPE_INT,
	                           G_TYPE_INT);

	gv_signals[EDITING_HAS_STARTED] = g_signal_new ("editing-has-started",
	                                  G_OBJECT_CLASS_TYPE (object_class),
	                                  G_SIGNAL_RUN_LAST,
	                                  G_STRUCT_OFFSET (MlViewCellRendererClass, editing_has_started),
	                                  NULL, NULL,
	                                  mlview_marshal_VOID__POINTER_POINTER,
	                                  G_TYPE_NONE,2,
	                                  G_TYPE_POINTER,
	                                  G_TYPE_POINTER) ;

	gv_signals[SELECT_EDITABLE_REGION] = g_signal_new ("select_editable_region",
	                                     G_OBJECT_CLASS_TYPE (object_class),
	                                     G_SIGNAL_RUN_LAST,
	                                     G_STRUCT_OFFSET (MlViewCellRendererClass,
	                                                      select_editable_region),
	                                     NULL, NULL,
	                                     mlview_marshal_BOOLEAN__POINTER,
	                                     G_TYPE_BOOLEAN,1,
	                                     G_TYPE_POINTER) ;
}

static void
mlview_cell_renderer_get_property (GObject *a_this,
                                   guint a_param_id,
                                   GValue *a_value,
                                   GParamSpec *a_param_spec)
{
	MlViewCellRenderer *thiz = NULL ;

	g_return_if_fail (a_this
	                  && MLVIEW_IS_CELL_RENDERER (a_this)) ;

	thiz = MLVIEW_CELL_RENDERER (a_this) ;
	g_return_if_fail (thiz && PRIVATE (thiz)) ;

	switch (a_param_id) {
	case PROP_IS_COMPLETION_ON:
		g_value_set_boolean (a_value,
		                     PRIVATE (thiz)->is_completion_on) ;

		break ;
	case PROP_ATTRIBUTES:
		g_value_set_boxed (a_value, PRIVATE (thiz)->extra_attrs);
		break;

	case PROP_SINGLE_PARAGRAPH_MODE:
		g_value_set_boolean (a_value, PRIVATE (thiz)->single_paragraph);
		break;

	case PROP_BACKGROUND_GDK: {
			GdkColor color;

			color.red = PRIVATE (thiz)->background.red;
			color.green = PRIVATE (thiz)->background.green;
			color.blue = PRIVATE (thiz)->background.blue;

			g_value_set_boxed (a_value, &color);
		}
		break;

	case PROP_FOREGROUND_GDK: {
			GdkColor color;

			color.red = PRIVATE (thiz)->foreground.red;
			color.green = PRIVATE (thiz)->foreground.green;
			color.blue = PRIVATE (thiz)->foreground.blue;

			g_value_set_boxed (a_value, &color);
		}
		break;

	case PROP_FONT: {
			/* FIXME GValue imposes a totally gratuitous string copy
			 * here, we could just hand off string ownership
			 */
			gchar *str = pango_font_description_to_string (PRIVATE (thiz)->font);
			g_value_set_string (a_value, str);
			g_free (str);
		}
		break;

	case PROP_FONT_DESC:
		g_value_set_boxed (a_value, PRIVATE (thiz)->font);
		break;

	case PROP_FAMILY:
		g_value_set_string (a_value, pango_font_description_get_family (PRIVATE (thiz)->font));
		break;

	case PROP_STYLE:
		g_value_set_enum (a_value, pango_font_description_get_style (PRIVATE (thiz)->font));
		break;

	case PROP_VARIANT:
		g_value_set_enum (a_value, pango_font_description_get_variant (PRIVATE (thiz)->font));
		break;

	case PROP_WEIGHT:
		g_value_set_int (a_value, pango_font_description_get_weight (PRIVATE (thiz)->font));
		break;

	case PROP_STRETCH:
		g_value_set_enum (a_value, pango_font_description_get_stretch (PRIVATE (thiz)->font));
		break;

	case PROP_SIZE:
		g_value_set_int (a_value, pango_font_description_get_size (PRIVATE (thiz)->font));
		break;

	case PROP_SIZE_POINTS:
		g_value_set_double (a_value, ((double)pango_font_description_get_size (PRIVATE (thiz)->font)) / (double)PANGO_SCALE);
		break;

	case PROP_SCALE:
		g_value_set_double (a_value, PRIVATE (thiz)->font_scale);
		break;

	case PROP_EDITABLE:
		g_value_set_boolean (a_value, PRIVATE (thiz)->editable);
		break;

	case PROP_STRIKETHROUGH:
		g_value_set_boolean (a_value, PRIVATE (thiz)->strikethrough);
		break;

	case PROP_UNDERLINE:
		g_value_set_enum (a_value, PRIVATE (thiz)->underline_style);
		break;

	case PROP_RISE:
		g_value_set_int (a_value, PRIVATE (thiz)->rise);
		break;

	case PROP_LANGUAGE:
		g_value_set_string (a_value, pango_language_to_string (PRIVATE (thiz)->language));
		break;

	case PROP_BACKGROUND_SET:
		g_value_set_boolean (a_value, PRIVATE (thiz)->background_set);
		break;

	case PROP_FOREGROUND_SET:
		g_value_set_boolean (a_value, PRIVATE (thiz)->foreground_set);
		break;

	case PROP_FAMILY_SET:
	case PROP_STYLE_SET:
	case PROP_VARIANT_SET:
	case PROP_WEIGHT_SET:
	case PROP_STRETCH_SET:
	case PROP_SIZE_SET: {
			PangoFontMask mask = get_property_font_set_mask (a_param_id);
			g_value_set_boolean (a_value,
			                     (pango_font_description_get_set_fields
			                      (PRIVATE (thiz)->font) & mask) != 0);

			break;
		}

	case PROP_SCALE_SET:
		g_value_set_boolean (a_value, PRIVATE (thiz)->scale_set);
		break;

	case PROP_EDITABLE_SET:
		g_value_set_boolean (a_value, PRIVATE (thiz)->editable_set);
		break;

	case PROP_STRIKETHROUGH_SET:
		g_value_set_boolean (a_value, PRIVATE (thiz)->strikethrough_set);
		break;

	case PROP_UNDERLINE_SET:
		g_value_set_boolean (a_value, PRIVATE (thiz)->underline_set);
		break;

	case  PROP_RISE_SET:
		g_value_set_boolean (a_value, PRIVATE (thiz)->rise_set);
		break;

	case PROP_LANGUAGE_SET:
		g_value_set_boolean (a_value, PRIVATE (thiz)->language_set);
		break;

	case PROP_BACKGROUND:
	case PROP_FOREGROUND:
	case PROP_MARKUP:
	default :
		G_OBJECT_WARN_INVALID_PROPERTY_ID (a_this, a_param_id,
		                                   a_param_spec);
		break ;
	}
}

static void
mlview_cell_renderer_set_property (GObject *a_this,
                                   guint a_param_id,
                                   const GValue *a_value,
                                   GParamSpec *a_param_spec)
{
	MlViewCellRenderer *thiz = NULL ;

	g_return_if_fail (a_this
	                  && MLVIEW_IS_CELL_RENDERER (a_this)) ;
	thiz = MLVIEW_CELL_RENDERER (a_this) ;
	g_return_if_fail (thiz && PRIVATE (thiz)) ;

	switch (a_param_id) {
	case PROP_IS_COMPLETION_ON:
		g_value_set_boolean ((GValue*)a_value,
		                     PRIVATE (thiz)->is_completion_on) ;
		break ;
	case PROP_TEXT:
		if (PRIVATE (thiz)->text)
			g_free (PRIVATE (thiz)->text);

		if (PRIVATE (thiz)->markup_set) {
			if (PRIVATE (thiz)->extra_attrs)
				pango_attr_list_unref (PRIVATE (thiz)->extra_attrs);
			PRIVATE (thiz)->extra_attrs = NULL;
			PRIVATE (thiz)->markup_set = FALSE;
		}

		PRIVATE (thiz)->text = g_strdup (g_value_get_string (a_value));
		g_object_notify (a_this, "text");
		break;

	case PROP_ATTRIBUTES:
		if (PRIVATE (thiz)->extra_attrs)
			pango_attr_list_unref (PRIVATE (thiz)->extra_attrs);

		PRIVATE (thiz)->extra_attrs = (PangoAttrList *) g_value_get_boxed (a_value);
		if (PRIVATE (thiz)->extra_attrs)
			pango_attr_list_ref (PRIVATE (thiz)->extra_attrs);
		break;
	case PROP_MARKUP: {
			const gchar *str;
			gchar *text = NULL;
			GError *error = NULL;
			PangoAttrList *attrs = NULL;

			str = g_value_get_string (a_value);
			if (str && !pango_parse_markup (str,
			                                -1,
			                                0,
			                                &attrs,
			                                &text,
			                                NULL,
			                                &error)) {
				g_warning ("Failed to set cell text from markup due to error parsing markup: %s",
				           error->message);
				g_error_free (error);
				return;
			}

			if (PRIVATE (thiz)->text)
				g_free (PRIVATE (thiz)->text);

			if (PRIVATE (thiz)->extra_attrs)
				pango_attr_list_unref (PRIVATE (thiz)->extra_attrs);

			PRIVATE (thiz)->text = text;
			PRIVATE (thiz)->extra_attrs = attrs;
			PRIVATE (thiz)->markup_set = TRUE;
		}
		break;

	case PROP_SINGLE_PARAGRAPH_MODE:
		PRIVATE (thiz)->single_paragraph = g_value_get_boolean (a_value);
		break;

	case PROP_BACKGROUND: {
			GdkColor color;

			if (!g_value_get_string (a_value))
				set_bg_color (thiz, NULL);       /* reset to backgrounmd_set to FALSE */
			else if (gdk_color_parse (g_value_get_string (a_value), &color))
				set_bg_color (thiz, &color);
			else
				g_warning ("Don't know color `%s'", g_value_get_string (a_value));

			g_object_notify (a_this, "background_gdk");
		}
		break;

	case PROP_FOREGROUND: {
			GdkColor color;

			if (!g_value_get_string (a_value))
				set_fg_color (thiz, NULL);       /* reset to foreground_set to FALSE */
			else if (gdk_color_parse (g_value_get_string (a_value), &color))
				set_fg_color (thiz, &color);
			else
				g_warning ("Don't know color `%s'", g_value_get_string (a_value));

			g_object_notify (a_this, "foreground_gdk");
		}
		break;

	case PROP_BACKGROUND_GDK:
		/* This notifies the GObject itself. */
		set_bg_color (thiz, (GdkColor *) g_value_get_boxed (a_value));
		break;

	case PROP_FOREGROUND_GDK:
		/* This notifies the GObject itself. */
		set_fg_color (thiz, (GdkColor *) g_value_get_boxed (a_value));
		break;

	case PROP_FONT: {
			PangoFontDescription *font_desc = NULL;
			const gchar *name;

			name = g_value_get_string (a_value);

			if (name)
				font_desc = pango_font_description_from_string (name);

			set_font_description (thiz, font_desc);

			if (PRIVATE (thiz)->fixed_height_rows != -1)
				PRIVATE (thiz)->calc_fixed_height = TRUE;
		}
		break;

	case PROP_FONT_DESC:
		set_font_description (thiz, (PangoFontDescription *) g_value_get_boxed (a_value));

		if (PRIVATE (thiz)->fixed_height_rows != -1)
			PRIVATE (thiz)->calc_fixed_height = TRUE;
		break;

	case PROP_FAMILY:
	case PROP_STYLE:
	case PROP_VARIANT:
	case PROP_WEIGHT:
	case PROP_STRETCH:
	case PROP_SIZE:
	case PROP_SIZE_POINTS: {
			PangoFontMask old_set_mask = pango_font_description_get_set_fields (PRIVATE (thiz)->font);

			switch (a_param_id) {
			case PROP_FAMILY:
				pango_font_description_set_family (PRIVATE (thiz)->font,
				                                   g_value_get_string (a_value));
				break;
			case PROP_STYLE:
				pango_font_description_set_style (PRIVATE (thiz)->font,
				                                  (PangoStyle) g_value_get_enum (a_value));
				break;
			case PROP_VARIANT:
				pango_font_description_set_variant (PRIVATE (thiz)->font,
				                                    (PangoVariant) g_value_get_enum (a_value));
				break;
			case PROP_WEIGHT:
				pango_font_description_set_weight (PRIVATE (thiz)->font,
				                                   (PangoWeight) g_value_get_int (a_value));
				break;
			case PROP_STRETCH:
				pango_font_description_set_stretch (PRIVATE (thiz)->font,
				                                    (PangoStretch) g_value_get_enum (a_value));
				break;
			case PROP_SIZE:
				pango_font_description_set_size (PRIVATE (thiz)->font,
				                                 g_value_get_int (a_value));
				g_object_notify (a_this, "size_points");
				break;
			case PROP_SIZE_POINTS:
				pango_font_description_set_size (PRIVATE (thiz)->font,
				                                 (gint)g_value_get_double (a_value) * PANGO_SCALE);
				g_object_notify (a_this, "size");
				break;
			}

			if (PRIVATE (thiz)->fixed_height_rows != -1)
				PRIVATE (thiz)->calc_fixed_height = TRUE;

			notify_set_changed (a_this, (PangoFontMask) (old_set_mask & pango_font_description_get_set_fields (PRIVATE (thiz)->font)));
			g_object_notify (a_this, "font_desc");
			g_object_notify (a_this, "font");

			break;
		}

	case PROP_SCALE:
		PRIVATE (thiz)->font_scale = g_value_get_double (a_value);
		PRIVATE (thiz)->scale_set = TRUE;
		if (PRIVATE (thiz)->fixed_height_rows != -1)
			PRIVATE (thiz)->calc_fixed_height = TRUE;
		g_object_notify (a_this, "scale_set");
		break;

	case PROP_EDITABLE:
		PRIVATE (thiz)->editable = g_value_get_boolean (a_value);
		PRIVATE (thiz)->editable_set = TRUE;
		if (PRIVATE (thiz)->editable)
			GTK_CELL_RENDERER (thiz)->mode = GTK_CELL_RENDERER_MODE_EDITABLE;
		else
			GTK_CELL_RENDERER (thiz)->mode = GTK_CELL_RENDERER_MODE_INERT;
		g_object_notify (a_this, "editable_set");
		break;

	case PROP_STRIKETHROUGH:
		PRIVATE (thiz)->strikethrough = g_value_get_boolean (a_value);
		PRIVATE (thiz)->strikethrough_set = TRUE;
		g_object_notify (a_this, "strikethrough_set");
		break;

	case PROP_UNDERLINE:
		PRIVATE (thiz)->underline_style = (PangoUnderline) g_value_get_enum (a_value);
		PRIVATE (thiz)->underline_set = TRUE;
		g_object_notify (a_this, "underline_set");

		break;

	case PROP_RISE:
		PRIVATE (thiz)->rise = g_value_get_int (a_value);
		PRIVATE (thiz)->rise_set = TRUE;
		g_object_notify (a_this, "rise_set");
		if (PRIVATE (thiz)->fixed_height_rows != -1)
			PRIVATE (thiz)->calc_fixed_height = TRUE;
		break;

	case PROP_LANGUAGE:
		PRIVATE (thiz)->language_set = TRUE;
		if (PRIVATE (thiz)->language)
			g_object_unref (PRIVATE (thiz)->language);
		PRIVATE (thiz)->language = pango_language_from_string (g_value_get_string (a_value));
		g_object_notify (a_this, "language_set");
		break;

	case PROP_BACKGROUND_SET:
		PRIVATE (thiz)->background_set = g_value_get_boolean (a_value);
		break;

	case PROP_FOREGROUND_SET:
		PRIVATE (thiz)->foreground_set = g_value_get_boolean (a_value);
		break;

	case PROP_FAMILY_SET:
	case PROP_STYLE_SET:
	case PROP_VARIANT_SET:
	case PROP_WEIGHT_SET:
	case PROP_STRETCH_SET:
	case PROP_SIZE_SET:
		if (!g_value_get_boolean (a_value)) {
			pango_font_description_unset_fields (PRIVATE (thiz)->font,
			                                     get_property_font_set_mask (a_param_id));
		} else {
			PangoFontMask changed_mask;

			changed_mask = set_font_desc_fields (PRIVATE (thiz)->font,
			                                     get_property_font_set_mask (a_param_id));
			notify_fields_changed (G_OBJECT (thiz), changed_mask);
		}
		break;

	case PROP_SCALE_SET:
		PRIVATE (thiz)->scale_set = g_value_get_boolean (a_value);
		break;

	case PROP_EDITABLE_SET:
		PRIVATE (thiz)->editable_set = g_value_get_boolean (a_value);
		break;

	case PROP_STRIKETHROUGH_SET:
		PRIVATE (thiz)->strikethrough_set = g_value_get_boolean (a_value);
		break;

	case PROP_UNDERLINE_SET:
		PRIVATE (thiz)->underline_set = g_value_get_boolean (a_value);
		break;

	case PROP_RISE_SET:
		PRIVATE (thiz)->rise_set = g_value_get_boolean (a_value);
		break;

	case PROP_LANGUAGE_SET:
		PRIVATE (thiz)->language_set = g_value_get_boolean (a_value);
		break;

	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (a_this, a_param_id,
		                                   a_param_spec);
		break;
		break ;
	}
}

static void
mlview_cell_renderer_init (MlViewCellRenderer *a_this)
{
	if (PRIVATE (a_this))
		return ;

	g_return_if_fail (a_this && MLVIEW_IS_CELL_RENDERER (a_this)) ;

	PRIVATE (a_this) = (MlViewCellRendererPrivate *) g_try_malloc (sizeof (MlViewCellRendererPrivate)) ;
	if (!PRIVATE (a_this)) {
		mlview_utils_trace_debug ("Couldn't instanciate MlViewCellRenderer. System may be out of memory") ;
		return ;
	}
	memset (PRIVATE (a_this), 0, sizeof (MlViewCellRendererPrivate)) ;

	GTK_CELL_RENDERER (a_this)->xalign = 0.0;
	GTK_CELL_RENDERER (a_this)->yalign = 0.5;
	GTK_CELL_RENDERER (a_this)->xpad = 2;
	GTK_CELL_RENDERER (a_this)->ypad = 2;

	PRIVATE (a_this)->fixed_height_rows = -1;
	PRIVATE (a_this)->font = pango_font_description_new ();
}


static void
mlview_cell_renderer_dispose (GObject *a_this)
{
	MlViewCellRenderer *thiz = NULL ;

	g_return_if_fail (a_this && MLVIEW_IS_CELL_RENDERER (a_this)) ;
	thiz = MLVIEW_CELL_RENDERER (a_this) ;
	g_return_if_fail (thiz && PRIVATE (thiz)) ;

	if (PRIVATE (thiz)->dispose_has_run == TRUE)
		return ;

	if (PRIVATE (thiz)->text) {
		g_free (PRIVATE (thiz)->text) ;
		PRIVATE (thiz)->text = NULL ;
	}
	if (PRIVATE (thiz)->extra_attrs) {
		pango_attr_list_unref (PRIVATE (thiz)->extra_attrs) ;
		PRIVATE (thiz)->extra_attrs = NULL ;
	}
	if (PRIVATE (thiz)->language) {
		g_object_unref (PRIVATE (thiz)->language) ;
		PRIVATE (thiz)->language = NULL ;
	}

	if (G_OBJECT_CLASS (gv_parent_class)->dispose) {
		G_OBJECT_CLASS (gv_parent_class)->dispose (a_this) ;
	}
	PRIVATE (thiz)->dispose_has_run = TRUE ;
}

static void
mlview_cell_renderer_finalize (GObject *a_this)
{
	MlViewCellRenderer *thiz = NULL ;

	g_return_if_fail (a_this && MLVIEW_IS_CELL_RENDERER (a_this)) ;
	thiz = MLVIEW_CELL_RENDERER (a_this) ;
	g_return_if_fail (thiz && PRIVATE (thiz)) ;
	g_free (PRIVATE (thiz)) ;
	PRIVATE (thiz) = NULL ;

	if (G_OBJECT_CLASS (gv_parent_class)->finalize) {
		G_OBJECT_CLASS (gv_parent_class)->finalize (a_this) ;
	}
}

/*****************
 *Event callbacks
 ****************/

static void
mlview_cell_renderer_editing_done_cb (GtkCellEditable *a_entry,
                                      gpointer         a_data)
{
	const gchar *path;
	const gchar *new_text;

	MlViewCellRenderer *thiz = NULL ;

	thiz = MLVIEW_CELL_RENDERER (a_data) ;
	g_return_if_fail (thiz && PRIVATE (thiz)) ;


	if (PRIVATE (thiz)->focus_out_id > 0) {
		g_signal_handler_disconnect (a_entry, PRIVATE (thiz)->focus_out_id);
		PRIVATE (thiz)->focus_out_id = 0;
	}

	if (GTK_ENTRY (a_entry)->editing_canceled) {
		gtk_cell_renderer_editing_canceled (GTK_CELL_RENDERER (a_data));
		return;
	}

	path = (const gchar *) g_object_get_data (G_OBJECT (a_entry),
	        MLVIEW_CELL_RENDERER_PATH);
	new_text = gtk_entry_get_text (GTK_ENTRY (a_entry));

	g_signal_emit (a_data, gv_signals[EDITED],
	               0, path, new_text);
}

static gboolean
mlview_cell_renderer_focus_out_event_cb (GtkWidget *a_entry,
        GdkEvent  *a_event,
        gpointer   a_data)
{
	mlview_cell_renderer_editing_done_cb (GTK_CELL_EDITABLE (a_entry),
	                                      a_data);

	/* entry needs focus-out-event */
	return FALSE;
}

/*
 *TODO: finish this.
 */
static void
mlview_cell_renderer_insert_text_cb (GtkEditable *a_this,
                                     gchar *a_new_text,
                                     gint a_new_text_len,
                                     gint *a_position,
                                     gpointer a_user_data)
{
	MlViewCellRenderer *thiz = NULL ;
	GString *stringue = NULL ;
	gchar *str = NULL, *word_start = NULL, *word_end = NULL ;

	g_return_if_fail (a_this &&  GTK_IS_ENTRY (a_this)) ;

	if (a_user_data) {
		g_return_if_fail (MLVIEW_IS_CELL_RENDERER (a_user_data)) ;
		thiz = MLVIEW_CELL_RENDERER (a_user_data) ;
	}
	str = gtk_editable_get_chars (a_this, 0, -1) ;
	stringue = g_string_new (str) ;
	stringue = g_string_insert_len (stringue, *a_position,
	                                a_new_text, a_new_text_len) ;

	/*get the extents of the word being edited*/
	mlview_utils_get_current_word_bounds (stringue->str, stringue->len,
	                                      *a_position, &word_start,
	                                      &word_end) ;

	g_signal_emit (thiz, gv_signals[WORD_CHANGED],
	               0, a_this, word_start, word_end,
	               TRUE, *a_position,
	               word_start - stringue->str,
	               word_end - stringue->str) ;

	if (stringue) {
		g_string_free (stringue, TRUE) ;
		stringue = NULL ;
	}
}



/**********************
 *Misc private methods
 *********************/



static void
notify_fields_changed (GObject       *a_this,
                       PangoFontMask  a_changed_mask)
{
	if (a_changed_mask & PANGO_FONT_MASK_FAMILY)
		g_object_notify (a_this, "family");
	if (a_changed_mask & PANGO_FONT_MASK_STYLE)
		g_object_notify (a_this, "style");
	if (a_changed_mask & PANGO_FONT_MASK_VARIANT)
		g_object_notify (a_this, "variant");
	if (a_changed_mask & PANGO_FONT_MASK_WEIGHT)
		g_object_notify (a_this, "weight");
	if (a_changed_mask & PANGO_FONT_MASK_STRETCH)
		g_object_notify (a_this, "stretch");
	if (a_changed_mask & PANGO_FONT_MASK_SIZE)
		g_object_notify (a_this, "size");
}

static PangoFontMask
set_font_desc_fields (PangoFontDescription *a_desc,
                      PangoFontMask         a_to_set)
{
	PangoFontMask changed_mask = (PangoFontMask) 0;

	if (a_to_set & PANGO_FONT_MASK_FAMILY) {
		const char *family = pango_font_description_get_family
		                     (a_desc);
		if (!family) {
			family = "sans";
			changed_mask = (PangoFontMask) (changed_mask | PANGO_FONT_MASK_FAMILY);
		}

		pango_font_description_set_family (a_desc, family);
	}
	if (a_to_set & PANGO_FONT_MASK_STYLE)
		pango_font_description_set_style (a_desc, pango_font_description_get_style (a_desc));
	if (a_to_set & PANGO_FONT_MASK_VARIANT)
		pango_font_description_set_variant (a_desc, pango_font_description_get_variant (a_desc));
	if (a_to_set & PANGO_FONT_MASK_WEIGHT)
		pango_font_description_set_weight (a_desc, pango_font_description_get_weight (a_desc));
	if (a_to_set & PANGO_FONT_MASK_STRETCH)
		pango_font_description_set_stretch (a_desc, pango_font_description_get_stretch (a_desc));
	if (a_to_set & PANGO_FONT_MASK_SIZE) {
		gint size = pango_font_description_get_size (a_desc);
		if (size <= 0) {
			size = 10 * PANGO_SCALE;
			changed_mask = (PangoFontMask) (changed_mask | PANGO_FONT_MASK_SIZE);
		}

		pango_font_description_set_size (a_desc, size);
	}

	return changed_mask;
}

static void
notify_set_changed (GObject       *a_this,
                    PangoFontMask  a_changed_mask)
{
	if (a_changed_mask & PANGO_FONT_MASK_FAMILY)
		g_object_notify (a_this, "family_set");
	if (a_changed_mask & PANGO_FONT_MASK_STYLE)
		g_object_notify (a_this, "style_set");
	if (a_changed_mask & PANGO_FONT_MASK_VARIANT)
		g_object_notify (a_this, "variant_set");
	if (a_changed_mask & PANGO_FONT_MASK_WEIGHT)
		g_object_notify (a_this, "weight_set");
	if (a_changed_mask & PANGO_FONT_MASK_STRETCH)
		g_object_notify (a_this, "stretch_set");
	if (a_changed_mask & PANGO_FONT_MASK_SIZE)
		g_object_notify (a_this, "size_set");
}

static void
set_font_description (MlViewCellRenderer  *a_this,
                      PangoFontDescription *a_font_desc)
{
	GObject *object = G_OBJECT (a_this);
	PangoFontDescription *new_font_desc;
	PangoFontMask old_mask, new_mask, changed_mask, set_changed_mask;

	if (a_font_desc)
		new_font_desc = pango_font_description_copy (a_font_desc);
	else
		new_font_desc = pango_font_description_new ();

	old_mask = pango_font_description_get_set_fields (PRIVATE (a_this)->font);
	new_mask = pango_font_description_get_set_fields (new_font_desc);

	changed_mask = (PangoFontMask) (old_mask | new_mask);
	set_changed_mask = (PangoFontMask) (old_mask ^ new_mask);

	pango_font_description_free (PRIVATE (a_this)->font);
	PRIVATE (a_this)->font = new_font_desc;

	g_object_freeze_notify (object);

	g_object_notify (object, "font_desc");
	g_object_notify (object, "font");

	if (changed_mask & PANGO_FONT_MASK_FAMILY)
		g_object_notify (object, "family");
	if (changed_mask & PANGO_FONT_MASK_STYLE)
		g_object_notify (object, "style");
	if (changed_mask & PANGO_FONT_MASK_VARIANT)
		g_object_notify (object, "variant");
	if (changed_mask & PANGO_FONT_MASK_WEIGHT)
		g_object_notify (object, "weight");
	if (changed_mask & PANGO_FONT_MASK_STRETCH)
		g_object_notify (object, "stretch");
	if (changed_mask & PANGO_FONT_MASK_SIZE) {
		g_object_notify (object, "size");
		g_object_notify (object, "size_points");
	}

	notify_set_changed (object, set_changed_mask);

	g_object_thaw_notify (object);
}

static void
set_fg_color (MlViewCellRenderer *a_this,
              GdkColor            *a_color)
{
	if (a_color) {
		if (!PRIVATE (a_this)->foreground_set) {
			PRIVATE (a_this)->foreground_set = TRUE;
			g_object_notify (G_OBJECT (a_this), "foreground_set");
		}

		PRIVATE (a_this)->foreground.red = a_color->red;
		PRIVATE (a_this)->foreground.green = a_color->green;
		PRIVATE (a_this)->foreground.blue = a_color->blue;
	} else {
		if (PRIVATE (a_this)->foreground_set) {
			PRIVATE (a_this)->foreground_set = FALSE;
			g_object_notify (G_OBJECT (a_this), "foreground_set");
		}
	}
}

static void
set_bg_color (MlViewCellRenderer *a_this,
              GdkColor            *a_color)
{
	if (a_color) {
		if (!PRIVATE (a_this)->background_set) {
			PRIVATE (a_this)->background_set = TRUE;
			g_object_notify (G_OBJECT (a_this), "background_set");
		}

		PRIVATE (a_this)->background.red = a_color->red;
		PRIVATE (a_this)->background.green = a_color->green;
		PRIVATE (a_this)->background.blue = a_color->blue;
	} else {
		if (PRIVATE (a_this)->background_set) {
			PRIVATE (a_this)->background_set = FALSE;
			g_object_notify (G_OBJECT (a_this), "background_set");
		}
	}
}

static void
add_attr (PangoAttrList  *a_attr_list,
          PangoAttribute *a_attr)
{
	a_attr->start_index = 0;
	a_attr->end_index = G_MAXINT;

	pango_attr_list_insert (a_attr_list, a_attr);
}

static PangoFontMask
get_property_font_set_mask (guint a_prop_id)
{
	switch (a_prop_id) {
	case PROP_FAMILY_SET:
		return PANGO_FONT_MASK_FAMILY;
	case PROP_STYLE_SET:
		return PANGO_FONT_MASK_STYLE;
	case PROP_VARIANT_SET:
		return PANGO_FONT_MASK_VARIANT;
	case PROP_WEIGHT_SET:
		return PANGO_FONT_MASK_WEIGHT;
	case PROP_STRETCH_SET:
		return PANGO_FONT_MASK_STRETCH;
	case PROP_SIZE_SET:
		return PANGO_FONT_MASK_SIZE;
	}

	return (PangoFontMask) 0;
}

static PangoLayout*
get_layout (MlViewCellRenderer   *a_this,
            GtkWidget            *widget,
            gboolean             will_render,
            GtkCellRendererState flags)
{
	PangoAttrList *attr_list;
	PangoLayout *layout;
	PangoUnderline uline;
	PangoFontMetrics *font_metrics = NULL ;
	PangoContext *pango_context = NULL ;
	PangoFontDescription *font_desc = NULL ;
	gint char_width = 0 ;

	layout = gtk_widget_create_pango_layout (widget, PRIVATE (a_this)->text);

	if (PRIVATE (a_this)->extra_attrs)
		attr_list = pango_attr_list_copy (PRIVATE (a_this)->extra_attrs);
	else
		attr_list = pango_attr_list_new ();

	pango_layout_set_single_paragraph_mode (layout, PRIVATE(a_this)->single_paragraph);

	if (will_render) {
		/* Add options that affect appearance but not size */

		/* note that background doesn't go here, since it affects
		 * background_area not the PangoLayout area
		 */

		if (PRIVATE (a_this)->foreground_set) {
			PangoColor color;

			color = PRIVATE (a_this)->foreground;

			add_attr (attr_list,
			          pango_attr_foreground_new (color.red, color.green, color.blue));
		}

		if (PRIVATE (a_this)->strikethrough_set)
			add_attr (attr_list,
			          pango_attr_strikethrough_new (PRIVATE (a_this)->strikethrough));
	}
	add_attr (attr_list, pango_attr_font_desc_new (PRIVATE (a_this)->font));

	if (PRIVATE (a_this)->scale_set &&
	        PRIVATE (a_this)->font_scale != 1.0)
		add_attr (attr_list, pango_attr_scale_new (PRIVATE (a_this)->font_scale));

	if (PRIVATE (a_this)->underline_set)
		uline = PRIVATE (a_this)->underline_style;
	else
		uline = PANGO_UNDERLINE_NONE;

	if (PRIVATE (a_this)->language_set)
		add_attr (attr_list, pango_attr_language_new (PRIVATE (a_this)->language));

	if ((flags & GTK_CELL_RENDERER_PRELIT) == GTK_CELL_RENDERER_PRELIT) {
		switch (uline) {
		case PANGO_UNDERLINE_NONE:
			uline = PANGO_UNDERLINE_SINGLE;
			break;

		case PANGO_UNDERLINE_SINGLE:
			uline = PANGO_UNDERLINE_DOUBLE;
			break;

		default:
			break;
		}
	}

	if (uline != PANGO_UNDERLINE_NONE)
		add_attr (attr_list, pango_attr_underline_new (PRIVATE (a_this)->underline_style));

	if (PRIVATE (a_this)->rise_set)
		add_attr (attr_list, pango_attr_rise_new (PRIVATE (a_this)->rise));

	pango_layout_set_attributes (layout, attr_list);

	/*here, set the wrapping*/
	font_desc = pango_font_description_copy (widget->style->font_desc) ;
	pango_context = gtk_widget_get_pango_context (widget) ;
	font_metrics = pango_context_get_metrics (pango_context, font_desc,
	               pango_context_get_language
	               (pango_context)) ;
	char_width = pango_font_metrics_get_approximate_char_width (font_metrics) ;
	if (font_desc) {
		pango_font_description_free (font_desc) ;
		font_desc = NULL ;
	}
	if (font_metrics) {
		pango_font_metrics_unref (font_metrics) ;
		font_metrics = NULL ;
	}

	pango_layout_set_width (layout, char_width * 85);

	pango_attr_list_unref (attr_list);

	return layout;
}

static void
mlview_cell_renderer_get_size (GtkCellRenderer      *a_cell,
                               GtkWidget            *a_widget,
                               GdkRectangle         *a_cell_area,
                               gint                 *a_x_offset,
                               gint                 *a_y_offset,
                               gint                 *a_width,
                               gint                 *a_height)
{

	PangoRectangle rect;
	PangoLayout *layout ;

	MlViewCellRenderer *thiz = NULL ;

	g_return_if_fail (a_cell && MLVIEW_IS_CELL_RENDERER (a_cell)) ;
	thiz = MLVIEW_CELL_RENDERER (a_cell) ;

	if (PRIVATE (thiz)->calc_fixed_height) {
		PangoContext *context;
		PangoFontMetrics *metrics;
		PangoFontDescription *font_desc;
		gint row_height;

		font_desc = pango_font_description_copy (a_widget->style->font_desc);
		pango_font_description_merge (font_desc, PRIVATE (thiz)->font, TRUE);

		if (PRIVATE (thiz)->scale_set) {
			pango_font_description_set_size (font_desc,
			                                 (gint)PRIVATE (thiz)->font_scale * pango_font_description_get_size (font_desc));
		}
		context = gtk_widget_get_pango_context (a_widget);

		metrics = pango_context_get_metrics (context,
		                                     font_desc,
		                                     pango_context_get_language (context));
		row_height = (pango_font_metrics_get_ascent (metrics) +
		              pango_font_metrics_get_descent (metrics));
		pango_font_metrics_unref (metrics);

		gtk_cell_renderer_set_fixed_size (a_cell,
		                                  a_cell->width, 2 * a_cell->ypad +
		                                  PRIVATE (thiz)->fixed_height_rows * PANGO_PIXELS (row_height));

		if (a_height) {
			*a_height = a_cell->height;
			a_height = NULL;
		}
		PRIVATE (thiz)->calc_fixed_height = FALSE;
		if (a_width == NULL)
			return;
	}
	layout = get_layout (thiz, a_widget, FALSE, (GtkCellRendererState) 0);
	pango_layout_get_pixel_extents (layout, NULL, &rect);

	if (a_width)
		*a_width = GTK_CELL_RENDERER (thiz)->xpad * 2 + rect.width;

	if (a_height)
		*a_height = GTK_CELL_RENDERER (thiz)->ypad * 2 + rect.height;

	if (a_cell_area) {
		if (a_x_offset) {
			*a_x_offset = ((gtk_widget_get_direction (a_widget) == GTK_TEXT_DIR_RTL) ?
			               (gint)(1.0 - a_cell->xalign)
			               :
			               (gint) a_cell->xalign) * (a_cell_area->width - rect.width - (2 * a_cell->xpad));
			*a_x_offset = MAX (*a_x_offset, 0);
		}
		if (a_y_offset) {
			*a_y_offset = (gint) a_cell->yalign * (a_cell_area->height - rect.height - (2 * a_cell->ypad));
			*a_y_offset = MAX (*a_y_offset, 0);
		}
	}

	g_object_unref (layout);
}


static void
mlview_cell_renderer_render (GtkCellRenderer      *a_this,
                             GdkDrawable          *a_window,
                             GtkWidget            *a_widget,
                             GdkRectangle         *a_background_area,
                             GdkRectangle         *a_cell_area,
                             GdkRectangle         *a_expose_area,
                             GtkCellRendererState  a_flags)

{
	PangoLayout *layout;
	GtkStateType state;
	gint x_offset;
	gint y_offset;
	MlViewCellRenderer *thiz = NULL ;

	g_return_if_fail (a_this
	                  && MLVIEW_IS_CELL_RENDERER (a_this)) ;

	thiz = MLVIEW_CELL_RENDERER (a_this) ;
	g_return_if_fail (thiz && PRIVATE (thiz)) ;

	layout = get_layout (thiz, a_widget, TRUE, a_flags);

	mlview_cell_renderer_get_size (a_this, a_widget, a_cell_area,
	                               &x_offset, &y_offset, NULL, NULL);

	if ((a_flags & GTK_CELL_RENDERER_SELECTED) == GTK_CELL_RENDERER_SELECTED) {
		if (GTK_WIDGET_HAS_FOCUS (a_widget))
			state = GTK_STATE_SELECTED;
		else
			state = GTK_STATE_ACTIVE;
	} else {
		if (GTK_WIDGET_STATE (a_widget) == GTK_STATE_INSENSITIVE)
			state = GTK_STATE_INSENSITIVE;
		else
			state = GTK_STATE_NORMAL;
	}

	if (PRIVATE (thiz)->background_set && state != GTK_STATE_SELECTED) {
		GdkColor color;
		GdkGC *gc;

		color.red = PRIVATE (thiz)->background.red;
		color.green = PRIVATE (thiz)->background.green;
		color.blue = PRIVATE (thiz)->background.blue;

		gc = gdk_gc_new (a_window);

		gdk_gc_set_rgb_fg_color (gc, &color);

		gdk_draw_rectangle (a_window,
		                    gc,
		                    TRUE,
		                    a_background_area->x,
		                    a_background_area->y,
		                    a_background_area->width,
		                    a_background_area->height);

		g_object_unref (gc);
	}

	gtk_paint_layout (a_widget->style,
	                  a_window,
	                  state,
	                  TRUE,
	                  a_cell_area,
	                  a_widget,
	                  "cellrenderertext",
	                  a_cell_area->x + x_offset + a_this->xpad,
	                  a_cell_area->y + y_offset + a_this->ypad,
	                  layout);

	g_object_unref (layout);
}


static void
select_elem_char_string (MlViewEntry *a_editable)
{

	gchar *str = NULL, *str_ptr = NULL ;
	gint start_index = 0, end_index = 0, len = 0, i=0 ;
	guint32 unichar = 0 ;

	g_return_if_fail (a_editable && MLVIEW_IS_ENTRY (a_editable)) ;

	str = gtk_editable_get_chars (GTK_EDITABLE (a_editable), 0, -1) ;
	str_ptr = str ;
	len = g_utf8_strlen (str, -1) ;
	unichar = g_utf8_get_char (str_ptr) ;
	while (str_ptr && mlview_utils_is_name_char (unichar) == FALSE) {
		str_ptr = g_utf8_next_char (str_ptr) ;
		unichar = g_utf8_get_char (str_ptr) ;
		i++ ;
	}
	start_index = i ;
	str_ptr = &str[len - 1] ;
#ifdef MLVIEW_VERBOSE

	g_print ("Content of the MlViewEntry: %s\n", str) ;
	g_print ("Length of the content of the MlViewEntry: %d\n", len) ;
#endif

	unichar  = g_utf8_get_char (str_ptr) ;
	i = len ;
	while (str && mlview_utils_is_name_char (unichar) == FALSE) {
		str_ptr = g_utf8_prev_char (str_ptr) ;
		unichar = g_utf8_get_char (str_ptr) ;
		i-- ;
	}
	end_index = i ;
#ifdef MLVIEW_VERBOSE

	g_print ("Gonna select the content of the MlViewEntry:\n") ;
	g_print ("start offset: %d\n", start_index) ;
	g_print ("end offset: %d\n", end_index) ;
#endif

	if (str) {
		g_free (str) ;
		str = NULL ;
	}
	gtk_editable_select_region (GTK_EDITABLE (a_editable),
	                            start_index, end_index) ;
}

/**
 *this function does so that:
 *when requested to select all the the content
 *of the cell, select only the part of the content without '<' '>', if
 *any.
 */
static void
custom_selection_bounds (GtkEditable    *a_editable,
                         gint            a_start_pos,
                         gint            a_end_pos)
{

	g_return_if_fail (a_editable && GTK_IS_EDITABLE (a_editable)) ;
	if (!MLVIEW_IS_ENTRY (a_editable)) {
		if (gv_editable_set_selection_bounds_func_backup)
			gv_editable_set_selection_bounds_func_backup
			(a_editable, a_start_pos, a_end_pos) ;
		return ;
	}
	if (a_start_pos == 0 && a_end_pos < 0) {
		/*
		 *we received a request to select all the
		 *the content of the entry. Just select
		 *the elem char strings.
		 *This is a hack to allow easy modifcation
		 *of the element node name when we do editing
		 *without the mouse.
		 */
		select_elem_char_string (MLVIEW_ENTRY (a_editable)) ;
	} else {
		if (gv_editable_set_selection_bounds_func_backup)
			gv_editable_set_selection_bounds_func_backup
			(a_editable, a_start_pos, a_end_pos) ;
	}
}

/**
 *@param the implementation of the 
 *start_editing method of the GtkCellRenderer parent class.
 *It builds and returns a GtkEntry used to edit the currently selected
 *cell in the tree view.
 *@param a_this the current instance of #MlViewCellRenderer
 *@param a_event the event that triggered the editing
 *@param a_widget
 *@param a_background_area the background area of the cell
 *@param a_cell_area the cell_area
 *@param a_flags
 */
static GtkCellEditable *
mlview_cell_renderer_text_start_editing (GtkCellRenderer *a_this,
        GdkEvent *a_event,
        GtkWidget *a_widget,
        const gchar *a_path,
        GdkRectangle *a_background_area,
        GdkRectangle *a_cell_area,
        GtkCellRendererState a_flags)
{

	GtkWidget *entry;
	MlViewCellRenderer *thiz = NULL ;
	GtkTreePath *path = NULL ;
	gboolean dont_select_entry_region = FALSE ;
	GtkEditableClass *editable_iface = NULL ;

	g_return_val_if_fail (a_this && MLVIEW_IS_CELL_RENDERER (a_this),
	                      NULL) ;
	thiz = MLVIEW_CELL_RENDERER (a_this) ;
	g_return_val_if_fail (thiz && PRIVATE (thiz), NULL) ;


	/* If the cell isn't editable we return NULL. */
	if (PRIVATE (thiz)->editable == FALSE)
		return NULL;

	entry = (GtkWidget *) g_object_new (MLVIEW_TYPE_ENTRY,
	                                    "has_frame", FALSE,
	                                    NULL);

	if (PRIVATE (thiz)->text)
		gtk_entry_set_text (GTK_ENTRY (entry), PRIVATE (thiz)->text);
	g_object_set_data_full (G_OBJECT (entry), MLVIEW_CELL_RENDERER_PATH,
	                        g_strdup (a_path), g_free);

	g_signal_emit (thiz, gv_signals[SELECT_EDITABLE_REGION], 0,
	               entry, &dont_select_entry_region) ;

	/*
	 *This is an hugly hack
	 *
	 */
	editable_iface = GTK_EDITABLE_GET_CLASS (entry) ;
	if (editable_iface) {
		if (!gv_editable_set_selection_bounds_func_backup) {
			gv_editable_set_selection_bounds_func_backup =
			    editable_iface->set_selection_bounds ;
		}
		editable_iface->set_selection_bounds = custom_selection_bounds ;
	}

	if (dont_select_entry_region == FALSE) {
		gtk_editable_select_region (GTK_EDITABLE (entry), 0, -1);
	}

	gtk_widget_show (entry);
	g_signal_connect (entry,
	                  "editing_done",
	                  G_CALLBACK (mlview_cell_renderer_editing_done_cb),
	                  thiz);
	g_signal_connect (entry,
	                  "insert-text",
	                  G_CALLBACK (mlview_cell_renderer_insert_text_cb),
	                  thiz) ;

	PRIVATE (thiz)->focus_out_id =
	    g_signal_connect (entry, "focus_out_event",
	                      G_CALLBACK (mlview_cell_renderer_focus_out_event_cb),
	                      thiz);

	path = gtk_tree_path_new_from_string (a_path) ;
	g_signal_emit (a_this, gv_signals[EDITING_HAS_STARTED],
	               0, path, entry) ;
	if (path) {
		gtk_tree_path_free (path) ;
		path = NULL ;
	}
	return GTK_CELL_EDITABLE (entry);
}

/****************************
 *Public methods
 ****************************/

/**
 *Classical MlViewCellRenderer type getter.
 */
GType
mlview_cell_renderer_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo type_info = {
		                                       sizeof (MlViewCellRendererClass),
		                                       NULL, NULL,
		                                       (GClassInitFunc)
		                                       mlview_cell_renderer_class_init,
		                                       NULL, NULL,
		                                       sizeof (MlViewCellRenderer),
		                                       0,
		                                       (GInstanceInitFunc)
		                                       mlview_cell_renderer_init
		                                   };
		type = g_type_register_static (GTK_TYPE_CELL_RENDERER,
		                               "MlViewCellRenderer",
		                               &type_info, (GTypeFlags) 0);
	}
	return type;
}


/**
 *Instanciates a new cell renderer text
 *@return the newly built instance of GtkCellRenderer
 */
GtkCellRenderer *
mlview_cell_renderer_new (void)
{
	GtkCellRenderer * renderer = NULL ;
	renderer = (GtkCellRenderer *) g_object_new (MLVIEW_TYPE_CELL_RENDERER, (const gchar *) NULL) ;
	return renderer ;
}

#endif /*ifdef MLVIEW_WITH_CUSTOM_CELL_RENDERER*/
