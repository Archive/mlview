/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 8-*- */

/*This file is part of GNU MlView
 *
 *GNU MlView is free software; 
 *you can redistribute it and/or modify it under the terms of 
 *the GNU General Public License as 
 *published by the Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope 
 *that it will be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */

#ifndef __MLVIEW_SCHEMA_H__
#define __MLVIEW_SCHEMA_H__

#include <libxml/tree.h>
#include <libxml/relaxng.h>
#include <libxml/xmlschemas.h>

#include "mlview-file-descriptor.h"
#include "mlview-app-context.h"

G_BEGIN_DECLS

enum MlViewSchemaType {
        SCHEMA_TYPE_UNDEF = 0,
        SCHEMA_TYPE_DTD,
        SCHEMA_TYPE_RNG,
        SCHEMA_TYPE_XSD
} ;


typedef struct _MlViewSchema MlViewSchema;
typedef struct _MlViewSchemaPrivate MlViewSchemaPrivate;

struct _MlViewSchema {
        MlViewSchemaPrivate *priv;
};

MlViewSchema *mlview_schema_load_from_file (const gchar *a_url, 
											enum MlViewSchemaType a_type);

void mlview_schema_ref (MlViewSchema *a_schema);

void mlview_schema_unref (MlViewSchema *a_schema);

void mlview_schema_destroy (MlViewSchema *a_schema,
		                    gboolean a_free_native_schema);

gchar *mlview_schema_get_url (MlViewSchema *a_schema);

MlViewSchema *mlview_schema_load_interactive (enum MlViewSchemaType a_type);

MlViewSchema *mlview_schema_new_from_dtd (xmlDtdPtr a_dtd,
                                          const gchar *a_url, 
										  gboolean a_free_dtd_on_destroy);

enum MlViewStatus mlview_schema_get_type (MlViewSchema *a_this,
                                          enum MlViewSchemaType *a_type) ;

enum MlViewStatus mlview_schema_get_native_schema (MlViewSchema *a_this,
                                                   gpointer *a_nativeSchema) ;

G_END_DECLS

#endif
