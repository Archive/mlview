/*
  This file is part of MlView
  Philippe Mechaï (2005)
 
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include "mlview-prefs-category-frame.h"

namespace mlview
{
struct PrefsCategoryFramePriv
{
	Glib::RefPtr<Gnome::Glade::Xml> m_refxml;
	Gtk::VBox *m_box;
};

PrefsCategoryFrame::PrefsCategoryFrame (const Glib::ustring& name_)
{
	m_priv = new PrefsCategoryFramePriv ();

	gchar *glade_file =
	    gnome_program_locate_file (NULL,
	                               GNOME_FILE_DOMAIN_APP_DATADIR,
	                               PACKAGE "/mlview-prefs-window.glade",
	                               TRUE,
	                               NULL) ;
	m_priv->m_refxml = Gnome::Glade::Xml::create (glade_file,
	                   name_);
	m_priv->m_refxml->get_widget (name_, m_priv->m_box);
}

PrefsCategoryFrame::~PrefsCategoryFrame ()
{
	if (m_priv != NULL)
		delete m_priv;
}

Gtk::Widget&
PrefsCategoryFrame::widget_ref ()
{
	return dynamic_cast<Gtk::Widget&>(*(m_priv->m_box));
}

Glib::RefPtr<Gnome::Glade::Xml>
PrefsCategoryFrame::get_gladexml_ref ()
{
	return m_priv->m_refxml;
}

} // namespace mlview
