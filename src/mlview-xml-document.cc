/* -*- Mode: C++; indent-tabs-mode:true; c-basic-offset: 4-*- */
/*
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute 
 *it and/or modify it under the terms of 
 *the GNU General Public License as published by the 
 *Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYING file for copyright information.
 */

#include <stdio.h>
#include <string.h>
#include <libxml/tree.h>
#include <libxml/valid.h>
#include <libxml/xpath.h>
#include <libxml/xmlerror.h>
#include <libxml/xpathInternals.h>
#include "mlview-xml-document.h"
#include "mlview-parsing-utils.h"
#include "mlview-file-selection.h"
#include "mlview-file-descriptor.h"
#include "mlview-marshal.h"
#include "mlview-doc-mutation-stack.h"
#include "mlview-exception.h"
#include "mlview-app-context.h"
#include "mlview-clipboard.h"
#include "mlview-prefs.h"
#include "mlview-prefs-category-general.h"

/**
 *@file
 *The definition of the #MlViewXMLDocument class.
 *
 *This is a wrapper of the "tree" module
 *of Daniel Veillard's libxml.
 *That way, all the real actions made on
 *the underlying xml data structure
 *are made through this class.
 *This class then emits signals to
 *notify its state to all the views that are
 *in relation with it instances. Of course, this
 *class should not know anything about the views that holds it ...
 *
 */

struct _MlViewXMLDocumentPrivate
{
	/*
	 *the file descriptor associated to
	 *this mlview xml document.
	 */
	MlViewFileDescriptor *file_desc;

	/*the underlying xml doc data structure */
	xmlDocPtr native_doc;


	/*used in the dispose private method*/
	gboolean dispose_has_run;

	/*the currently selected node*/
	xmlNode *cur_node ;

	/**
	 *A list of all the xml nodes
	 *arranged in a linear way.
	 *This is used by the search functions.
	 */
	GList *nodes_list ;

	GHashTable *nodes_hash ;

	/*
	 *a number that is incremented at each modification
	 *on the tree.
	 */
	gulong modif_sequence ;

	/*somewhere to store a given modif_sequence*/
	gulong last_modif_sequence ;

	MlViewDocMutationStack *undo_stack ;
	MlViewDocMutationStack *redo_stack ;

	xmlXPathContext *xpath_context ;
	GHashTable *default_ns_prefixes ;
	gchar *mime_type ;

	/* associated schemas */
	MlViewSchemaList *schemas;

	/*
	 * whether the document has been reloaded since the last
	 * editing view swapping
	 */
} ;

enum {
    DOCUMENT_CHANGED = 0,
    NODE_CUT,
    CHILD_NODE_ADDED,
    PREV_SIBLING_NODE_INSERTED,
    NEXT_SIBLING_NODE_INSERTED,
    CONTENT_CHANGED,
    NAME_CHANGED,
    REPLACE_NODE,
    NODE_COMMENTED,
    NODE_UNCOMMENTED,
    NODE_CHANGED,
    NODE_ATTRIBUTE_ADDED,
    NODE_ATTRIBUTE_NAME_CHANGED,
    NODE_ATTRIBUTE_VALUE_CHANGED,
    NODE_ATTRIBUTE_REMOVED,
    NODE_NAMESPACE_ADDED,
    NODE_NAMESPACE_REMOVED,
    NODE_NAMESPACE_CHANGED,
    FILE_PATH_CHANGED,
    SEARCHED_NODE_FOUND,
    NODE_SELECTED,
    NODE_UNSELECTED,
    DTD_NODE_SYSTEM_ID_CHANGED,
    DTD_NODE_PUBLIC_ID_CHANGED,
    DTD_NODE_CREATED,
    ENTITY_NODE_CONTENT_CHANGED,
    ENTITY_NODE_PUBLIC_ID_CHANGED,
    ENTITY_NODE_SYSTEM_ID_CHANGED,
    EXT_SUBSET_CHANGED,
    DOCUMENT_CLOSED,
    DOCUMENT_RELOADED,
    GOING_TO_SAVE,
    DOCUMENT_UNDO_STATE_CHANGED,
    NUMBER_OF_SIGNALS
};

#define PRIVATE(mlview_xml_doc) ((mlview_xml_doc)->priv)
#define MLVIEW_XML_DOCUMENT_DEFAULT_VALIDATION_ON FALSE ;


/*
   NOT USED ATM
   static void mlview_xml_document_do_interactive_validation_if_needed (MlViewXMLDocument * a_doc);
*/

static GObjectClass *gv_parent_class = NULL;
static guint gv_signals[NUMBER_OF_SIGNALS] = { 0 };

static void mlview_xml_document_class_init (MlViewXMLDocumentClass * a_klass);

static void mlview_xml_document_init (MlViewXMLDocument * a_xml_doc);

static void free_tree_list_cache (MlViewXMLDocument *a_this) ;

static xmlNode * mlview_xml_document_add_child_node_real (MlViewXMLDocument *a_this,
        const gchar * a_parent_xml_node_path,
        xmlNode * a_xml_node,
        gboolean a_subtree_required,
        gboolean a_emit_signal) ;

static xmlNode * mlview_xml_document_cut_node_real (MlViewXMLDocument * a_this,
        const gchar * a_xml_node_path,
        gboolean a_emit_signal) ;

static xmlNode * mlview_xml_document_insert_prev_sibling_node_real (MlViewXMLDocument *a_this,
        const gchar* a_sibling_node_path,
        xmlNode *a_xml_node,
        gboolean a_subtree_required,
        gboolean a_emit_signal) ;

static xmlNode * mlview_xml_document_insert_next_sibling_node_real (MlViewXMLDocument *a_this,
        const gchar * a_sibling_node_path,
        xmlNode * a_xml_node,
        gboolean a_subtree_required,
        gboolean a_emit_signal) ;

static xmlNode * mlview_xml_document_set_node_name_real (MlViewXMLDocument *a_this,
        const gchar *a_node_path,
        gchar *a_name,
        gboolean a_emit_signal) ;

static xmlNode * mlview_xml_document_set_node_content_real (MlViewXMLDocument *a_this,
        const gchar *a_node_path,
        gchar *a_content,
        gboolean a_emit_signal) ;

static xmlAttr * mlview_xml_document_set_attribute_real (MlViewXMLDocument *a_this,
        const gchar *node_path,
        const gchar *a_name,
        const gchar *a_value,
        gboolean a_emit_signal) ;

static enum MlViewStatus mlview_xml_document_remove_attribute_real (MlViewXMLDocument *a_this,
        const gchar *a_node_path,
        const xmlChar *a_name,
        gboolean a_emit_signal) ;

static enum MlViewStatus mlview_xml_document_remove_redundant_ns_def_from_node (MlViewXMLDocument *a_this,
        xmlNode *a_node,
        xmlNode *a_ref_node) ;

static enum MlViewStatus mlview_xml_document_do_mutation_set_node_content (MlViewDocMutation *a_this,
        gpointer a_user_data) ;

static enum MlViewStatus mlview_xml_document_comment_node_real (MlViewXMLDocument *a_this,
        xmlNode *a_node,
        xmlNode **commented_node,
        gboolean a_emit_signal) ;


static enum MlViewStatus mlview_xml_document_uncomment_node_real (MlViewXMLDocument *a_this,
        xmlNode *a_node,
        xmlNode **a_uncommented_node,
        gboolean a_emit_signal) ;

static void mlview_xml_document_node_commented_cb (MlViewXMLDocument *a_this,
        xmlNode *a_node,
        xmlNode *a_new_node,
        gpointer a_user_data) ;

static void mlview_xml_document_node_uncommented_cb (MlViewXMLDocument *a_this,
        xmlNode *a_node,
        xmlNode *a_new_node,
        gpointer a_user_data) ;

static enum MlViewStatus  mlview_xml_document_replace_node_real (MlViewXMLDocument *a_this,
        xmlNode *a_node,
        xmlNode *a_replacement,
        gboolean a_emit_signal) ;

/* In development, TODO: finish coding this.
static enum MlViewStatus replace_string_in_node_name (MlViewXMLDocument *a_this,
		                                      xmlNode *a_node,
		                                      const gchar *a_look_str,
						      const gchar *a_repl_str) ;
 
static enum MlViewStatus replace_string_in_attr_value (MlViewXMLDocument *a_this,
		                                       xmlNode *a_node,
		                                       const gchar *a_look_str,
					               const gchar *a_repl_str) ;
 
static enum MlViewStatus replace_string_in_attr_name (MlViewXMLDocument *a_this,
		                                      xmlNode *a_node,
					              const gchar *a_loo_str,
						      const gchar *a_repl_str) ;
 
static enum MlViewStatus replace_string_content (MlViewXMLDocument *a_this,
		                                 xmlNode *a_node,
						 const gchar *a_look_str,
						 const gchar *a_repl_str) ;
*/


static enum MlViewStatus
mlview_xml_document_lookup_default_ns (MlViewXMLDocument *a_this,
                                       xmlNode *a_node,
                                       xmlNs **a_default_ns)
{
	xmlNs *result = NULL,  **ns_list = NULL, **list_it;

	THROW_IF_FAIL (a_this
	               && MLVIEW_IS_XML_DOCUMENT (a_this)
	               && PRIVATE (a_this)
	               && a_node
	               && a_default_ns) ;

	ns_list = xmlGetNsList (PRIVATE (a_this)->native_doc,
	                        a_node) ;

	for (list_it = ns_list ; list_it && *list_it; list_it++) {
		if (!(*list_it)->prefix && (*list_it)->href) {
			result = *list_it ;
			break ;
		}
	}

	*a_default_ns = result ;

	return MLVIEW_OK ;
}

static xmlNs*
mlview_xml_document_lookup_ns_prefix (gchar *a_prefix,
                                      xmlNs **a_ns_tab)
{
	xmlNs **cur = NULL ;

	if (!a_prefix || !a_ns_tab)
		return NULL ;

	for (cur = a_ns_tab ; cur && *cur ; cur ++) {
		if ((*cur)->prefix && !strcmp ((const char*)(*cur)->prefix,
		                               a_prefix))
			return *cur ;
	}
	return NULL ;
}

static gchar *
mlview_xml_document_construct_unique_ns_prefix (MlViewXMLDocument *a_this,
        xmlNode *a_node)
{
	const gchar *base = "dummyprefix" ;
	gchar *result = NULL, *tmp_str = NULL ;
	xmlNs **ns_list = NULL,  *ns = NULL;

	THROW_IF_FAIL (a_this && MLVIEW_XML_DOCUMENT (a_this)
	               && PRIVATE (a_this)
	               && PRIVATE (a_this)->native_doc
	               && a_node) ;

	result = g_strdup (base) ;

	ns_list = xmlGetNsList (PRIVATE (a_this)->native_doc,
	                        a_node) ;
	if (!ns_list)
		return result ;
	do {
		ns = mlview_xml_document_lookup_ns_prefix (result, ns_list) ;
		if (!ns)
			break ;

		tmp_str = g_strconcat (result, "_a", NULL) ;
		g_free (result) ;
		result = tmp_str ;
	} while (1) ;

	return result ;
}

static gchar *
mlview_xml_document_get_default_ns_invented_prefix (MlViewXMLDocument *a_this,
        const gchar *a_default_ns_uri,
        xmlNode *a_node)
{
	gchar *prefix = NULL, *str = NULL ;

	THROW_IF_FAIL (a_this && MLVIEW_IS_XML_DOCUMENT (a_this)
	               && PRIVATE (a_this)
	               && a_default_ns_uri
	               && a_node) ;

	if (!PRIVATE (a_this)->default_ns_prefixes) {
		PRIVATE (a_this)->default_ns_prefixes =
		    g_hash_table_new (g_str_hash, g_str_equal) ;

	}
	prefix = (gchar*)g_hash_table_lookup
	         (PRIVATE (a_this)->default_ns_prefixes, a_default_ns_uri) ;

	if (!prefix) {
		prefix = mlview_xml_document_construct_unique_ns_prefix
		         (a_this, a_node) ;
		if (!prefix)
			return NULL ;

		g_hash_table_insert (PRIVATE (a_this)->default_ns_prefixes,
		                     (gpointer)a_default_ns_uri,
		                     prefix) ;
	}
	str = g_strdup (prefix) ;
	return str ;
}

/**
  NOT USED ATM
  
 *Validates the document if the settings allows
 *it.
 *@param a_doc the "this" pointer.
 *
static void
mlview_xml_document_do_interactive_validation_if_needed (MlViewXMLDocument * a_doc) 
{
        struct MlViewAppSettings *settings = NULL ;
 
        THROW_IF_FAIL (a_doc != NULL);
        THROW_IF_FAIL (PRIVATE (a_doc) != NULL);
        THROW_IF_FAIL (PRIVATE (a_doc)->native_doc != NULL);
 
        if (PRIVATE (a_doc)->app_context) {
                settings = mlview_app_context_get_settings 
                        (PRIVATE (a_doc)->app_context) ;
                THROW_IF_FAIL (settings) ;
        }
 
        if (settings->general.validation_is_on == TRUE) {
                mlview_xml_document_validate (a_doc);
        }
}
*/

/* In development
static enum MlViewStatus 
replace_string_in_node_name (MlViewXMLDocument *a_this,
		             xmlNode *a_node,
		             const gchar *a_look_str,
		             const gchar *a_repl_str)
{
	gchar *node_name = NULL ;
 
	g_return_val_if_fail (a_this && MLVIEW_IS_XML_DOCUMENT (a_this)
			      && a_node && a_look_str && a_repl_str,
			      MLVIEW_BAD_PARAM_ERROR) ;
 
	if (!node->name) {
		return MLVIEW_NULL_POINTER_ERROR ;
	}
	node_name = g_strdup (a_node->name) ;
 
	if (node_name) {
		g_free (node_name) ;
		node_name = NULL ;
	}
	return MLVIEW_OK ;
}
*/



static enum MlViewStatus
mlview_xml_document_do_mutation_add_child_node (MlViewDocMutation *a_mutation,
        gpointer a_user_data)
{
	xmlNode *added_node = NULL, *xml_node=NULL ;
	gboolean subtree_required = FALSE, emit_signal = FALSE ;
	gchar *added_node_path = NULL,
	                         *parent_xml_node_path=NULL ;
	MlViewXMLDocument *mlview_xml_doc = NULL ;

	THROW_IF_FAIL (a_mutation && MLVIEW_IS_DOC_MUTATION (a_mutation)) ;

	mlview_xml_doc = mlview_doc_mutation_get_doc (a_mutation) ;
	THROW_IF_FAIL (mlview_xml_doc) ;

	parent_xml_node_path = (gchar*)g_object_get_data
	                       (G_OBJECT (a_mutation), "add-child-node::parent-xml-node-path") ;

	xml_node = (xmlNode*)g_object_get_data (G_OBJECT (a_mutation),
	                                        "add-child-node::xml-node") ;

	subtree_required = GPOINTER_TO_INT (g_object_get_data (G_OBJECT (a_mutation),
	                                    "add-child-node::subtree-required")) ;
	emit_signal = GPOINTER_TO_INT (g_object_get_data (G_OBJECT (a_mutation),
	                               "add-child-node::emit-signal")) ;

	added_node = mlview_xml_document_add_child_node_real (mlview_xml_doc,
	             parent_xml_node_path,
	             xml_node,
	             subtree_required,
	             emit_signal) ;
	/*
	 *get the xpath expr of the added_node so that we can
	 *undo this mutation later by cuting that added node.
	 *store that xpath expr in the the mutation object so
	 *that the undo method can get it.
	 */
	mlview_xml_document_get_node_path (mlview_xml_doc,
	                                   added_node,
	                                   &added_node_path) ;
	if (!added_node_path) {
		mlview_utils_trace_debug ("Could not get node path") ;
		return MLVIEW_ERROR ;
	}
	g_object_set_data (G_OBJECT (a_mutation),
	                   "add-child-node::added-node-path",
	                   added_node_path) ;
	added_node_path = NULL ;
	return MLVIEW_OK ;
}

static enum MlViewStatus
mlview_xml_document_undo_mutation_add_child_node (MlViewDocMutation *a_mutation,
        gpointer a_user_data)
{
	xmlChar *added_node_path = NULL ;
	MlViewXMLDocument *mlview_xml_doc = NULL ;
	enum MlViewStatus status = MLVIEW_OK ;

	THROW_IF_FAIL (a_mutation
	               && MLVIEW_IS_DOC_MUTATION (a_mutation)) ;

	added_node_path = (xmlChar*)g_object_get_data
	                  (G_OBJECT (a_mutation), "add-child-node::added-node-path") ;

	if (!added_node_path) {
		mlview_utils_trace_debug
		("mlview_xml_document_do_mutation_add_child_node() has "
		 "left the mutation object into an inconsistent state") ;
		return MLVIEW_ERROR ;
	}
	mlview_xml_doc = mlview_doc_mutation_get_doc (a_mutation) ;
	if (!mlview_xml_doc) {
		status = MLVIEW_ERROR ;
		goto cleanup ;
	}
	/*
	 *here, we are in an undo process so we use 
	 *the undo-free cut-node method so that
	 *we don't pollute the undo/redo stacks.
	 */
	mlview_xml_document_cut_node_real (mlview_xml_doc,
	                                   (const gchar*)added_node_path, TRUE) ;

cleanup:
	if (added_node_path) {
		g_free (added_node_path) ;
		added_node_path = NULL ;
	}
	return status ;
}

static enum MlViewStatus
mlview_xml_document_do_mutation_cut_node (MlViewDocMutation *a_this,
        gpointer a_user_data)
{
	gchar *serialized_cut_node = NULL,
	                             *node_to_cut_prev_sibling_path = NULL,
	                                                              *node_to_cut_next_sibling_path = NULL,
	                                                                                               *node_to_cut_parent_path = NULL,
	                                                                                                                          *node_to_cut_path = NULL ;
	MlViewXMLDocument *mlview_xml_doc = NULL ;
	xmlDoc *doc = NULL ;
	xmlNode *node_to_cut_prev_sibling = NULL,
	                                    *node_to_cut_next_sibling = NULL,
	                                                                *node_to_cut_parent = NULL,
	                                                                                      *cut_node = NULL,
	                                                                                                  *node_to_cut = NULL;
	gboolean emit_signal = FALSE ;
	enum MlViewStatus status = MLVIEW_OK ;

	THROW_IF_FAIL (a_this && MLVIEW_IS_DOC_MUTATION (a_this)) ;

	mlview_xml_doc = mlview_doc_mutation_get_doc (a_this) ;
	if (!mlview_xml_doc) {
		mlview_utils_trace_debug ("Could not get xml doc from doc mutation") ;
		return MLVIEW_ERROR ;
	}
	doc = mlview_xml_document_get_native_document (mlview_xml_doc) ;
	if (!doc) {
		mlview_utils_trace_debug ("Could not get xml doc from mlview xml doc") ;
		return MLVIEW_ERROR ;
	}

	node_to_cut_path = (gchar*) g_object_get_data (G_OBJECT (a_this),
	                   "cut-node::node-to-cut-path") ;

	emit_signal = GPOINTER_TO_INT (g_object_get_data (G_OBJECT (a_this),
	                               "cut-node::emit-signal")) ;

	node_to_cut = mlview_xml_document_get_node_from_xpath (mlview_xml_doc,
	              node_to_cut_path) ;
	if (!node_to_cut) {
		mlview_utils_trace_debug ("XPATH expr could not resolve to node") ;
		return MLVIEW_ERROR ;
	}
	node_to_cut_prev_sibling = node_to_cut->prev ;
	node_to_cut_next_sibling = node_to_cut->next ;
	node_to_cut_parent = node_to_cut->parent ;

	cut_node = mlview_xml_document_cut_node_real (mlview_xml_doc,
	           node_to_cut_path,
	           emit_signal) ;
	if (!cut_node)
		return MLVIEW_ERROR ;

	mlview_parsing_utils_serialize_node_to_buf (cut_node,
	        &serialized_cut_node) ;
	if (!serialized_cut_node) {
		mlview_utils_trace_debug ("could not serialize node") ;
		return MLVIEW_ERROR ;
	}
	if (node_to_cut_prev_sibling)
		mlview_xml_document_get_node_path (mlview_xml_doc,
		                                   node_to_cut_prev_sibling,
		                                   &node_to_cut_prev_sibling_path) ;

	if (node_to_cut_next_sibling)
		mlview_xml_document_get_node_path (mlview_xml_doc,
		                                   node_to_cut_next_sibling,
		                                   &node_to_cut_next_sibling_path) ;

	if (node_to_cut_parent)
		mlview_xml_document_get_node_path (mlview_xml_doc,
		                                   node_to_cut_parent,
		                                   &node_to_cut_parent_path) ;

	if (node_to_cut_prev_sibling_path) {
		g_object_set_data (G_OBJECT (a_this),
		                   "cut-node::node-to-cut-prev-sibling-path",
		                   node_to_cut_prev_sibling_path) ;
		node_to_cut_prev_sibling_path = NULL ;
	}

	if (node_to_cut_next_sibling_path) {
		g_object_set_data (G_OBJECT (a_this),
		                   "cut-node::node-to-cut-next-sibling-path",
		                   node_to_cut_next_sibling_path) ;
		node_to_cut_next_sibling_path = NULL ;
	}
	if (node_to_cut_parent_path) {
		g_object_set_data (G_OBJECT (a_this),
		                   "cut-node::node-to-cut-parent-path",
		                   node_to_cut_parent_path) ;
		node_to_cut_parent_path = NULL ;
	}
	if (serialized_cut_node) {
		g_object_set_data (G_OBJECT (a_this),
		                   "cut-node::serialized-cut-node",
		                   serialized_cut_node) ;
		serialized_cut_node = NULL ;
	}

	return status ;
}

static enum MlViewStatus
mlview_xml_document_undo_mutation_cut_node (MlViewDocMutation *a_this,
        gpointer a_user_data)
{
	enum MlViewStatus status = MLVIEW_OK ;
	gchar *serialized_cut_node = NULL ;
	gchar *node_to_cut_prev_sibling_path = NULL,
	                                       *node_to_cut_next_sibling_path = NULL,
	                                                                        *node_to_cut_parent_path = NULL ;
	xmlNode *cut_node = NULL ;
	gboolean subtree_required = FALSE ;
	gboolean emit_signal = TRUE ;
	xmlDoc *doc = NULL ;
	MlViewXMLDocument *mlview_xml_doc = NULL ;

	THROW_IF_FAIL (a_this && MLVIEW_IS_DOC_MUTATION (a_this)) ;

	mlview_xml_doc = mlview_doc_mutation_get_doc (a_this) ;
	THROW_IF_FAIL (mlview_xml_doc) ;
	doc = mlview_xml_document_get_native_document (mlview_xml_doc) ;

	serialized_cut_node = (gchar*) g_object_get_data
	                      (G_OBJECT (a_this), "cut-node::serialized-cut-node") ;

	if (!serialized_cut_node) {
		mlview_utils_trace_debug ("could not find serialized_cut_node") ;
		return MLVIEW_ERROR ;
	}

	node_to_cut_prev_sibling_path = (gchar*)g_object_get_data
	                                (G_OBJECT (a_this), "cut-node::node-to-cut-prev-sibling-path") ;

	node_to_cut_parent_path = (gchar*) g_object_get_data
	                          (G_OBJECT (a_this), "cut-node::node-to-cut-parent-path") ;

	node_to_cut_next_sibling_path = (gchar*)g_object_get_data
	                                (G_OBJECT (a_this), "cut-node::node-to-cut-next-sibling-path") ;
	subtree_required = (gboolean)GPOINTER_TO_INT
	                   (g_object_get_data (G_OBJECT (a_this),
	                                       "cut-node::subtree-required")) ;
	emit_signal = GPOINTER_TO_INT (g_object_get_data (G_OBJECT (a_this),
	                               "cut-node::emit-signal")) ;


	/*deserialize the serialized cut node*/
	status = mlview_parsing_utils_parse_fragment
	         (doc, (const xmlChar*)serialized_cut_node, &cut_node) ;

	if (!cut_node) {
		mlview_utils_trace_debug ("could not deserialized cut node") ;
		return MLVIEW_ERROR ;
	}
	/*
	 *insert the cut node where it was before being cut
	 *NOTE: we are in a undo process, so we use
	 *the undo free methods to insert the previously
	 *cut node so that we don't pollute the undo/redo stack.
	 */
	if (node_to_cut_prev_sibling_path) {
		mlview_xml_document_insert_prev_sibling_node_real
		(mlview_xml_doc, node_to_cut_prev_sibling_path,
		 cut_node, subtree_required, emit_signal) ;
	} else if (node_to_cut_next_sibling_path) {
		mlview_xml_document_insert_next_sibling_node_real
		(mlview_xml_doc, node_to_cut_next_sibling_path,
		 cut_node, subtree_required, emit_signal) ;
	} else {
		mlview_xml_document_add_child_node_real (mlview_xml_doc,
		        node_to_cut_parent_path,
		        cut_node,
		        subtree_required,
		        emit_signal) ;
	}
	return status ;
}

static enum MlViewStatus
mlview_xml_document_do_mutation_insert_next_sibling_node (MlViewDocMutation *a_this,
        gpointer a_user_data)
{
	xmlNode *xml_node = NULL,
	                    *inserted_node = NULL;
	gboolean subtree_required = FALSE,
	                            emit_signal = TRUE ;
	MlViewXMLDocument *mlview_xml_doc = NULL ;
	gchar *inserted_node_path = NULL,
	                            *sibling_node_path = NULL ;

	THROW_IF_FAIL (a_this && MLVIEW_IS_DOC_MUTATION (a_this)) ;

	mlview_xml_doc = mlview_doc_mutation_get_doc (a_this) ;
	THROW_IF_FAIL (mlview_xml_doc) ;

	sibling_node_path = (gchar*)g_object_get_data
	                    (G_OBJECT (a_this), "insert-next-sibling-node::sibling-node-path") ;
	xml_node = (xmlNode*)g_object_get_data
	           (G_OBJECT (a_this), "insert-next-sibling-node::xml-node") ;

	subtree_required = (gboolean)GPOINTER_TO_INT
	                   (g_object_get_data
	                    (G_OBJECT (a_this),
	                     "insert-next-sibling-node::subtree-required")) ;

	emit_signal = GPOINTER_TO_INT (g_object_get_data (G_OBJECT (a_this),
	                               "insert-next-sibling-node::emit-signal")) ;

	inserted_node = mlview_xml_document_insert_next_sibling_node_real
	                (mlview_xml_doc, sibling_node_path,
	                 xml_node, subtree_required,
	                 emit_signal) ;

	if (!inserted_node)
		return MLVIEW_ERROR ;

	mlview_xml_document_get_node_path (mlview_xml_doc,
	                                   inserted_node,
	                                   &inserted_node_path) ;

	g_object_set_data (G_OBJECT (a_this),
	                   "insert-next-sibling-node::inserted-node-path",
	                   inserted_node_path) ;

	return MLVIEW_OK ;
}


static enum MlViewStatus
mlview_xml_document_undo_mutation_insert_next_sibling_node (MlViewDocMutation *a_mutation,
        gpointer a_user_data)
{
	xmlChar *inserted_node_path = NULL ;
	MlViewXMLDocument *mlview_xml_doc = NULL ;
	enum MlViewStatus status = MLVIEW_OK ;

	THROW_IF_FAIL (a_mutation
	               && MLVIEW_IS_DOC_MUTATION (a_mutation)) ;

	inserted_node_path = (xmlChar*)g_object_get_data
	                     (G_OBJECT (a_mutation),
	                      "insert-next-sibling-node::inserted-node-path") ;

	if (!inserted_node_path) {
		mlview_utils_trace_debug ("mlview_xml_document_do_mutation_insert_next_sibling_node() has "
		                          "left the mutation object into an inconsistent state") ;
		return MLVIEW_ERROR ;
	}
	mlview_xml_doc = mlview_doc_mutation_get_doc (a_mutation) ;
	if (!mlview_xml_doc) {
		status = MLVIEW_ERROR ;
		goto cleanup ;
	}
	/*
	 *here, we are in an undo process so we use 
	 *the undo-free cut-node method so that
	 *we don't pollute the undo/redo stacks.
	 */
	mlview_xml_document_cut_node_real
	(mlview_xml_doc, (const gchar*)inserted_node_path, TRUE) ;

cleanup:
	if (inserted_node_path) {
		g_free (inserted_node_path) ;
		inserted_node_path = NULL ;
	}
	return status ;
}

static enum MlViewStatus
mlview_xml_document_do_mutation_insert_prev_sibling_node (MlViewDocMutation *a_this,
        gpointer a_user_data)
{
	xmlNode *xml_node = NULL,
	                    *inserted_node = NULL;
	gboolean subtree_required = FALSE,
	                            emit_signal = TRUE ;
	MlViewXMLDocument *mlview_xml_doc = NULL ;
	gchar *inserted_node_path = NULL,
	                            *sibling_node_path = NULL ;

	THROW_IF_FAIL (a_this && MLVIEW_IS_DOC_MUTATION (a_this)) ;

	mlview_xml_doc = mlview_doc_mutation_get_doc (a_this) ;
	THROW_IF_FAIL (mlview_xml_doc) ;

	sibling_node_path = (gchar*)g_object_get_data
	                    (G_OBJECT (a_this),
	                     "insert-prev-sibling-node::sibling-node-path") ;

	xml_node = (xmlNode*)g_object_get_data
	           (G_OBJECT (a_this), "insert-prev-sibling-node::xml-node") ;

	subtree_required = (gboolean)GPOINTER_TO_INT
	                   (g_object_get_data (G_OBJECT (a_this),
	                                       "insert-prev-sibling-node::subtree-required")) ;

	emit_signal = (gboolean)GPOINTER_TO_INT
	              (g_object_get_data (G_OBJECT (a_this),
	                                  "insert-prev-sibling-node::emit-signal")) ;

	inserted_node = mlview_xml_document_insert_prev_sibling_node_real
	                (mlview_xml_doc, sibling_node_path,
	                 xml_node, subtree_required,
	                 emit_signal) ;

	if (!inserted_node)
		return MLVIEW_ERROR ;

	mlview_xml_document_get_node_path (mlview_xml_doc,
	                                   inserted_node,
	                                   &inserted_node_path) ;
	g_object_set_data (G_OBJECT (a_this),
	                   "insert-prev-sibling-node::inserted-node-path",
	                   inserted_node_path) ;
	return MLVIEW_OK ;
}


static enum MlViewStatus
mlview_xml_document_undo_mutation_insert_prev_sibling_node (MlViewDocMutation *a_mutation,
        gpointer a_user_data)
{
	xmlChar *inserted_node_path = NULL ;
	MlViewXMLDocument *mlview_xml_doc = NULL ;
	enum MlViewStatus status = MLVIEW_OK ;

	THROW_IF_FAIL (a_mutation
	               && MLVIEW_IS_DOC_MUTATION (a_mutation)) ;

	inserted_node_path = (xmlChar*)
	                     g_object_get_data
	                     (G_OBJECT (a_mutation),
	                      "insert-prev-sibling-node::inserted-node-path") ;

	if (!inserted_node_path) {
		mlview_utils_trace_debug ("mlview_xml_document_do_mutation_insert_prev_sibling_node() has "
		                          "left the mutation object into an inconsistent state") ;
		return MLVIEW_ERROR ;
	}
	mlview_xml_doc = mlview_doc_mutation_get_doc (a_mutation) ;
	if (!mlview_xml_doc) {
		status = MLVIEW_ERROR ;
		goto cleanup ;
	}
	/*
	 *here, we are in an undo process so we use 
	 *the undo-free cut-node method so that
	 *we don't pollute the undo/redo stacks.
	 */
	mlview_xml_document_cut_node_real (mlview_xml_doc,
	                                   (const gchar*)inserted_node_path,
	                                   TRUE) ;
	inserted_node_path = NULL ;

cleanup:
	if (inserted_node_path) {
		g_free (inserted_node_path) ;
		inserted_node_path = NULL ;
	}
	return status ;
}

static enum MlViewStatus
mlview_xml_document_do_mutation_set_node_name (MlViewDocMutation *a_this,
        gpointer a_user_data)
{
	gchar *name = NULL, *previous_name = NULL ;
	gchar *node_path = NULL ;
	gboolean emit_signal = TRUE ;
	MlViewXMLDocument *mlview_xml_doc = NULL ;
	xmlNode *node = NULL ;

	THROW_IF_FAIL (a_this && MLVIEW_IS_DOC_MUTATION (a_this)) ;

	mlview_xml_doc = mlview_doc_mutation_get_doc (a_this) ;
	THROW_IF_FAIL (mlview_xml_doc) ;

	node_path = (gchar*)g_object_get_data (G_OBJECT (a_this),
	                                       "set-node-name::node-path") ;
	name = (gchar*)g_object_get_data (G_OBJECT (a_this),
	                                  "set-node-name::name") ;

	emit_signal = (gboolean)GPOINTER_TO_INT
	              (g_object_get_data (G_OBJECT (a_this),
	                                  "set-node-name::emit-signal")) ;

	if (!node_path) {
		mlview_utils_trace_debug ("Could not get node path") ;
		return MLVIEW_ERROR ;
	}
	if (!name) {
		mlview_utils_trace_debug ("Could not get name") ;
		return MLVIEW_ERROR ;
	}
	node = mlview_xml_document_get_node_from_xpath (mlview_xml_doc,
	        node_path) ;
	if (!node) {
		mlview_utils_trace_debug ("Could not get node from node_path") ;
		mlview_utils_trace_debug (node_path) ;
		return MLVIEW_ERROR ;
	}
	previous_name = (gchar*)g_strdup ((const gchar*)node->name) ;
	g_object_set_data (G_OBJECT (a_this),
	                   "set-node-name::previous-name",
	                   previous_name) ;

	if (!mlview_xml_document_set_node_name_real (mlview_xml_doc,
	        node_path, name,
	        emit_signal))
		return MLVIEW_ERROR ;

	/*
	 *node name has changed, so its path has changed
	 *save it !
	 */
	mlview_xml_document_get_node_path (mlview_xml_doc,
	                                   node,
	                                   &node_path) ;
	g_object_set_data (G_OBJECT (a_this),
	                   "set-node-name::node-path",
	                   node_path) ;
	return MLVIEW_OK  ;
}

static enum MlViewStatus
mlview_xml_document_undo_mutation_set_node_name (MlViewDocMutation *a_this,
        gpointer a_user_data)
{
	gchar *name=NULL, *previous_name = NULL ;
	gboolean emit_signal = TRUE ;
	MlViewXMLDocument *mlview_xml_doc = NULL ;
	gchar *node_path = NULL ;
	xmlNode *node = NULL ;

	THROW_IF_FAIL (a_this && MLVIEW_IS_DOC_MUTATION (a_this)) ;

	mlview_xml_doc = mlview_doc_mutation_get_doc (a_this) ;
	THROW_IF_FAIL (mlview_xml_doc) ;

	node_path = (gchar*)g_object_get_data
	            (G_OBJECT (a_this), "set-node-name::node-path") ;

	previous_name = (gchar*) g_object_get_data
	                (G_OBJECT (a_this), "set-node-name::previous-name") ;

	emit_signal = (gboolean) GPOINTER_TO_INT
	              (g_object_get_data (G_OBJECT (a_this),
	                                  "set-node-name::emit-signal")) ;

	if (!previous_name) {
		mlview_utils_trace_debug ("Could not get previous name") ;
		return MLVIEW_ERROR ;
	}
	node = mlview_xml_document_get_node_from_xpath (mlview_xml_doc,
	        node_path) ;
	if (!node) {
		mlview_utils_trace_debug ("XPATH expr could not resolve to node") ;
		return MLVIEW_ERROR ;
	}
	if (node->name)
		name = (gchar*)g_strdup ((const gchar*)node->name) ;
	else
		name = (gchar*)g_strdup ("unnamed") ;

	g_object_set_data (G_OBJECT (a_this),
	                   "set-node-name::name",
	                   name) ;

	if (!mlview_xml_document_set_node_name_real (mlview_xml_doc,
	        node_path,
	        previous_name,
	        emit_signal))
		return MLVIEW_ERROR ;
	/*
	 *node name has changed, so its path has changed too.
	 *Save it !
	 */
	mlview_xml_document_get_node_path (mlview_xml_doc,
	                                   node,
	                                   &node_path) ;
	g_object_set_data (G_OBJECT (a_this),
	                   "set-node-name::node-path",
	                   node_path) ;

	return MLVIEW_OK  ;
}

static enum MlViewStatus
mlview_xml_document_do_mutation_set_node_content (MlViewDocMutation *a_this,
        gpointer a_user_data)
{
	xmlNode *result = NULL, *node = NULL ;
	gchar * content = NULL, *previous_content = NULL,
	                  *node_path = NULL;
	gboolean emit_signal = TRUE ;
	MlViewXMLDocument *mlview_xml_doc = NULL ;

	THROW_IF_FAIL (a_this && MLVIEW_IS_DOC_MUTATION (a_this)) ;

	mlview_xml_doc = mlview_doc_mutation_get_doc (a_this) ;
	THROW_IF_FAIL (mlview_xml_doc) ;

	node_path = (gchar*) g_object_get_data (G_OBJECT (a_this),
	                                        "set-node-content::node-path") ;

	content = (gchar*) g_object_get_data (G_OBJECT (a_this),
	                                      "set-node-content::content") ;

	emit_signal = (gboolean)GPOINTER_TO_INT
	              (g_object_get_data (G_OBJECT (a_this),
	                                  "set-node-content::emit-signal")) ;

	node = mlview_xml_document_get_node_from_xpath
	       (mlview_xml_doc, node_path) ;
	previous_content = (gchar*) xmlNodeGetContent (node) ;
	result = mlview_xml_document_set_node_content_real (mlview_xml_doc,
	         node_path,
	         (gchar*)content,
	         emit_signal) ;
	g_object_set_data (G_OBJECT (a_this),
	                   "set-node-content::previous-content",
	                   previous_content) ;

	if (result)
		return MLVIEW_OK ;

	return MLVIEW_ERROR ;
}

static enum MlViewStatus
mlview_xml_document_undo_mutation_set_node_content (MlViewDocMutation *a_this,
        gpointer a_user_data)
{
	xmlNode *result = NULL, *node = NULL ;
	gchar * content = NULL, *previous_content = NULL,
	                  *node_path = NULL;
	gboolean emit_signal = TRUE ;
	MlViewXMLDocument *mlview_xml_doc = NULL ;

	THROW_IF_FAIL (a_this && MLVIEW_IS_DOC_MUTATION (a_this)) ;

	mlview_xml_doc = mlview_doc_mutation_get_doc (a_this) ;
	THROW_IF_FAIL (mlview_xml_doc) ;

	node_path = (gchar*)g_object_get_data (G_OBJECT (a_this),
	                                       "set-node-content::node-path") ;

	previous_content = (gchar*)g_object_get_data
	                   (G_OBJECT (a_this), "set-node-content::previous-content") ;

	node = mlview_xml_document_get_node_from_xpath (mlview_xml_doc,
	        node_path) ;
	if (!node) {
		mlview_utils_trace_debug ("XPATH expr could not resolve to node") ;
		return MLVIEW_ERROR ;
	}
	emit_signal = GPOINTER_TO_INT (g_object_get_data (G_OBJECT (a_this),
	                               "set-node-content::emit-signal")) ;

	content = (gchar*)xmlNodeGetContent (node) ;
	g_object_set_data (G_OBJECT (a_this),
	                   "set-node-content::content",
	                   content) ;

	result = mlview_xml_document_set_node_content_real
	         (mlview_xml_doc, node_path,
	          (gchar*)previous_content, emit_signal) ;

	if (result)
		return MLVIEW_OK ;

	return MLVIEW_ERROR ;
}

static enum MlViewStatus
mlview_xml_document_do_mutation_set_attribute (MlViewDocMutation *a_this,
        gpointer a_user_data)
{
	MlViewXMLDocument *mlview_xml_doc = NULL ;
	gchar *node_path = NULL, *attr_name=NULL,
	                                    *attr_value = NULL, *prev_attr_value=NULL ;
	xmlAttr *attr = NULL ;
	gboolean emit_signal = FALSE ;
	xmlNode *node = NULL ;

	THROW_IF_FAIL (a_this && MLVIEW_IS_DOC_MUTATION (a_this)) ;

	mlview_xml_doc = mlview_doc_mutation_get_doc (a_this) ;
	if (!mlview_xml_doc) {
		mlview_utils_trace_debug ("Could not get document") ;
		return MLVIEW_ERROR ;
	}
	node_path = (gchar*)g_object_get_data (G_OBJECT (a_this),
	                                       "set-attribute::node-path") ;
	attr_name = (gchar*)g_object_get_data (G_OBJECT (a_this),
	                                       "set-attribute::attribute-name") ;
	attr_value = (gchar*)g_object_get_data (G_OBJECT (a_this),
	                                        "set-attribute::attribute-value") ;

	emit_signal = (gboolean)GPOINTER_TO_INT
	              (g_object_get_data (G_OBJECT (a_this),
	                                  "set-attribute::emit-signal")) ;

	if (!node_path) {
		mlview_utils_trace_debug ("could not get node_path") ;
		return MLVIEW_ERROR ;
	}
	if (!attr_name) {
		mlview_utils_trace_debug ("could not get attr_name") ;
		return MLVIEW_ERROR ;
	}
	if (!attr_value) {
		mlview_utils_trace_debug ("could not get attr_value") ;
		return MLVIEW_ERROR ;
	}
	node = mlview_xml_document_get_node_from_xpath (mlview_xml_doc,
	        node_path) ;
	if (!node) {
		mlview_utils_trace_debug ("XPATH expr could not resolve to node") ;
		return MLVIEW_ERROR ;
	}
	prev_attr_value = (gchar*)xmlGetProp (node, (xmlChar*)attr_name) ;
	if (prev_attr_value) {
		g_object_set_data (G_OBJECT (a_this),
		                   "set-attribute::previous-attribute-value",
		                   prev_attr_value) ;

		g_object_set_data (G_OBJECT (a_this),
		                   "set-attribute::previous-attribute-name",
		                   attr_name) ;
	}
	attr = mlview_xml_document_set_attribute_real (mlview_xml_doc,
	        node_path,
	        attr_name,
	        attr_value,
	        emit_signal) ;

	if (attr)
		return MLVIEW_OK ;

	return MLVIEW_ERROR ;
}

static enum MlViewStatus
mlview_xml_document_undo_mutation_set_attribute (MlViewDocMutation *a_this,
        gpointer a_user_data)
{
	enum MlViewStatus status = MLVIEW_OK ;
	MlViewXMLDocument *mlview_xml_doc = NULL ;
	gchar *node_path = NULL, *prev_attr_name=NULL,
	                   *prev_attr_value=NULL, *attr_name=NULL ;
	xmlAttr *attr = NULL ;
	gboolean emit_signal = FALSE ;
	xmlNode *node = NULL ;

	THROW_IF_FAIL (a_this && MLVIEW_IS_DOC_MUTATION (a_this)) ;

	mlview_xml_doc = mlview_doc_mutation_get_doc (a_this) ;
	if (!mlview_xml_doc) {
		mlview_utils_trace_debug ("Could not get document") ;
		return MLVIEW_ERROR ;
	}
	node_path = (gchar*)g_object_get_data (G_OBJECT (a_this),
	                                       "set-attribute::node-path") ;
	prev_attr_name = (gchar*) g_object_get_data
	                 (G_OBJECT (a_this),
	                  "set-attribute::previous-attribute-name") ;

	prev_attr_value = (gchar*)g_object_get_data
	                  (G_OBJECT (a_this), "set-attribute::previous-attribute-value") ;

	emit_signal = (gboolean)GPOINTER_TO_INT
	              (g_object_get_data (G_OBJECT (a_this),
	                                  "set-attribute::emit-signal")) ;

	attr_name = (gchar*)g_object_get_data (G_OBJECT (a_this),
	                                       "set-attribute::attribute-name") ;
	if (!attr_name) {
		mlview_utils_trace_debug ("Could not get attribute name") ;
		return MLVIEW_ERROR ;
	}
	if (!node_path) {
		mlview_utils_trace_debug ("could not get node_path") ;
		return MLVIEW_ERROR ;
	}

	node = mlview_xml_document_get_node_from_xpath (mlview_xml_doc,
	        node_path) ;
	if (!node) {
		mlview_utils_trace_debug ("XPATH expr could not resolve to node") ;
		return MLVIEW_ERROR ;
	}
	if (!prev_attr_name) {
		status = mlview_xml_document_remove_attribute_real
		         (mlview_xml_doc, node_path,
		          (xmlChar*)attr_name, emit_signal) ;
		return status ;
	} else {
		attr = mlview_xml_document_set_attribute_real (mlview_xml_doc,
		        node_path,
		        prev_attr_name,
		        prev_attr_value,
		        emit_signal) ;
		if (attr)
			return MLVIEW_OK ;
	}
	return MLVIEW_OK ;
}

static enum MlViewStatus
mlview_xml_document_do_mutation_comment_node (MlViewDocMutation *a_this,
        gpointer a_user_data)
{
	MlViewXMLDocument *mlview_xml_doc = NULL ;
	gchar *node_path = NULL, *node_path2 = NULL ;
	gboolean emit_signal ;
	xmlNode *node = NULL , *commented_node = NULL ;
	enum MlViewStatus status = MLVIEW_OK ;


	THROW_IF_FAIL (a_this && MLVIEW_IS_DOC_MUTATION (a_this)) ;
	mlview_xml_doc = mlview_doc_mutation_get_doc (a_this) ;
	THROW_IF_FAIL (mlview_xml_doc) ;

	node_path = (gchar*)g_object_get_data (G_OBJECT (a_this),
	                                       "comment-node::node-path") ;
	if (!node_path) {
		mlview_utils_trace_debug ("Could not get node path") ;
		return MLVIEW_ERROR ;
	}
	emit_signal = GPOINTER_TO_INT (g_object_get_data
	                               (G_OBJECT (a_this),
	                                "comment-node::emit-signal")) ;


	node = mlview_xml_document_get_node_from_xpath (mlview_xml_doc,
	        node_path) ;
	if (!node) {
		mlview_utils_trace_debug ("Could not get node from xpath") ;
		return MLVIEW_ERROR ;
	}
	status = mlview_xml_document_comment_node_real (mlview_xml_doc,
	         node,
	         &commented_node,
	         emit_signal) ;
	if (status != MLVIEW_OK || ! commented_node) {
		return  MLVIEW_ERROR;
	}

	/*
	 * node has changed, so it's path has changed
	 */
	mlview_xml_document_get_node_path (mlview_xml_doc,
	                                   commented_node,
	                                   &node_path2) ;
	g_object_set_data (G_OBJECT (a_this),
	                   "comment-node::node-path",
	                   node_path2) ;
	return MLVIEW_OK ;
}

static enum MlViewStatus
mlview_xml_document_undo_mutation_comment_node (MlViewDocMutation *a_this,
        gpointer a_user_data)
{
	MlViewXMLDocument * mlview_xml_doc = NULL ;
	gchar *node_path = NULL, *node_path2 = NULL;
	gboolean emit_signal = FALSE ;
	xmlNode *node=NULL, *uncommented_node = NULL ;
	enum MlViewStatus status = MLVIEW_OK ;

	THROW_IF_FAIL (a_this && MLVIEW_IS_DOC_MUTATION (a_this)) ;
	mlview_xml_doc = mlview_doc_mutation_get_doc (a_this) ;
	THROW_IF_FAIL (mlview_xml_doc) ;
	node_path = (gchar*)g_object_get_data (G_OBJECT (a_this),
	                                       "comment-node::node-path") ;
	if (!node_path) {
		mlview_utils_trace_debug ("Could not get node path") ;
		return MLVIEW_ERROR ;
	}
	emit_signal = GPOINTER_TO_INT (g_object_get_data
	                               (G_OBJECT (a_this),
	                                "comment-node::emit-signal")) ;
	node = mlview_xml_document_get_node_from_xpath (mlview_xml_doc,
	        node_path) ;
	if (!node) {
		mlview_utils_trace_debug ("Could not get node from xpath") ;
		return MLVIEW_ERROR ;
	}
	THROW_IF_FAIL (node && node->type == XML_COMMENT_NODE) ;

	/*
	 * Now, really uncomment the node
	 */
	/*TODO: finish this !*/
	status = mlview_xml_document_uncomment_node_real (mlview_xml_doc,
	         node,
	         &uncommented_node,
	         emit_signal) ;
	if (status != MLVIEW_OK) {
		mlview_utils_trace_debug ("Could not uncomment node") ;
		return status ;
	}
	status = mlview_xml_document_get_node_path (mlview_xml_doc,
	         uncommented_node,
	         &node_path2) ;
	if (status != MLVIEW_OK || !node_path2) {
		mlview_utils_trace_debug ("Could not get node path") ;
		return MLVIEW_ERROR ;
	}
	g_object_set_data (G_OBJECT (a_this),
	                   "comment-node::node-path",
	                   node_path2) ;
	return MLVIEW_OK ;
}

static enum MlViewStatus
mlview_xml_document_do_mutation_uncomment_node (MlViewDocMutation *a_this,
        gpointer a_user_data)
{
	MlViewXMLDocument *mlview_xml_doc = NULL ;
	gchar *node_path = NULL, *node_path2 = NULL ;
	gboolean emit_signal = FALSE ;
	xmlNode *node = NULL, *uncommented_node = NULL ;
	enum MlViewStatus status = MLVIEW_OK ;

	THROW_IF_FAIL (a_this && MLVIEW_IS_DOC_MUTATION (a_this)) ;

	mlview_xml_doc = mlview_doc_mutation_get_doc (a_this) ;
	THROW_IF_FAIL (mlview_xml_doc) ;
	node_path = (gchar*)g_object_get_data
	            (G_OBJECT (a_this), "uncomment-node::node-path") ;
	if (!node_path) {
		mlview_utils_trace_debug ("Could not get node path") ;
		return MLVIEW_ERROR ;
	}
	node = mlview_xml_document_get_node_from_xpath (mlview_xml_doc,
	        node_path) ;
	if (!node) {
		mlview_utils_trace_debug ("Could not get node from xpath") ;
		return MLVIEW_ERROR ;
	}
	THROW_IF_FAIL (node && node->type == XML_COMMENT_NODE) ;

	emit_signal = GPOINTER_TO_INT (g_object_get_data (G_OBJECT (a_this),
	                               "uncomment-node::emit-signal")) ;

	status = mlview_xml_document_uncomment_node_real (mlview_xml_doc, node,
	         &uncommented_node,
	         emit_signal) ;
	if (status != MLVIEW_OK || !uncommented_node) {
		mlview_utils_trace_debug ("Uncomment node failed") ;
		return MLVIEW_ERROR ;
	}
	mlview_xml_document_get_node_path (mlview_xml_doc,
	                                   uncommented_node,
	                                   &node_path2) ;
	if (!node_path2) {
		mlview_utils_trace_debug ("COuld not get path") ;
		return MLVIEW_OK ;
	}
	g_object_set_data (G_OBJECT (a_this),
	                   "uncomment-node::node-path",
	                   node_path2) ;

	return MLVIEW_OK ;
}


static enum MlViewStatus
mlview_xml_document_undo_mutation_uncomment_node (MlViewDocMutation *a_this,
        gpointer a_user_data)
{
	MlViewXMLDocument *mlview_xml_doc = NULL ;
	gchar *node_path = NULL, *node_path2 = NULL ;
	gboolean emit_signal = FALSE ;
	xmlNode *node = NULL, *commented_node = NULL ;
	enum MlViewStatus status = MLVIEW_OK ;

	THROW_IF_FAIL (a_this && MLVIEW_IS_DOC_MUTATION (a_this)) ;

	mlview_xml_doc = mlview_doc_mutation_get_doc (a_this) ;
	THROW_IF_FAIL (mlview_xml_doc) ;
	node_path = (gchar*)g_object_get_data (G_OBJECT (a_this),
	                                       "uncomment-node::node-path") ;
	if (!node_path) {
		mlview_utils_trace_debug ("Could not get node path") ;
		return MLVIEW_ERROR ;
	}
	node = mlview_xml_document_get_node_from_xpath (mlview_xml_doc,
	        node_path) ;
	if (!node) {
		mlview_utils_trace_debug ("Could not get node from xpath") ;
		return MLVIEW_ERROR ;
	}
	THROW_IF_FAIL (node && node->type != XML_COMMENT_NODE) ;
	emit_signal = GPOINTER_TO_INT (g_object_get_data (G_OBJECT (a_this),
	                               "uncomment-node::emit-signal")) ;

	status = mlview_xml_document_comment_node_real (mlview_xml_doc, node,
	         &commented_node,
	         emit_signal) ;
	if (status != MLVIEW_OK || !commented_node) {
		mlview_utils_trace_debug ("Undo uncomment node failed") ;
		return MLVIEW_ERROR ;
	}
	mlview_xml_document_get_node_path (mlview_xml_doc,
	                                   commented_node,
	                                   &node_path2) ;
	if (!node_path2) {
		mlview_utils_trace_debug ("COuld not get path") ;
		return MLVIEW_OK ;
	}
	g_object_set_data (G_OBJECT (a_this),
	                   "uncomment-node::node-path",
	                   node_path2) ;

	return MLVIEW_OK ;
}

static enum MlViewStatus
mlview_xml_document_do_mutation_replace_node (MlViewDocMutation *a_this,
        gpointer a_user_data)
{
	enum MlViewStatus status = MLVIEW_OK ;
	MlViewXMLDocument *mlview_xml_doc = NULL ;
	gchar *node_path = NULL, *node_path2 = NULL,
	                                       *serialized_node = NULL,
	                                                          *serialized_replacing_node = NULL ;
	gboolean emit_signal = FALSE ;
	xmlNode *node = NULL, *replacing_node = NULL ;


	THROW_IF_FAIL (a_this && MLVIEW_IS_DOC_MUTATION (a_this)) ;

	mlview_xml_doc = mlview_doc_mutation_get_doc (a_this) ;
	THROW_IF_FAIL (mlview_xml_doc) ;

	node_path = (gchar*)g_object_get_data (G_OBJECT (a_this),
	                                       "replace-node::node-path") ;

	THROW_IF_FAIL (node_path) ;
	serialized_replacing_node = (gchar*) g_object_get_data
	                            (G_OBJECT (a_this),
	                             "replace-node::serialized-replacing-node") ;
	THROW_IF_FAIL (serialized_replacing_node) ;

	mlview_parsing_utils_parse_fragment (PRIVATE (mlview_xml_doc)->native_doc,
	                                     (xmlChar*) serialized_replacing_node,
	                                     &replacing_node) ;
	if (!replacing_node) {
		mlview_utils_trace_debug ("Could not deserialized replacing node") ;
		return MLVIEW_ERROR ;
	}

	emit_signal = GPOINTER_TO_INT (g_object_get_data (G_OBJECT (a_this),
	                               "replace-node::emit-signal")) ;
	THROW_IF_FAIL (replacing_node) ;

	node = mlview_xml_document_get_node_from_xpath (mlview_xml_doc,
	        node_path) ;
	if (!node) {
		mlview_utils_trace_debug ("could not create") ;
		return MLVIEW_OK ;
	}
	mlview_parsing_utils_serialize_node_to_buf
	(node, &serialized_node) ;
	if (!serialized_node) {
		mlview_utils_trace_debug ("Could not serialized the node") ;
		return MLVIEW_ERROR ;
	}

	status = mlview_xml_document_replace_node_real (mlview_xml_doc,
	         node, replacing_node,
	         emit_signal) ;

	mlview_xml_document_get_node_path (mlview_xml_doc,
	                                   replacing_node,
	                                   &node_path2) ;
	if (!node_path2) {
		mlview_utils_trace_debug ("Could not get node path") ;
		return MLVIEW_ERROR ;
	}

	g_object_set_data (G_OBJECT (a_this),
	                   "replace-node::replaced-node-path",
	                   node_path2) ;
	g_object_set_data (G_OBJECT (a_this),
	                   "replace-node::serialized-initial-node",
	                   serialized_node) ;
	return MLVIEW_OK ;

}

static enum MlViewStatus
mlview_xml_document_undo_mutation_replace_node (MlViewDocMutation *a_this,
        gpointer a_user_data)
{

	MlViewXMLDocument *mlview_xml_doc = NULL ;
	gchar *node_path = NULL, *node_path2 = NULL,
	                                       *serialized_initial_node = NULL, *serialized_node = NULL ;
	gboolean emit_signal = FALSE ;
	xmlNode *node = NULL, *initial_node = NULL ;
	enum MlViewStatus status = MLVIEW_OK ;

	g_return_val_if_fail (a_this && MLVIEW_IS_DOC_MUTATION (a_this),
	                      MLVIEW_BAD_PARAM_ERROR) ;

	mlview_xml_doc = mlview_doc_mutation_get_doc (a_this) ;

	node_path = (gchar*)g_object_get_data
	            (G_OBJECT (a_this), "replace-node::replaced-node-path") ;
	if (!node_path) {
		mlview_utils_trace_debug ("could not get the node path") ;
		return MLVIEW_ERROR ;
	}
	emit_signal = GPOINTER_TO_INT (g_object_get_data (G_OBJECT (a_this),
	                               "replace-node::emit-signal")) ;

	serialized_initial_node = (gchar*)g_object_get_data
	                          (G_OBJECT (a_this),
	                           "replace-node::serialized-node-path") ;

	node = mlview_xml_document_get_node_from_xpath
	       (mlview_xml_doc, node_path) ;
	if (!node) {
		mlview_utils_trace_debug ("could not get node from xpath") ;
		return MLVIEW_ERROR ;
	}
	mlview_parsing_utils_parse_fragment (PRIVATE (mlview_xml_doc)->native_doc,
	                                     (xmlChar*)serialized_initial_node,
	                                     &initial_node) ;
	if (!initial_node) {
		mlview_utils_trace_debug ("Could not deserialize the node") ;
		return MLVIEW_ERROR ;
	}

	if (!serialized_node) {
		mlview_utils_trace_debug ("Could not serialize node") ;
		return MLVIEW_ERROR ;
	}

	status = mlview_xml_document_replace_node_real (mlview_xml_doc,
	         node, initial_node,
	         emit_signal) ;

	mlview_xml_document_get_node_path (mlview_xml_doc,
	                                   initial_node,
	                                   &node_path2) ;
	if (!node_path2) {
		mlview_utils_trace_debug ("could not get node path") ;
		return MLVIEW_ERROR ;
	}

	g_object_set_data (G_OBJECT (a_this),
	                   "replace-node::node-path",
	                   node_path2) ;
	return MLVIEW_OK ;
}


static xmlNode *
mlview_xml_document_add_child_node_real (MlViewXMLDocument *a_this,
										 const gchar * a_parent_xml_node_path,
										 xmlNode * a_xml_node,
										 gboolean a_subtree_required,
										 gboolean a_emit_signal)
{
	xmlNode *result = NULL, *parent_node=NULL ;

	THROW_IF_FAIL (a_this != NULL);
	THROW_IF_FAIL (MLVIEW_IS_XML_DOCUMENT (a_this));
	THROW_IF_FAIL (a_parent_xml_node_path);
	THROW_IF_FAIL (a_xml_node != NULL);

    mlview::PrefsCategoryGeneral *prefs =
	    dynamic_cast<mlview::PrefsCategoryGeneral*> (
		mlview::Preferences::get_instance ()
		->get_category_by_id (mlview::PrefsCategoryGeneral::CATEGORY_ID));
    THROW_IF_FAIL (prefs);



	parent_node = mlview_xml_document_get_node_from_xpath 
			(a_this, a_parent_xml_node_path) ;
	if (!parent_node) {
		mlview_utils_trace_debug ("XPATH expr did not resolved to a node") ;
		return NULL ;
	}
	g_return_val_if_fail ((parent_node->type ==
	                       XML_ELEMENT_NODE)
	                      || (parent_node->type ==
	                          XML_DOCUMENT_NODE)
	                      || (parent_node->type ==
	                          XML_DTD_NODE
	                          && a_xml_node->type ==
	                          XML_ENTITY_DECL),
	                      NULL);

	result = xmlAddChild (parent_node, a_xml_node);
	THROW_IF_FAIL (result != NULL);

	mlview_xml_document_remove_redundant_ns_def_from_node (a_this,
	        result,
	        parent_node) ;
	xmlReconciliateNs (PRIVATE (a_this)->native_doc,
	                   result) ;

	if (!result->ns) {
		xmlNs *default_ns = NULL ;
		mlview_xml_document_lookup_default_ns (a_this, result, &default_ns) ;
		if (default_ns) {
			result->ns = default_ns ;
		}
	}

	if (a_subtree_required == TRUE
	        && prefs->use_validation () == TRUE
	        && result->type == XML_ELEMENT_NODE) {
		enum MLVIEW_PARSING_UTILS_STATUS status;
		status = mlview_parsing_utils_build_required_attributes_list (result);
		status = mlview_parsing_utils_build_required_children_tree (&result);
	}

	/*emit the proper signals */
	if (a_emit_signal == TRUE) {
		g_signal_emit
		(G_OBJECT (a_this),
		 gv_signals[CHILD_NODE_ADDED], 0,
		 parent_node, result);
		g_signal_emit
		(G_OBJECT (a_this),
		 gv_signals[DOCUMENT_CHANGED], 0);
	}
	return result;
}


static xmlNode *
mlview_xml_document_cut_node_real (MlViewXMLDocument * a_this,
                                   const gchar * a_xml_node_path,
                                   gboolean a_emit_signal)
{
	xmlNode *result = NULL,
	                  *xml_node = NULL,
	                              *xml_parent_node = NULL;

	THROW_IF_FAIL (a_this != NULL);
	g_return_val_if_fail (MLVIEW_IS_XML_DOCUMENT (a_this),
	                      NULL);
	THROW_IF_FAIL (PRIVATE (a_this) != NULL);
	g_return_val_if_fail (PRIVATE (a_this)->native_doc !=
	                      NULL, NULL);
	THROW_IF_FAIL (a_xml_node_path != NULL);

	xml_node = mlview_xml_document_get_node_from_xpath (a_this,
	           a_xml_node_path) ;
	THROW_IF_FAIL (xml_node) ;

	mlview_xml_document_copy_node_to_clipboard2
	(xml_node, PRIVATE (a_this)->native_doc);

	xml_parent_node = xml_node->parent ;
	xmlUnlinkNode (xml_node);
	result = xml_node;

	if (a_emit_signal == TRUE) {

		g_signal_emit (G_OBJECT (a_this),
		               gv_signals[NODE_CUT], 0,
		               xml_parent_node,
		               xml_node);

		g_signal_emit
		(G_OBJECT (a_this),
		 gv_signals[DOCUMENT_CHANGED], 0);
	}
	return result;
}

static enum MlViewStatus
mlview_xml_document_remove_redundant_ns_def_from_node (MlViewXMLDocument *a_this,
        xmlNode *a_node,
        xmlNode *a_ref_node)
{
	xmlNs *cur_ns_def = NULL,
	                    **tmp_ns_ptr = NULL,
	                                   *tmp_ns2 = NULL,
	                                              *tmp_ns3 = NULL,
	                                                         **ns_list = NULL;
	gboolean is_redundant = FALSE ;

	g_return_val_if_fail (a_this && MLVIEW_IS_XML_DOCUMENT (a_this)
	                      && PRIVATE (a_this)
	                      && a_node
	                      && a_ref_node,
	                      MLVIEW_BAD_PARAM_ERROR) ;


	for (cur_ns_def = a_node->nsDef ;
	        cur_ns_def ;
	        cur_ns_def = (cur_ns_def)?cur_ns_def->next:NULL) {

		is_redundant = FALSE ;
		ns_list = xmlGetNsList (PRIVATE (a_this)->native_doc,
		                        a_ref_node) ;
		for (tmp_ns_ptr = ns_list ;
		        tmp_ns_ptr && *tmp_ns_ptr;
		        tmp_ns_ptr++) {
			if (( (*tmp_ns_ptr)->href
			        && cur_ns_def->href
			        && !strcmp ((char*)(*tmp_ns_ptr)->href ,
			                    (char*)cur_ns_def->href))
			        ||
			        ( ((*tmp_ns_ptr)->href == cur_ns_def->href)
			          && (cur_ns_def == NULL))) {

				if (((*tmp_ns_ptr)->prefix
				        && cur_ns_def->prefix
				        && !strcmp ((char*)(*tmp_ns_ptr)->prefix ,
				                    (char*)cur_ns_def->prefix))
				        ||
				        ((*tmp_ns_ptr)->prefix == NULL
				         && cur_ns_def->prefix == NULL)) {
					is_redundant = TRUE ;
					break ;
				}
			}
		}
		if (is_redundant ==  TRUE) {
			/*unlink current ns def*/
			tmp_ns3 = tmp_ns2 ;
			if (tmp_ns2) {

				if (tmp_ns2->next)
					tmp_ns2->next = tmp_ns2->next->next ;
				else
					tmp_ns2->next = NULL ;
			} else {
				a_node->nsDef = NULL ;
			}
			cur_ns_def = tmp_ns3 ;
			continue ;
		}
		tmp_ns2= cur_ns_def ;
	}

	return MLVIEW_OK ;
}

static xmlNode *
mlview_xml_document_insert_prev_sibling_node_real (MlViewXMLDocument *a_this,
        const gchar * a_sibling_node_path,
        xmlNode *a_xml_node,
        gboolean a_subtree_required,
        gboolean a_emit_signal)
{
	xmlNode *result = NULL, *sibling_node = NULL;

	THROW_IF_FAIL (a_this != NULL);
	THROW_IF_FAIL (MLVIEW_IS_XML_DOCUMENT (a_this));
	THROW_IF_FAIL (PRIVATE (a_this));
	THROW_IF_FAIL (a_sibling_node_path != NULL);
	THROW_IF_FAIL (a_xml_node != NULL);

    mlview::PrefsCategoryGeneral *prefs =
	    dynamic_cast<mlview::PrefsCategoryGeneral*> (
		mlview::Preferences::get_instance ()
		->get_category_by_id (mlview::PrefsCategoryGeneral::CATEGORY_ID));
    THROW_IF_FAIL (prefs);

	sibling_node = mlview_xml_document_get_node_from_xpath (a_this,
	               a_sibling_node_path) ;
	if (!sibling_node) {
		mlview_utils_trace_debug ("Could not resolve XPATH expr to node") ;
		return NULL ;
	}
	result = xmlAddPrevSibling (sibling_node, a_xml_node) ;
	THROW_IF_FAIL (result) ;

	mlview_xml_document_remove_redundant_ns_def_from_node (a_this,
	        a_xml_node,
	        sibling_node) ;
	xmlReconciliateNs (PRIVATE (a_this)->native_doc,
	                   result) ;

	if (!result->ns) {
		xmlNs *default_ns = NULL ;
		mlview_xml_document_lookup_default_ns (a_this, result, &default_ns) ;
		if (default_ns) {
			result->ns = default_ns ;
		}
	}

	if (a_subtree_required == TRUE
	    && prefs->use_validation ()
	    && result->type == XML_ELEMENT_NODE) {
		enum MLVIEW_PARSING_UTILS_STATUS status;
		status = mlview_parsing_utils_build_required_attributes_list (result);

		status = mlview_parsing_utils_build_required_children_tree (&result);
	}

	if (a_emit_signal == TRUE) {
		g_signal_emit
		(G_OBJECT (a_this),
		 gv_signals[PREV_SIBLING_NODE_INSERTED], 0,
		 sibling_node, result);
		g_signal_emit
		(G_OBJECT (a_this),
		 gv_signals[DOCUMENT_CHANGED], 0);
	}
	return result;
}



static xmlNode *
mlview_xml_document_insert_next_sibling_node_real (MlViewXMLDocument *a_this,
        const gchar * a_sibling_node_path,
        xmlNode * a_xml_node,
        gboolean a_subtree_required,
        gboolean a_emit_signal)
{
	xmlNode *result = NULL;
	xmlNode *sibling_node = NULL ;

	THROW_IF_FAIL (a_this != NULL);
	THROW_IF_FAIL (MLVIEW_IS_XML_DOCUMENT (a_this));
	THROW_IF_FAIL (a_sibling_node_path != NULL);
	THROW_IF_FAIL (a_xml_node != NULL);

    mlview::PrefsCategoryGeneral *prefs =
	    dynamic_cast<mlview::PrefsCategoryGeneral*> (
		mlview::Preferences::get_instance ()
		->get_category_by_id (mlview::PrefsCategoryGeneral::CATEGORY_ID));
    THROW_IF_FAIL (prefs);

	sibling_node = mlview_xml_document_get_node_from_xpath (a_this,
	               a_sibling_node_path) ;
	THROW_IF_FAIL (sibling_node) ;
	result = xmlAddNextSibling (sibling_node, a_xml_node) ;
	THROW_IF_FAIL (result) ;

	mlview_xml_document_remove_redundant_ns_def_from_node (a_this,
	        a_xml_node,
	        sibling_node) ;
	xmlReconciliateNs (PRIVATE (a_this)->native_doc,
	                   result) ;

	if (!result->ns) {
		xmlNs *default_ns = NULL ;
		mlview_xml_document_lookup_default_ns (a_this, result, &default_ns) ;
		if (default_ns) {
			result->ns = default_ns ;
		}
	}

	if (a_subtree_required == TRUE
	    && prefs->use_validation () == TRUE
	    && result
	    && result->type == XML_ELEMENT_NODE) {
		enum MLVIEW_PARSING_UTILS_STATUS status;
		status = mlview_parsing_utils_build_required_attributes_list (result);
		status = mlview_parsing_utils_build_required_children_tree (&result);
	}


	if (a_emit_signal == TRUE) {
		g_signal_emit
		(G_OBJECT (a_this),
		 gv_signals[NEXT_SIBLING_NODE_INSERTED], 0,
		 sibling_node, result);
		g_signal_emit
		(G_OBJECT (a_this),
		 gv_signals[DOCUMENT_CHANGED], 0);
	}
	return result;
}


static xmlNode *
mlview_xml_document_set_node_name_real (MlViewXMLDocument *a_this,
                                        const gchar *a_node_path,
                                        gchar *a_name,
                                        gboolean a_emit_signal)
{
	xmlNode *node= NULL ;
	THROW_IF_FAIL (a_this != NULL);
	g_return_val_if_fail (MLVIEW_IS_XML_DOCUMENT
	                      (a_this), NULL);
	THROW_IF_FAIL (a_node_path != NULL);

	node = mlview_xml_document_get_node_from_xpath (a_this,
	        a_node_path) ;
	xmlNodeSetName (node, (xmlChar*)a_name);

	if (a_emit_signal) {
		g_signal_emit (G_OBJECT (a_this),
		               gv_signals[NAME_CHANGED], 0, node);
		g_signal_emit (G_OBJECT (a_this),
		               gv_signals[NODE_CHANGED], 0, node);
		g_signal_emit
		(G_OBJECT (a_this),
		 gv_signals[DOCUMENT_CHANGED], 0);
	}
	return node;
}

static xmlNode *
mlview_xml_document_set_node_content_real (MlViewXMLDocument *a_this,
        const gchar *a_node_path,
        gchar *a_content,
        gboolean a_emit_signal)
{
	xmlNode *result = NULL, *node = NULL;

	THROW_IF_FAIL (a_this != NULL);
	g_return_val_if_fail (MLVIEW_IS_XML_DOCUMENT
	                      (a_this), NULL);
	THROW_IF_FAIL (a_node_path != NULL);

	node = mlview_xml_document_get_node_from_xpath (a_this, a_node_path) ;
	if (!node) {
		mlview_utils_trace_debug ("XPATH expr could not resolve to node") ;
		return NULL ;
	}

	xmlNodeSetContent (node, (xmlChar*)a_content);

	if (a_emit_signal) {
		g_signal_emit
		(G_OBJECT (a_this),
		 gv_signals[CONTENT_CHANGED], 0, node);

		g_signal_emit (G_OBJECT (a_this),
		               gv_signals[NODE_CHANGED], 0, node);
		g_signal_emit
		(G_OBJECT (a_this),
		 gv_signals[DOCUMENT_CHANGED], 0);
	}
	result = node;
	return result;
}

static xmlAttr *
mlview_xml_document_set_attribute_real (MlViewXMLDocument *a_this,
                                        const gchar *a_node_path,
                                        const gchar *a_name,
                                        const gchar *a_value,
                                        gboolean a_emit_signal)
{
	xmlNode *node = NULL ;
	gboolean attr_added = TRUE ;
	xmlChar *value = NULL ;
	xmlAttr *attr = NULL ;

	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_XML_DOCUMENT (a_this)
	                      && PRIVATE (a_this)
	                      && a_node_path
	                      && a_name && a_value,
	                      NULL) ;

	node = mlview_xml_document_get_node_from_xpath (a_this,
	        a_node_path) ;
	if (!node) {
		mlview_utils_trace_debug ("XPATH expression could not resolve to node") ;
		mlview_utils_trace_debug (a_node_path) ;
		return NULL ;
	}

	if ((value = xmlGetProp (node, (xmlChar*)a_name))) {
		xmlFree (value) ;
		value = NULL ;
		attr_added = FALSE ;
	}
	attr = xmlSetProp (node, (xmlChar*)a_name, (xmlChar*)a_value) ;
	if (a_emit_signal == TRUE) {
		if (attr_added == TRUE) {
			g_signal_emit (G_OBJECT (a_this),
			               gv_signals[NODE_ATTRIBUTE_ADDED], 0,
			               attr) ;
		}
		g_signal_emit (G_OBJECT (a_this),
		               gv_signals[NODE_ATTRIBUTE_VALUE_CHANGED], 0,
		               attr) ;
		g_signal_emit (G_OBJECT (a_this),
		               gv_signals[NODE_CHANGED], 0, node);
		g_signal_emit
		(G_OBJECT (a_this),
		 gv_signals[DOCUMENT_CHANGED], 0);
	}
	return attr ;
}

static enum MlViewStatus
mlview_xml_document_remove_attribute_real (MlViewXMLDocument *a_this,
        const gchar *a_node_path,
        const xmlChar *a_name,
        gboolean a_emit_signal)
{
	xmlNode *node = NULL ;
	xmlAttr *attr = NULL ;
	xmlChar *attr_name = NULL ;

	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_XML_DOCUMENT (a_this)
	                      && PRIVATE (a_this)
	                      && a_node_path && a_name,
	                      MLVIEW_BAD_PARAM_ERROR) ;

	node = mlview_xml_document_get_node_from_xpath (a_this,
	        a_node_path) ;
	if (!node) {
		mlview_utils_trace_debug ("XPATH expr could not resolve to node") ;
		return MLVIEW_ERROR ;
	}
	attr = xmlHasProp (node, a_name) ;

	if (attr) {
		/*
		 *a_name may be allocated on attr
		 *before removing (freeing) attr
		 *let's keep a copy of a_name.
		 */
		attr_name = xmlStrdup (a_name) ;
		if (!attr_name) {
			mlview_utils_trace_debug ("xmlStrdup failed. "
			                          "system may be out of memory.") ;
			return MLVIEW_OUT_OF_MEMORY_ERROR ;
		}
		xmlRemoveProp (attr) ;
		attr = NULL ;
		if (a_emit_signal == TRUE) {
			g_signal_emit
			(G_OBJECT (a_this),
			 gv_signals[NODE_ATTRIBUTE_REMOVED], 0,
			 node,
			 attr_name/*use our copy of a_name here.*/) ;
			xmlFree (attr_name) ;
			attr_name = NULL ;
			g_signal_emit
			(G_OBJECT (a_this),
			 gv_signals[NODE_CHANGED], 0,
			 node) ;
			g_signal_emit
			(G_OBJECT (a_this),
			 gv_signals[DOCUMENT_CHANGED], 0);
		}
	}
	return MLVIEW_OK ;
}


static enum MlViewStatus
mlview_xml_document_comment_node_real (MlViewXMLDocument *a_this,
                                       xmlNode *a_node,
                                       xmlNode **a_commented_node,
                                       gboolean a_emit_signal)
{
	enum MlViewStatus status = MLVIEW_OK ;
	xmlNode *commented_node = NULL ;

	g_return_val_if_fail (a_this && MLVIEW_IS_XML_DOCUMENT (a_this),
	                      MLVIEW_BAD_PARAM_ERROR) ;

	status = mlview_parsing_utils_do_comment_node (a_node,
	         &commented_node) ;
	if (status != MLVIEW_OK || !commented_node) {
		return status ;
	}

	status = mlview_xml_document_replace_node_real (a_this,
	         a_node,
	         commented_node,
	         TRUE) ;
	if (status != MLVIEW_OK) {
		if (commented_node) {
			xmlFreeNode (commented_node) ;
			commented_node = NULL ;
		}
	} else {
		if (a_emit_signal == TRUE) {
			g_signal_emit (G_OBJECT (a_this),
			               gv_signals[NODE_COMMENTED], 0,
			               a_node, commented_node) ;
			g_signal_emit (G_OBJECT (a_this),
			               gv_signals[DOCUMENT_CHANGED], 0) ;
		}
		if (a_commented_node) {
			*a_commented_node = commented_node ;
		}
	}

	return status ;
}

static enum MlViewStatus
mlview_xml_document_uncomment_node_real (MlViewXMLDocument *a_this,
        xmlNode *a_node,
        xmlNode **a_uncommented_node,
        gboolean a_emit_signal)
{
	enum MlViewStatus status = MLVIEW_OK ;
	xmlNode *uncommented_node = NULL ;

	g_return_val_if_fail (a_this && MLVIEW_IS_XML_DOCUMENT (a_this)
	                      && PRIVATE (a_this),
	                      MLVIEW_BAD_PARAM_ERROR) ;

	status = mlview_parsing_utils_uncomment_node (PRIVATE (a_this)->native_doc,
	         a_node, &uncommented_node) ;

	if (status != MLVIEW_OK || !uncommented_node) {
		return status ;
	}

	status = mlview_xml_document_replace_node_real (a_this,
	         a_node,
	         uncommented_node,
	         TRUE) ;
	if (status != MLVIEW_OK) {
		if (uncommented_node) {
			xmlFreeNode (uncommented_node) ;
			uncommented_node = NULL ;
		}
	} else {
		if (a_emit_signal == TRUE) {
			g_signal_emit (G_OBJECT (a_this),
			               gv_signals[NODE_UNCOMMENTED], 0,
			               a_node, uncommented_node) ;
			g_signal_emit (G_OBJECT (a_this),
			               gv_signals[DOCUMENT_CHANGED], 0) ;
		}
		if (a_uncommented_node) {
			*a_uncommented_node = uncommented_node ;

		}
	}

	return status ;
}

static enum MlViewStatus
mlview_xml_document_replace_node_real (MlViewXMLDocument *a_this,
                                       xmlNode *a_node,
                                       xmlNode *a_replacement,
                                       gboolean a_emit_signal)
{
	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_XML_DOCUMENT (a_this)
	                      && PRIVATE (a_this)
	                      && a_node && a_replacement,
	                      MLVIEW_BAD_PARAM_ERROR) ;

	g_return_val_if_fail (PRIVATE (a_this)->native_doc == a_node->doc,
	                      MLVIEW_BAD_PARAM_ERROR) ;

	if (a_emit_signal == TRUE) {
		g_signal_emit (G_OBJECT (a_this),
		               gv_signals[REPLACE_NODE], 0,
		               a_node, a_replacement) ;
	}

	if (!xmlReplaceNode (a_node, a_replacement)) {
		return MLVIEW_ERROR ;
	}

	if (a_emit_signal == TRUE) {
		g_signal_emit (G_OBJECT (a_this),
		               gv_signals[NODE_CHANGED], 0,
		               a_replacement) ;
		g_signal_emit (G_OBJECT (a_this),
		               gv_signals[DOCUMENT_CHANGED], 0) ;
	}
	return MLVIEW_OK ;
}

static enum MlViewStatus
mlview_xml_document_record_mutation_for_undo (MlViewXMLDocument *a_this,
        MlViewDocMutation *a_mutation,
        gboolean a_clear_redo_stack)
{
	MlViewDocMutation *mutation = NULL ;

	g_return_val_if_fail (a_this && MLVIEW_IS_XML_DOCUMENT (a_this)
	                      && PRIVATE (a_this),
	                      MLVIEW_BAD_PARAM_ERROR) ;

	if (!PRIVATE (a_this)->undo_stack) {
		PRIVATE (a_this)->undo_stack = mlview_doc_mutation_stack_new () ;
	}
	mlview_doc_mutation_stack_push (PRIVATE (a_this)->undo_stack,
	                                a_mutation) ;
	if (PRIVATE (a_this)->redo_stack) {
		if (a_clear_redo_stack == TRUE)
			mlview_doc_mutation_stack_clear (PRIVATE (a_this)->redo_stack) ;
		else
			mlview_doc_mutation_stack_pop (PRIVATE (a_this)->redo_stack,
			                               &mutation) ;
	}

	/**
	 *Notify the world about the change in the undo state.
	 */
	mlview_xml_document_notify_undo_state_changed (a_this) ;

	return MLVIEW_OK ;
}

static enum MlViewStatus
mlview_xml_document_record_mutation_for_redo (MlViewXMLDocument *a_this,
        MlViewDocMutation *a_mutation)
{
	MlViewDocMutation *mutation = NULL ;
	g_return_val_if_fail (a_this && MLVIEW_IS_XML_DOCUMENT (a_this)
	                      && PRIVATE (a_this),
	                      MLVIEW_BAD_PARAM_ERROR) ;

	g_return_val_if_fail (PRIVATE (a_this)->undo_stack,
	                      MLVIEW_BAD_PARAM_ERROR) ;

	mlview_doc_mutation_stack_peek (PRIVATE (a_this)->undo_stack,
	                                &mutation) ;
	g_return_val_if_fail (mutation == a_mutation,
	                      MLVIEW_BAD_PARAM_ERROR) ;
	mutation = NULL ;

	if (!PRIVATE (a_this)->redo_stack) {
		PRIVATE (a_this)->redo_stack = mlview_doc_mutation_stack_new () ;
	}
	/*
	 *push the mutation on the redo stack 
	 * before poping it from the undo stack
	 *otherwise, the ref count will reach zero
	 *and the mutation will be destroyed.
	 */
	mlview_doc_mutation_stack_push (PRIVATE (a_this)->redo_stack,
	                                a_mutation) ;

	mlview_doc_mutation_stack_pop (PRIVATE (a_this)->undo_stack,
	                               &mutation) ;

	/**
	 *Notify the world about the change in the undo state.
	 */
	mlview_xml_document_notify_undo_state_changed (a_this) ;
	return MLVIEW_OK ;
}

/***********************************************
 *private gobject framework methods						
 ***********************************************/

static void
mlview_xml_document_dispose (GObject * a_xml_doc)
{
	MlViewXMLDocument *xml_doc = NULL;
	LOG_TO_ERROR_STREAM ("here") ;

	THROW_IF_FAIL (a_xml_doc != NULL);
	THROW_IF_FAIL (MLVIEW_IS_XML_DOCUMENT (a_xml_doc));

	xml_doc = MLVIEW_XML_DOCUMENT (a_xml_doc);

	THROW_IF_FAIL (PRIVATE (xml_doc));
	THROW_IF_FAIL (PRIVATE (xml_doc)->dispose_has_run ==
	               FALSE);

	g_signal_emit
	(G_OBJECT (a_xml_doc),
	 gv_signals [DOCUMENT_CLOSED], 0);

	if (PRIVATE (xml_doc)->file_desc != NULL) {
		mlview_file_descriptor_destroy (PRIVATE
		                                (xml_doc)->
		                                file_desc);
		PRIVATE (xml_doc)->file_desc = NULL;
	}

	/*
	 *Free the nodes contained in the clipboard
	 *before freeing the document that contain
	 *them or else, the nodes will have a dangling
	 *pointer to a freed document. That could lead
	 *to invalid read or invalid write and thus to
	 *stability problems.
	 */
	if (PRIVATE (xml_doc)->native_doc != NULL) {
		mlview_xml_document_set_ext_subset_with_url (xml_doc, NULL);

		xmlFreeDoc (PRIVATE (xml_doc)->native_doc);

		PRIVATE (xml_doc)->native_doc = NULL;
	}
	if (PRIVATE (xml_doc)->nodes_list) {
		free_tree_list_cache (xml_doc) ;
	}

	if (PRIVATE (xml_doc)->schemas) {
		g_object_unref (G_OBJECT (PRIVATE (xml_doc)->schemas));
		PRIVATE (xml_doc)->schemas = NULL;
	}

	if (PRIVATE (xml_doc)->mime_type) {
		g_free (PRIVATE (xml_doc)->mime_type) ;
		PRIVATE (xml_doc)->mime_type = NULL ;
	}

	PRIVATE (xml_doc)->dispose_has_run = TRUE;
}

static void
mlview_xml_document_finalize (GObject * a_object)
{
	MlViewXMLDocument *xml_document = NULL;

	THROW_IF_FAIL (a_object);
	xml_document = MLVIEW_XML_DOCUMENT (a_object);
	THROW_IF_FAIL (xml_document);
	THROW_IF_FAIL (PRIVATE (xml_document));
	THROW_IF_FAIL (PRIVATE (xml_document)->
	               dispose_has_run == TRUE);

	g_free (PRIVATE (xml_document));
	PRIVATE (xml_document) = NULL;
}

/**
 *Inits the MlViewXMLDocument class.
 *@param a_klass the class structure containing the vtable
 *and all the class wide data.
 */
static void
mlview_xml_document_class_init (MlViewXMLDocumentClass * a_klass)
{
	GObjectClass *object_class = NULL;

	THROW_IF_FAIL (a_klass != NULL);
	THROW_IF_FAIL (MLVIEW_IS_XML_DOCUMENT_CLASS
	               (a_klass));

	object_class = G_OBJECT_CLASS (a_klass);
	gv_parent_class = (GObjectClass*)g_type_class_peek_parent (a_klass);
	object_class->dispose = mlview_xml_document_dispose;
	object_class->finalize = mlview_xml_document_finalize;

	/*signal creation code */
	gv_signals[DOCUMENT_CHANGED] =
	    g_signal_new ("document-changed",
	                  G_TYPE_FROM_CLASS (object_class),
	                  G_SIGNAL_RUN_FIRST,
	                  G_STRUCT_OFFSET
	                  (MlViewXMLDocumentClass,
	                   document_changed), NULL, NULL,
	                  mlview_marshal_VOID__VOID,
	                  G_TYPE_NONE, 0, NULL);

	gv_signals[NODE_CUT] =
	    g_signal_new ("node-cut",
	                  G_TYPE_FROM_CLASS (object_class),
	                  G_SIGNAL_RUN_FIRST,
	                  G_STRUCT_OFFSET
	                  (MlViewXMLDocumentClass, node_cut),
	                  NULL, NULL,
	                  mlview_marshal_VOID__POINTER_POINTER,
	                  G_TYPE_NONE, 2, G_TYPE_POINTER,
	                  G_TYPE_POINTER);

	gv_signals[CHILD_NODE_ADDED] =
	    g_signal_new ("child-node-added",
	                  G_TYPE_FROM_CLASS (object_class),
	                  G_SIGNAL_RUN_FIRST,
	                  G_STRUCT_OFFSET
	                  (MlViewXMLDocumentClass,
	                   child_node_added), NULL, NULL,
	                  mlview_marshal_VOID__POINTER_POINTER,
	                  G_TYPE_NONE, 2, G_TYPE_POINTER,
	                  G_TYPE_POINTER);

	gv_signals
	[PREV_SIBLING_NODE_INSERTED] =
	    g_signal_new ("prev-sibling-node-inserted",
	                  G_TYPE_FROM_CLASS (object_class),
	                  G_SIGNAL_RUN_FIRST,
	                  G_STRUCT_OFFSET
	                  (MlViewXMLDocumentClass,
	                   prev_sibling_node_inserted), NULL,
	                  NULL,
	                  mlview_marshal_VOID__POINTER_POINTER,
	                  G_TYPE_NONE, 2, G_TYPE_POINTER,
	                  G_TYPE_POINTER);

	gv_signals
	[NEXT_SIBLING_NODE_INSERTED] =
	    g_signal_new ("next-sibling-node-inserted",
	                  G_TYPE_FROM_CLASS (object_class),
	                  G_SIGNAL_RUN_FIRST,
	                  G_STRUCT_OFFSET
	                  (MlViewXMLDocumentClass,
	                   next_sibling_node_inserted), NULL,
	                  NULL,
	                  mlview_marshal_VOID__POINTER_POINTER,
	                  G_TYPE_NONE, 2, G_TYPE_POINTER,
	                  G_TYPE_POINTER);

	gv_signals[CONTENT_CHANGED] =
	    g_signal_new ("content-changed",
	                  G_TYPE_FROM_CLASS (object_class),
	                  G_SIGNAL_RUN_FIRST,
	                  G_STRUCT_OFFSET
	                  (MlViewXMLDocumentClass,
	                   content_changed), NULL, NULL,
	                  mlview_marshal_VOID__POINTER,
	                  G_TYPE_NONE, 1, G_TYPE_POINTER);

	gv_signals[NAME_CHANGED] =
	    g_signal_new ("name-changed",
	                  G_TYPE_FROM_CLASS (object_class),
	                  G_SIGNAL_RUN_FIRST,
	                  G_STRUCT_OFFSET
	                  (MlViewXMLDocumentClass,
	                   name_changed), NULL, NULL,
	                  mlview_marshal_VOID__POINTER,
	                  G_TYPE_NONE, 1, G_TYPE_POINTER);

	gv_signals[REPLACE_NODE] =
	    g_signal_new ("replace-node",
	                  G_TYPE_FROM_CLASS (object_class),
	                  G_SIGNAL_RUN_FIRST,
	                  G_STRUCT_OFFSET
	                  (MlViewXMLDocumentClass, replace_node),
	                  NULL, NULL,
	                  mlview_marshal_VOID__POINTER_POINTER,
	                  G_TYPE_NONE, 2,
	                  G_TYPE_POINTER,
	                  G_TYPE_POINTER);

	gv_signals[NODE_COMMENTED] =
	    g_signal_new ("node-commented",
	                  G_TYPE_FROM_CLASS (object_class),
	                  G_SIGNAL_RUN_FIRST,
	                  G_STRUCT_OFFSET
	                  (MlViewXMLDocumentClass, node_commented),
	                  NULL, NULL,
	                  mlview_marshal_VOID__POINTER_POINTER,
	                  G_TYPE_NONE, 2,
	                  G_TYPE_POINTER,
	                  G_TYPE_POINTER);

	gv_signals[NODE_UNCOMMENTED] =
	    g_signal_new ("node-uncommented",
	                  G_TYPE_FROM_CLASS (object_class),
	                  G_SIGNAL_RUN_FIRST,
	                  G_STRUCT_OFFSET
	                  (MlViewXMLDocumentClass,
	                   node_uncommented), NULL, NULL,
	                  mlview_marshal_VOID__POINTER_POINTER,
	                  G_TYPE_NONE, 2,
	                  G_TYPE_POINTER,
	                  G_TYPE_POINTER);

	gv_signals[NODE_CHANGED] =
	    g_signal_new ("node-changed",
	                  G_TYPE_FROM_CLASS (object_class),
	                  G_SIGNAL_RUN_FIRST,
	                  G_STRUCT_OFFSET
	                  (MlViewXMLDocumentClass,
	                   node_changed), NULL, NULL,
	                  mlview_marshal_VOID__POINTER,
	                  G_TYPE_NONE, 1, G_TYPE_POINTER);

	gv_signals[FILE_PATH_CHANGED] =
	    g_signal_new ("file-path-changed",
	                  G_TYPE_FROM_CLASS (object_class),
	                  G_SIGNAL_RUN_FIRST,
	                  G_STRUCT_OFFSET
	                  (MlViewXMLDocumentClass,
	                   file_path_changed), NULL, NULL,
	                  mlview_marshal_VOID__VOID,
	                  G_TYPE_NONE, 0, NULL);

	gv_signals[SEARCHED_NODE_FOUND] =
	    g_signal_new ("searched-node-found",
	                  G_TYPE_FROM_CLASS (object_class),
	                  G_SIGNAL_RUN_FIRST,
	                  G_STRUCT_OFFSET
	                  (MlViewXMLDocumentClass,
	                   searched_node_found), NULL, NULL,
	                  mlview_marshal_VOID__POINTER,
	                  G_TYPE_NONE, 1, G_TYPE_POINTER);

	gv_signals[NODE_ATTRIBUTE_NAME_CHANGED] =
	    g_signal_new ("node-attribute-name-changed",
	                  G_TYPE_FROM_CLASS (object_class),
	                  G_SIGNAL_RUN_FIRST,
	                  G_STRUCT_OFFSET
	                  (MlViewXMLDocumentClass,
	                   node_attribute_name_changed), NULL, NULL,
	                  mlview_marshal_VOID__POINTER,
	                  G_TYPE_NONE, 1, G_TYPE_POINTER) ;

	gv_signals[NODE_ATTRIBUTE_ADDED] =
	    g_signal_new ("node-attribute-added",
	                  G_TYPE_FROM_CLASS (object_class),
	                  G_SIGNAL_RUN_FIRST,
	                  G_STRUCT_OFFSET
	                  (MlViewXMLDocumentClass,
	                   node_attribute_added), NULL, NULL,
	                  mlview_marshal_VOID__POINTER,
	                  G_TYPE_NONE, 1, G_TYPE_POINTER) ;

	gv_signals[NODE_ATTRIBUTE_VALUE_CHANGED] =
	    g_signal_new ("node-attribute-value-changed",
	                  G_TYPE_FROM_CLASS (object_class),
	                  G_SIGNAL_RUN_FIRST,
	                  G_STRUCT_OFFSET
	                  (MlViewXMLDocumentClass,
	                   node_attribute_value_changed),
	                  NULL, NULL,
	                  mlview_marshal_VOID__POINTER,
	                  G_TYPE_NONE, 1, G_TYPE_POINTER) ;

	gv_signals[NODE_ATTRIBUTE_REMOVED] =
	    g_signal_new ("node-attribute-removed",
	                  G_TYPE_FROM_CLASS (object_class),
	                  G_SIGNAL_RUN_FIRST,
	                  G_STRUCT_OFFSET
	                  (MlViewXMLDocumentClass,
	                   node_attribute_removed), NULL, NULL,
	                  mlview_marshal_VOID__POINTER_POINTER,
	                  G_TYPE_NONE, 2, G_TYPE_POINTER, G_TYPE_POINTER) ;

	gv_signals[NODE_NAMESPACE_ADDED] =
	    g_signal_new ("node-namespace-added",
	                  G_TYPE_FROM_CLASS (object_class),
	                  G_SIGNAL_RUN_FIRST,
	                  G_STRUCT_OFFSET
	                  (MlViewXMLDocumentClass,
	                   node_namespace_added), NULL, NULL,
	                  mlview_marshal_VOID__POINTER_POINTER,
	                  G_TYPE_NONE, 2, G_TYPE_POINTER, G_TYPE_POINTER) ;

	gv_signals[NODE_NAMESPACE_CHANGED] =
	    g_signal_new ("node-namespace-changed",
	                  G_TYPE_FROM_CLASS (object_class),
	                  G_SIGNAL_RUN_FIRST,
	                  G_STRUCT_OFFSET
	                  (MlViewXMLDocumentClass,
	                   node_namespace_changed), NULL, NULL,
	                  mlview_marshal_VOID__POINTER_POINTER,
	                  G_TYPE_NONE, 2, G_TYPE_POINTER, G_TYPE_POINTER) ;

	gv_signals[NODE_NAMESPACE_REMOVED] =
	    g_signal_new ("node-namespace-removed",
	                  G_TYPE_FROM_CLASS (object_class),
	                  G_SIGNAL_RUN_FIRST,
	                  G_STRUCT_OFFSET
	                  (MlViewXMLDocumentClass,
	                   node_namespace_removed), NULL, NULL,
	                  mlview_marshal_VOID__POINTER_POINTER,
	                  G_TYPE_NONE, 2, G_TYPE_POINTER, G_TYPE_POINTER) ;

	gv_signals[NODE_SELECTED] =
	    g_signal_new ("node-selected",
	                  G_TYPE_FROM_CLASS (object_class),
	                  G_SIGNAL_RUN_FIRST,
	                  G_STRUCT_OFFSET
	                  (MlViewXMLDocumentClass, node_selected),
	                  NULL, NULL,
	                  mlview_marshal_VOID__POINTER,
	                  G_TYPE_NONE, 1, G_TYPE_POINTER) ;

	gv_signals[NODE_UNSELECTED] =
	    g_signal_new ("node-unselected",
	                  G_TYPE_FROM_CLASS (object_class),
	                  G_SIGNAL_RUN_FIRST,
	                  G_STRUCT_OFFSET
	                  (MlViewXMLDocumentClass, node_unselected),
	                  NULL, NULL,
	                  mlview_marshal_VOID__POINTER,
	                  G_TYPE_NONE, 1, G_TYPE_POINTER) ;

	gv_signals[DTD_NODE_SYSTEM_ID_CHANGED] =
	    g_signal_new ("dtd-node-system-id-changed",
	                  G_TYPE_FROM_CLASS (object_class),
	                  G_SIGNAL_RUN_FIRST,
	                  G_STRUCT_OFFSET
	                  (MlViewXMLDocumentClass,
	                   dtd_node_system_id_changed),
	                  NULL, NULL,
	                  mlview_marshal_VOID__POINTER,
	                  G_TYPE_NONE, 1, G_TYPE_POINTER) ;

	gv_signals[DTD_NODE_PUBLIC_ID_CHANGED] =
	    g_signal_new ("dtd-node-public-id-changed",
	                  G_TYPE_FROM_CLASS (object_class),
	                  G_SIGNAL_RUN_FIRST,
	                  G_STRUCT_OFFSET
	                  (MlViewXMLDocumentClass,
	                   dtd_node_public_id_changed),
	                  NULL, NULL,
	                  mlview_marshal_VOID__POINTER,
	                  G_TYPE_NONE, 1, G_TYPE_POINTER) ;

	gv_signals[DTD_NODE_CREATED] =
	    g_signal_new ("dtd-node-created",
	                  G_TYPE_FROM_CLASS (object_class),
	                  G_SIGNAL_RUN_FIRST,
	                  G_STRUCT_OFFSET
	                  (MlViewXMLDocumentClass,
	                   dtd_node_created),
	                  NULL, NULL,
	                  mlview_marshal_VOID__POINTER,
	                  G_TYPE_NONE, 1, G_TYPE_POINTER) ;

	gv_signals[ENTITY_NODE_CONTENT_CHANGED] =
	    g_signal_new ("entity-node-content-changed",
	                  G_TYPE_FROM_CLASS (object_class),
	                  G_SIGNAL_RUN_FIRST,
	                  G_STRUCT_OFFSET
	                  (MlViewXMLDocumentClass,
	                   entity_node_content_changed),
	                  NULL, NULL,
	                  mlview_marshal_VOID__POINTER,
	                  G_TYPE_NONE, 1, G_TYPE_POINTER) ;

	gv_signals[ENTITY_NODE_SYSTEM_ID_CHANGED] =
	    g_signal_new ("entity-node-system-id-changed",
	                  G_TYPE_FROM_CLASS (object_class),
	                  G_SIGNAL_RUN_FIRST,
	                  G_STRUCT_OFFSET
	                  (MlViewXMLDocumentClass,
	                   entity_node_system_id_changed),
	                  NULL, NULL,
	                  mlview_marshal_VOID__POINTER,
	                  G_TYPE_NONE, 1, G_TYPE_POINTER) ;

	gv_signals[ENTITY_NODE_PUBLIC_ID_CHANGED] =
	    g_signal_new ("entity-node-public-id-changed",
	                  G_TYPE_FROM_CLASS (object_class),
	                  G_SIGNAL_RUN_FIRST,
	                  G_STRUCT_OFFSET
	                  (MlViewXMLDocumentClass,
	                   entity_node_public_id_changed),
	                  NULL, NULL,
	                  mlview_marshal_VOID__POINTER,
	                  G_TYPE_NONE, 1, G_TYPE_POINTER) ;

	gv_signals [EXT_SUBSET_CHANGED] =
	    g_signal_new ("ext-subset-changed",
	                  G_TYPE_FROM_CLASS (object_class),
	                  G_SIGNAL_RUN_FIRST,
	                  G_STRUCT_OFFSET
	                  (MlViewXMLDocumentClass,
	                   ext_subset_changed),
	                  NULL, NULL,
	                  mlview_marshal_VOID__VOID,
	                  G_TYPE_NONE, 0, NULL);

	gv_signals [DOCUMENT_CLOSED] =
	    g_signal_new ("document-closed",
	                  G_TYPE_FROM_CLASS (object_class),
	                  G_SIGNAL_RUN_FIRST,
	                  G_STRUCT_OFFSET
	                  (MlViewXMLDocumentClass,
	                   document_closed),
	                  NULL, NULL,
	                  mlview_marshal_VOID__VOID,
	                  G_TYPE_NONE, 0, NULL);

	gv_signals [DOCUMENT_RELOADED] =
	    g_signal_new ("document-reloaded",
	                  G_TYPE_FROM_CLASS (object_class),
	                  G_SIGNAL_RUN_FIRST,
	                  G_STRUCT_OFFSET
	                  (MlViewXMLDocumentClass,
	                   document_reloaded),
	                  NULL, NULL,
	                  mlview_marshal_VOID__VOID,
	                  G_TYPE_NONE, 0, NULL);

	gv_signals [GOING_TO_SAVE] =
	    g_signal_new ("going-to-save",
	                  G_TYPE_FROM_CLASS (object_class),
	                  G_SIGNAL_RUN_FIRST,
	                  G_STRUCT_OFFSET
	                  (MlViewXMLDocumentClass,
	                   going_to_save),
	                  NULL, NULL,
	                  mlview_marshal_VOID__VOID,
	                  G_TYPE_NONE, 0, NULL);

	gv_signals [DOCUMENT_UNDO_STATE_CHANGED] =
	    g_signal_new ("document-undo-state-changed",
	                  G_TYPE_FROM_CLASS (object_class),
	                  G_SIGNAL_RUN_FIRST,
	                  G_STRUCT_OFFSET
	                  (MlViewXMLDocumentClass,
	                   document_undo_state_changed),
	                  NULL, NULL,
	                  mlview_marshal_VOID__VOID,
	                  G_TYPE_NONE, 0, NULL);

	a_klass->document_changed = NULL;
	a_klass->node_cut = NULL;
	a_klass->child_node_added = NULL;
	a_klass->node_changed = NULL;
	a_klass->content_changed = NULL;
	a_klass->name_changed = NULL;
	a_klass->searched_node_found = NULL;
	a_klass->node_commented = mlview_xml_document_node_commented_cb ;
	a_klass->node_uncommented = mlview_xml_document_node_uncommented_cb ;
}

static void
schema_unassociated_cb (MlViewSchemaList *a_list,
                        MlViewSchema *a_schema,
                        MlViewXMLDocument *a_doc)
{
	gchar *url = NULL;

	THROW_IF_FAIL (a_schema);
	THROW_IF_FAIL (a_doc && MLVIEW_IS_XML_DOCUMENT (a_doc));
	THROW_IF_FAIL (PRIVATE (a_doc) && PRIVATE (a_doc)->native_doc);

	if (PRIVATE (a_doc)->native_doc->extSubset) {
		THROW_IF_FAIL (PRIVATE (a_doc)->native_doc->extSubset->SystemID);

		url = mlview_schema_get_url (a_schema);

		THROW_IF_FAIL (url);

		if (!strcmp ((char*)url,
		             (char*)PRIVATE
		             (a_doc)->native_doc->extSubset->SystemID))
			mlview_xml_document_set_ext_subset_with_url (a_doc, NULL);
	}
}

static void
schema_associated_cb (MlViewSchemaList *a_this,
                      MlViewSchema *a_schema,
                      gpointer a_user_data)
{
	enum MlViewSchemaType schema_type = SCHEMA_TYPE_UNDEF ;
	enum MlViewStatus status =  MLVIEW_OK ;
	MlViewXMLDocument *doc = NULL ;
	gchar *schema_url = NULL ;

	THROW_IF_FAIL (a_this && MLVIEW_IS_SCHEMA_LIST (a_this)
	               && a_schema) ;
	THROW_IF_FAIL (a_user_data && MLVIEW_IS_XML_DOCUMENT (a_user_data)) ;

	doc = MLVIEW_XML_DOCUMENT (a_user_data) ;
	THROW_IF_FAIL (doc) ;

	schema_url = mlview_schema_get_url (a_schema) ;
	THROW_IF_FAIL (schema_url) ;
	status = mlview_schema_get_type (a_schema, &schema_type) ;

	THROW_IF_FAIL (status == MLVIEW_OK
	               && schema_type != SCHEMA_TYPE_UNDEF) ;
	/*
	       if (schema_type == SCHEMA_TYPE_DTD) {
	               mlview_xml_document_set_ext_subset_with_url (doc, schema_url) ;
	       }
	*/
}

static void
mlview_xml_document_node_commented_cb (MlViewXMLDocument *a_this,
                                       xmlNode *a_node,
                                       xmlNode *a_new_node,
                                       gpointer a_user_data)
{
	THROW_IF_FAIL (a_this && MLVIEW_IS_XML_DOCUMENT (a_this)
	               && PRIVATE (a_this)
	               && a_node && a_new_node) ;
	THROW_IF_FAIL (a_node->type != XML_COMMENT_NODE
	               && a_new_node->type == XML_COMMENT_NODE) ;

	PRIVATE (a_this)->cur_node = a_new_node ;
	mlview_xml_document_select_node (a_this, a_new_node) ;
}

static void
mlview_xml_document_node_uncommented_cb (MlViewXMLDocument *a_this,
        xmlNode *a_node,
        xmlNode *a_new_node,
        gpointer a_user_data)
{
	THROW_IF_FAIL (a_this && MLVIEW_IS_XML_DOCUMENT (a_this)
	               && PRIVATE (a_this) && a_node && a_new_node) ;

	THROW_IF_FAIL (a_node->type == XML_COMMENT_NODE
	               && a_new_node->type != XML_COMMENT_NODE);

	PRIVATE (a_this)->cur_node = a_new_node ;
	mlview_xml_document_select_node (a_this, a_new_node) ;
}

/**
 *The init of the class instance.
 *@param a_xml_doc the newly built "this pointer".
 */
static void
mlview_xml_document_init (MlViewXMLDocument * a_xml_doc)
{
	THROW_IF_FAIL (a_xml_doc != NULL);
	THROW_IF_FAIL (MLVIEW_IS_XML_DOCUMENT (a_xml_doc));

	PRIVATE (a_xml_doc) = (MlViewXMLDocumentPrivate*)
	                      g_malloc0 (sizeof (MlViewXMLDocumentPrivate));

	PRIVATE (a_xml_doc)->schemas = mlview_schema_list_new ();

	THROW_IF_FAIL (PRIVATE (a_xml_doc)->schemas);

	g_signal_connect (G_OBJECT (PRIVATE (a_xml_doc)->schemas),
	                  "schema-unassociated",
	                  G_CALLBACK (schema_unassociated_cb),
	                  a_xml_doc);
	g_signal_connect (G_OBJECT (PRIVATE (a_xml_doc)->schemas),
	                  "schema-associated",
	                  G_CALLBACK (schema_associated_cb),
	                  a_xml_doc) ;
}

/**
 *Is called on the emission of the "document-changed" signal.
 *@param a_doc the instance of #MlViewXMLDocument that emitted
 *the signal.
 *@param a custom data pointed.
 */
static void
document_changed_cb (MlViewXMLDocument *a_doc,
                     gpointer a_data)
{
	THROW_IF_FAIL (a_doc
	               && MLVIEW_IS_XML_DOCUMENT (a_doc)
	               && PRIVATE (a_doc)) ;
	PRIVATE (a_doc)->modif_sequence ++ ;
	if (PRIVATE (a_doc)->file_desc) {
		mlview_file_descriptor_update_modified_time
		(PRIVATE (a_doc)->file_desc) ;
	}
}


/**
 *Builds a chained list where each element is an xml node.
 *the list is build by visiting the xml tree nodes in a depth first
 *scheme. During the visit, each node found is prepended to the list.
 *Yeah, we prepend each xml node to the list (not append) because
 *prepend is _much_ faster than append.
 *To walk the resulting tree, one must first, get the tail of
 *the list, and walk the list backward.
 *@param a_node the root of the xml tree to visit.
 *@param a_list out parameter. The list to build.
 *@param a_nodes_hash a lookup table built during the visit of
 *xml nodes. It contains pairs of (xml node/GList).
 *@return
 */
static enum MlViewStatus
build_tree_list_cache_real (xmlNode *a_node,
                            GList **a_list,
                            GHashTable **a_nodes_hash)
{
	GList *list = NULL ;
	GHashTable *hash = NULL ;
	xmlNode *cur_node = NULL ;
	enum MlViewStatus status = MLVIEW_OK ;
	list = *a_list ;
	hash = *a_nodes_hash ;

	if (!hash) {
		hash = g_hash_table_new (g_direct_hash,
		                         g_direct_equal) ;
		if (!hash) {
			mlview_utils_trace_debug
			("hash failed. "
			 "System may be out of memory.") ;
			return MLVIEW_OUT_OF_MEMORY_ERROR ;
		}
	}
	for (cur_node = a_node ;
	        cur_node ;
	        cur_node = cur_node->next) {
		list = g_list_prepend (list, cur_node) ;
		g_hash_table_insert (hash, cur_node, list) ;
		if (cur_node->children) {
			status = build_tree_list_cache_real
			         (cur_node->children,
			          &list, &hash) ;
			g_return_val_if_fail (status == MLVIEW_OK,
			                      status) ;
		}
	}
	*a_list = list ;
	*a_nodes_hash = hash ;
	return MLVIEW_OK ;
}

/**
 *Frees the list built by build_tree_list_cache_real()
 */
static void
free_tree_list_cache (MlViewXMLDocument *a_this)
{
	THROW_IF_FAIL (a_this
	               && MLVIEW_IS_XML_DOCUMENT (a_this)
	               && PRIVATE (a_this)) ;
	if (PRIVATE (a_this)->nodes_list) {
		g_list_free (PRIVATE (a_this)->nodes_list) ;
		PRIVATE (a_this)->nodes_list = NULL ;
	}
	if (PRIVATE (a_this)->nodes_hash) {
		g_hash_table_destroy (PRIVATE (a_this)->nodes_hash) ;
		PRIVATE (a_this)->nodes_hash = NULL ;
	}
}


/**
 *Builds an oredered list of all the xml node
 *of the current document.
 *This is used in the algorithm of "search node" feature.
 *@param a_this the current instance of #MlViewXMLDocument.
 */
static enum MlViewStatus
build_tree_list_cache (MlViewXMLDocument *a_this)
{
	enum MlViewStatus status = MLVIEW_OK ;

	g_return_val_if_fail (a_this
	                      && MLVIEW_XML_DOCUMENT (a_this)
	                      && PRIVATE (a_this)
	                      && PRIVATE (a_this)->native_doc,
	                      MLVIEW_BAD_PARAM_ERROR) ;
	if (!PRIVATE (a_this)->native_doc->children)
		return MLVIEW_OK ;
	status = build_tree_list_cache_real
	         (PRIVATE (a_this)->native_doc->children,
	          &PRIVATE (a_this)->nodes_list,
	          &PRIVATE (a_this)->nodes_hash) ;
	THROW_IF_FAIL (status == MLVIEW_OK) ;
	return status ;
}

/**
 *Searches a string in the various parts
 *of an xml node (name, content, attribute names, 
 *attribute values)
 *@param a_node the node to search in.
 *@param a_conf the search configuration (i.e: what string 
 *to search and where).
 *@return TRUE if the string has been found in this node.
 */
static gboolean
search_in_node (xmlNode *a_node,
                const struct SearchConfig *a_conf)
{
	gboolean node_matches = FALSE ;

	THROW_IF_FAIL (a_node && a_conf) ;

	if ((a_node->type == XML_ELEMENT_NODE
	        || a_node->type == XML_ENTITY_DECL
	        || a_node->type == XML_ENTITY_REF_NODE
	        || a_node->type == XML_PI_NODE
	        || a_node->type == XML_ENTITY_NODE
	        || a_node->type == XML_ELEMENT_DECL
	        || a_node->type == XML_NAMESPACE_DECL)
	        && (a_conf->where & NODE_NAME))
	{
		if (mlview_utils_strstr
		        ((gchar*)a_node->name, a_conf->search_string,
		         a_conf->ignore_case) == TRUE) {
			return TRUE ;
		}
	}
	if (a_node->type == XML_ELEMENT_NODE
	        && a_node->properties
	        && ((a_conf->where & NODE_ATTRIBUTE_NAME)
	            ||(a_conf->where & NODE_ATTRIBUTE_VALUE)))
	{
		xmlAttr *cur_attr = NULL;
		xmlChar *cur_attr_val = NULL ;
		/*
		 *cycle through the attribute list
		 *to see if one matches.
		 */
		for (cur_attr = a_node->properties;
		        cur_attr; cur_attr = cur_attr->next) {
			if (cur_attr->name
			        && (a_conf->where & NODE_ATTRIBUTE_NAME)
			        && mlview_utils_strstr
			        ((gchar*)cur_attr->name,
			         a_conf->search_string,
			         a_conf->ignore_case) == TRUE) {
				node_matches = TRUE ;
				break ;
			}
			if (cur_attr->name
			        && (a_conf->where
			            & NODE_ATTRIBUTE_VALUE)
			        && (cur_attr_val
			            = xmlGetProp (a_node,
			                          cur_attr->name))) {
				if (mlview_utils_strstr
				        ((gchar*)cur_attr_val,
				         a_conf->search_string,
				         a_conf->ignore_case) == TRUE) {
					node_matches = TRUE ;
				}
			}
			/*
			 *free the result of xmlGetProp,
			 *if any.
			 */
			if (cur_attr_val) {
				xmlFree (cur_attr_val) ;
				cur_attr_val = NULL ;
			}
			if (node_matches == TRUE)
				break ;
		}
	}
	if ((a_node->type == XML_TEXT_NODE
	        ||a_node->type == XML_COMMENT_NODE
	        ||a_node->type == XML_CDATA_SECTION_NODE
	        ||a_node->type == XML_PI_NODE)
	        && a_conf->where & NODE_CONTENT)
	{
		gchar *content = NULL ;
		content = (gchar*)xmlNodeGetContent (a_node) ;
		if (content
		        && mlview_utils_strstr
		        (content, a_conf->search_string,
		         a_conf->ignore_case) == TRUE) {
			node_matches = TRUE ;
		}
		if (content) {
			xmlFree (content) ;
			content = NULL ;
		}
	}
	return node_matches ;
}

/*****************
 *public methods *
 *****************/


/**
 *The actual #MlViewXMLDocument type ID builder.
 *@return the type ID of the #MlViewXMLDocument class.
 */
guint
mlview_xml_document_get_type (void)
{
	static guint type = 0;

	if (!type) {
		static const GTypeInfo type_info = {
		                                       sizeof (MlViewXMLDocumentClass),
		                                       NULL, NULL,
		                                       (GClassInitFunc)
		                                       mlview_xml_document_class_init,
		                                       NULL, NULL,
		                                       sizeof (MlViewXMLDocument),
		                                       0,
		                                       (GInstanceInitFunc)
		                                       mlview_xml_document_init
		                                   };
		type = g_type_register_static (G_TYPE_OBJECT,
		                               "MlViewXMLDocument",
		                               &type_info, (GTypeFlags)0);
	}

	return type;
}


/**
 *The class instance builder.
 *Builds a new instance of the #MlViewXMLDocument. Initialyzes
 *it with a native instance of xmlDoc * (see libxml2 to learn more
 *about xmlDoc*)
 *
 *@param a_xml_doc the native libxml2 xmlDocPtr.
 *@return the newly created instance of MlViewXMLDocument.
 */
MlViewXMLDocument *
mlview_xml_document_new (xmlDocPtr a_xml_doc)
{
	MlViewSchema *schema = NULL;
	MlViewXMLDocument *result = NULL;
	xmlDocPtr xml_doc = NULL;
	xmlNodePtr xml_node;
	gboolean res = FALSE;
	gchar *url = NULL;

	result = (MlViewXMLDocument*)g_object_new
	         (MLVIEW_TYPE_XML_DOCUMENT, NULL);

	xml_doc = a_xml_doc;

	if (xml_doc == NULL) {
		xml_doc = xmlNewDoc ((xmlChar*)"1.0");
		xml_node = xmlNewNode (NULL, (xmlChar*)"");
		xmlDocSetRootElement (xml_doc, xml_node);
	}

	PRIVATE (result)->native_doc = a_xml_doc;

	g_signal_connect (G_OBJECT (result),
	                  "document-changed",
	                  G_CALLBACK (document_changed_cb),
	                  NULL) ;


	if (a_xml_doc->extSubset) {
		if (!a_xml_doc->extSubset->SystemID)
			goto ext_subset_failed;

		/*
		 * Reload the DTD from file instead of sharing the one
		 * owned by a_xml_doc. Otherwise, memory management becomes
		 * a nightmare as the lifetime of a_xml_doc is different from
		 * the one the instance of MlViewXMLDocument we are building.
		 */
		schema = mlview_schema_load_from_file
		         ((gchar*)a_xml_doc->extSubset->SystemID,
		          SCHEMA_TYPE_DTD) ;

		if (!schema)
			goto ext_subset_failed;

		url = mlview_schema_get_url (schema);

		if (!url)
			goto schema_failed;

		res = mlview_schema_list_add_schema
		      (PRIVATE (result)->schemas, schema);

		if (!res)
			goto schema_failed;

		g_signal_emit (G_OBJECT (result),
		               gv_signals [EXT_SUBSET_CHANGED], 0) ;

		mlview_schema_unref (schema) ;
		schema = NULL;
	}

	return result;

ext_subset_failed:
	if (a_xml_doc->extSubset) {
		xmlFreeDtd (a_xml_doc->extSubset);
		a_xml_doc->extSubset = NULL;
	}

	return result;

schema_failed:
	if (schema) {
		mlview_schema_unref (schema);
		schema = NULL;
	}

	return result;
}


/************************************
 *clipboard methods.
 ***********************************/

/**
 *Stores the a copy of the xmlNode given in argument into the node clipboard.
 *The cilpboard is a circular buffer. It cannot be overflowed. 
 *
 *@param a_visual_node the pointer to 
 *xmlNode to store in the clipboard. If it is a 
 *NULL pointer, a NULL pointer is actually stored into clipboard.
 */
void
mlview_xml_document_copy_node_to_clipboard2 (xmlNode * a_xml_node,
											 xmlDoc * a_doc)
{
	THROW_IF_FAIL (a_xml_node != NULL);

	mlview::AppContext *context = mlview::AppContext::get_instance () ;
	THROW_IF_FAIL (context) ;
	mlview::Clipboard * cb = context->get_clipboard () ;
	THROW_IF_FAIL (cb) ;
	cb->put (a_xml_node) ;
}

/**
 *Gets the last node stored in the node clipboard.
 *
 *@param a_xml_doc the "this pointer".
 *@return a copy of the 
 *xmlNode that has been previously stored. 
 */
xmlNode *
mlview_xml_document_get_node_from_clipboard2 (xmlDoc * a_xml_doc)
{
	xmlNode *xml_node = NULL;

	THROW_IF_FAIL (a_xml_doc) ;
	mlview::AppContext *context = mlview::AppContext::get_instance () ;
	THROW_IF_FAIL (context) ;
	mlview::Clipboard *cb = context->get_clipboard () ;
	THROW_IF_FAIL (cb) ;
	xml_node = cb->get (a_xml_doc) ;
	return xml_node;
}

/*************************************
 *Other miscellaneous methods
 *************************************/

/**
 *Opens and loads the XML file contains in the filename given in parameter. 
 *The user IS prompted for a DTD.
 *
 *@param a_file_name the total file path of the file that contains 
 *the xml document to open
 *the current instance of MlViewXMLDocument.
 *@return the newly loaded xml document.
 */
MlViewXMLDocument *
mlview_xml_document_open_with_dtd_interactive (const gchar * a_file_name)
{
	MlViewXMLDocument *result = NULL;
	xmlDoc *xml_doc = NULL;

	THROW_IF_FAIL (a_file_name);
	xml_doc = mlview_parsing_utils_load_xml_file_with_dtd_interactive
											(a_file_name);

	if (!xml_doc)
		return NULL;

	result = mlview_xml_document_new (xml_doc);

	mlview_xml_document_set_file_path (result, a_file_name);

	return result;
}

gboolean
mlview_xml_document_set_ext_subset_with_url (MlViewXMLDocument *a_doc,
        const gchar *a_url)
{
	MlViewSchema *schema = NULL;
	gboolean res = FALSE;
	xmlDtdPtr old_subset = NULL;
	enum MlViewSchemaType schema_type = SCHEMA_TYPE_UNDEF ;
	enum MlViewStatus status = MLVIEW_OK ;

	THROW_IF_FAIL (a_doc && MLVIEW_IS_XML_DOCUMENT (a_doc));
	THROW_IF_FAIL (PRIVATE (a_doc) && PRIVATE (a_doc)->schemas);
	g_return_val_if_fail (MLVIEW_IS_SCHEMA_LIST (PRIVATE (a_doc)->schemas),
	                      FALSE);
	THROW_IF_FAIL (PRIVATE (a_doc)->native_doc);

	old_subset = PRIVATE (a_doc)->native_doc->extSubset;

	if (!a_url) {
		PRIVATE (a_doc)->native_doc->extSubset = NULL;
		PRIVATE (a_doc)->native_doc->standalone = FALSE;
	} else {
		schema = mlview_schema_list_lookup_by_url (PRIVATE (a_doc)->schemas, a_url);

		if (!schema) {
			schema = mlview_schema_load_from_file
			         (a_url, SCHEMA_TYPE_DTD) ;
			if (!schema)
				return FALSE;

			res = mlview_schema_list_add_schema
			      (PRIVATE (a_doc)->schemas, schema);

			mlview_schema_unref (schema);

			if (!res)
				return FALSE;
		}

		mlview_schema_get_type (schema, &schema_type) ;
		g_return_val_if_fail (schema_type != SCHEMA_TYPE_UNDEF,
		                      FALSE) ;
		if (schema_type != SCHEMA_TYPE_DTD)
			return FALSE;

		/*
		 * We now need to set the extSubset of the native doc to
		 * a dtd. When we do that, the native doc will own that dtd.
		 * Therefore, we need to reload the dtd to have a pointer owned
		 * by the doc only. This may be seen as a waste of resource, but
		 * we are obliged to do so as libxml2 doesn't do refcounting of
		 * its internal objects like dtds.
		 */
		schema = mlview_schema_load_from_file
		         (a_url, SCHEMA_TYPE_DTD) ;
		if (!schema) {
			return FALSE ;
		}
		status = mlview_schema_get_native_schema
		         (schema,
		          (gpointer *)&PRIVATE (a_doc)->native_doc->extSubset) ;
		THROW_IF_FAIL (status == MLVIEW_OK) ;
		mlview_schema_destroy (schema , FALSE) ;
		schema = NULL ;

		PRIVATE (a_doc)->native_doc->standalone = TRUE;
		PRIVATE (a_doc)->native_doc->extSubset->doc =
		    PRIVATE (a_doc)->native_doc;
	}

	if (old_subset)
		mlview_parsing_utils_clean_dtd (old_subset);

	g_signal_emit (G_OBJECT (a_doc), gv_signals [EXT_SUBSET_CHANGED],
	               0);

	return TRUE;
}

/**
 *Opens and loads the XML file contains in the filename given in parameter. 
 *If the specified DTD name is NULL, no DTD is associated with the document.
 *The user is NOT prompted for a DTD.
 *
 *@param a_file_name the total file path of the file that contains 
 *the xml document to open
 *@param a_dtd_name the total file path of the file that contains
 *the dtd to validate against
 *the current instance of MlViewXMLDocument.
 *@return the newly loaded xml document.
 */
MlViewXMLDocument *
mlview_xml_document_open_with_dtd (const gchar * a_file_name,
                                   const gchar * a_dtd_name)
{
	MlViewXMLDocument *result = NULL;
	xmlDoc *xml_doc = NULL;

	THROW_IF_FAIL (a_file_name);
	xml_doc = mlview_parsing_utils_load_xml_file_with_dtd 
					(a_file_name, a_dtd_name);

	if (!xml_doc)
		return NULL;

	result = mlview_xml_document_new (xml_doc);

	mlview_xml_document_set_file_path (result, a_file_name);

	return result;
}

/**
 *Validates the document agains the dtd that is associated to it. 
 *
 *@param a_doc the document to validate.
 */
void
mlview_xml_document_validate (MlViewXMLDocument * a_doc)
{
	/*GtkWidget *win = NULL;
	MlViewValidationContext *ctxt = NULL;

	THROW_IF_FAIL (a_doc != NULL);
	THROW_IF_FAIL (PRIVATE (a_doc) != NULL);
	THROW_IF_FAIL (PRIVATE (a_doc)->native_doc);

	ctxt = mlview_validation_context_new_with_document_and_dtd (a_doc);

	THROW_IF_FAIL (ctxt);

	win = mlview_validation_report_new_with_validation_context (PRIVATE (a_doc)->app_context,
	                                                            ctxt);

	mlview_validation_context_unref (ctxt);

	gtk_widget_show_all (win);*/
}


/**
 *Saves the document.
 *
 *@param a_doc the current instance of MlViewXMLDocument
 *@param a_file_path the path where to save the document.
 *@param a_check_overwrt  whether to check and warn user
 *when the we are going to overwrite a file.
 *@return the number of bytes saved or -1 in case of error.
 */
gint
mlview_xml_document_save (MlViewXMLDocument * a_doc,
                          const gchar * a_file_path,
                          gboolean a_check_overwrt)
{
	MlViewFileDescriptor *file_desc = NULL;
	gchar *cur_doc_file_path = NULL ;
	gboolean save_file = TRUE,
	                     is_reg_file = TRUE;

	THROW_IF_FAIL (a_doc != NULL) ;
	THROW_IF_FAIL (PRIVATE (a_doc) != NULL) ;

	if (PRIVATE (a_doc)->file_desc) {
		cur_doc_file_path = (gchar*)mlview_file_descriptor_get_file_path
		                    (PRIVATE (a_doc)->file_desc) ;
	}
	if (a_check_overwrt == TRUE
	        && (!cur_doc_file_path
	            || strcmp ((char*)cur_doc_file_path, (char*)a_file_path))) {
		file_desc =
		    mlview_file_descriptor_new (a_file_path);
		THROW_IF_FAIL (file_desc);

		if (!mlview_file_descriptor_is_regular_file
		        (file_desc, &is_reg_file)
		        && is_reg_file == TRUE) {
			GtkDialog *ok_cancel_dialog = NULL;
			GtkLabel *message = NULL;
			gchar *str = NULL;

			str = g_strdup_printf
			      (_("The file '%s' already exists.\n"
			         "Do you want to overwrite it?"),
			       a_file_path);
			message =
			    GTK_LABEL (gtk_label_new (str));
			ok_cancel_dialog =
			    GTK_DIALOG
			    (gtk_dialog_new_with_buttons
			     (_("Save"), NULL,
			      GTK_DIALOG_MODAL,
			      GTK_STOCK_NO, GTK_RESPONSE_REJECT,
			      GTK_STOCK_YES, GTK_RESPONSE_ACCEPT,
			      NULL));
			g_return_val_if_fail
			        (ok_cancel_dialog, -1) ;
			gtk_dialog_set_default_response
			(GTK_DIALOG (ok_cancel_dialog),
			 GTK_RESPONSE_ACCEPT) ;
			gtk_box_pack_start
			(GTK_BOX
			 (GTK_DIALOG
			  (ok_cancel_dialog)->vbox),
			 GTK_WIDGET (message),
			 TRUE, TRUE, 0);

			gtk_widget_show_all
			(GTK_WIDGET
			 (GTK_DIALOG
			  (ok_cancel_dialog)->vbox));

			if (str) {
				g_free (str);
				str = NULL;
			}
			if (ok_cancel_dialog) {
				gint button = 0;

				button = gtk_dialog_run
				         (ok_cancel_dialog);

				switch (button) {
				case GTK_RESPONSE_ACCEPT:
					/*The OK Button was pressed */
					save_file = TRUE;
					break;

				case GTK_RESPONSE_REJECT:
					/*The cancel Button was pressed */
				case GTK_RESPONSE_CLOSE:
				default:
					save_file = FALSE;
					break;
				}
				gtk_widget_destroy
				(GTK_WIDGET
				 (ok_cancel_dialog));
			}
		}
	}

	if (file_desc) {
		mlview_file_descriptor_destroy (file_desc);
		file_desc = NULL;
	}
	if (save_file == TRUE) {
		gint res = 0;

		res = mlview_xml_document_save_xml_doc
		      (a_doc, a_file_path) ;

		if (res > 0)
			mlview_xml_document_set_file_path
			(a_doc, a_file_path);
		return res;
	}

	return 0;
}


/**
 *Clones the current instance of MlViewXMLDocument. 
 *@param a_original the current instance of MlViewXMLDocument.
 *@return the cloned instance of #MlViewXMLDocument.
 */
MlViewXMLDocument *
mlview_xml_document_clone (MlViewXMLDocument * a_original)
{
	MlViewXMLDocument *result;
	MlViewFileDescriptor *file_desc;
	gchar *file_path;

	if (a_original == NULL)
		return NULL;
	file_desc =
	    mlview_xml_document_get_file_descriptor
	    (a_original);
	file_path =
	    mlview_file_descriptor_get_file_path (file_desc);

	result = mlview_xml_document_open_with_dtd_interactive (file_path);
	return result;
}

/**
 *Sets the file path associated to the current instance
 *of MlViewXMLDocument.
 *@param a_xml_doc the "this" pointer of the current object.
 *@param a_file_path the new file path.
 *
 */
void
mlview_xml_document_set_file_path (MlViewXMLDocument * a_xml_doc,
                                   const gchar * a_file_path)
{
	THROW_IF_FAIL (a_xml_doc != NULL);
	THROW_IF_FAIL (PRIVATE (a_xml_doc) != NULL);

	if (PRIVATE (a_xml_doc)->file_desc == NULL) {
		PRIVATE (a_xml_doc)->file_desc =
		    mlview_file_descriptor_new (a_file_path);
	} else {
		/* should never get there */
		mlview_file_descriptor_set_file_path
		(PRIVATE (a_xml_doc)->file_desc,
		 a_file_path);
	}

	g_signal_emit (G_OBJECT (a_xml_doc),
	               gv_signals[FILE_PATH_CHANGED], 0);
}


/**
 *Gets the file path associated to the current instance of
 *#MlViewXMLDocument.
 *@param a_xml_doc the "this" pointer of the current
 *instance.
 *@return the file path. Note that the caller MUST free
 *the returned string returned using g_free() !
 */
gchar *
mlview_xml_document_get_file_path (MlViewXMLDocument * a_xml_doc)
{
	g_return_val_if_fail (a_xml_doc
	                      && PRIVATE (a_xml_doc), NULL);

	if (PRIVATE (a_xml_doc)->file_desc == NULL) {
		return NULL;
	} else {
		return mlview_file_descriptor_get_file_path
		       (PRIVATE (a_xml_doc)->file_desc);
	}
}


/**
 *throughout the sourcecode, use mlview_xml_document_get_node_path()
 *instead of xmlGetNodePath().
 *
 */
xmlNode *
mlview_xml_document_get_node_from_xpath (MlViewXMLDocument *a_this,
        const gchar *an_xpath_expr)
{
	xmlXPathContext *xpath_ctxt = NULL ;
	xmlXPathObject *xpath_object = NULL ;
	xmlNode *result = NULL ;

	g_return_val_if_fail (a_this && MLVIEW_IS_XML_DOCUMENT (a_this)
	                      && PRIVATE (a_this)
	                      && PRIVATE (a_this)->native_doc,
	                      NULL) ;

	xpath_ctxt = PRIVATE (a_this)->xpath_context ;
	THROW_IF_FAIL (xpath_ctxt) ;
	xpath_object = xmlXPathEvalExpression ((xmlChar*)an_xpath_expr,
	                                       xpath_ctxt) ;
	if (!xpath_object || xpath_object->type != XPATH_NODESET
	        || !xpath_object->nodesetval || !xpath_object->nodesetval->nodeTab
	        || !xpath_object->nodesetval->nodeNr) {
		mlview_utils_trace_debug ("xpath evaluation didn't return a set of nodes :(") ;
		return NULL ;
	}
	result = xpath_object->nodesetval->nodeTab[0] ;
	return result ;
}

/**
 *Adds a child node to the a_parent_node node. 
 *After adding the child node, this methods emits the following
 *signals: "child-node-added" and "document-changed".
 *
 *@param a_xml_node the node to add.
 *@param a_xml_doc the current instance of MlViewXMLDocument.
 *@param a_parent_node the parent of the node to add.
 *@return the newly added node or NULL if adding failed.
 */
enum MlViewStatus
mlview_xml_document_add_child_node (MlViewXMLDocument *a_this,
                                    const gchar * a_parent_xml_node_path,
                                    xmlNode* a_xml_node,
                                    gboolean a_subtree_required,
                                    gboolean a_emit_signal)
{
	enum MlViewStatus status = MLVIEW_OK ;
	MlViewDocMutation *add_child_node_mutation = NULL ;

	THROW_IF_FAIL (a_this != NULL);
	g_return_val_if_fail (MLVIEW_IS_XML_DOCUMENT (a_this),
	                      MLVIEW_BAD_PARAM_ERROR);
	THROW_IF_FAIL (PRIVATE (a_this));
	THROW_IF_FAIL (a_parent_xml_node_path);
	THROW_IF_FAIL (a_xml_node != NULL);

	add_child_node_mutation = mlview_doc_mutation_new
	                          (a_this,
	                           mlview_xml_document_do_mutation_add_child_node,
	                           mlview_xml_document_undo_mutation_add_child_node,
	                           "add-child-node") ;
	if (!add_child_node_mutation) {
		mlview_utils_trace_debug ("Could not instanciate mutation object") ;
		return MLVIEW_ERROR ;
	}

	g_object_set_data (G_OBJECT (add_child_node_mutation),
	                   "add-child-node::parent-xml-node-path",
	                   (gpointer)g_strdup (a_parent_xml_node_path)) ;
	g_object_set_data (G_OBJECT (add_child_node_mutation),
	                   "add-child-node::xml-node",
	                   a_xml_node) ;
	g_object_set_data (G_OBJECT (add_child_node_mutation),
	                   "add-child-node::subtree-required",
	                   GINT_TO_POINTER (a_subtree_required)) ;
	g_object_set_data (G_OBJECT (add_child_node_mutation),
	                   "add-child-node::emit-signal",
	                   GINT_TO_POINTER (a_emit_signal)) ;

	status = mlview_doc_mutation_do_mutation (add_child_node_mutation,
	         NULL) ;

	if (status == MLVIEW_OK) {
		mlview_xml_document_record_mutation_for_undo (a_this,
		        add_child_node_mutation,
		        TRUE) ;
	}
	return status ;
}


/**
 *Cuts the node a_xml_node. The node is stored in the node clipboard
 *and then unlinked from the xml tree. 
 *After a successfull cut, this methods emits the following signals:
 *"node-cut" and "document-changed".
 *
 *@param a_this the current xml node
 *@return the xmlNode cut or NULL if something bad happened.
 */
enum MlViewStatus
mlview_xml_document_cut_node (MlViewXMLDocument * a_this,
                              const gchar * a_xml_node_path,
                              gboolean a_emit_signal)
{
	enum MlViewStatus status = MLVIEW_OK ;
	MlViewDocMutation *mutation = NULL ;
	gchar *xml_node_path = NULL ;

	THROW_IF_FAIL (a_this != NULL);
	g_return_val_if_fail (MLVIEW_IS_XML_DOCUMENT (a_this),
	                      MLVIEW_BAD_PARAM_ERROR);
	g_return_val_if_fail (PRIVATE (a_this) != NULL,
	                      MLVIEW_BAD_PARAM_ERROR);
	g_return_val_if_fail (PRIVATE (a_this)->native_doc !=
	                      NULL, MLVIEW_BAD_PARAM_ERROR);
	g_return_val_if_fail (a_xml_node_path != NULL,
	                      MLVIEW_BAD_PARAM_ERROR);

	xml_node_path = g_strdup (a_xml_node_path) ;
	if (!xml_node_path) {
		mlview_utils_trace_debug ("System may be out of memory") ;
		return MLVIEW_ERROR ;
	}
	mutation = mlview_doc_mutation_new
	           (a_this,
	            mlview_xml_document_do_mutation_cut_node,
	            mlview_xml_document_undo_mutation_cut_node,
	            "cut-node") ;
	if (!mutation) {
		mlview_utils_trace_debug ("Could node instanciate mutation object") ;
		return MLVIEW_ERROR ;
	}

	g_object_set_data (G_OBJECT (mutation),
	                   "cut-node::node-to-cut-path",
	                   (gpointer) xml_node_path) ;
	g_object_set_data (G_OBJECT (mutation),
	                   "cut-node::emit-signal",
	                   GINT_TO_POINTER (a_emit_signal)) ;

	status = mlview_doc_mutation_do_mutation (mutation, NULL) ;
	if (status == MLVIEW_OK) {
		mlview_xml_document_record_mutation_for_undo (a_this,
		        mutation,
		        TRUE) ;
	}

	return status;
}


/**
 *Inserts a_sibling_node as the previous sibling node of a_xml_node. 
 *@param a_this the current xml document. Must be not NULL.
 *@param a_sibling_node a the new sibling node to insert. Must be not NULL.
 *@param a_xml_node the current reference node. Must be not NULL.
 *@param a_subtree_required if TRUE, add a subtree to a_sibling_node to
 *make the a_sibling_node validate.
 *@param a_emit_signal if TRUE, emits the signals "prev-sibling-node-inserted"
 *and "document-changed" upon successful completion.
 *@return the newly insterted sibling (a_sibling_node) or 
 *NULL if something BAD happened.
 */
enum MlViewStatus
mlview_xml_document_insert_prev_sibling_node (MlViewXMLDocument *a_this,
        const gchar * a_sibling_node_path,
        xmlNode *a_xml_node,
        gboolean a_subtree_required,
        gboolean a_emit_signal)
{
	enum MlViewStatus status = MLVIEW_OK ;
	MlViewDocMutation *mutation = NULL ;
	gchar *sibling_node_path = NULL ;

	THROW_IF_FAIL (a_this != NULL);
	THROW_IF_FAIL (MLVIEW_IS_XML_DOCUMENT (a_this));
	THROW_IF_FAIL (PRIVATE (a_this));
	THROW_IF_FAIL (a_sibling_node_path != NULL);
	THROW_IF_FAIL (a_xml_node != NULL);

	mutation = mlview_doc_mutation_new 
	(a_this, mlview_xml_document_do_mutation_insert_prev_sibling_node,
	mlview_xml_document_undo_mutation_insert_prev_sibling_node,
	"insert-prev-sibling") ;
	if (!mutation) {
		mlview_utils_trace_debug ("Could not instanciate a mutation") ;
		return MLVIEW_ERROR ;
	}
	sibling_node_path = g_strdup (a_sibling_node_path) ;
	if (!sibling_node_path) {
		mlview_utils_trace_debug ("system may be out of memory") ;
		return MLVIEW_ERROR ;
	}

	g_object_set_data (G_OBJECT (mutation),
	                   "insert-prev-sibling-node::sibling-node-path",
	                   sibling_node_path) ;
	g_object_set_data (G_OBJECT (mutation),
	                   "insert-prev-sibling-node::xml-node",
	                   a_xml_node) ;
	g_object_set_data (G_OBJECT (mutation),
	                   "insert-prev-sibling-node::subtree-required",
	                   GINT_TO_POINTER (a_subtree_required)) ;
	g_object_set_data (G_OBJECT (mutation),
	                   "insert-prev-sibling-node::emit-signal",
	                   GINT_TO_POINTER(a_emit_signal)) ;

	status = mlview_doc_mutation_do_mutation (mutation, NULL) ;
	if (status == MLVIEW_OK) {
		mlview_xml_document_record_mutation_for_undo (a_this,
		        mutation,
		        TRUE) ;
	} else {
		mlview_utils_trace_debug ("mutation failed") ;
	}
	return status ;
}


/**
 *Inserts a_sibling_node as  the next sibling of a_xml_node. 
 *
 *@param a_xml_node the reference xml node.
 *@param a_this the current xml document.
 *@param a_sibling_node the new sibling node to insert.
 *@param a_subtree_required if TRUE, add a subtree to a_sibling_node to
 *make it validate against the DTD associated to the current xml document.
 *@param a_emit_signal if TRUE emits signals 
 *"next-sibling-node-inserted" and "document-changed" 
 *upon successful completion.
 *@return the newly inserted next sibling node (a_sibling_node) or NULL
 *if an error arises.
 */
enum MlViewStatus
mlview_xml_document_insert_next_sibling_node (MlViewXMLDocument *a_this,
        const gchar * a_sibling_node_path,
        xmlNode * a_xml_node,
        gboolean a_subtree_required,
        gboolean a_emit_signal)
{
	enum MlViewStatus status = MLVIEW_OK ;
	MlViewDocMutation *mutation = NULL ;
	gchar *sibling_node_path = NULL ;

	THROW_IF_FAIL (a_this != NULL);
	THROW_IF_FAIL (MLVIEW_IS_XML_DOCUMENT (a_this));
	THROW_IF_FAIL (PRIVATE (a_this));
	THROW_IF_FAIL (a_sibling_node_path != NULL);
	THROW_IF_FAIL (a_xml_node != NULL);

	sibling_node_path = g_strdup (a_sibling_node_path) ;
	if (!sibling_node_path) {
		mlview_utils_trace_debug ("System may be out of memory") ;
		return MLVIEW_ERROR ;
	}
	mutation = mlview_doc_mutation_new (a_this,
	                                    mlview_xml_document_do_mutation_insert_next_sibling_node,
	                                    mlview_xml_document_undo_mutation_insert_next_sibling_node,
	                                    "insert-next-sibling") ;
	if (!mutation) {
		mlview_utils_trace_debug ("Could not instanciate a mutation") ;
		return MLVIEW_ERROR ;
	}
	g_object_set_data (G_OBJECT (mutation),
	                   "insert-next-sibling-node::sibling-node-path",
	                   sibling_node_path) ;
	g_object_set_data (G_OBJECT (mutation),
	                   "insert-next-sibling-node::xml-node",
	                   a_xml_node) ;
	g_object_set_data (G_OBJECT (mutation),
	                   "insert-next-sibling-node::subtree-required",
	                   GINT_TO_POINTER (a_subtree_required)) ;
	g_object_set_data (G_OBJECT (mutation),
	                   "insert-next-sibling-node::emit-signal",
	                   GINT_TO_POINTER(a_emit_signal)) ;

	status = mlview_doc_mutation_do_mutation (mutation, NULL) ;
	if (status == MLVIEW_OK) {
		mlview_xml_document_record_mutation_for_undo (a_this,
		        mutation,
		        TRUE) ;
	}
	return status ;
}


/**
 *Sets a new content a_content to a_node.
 *After setting the content, this method emits the following
 *signals: "content-changed", "node-changed" and "document-changed".
 *
 *@param a_content the new content to set to a_node.
 *@param a_this the current xml document.
 *@param a_node the node to set the new content to.
 *
 *@return the a_node with the new content or NULL if something bad happened. 
 */
enum MlViewStatus
mlview_xml_document_set_node_content (MlViewXMLDocument * a_this,
                                      const gchar * a_node_path,
                                      gchar * a_content,
                                      gboolean a_emit_signal)
{
	enum MlViewStatus status =  MLVIEW_OK ;
	MlViewDocMutation *mutation = NULL ;
	gchar *node_path = NULL ;

	THROW_IF_FAIL (a_this != NULL);
	g_return_val_if_fail (MLVIEW_IS_XML_DOCUMENT
	                      (a_this), MLVIEW_BAD_PARAM_ERROR);
	THROW_IF_FAIL (a_node_path != NULL);

	mutation = mlview_doc_mutation_new (a_this,
	                                    mlview_xml_document_do_mutation_set_node_content,
	                                    mlview_xml_document_undo_mutation_set_node_content,
	                                    "set-node-content") ;

	node_path = g_strdup (a_node_path) ;

	g_object_set_data (G_OBJECT (mutation),
	                   "set-node-content::node-path",
	                   node_path) ;
	g_object_set_data (G_OBJECT (mutation),
	                   "set-node-content::content",
	                   a_content) ;
	g_object_set_data (G_OBJECT (mutation),
	                   "set-node-content::emit-signal",
	                   GINT_TO_POINTER (a_emit_signal)) ;

	status = mlview_doc_mutation_do_mutation (mutation, NULL) ;

	if (status == MLVIEW_OK) {
		mlview_xml_document_record_mutation_for_undo (a_this,
		        mutation,
		        TRUE) ;
	}
	return status ;
}

/**
 *Selects a node of the XML document.
 *This actually emits a "node-selected" signal only
 *It's up to the listeners of that signal to actually
 *do what's needed to select the node.
 */
void
mlview_xml_document_select_node (MlViewXMLDocument *a_this,
                                 xmlNode *a_node)
{
	THROW_IF_FAIL (a_this
	               && MLVIEW_IS_XML_DOCUMENT (a_this)
	               && PRIVATE (a_this)
	               && a_node) ;

	if (PRIVATE (a_this)->cur_node
	        && PRIVATE (a_this)->cur_node != a_node) {
		g_signal_emit (G_OBJECT (a_this),
		               gv_signals[NODE_UNSELECTED],0,
		               PRIVATE (a_this)->cur_node) ;
	}
	PRIVATE (a_this)->cur_node = a_node;
	g_signal_emit (G_OBJECT (a_this),
	               gv_signals[NODE_SELECTED], 0,
	               a_node) ;
}


/**
 *Sets or reset the attribute value.
 *@param a_this the current instance of #MlViewXMLDocument.
 *@param a_node the xml node to consider.
 *@param a_name the name of the attribute
 *@param a_value the value of the attribute.
 *@param a_emit_signal if TRUE, the function emits the signals
 *"node-attribute-added", "node-attribute-value-changed", and
 *"node-changed" signals in case of successfull completion.
 *@return the xmlAttr * of the attribute set.
 *upon successfull completion, an error code
 *otherwise.
 */
enum MlViewStatus
mlview_xml_document_set_attribute (MlViewXMLDocument *a_this,
                                   const gchar *a_node_path,
                                   const xmlChar* a_name,
                                   const xmlChar *a_value,
                                   gboolean a_emit_signal)
{
	enum MlViewStatus status = MLVIEW_OK ;
	gchar *node_path = NULL, *name=NULL, *value=NULL ;
	MlViewDocMutation *mutation = NULL ;

	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_XML_DOCUMENT (a_this)
	                      && PRIVATE (a_this)
	                      && a_node_path
	                      && a_name && a_value,
	                      MLVIEW_BAD_PARAM_ERROR) ;


	mutation = mlview_doc_mutation_new (a_this,
	                                    mlview_xml_document_do_mutation_set_attribute,
	                                    mlview_xml_document_undo_mutation_set_attribute,
	                                    "set-attribute") ;
	if (!mutation) {
		mlview_utils_trace_debug ("Could not instanciate the mutation object") ;
		return MLVIEW_ERROR ;
	}

	node_path = g_strdup (a_node_path) ;
	if (!node_path) {
		mlview_utils_trace_debug ("System may be out of memory") ;
		return MLVIEW_ERROR ;
	}
	name = g_strdup ((gchar*)a_name) ;
	if (!name) {
		mlview_utils_trace_debug ("System may be out of memory") ;
		return MLVIEW_ERROR ;
	}
	value = g_strdup ((gchar*)a_value) ;
	if (!value) {
		mlview_utils_trace_debug ("System may be out of memory") ;
		return MLVIEW_ERROR ;
	}

	g_object_set_data (G_OBJECT (mutation),
	                   "set-attribute::node-path",
	                   node_path) ;

	g_object_set_data (G_OBJECT (mutation),
	                   "set-attribute::attribute-name",
	                   name) ;
	g_object_set_data (G_OBJECT (mutation),
	                   "set-attribute::attribute-value",
	                   value) ;
	g_object_set_data (G_OBJECT (mutation),
	                   "set-attribute::emit-signal",
	                   GINT_TO_POINTER (a_emit_signal)) ;

	status = mlview_doc_mutation_do_mutation (mutation, NULL) ;

	if (status == MLVIEW_OK) {
		mlview_xml_document_record_mutation_for_undo (a_this,
		        mutation,
		        TRUE) ;
	}
	return status ;
}

/**
 *Removes an attribute from the a node of the tree.
 *@param a_this the current instance of #MlViewXMLDocument.
 *@param a_node the node that holds the attribute to remove.
 *@param a_name the name of attribute to remove.
 *@param a_emit_signal if set to TRUE, the function emits the
 *"node-attribute-removed" signal.
 *@return MLVIEW_OK upon successful completion, an error code
 *otherwise.
 */
enum MlViewStatus
mlview_xml_document_remove_attribute (MlViewXMLDocument *a_this,
                                      xmlNode *a_node,
                                      const xmlChar *a_name,
                                      gboolean a_emit_signal)
{
	xmlAttr *attr = NULL ;
	xmlChar *attr_name = NULL ;

	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_XML_DOCUMENT (a_this)
	                      && PRIVATE (a_this)
	                      && a_node && a_name,
	                      MLVIEW_BAD_PARAM_ERROR) ;
	attr = xmlHasProp (a_node, a_name) ;
	if (attr) {
		/*
		 *a_name may be allocated on attr
		 *before removing (freeing) attr
		 *let's keep a copy of a_name.
		 */
		attr_name = xmlStrdup (a_name) ;
		if (!attr_name) {
			mlview_utils_trace_debug ("xmlStrdup failed. "
			                          "system may be out of memory.") ;
			return MLVIEW_OUT_OF_MEMORY_ERROR ;
		}
		xmlRemoveProp (attr) ;
		attr = NULL ;
		if (a_emit_signal == TRUE) {
			g_signal_emit
			(G_OBJECT (a_this),
			 gv_signals[NODE_ATTRIBUTE_REMOVED], 0,
			 a_node,
			 attr_name/*use our copy of a_name here.*/) ;
			xmlFree (attr_name) ;
			attr_name = NULL ;
			g_signal_emit
			(G_OBJECT (a_this),
			 gv_signals[NODE_CHANGED], 0,
			 a_node) ;
			g_signal_emit
			(G_OBJECT (a_this),
			 gv_signals[DOCUMENT_CHANGED], 0);
		}
	}
	return MLVIEW_OK ;
}

/**
 *Makes the attribute list of a given node equals the
 *attribute list given in parameter.
 *@param a_doc the current instance of #MlViewXMLDocument
 *@param a_node the xml node to consider.
 *@param a_nv_pair_list the list of attributes to synch with.
 *@return MLVIEW_OK upon successfull completion, an error code otherwise.
 */
enum MlViewStatus
mlview_xml_document_synch_attributes (MlViewXMLDocument *a_doc,
                                      const gchar *a_node_path,
                                      GList *a_nv_pair_list)
{
	struct NameValuePair *cur_nv_pair = NULL ;
	xmlAttr *cur_attr = NULL , *attr_to_remove = NULL;
	xmlNode *node = NULL ;

	node = mlview_xml_document_get_node_from_xpath (a_doc, a_node_path) ;
	if (!node) {
		mlview_utils_trace_debug ("XPATH expr could not resolve to XML node") ;
		return MLVIEW_ERROR ;
	}

	if (!a_nv_pair_list) {
		/*remove all the attributes from the xml node*/
		for (attr_to_remove = NULL, cur_attr = node->properties ;
		        cur_attr ;
		        attr_to_remove = cur_attr, cur_attr = cur_attr->next) {
			if (attr_to_remove) {
				mlview_xml_document_remove_attribute
				(a_doc, node,
				 attr_to_remove->name,
				 TRUE) ;
				attr_to_remove = NULL ;
			}
		}
		if (attr_to_remove) {
			mlview_xml_document_remove_attribute
			(a_doc, node,
			 attr_to_remove->name,
			 TRUE) ;
			attr_to_remove = NULL ;
		}
	} else {
		GList *cur_item = NULL ;
		/*add attributes that are to be added.*/
		for (cur_item = a_nv_pair_list ; cur_item ;
		        cur_item = cur_item->next) {
			xmlChar *attr_value = NULL ;

			cur_nv_pair = (struct NameValuePair*)cur_item->data ;
			if (!cur_nv_pair || !cur_nv_pair->name
			        || !cur_nv_pair->name->str)
				continue ;
			attr_value = xmlGetProp
			             (node,  (xmlChar*)cur_nv_pair->name->str) ;

			if (!attr_value
			        || strcmp ((char*)attr_value,
			                   (char*)cur_nv_pair->value->str)) {
				mlview_xml_document_set_attribute
				(a_doc,  (gchar*)a_node_path,
				 (xmlChar*)cur_nv_pair->name->str,
				 (xmlChar*)cur_nv_pair->value->str,
				 TRUE) ;
			}
			if (attr_value) {
				xmlFree (attr_value) ;
				attr_value = NULL ;
			}
		}
	}
	/*now, remove attributes that are to be removed*/
	attr_to_remove = NULL ;
	for (cur_attr = node->properties ;
	        cur_attr ;
	        cur_attr = cur_attr->next) {
		if (attr_to_remove) {
			mlview_xml_document_remove_attribute
			(a_doc, node,
			 attr_to_remove->name,
			 TRUE) ;
			attr_to_remove = NULL ;
		}
		if (!cur_attr->name)
			continue ;
		cur_nv_pair =
		    mlview_utils_name_value_pair_list_lookup
		    (a_nv_pair_list, (gchar*)cur_attr->name) ;
		if (!cur_nv_pair) {
			attr_to_remove = cur_attr ;
		}
	}
	if (attr_to_remove) {
		mlview_xml_document_remove_attribute
		(a_doc, node, attr_to_remove->name, TRUE) ;
		attr_to_remove = NULL ;
	}

	return MLVIEW_OK ;
}

/**
 *Sets the name of the attribute.
 *@param a_this the document to consider.
 *@param a_attr the attribute which name to set.
 *@param a_name the new name of the attribute
 *@param a_emit_signal if TRUE, emits the signals
 *"node-attribute-name-changed" and "node-changed".
 *@return MLVIEW_OK upon successfull completion, an error code
 *otherwise.
 */
enum MlViewStatus
mlview_xml_document_set_attribute_name (MlViewXMLDocument *a_this,
                                        xmlAttr *a_attr,
                                        const xmlChar *a_name,
                                        gboolean a_emit_signal)
{
	xmlNode * node = NULL ;
	xmlNs *ns = NULL ;
	gchar *local_name = NULL ;

	g_return_val_if_fail (a_this
	                      && a_attr
	                      && a_attr->parent
	                      && a_name,
	                      MLVIEW_BAD_PARAM_ERROR) ;

	node = a_attr->parent ;
	g_return_val_if_fail  (node->type == XML_ELEMENT_NODE,
	                       MLVIEW_BAD_PARAM_ERROR) ;
	mlview_utils_parse_full_name (node, (gchar*)a_name,
	                              &ns, &local_name) ;
	if (ns) {
		xmlSetNs ((xmlNode*)a_attr, ns) ;
	}
	xmlNodeSetName ((xmlNode*)a_attr, a_name) ;
	if (a_emit_signal == TRUE) {
		g_signal_emit
		(G_OBJECT (a_this),
		 gv_signals[NODE_ATTRIBUTE_NAME_CHANGED], 0,
		 a_attr) ;
		g_signal_emit
		(G_OBJECT (a_this),
		 gv_signals[NODE_CHANGED], 0,
		 node) ;
		g_signal_emit
		(G_OBJECT (a_this),
		 gv_signals[DOCUMENT_CHANGED], 0);
	}

	if (local_name) {
		g_free (local_name) ;
		local_name = NULL ;
	}
	return MLVIEW_OK ;
}

/**
 *Creates a new namespace on a given xml node.
 *@param a_this the current instance of #MlViewXMLDocument
 *@param a_node the node that holds the namespace
 *@param a_uri the uri part of the namespace.
 *@param a_prefix the prefix of the namespace.
 *@param a_emit_signal if TRUE, emits the signals
 *"node-namespace-added" and "node-changed" upon successful
 *completion.
 *@return a pointer to the newly created uri upon successful
 *completion, a NULL pointer otherwise.
 */
xmlNs *
mlview_xml_document_create_ns (MlViewXMLDocument *a_this,
                               xmlNode *a_node,
                               gchar *a_uri,
                               gchar *a_prefix,
                               gboolean a_emit_signal)
{
	xmlNs *result = NULL ;

	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_XML_DOCUMENT (a_this)
	                      && PRIVATE (a_this)
	                      && a_node, NULL) ;
	if (a_prefix && !strcmp ((char*)a_prefix, ""))
		a_prefix = NULL ;
	result = xmlNewNs (a_node, (xmlChar*)a_uri, (xmlChar*)a_prefix) ;
	THROW_IF_FAIL (result) ;
	result->_private = a_node ;
	if (a_emit_signal == TRUE) {
		g_signal_emit (G_OBJECT (a_this),
		               gv_signals[NODE_NAMESPACE_ADDED], 0,
		               a_node, result) ;
		g_signal_emit (G_OBJECT (a_this),
		               gv_signals[NODE_CHANGED], 0,
		               a_node) ;
		g_signal_emit
		(G_OBJECT (a_this),
		 gv_signals[DOCUMENT_CHANGED], 0);
	}
	return result ;
}


/**
 *Removes a namespace defined on the xml node given
 *in argument.
 *@param a_this the current instance of #MlViewXMLDocument
 *@param a_ns the namespace to remove.
 *@param a_node the xml node that hosts the namespace.
 *@param a_emit_signal if TRUE, emits the signals
 *"node-namespace-removed" and "node-changed".
 *@return MLVIEW_OK upon successfull completion, an error
 *code otherwise.
 */
enum MlViewStatus
mlview_xml_document_remove_ns (MlViewXMLDocument *a_this,
                               xmlNs *a_ns,
                               xmlNode *a_node,
                               gboolean a_emit_signal)
{
	xmlNs *ns = NULL ;

	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_XML_DOCUMENT (a_this)
	                      && PRIVATE (a_this),
	                      MLVIEW_BAD_PARAM_ERROR) ;

	ns = xmlUnlinkNsDef (a_node, a_ns) ;
	if (!ns) {
		/*the ns is not defined on this xml node*/
		return MLVIEW_OK ;
	}
	if (a_emit_signal == TRUE) {
		g_signal_emit (G_OBJECT (a_this),
		               gv_signals[NODE_NAMESPACE_REMOVED],
		               0, a_node, ns) ;
		g_signal_emit (G_OBJECT (a_this),
		               gv_signals[NODE_CHANGED],
		               0, a_node) ;
		g_signal_emit
		(G_OBJECT (a_this),
		 gv_signals[DOCUMENT_CHANGED], 0);
	}
	if (ns) {
		xmlFreeNs (ns) ;
		ns = NULL ;
	}
	return MLVIEW_OK ;
}

/**
 *Sets the prefix/href of a given
 *namespace declaration.
 *@param a_this the current instance of
 *#MlViewXMLDocument
 *@param a_node the xml node that holds the xmlNs to modify.
 *@param a_ns the namespace definition to modify.
 *@param a_uri the uri of the namespace.
 *@param a_prefix the prefix of the namespace.
 *@param a_emit_signal if set to true, emits
 *signals "node-namespace-changed" and "node-changed" 
 *after successfull completion.
 *@return MLVIEW_OK upon successfull completion, an
 *error code otherwise.
 */
enum MlViewStatus
mlview_xml_document_set_ns (MlViewXMLDocument *a_this,
                            xmlNode *a_node,
                            xmlNs *a_ns,
                            xmlChar *a_uri,
                            xmlChar *a_prefix,
                            gboolean a_emit_signal)
{
	xmlNs *cur_ns = NULL ;

	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_XML_DOCUMENT (a_this)
	                      && a_node
	                      && a_ns
	                      && a_uri,
	                      MLVIEW_BAD_PARAM_ERROR) ;

	/*are we sure a_ns is a Ns defined on node a_node ?*/
	for (cur_ns = a_node->nsDef ;
	        cur_ns; cur_ns = cur_ns->next) {
		if (cur_ns == a_ns)
			break ;
	}
	if (cur_ns != a_ns) {
		mlview_utils_trace_debug
		("a_ns is not a namespace defined on node a_node") ;
		return MLVIEW_BAD_PARAM_ERROR ;
	}
	if (a_ns->href) {
		xmlFree ((void*)a_ns->href) ;
	}
	a_ns->href = xmlStrdup (a_uri) ;
	if (a_ns->prefix) {
		xmlFree ((void*)a_ns->prefix) ;
	}
	a_ns->prefix = xmlStrdup (a_prefix) ;
	/*emit the required signals*/
	if (a_emit_signal == TRUE) {
		g_signal_emit (G_OBJECT (a_this),
		               gv_signals[NODE_NAMESPACE_CHANGED],0,
		               a_node, a_ns) ;
		g_signal_emit (G_OBJECT (a_this),
		               gv_signals[NODE_CHANGED], 0,
		               a_node) ;
		g_signal_emit
		(G_OBJECT (a_this),
		 gv_signals[DOCUMENT_CHANGED], 0);
	}
	return MLVIEW_OK ;
}

/**
 *FIXME: comment this
 */
enum MlViewStatus
mlview_xml_document_create_internal_subset (MlViewXMLDocument *a_this,
        const xmlChar * a_name,
        const xmlChar *public_id,
        const xmlChar *system_id,
        gboolean a_emit_signal)
{
	xmlDoc *doc = NULL ;
	xmlDtd *dtd_node = NULL ;

	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_XML_DOCUMENT (a_this),
	                      MLVIEW_OK) ;

	doc = mlview_xml_document_get_native_document (a_this) ;
	THROW_IF_FAIL (doc) ;
	dtd_node = xmlCreateIntSubset (doc, a_name, public_id, system_id) ;
	if (dtd_node) {
		if (a_emit_signal == TRUE) {
			g_signal_emit (G_OBJECT (a_this),
			               gv_signals[DTD_NODE_CREATED],0,
			               dtd_node) ;
			g_signal_emit (G_OBJECT (a_this),
			               gv_signals[NODE_CHANGED], 0,
			               dtd_node) ;
			g_signal_emit
			(G_OBJECT (a_this),
			 gv_signals[DOCUMENT_CHANGED], 0);
		}
	} else {
		return MLVIEW_ERROR ;
	}
	return MLVIEW_OK ;
}

/**
 *Sets the name of a_node to a_name.
 *@param a_this the current xml document.
 *@param a_node the node to set the name on.
 *@param a_name the new name to set.
 *@param a_enc the encoding of "a_name". Better set
 *it to UTF8 and make sure a_name is encoded in utf8
 *@param a_emit_signal if TRUE, emits the signals 
 *"name-changed" and "node-changed" upon succesful completion.
 *@return a_node with it new name or NULL if something bad happened. 
 */
enum MlViewStatus
mlview_xml_document_set_node_name (MlViewXMLDocument *a_this,
                                   const gchar * a_node_path,
                                   gchar * a_name,
                                   gboolean a_emit_signal)
{
	MlViewDocMutation *mutation = NULL ;
	enum MlViewStatus status = MLVIEW_OK ;
	gchar *node_path = NULL ;

	THROW_IF_FAIL (a_this != NULL);
	g_return_val_if_fail (MLVIEW_IS_XML_DOCUMENT
	                      (a_this), MLVIEW_BAD_PARAM_ERROR);
	g_return_val_if_fail (a_node_path != NULL,
	                      MLVIEW_BAD_PARAM_ERROR);

	node_path = g_strdup (a_node_path) ;
	if (!node_path) {
		mlview_utils_trace_debug ("System may be out of memory") ;
		return MLVIEW_ERROR ;
	}
	mutation = mlview_doc_mutation_new (a_this,
	                                    mlview_xml_document_do_mutation_set_node_name,
	                                    mlview_xml_document_undo_mutation_set_node_name,
	                                    "set-node-name") ;
	if (!mutation) {
		mlview_utils_trace_debug ("Could node instanciate mutation") ;
		return MLVIEW_ERROR ;
	}

	g_object_set_data (G_OBJECT (mutation),
	                   "set-node-name::node-path",
	                   node_path) ;
	g_object_set_data (G_OBJECT (mutation),
	                   "set-node-name::name",
	                   a_name) ;
	g_object_set_data (G_OBJECT (mutation),
	                   "set-node-name::emit-signal",
	                   GINT_TO_POINTER (a_emit_signal)) ;

	status = mlview_doc_mutation_do_mutation (mutation, NULL) ;

	if (status == MLVIEW_OK) {
		mlview_xml_document_record_mutation_for_undo (a_this,
		        mutation,
		        TRUE) ;
	}
	return status ;
}

enum MlViewStatus
mlview_xml_document_set_node_name_without_xpath (MlViewXMLDocument *a_this,
        xmlNode *a_node,
        gchar *a_name,
        gboolean a_emit_signal)
{
	THROW_IF_FAIL (a_this);
	g_return_val_if_fail (MLVIEW_IS_XML_DOCUMENT
	                      (a_this), MLVIEW_BAD_PARAM_ERROR);
	THROW_IF_FAIL (a_node != NULL);

	xmlNodeSetName (a_node, (xmlChar*)a_name);

	if (a_emit_signal) {
		g_signal_emit (G_OBJECT (a_this),
		               gv_signals[NAME_CHANGED], 0, a_node);
		g_signal_emit (G_OBJECT (a_this),
		               gv_signals[NODE_CHANGED], 0, a_node);
		g_signal_emit
		(G_OBJECT (a_this),
		 gv_signals[DOCUMENT_CHANGED], 0);
	}
	return MLVIEW_OK ;
}

/**
 *Sets the name of an entity declaration node.
 *@param a_this the xml document hte entity declaration
 *belongs to.
 *@param a_entity the entity declaration node to consider.
 *@param a_dtd_node the dtd subset the entity is attached to.
 *@param a_name the new name of the entity declaration.
 *@param a_emit_signal if TRUE, emits the signals
 *"name-changed", "node-changed", and "document-changed" after
 *successful completion.
 *@return MLVIEW_OK upon successful completion, an error code
 *otherwise.
 */
enum MlViewStatus
mlview_xml_document_set_entity_node_name (MlViewXMLDocument *a_this,
        xmlEntity *a_entity,
        xmlDtd *a_dtd_node,
        gchar *a_name,
        gboolean a_emit_signal)
{
	gint res = 0 ;
	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_XML_DOCUMENT (a_this)
	                      && a_entity
	                      && a_dtd_node
	                      && a_dtd_node->entities,
	                      MLVIEW_BAD_PARAM_ERROR) ;

	res = xmlSetEntityNodeName (a_dtd_node, a_entity, (xmlChar*)a_name) ;
	switch (res) {
	case -1:
		return MLVIEW_BAD_PARAM_ERROR ;
	case 1:
		return MLVIEW_ENTITY_NAME_EXISTS_ERROR ;
	case 0:/*OK*/
		break ;
	default:
		return MLVIEW_ERROR ;
	}
	if (a_emit_signal == TRUE) {
		g_signal_emit (G_OBJECT (a_this),
		               gv_signals[NAME_CHANGED], 0,
		               a_entity);
		g_signal_emit (G_OBJECT (a_this),
		               gv_signals[NODE_CHANGED], 0,
		               a_entity);
		g_signal_emit
		(G_OBJECT (a_this),
		 gv_signals[DOCUMENT_CHANGED], 0);
	}
	return MLVIEW_OK ;
}

/**
 *Sets the system ID of a DTD node. This basically sets the
 *SystemID field of the xmlDtd object.
 *@param a_this the current instance of #MlViewXMLDocument
 *@param a_dtd the dtd node to consider
 *@param a_system_id the new system ID
 *@param a_emit_signal if set to TRUE, emits 
 *the "dtd-node-system-id-changed"
 *,"node-changed" and "document-changed" signals.
 *@return MLVIEW_OK upon successful completion, 
 *an error code otherwise.
 */
enum MlViewStatus
mlview_xml_document_set_dtd_node_system_id (MlViewXMLDocument *a_this,
        xmlDtd *a_dtd,
        xmlChar *a_system_id,
        gboolean a_emit_signal)
{
	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_XML_DOCUMENT (a_this)
	                      && PRIVATE (a_this)
	                      && a_dtd,
	                      MLVIEW_BAD_PARAM_ERROR) ;
	if (a_dtd->SystemID) {
		xmlFree ((void*)a_dtd->SystemID);
		a_dtd->SystemID = NULL ;
	}
	if (a_system_id) {
		a_dtd->SystemID = xmlStrdup (a_system_id) ;
	}

	if (a_emit_signal == TRUE) {
		g_signal_emit
		(G_OBJECT (a_this),
		 gv_signals[DTD_NODE_SYSTEM_ID_CHANGED],
		 0, a_dtd);
		g_signal_emit (G_OBJECT (a_this),
		               gv_signals[NODE_CHANGED], 0, a_dtd) ;
		g_signal_emit
		(G_OBJECT (a_this),
		 gv_signals[DOCUMENT_CHANGED], 0);
	}
	return MLVIEW_OK ;
}

/**
 *Sets the public ID of a DTD node. This basically sets the
 *ExternalID field of the xmlDtd object.
 *@param a_this the current instance of #MlViewXMLDocument
 *@param a_dtd the dtd node to consider
 *@param a_public_id the new public ID
 *@param a_emit_signal if set to TRUE, sents the "dtd-node-public-id-changed"
 *,"node-changed" and "document-changed" signals.
 *@return MLVIEW_OK upon successful completion, 
 *an error code otherwise.
 */
enum MlViewStatus
mlview_xml_document_set_dtd_node_public_id (MlViewXMLDocument *a_this,
        xmlDtd *a_dtd,
        xmlChar *a_public_id,
        gboolean a_emit_signal)
{
	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_XML_DOCUMENT (a_this)
	                      && PRIVATE (a_this)
	                      && a_dtd,
	                      MLVIEW_BAD_PARAM_ERROR) ;

	if (a_dtd->ExternalID) {
		xmlFree ((void*)a_dtd->ExternalID);
		a_dtd->ExternalID = NULL ;
	}
	if (a_public_id) {
		a_dtd->ExternalID = xmlStrdup (a_public_id) ;
	}

	if (a_emit_signal == TRUE) {
		g_signal_emit
		(G_OBJECT (a_this),
		 gv_signals[DTD_NODE_PUBLIC_ID_CHANGED],
		 0, a_dtd);
		g_signal_emit (G_OBJECT (a_this),
		               gv_signals[NODE_CHANGED], 0, a_dtd);
		g_signal_emit
		(G_OBJECT (a_this),
		 gv_signals[DOCUMENT_CHANGED], 0);
	}
	return MLVIEW_OK ;
}

/**
 *Sets the content of an entity node.
 *@param a_this the xml document that contains the
 *entity node.
 *@param a_entity the entity node to consider.
 *@param a_content the new content to set.
 *@param a_emit_signal if TRUE, this function will emit
 *the folowin signals after completion: entity-node-content-changed,
 *node-changed, document-changed.
 *@return MLVIEW_OK upon successful completion, 
 *an error code otherwise.
 */
enum MlViewStatus
mlview_xml_document_set_entity_content (MlViewXMLDocument *a_this,
                                        xmlEntity *a_entity,
                                        xmlChar *a_content,
                                        gboolean a_emit_signal)
{
	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_XML_DOCUMENT (a_this)
	                      && a_entity,
	                      MLVIEW_BAD_PARAM_ERROR) ;

	if (a_entity->content) {
		xmlFree (a_entity->content) ;
		a_entity->content = NULL ;
	}
	if (a_content)
		a_entity->content = xmlStrdup (a_content) ;

	if (a_emit_signal == TRUE) {
		g_signal_emit
		(G_OBJECT (a_this),
		 gv_signals[ENTITY_NODE_CONTENT_CHANGED],
		 0, a_entity);
		g_signal_emit (G_OBJECT (a_this),
		               gv_signals[NODE_CHANGED], 0,
		               a_entity);
		g_signal_emit
		(G_OBJECT (a_this),
		 gv_signals[DOCUMENT_CHANGED], 0);
	}
	return MLVIEW_OK ;
}

/**
 *sets the external id of an xml entity node.
 *@param a_this the xml document that holds the xml entity node.
 *@param a_entity the entity node to consider.
 *@param a_emit_signal if TRUE, emits the following signals after
 *successful completion:entity-node-content-changed, node-changed,
 *document-changed.
 *@return MLVIEW_OK upon successful completion, an error code otherwise.
 */
enum MlViewStatus
mlview_xml_document_set_entity_public_id (MlViewXMLDocument *a_this,
        xmlEntity *a_entity,
        xmlChar *a_external_id,
        gboolean a_emit_signal)
{

	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_XML_DOCUMENT (a_this)
	                      && a_entity,
	                      MLVIEW_BAD_PARAM_ERROR) ;

	if (a_entity->ExternalID) {
		xmlFree ((void*)a_entity->ExternalID) ;
		a_entity->ExternalID = NULL ;
	}
	if (a_external_id)
		a_entity->ExternalID = xmlStrdup (a_external_id) ;
	if (a_emit_signal == TRUE) {
		g_signal_emit
		(G_OBJECT (a_this),
		 gv_signals[ENTITY_NODE_PUBLIC_ID_CHANGED],
		 0, a_entity);
		g_signal_emit (G_OBJECT (a_this),
		               gv_signals[NODE_CHANGED], 0,
		               a_entity);
		g_signal_emit
		(G_OBJECT (a_this),
		 gv_signals[DOCUMENT_CHANGED], 0);
	}
	return MLVIEW_OK ;
}

/**
 *Sets the system id of an xml entity node.
 *@param a_this the xml document that holds the entity node.
 *@param a_entity the entity node to consider.
 *@param a_system_id the new system to set.
 *@return MLVIEW_OK upon succesful completion, an error code
 *otherwise.
 */
enum MlViewStatus
mlview_xml_document_set_entity_system_id (MlViewXMLDocument *a_this,
        xmlEntity *a_entity,
        xmlChar *a_system_id,
        gboolean a_emit_signal)
{
	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_XML_DOCUMENT (a_this)
	                      && a_entity,
	                      MLVIEW_BAD_PARAM_ERROR) ;

	if (a_entity->SystemID) {
		xmlFree ((void*)a_entity->SystemID) ;
		a_entity->SystemID = NULL ;
	}
	if (a_system_id)
		a_entity->SystemID = xmlStrdup (a_system_id) ;

	if (a_emit_signal == TRUE) {
		g_signal_emit
		(G_OBJECT (a_this),
		 gv_signals[ENTITY_NODE_SYSTEM_ID_CHANGED],
		 0, a_entity);
		g_signal_emit (G_OBJECT (a_this),
		               gv_signals[NODE_CHANGED], 0,
		               a_entity);
		g_signal_emit
		(G_OBJECT (a_this),
		 gv_signals[DOCUMENT_CHANGED], 0);
	}

	return MLVIEW_OK ;
}

/**
 *Gets the node name of a given XML node.
 *@param a_node the XML node to get the name from.
 *@param a_enc the encoding in with the name should be converted
 *to. (Note that xmlNode's name is natively stored in utf8).
 *@param a_outbuf out parameter. A pointer to where the node name will
 *be stored. *a_outbut must be freed by the user.
 *@return MLVIEW_OK upon successfull completion, an error code otherwise.
 */
enum MlViewStatus
mlview_xml_document_node_get_name (xmlNode * a_node,
                                   enum MlViewEncoding a_enc,
                                   gchar ** a_outbuf)
{
	enum MlViewStatus status = MLVIEW_OK;

	g_return_val_if_fail (a_node != NULL
	                      && (a_node->type ==
	                          XML_ELEMENT_NODE
	                          || a_node->type ==
	                          XML_DOCUMENT_NODE
	                          || a_node->type == XML_PI_NODE)
	                      && a_outbuf != NULL,
	                      MLVIEW_BAD_PARAM_ERROR);

	if (a_node->name == NULL) {
		*a_outbuf = 0;
		return MLVIEW_OK;
	}

	if (a_enc == ISO8859_1) {
		status = mlview_utils_utf8_str_to_isolat1
		         ((gchar *) a_node->name, a_outbuf);
		if (status != MLVIEW_OK)
			return status;
		status = MLVIEW_OK;
	} else if (a_enc == UTF8) {
		*a_outbuf = (gchar*)g_strdup ((char*)a_node->name);
		status = MLVIEW_OK;
	} else {
		status = MLVIEW_UNKNOWN_ENCODING_ERROR;
	}
	return status;
}

/**
 * Returns the mime type associated to the current document.
 * @param a_this the current instance of #MlViewXMLDocument.
 * @return the mime type. Must *NOT* be freed by the caller.
 */
const gchar *
mlview_xml_document_get_mime_type (MlViewXMLDocument *a_this)
{
	g_return_val_if_fail (a_this && MLVIEW_IS_XML_DOCUMENT (a_this)
	                      && PRIVATE (a_this),
	                      NULL) ;

	if (!PRIVATE (a_this)->mime_type) {
		if (PRIVATE (a_this)->file_desc) {
			PRIVATE (a_this)->mime_type =
			    mlview_file_descriptor_get_mime_type
			    (PRIVATE (a_this)->file_desc) ;
			if (!PRIVATE (a_this)->mime_type) {
				mlview_utils_trace_debug ("get mime type failed") ;
				return NULL ;
			}

		} else {
			PRIVATE (a_this)->mime_type = g_strdup ("text/xml") ;
			;
		}
	}
	return PRIVATE (a_this)->mime_type ;
}

/**
 *Gets the iso latin 1 string length of the fully qualified name of an
 *xml node.
 *@param a_node the XML node to consider.
 *@param a_len out parameter. A pointer to where to store the length.
 *a_len must be allocated by the caller.
 *@return MLVIEW_OK upon successfull completion, an error code otherwise.
 */
enum MlViewStatus
mlview_xml_document_node_get_fqn_len_as_isolat1 (xmlNode *a_node,
        gint * a_len)
{
	enum MlViewStatus status = MLVIEW_OK;
	gchar *fqn = NULL;
	gint len = 0;

	g_return_val_if_fail (a_node && a_len,
	                      MLVIEW_BAD_PARAM_ERROR);

	if (a_node->ns
	        && a_node->ns->prefix
	        && mlview_utils_is_white_string
	        ((gchar*)a_node->ns->prefix) == FALSE) {

		fqn = g_strconcat ((gchar*)a_node->ns->prefix,
		                   ":", (gchar*)a_node->name, NULL);
	} else {
		fqn = g_strdup ((gchar*)a_node->name);
	}

	if (fqn == NULL || *fqn == 0) {
		len = 0;
		status = MLVIEW_OK;

	} else {
		status = mlview_utils_utf8_str_len_as_isolat1
		         ((gchar*)fqn, &len);
	}

	if (status == MLVIEW_OK)
		*a_len = len;

	if (fqn) {
		g_free (fqn);
		fqn = NULL;
	}

	return status;
}

/**
 *Gets the fully qualified name of a given xml Node.
 *@param a_node the XML node to consider.
 *@param a_enc the encoding in which the name is to be translated.
 *@param a_outbuf out parameter. A pointer to where the name is gonna
 *bo store. *a_outbut is to be freed by caller.
 *@return MLVIEW_OK upon successfull completion, an error code otherwise.
 */
enum MlViewStatus
mlview_xml_document_node_get_fqn (xmlNode * a_node,
                                  enum MlViewEncoding a_enc,
                                  gchar ** a_outbuf)
{
	enum MlViewStatus status = MLVIEW_OK;
	gchar *fqn = NULL;

	g_return_val_if_fail (a_node != NULL
	                      && a_node->type == XML_ELEMENT_NODE
	                      && a_outbuf != NULL,
	                      MLVIEW_BAD_PARAM_ERROR);
	if (a_node->name == NULL) {
		*a_outbuf = 0;
		return MLVIEW_OK;
	}
	if (a_node->ns
	        && a_node->ns->prefix
	        && mlview_utils_is_white_string
	        ((gchar*)a_node->ns->prefix) == FALSE) {
		fqn = g_strconcat ((gchar*)a_node->ns->prefix,
		                   ":", (gchar*)a_node->name, NULL);
	} else {
		fqn = g_strdup ((gchar*)a_node->name);
	}
	if (a_enc == ISO8859_1) {
		status = mlview_utils_utf8_str_to_isolat1 ((gchar*)fqn,
		         a_outbuf);
		if (status != MLVIEW_OK) {
			goto release_resources;
		}
	} else if (a_enc == UTF8) {
		*a_outbuf = (gchar*)g_strdup ((gchar*)fqn);
	} else {
		status = MLVIEW_UNKNOWN_ENCODING_ERROR;
		goto release_resources;
	}

release_resources:
	if (fqn) {
		g_free (fqn);
		fqn = NULL;
	}

	return status;
}

/**
 *Gets the content of a given xml node.
 *@param a_node the xml node to consider.
 *@param a_enc the encoding in which the content is to be translated to.
 *@param a_outbuf out parameter. A pointer to where the content must be
 *copied to. *a_outbuf must be freeed by caller.
 *@return MLVIEW_OK upon successful completion.
 */
enum MlViewStatus
mlview_xml_document_node_get_content (xmlNode * a_node,
                                      enum MlViewEncoding a_enc,
                                      gchar ** a_outbuf)
{
	gchar *utf8_content = NULL;
	enum MlViewStatus status = MLVIEW_OK;

	g_return_val_if_fail (a_node != NULL
	                      && a_outbuf != NULL,
	                      MLVIEW_BAD_PARAM_ERROR);

	utf8_content = (gchar*)xmlNodeGetContent (a_node);

	if (utf8_content == NULL) {
		*a_outbuf = 0;
		return MLVIEW_OK;
	}

	if (a_enc == ISO8859_1) {
		status = mlview_utils_utf8_str_to_isolat1
		         ((gchar*)utf8_content, a_outbuf);
		if (status != MLVIEW_OK) {
			goto release_resources;
		}

		status = MLVIEW_OK;

	} else if (a_enc == UTF8) {
		*a_outbuf = (gchar*)g_strdup (utf8_content);
		status = MLVIEW_OK;
	} else {
		status = MLVIEW_UNKNOWN_ENCODING_ERROR;
		goto release_resources;
	}

release_resources:
	if (utf8_content) {
		g_free (utf8_content);
		utf8_content = NULL;
	}

	return status;
}




/**
 *Pastes a node (coming from the internal clipboard) as a child of
 *a given xml node.
 *@param the current xml document to consider.
 *@param a_parent_node the parent node of the xml node to paste.
 *@param a_emit_signal if TRUE emits the appropriate signals upon successful
 *completion. Actually emits the same signals as mlview_xml_document_add_child_node().
 */
void
mlview_xml_document_paste_node_as_child (MlViewXMLDocument *a_this,
        const gchar * a_parent_node_path,
        gboolean a_emit_signal)
{
	xmlNode *xml_node = NULL;

	THROW_IF_FAIL (a_this != NULL);
	THROW_IF_FAIL (MLVIEW_IS_XML_DOCUMENT
	               (a_this));
	THROW_IF_FAIL (PRIVATE (a_this));
	THROW_IF_FAIL (a_parent_node_path != NULL);

	xml_node =
	    mlview_xml_document_get_node_from_clipboard2
	    (PRIVATE (a_this)->native_doc);
	THROW_IF_FAIL (xml_node != NULL);
	mlview_xml_document_add_child_node (a_this,
	                                    a_parent_node_path,
	                                    xml_node, FALSE,
	                                    a_emit_signal);
}

/**
 *Pastes a node (coming from the internal clipboard) as a sibling of
 *a given xml node.
 *@param the current xml document to consider.
 *@param a_parent_node the parent node of the xml node to paste.
 *@param a_emit_signal if TRUE emits the appropriate signals upon successful
 *completion. Actually emits the same signals as 
 *mlview_xml_document_insert_next_sibling_node() or
 *mlview_xml_document_insert_prev_sibling_node() .
 */
void
mlview_xml_document_paste_node_as_sibling (MlViewXMLDocument *a_this,
        const gchar *a_parent_node_path,
        const gchar *a_sibling_node_path,
        gboolean a_previous,
        gboolean a_emit_signal)
{
	xmlNode *xml_node = NULL;

	THROW_IF_FAIL (a_this != NULL);
	THROW_IF_FAIL (MLVIEW_IS_XML_DOCUMENT
	               (a_this));
	THROW_IF_FAIL (PRIVATE (a_this) != NULL);
	THROW_IF_FAIL (a_parent_node_path != NULL);
	THROW_IF_FAIL (a_sibling_node_path != NULL);

	xml_node =
	    mlview_xml_document_get_node_from_clipboard2 (PRIVATE (a_this)->native_doc);

	THROW_IF_FAIL (xml_node != NULL);

	if (a_previous == FALSE) {

		mlview_xml_document_insert_next_sibling_node
		(a_this, a_sibling_node_path,
		 xml_node, FALSE, a_emit_signal);
	} else {
		mlview_xml_document_insert_prev_sibling_node
		(a_this , a_sibling_node_path,
		 xml_node, FALSE, a_emit_signal);
	}
}


/**
 *Tests if the document needs to be saved,
 *(has been modified since the last save) or not.
 *@param a_doc the xml document to consider.
 *@return TRUE if the document needs to be saved, FALSE otherwise.
 */
gboolean
mlview_xml_document_needs_saving (MlViewXMLDocument * a_doc)
{
	MlViewFileDescriptor *file_desc;
	gboolean is_modified;

	THROW_IF_FAIL (a_doc != NULL);
	THROW_IF_FAIL (PRIVATE (a_doc) != NULL);

	file_desc =
	    mlview_xml_document_get_file_descriptor (a_doc);
	if (file_desc == NULL)
		return TRUE;

	if (mlview_file_descriptor_is_modified
	        (file_desc, &is_modified))
		return FALSE;
	return is_modified;
}


/**
 *Increments the reference count associated to this instance.
 *To decrement that reference count, please use
 *mlview_xml_document_unref().
 *
 *@param a_this the "this pointer".
 */
void
mlview_xml_document_ref (MlViewXMLDocument * a_this)
{
	THROW_IF_FAIL (a_this
	               && MLVIEW_IS_XML_DOCUMENT (a_this)
	               && PRIVATE (a_this));

	g_object_ref (G_OBJECT (a_this));
}


/**
 *Decrements the reference count associated to the current
 *instance of #MlViewXMLDocument. If the reference count reaches
 *zero, the current instance of #MlViewXMLDocument is destroyed.
 *
 *@param a_this in out parameter. Points to the "this" pointer.
 *If the ref count reached zero and the current instance has been
 *destroyed, *a_this is set to NULL.
 */
void
mlview_xml_document_unref (MlViewXMLDocument * a_this)
{
	THROW_IF_FAIL (a_this
	               && MLVIEW_IS_XML_DOCUMENT (a_this)
	               && PRIVATE (a_this));

	g_object_unref (G_OBJECT (a_this));
}


/**
 *Gets the file descriptor asscociated to the current instance
 *of #MlViewXMLDocument.
 *
 *@param a_this the "this pointer".
 *@return the file descriptor associated to the document.
 */
MlViewFileDescriptor *
mlview_xml_document_get_file_descriptor (MlViewXMLDocument *a_this)
{
	THROW_IF_FAIL (a_this != NULL);
	THROW_IF_FAIL (PRIVATE (a_this) != NULL);

	return PRIVATE (a_this)->file_desc;
}

/**
 *Sets the file descriptor associated to the document.
 *@param a_this the this pointer.
 *@param a_file_desc the new file descriptor to set.
 */
void
mlview_xml_document_set_file_descriptor (MlViewXMLDocument *a_this,
        MlViewFileDescriptor *a_file_desc)
{
	THROW_IF_FAIL (a_this != NULL);
	THROW_IF_FAIL (PRIVATE (a_this) != NULL);

	PRIVATE (a_this)->file_desc = a_file_desc;
}

/**
 *Gets the native underlaying libxml2 document.
 *@param a_this the "this pointer".
 *@return the underlying native libxml2 document.
 */
xmlDocPtr
mlview_xml_document_get_native_document (const MlViewXMLDocument *a_this)
{
	THROW_IF_FAIL (a_this != NULL);
	THROW_IF_FAIL (PRIVATE (a_this) != NULL);

	return PRIVATE (a_this)->native_doc;
}

/**
 *Searches a string in the xml document tree.
 *The search is done on all the nodes of the tree that are
 *located between a_from and the end of the document.
 *Whenever a node that contains the researched string is found,
 *this method put the found node in *a_found and emits the signal
 *"searched-node-found".
 *@param a_this the current instance of #MlViewXMLDocument.
 *@param a_conf the configuration of what to search and where.
 *@param a_from the node to start the search from. If NULL, 
 *start the search from the document root node.
 *@param a_found out parameter. The node found, or NULL if nothing
 *has been found.
 *@param a_emit_signal if set to TRUE, this function emits the
 *"searched-node-found" signal if the node has been found.
 *@return MLVIEW_OK upon successful completion, an error code
 *otherwise.
 */
enum MlViewStatus
mlview_xml_document_search (MlViewXMLDocument *a_this,
                            const struct SearchConfig *a_conf,
                            xmlNode *a_from,
                            xmlNode **a_found,
                            gboolean a_emit_signal)
{
	xmlNode *xml_node = NULL ;
	GList *start_list = NULL, *cur = NULL ;
	gboolean node_matches = FALSE ;
	enum MlViewStatus status = MLVIEW_OK ;

	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_XML_DOCUMENT (a_this)
	                      && PRIVATE (a_this)
	                      && PRIVATE (a_this)->native_doc
	                      && a_conf
	                      && a_found, MLVIEW_BAD_PARAM_ERROR) ;
	if (!PRIVATE (a_this)->native_doc->children)
		return MLVIEW_OK ;

	if ((PRIVATE (a_this)->last_modif_sequence
	        < PRIVATE (a_this)->modif_sequence)
	        || !PRIVATE (a_this)->nodes_list)
	{
		if (PRIVATE (a_this)->nodes_list) {
			free_tree_list_cache (a_this) ;
		}
		status = build_tree_list_cache (a_this) ;
		PRIVATE (a_this)->last_modif_sequence =
		    PRIVATE (a_this)->modif_sequence ;
	}
	if (a_from)
	{
		g_return_val_if_fail
		        (a_from->doc == PRIVATE (a_this)->native_doc,
		         MLVIEW_BAD_PARAM_ERROR) ;
		if (a_from->type == XML_DOCUMENT_NODE) {
			start_list  = (GList*)g_hash_table_lookup
			              (PRIVATE (a_this)->nodes_hash,
			               PRIVATE (a_this)->native_doc->children);
		} else {
			start_list = (GList*)g_hash_table_lookup
			             (PRIVATE (a_this)->nodes_hash,
			              a_from) ;
		}
	} else
	{
		start_list = (GList*)g_hash_table_lookup
		             (PRIVATE (a_this)->nodes_hash,
		              PRIVATE (a_this)->native_doc->children) ;
	}
	THROW_IF_FAIL (start_list) ;
	if (a_conf->downward == TRUE)
	{
		start_list = start_list->prev ;
	} else
	{
		start_list = start_list->next ;
	}
	cur = start_list ;
	while (cur)
	{
		xml_node = (xmlNode *)cur->data ;
		node_matches = search_in_node
		               (xml_node, a_conf) ;
		if (node_matches == TRUE) {
			*a_found = xml_node ;
			goto end ;
		}
		if (a_conf->downward == TRUE) {
			cur = cur->prev ;
		} else {
			cur = cur->next ;
		}
	}

end:
	if (a_emit_signal == TRUE && *a_found)
	{
		g_signal_emit (G_OBJECT (a_this),
		               gv_signals[SEARCHED_NODE_FOUND], 0,
		               *a_found) ;
	}
	return MLVIEW_OK ;
}

/* In development. TODO: finish this
enum MlViewStatus
mlview_xml_document_replace_string (MlViewXMLDocument *a_this,
				    struct SearchConfig *a_config,
				    xmlNode *a_node,
				    const gchar *a_string_to_replace,
				    const gchar *a_replacement,
				    gboolean a_emit_signal)
{
}
*/

/**
 *Determines if completion can be possible on some valid elements
 *of the documents (if validation is ON and if the document has an
 *associated DTD, currently).
 *@param a_this the current instance of #MlViewXMLDocument.
 *@return TRUE if completion is possible for valid elements in this document, or FALSE.
 */
gboolean
mlview_xml_document_is_completion_possible_global (MlViewXMLDocument *a_this)
{
	THROW_IF_FAIL (a_this && MLVIEW_IS_XML_DOCUMENT (a_this));
	THROW_IF_FAIL (PRIVATE (a_this));

    mlview::PrefsCategoryGeneral *prefs =
	    dynamic_cast<mlview::PrefsCategoryGeneral*> (
		mlview::Preferences::get_instance ()
		->get_category_by_id (mlview::PrefsCategoryGeneral::CATEGORY_ID));
    THROW_IF_FAIL (prefs);

	if (prefs->use_validation ()
		&& PRIVATE (a_this)->native_doc->extSubset)
		return TRUE;
	else
		return FALSE;
}

/**
 *Determines if a node is valid according to the DTD (if validation is ON,
 *if the document has an associated DTD and if the node is valid).
 *@param a_this the current instance of #MlViewXMLDocument.
 *@param a_node the current node.
 *@return TRUE if the specified node is valid, or FALSE.
 */
gboolean
mlview_xml_document_is_node_valid (MlViewXMLDocument *a_this,
                                   xmlNode *a_node)
{
	xmlValidCtxt vctxt;
	xmlDoc *xml_doc = NULL;

	THROW_IF_FAIL (a_this && MLVIEW_IS_XML_DOCUMENT (a_this));
	THROW_IF_FAIL (a_node);

    mlview::PrefsCategoryGeneral *prefs =
	    dynamic_cast<mlview::PrefsCategoryGeneral*> (
		mlview::Preferences::get_instance ()
		->get_category_by_id (mlview::PrefsCategoryGeneral::CATEGORY_ID));
    THROW_IF_FAIL (prefs);

	memset (&vctxt, 0, sizeof (xmlValidCtxt));
	xml_doc = mlview_xml_document_get_native_document (a_this);

	THROW_IF_FAIL (xml_doc);

	if (prefs->use_validation () &&
	        xml_doc->extSubset &&
	        xmlValidateOneElement (&vctxt,
	                               xml_doc,
	                               a_node))
		return TRUE;
	else
		return FALSE;
}

MlViewSchemaList *
mlview_xml_document_get_schema_list (MlViewXMLDocument *a_doc)
{
	THROW_IF_FAIL (a_doc && MLVIEW_IS_XML_DOCUMENT (a_doc));
	THROW_IF_FAIL (PRIVATE (a_doc) && PRIVATE (a_doc)->schemas);

	return PRIVATE (a_doc)->schemas;
}


/*
 * In the process of being removed
 *
enum MlViewStatus
mlview_xml_document_replace_node (MlViewXMLDocument *a_this,
                                  xmlNode *a_node,
                                  xmlNode *a_replacement,
                                  gboolean a_emit_signal)
{
        g_return_val_if_fail (a_this 
                              && MLVIEW_IS_XML_DOCUMENT (a_this)
                              && PRIVATE (a_this)
                              && a_node && a_replacement,
                              MLVIEW_BAD_PARAM_ERROR) ;
 
        g_return_val_if_fail (PRIVATE (a_this)->native_doc == a_node->doc,
                              MLVIEW_BAD_PARAM_ERROR) ;
 
	if (a_emit_signal == TRUE) {
		g_signal_emit (G_OBJECT (a_this),
				gv_signals[REPLACE_NODE], 0, 
				a_node, a_replacement) ;
	}
 
        if (!xmlReplaceNode (a_node, a_replacement)) {
                return MLVIEW_ERROR ;
        }
 
        if (a_emit_signal == TRUE) {
		g_signal_emit (G_OBJECT (a_this),
				gv_signals[NODE_CHANGED], 0,
				a_replacement) ;
                g_signal_emit (G_OBJECT (a_this),
                               gv_signals[DOCUMENT_CHANGED], 0) ;
        }
        return MLVIEW_OK ;
}
*/

enum MlViewStatus
mlview_xml_document_replace_node (MlViewXMLDocument *a_this,
                                  gchar *a_node_path,
                                  xmlNode *a_replacement,
                                  gboolean a_emit_signal)
{
	MlViewDocMutation *mutation = NULL ;
	enum MlViewStatus status = MLVIEW_OK ;
	gchar *node_path = NULL, *serialized_replacing_node = NULL ;

	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_XML_DOCUMENT (a_this)
	                      && PRIVATE (a_this)
	                      && a_node_path && a_replacement,
	                      MLVIEW_BAD_PARAM_ERROR) ;


	mutation = mlview_doc_mutation_new
	           (a_this,
	            mlview_xml_document_do_mutation_replace_node,
	            mlview_xml_document_undo_mutation_replace_node,
	            "replace-node") ;

	if (!mutation) {
		mlview_utils_trace_debug ("Could not instanciate mutation") ;
		return MLVIEW_ERROR ;
	}
	mlview_parsing_utils_serialize_node_to_buf (a_replacement,
	        &serialized_replacing_node) ;
	if (!serialized_replacing_node) {
		mlview_utils_trace_debug ("Could not serialize the node") ;
		return MLVIEW_ERROR ;
	}

	node_path = g_strdup (a_node_path) ;
	g_object_set_data (G_OBJECT (mutation),
	                   "replace-node::node-path",
	                   node_path) ;
	g_object_set_data (G_OBJECT (mutation),
	                   "replace-node::serialized-replacing-node",
	                   serialized_replacing_node) ;
	g_object_set_data (G_OBJECT (mutation),
	                   "replace-node::emit-signal",
	                   GINT_TO_POINTER (a_emit_signal)) ;

	status = mlview_doc_mutation_do_mutation (mutation, NULL) ;
	if (status == MLVIEW_OK) {
		mlview_xml_document_record_mutation_for_undo (a_this,
		        mutation,
		        TRUE) ;
	}
	return status ;

}

enum MlViewStatus
mlview_xml_document_comment_node (MlViewXMLDocument *a_this,
                                  const gchar *a_node_path,
                                  gboolean a_emit_signal)
{
	MlViewDocMutation *mutation = NULL ;
	enum MlViewStatus status = MLVIEW_OK ;
	gchar *node_path = NULL ;

	g_return_val_if_fail (a_this && MLVIEW_IS_XML_DOCUMENT (a_this),
	                      MLVIEW_BAD_PARAM_ERROR) ;
	THROW_IF_FAIL (a_node_path) ;

	node_path = g_strdup (a_node_path)  ;
	if (!node_path) {
		mlview_utils_trace_debug ("System may be out of memory") ;
		return MLVIEW_ERROR ;
	}

	mutation = mlview_doc_mutation_new (a_this,
	                                    mlview_xml_document_do_mutation_comment_node,
	                                    mlview_xml_document_undo_mutation_comment_node,
	                                    "comment-node") ;
	if (!mutation) {
		mlview_utils_trace_debug ("Could not instanciate mutation") ;
		return MLVIEW_ERROR ;
	}

	g_object_set_data (G_OBJECT (mutation),
	                   "comment-node::node-path",
	                   node_path) ;
	g_object_set_data (G_OBJECT (mutation),
	                   "comment-node::emit-signal",
	                   GINT_TO_POINTER (a_emit_signal)) ;
	status = mlview_doc_mutation_do_mutation (mutation, NULL) ;

	if (status == MLVIEW_OK) {
		mlview_xml_document_record_mutation_for_undo (a_this,
		        mutation,
		        TRUE) ;
	}

	return status ;
}




enum MlViewStatus
mlview_xml_document_uncomment_node (MlViewXMLDocument *a_this,
                                    const gchar *a_node_path,
                                    gboolean a_emit_signal)
{
	MlViewDocMutation *mutation = NULL ;
	enum MlViewStatus status = MLVIEW_OK ;
	gchar *node_path = NULL ;

	g_return_val_if_fail (a_this && MLVIEW_IS_XML_DOCUMENT (a_this),
	                      MLVIEW_BAD_PARAM_ERROR) ;
	THROW_IF_FAIL (a_node_path) ;

	node_path = g_strdup (a_node_path)  ;
	if (!node_path) {
		mlview_utils_trace_debug ("System may be out of memory") ;
		return MLVIEW_ERROR ;
	}

	mutation = mlview_doc_mutation_new (a_this,
	                                    mlview_xml_document_do_mutation_uncomment_node,
	                                    mlview_xml_document_undo_mutation_uncomment_node,
	                                    "comment-node-name") ;
	if (!mutation) {
		mlview_utils_trace_debug ("Could not instanciate mutation") ;
		return MLVIEW_ERROR ;
	}

	g_object_set_data (G_OBJECT (mutation),
	                   "uncomment-node::node-path",
	                   node_path) ;
	g_object_set_data (G_OBJECT (mutation),
	                   "uncomment-node::emit-signal",
	                   GINT_TO_POINTER (a_emit_signal)) ;
	status = mlview_doc_mutation_do_mutation (mutation, NULL) ;

	if (status == MLVIEW_OK) {
		mlview_xml_document_record_mutation_for_undo (a_this,
		        mutation,
		        TRUE) ;
	}

	return status ;
}


gchar *
mlview_xml_document_get_uri (MlViewXMLDocument *a_this)
{
	MlViewFileDescriptor *fd = NULL;

	THROW_IF_FAIL (a_this);
	THROW_IF_FAIL (MLVIEW_IS_XML_DOCUMENT (a_this));

	fd = mlview_xml_document_get_file_descriptor (a_this);

	THROW_IF_FAIL (fd);

	return mlview_file_descriptor_get_file_path (fd);
}

gboolean
mlview_xml_document_can_undo_mutation (MlViewXMLDocument *a_this)
{
	guint undo_stack_size = 0 ;
	g_return_val_if_fail (a_this && MLVIEW_IS_XML_DOCUMENT (a_this)
	                      && PRIVATE (a_this), FALSE) ;

	if (!PRIVATE (a_this)->undo_stack) {
		return FALSE ;
	}
	if (mlview_doc_mutation_stack_get_size (PRIVATE (a_this)->undo_stack,
	                                        &undo_stack_size)
	        !=  MLVIEW_OK) {
		return FALSE ;
	}
	if (undo_stack_size)
		return TRUE ;
	return FALSE ;
}

gboolean
mlview_xml_document_can_redo_mutation (MlViewXMLDocument *a_this)
{
	guint redo_stack_size = 0 ;
	g_return_val_if_fail (a_this && MLVIEW_IS_XML_DOCUMENT (a_this)
	                      && PRIVATE (a_this), FALSE) ;

	if (!PRIVATE (a_this)->redo_stack) {
		return FALSE ;
	}
	if (mlview_doc_mutation_stack_get_size (PRIVATE (a_this)->redo_stack,
	                                        &redo_stack_size)
	        !=  MLVIEW_OK) {
		return FALSE ;
	}
	if (redo_stack_size)
		return TRUE ;
	return FALSE ;
}

enum MlViewStatus
mlview_xml_document_undo_mutation (MlViewXMLDocument *a_this,
                                   gpointer a_user_data)
{
	MlViewDocMutation *mutation = NULL ;
	enum MlViewStatus status = MLVIEW_OK ;

	g_return_val_if_fail (a_this && MLVIEW_IS_XML_DOCUMENT (a_this)
	                      && PRIVATE (a_this),
	                      MLVIEW_BAD_PARAM_ERROR) ;

	if (mlview_xml_document_can_undo_mutation (a_this) == FALSE)
		return MLVIEW_CANT_UNDO_ERROR ;

	mlview_doc_mutation_stack_peek (PRIVATE (a_this)->undo_stack,
	                                &mutation) ;
	if (!mutation) {
		mlview_utils_trace_debug ("Could not get mutation") ;
		status = MLVIEW_ERROR ;
	}
	status = mlview_doc_mutation_undo_mutation (mutation, NULL) ;
	mlview_xml_document_record_mutation_for_redo (a_this,
	        mutation) ;
	return status ;
}

enum MlViewStatus
mlview_xml_document_redo_mutation (MlViewXMLDocument *a_this,
                                   gpointer a_user_data)
{
	MlViewDocMutation *mutation = NULL ;
	enum MlViewStatus status = MLVIEW_OK ;

	g_return_val_if_fail (a_this && MLVIEW_IS_XML_DOCUMENT (a_this)
	                      && PRIVATE (a_this),
	                      MLVIEW_BAD_PARAM_ERROR) ;

	if (mlview_xml_document_can_redo_mutation (a_this) == FALSE)
		return MLVIEW_CANT_UNDO_ERROR ;

	mlview_doc_mutation_stack_peek (PRIVATE (a_this)->redo_stack,
	                                &mutation) ;
	if (!mutation) {
		mlview_utils_trace_debug ("Could not get mutation") ;
		status = MLVIEW_ERROR ;
	}
	status = mlview_doc_mutation_do_mutation (mutation, NULL) ;
	if (status != MLVIEW_OK)
		return status ;
	status = mlview_xml_document_record_mutation_for_undo (a_this,
	         mutation,
	         FALSE) ;
	return status ;
}

/**
 *Gets the xpath expression that addresses this @a_node
 *This function is a derivative work of the xmlGetNodePath()
 *It adds proper namespace support to the returned path an updates
 *the cached xmlXPathcontext accordingly.
 *
 */
enum MlViewStatus
mlview_xml_document_get_node_path (MlViewXMLDocument *a_this,
                                   xmlNode *a_node,
                                   gchar **a_path)
{
	xmlNodePtr cur, tmp, next;
	xmlChar *buffer = NULL, *temp= NULL;
	size_t buf_len = 0;
	xmlChar *buf = NULL;
	const char *sep = NULL;
	const char *name = NULL;
	char nametemp[100] = {0};
	int occur = 0;

	g_return_val_if_fail (a_this && MLVIEW_IS_XML_DOCUMENT (a_this)
	                      && PRIVATE (a_this)
	                      && PRIVATE (a_this)->native_doc
	                      && a_node && a_path,
	                      MLVIEW_BAD_PARAM_ERROR) ;

	if (!PRIVATE (a_this)->xpath_context) {
		PRIVATE (a_this)->xpath_context =
		    xmlXPathNewContext (PRIVATE (a_this)->native_doc) ;
	}

	buf_len = 500;
	buffer = (xmlChar *) xmlMallocAtomic(buf_len * sizeof(xmlChar));
	if (buffer == NULL) {
		mlview_utils_trace_debug ("getting node path");
		return MLVIEW_OUT_OF_MEMORY_ERROR;
	}
	buf = (xmlChar *) xmlMallocAtomic(buf_len * sizeof(xmlChar));
	if (buf == NULL) {
		mlview_utils_trace_debug ("getting node path");
		xmlFree(buffer);
		return MLVIEW_OUT_OF_MEMORY_ERROR;
	}

	buffer[0] = 0;
	cur = a_node;
	do {
		name = "";
		sep = "?";
		occur = 0;
		if ((cur->type == XML_DOCUMENT_NODE) ||
		        (cur->type == XML_HTML_DOCUMENT_NODE)) {
			if (buffer[0] == '/')
				break;
			sep = "/";
			next = NULL;
		} else if (cur->type == XML_ELEMENT_NODE) {
			xmlNs * ns = NULL ;
			sep = "/";
			name = (const char *) cur->name;

			if (cur->ns) {
				ns = cur->ns ;
			} else {
				mlview_xml_document_lookup_default_ns (a_this,
				                                       cur, &ns) ;
			}
			if (ns) {
				if (ns->prefix != NULL) {
					g_snprintf (nametemp, sizeof(nametemp) - 1,
					            "%s:%s", ns->prefix, cur->name);
				} else {
					/*
					 *we have hit a default namespace (without prefix)
					 *So lets guess a dummy prefix and register it
					 *to the xpath_context.
					 */
					gchar *dummy_prefix =
					    mlview_xml_document_get_default_ns_invented_prefix
					    (a_this, (gchar*)ns->href, cur) ;
					if (!dummy_prefix) {
						g_snprintf (nametemp, sizeof(nametemp) - 1,
						            "%s", cur->name);
					} else {

						xmlXPathRegisterNs (PRIVATE (a_this)->xpath_context,
						                    (xmlChar*)dummy_prefix,
						                    ns->href) ;
						g_snprintf (nametemp, sizeof(nametemp) - 1,
						            "%s:%s", dummy_prefix, cur->name);
					}
					if (dummy_prefix) {
						g_free (dummy_prefix) ;
						dummy_prefix = NULL ;
					}
				}
				nametemp[sizeof(nametemp) - 1] = 0;
				name = nametemp;
			}
			next = cur->parent;

			/*
			 * Thumbler index computation
			 * TODO: the ocurence test seems bogus for namespaced names
			 */
			tmp = cur->prev;
			while (tmp != NULL) {
				if ((tmp->type == XML_ELEMENT_NODE) &&
				        (xmlStrEqual(cur->name, tmp->name)))
					occur++;
				tmp = tmp->prev;
			}
			if (occur == 0) {
				tmp = cur->next;
				while (tmp != NULL && occur == 0) {
					if ((tmp->type == XML_ELEMENT_NODE) &&
					        (xmlStrEqual(cur->name, tmp->name)))
						occur++;
					tmp = tmp->next;
				}
				if (occur != 0)
					occur = 1;
			} else
				occur++;
		} else if (cur->type == XML_COMMENT_NODE) {
			sep = "/";
			name = "comment()";
			next = cur->parent;

			/*
			 * Thumbler index computation
			 */
			tmp = cur->prev;
			while (tmp != NULL) {
				if (tmp->type == XML_COMMENT_NODE)
					occur++;
				tmp = tmp->prev;
			}
			if (occur == 0) {
				tmp = cur->next;
				while (tmp != NULL && occur == 0) {
					if (tmp->type == XML_COMMENT_NODE)
						occur++;
					tmp = tmp->next;
				}
				if (occur != 0)
					occur = 1;
			} else
				occur++;
		} else if ((cur->type == XML_TEXT_NODE) ||
		           (cur->type == XML_CDATA_SECTION_NODE)) {
			sep = "/";
			name = "text()";
			next = cur->parent;

			/*
			 * Thumbler index computation
			 */
			tmp = cur->prev;
			while (tmp != NULL) {
				if ((tmp->type == XML_TEXT_NODE) ||
				        (tmp->type == XML_CDATA_SECTION_NODE))
					occur++;
				tmp = tmp->prev;
			}
			if (occur == 0) {
				tmp = cur->next;
				while (tmp != NULL && occur == 0) {
					if ((tmp->type == XML_TEXT_NODE) ||
					        (tmp->type == XML_CDATA_SECTION_NODE))
						occur++;
					tmp = tmp->next;
				}
				if (occur != 0)
					occur = 1;
			} else
				occur++;
		} else if (cur->type == XML_PI_NODE) {
			sep = "/";
			g_snprintf (nametemp, sizeof(nametemp) - 1,
			            "processing-instruction('%s')", cur->name);
			nametemp[sizeof(nametemp) - 1] = 0;
			name = nametemp;

			next = cur->parent;

			/*
			 * Thumbler index computation
			 */
			tmp = cur->prev;
			while (tmp != NULL) {
				if ((tmp->type == XML_PI_NODE) &&
				        (xmlStrEqual(cur->name, tmp->name)))
					occur++;
				tmp = tmp->prev;
			}
			if (occur == 0) {
				tmp = cur->next;
				while (tmp != NULL && occur == 0) {
					if ((tmp->type == XML_PI_NODE) &&
					        (xmlStrEqual(cur->name, tmp->name)))
						occur++;
					tmp = tmp->next;
				}
				if (occur != 0)
					occur = 1;
			} else
				occur++;

		} else if (cur->type == XML_ATTRIBUTE_NODE) {
			sep = "/@";
			name = (const char *) (((xmlAttrPtr) cur)->name);
			next = ((xmlAttrPtr) cur)->parent;
		} else {
			next = cur->parent;
		}

		/*
		 * Make sure there is enough room
		 */
		if (xmlStrlen(buffer) + sizeof(nametemp) + 20 > buf_len) {
			buf_len =
			    2 * buf_len + xmlStrlen(buffer) + sizeof(nametemp) + 20;
			temp = (xmlChar *) xmlRealloc(buffer, buf_len);
			if (temp == NULL) {
				mlview_utils_trace_debug ("getting node path");
				xmlFree(buf);
				xmlFree(buffer);
				return MLVIEW_OUT_OF_MEMORY_ERROR;
			}
			buffer = temp;
			temp = (xmlChar *) xmlRealloc(buf, buf_len);
			if (temp == NULL) {
				mlview_utils_trace_debug ("getting node path");
				xmlFree(buf);
				xmlFree(buffer);
				return MLVIEW_OUT_OF_MEMORY_ERROR;
			}
			buf = temp;
		}
		if (occur == 0)
			g_snprintf ((char *) buf, buf_len, "%s%s%s",
			            sep, name, (char *) buffer);
		else
			g_snprintf ((char *) buf, buf_len, "%s%s[%d]%s",
			            sep, name, occur, (char *) buffer);
		g_snprintf ((char *) buffer, buf_len, "%s", buf);
		cur = next;
	} while (cur != NULL);
	xmlFree(buf);
	*a_path = (gchar*)buffer ;
	return MLVIEW_OK ;
}

/**
 *Gets the root element node of the current instance of #MlViewXMLDocument
 *@param a_this the current instance of #MlViewXMLDocument
 *@return MLVIEW_OK upon successful completion, an error code otherwise.
 */
enum MlViewStatus
mlview_xml_document_get_root_element (MlViewXMLDocument *a_this,
                                      xmlNode **a_root_element)
{
	xmlNode *root_elem = NULL ;

	g_return_val_if_fail (a_this && MLVIEW_IS_XML_DOCUMENT (a_this)
	                      && PRIVATE (a_this)
	                      && a_root_element,
	                      MLVIEW_BAD_PARAM_ERROR) ;

	*a_root_element = root_elem ;
	return MLVIEW_OK ;
}

/**
 *Saves an xml file using the application wide settings. 
 *@param a_this the xml document to save.
 *@param a_app_context the current xml app context.
 *@return the number of bytes written or -1 in case of an error. 
 */
gint
mlview_xml_document_save_xml_doc (MlViewXMLDocument * a_this,
                                  const gchar * a_file_path)
{
	xmlDoc *native_doc = NULL ;
	gint nb_bytes_writen = 0 ;

	THROW_IF_FAIL (a_this != NULL);
	THROW_IF_FAIL (a_file_path != NULL);

	g_signal_emit (G_OBJECT (a_this), gv_signals[GOING_TO_SAVE], 0) ;
	native_doc = mlview_xml_document_get_native_document (a_this) ;
	if (!native_doc) {
		return -1 ;
	}
	nb_bytes_writen = xmlSaveFormatFile (a_file_path, native_doc, 1);
	return nb_bytes_writen;
}

void
mlview_xml_document_save_xml_doc2 (MlViewXMLDocument *a_this,
                                   gchar **a_buffer,
                                   gint *a_buffer_len)
{
	xmlDoc *native_doc = NULL ;

	THROW_IF_FAIL (a_this && a_buffer && a_buffer_len) ;

	native_doc = mlview_xml_document_get_native_document (a_this) ;
	if (!native_doc) {
		return  ;
	}

	g_signal_emit (G_OBJECT (a_this), gv_signals[GOING_TO_SAVE], 0) ;
	xmlDocDumpFormatMemory (native_doc, (xmlChar**)a_buffer,
	                        a_buffer_len, 1) ;
}

enum MlViewStatus
mlview_xml_document_reload_from_buffer (MlViewXMLDocument *a_this,
                                        const gchar *a_buffer,
                                        gboolean a_emit_signal)
{
	xmlDoc *native_doc = NULL ;
	gchar *doc_uri = NULL ;

	g_return_val_if_fail (a_this && MLVIEW_IS_XML_DOCUMENT (a_this)
	                      && PRIVATE (a_this),
	                      MLVIEW_BAD_PARAM_ERROR) ;

	native_doc = xmlParseMemory (a_buffer, strlen (a_buffer)) ;
	if (!native_doc) {
		g_warning ("in memory buffer parsing failed") ;
		return MLVIEW_PARSING_ERROR ;
	}

	if (PRIVATE (a_this)->native_doc) {
		xmlFreeDoc (PRIVATE (a_this)->native_doc) ;
		PRIVATE (a_this)->native_doc = NULL ;
	}
	PRIVATE (a_this)->native_doc = native_doc ;
	PRIVATE (a_this)->cur_node = NULL ;

	/*
	 * re set the doc name (= doc uri) that may have been lost during
	 * the xmlParseMemory() call.
	 */
	doc_uri = mlview_xml_document_get_file_path (a_this) ;
	if (doc_uri) {
		xmlNodeSetName ((xmlNode*)native_doc, (xmlChar*)doc_uri) ;
		g_free (doc_uri) ;
		doc_uri = NULL ;
	}
	if (a_emit_signal == TRUE) {
		g_signal_emit (G_OBJECT (a_this),
		               gv_signals[DOCUMENT_RELOADED], 0) ;
		g_signal_emit
		(G_OBJECT (a_this),
		 gv_signals[DOCUMENT_CHANGED], 0);
	}
	return MLVIEW_OK ;
}

/**
 * Make the current instance of #MlViewXMLDocument 
 * emit the "document-undo-state-changed" signal.
 * @param a_this the current instance of #MlViewXMLDocument
 * @return MLVIEW_OK upon succesful completion, an error code otherwise
 */
enum MlViewStatus
mlview_xml_document_notify_undo_state_changed (MlViewXMLDocument *a_this)
{
	g_return_val_if_fail (a_this && MLVIEW_IS_XML_DOCUMENT (a_this)
	                      && PRIVATE (a_this),
	                      MLVIEW_BAD_PARAM_ERROR) ;
	g_signal_emit (G_OBJECT (a_this),
	               gv_signals[DOCUMENT_UNDO_STATE_CHANGED], 0) ;

	return MLVIEW_OK ;
}

/**
 * Check if the current document is standalone
 *
 * @param a_this the current instance of #MlViewXMLDocument
 * @return TRUE if the current document is standalone
 */
gboolean
mlview_xml_document_is_standalone (MlViewXMLDocument *a_this)
{
	g_return_val_if_fail (a_this && PRIVATE (a_this)
	                      && MLVIEW_IS_XML_DOCUMENT (a_this),
	                      MLVIEW_BAD_PARAM_ERROR);

	if (PRIVATE(a_this)->native_doc->standalone == 1)
		return TRUE;

	return FALSE;
}


/**
 * Make the current document standalone
 *
 * @param a_this the current instance of #MlViewXMLDocument
 * @param standalone if the current document should be standalone
 */
void
mlview_xml_document_set_standalone (MlViewXMLDocument *a_this, gboolean standalone)
{
	g_return_if_fail (a_this && PRIVATE (a_this)
	                  && MLVIEW_IS_XML_DOCUMENT (a_this));

	if (standalone == TRUE)
		PRIVATE(a_this)->native_doc->standalone = 1;
	else
		PRIVATE(a_this)->native_doc->standalone = 0;
}
