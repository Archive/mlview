/*
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute 
 *it and/or modify it under the terms of 
 *the GNU General Public License as published by the 
 *Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */

/**
 *@file
 *The declaration of the #MlViewNodeTypePicker class.
 */

#ifndef __MLV_ELTYPE_PICKER_H__
#define __MLV_ELTYPE_PICKER_H__

#include <libxml/tree.h>
#include "mlview-app-context.h"
#include "mlview-parsing-utils.h"

G_BEGIN_DECLS
/*common macros to comply with the GtkObject typing system.*/
#define MLVIEW_TYPE_NODE_TYPE_PICKER (mlview_node_type_picker_get_type())
#define MLVIEW_NODE_TYPE_PICKER(object) (GTK_CHECK_CAST((object),MLVIEW_TYPE_NODE_TYPE_PICKER,MlViewNodeTypePicker))
#define MLVIEW_NODE_TYPE_PICKER_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),MLVIEW_TYPE_NODE_TYPE_PICKER,MlViewNodeTypePickerClass))
#define MLVIEW_IS_NODE_TYPE_PICKER(object) (GTK_CHECK_TYPE((object),MLVIEW_TYPE_NODE_TYPE_PICKER))
#define MLVIEW_IS_NODE_TYPE_PICKER_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass),MLVIEW_TYPE_NODE_TYPE_PICKER))

/*The data structure to define xml node types*/
typedef struct _NodeTypeDefinition NodeTypeDefinition;
struct _NodeTypeDefinition
{
	const char *node_type_name;
	xmlElementType node_type ;
	xmlEntityType entity_type ;
};

typedef struct _MlViewNodeTypePicker
			MlViewNodeTypePicker;
typedef struct _MlViewNodeTypePickerClass
			MlViewNodeTypePickerClass;
typedef struct _MlViewNodeTypePickerPrivate
			MlViewNodeTypePickerPrivate;

/**
 *The MlViewNodeTypePicker class.
 *It's basically a dialog window that lets the user
 *choose the type of element she wants to insert
 *into the xml document. This class also provides methods
 *to get the type of node the user has choosen.
 */
struct _MlViewNodeTypePicker
{
	GtkDialog dialog;
	MlViewNodeTypePickerPrivate *priv;
};

/*The MlViewNodeTypePicker class definition */
struct _MlViewNodeTypePickerClass
{
	GtkDialogClass parent_class;
	/*any signal handler shoud come here */
};

/*public methods */
guint mlview_node_type_picker_get_type (void);

NodeTypeDefinition* mlview_node_type_picker_get_selected_node_type (MlViewNodeTypePicker * a_nt_picker);

gchar *mlview_node_type_picker_get_selected_node_type_name (MlViewNodeTypePicker * a_nt_picker);

gchar *mlview_node_type_picker_get_node_name_or_content (MlViewNodeTypePicker * a_nt_picker);

void mlview_node_type_picker_set_selected_node_type (MlViewNodeTypePicker * a_nt_picker,
        xmlElementType a_node_type,
        xmlEntityType a_entity_type);

void mlview_node_type_picker_set_focus_to_node_name_or_content_entry (MlViewNodeTypePicker * a_nt_picker);

void mlview_node_type_picker_select_node_name_or_content_entry_text (MlViewNodeTypePicker * a_nt_picker);

GtkWidget *mlview_node_type_picker_new ();

GtkWidget *mlview_node_type_picker_new_with_title (gchar*a_title);

void mlview_node_type_picker_set_on_going_validation (gboolean a_on);

gboolean mlview_node_type_picker_on_going_validation_is_on (void);

void mlview_node_type_picker_set_title (MlViewNodeTypePicker * a_nt_picker,
					const gchar * a_title);

void mlview_node_type_picker_build_element_name_choice_list (MlViewNodeTypePicker * a_picker,
							     enum NODE_INSERTION_SCHEME a_insertion_scheme,
							     xmlNode * a_current_xml_node);

G_END_DECLS
#endif                          /*MLVIEW_ELEMENT_TYPE_PICKER */
