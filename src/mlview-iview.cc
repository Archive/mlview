/* -*- Mode: C++; indent-tabs-mode:true ; c-basic-offset: 4-*- */

/*
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute 
 *it and/or modify it under the terms of 
 *the GNU General Public License as published by the 
 *Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */

#include <string.h>
#include "mlview-iview.h"
#include "mlview-marshal.h"
#include "mlview-safe-ptr-utils.h"

/**
 *@file
 *The definition of the #MlViewIView interface.
 */
namespace mlview
{
struct IViewPriv
{
	UString name ;
	UString desc_type_name ;
	MlViewXMLDocument *doc ;
	SafePtr<Gtk::Widget, GtkWidgetMMRef, GtkWidgetMMUnref> view_impl_ptr ;

	sigc::signal1<void, IView*> signal_view_name_changed ;
	sigc::signal0<void> signal_is_swapped_out ;
	sigc::signal0<void> signal_is_swapped_in ;
	sigc::signal0<void> signal_application_menu_populating_requested ;

	IViewPriv ():
			doc (NULL),
			view_impl_ptr (NULL)
	{}
}
;

//******************
// protected methods
//******************

//************
//public methods
//************

IView::IView ()
{
	m_priv = new IViewPriv () ;
	THROW_IF_FAIL (m_priv) ;
}

IView::IView (MlViewXMLDocument *a_doc,
              const UString &a_name,
              const UString &a_desc_type_name)
{
	m_priv = new IViewPriv () ;
	THROW_IF_FAIL (m_priv) ;
	m_priv->name = a_name ;
	set_document (a_doc) ;
}

IView::~IView ()
{
	THROW_IF_FAIL (m_priv) ;
	if (m_priv->doc) {
		set_document (NULL) ;
	}
	delete m_priv ;
	m_priv = NULL ;
}

IView *
IView::create_instance (MlViewXMLDocument *a_doc,
                        const UString &a_name)
{
	THROW_EXCEPTION (MethodNotImplementedException,
	                 "IView::create_instance() not implemented") ;
}

void
IView::set_view_name (const UString &a_name)
{
	m_priv->name = a_name ;
	m_priv->signal_view_name_changed.emit (this) ;
}

UString
IView::get_view_name () const
{
	return m_priv->name ;
}

MlViewXMLDocument*
IView::get_document () const
{
	return m_priv->doc ;
}

void
IView::set_document (MlViewXMLDocument *a_doc)
{
	if (m_priv->doc) {
		if (a_doc == m_priv->doc)
			return ;
		g_object_unref (G_OBJECT (m_priv->doc)) ;
		m_priv->doc = NULL ;
	}
	m_priv->doc = a_doc ;
	if (a_doc)
		g_object_ref (G_OBJECT (m_priv->doc)) ;
}


void
IView::set_desc_type_name (const UString &a_name)
{
	m_priv->desc_type_name = a_name ;
}

Gtk::Widget*
IView::get_view_widget () const
{
	if (m_priv->view_impl_ptr == NULL) {
		THROW ("View implementor forget to call IView::set_view_widget()") ;
	}
	return m_priv->view_impl_ptr ;
}

void
IView::set_view_widget (Gtk::Widget *a_widget)
{
	m_priv->view_impl_ptr = a_widget ;
}

UString
IView::get_desc_type_name () const
{
	return m_priv->desc_type_name ;
}

enum MlViewStatus
IView::notify_swapped_out ()
{
	m_priv->signal_is_swapped_out.emit () ;
	return MLVIEW_OK ;
}

enum MlViewStatus
IView::notify_swapped_in ()
{
	m_priv->signal_is_swapped_in.emit () ;
	return MLVIEW_OK ;
}

enum MlViewStatus
IView::request_application_menu_populating ()
{
	m_priv->signal_application_menu_populating_requested.emit () ;
	return MLVIEW_OK ;
}


//****************
// signals
//****************
sigc::signal1<void, IView*>
IView::signal_view_name_changed ()
{
	return m_priv->signal_view_name_changed ;
}

sigc::signal0<void>
IView::signal_is_swapped_out ()
{
	return m_priv->signal_is_swapped_out ;
}

sigc::signal0<void>
IView::signal_is_swapped_in ()
{
	return m_priv->signal_is_swapped_in ;
}

sigc::signal0<void>
IView::signal_application_menu_populating_requested ()
{
	return m_priv->signal_application_menu_populating_requested ;
}

}//end namespace

