/* -*- Mode: C++; indent-tabs-mode: true; c-basic-offset: 8-*- */

/*
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute it 
 *and/or modify it under the terms of 
 *the GNU General Public License as published 
 *by the Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *MlView is distributed in the hope that it will be useful, 
 *but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. If not, write to the Free Software Foundation, Inc., 
 *59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */

#ifndef __MLVIEW_PLUGIN_H__
#define __MLVIEW_PLUGIN_H__

#include "mlview-plugin-descriptor.h"
#include "mlview-exception.h"
#include "mlview-object.h"

/*
 *@file
 *The declaration of the #Plugin class.
 *
 */

namespace mlview {
	struct PluginPriv ;
	
	class Plugin : public Object
	{
		friend struct PluginPriv ;
		PluginPriv *m_priv ;
		
		//forbid assignation/copy
		Plugin& operator= (Plugin const&) ;
		Plugin (Plugin const&) ;

	public:
		Plugin (const PluginDescriptor &a_descr) ;

		Plugin (const UString &a_url) ;
		
		virtual ~Plugin () ;
		
		gpointer user_data ;
		
		const PluginDescriptor& get_descriptor () const ;
		
		//**************************
		// signals
		//**************************
		sigc::signal0<void>signal_unloaded ;
		
	protected:
		
		//***************************
		//forbid copy and assignation
		//***************************
		Plugin (Plugin &a_plugin) ;
		Plugin& operator= (Plugin &an_other_plugin) ;
	} ; // end class mlview::Plugin
	
	typedef bool (*PluginSymbol) (Plugin *a_plugin);
        
} // end namespace mlview

#endif /* __MLVIEW_PLUGIN_H__ */
