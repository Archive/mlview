/* -*- Mode: C++; indent-tabs-mode: true; c-basic-offset: 4 -*- */
/*
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute 
 *it and/or modify it under the terms of 
 *the GNU General Public License as published by the 
 *Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYING file for copyright information.
 */
#ifndef MLVIEW_PREFS_CATEGORY_SEARCH_H
#define MLVIEW_PREFS_CATEGORY_SEARCH_H

#include "mlview-prefs-storage-manager.h"
#include "mlview-prefs-category.h"

namespace mlview
{
///
/// This structure holds PrefsCategorySearch
/// private members
///
struct PrefsCategorySearchPriv;

///
/// This class is the category handling search-specific preferences
///
class PrefsCategorySearch : public PrefsCategory
{
	friend struct PrefsCategorySearchPriv;
	PrefsCategorySearchPriv *m_priv;

	//forbid copy/assignation
	PrefsCategorySearch (PrefsCategorySearch const&) ;
	PrefsCategorySearch& operator= (PrefsCategorySearch const&) ;

public:
	///
	/// Create a new PrefsCategorySearch object.
	///
	/// \param manager the storage manager used to access preferences
	///
	PrefsCategorySearch (PrefsStorageManager *manager);
	///
	/// Default destructor
	///
	virtual ~PrefsCategorySearch ();


	///
	/// Retrieve the 'search among node names' property default value
	/// This property tells whether to search for matching node names or not
	///
	/// \return TRUE if node names are included, FALSE otherwise
	///
	bool search_among_node_names_default ();
	///
	/// Retrieve the 'search among node values' property value
	/// This property tells whether to search for matching node values or not
	///
	/// \return TRUE if node values are included, FALSE otherwise
	///
	bool search_among_node_values_default ();
	///
	/// Retrieve the 'search among attribute names' property value
	/// This property tells whether to search for matching attribute names or not
	///
	/// \return TRUE if attribute names are included, FALSE otherwise
	///
	bool search_among_attr_names_default ();
	///
	/// Retrieve the 'search among attribute values' property value
	/// This property tells whether to search for matching attribute values or not
	///
	/// \return TRUE if attribute values are included, FALSE otherwise
	///
	bool search_among_attr_values_default ();

	///
	/// Retrieve the 'search among node names' property value
	/// This property tells whether to search for matching node names or not
	///
	/// \return TRUE if node names are included, FALSE otherwise
	///
	bool search_among_node_names ();
	///
	/// Retrieve the 'search among node values' property value
	/// This property tells whether to search for matching node values or not
	///
	/// \return TRUE if node values are included, FALSE otherwise
	///
	bool search_among_node_values ();
	///
	/// Retrieve the 'search among attribute names' property value
	/// This property tells whether to search for matching attribute names or not
	///
	/// \return TRUE if attribute names are included, FALSE otherwise
	///
	bool search_among_attr_names ();
	///
	/// Retrieve the 'search among attribute values' property value
	/// This property tells whether to search for matching attribute values or not
	///
	/// \return TRUE if attribute values are included, FALSE otherwise
	///
	bool search_among_attr_values ();
	///
	/// Retrieve the 'search case sensitive' property value
	/// This property tells whether search is case sensitive or not
	///
	/// \return TRUE if node names are included, FALSE otherwise
	///
	bool search_case_sensitive ();

	///
	/// Store the 'search among node names' property value
	/// This property tells whether to search for matching node names or not
	///
	/// \param value TRUE if node names are included in search, FALSE otherwise
	///
	void set_search_among_node_names (bool value);
	///
	/// Store the 'search among node values' property value
	/// This property tells whether to search for matching node values or not
	///
	/// \param value TRUE if node values are included in search, FALSE otherwise
	///
	void set_search_among_node_values (bool value);
	///
	/// Store the 'search among attribute names' property value
	/// This property tells whether to search for matching attribute names or not
	///
	/// \param value TRUE if attribute names are included in search,
	/// FALSE otherwise
	///
	void set_search_among_attr_names (bool value);
	///
	/// Store the 'search among attribute values' property value
	/// This property tells whether to search for matching attribute values or not
	///
	/// \param value TRUE if attribute values are included in search,
	/// FALSE otherwise
	///
	void set_search_among_attr_values (bool value);
	///
	/// Store the 'search case sensitive' property value
	/// This property tells whether search is case sensitive or not
	///
	/// \param value TRUE if node names are included in search, FALSE otherwise
	///
	void set_search_case_sensitive (bool value);
};

} // namespace mlview


#endif
