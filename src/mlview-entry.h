/* -*- Mode: C; indent-tabs-mode:nil; c-basic-offset: 8-*- */
/*
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute 
 *it and/or modify it under the terms of 
 *the GNU General Public License as published by the 
 *Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */

/**
 *This class is a custom GtkEntry.
 *It does completion for separate words of
 *a "sentence" while the user types.
 */
#ifndef __MLVIEW_ENTRY_H__
#define __MLVIEW_ENTRY_H__

#include <gtk/gtk.h>
#include "mlview-utils.h"

#define MLVIEW_TYPE_ENTRY (mlview_entry_get_type())
#define MLVIEW_ENTRY(object) (G_TYPE_CHECK_INSTANCE_CAST((object),MLVIEW_TYPE_ENTRY,MlViewEntry))
#define MLVIEW_ENTRY_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST((klass),MLVIEW_TYPE_ENTRY,MlViewEntryClass))
#define MLVIEW_IS_ENTRY(object) (G_TYPE_CHECK_INSTANCE_TYPE((object),MLVIEW_TYPE_ENTRY))
#define MLVIEW_IS_ENTRY_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass),MLVIEW_TYPE_ENTRY))
/*common class and instance definitions*/
typedef struct _MlViewEntry MlViewEntry;
typedef struct _MlViewEntryClass MlViewEntryClass;
typedef struct _MlViewEntryPrivate MlViewEntryPrivate;


struct _MlViewEntry
{
	GtkEntry parent ;
	MlViewEntryPrivate *priv ;
} ;

struct _MlViewEntryClass
{
	GtkEntryClass parent_class ;
} ;

GType mlview_entry_get_type (void) ;

GtkWidget * mlview_entry_new (void) ;

enum MlViewStatus mlview_entry_popup_word_completion_menu (MlViewEntry *a_this,
        gint a_word_start_index,
        gint a_word_end_index) ;

enum MlViewStatus mlview_entry_select_next_popup_menu_item (MlViewEntry *a_this) ;

enum MlViewStatus mlview_entry_select_prev_popup_menu_item (MlViewEntry *a_this) ;

enum MlViewStatus mlview_entry_hide_word_completion_menu (MlViewEntry *a_this) ;

gboolean mlview_entry_is_popup_win_visible (MlViewEntry *a_this) ;

enum MlViewStatus mlview_entry_set_current_word_to_current_completion_string (MlViewEntry *a_this) ;

enum MlViewStatus mlview_entry_set_completion_list (MlViewEntry *a_this,
        GList *a_completion_list) ;

enum MlViewStatus mlview_entry_get_completion_list (MlViewEntry *a_this,
        GList **a_completion_list) ;

#endif /*__MLVIEW_ENTRY_H__*/
