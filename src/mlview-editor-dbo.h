/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 8-*- */

/*
 *This file is part of MlView
 *
 *MlView is free software; you can redistribute it and/or 
 *modify it under the terms of 
 *the GNU General Public License as published 
 *by the Free Software Foundation; either version 2
 *or (at your option) any later version.
 *
 *MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView.
 *see the file COPYING. 
 *If not, write to the 
 *Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */

#ifndef __MLVIEW_EDITOR_DBO_H__
#define __MLVIEW_EDITOR_DBO_H__

#include "mlview-utils.h"
#ifdef MLVIEW_WITH_DBUS

#include "mlview-idbo.h"

G_BEGIN_DECLS

#define PATH_ORG_MLVIEW_EDITOR_OBJECT "/org/mlview/MlViewEditorObject"
#define INTERFACE_ORG_MLVIEW_EDITOR_IFACE "org.mlview.EditorIface"

#define MLVIEW_TYPE_EDITOR_DBO (mlview_editor_dbo_get_type ())
#define MLVIEW_EDITOR_DBO(widget) (G_TYPE_CHECK_INSTANCE_CAST ((widget), MLVIEW_TYPE_EDITOR_DBO, MlViewEditorDBO))
#define MLVIEW_EDITOR_DBO_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), MLVIEW_TYPE_EDITOR_DBO, MlViewEditorDBOClass))
#define MLVIEW_IS_EDITOR_DBO(widget) (G_TYPE_CHECK_INSTANCE_TYPE ((widget), MLVIEW_TYPE_EDITOR_DBO))
#define MLVIEW_IS_EDITOR_DBO_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), MLVIEW_TYPE_EDITOR_DBO))

typedef struct _MlViewEditorDBO MlViewEditorDBO ;
typedef struct _MlViewEditorDBOClass MlViewEditorDBOClass ;
typedef struct _MlViewEditorDBOPriv MlViewEditorDBOPriv ;

struct _MlViewEditorDBO
{
	GObject parent_object ;
	MlViewEditorDBOPriv *priv ;
} ;

struct _MlViewEditorDBOClass
{
	GObjectClass parent_object ;
} ;

MlViewEditorDBO *
mlview_editor_dbo_new (MlViewAppContext *a_this) ;

GType mlview_editor_dbo_get_type (void) ;

enum MlViewStatus
mlview_editor_dbo_load_xml_file_with_dtd (MlViewEditorDBO *a_this,
        const char *doc_uri,
        const char *dtd_uri) ;

G_BEGIN_DECLS

#endif /*MLVIEW_WITH_DBUS*/
#endif /*__MLIVEW_EDITOR_DBO_H__*/
