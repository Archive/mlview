/* -*- Mode: C++; indent-tabs-mode:true; c-basic-offset: 4-*- */

/*This file is part of GNU MlView
 *
 *GNU MlView is free software; 
 *you can redistribute it and/or modify it under the terms of 
 *the GNU General Public License as 
 *published by the Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope 
 *that it will be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */
#ifndef __MLVIEW_SAFE_PTR_UTILS_H__
#define __MLVIEW_SAFE_PTR_UTILS_H__

#include <libxml/xmlregexp.h>
#include <libxml/xmlreader.h>
#include <gtkmm.h>
#include <gtksourceview/gtksourceview.h>
#include "mlview-safe-ptr.h"
#include "mlview-ustring.h"
#include "mlview-object.h"
#include "mlview-plugin-descriptor.h"

namespace mlview {
//************************************
//libc related Rer/Unref functors
//************************************
struct StatRef
{
    void
    operator () (struct stat *buf)
    {}
} ;

struct StatUnref
{
    void
    operator () (struct stat *buf)
    {
		if (buf) {
			delete buf ;
			buf = NULL ;
		}
    }
} ;

//************************************
//glib/gobject related Rer/Unref functors
//************************************
struct GDirRef
{
    void
    operator () (GDir *dir)
    {}
} ;

struct GDirUnref
{
    void
    operator () (GDir *dir)
    {
		if (dir) {
			g_dir_close (dir) ;
			dir = NULL ;
		}
    }
} ;

struct GMemRef
{
	void
	operator () (gchar *)
	{}
};

struct GMemUnref
{
	void
	operator () (gchar *a_mem)
	{
	    if (a_mem) {
			g_free (a_mem) ;
			a_mem = NULL ;
		}
	}
};

struct GErrorRef
{
	void
	operator () (GError *a_error)
	{
	}
};

struct GErrorUnref
{
	void
	operator () (GError *a_error)
	{
		if (a_error) {
			g_error_free (a_error) ;
			a_error = NULL ;
		}
	}
};

struct GObjectRef
{
	void
	operator () (GObject *a_ptr)
	{
		long ref = 1 ;
		if (a_ptr) {
			g_object_ref (a_ptr) ;
			ref = a_ptr->ref_count ;
		}
	}
} ;

struct GObjectUnref
{
	void
	operator () (GObject *a_ptr)
	{
		long ref = 0 ;
		if (a_ptr) {
			ref = a_ptr->ref_count ;
			g_object_unref (a_ptr) ;
			--ref ;
		}
	}
};

//**************************
//gtk related functors
//**************************

struct GtkWidgetRef
{
	void
	operator () (GtkWidget *a_ptr)
	{
		if (a_ptr && G_IS_OBJECT (a_ptr))
			g_object_ref (G_OBJECT (a_ptr)) ;
	}
};

struct GtkWidgetUnref
{
	void
	operator () (GtkWidget *a_ptr)
	{
		if (a_ptr && G_IS_OBJECT (a_ptr))
			g_object_unref (G_OBJECT (a_ptr)) ;
	}
};

struct GtkSourceViewRef
{
	void
	operator () (GtkSourceView *a_ptr)
	{
		if (a_ptr && GTK_IS_SOURCE_VIEW (a_ptr))
			g_object_ref (a_ptr) ;
	}
};

struct GtkSourceViewUnref
{
	void
	operator () (GtkSourceView *a_ptr)
	{
		if (a_ptr && GTK_IS_SOURCE_VIEW (a_ptr))
			g_object_unref (a_ptr) ;
	}
};

//**************************************
//glibmm related functors
//**************************************

struct GObjectMMRef
{
	void
	operator () (Glib::Object *a_ptr)
	{
		if (a_ptr) {
			a_ptr->reference () ;
		}
	}
};

struct GObjectMMUnref
{
	void
	operator () (Glib::Object *a_ptr)
	{
		if (a_ptr) {
			a_ptr->unreference () ;
		}
	}
};


//******************************
//gtkmm related functors
//******************************

struct GtkWidgetMMRef
{
	void
	operator () (Gtk::Widget *a_widget)
	{
		if (a_widget)
			a_widget->reference () ;
	}

};

struct GtkWidgetMMUnref
{
	void
	operator () (Gtk::Widget *a_widget)
	{
		if (a_widget)
			a_widget->unreference () ;
	}
};

//*************************
//libxml related functors
//*************************
struct XMLTextReaderRef
{
	void
	operator () (xmlTextReader *a_reader)
	{
	}
};

struct XMLTextReaderUnref
{
	void
	operator () (xmlTextReader *a_reader)
	{
		if (a_reader) {
			xmlFreeTextReader (a_reader) ;
			a_reader = NULL ;
		}
	}
};

struct XMLRegExecReference
{

	void
	operator () (xmlRegExecCtxt *a_reg_exec)
		{}
}
;

struct XMLRegExecUnreference
{
public:

	void
	operator () (xmlRegExecCtxt *a_reg_exec)
	{
		if (a_reg_exec)
			xmlRegFreeExecCtxt
			(const_cast<xmlRegExecCtxt*>(a_reg_exec));
	}
};

struct XMLMemRef
{
	void
	operator () (xmlChar*)
	{
	}
};

struct XMLMemUnref
{
	void
	operator () (xmlChar* a_buf)
	{
		xmlFree (a_buf) ;
	}
};

//*******************************
//mlview types related functors
//*******************************

struct ObjectRef
{

	void
	operator () (const mlview::Object* a_ptr)
	{
		if (a_ptr)
			a_ptr->ref () ;
	}
}
; //end ObjectReference

struct ObjectUnref
{

	void
	operator () (mlview::Object* a_ptr)
	{
		if (a_ptr)
			a_ptr->unref () ;
	}
}; //end ObjectUnReference


//typedefs to save some tiping when using some SafePtrs
typedef SafePtr<GError, GErrorRef, GErrorUnref> GErrorSafePtr ;
typedef SafePtr<GDir, GDirRef, GDirUnref> GDirSafePtr ;
typedef SafePtr<list<UString> > ListOfUStringsSafePtr ;
typedef SafePtr<struct stat, StatRef, StatUnref> StatSafePtr ;
typedef SafePtr<ListOfPluginDescriptors> ListOfPluginDescriptorsSafePtr ;
typedef SafePtr<gchar, GMemRef, GMemUnref> GMemSafePtr ;
typedef SafePtr<xmlTextReader, XMLTextReaderRef, XMLTextReaderUnref> XMLTextReaderSafePtr;
}//end namespace mlview
#endif //__MLVIEW_SAFE_GOBJECT_PTR_H__
