/* -*- Mode: C++; indent-tabs-mode:nil; c-basic-offset: 4-*- */

/*
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute 
 *it and/or modify it under the terms of 
 *the GNU General Public License as published by the 
 *Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYING file for copyright information.
 */

#include <string.h>
#include "mlview-ns-editor.h"
#include "mlview-xml-document.h"
#include "mlview-utils.h"
#include "mlview-marshal.h"


/**
 *@file
 *The definition of the #MlViewNSEditor class.
 */
struct _MlViewNSEditorPrivate
{
	GtkTreeView *view ;
	GtkTreeModel *model ;
	GtkTreeRowReference *cur_sel_start ;
	GtkTreeRowReference *cur_sel_end ;
	GHashTable *ns_row_hash ;
	xmlNode *cur_xml_node ;
	MlViewXMLDocument *xml_doc ;
	gboolean enable_node_alteration ;
	gboolean dispose_has_run ;
};

enum {
    NAMESPACE_ADDED,
    NAMESPACE_PREFIX_CHANGED,
    NAMESPACE_URI_CHANGED,
    NAMESPACE_CHANGED,
    NAMESPACE_DELETED,
    NUMBER_OF_SIGNALS
};

enum MLVIEW_NS_EDITOR_COLUMNS{
    /*
     *hidden column, 
     * contains a pointer to xmlNs
     */
    XML_NS_COLUMN,

    IS_EDITABLE_COLUMN,
    /*
     *hidden column that contains
     *a flag to indicate if a row
     *is the "new ns" row or not.
     */
    IS_ADD_NEW_NS_COLUMN,
    /*the first visible column: ns uri column*/
    NS_URI_COLUMN,

    /*the second visible column. ns prefix column*/
    NS_PREFIX_COLUMN,
    NB_COLUMNS
};

#define PRIVATE(editor) (editor->priv)

static GtkVBoxClass *gv_parent_class = NULL;
static guint gv_signals[NUMBER_OF_SIGNALS] = {0};

static void mlview_ns_editor_class_init (MlViewNSEditorClass *a_klass) ;
static void mlview_ns_editor_init (MlViewNSEditor *a_editor) ;
static void mlview_ns_editor_dispose (GObject *a_object) ;
static void mlview_ns_editor_finalize (GObject *a_object) ;

/*callbacks function definitions*/
static void row_selected_cb (GtkTreeSelection *a_sel,
                             MlViewNSEditor *a_editor) ;

static void ns_uri_cell_edited_cb (GtkCellRendererText *a_renderer,
                                   gchar *a_cell_path,
                                   gchar *a_ns_uri,
                                   gpointer a_data) ;

static void ns_prefix_cell_edited_cb (GtkCellRendererText *a_renderer,
                                      gchar *a_cell_path,
                                      gchar *a_ns_prefix,
                                      gpointer a_data) ;
/********************
 *private functions
 ********************/

static void
mlview_ns_editor_class_init (MlViewNSEditorClass *a_class)
{
	GObjectClass *gobject_class = NULL;

	gobject_class = G_OBJECT_CLASS (a_class) ;
	g_return_if_fail (gobject_class) ;
	gv_parent_class = (GtkVBoxClass*) g_type_class_peek_parent (a_class) ;
	g_return_if_fail (gv_parent_class) ;
	gobject_class->dispose = mlview_ns_editor_dispose ;
	gobject_class->finalize = mlview_ns_editor_finalize ;

	gv_signals[NAMESPACE_ADDED] =
	    g_signal_new ("namespace-added",
	                  G_TYPE_FROM_CLASS (gobject_class),
	                  G_SIGNAL_RUN_FIRST,
	                  G_STRUCT_OFFSET (MlViewNSEditorClass,
	                                   namespace_added),
	                  NULL, NULL,
	                  mlview_marshal_VOID__POINTER,
	                  G_TYPE_NONE, 1,
	                  G_TYPE_POINTER, NULL) ;

	gv_signals[NAMESPACE_PREFIX_CHANGED] =
	    g_signal_new ("namespace-prefix-changed",
	                  G_TYPE_FROM_CLASS (gobject_class),
	                  G_SIGNAL_RUN_FIRST,
	                  G_STRUCT_OFFSET
	                  (MlViewNSEditorClass,
	                   namespace_prefix_changed),
	                  NULL, NULL,
	                  mlview_marshal_VOID__POINTER,
	                  G_TYPE_NONE, 1,
	                  G_TYPE_POINTER, NULL) ;

	gv_signals[NAMESPACE_URI_CHANGED] =
	    g_signal_new ("namespace-uri-changed",
	                  G_TYPE_FROM_CLASS (gobject_class),
	                  G_SIGNAL_RUN_FIRST,
	                  G_STRUCT_OFFSET
	                  (MlViewNSEditorClass,
	                   namespace_uri_changed),
	                  NULL, NULL,
	                  mlview_marshal_VOID__POINTER,
	                  G_TYPE_NONE, 1, G_TYPE_POINTER, NULL) ;

	gv_signals[NAMESPACE_CHANGED] =
	    g_signal_new ("namespace-changed",
	                  G_TYPE_FROM_CLASS (gobject_class),
	                  G_SIGNAL_RUN_FIRST,
	                  G_STRUCT_OFFSET
	                  (MlViewNSEditorClass,
	                   namespace_changed),
	                  NULL, NULL,
	                  mlview_marshal_VOID__POINTER,
	                  G_TYPE_NONE, 1, G_TYPE_POINTER, NULL) ;

	gv_signals[NAMESPACE_DELETED] =
	    g_signal_new ("namespace-deleted",
	                  G_TYPE_FROM_CLASS (gobject_class),
	                  G_SIGNAL_RUN_FIRST,
	                  G_STRUCT_OFFSET
	                  (MlViewNSEditorClass,
	                   namespace_deleted),
	                  NULL, NULL,
	                  mlview_marshal_VOID__POINTER,
	                  G_TYPE_NONE, 1, G_TYPE_POINTER, NULL) ;
}

/**
 *Free the objects hold by the current instance of
 *#MlViewNSEditor.
 *@param a_object the current instance of #MlViewNSEditor.
 */
static void
mlview_ns_editor_dispose (GObject *a_object)
{
	MlViewNSEditor *editor = NULL ;

	editor = MLVIEW_NS_EDITOR (a_object) ;
	g_return_if_fail (editor && PRIVATE (editor)) ;
	if (PRIVATE (editor)->dispose_has_run == TRUE) {
		return ;
	}
	if (PRIVATE (editor)->view) {
		gtk_widget_destroy
		(GTK_WIDGET (PRIVATE (editor)->view)) ;
		PRIVATE (editor)->view = NULL ;
	}
	PRIVATE (editor)->dispose_has_run = TRUE ;
}

/**
 *Frees the private data structure of #MlViewNSEditor.
 *@param a_object the current instance of #MlViewNSEditor.
 */
static void
mlview_ns_editor_finalize (GObject *a_object)
{
	MlViewNSEditor *editor = NULL ;

	g_return_if_fail (a_object) ;
	editor = MLVIEW_NS_EDITOR (a_object) ;
	g_return_if_fail (editor && PRIVATE (editor)) ;

	g_free (PRIVATE (editor)) ;
	PRIVATE (editor) = NULL ;
}

static void
mlview_ns_editor_init (MlViewNSEditor *a_this)
{
	PRIVATE (a_this) = (MlViewNSEditorPrivate*) g_try_malloc
	                   (sizeof (MlViewNSEditorPrivate)) ;
	if (!PRIVATE (a_this)) {
		mlview_utils_trace_debug ("g_try_malloc failed") ;
		return ;
	}
	memset (PRIVATE (a_this), 0, sizeof (MlViewNSEditorPrivate)) ;
}

static enum MlViewStatus
mlview_ns_editor_construct (MlViewNSEditor *a_this,
                            const gchar *a_first_column_title,
                            const gchar *a_second_column_title)
{
	GtkWidget *scr_win = NULL ;
	GtkCellRenderer *cell_renderer = NULL ;
	GtkTreeIter iter = {0} ;
	GtkTreeSelection *selection = NULL ;

	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_NS_EDITOR (a_this)
	                      && PRIVATE (a_this)
	                      && !PRIVATE (a_this)->view
	                      && !PRIVATE (a_this)->model,
	                      MLVIEW_BAD_PARAM_ERROR) ;
	PRIVATE (a_this)->model = GTK_TREE_MODEL
	                          (gtk_list_store_new
	                           (NB_COLUMNS, G_TYPE_POINTER,
	                            G_TYPE_BOOLEAN, G_TYPE_BOOLEAN,
	                            G_TYPE_STRING, G_TYPE_STRING)) ;
	gtk_list_store_append (GTK_LIST_STORE
	                       (PRIVATE (a_this)->model),
	                       &iter) ;

	gtk_list_store_set (GTK_LIST_STORE
	                    (PRIVATE (a_this)->model),
	                    &iter, XML_NS_COLUMN, NULL,
	                    IS_ADD_NEW_NS_COLUMN, TRUE,
	                    IS_EDITABLE_COLUMN, TRUE,
	                    NS_URI_COLUMN, "",
	                    NS_PREFIX_COLUMN, "",
	                    -1) ;
	PRIVATE (a_this)->view = GTK_TREE_VIEW
	                         (gtk_tree_view_new_with_model
	                          (PRIVATE (a_this)->model)) ;
	cell_renderer = gtk_cell_renderer_text_new () ;
	g_return_val_if_fail (cell_renderer, MLVIEW_ERROR) ;
	gtk_tree_view_insert_column_with_attributes
	(PRIVATE (a_this)->view, NS_URI_COLUMN,
	 a_first_column_title, cell_renderer,
	 "text", NS_URI_COLUMN,
	 "editable", IS_EDITABLE_COLUMN,
	 NULL) ;
	g_signal_connect (G_OBJECT (cell_renderer),
	                  "edited",
	                  G_CALLBACK (ns_uri_cell_edited_cb),
	                  a_this) ;
	cell_renderer = gtk_cell_renderer_text_new () ;
	g_return_val_if_fail (cell_renderer, MLVIEW_ERROR) ;
	g_signal_connect (G_OBJECT (cell_renderer),
	                  "edited",
	                  G_CALLBACK (ns_prefix_cell_edited_cb),
	                  a_this) ;
	gtk_tree_view_insert_column_with_attributes
	(PRIVATE (a_this)->view, NS_PREFIX_COLUMN,
	 a_second_column_title, cell_renderer,
	 "text", NS_PREFIX_COLUMN,
	 "editable", IS_EDITABLE_COLUMN,
	 NULL) ;
	scr_win = gtk_scrolled_window_new (NULL, NULL) ;
	gtk_scrolled_window_set_policy (
	    GTK_SCROLLED_WINDOW (scr_win),
	    GTK_POLICY_AUTOMATIC,
	    GTK_POLICY_AUTOMATIC);

	gtk_container_add (GTK_CONTAINER (scr_win),
	                   GTK_WIDGET (PRIVATE (a_this)->view)) ;
	gtk_box_pack_start (GTK_BOX (a_this), scr_win,
	                    TRUE, TRUE, 0) ;
	selection = gtk_tree_view_get_selection
	            (PRIVATE (a_this)->view) ;
	g_signal_connect (G_OBJECT (selection),
	                  "changed",
	                  G_CALLBACK (row_selected_cb),
	                  a_this) ;

	PRIVATE (a_this)->enable_node_alteration = TRUE ;

	return MLVIEW_OK ;

}

static enum MlViewStatus
mlview_ns_editor_select_ns (MlViewNSEditor *a_this,
                            xmlNs *a_ns)
{
	GtkTreeRowReference *row_ref = NULL ;
	GtkTreePath * tree_path = NULL ;
	GtkTreeSelection *sel = NULL ;
	GtkTreeModel *model = NULL ;
	enum MlViewStatus status = MLVIEW_OK ;

	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_NS_EDITOR (a_this)
	                      && PRIVATE (a_this)
	                      && PRIVATE (a_this)->view,
	                      MLVIEW_BAD_PARAM_ERROR) ;

	row_ref = mlview_ns_editor_get_row_ref_from_ns
	          (a_this, a_ns) ;
	if (!row_ref)
		return MLVIEW_ERROR ;
	tree_path = gtk_tree_row_reference_get_path (row_ref) ;
	g_return_val_if_fail (tree_path, MLVIEW_ERROR) ;
	model = mlview_ns_editor_get_model (a_this) ;
	if (!model) {
		mlview_utils_trace_debug
		("mlview_ns_editor_get_model() failed") ;
		status = MLVIEW_ERROR ;
		goto cleanup ;
	}
	sel = gtk_tree_view_get_selection
	      (PRIVATE (a_this)->view) ;
	if (!sel) {
		mlview_utils_trace_debug
		("gtk_tree_model_get_selection () failed") ;
		status = MLVIEW_ERROR ;
		goto cleanup ;
	}
	gtk_tree_selection_select_path (sel, tree_path) ;
	status = MLVIEW_OK ;

cleanup:
	if (tree_path) {
		gtk_tree_path_free (tree_path) ;
		tree_path = NULL ;
	}
	return status ;
}


static void
row_selected_cb (GtkTreeSelection *a_sel,
                 MlViewNSEditor *a_editor)
{
	gboolean is_ok = TRUE ;
	GtkTreeIter iter = {0} ;
	GtkTreeModel * model = NULL ;

	g_return_if_fail (a_editor
	                  && MLVIEW_IS_NS_EDITOR (a_editor)
	                  && PRIVATE (a_editor)) ;
	model = mlview_ns_editor_get_model (a_editor) ;
	g_return_if_fail (model) ;
	is_ok = gtk_tree_selection_get_selected (a_sel, &model,
	        &iter) ;
	if (is_ok != TRUE)
		return ;
	mlview_ns_editor_set_current_selected_row (a_editor,
	        &iter) ;
}

static void
ns_uri_cell_edited_cb (GtkCellRendererText *a_renderer,
                       gchar *a_cell_path,
                       gchar *a_ns_uri,
                       gpointer a_data)
{
	MlViewNSEditor *editor = NULL ;
	enum MlViewStatus status = MLVIEW_OK ;
	GtkTreeIter iter = {0} ;
	gchar *ns_prefix = NULL ;
	GtkTreeModel *model = NULL ;
	xmlNs *ns = NULL ;

	g_return_if_fail (a_renderer && a_cell_path && a_data) ;
	g_return_if_fail (MLVIEW_IS_NS_EDITOR (a_data)) ;
	editor = (MlViewNSEditor*)a_data ;
	g_return_if_fail (PRIVATE (editor)
	                  && PRIVATE (editor)->cur_xml_node) ;

	if (PRIVATE (editor)->enable_node_alteration == FALSE)
		return ;

	status = mlview_ns_editor_get_cur_sel_start (editor, &iter) ;
	g_return_if_fail (status == MLVIEW_OK) ;
	model = mlview_ns_editor_get_model (editor) ;
	g_return_if_fail (model) ;
	if (mlview_ns_editor_is_row_the_add_new_ns_row
	        (editor, &iter) == TRUE) {
		gtk_tree_model_get (model, &iter,
		                    NS_PREFIX_COLUMN, &ns_prefix,
		                    -1) ;
		if (a_ns_uri
		        && PRIVATE (editor)->cur_xml_node
		        && strcmp (a_ns_uri, "")) {
			mlview_ns_editor_add_namespace
			(editor, ns_prefix, a_ns_uri) ;
			gtk_list_store_set (GTK_LIST_STORE (model),
			                    &iter,
			                    NS_URI_COLUMN, "",
			                    -1) ;
		}
	} else {
		gtk_tree_model_get (model, &iter,
		                    XML_NS_COLUMN, &ns,
		                    NS_PREFIX_COLUMN, &ns_prefix,
		                    -1) ;
		g_return_if_fail (ns) ;
		if (a_ns_uri
		        && strcmp (a_ns_uri, "")) {
			mlview_xml_document_set_ns
			(PRIVATE (editor)->xml_doc,
			 PRIVATE (editor)->cur_xml_node, ns,
			 (xmlChar*)a_ns_uri, (xmlChar*)ns_prefix, TRUE) ;
		} else if (a_ns_uri
		           && !strcmp (a_ns_uri, "")) {
			mlview_xml_document_remove_ns
			(PRIVATE (editor)->xml_doc,
			 ns,
			 PRIVATE (editor)->cur_xml_node,
			 TRUE) ;
		} else {
			mlview_utils_trace_debug
			("We shouldn't be in this case") ;
		}
	}
}

static void
ns_prefix_cell_edited_cb (GtkCellRendererText *a_renderer,
                          gchar *a_cell_path,
                          gchar *a_ns_prefix,
                          gpointer a_data)
{
	MlViewNSEditor *editor = NULL ;
	enum MlViewStatus status = MLVIEW_OK ;
	GtkTreeIter iter = {0} ;
	gchar *ns_uri = NULL ;
	GtkTreeModel *model = NULL ;
	xmlNs *ns = NULL ;

	g_return_if_fail (a_renderer && a_cell_path && a_data) ;
	g_return_if_fail (MLVIEW_IS_NS_EDITOR (a_data)) ;
	editor = (MlViewNSEditor*)a_data ;
	g_return_if_fail (PRIVATE (editor)
	                  && PRIVATE (editor)->cur_xml_node) ;

	status = mlview_ns_editor_get_cur_sel_start (editor, &iter) ;
	g_return_if_fail (status == MLVIEW_OK) ;

	if (PRIVATE (editor)->enable_node_alteration == FALSE)
		return ;

	model = mlview_ns_editor_get_model (editor) ;
	if (mlview_ns_editor_is_row_the_add_new_ns_row
	        (editor, &iter) == TRUE) {
		gtk_tree_model_get (model, &iter,
		                    NS_URI_COLUMN, &ns_uri,
		                    -1) ;
		if (ns_uri
		        && PRIVATE (editor)->cur_xml_node
		        && strcmp (ns_uri, "")) {
			mlview_ns_editor_add_namespace
			(editor, a_ns_prefix, ns_uri) ;
			gtk_list_store_set (GTK_LIST_STORE (model),
			                    &iter,
			                    NS_URI_COLUMN, "",
			                    NS_PREFIX_COLUMN, "",
			                    -1) ;
		}
	} else {
		gtk_tree_model_get (model, &iter,
		                    NS_URI_COLUMN, &ns_uri,
		                    XML_NS_COLUMN, &ns,
		                    -1) ;
		g_return_if_fail (ns_uri) ;
		mlview_xml_document_set_ns
		(PRIVATE (editor)->xml_doc,
		 PRIVATE (editor)->cur_xml_node, ns,
		 (xmlChar*)ns_uri, (xmlChar*)a_ns_prefix, TRUE) ;
	}
}

static void
xml_doc_node_namespace_added_cb (MlViewXMLDocument *a_this,
                                 xmlNode *a_node,
                                 xmlNs *a_ns,
                                 MlViewNSEditor *a_editor)
{
	g_return_if_fail (a_this
	                  && MLVIEW_IS_XML_DOCUMENT (a_this)
	                  && a_editor
	                  && MLVIEW_IS_NS_EDITOR (a_editor)) ;

	mlview_ns_editor_update_ns_added (a_editor, a_node, a_ns) ;
}


static void
xml_doc_node_namespace_changed_cb (MlViewXMLDocument *a_this,
                                   xmlNode *a_node,
                                   xmlNs *a_ns,
                                   MlViewNSEditor *a_editor)
{
	g_return_if_fail (a_this
	                  && MLVIEW_XML_DOCUMENT (a_this)
	                  && a_node
	                  && a_ns
	                  && a_editor
	                  && MLVIEW_NS_EDITOR (a_editor)) ;

	mlview_ns_editor_update_ns (a_editor, a_node, a_ns) ;
}

static void
xml_doc_node_namespace_removed_cb (MlViewXMLDocument *a_this,
                                   xmlNode *a_node,
                                   xmlNs *a_ns,
                                   MlViewNSEditor *a_editor)
{
	g_return_if_fail (a_this
	                  && MLVIEW_XML_DOCUMENT (a_this)
	                  && a_node
	                  && a_ns
	                  && a_editor
	                  && MLVIEW_NS_EDITOR (a_editor)) ;

	mlview_ns_editor_update_ns_removed (a_editor, a_node, a_ns) ;
}

/**************************
 *public functions
 ***************************/

/**
 *Standard type building/registering routine.
 */
guint
mlview_ns_editor_get_type (void)
{
	static guint type = 0 ;
	if (!type) {
		static const GTypeInfo type_info = {
		                                       sizeof (MlViewNSEditorClass),
		                                       NULL, /*base_init*/
		                                       NULL,/*base_finalize*/
		                                       (GClassInitFunc)mlview_ns_editor_class_init,
		                                       NULL, /*class_finalize*/
		                                       NULL, /*class_data*/
		                                       sizeof (MlViewNSEditor),
		                                       0, (GInstanceInitFunc)mlview_ns_editor_init
		                                   } ;
		type = g_type_register_static  (GTK_TYPE_VBOX,
		                                "MlViewNSEditor",
		                                &type_info, (GTypeFlags)0);
	}
	return type ;
}

/**
 *Creates a new #MlViewNSEditor widget.
 *@param a_context the application context.
 */
GtkWidget *
mlview_ns_editor_new (MlViewXMLDocument *a_doc)
{
	MlViewNSEditor *result = NULL ;
	enum MlViewStatus status = MLVIEW_OK ;

	result = (MlViewNSEditor*) g_object_new (MLVIEW_TYPE_NS_EDITOR, NULL) ;
	g_return_val_if_fail (result
	                      && GTK_IS_WIDGET (result), NULL) ;

	status = mlview_ns_editor_construct
	         (MLVIEW_NS_EDITOR (result),
	          _("namespace uris"),
	          _("namespace prefixes")) ;
	g_return_val_if_fail (status == MLVIEW_OK, NULL) ;
	status = mlview_ns_editor_set_xml_doc (result, a_doc) ;
	g_return_val_if_fail (status == MLVIEW_OK, NULL) ;
	return GTK_WIDGET (result) ;
}

/**
 *Sets the xml document this editor will work on.
 *@param a_this the current instance of #MlViewNSEditor
 *@param a_doc the xml document
 *@param MLVIEW_OK upon successfull completion, an error code
 *otherwise.
 */
enum MlViewStatus
mlview_ns_editor_set_xml_doc (MlViewNSEditor *a_this,
                              MlViewXMLDocument *a_doc)
{
	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_NS_EDITOR (a_this)
	                      && PRIVATE (a_this)
	                      && a_doc
	                      && MLVIEW_IS_XML_DOCUMENT (a_doc),
	                      MLVIEW_BAD_PARAM_ERROR) ;

	PRIVATE (a_this)->xml_doc = a_doc ;
	return MLVIEW_OK ;
}

/**
 *Gets the instance of GtkTreeModel hold by
 *the current instance of #MlViewNSEditor.
 *@param a_this the current instance of #MlViewNSEditor.
 *@return the GtkTreeModel.
 */
GtkTreeModel *
mlview_ns_editor_get_model (MlViewNSEditor *a_this)
{
	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_NS_EDITOR (a_this)
	                      && PRIVATE (a_this),
	                      NULL) ;

	return PRIVATE (a_this)->model ;
}

/**
 *Tests if a given row is the "add new ns row" or not.
 *@param a_this the current instance of #MlViewNSEditor.
 *@param a_iter an iterator to the row to test.
 *@return TRUE if the row is the "add new ns row", 
 *FALSE otherwise.
 */
gboolean
mlview_ns_editor_is_row_the_add_new_ns_row (MlViewNSEditor *a_this,
        GtkTreeIter *a_iter)
{
	gboolean is_add_new_ns_row = FALSE ;

	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_NS_EDITOR (a_this)
	                      && PRIVATE (a_this)
	                      && PRIVATE (a_this)->model,
	                      FALSE) ;

	gtk_tree_model_get (PRIVATE (a_this)->model, a_iter,
	                    IS_ADD_NEW_NS_COLUMN,
	                    &is_add_new_ns_row,
	                    -1) ;
	return is_add_new_ns_row ;
}

/**
 *Gets the row reference associated to
 *a given iterator.
 *@param a_this the current instance of #MlViewNSEditor
 *@param a_iter the iterator to consider.
 *@param a_create_if_not_exists. When set to TRUE, if no 
 *row reference is associated to a_iter, creates a new
 *row reference, associates it to the xmlNs pointed to by
 *a_iter and return this new row reference.
 *@return the row reference or NULL if no was not found or
 *if something bad happened.
 */
GtkTreeRowReference *
mlview_ns_editor_get_row_ref_from_iter (MlViewNSEditor *a_this,
                                        GtkTreeIter *a_iter,
                                        gboolean a_create_if_not_exists)
{
	xmlNs *ns = NULL ;
	GtkTreeModel *model = NULL ;
	gboolean exists = TRUE ;
	GtkTreeRowReference *row_ref = NULL, *result = NULL ;
	GtkTreePath *tree_path = NULL ;

	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_NS_EDITOR (a_this)
	                      && PRIVATE (a_this)
	                      && a_iter, NULL) ;

	model = mlview_ns_editor_get_model (a_this) ;
	g_return_val_if_fail (model, NULL) ;
	gtk_tree_model_get (model, a_iter,
	                    XML_NS_COLUMN, &ns, -1) ;
	if (!PRIVATE (a_this)->ns_row_hash) {
		exists = FALSE ;
	} else {
		row_ref = (GtkTreeRowReference*) g_hash_table_lookup
		          (PRIVATE (a_this)->ns_row_hash, ns) ;
		exists = (row_ref != NULL) ;
	}
	if (exists == TRUE || a_create_if_not_exists == FALSE) {
		result = row_ref ;
		row_ref = NULL ;
		goto cleanup ;
	}
	tree_path = gtk_tree_model_get_path (model, a_iter) ;
	g_return_val_if_fail (tree_path, NULL) ;
	row_ref = gtk_tree_row_reference_new (model, tree_path) ;
	if (!row_ref) {
		mlview_utils_trace_debug
		("gtk_tree_row_reference_new () failed") ;
		result = NULL ;
		goto cleanup ;
	}
	if (!PRIVATE (a_this)->ns_row_hash) {
		PRIVATE (a_this)->ns_row_hash = g_hash_table_new
		                                (g_direct_hash, g_direct_equal) ;
		if (!PRIVATE (a_this)->ns_row_hash) {
			mlview_utils_trace_debug
			("g_hash_table_new() failed") ;
			result = NULL ;
			goto cleanup ;
		}
	}
	g_hash_table_insert (PRIVATE (a_this)->ns_row_hash,
	                     ns, row_ref) ;
	result = row_ref ;
	row_ref = NULL ;
cleanup:
	if (tree_path) {
		gtk_tree_path_free (tree_path) ;
		tree_path = NULL ;
	}
	if (row_ref) {
		gtk_tree_row_reference_free (row_ref) ;
		row_ref = NULL ;
	}
	return result ;
}

/**
 *Gets the row reference associated to
 *a given namespace.
 *
 *@param a_this the current instance of #MlViewNSEditor
 *@param a_ns the namespace to consider.
 *@return the row reference or NULL if no was not found or
 *if something bad happened.
 */
GtkTreeRowReference *
mlview_ns_editor_get_row_ref_from_ns (MlViewNSEditor *a_this,
                                      xmlNs *a_ns)
{
	GtkTreeRowReference *row_ref = NULL ;

	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_NS_EDITOR (a_this)
	                      && PRIVATE (a_this), NULL) ;

	if (!PRIVATE (a_this)->ns_row_hash)
		return NULL ;

	row_ref = (GtkTreeRowReference*)g_hash_table_lookup
	          (PRIVATE (a_this)->ns_row_hash, a_ns) ;
	return row_ref ;
}

/**
 *Gets the currently selected visual node.
 *@param a_this the current instance of #MlViewNSEditor
 *@param a_iter out parameter. The returned iterator set
 *to the currently selected visual node. Note that this pointer
 *must be "allocated" and managed by the caller.
 *@return MLVIEW_OK upon successful completion, an error code
 *otherwise.
 */
enum MlViewStatus
mlview_ns_editor_get_cur_sel_start (MlViewNSEditor *a_this,
                                    GtkTreeIter *a_iter)
{
	GtkTreePath *tree_path = NULL ;

	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_NS_EDITOR (a_this)
	                      && PRIVATE (a_this)
	                      && PRIVATE (a_this)->cur_sel_start
	                      && PRIVATE (a_this)->model,
	                      MLVIEW_BAD_PARAM_ERROR) ;

	tree_path = gtk_tree_row_reference_get_path
	            (PRIVATE (a_this)->cur_sel_start) ;
	g_return_val_if_fail (tree_path, MLVIEW_ERROR) ;
	gtk_tree_model_get_iter (PRIVATE (a_this)->model,
	                         a_iter, tree_path) ;
	if (tree_path) {
		gtk_tree_path_free (tree_path) ;
		tree_path = NULL ;
	}
	return MLVIEW_OK ;
}

/**
 *Sets the editor's current selected row to
 *the row pointed to by a given tree iterator.
 *@param a_this the current instance of #MlViewNSEditor
 *@param a_iter the iterator to consider.
 *
 */
enum MlViewStatus
mlview_ns_editor_set_current_selected_row (MlViewNSEditor *a_this,
        GtkTreeIter *a_iter)
{
	GtkTreeRowReference *row_ref = NULL ;

	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_NS_EDITOR (a_this)
	                      && PRIVATE (a_this),
	                      MLVIEW_BAD_PARAM_ERROR) ;
	row_ref = mlview_ns_editor_get_row_ref_from_iter
	          (a_this, a_iter, TRUE) ;
	PRIVATE (a_this)->cur_sel_start = row_ref ;
	return MLVIEW_OK ;
}

/**
 *Visually Edits a namespace.
 *The namespace must have been added to
 *the xml document prior to calling this function.
 *@param a_this the current instance of #MlViewNSEditor
 *@param a_ns the namespace to edit
 *@param a_selectable
 *@return MLVIEW_OK upon succesful completion, an error code
 *otherwise
 */
enum MlViewStatus
mlview_ns_editor_edit_namespace (MlViewNSEditor * a_this,
                                 xmlNs * a_ns,
                                 gboolean a_editable)
{
	GtkTreeIter iter = {0}, iter2 = {0} ;
	gboolean is_ok = TRUE ;
	gboolean is_add_new_ns_row = FALSE ;
	GtkTreeModel *model = NULL ;
	GtkTreeRowReference *row_ref = NULL ;

	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_NS_EDITOR (a_this)
	                      && PRIVATE (a_this),
	                      MLVIEW_BAD_PARAM_ERROR) ;

	mlview_ns_editor_enable_node_alteration (a_this, FALSE) ;

	model = mlview_ns_editor_get_model (a_this) ;
	g_return_val_if_fail (model, MLVIEW_ERROR) ;
	is_ok = gtk_tree_model_get_iter_first (model, &iter) ;
	g_return_val_if_fail (is_ok == TRUE, MLVIEW_ERROR) ;
	/*go find the add new ns row*/
	while (is_ok == TRUE) {
		is_add_new_ns_row =
		    mlview_ns_editor_is_row_the_add_new_ns_row
		    (a_this, &iter) ;
		if (is_add_new_ns_row == TRUE)
			break ;
		is_ok = gtk_tree_model_iter_next (model, &iter) ;
	}
	gtk_list_store_insert_before (GTK_LIST_STORE (model),
	                              &iter2, &iter) ;
	gtk_list_store_set (GTK_LIST_STORE (model), &iter2,
	                    XML_NS_COLUMN, a_ns,
	                    IS_EDITABLE_COLUMN, a_editable,
	                    IS_ADD_NEW_NS_COLUMN, FALSE,
	                    NS_URI_COLUMN, a_ns->href,
	                    NS_PREFIX_COLUMN, a_ns->prefix,
	                    -1) ;
	/*
	 *create new ns->row_ref association for the newly
	 *edited ns.
	 */
	row_ref = mlview_ns_editor_get_row_ref_from_iter
	          (a_this, &iter2, TRUE/*create if not exist*/) ;
	g_return_val_if_fail (row_ref, MLVIEW_ERROR) ;
	mlview_ns_editor_select_ns (a_this, a_ns) ;

	mlview_ns_editor_enable_node_alteration (a_this, TRUE) ;

	return MLVIEW_OK ;
}

/**
 *Adds a new namespace the the namespace editor.
 *@param a_this the current instance of #MlViewNSEditor
 *@param a_prefix the namespace prefix.
 *@param a_uri the the namespace uri.
 *@param a_xml_node the node that holds the new namespace.
 */
xmlNs *
mlview_ns_editor_add_namespace (MlViewNSEditor * a_this,
                                gchar * a_prefix,
                                gchar * a_uri)
{
	xmlNs *ns = NULL ;

	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_NS_EDITOR (a_this)
	                      && PRIVATE (a_this)
	                      && PRIVATE (a_this)->xml_doc
	                      && a_prefix,
	                      NULL) ;

	ns = mlview_xml_document_create_ns
	     (PRIVATE (a_this)->xml_doc,
	      PRIVATE (a_this)->cur_xml_node,
	      a_uri, a_prefix, TRUE) ;
	return ns ;
}

/**
 *Edits the namespaces visible from a given xml node.
 *This method is basically what this widget is all about.
 *@param a_this the current instance of #MlViewNSEditor .
 *@param a_xml_node the xml node which namespaces are to be edited.
 *@return MLVIEW_OK upon succesful completion, an error code otherwise.
 */
enum MlViewStatus
mlview_ns_editor_edit_node_visible_namespaces (MlViewNSEditor * a_this,
        xmlNode * a_xml_node)
{
	xmlNode *node = NULL ;
	xmlNs *ns = NULL ;
	gboolean editable = FALSE ;

	g_return_val_if_fail (a_this
	                      && MLVIEW_NS_EDITOR (a_this)
	                      && a_xml_node,
	                      MLVIEW_BAD_PARAM_ERROR) ;

	mlview_ns_editor_enable_node_alteration (a_this,
	        FALSE) ;

	mlview_ns_editor_clear (a_this) ;
	node = a_xml_node ;
	while (node) {
		editable = (node == a_xml_node) ;
		/*
		 *loop over the namespaces 
		 *defined on the current node 
		 */
		for (ns = node->nsDef;
		        ns; ns = ns->next) {
			mlview_ns_editor_edit_namespace (a_this, ns,
			                                 editable) ;
		}
		node = node->parent ;
	}
	PRIVATE (a_this)->cur_xml_node = a_xml_node ;
	mlview_ns_editor_enable_node_alteration (a_this,
	        TRUE) ;
	return MLVIEW_OK ;
}

/**
 *Visually clears the rows of the editor.
 *@param a_this the current instance of
 *#MlViewNSEditor.
 *@return MLVIEW_OK upon successful completion,
 *an error code otherwise.
 */
enum MlViewStatus
mlview_ns_editor_clear (MlViewNSEditor *a_this)
{
	GtkTreeIter iter = {0} ;
	gboolean is_ok = TRUE ;
	xmlNs *cur_ns = NULL ;

	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_NS_EDITOR (a_this)
	                      && PRIVATE (a_this)
	                      && PRIVATE (a_this)->model,
	                      MLVIEW_BAD_PARAM_ERROR) ;
	while (1) {
		is_ok = gtk_tree_model_get_iter_first
		        (PRIVATE (a_this)->model, &iter) ;
		g_return_val_if_fail (is_ok == TRUE,
		                      MLVIEW_ERROR) ;
		if (mlview_ns_editor_is_row_the_add_new_ns_row
		        (a_this, &iter) == TRUE)
			break ;
		gtk_tree_model_get (PRIVATE (a_this)->model,
		                    &iter,
		                    XML_NS_COLUMN, &cur_ns,
		                    -1) ;
		if (cur_ns) {
			g_return_val_if_fail
			        (PRIVATE (a_this)->ns_row_hash,
			         MLVIEW_ERROR) ;
			g_hash_table_remove
			(PRIVATE (a_this)->ns_row_hash, cur_ns) ;
		}
		is_ok = gtk_list_store_remove
		        (GTK_LIST_STORE (PRIVATE (a_this)->model),
		         &iter) ;
		g_return_val_if_fail (is_ok == TRUE,
		                      MLVIEW_ERROR) ;
	}

	return MLVIEW_OK ;
}

/**
 *Removes a namespace declaration from the xml node
 *which namespace are currently being edited.
 *@param a_this the current instance of #MlViewNSEditor.
 *@param a_ns the namespace declaration to remove.
 *@return MLVIEW_OK upon successful completion, an
 *error code otherwise.
 */
enum MlViewStatus
mlview_ns_editor_remove_namespace (MlViewNSEditor * a_this,
                                   xmlNs * a_ns)
{
	g_return_val_if_fail (a_this
	                      && MLVIEW_NS_EDITOR (a_this)
	                      && PRIVATE (a_this)
	                      && PRIVATE (a_this)->xml_doc
	                      && PRIVATE (a_this)->cur_xml_node,
	                      MLVIEW_BAD_PARAM_ERROR) ;

	mlview_xml_document_remove_ns
	(PRIVATE (a_this)->xml_doc, a_ns,
	 PRIVATE (a_this)->cur_xml_node, TRUE) ;

	return MLVIEW_OK ;
}

/**
 *Updates the editor to reflect the addition of a new
 *row to a given xml node.
 *@param a_this the current instance of #MlViewNSEditor.
 *@param a_node the xml node the new namespace declaration belongs to.
 *@param a_ns the newly added ns declaration.
 *@return MLVIEW_OK upon succesful completion an error code
 *otherwise.
 */
enum MlViewStatus
mlview_ns_editor_update_ns_added (MlViewNSEditor *a_this,
                                  xmlNode *a_node,
                                  xmlNs *a_ns)
{
	GtkTreeRowReference *row_ref = NULL ;
	enum MlViewStatus status = MLVIEW_OK ;

	g_return_val_if_fail (a_this
	                      && MLVIEW_NS_EDITOR (a_this)
	                      && PRIVATE (a_this),
	                      MLVIEW_BAD_PARAM_ERROR) ;

	if (PRIVATE (a_this)->cur_xml_node != a_node)
		return MLVIEW_OK ;

	row_ref = mlview_ns_editor_get_row_ref_from_ns
	          (a_this, a_ns) ;
	if (row_ref) {
		return MLVIEW_OK ;
	}
	status = mlview_ns_editor_edit_namespace
	         (a_this, a_ns, FALSE) ;
	return status ;
}

/**
 *Updates the editor rows to reflect
 *a change in given ns declaration.
 *@param a_this the current instance of #MlViewNSEditor
 *@param a_node the xml node the ns declaration belongs to.
 *@param a_ns the ns declaration the row to be updated belongs to.
 *@return MLVIEW_OK upon successful completion, an error code otherwise.
 */
enum MlViewStatus
mlview_ns_editor_update_ns (MlViewNSEditor *a_this,
                            xmlNode *a_node,
                            xmlNs *a_ns)
{
	GtkTreeRowReference *row_ref = NULL ;
	GtkTreePath *tree_path = NULL ;
	GtkTreeIter iter = {0} ;
	GtkTreeModel *model = NULL ;
	gboolean is_ok = FALSE ;
	xmlNs *ns = NULL ;
	enum MlViewStatus status = MLVIEW_OK ;

	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_NS_EDITOR (a_this)
	                      && a_node
	                      && a_ns,
	                      MLVIEW_BAD_PARAM_ERROR) ;

	if (PRIVATE (a_this)->cur_xml_node != a_node) {
		return MLVIEW_OK ;
	}
	row_ref = mlview_ns_editor_get_row_ref_from_ns
	          (a_this, a_ns) ;
	g_return_val_if_fail (row_ref, MLVIEW_ERROR) ;
	model = mlview_ns_editor_get_model (a_this) ;
	g_return_val_if_fail (model, MLVIEW_ERROR) ;
	tree_path = gtk_tree_row_reference_get_path (row_ref) ;
	g_return_val_if_fail (tree_path, MLVIEW_ERROR) ;
	is_ok = gtk_tree_model_get_iter (model, &iter, tree_path) ;
	if (is_ok != TRUE) {
		mlview_utils_trace_debug
		("gtk_tree_model_get_iter() failed") ;
		status = MLVIEW_ERROR ;
		goto cleanup ;
	}
	gtk_tree_model_get (model, &iter,
	                    XML_NS_COLUMN, &ns,
	                    -1) ;
	if (!ns) {
		mlview_utils_trace_debug
		("argh!! this column should have an xmlNs* "
		 "associated") ;
		status = MLVIEW_ERROR ;
		goto cleanup ;
	}
	gtk_list_store_set (GTK_LIST_STORE (model), &iter,
	                    NS_URI_COLUMN, a_ns->href,
	                    NS_PREFIX_COLUMN, a_ns->prefix,
	                    -1) ;
	status = MLVIEW_OK ;

cleanup:
	if (tree_path) {
		gtk_tree_path_free (tree_path);
		tree_path = NULL ;
	}
	return status ;
}

/**
 *Updates the editor to reflect a the removal of an 
 *ns declaration.
 *@param a_this the current instance of #MlViewNSEditor
 *@param a_node the xml node the removed (unlinked) ns belonged to.
 *@param a_ns the ns declaration that has been removed (unlinked)
 *@return MLVIEW_OK upon successful completion, an error code otherwise.
 */
enum MlViewStatus
mlview_ns_editor_update_ns_removed (MlViewNSEditor * a_this,
                                    xmlNode *a_node,
                                    xmlNs * a_ns)
{
	GtkTreeRowReference *row_ref = NULL ;
	GtkTreeIter iter = {0} ;
	GtkTreePath *tree_path = NULL ;

	g_return_val_if_fail (a_this
	                      && MLVIEW_NS_EDITOR (a_this)
	                      && PRIVATE (a_this)
	                      && PRIVATE (a_this)->model
	                      && a_ns,
	                      MLVIEW_BAD_PARAM_ERROR) ;

	if (PRIVATE (a_this)->cur_xml_node != a_node) {
		return MLVIEW_OK ;
	}
	row_ref = mlview_ns_editor_get_row_ref_from_ns (a_this,
	          a_ns) ;
	g_return_val_if_fail (row_ref, MLVIEW_ERROR) ;
	tree_path = gtk_tree_row_reference_get_path (row_ref) ;
	g_return_val_if_fail (tree_path, MLVIEW_BAD_PARAM_ERROR) ;
	gtk_tree_model_get_iter (PRIVATE (a_this)->model,
	                         &iter, tree_path) ;
	gtk_list_store_remove (GTK_LIST_STORE
	                       (PRIVATE (a_this)->model),
	                       &iter) ;
	if (PRIVATE (a_this)->ns_row_hash) {
		g_hash_table_remove
		(PRIVATE (a_this)->ns_row_hash, a_ns) ;
		if (row_ref) {
			gtk_tree_row_reference_free (row_ref) ;
			row_ref = NULL ;
		}
	}
	if (tree_path) {
		gtk_tree_path_free (tree_path) ;
		tree_path = NULL ;
	}

	return MLVIEW_OK ;
}

/**
 *Connects the editor to the relevant signals emited
 *by the document model. Each relevant signal emited
 *will then trigger a callback defined by this function.
 *@param a_this the current instance of #MlViewNSEditor
 *@param a_doc the document object model to connect to.
 *@return MLVIEW_OK upon successful completion, an error
 *code otherwise.
 */
enum MlViewStatus
mlview_ns_editor_connect_to_doc (MlViewNSEditor *a_this,
                                 MlViewXMLDocument *a_doc)
{
	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_NS_EDITOR (a_this),
	                      MLVIEW_BAD_PARAM_ERROR) ;

	g_signal_connect (G_OBJECT (a_doc),
	                  "node-namespace-added",
	                  G_CALLBACK
	                  (xml_doc_node_namespace_added_cb),
	                  a_this) ;

	g_signal_connect (G_OBJECT (a_doc),
	                  "node-namespace-changed",
	                  G_CALLBACK
	                  (xml_doc_node_namespace_changed_cb),
	                  a_this) ;

	g_signal_connect (G_OBJECT (a_doc),
	                  "node-namespace-removed",
	                  G_CALLBACK
	                  (xml_doc_node_namespace_removed_cb),
	                  a_this) ;

	return MLVIEW_OK ;
}

/**
 *Disconnect from the document object model.
 *All the callbacks defined by mlview_ns_editor_connect_to_doc()
 *will be unset and therefore won't be called anymore.
 *@param a_this the current instance of #MlViewNSEditor.
 *@param a_doc the document object model to disconnect from.
 *@return MLVIEW_OK upon successful completion, an error code
 *otherwise.
 */
enum MlViewStatus
mlview_ns_editor_disconnect_from_doc (MlViewNSEditor *a_this,
                                      MlViewXMLDocument *a_doc)
{
	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_NS_EDITOR (a_this)
	                      && a_doc
	                      && MLVIEW_IS_XML_DOCUMENT (a_doc),
	                      MLVIEW_BAD_PARAM_ERROR) ;

	g_signal_handlers_disconnect_by_func
	(G_OBJECT (a_doc),
	 (void*) xml_doc_node_namespace_added_cb,
	 a_this) ;
	g_signal_handlers_disconnect_by_func
	(G_OBJECT (a_doc),
	 (void*) xml_doc_node_namespace_changed_cb,
	 a_this) ;
	g_signal_handlers_disconnect_by_func
	(G_OBJECT (a_doc),
	 (void*) xml_doc_node_namespace_removed_cb,
	 a_this) ;
	return MLVIEW_OK ;
}

enum MlViewStatus
mlview_ns_editor_enable_node_alteration (MlViewNSEditor *a_this,
        gboolean a_enable)
{
	g_return_val_if_fail (a_this && MLVIEW_IS_NS_EDITOR (a_this)
	                      && PRIVATE (a_this),
	                      MLVIEW_BAD_PARAM_ERROR) ;

	PRIVATE (a_this)->enable_node_alteration = a_enable ;
	return MLVIEW_OK ;
}
