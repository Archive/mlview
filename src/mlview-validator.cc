/* -*- Mode: C++; indent-tabs-mode: nil; c-basic-offset: 8-*- */

/*This file is part of GNU MlView
 *
 *GNU MlView is free software; 
 *you can redistribute it and/or modify it under the terms of 
 *the GNU General Public License as 
 *published by the Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope 
 *that it will be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */

#include <memory>
#include "mlview-validator.h"
#include "mlview-safe-ptr-utils.h"

using namespace std;

#define MESSAGE_LINE_LENGTH 55
namespace mlview
{

struct ValidatorPriv
{
	//automata and automata states
	xmlAutomata *automata ;

	//the current automata state
	xmlAutomataState* cur_state ;

	//the compiled form of an automata
	xmlRegexp *regexp ;

	ValidatorPriv ():
			automata (NULL)
	{}
}
;

static void
error_message_function (ValidationOutput *a_output,
                        const gchar *a_msg_format, ...)
{
	xmlErrorPtr xml_err = NULL;
	gchar *msg = NULL;

	THROW_IF_FAIL (a_output);

	xml_err = xmlGetLastError ();

	THROW_IF_FAIL (xml_err);

	if (xml_err->domain == XML_FROM_VALID ||
	        xml_err->domain == XML_FROM_RELAXNGV ||
	        xml_err->domain == XML_FROM_SCHEMASV) {
		msg = mlview_utils_normalize_text
		      (xml_err->message, "\n",
		       "\n", " ", MESSAGE_LINE_LENGTH);

		THROW_IF_FAIL (msg);
		a_output->append_message (msg, (xmlNode*)xml_err->node,
		                          xml_err->level) ;
		g_free (msg) ;
		msg = NULL ;
	}

	return;
}

Validator::Validator ()
{
	m_priv = new ValidatorPriv () ;
}

Validator::Validator (const Validator &a_validator)
{
	m_priv = new ValidatorPriv () ;
}

Validator::~Validator ()
{
	THROW_IF_FAIL (m_priv) ;

	if (m_priv->automata) {
		xmlFreeAutomata (m_priv->automata);
		m_priv->automata = NULL ;
	}
	if (m_priv->regexp) {
		xmlRegFreeRegexp (m_priv->regexp) ;
		m_priv->regexp = NULL ;
	}

	delete m_priv ;
	m_priv = NULL ;
}

enum MlViewStatus
Validator::compile_element_content_model_into_automata
(xmlElementContent* a_content_model)
{
	THROW_IF_FAIL (a_content_model) ;
	THROW_IF_FAIL (m_priv) ;

	enum MlViewStatus status =
	    compile_element_content_model_into_automata_real
	    (a_content_model) ;

	if (status == MLVIEW_OK && m_priv->automata) {
		if (m_priv->regexp) {
			xmlRegFreeRegexp (m_priv->regexp) ;
			m_priv->regexp = NULL ;
		}
		m_priv->regexp = xmlAutomataCompile (m_priv->automata) ;
	} else {
		if (status == MLVIEW_OK) {
			status = MLVIEW_ERROR ;
		}

	}
	return status ;
}


enum MlViewStatus
Validator::evaluate_element_name_seq_validity (const vector<UString> &a_name_seq,
        long &a_error_index)
{
	int result = 0, i=0;
	enum MlViewStatus status = MLVIEW_OK ;
	vector<UString>::const_iterator it ;

	THROW_IF_FAIL (m_priv) ;
	THROW_IF_FAIL (m_priv->regexp) ;
	SafePtr<xmlRegExecCtxt,
	XMLRegExecReference,
	XMLRegExecUnreference> regexec_ptr = xmlRegNewExecCtxt
	                                     (m_priv->regexp, NULL, NULL) ;

	for (it = a_name_seq.begin () ; it != a_name_seq.end () ; ++it) {
		result = xmlRegExecPushString (regexec_ptr,
		                               (xmlChar*)it->c_str (),
		                               NULL) ;
		if (result == 0) {
			status = MLVIEW_REACHED_NON_FINAL_STATE_ERROR ;
			a_error_index = i ;
		} else if (result < 0) {
			status = MLVIEW_SEQUENCE_NON_VALID_ERROR ;
			a_error_index = i;
		}
		if (status != MLVIEW_OK) {
			break ;
		}
		++i ;
	}
	return status ;
}

enum MlViewStatus
Validator::validate_with_dtd (MlViewXMLDocument *a_doc,
                              xmlDtdPtr a_dtd,
                              ValidationOutput **a_output)
{
	auto_ptr<ValidationOutput> validation_output_ptr ;
	xmlDocPtr xml_doc = NULL;
	xmlValidCtxt ctx = { 0 };
	int native_status = -1;
	enum MlViewStatus status = MLVIEW_OK ;

	THROW_IF_FAIL (a_doc && MLVIEW_IS_XML_DOCUMENT (a_doc));
	THROW_IF_FAIL (a_dtd);

	xml_doc = mlview_xml_document_get_native_document (a_doc);

	THROW_IF_FAIL (xml_doc);

	validation_output_ptr.reset (new ValidationOutput (a_doc)) ;

	ctx.userData = validation_output_ptr.get () ;
	ctx.error = (xmlValidityErrorFunc)error_message_function ;
	ctx.warning = (xmlValidityWarningFunc)error_message_function ;

	native_status = xmlValidateDtd (&ctx, xml_doc, a_dtd);
	if (native_status == 1)
		status = MLVIEW_OK;
	else
		status = MLVIEW_DOC_NOT_VALID_ERROR;

	if (a_output) {
		*a_output = validation_output_ptr.release () ;
	}
	return status ;
}

enum MlViewStatus
Validator::validate_with_rng (MlViewXMLDocument *a_doc,
                              xmlRelaxNGPtr a_rng,
                              ValidationOutput **a_output)
{
	auto_ptr<ValidationOutput> validation_output_ptr ;
	xmlDocPtr xml_doc = NULL;
	xmlRelaxNGValidCtxtPtr ctx = NULL;
	int native_status = -1;
	enum MlViewStatus status = MLVIEW_OK ;

	THROW_IF_FAIL (a_doc && MLVIEW_IS_XML_DOCUMENT (a_doc));
	THROW_IF_FAIL (a_rng);

	xml_doc = mlview_xml_document_get_native_document (a_doc);

	THROW_IF_FAIL (xml_doc);

	validation_output_ptr.reset (new ValidationOutput (a_doc));

	ctx = xmlRelaxNGNewValidCtxt (a_rng);

	if (!ctx)
		THROW ("xmlRelaxNGNewvalidCtxt() failed");

	xmlRelaxNGSetValidErrors
	(ctx,
	 (xmlRelaxNGValidityErrorFunc)error_message_function,
	 (xmlRelaxNGValidityErrorFunc)error_message_function,
	 validation_output_ptr.get ());

	native_status = xmlRelaxNGValidateDoc (ctx, xml_doc);

	if (native_status== 0) {
		status = MLVIEW_OK ;
	} else if (native_status > 0) {
		status = MLVIEW_DOC_NOT_VALID_ERROR ;
	} else {
		status = MLVIEW_ERROR ;
	}
	xmlRelaxNGFreeValidCtxt (ctx);

	if (a_output) {
		*a_output = validation_output_ptr.release () ;
	}
	return status ;
}

enum MlViewStatus
Validator::validate_with_xsd (MlViewXMLDocument *a_doc,
                              xmlSchemaPtr a_xsd,
                              ValidationOutput **a_output)
{
	auto_ptr<ValidationOutput> validation_output_ptr ;
	xmlDocPtr xml_doc = NULL;
	xmlSchemaValidCtxtPtr ctx = NULL;
	int native_result= -1;
	enum MlViewStatus status = MLVIEW_OK ;


	THROW_IF_FAIL (a_doc && MLVIEW_IS_XML_DOCUMENT (a_doc));
	THROW_IF_FAIL (a_xsd);

	xml_doc = mlview_xml_document_get_native_document (a_doc);

	THROW_IF_FAIL (xml_doc) ;

	validation_output_ptr.reset (new ValidationOutput (a_doc));

	ctx = xmlSchemaNewValidCtxt (a_xsd);

	THROW_IF_FAIL (ctx) ;

	xmlSchemaSetValidErrors
	(ctx,
	 (xmlSchemaValidityErrorFunc)error_message_function,
	 (xmlSchemaValidityErrorFunc)error_message_function,
	 validation_output_ptr.get ());

	native_result = xmlSchemaValidateDoc (ctx, xml_doc);

	if (native_result == 0)
		status = MLVIEW_OK ;
	else if (native_result > 0)
		status = MLVIEW_DOC_NOT_VALID_ERROR ;
	else
		status = MLVIEW_ERROR ;
	xmlSchemaFreeValidCtxt (ctx);

	if (a_output) {
		*a_output = validation_output_ptr.release () ;
	}

	return status;
}

enum MlViewStatus
Validator::validate_with_schema (MlViewXMLDocument *a_doc,
                                 MlViewSchema *a_schema,
                                 ValidationOutput **a_output)
{
	enum MlViewStatus status = MLVIEW_OK ;
	enum MlViewSchemaType schema_type = SCHEMA_TYPE_UNDEF ;
	gpointer native_schema = NULL ;

	THROW_IF_FAIL (a_doc && MLVIEW_IS_XML_DOCUMENT (a_doc));
	THROW_IF_FAIL (a_schema);

	status = mlview_schema_get_type (a_schema, &schema_type) ;
	THROW_IF_FAIL (status == MLVIEW_OK
	               && schema_type != SCHEMA_TYPE_UNDEF) ;

	status = mlview_schema_get_native_schema (a_schema, &native_schema) ;
	THROW_IF_FAIL (status == MLVIEW_OK && native_schema) ;

	switch (schema_type) {
	case SCHEMA_TYPE_DTD:
		return validate_with_dtd (a_doc,
		                          (xmlDtd*)native_schema,
		                          a_output);
	case SCHEMA_TYPE_RNG:
		return validate_with_rng (a_doc,
		                          (xmlRelaxNG*)native_schema,
		                          a_output) ;
	case SCHEMA_TYPE_XSD:
		return validate_with_xsd (a_doc,
		                          (xmlSchema*)native_schema,
		                          a_output);
	default:
		g_assert_not_reached () ;
	};

	return status ;
}

//*******************
//protected methods
//*******************

enum MlViewStatus
Validator::compile_element_content_model_into_automata_real
(xmlElementContent* a_model)
{
	enum MlViewStatus status = MLVIEW_OK ;
	if (!a_model) {
		return MLVIEW_ERROR ;
	}
	//instanciate a new finite states automata
	if (m_priv->automata) {
		xmlFreeAutomata (m_priv->automata) ;
	}
	m_priv->automata = xmlNewAutomata () ;
	m_priv->cur_state = xmlAutomataGetInitState (m_priv->automata) ;

	//****************************************************************
	//now, go visit the content model to build the automata accordingly
	//*****************************************************************

	switch (a_model->type) {
	case XML_ELEMENT_CONTENT_PCDATA:
		status = MLVIEW_BAD_CONTENT_MODEL_ERROR ;
		break ;

	case XML_ELEMENT_CONTENT_ELEMENT: {
			xmlChar *fullname=NULL, fn[50] ;
			fullname = xmlBuildQName (a_model->name, a_model->prefix,
			                          fn, 50) ;
			THROW_IF_FAIL (fullname) ;
			xmlAutomataState* old_state = m_priv->cur_state ;


			switch (a_model->ocur) {
			case XML_ELEMENT_CONTENT_ONCE:
				m_priv->cur_state =
				    xmlAutomataNewTransition
				    (m_priv->automata,
				     m_priv->cur_state,
				     NULL,
				     fullname,
				     NULL) ;
				break ;
			case XML_ELEMENT_CONTENT_OPT:
				m_priv->cur_state =
				    xmlAutomataNewTransition
				    (m_priv->automata,
				     m_priv->cur_state,
				     NULL,
				     fullname,
				     NULL) ;
				xmlAutomataNewEpsilon (m_priv->automata,
				                       old_state,
				                       m_priv->cur_state) ;
				break ;
			case XML_ELEMENT_CONTENT_PLUS:
				m_priv->cur_state =
				    xmlAutomataNewTransition
				    (m_priv->automata,
				     m_priv->cur_state,
				     NULL,
				     fullname,
				     NULL) ;
				m_priv->cur_state =
				    xmlAutomataNewTransition
				    (m_priv->automata,
				     m_priv->cur_state,
				     m_priv->cur_state,
				     fullname,
				     NULL) ;
				break ;
			case XML_ELEMENT_CONTENT_MULT:
				m_priv->cur_state =
				    xmlAutomataNewEpsilon
				    (m_priv->automata,
				     m_priv->cur_state,
				     NULL) ;
				m_priv->cur_state =
				    xmlAutomataNewTransition
				    (m_priv->automata,
				     m_priv->cur_state,
				     m_priv->cur_state,
				     fullname,
				     NULL) ;
				break ;
			}
		}
	case XML_ELEMENT_CONTENT_SEQ: {
			xmlAutomataState *old_state=NULL,
			                            *old_end=NULL ;
			xmlElementContentOccur occur ;

			//**********************************
			//walk through the content model sequence
			//and build the sub automata that matches
			//that sequence.
			//*********************************

			old_state = m_priv->cur_state ;
			occur = a_model->ocur ;

			//only a sequence of a cardinality of 'ONCE'
			//doesn't generate an epsilon transition ...
			if (occur != XML_ELEMENT_CONTENT_ONCE) {
				m_priv->cur_state = xmlAutomataNewEpsilon
				                    (m_priv->automata,
				                     m_priv->cur_state,
				                     NULL) ;
				old_state = m_priv->cur_state ;
			}
			xmlElementContent *cur_model = a_model;
			//if the content model is a sequence series where
			//the leftmost parts has a cardinality of ONCE,
			//recursively compile the matching automata
			do {
				compile_element_content_model_into_automata_real
				(cur_model->c1) ;
				cur_model = cur_model->c2 ;
			} while ((cur_model->type == XML_ELEMENT_CONTENT_SEQ)
			         && (cur_model->ocur == XML_ELEMENT_CONTENT_ONCE));
			//now that the content model is all but a sequence series
			//where the leftmost part has a cardinality of ONCE,
			//we are sure there will be a e-transition
			//(epsilon transition) between the current state and
			//the next state.
			compile_element_content_model_into_automata_real
			(cur_model) ;
			old_end = m_priv->cur_state ;
			m_priv->cur_state = xmlAutomataNewEpsilon
			                    (m_priv->automata, m_priv->cur_state, NULL) ;
			switch (occur) {
			case XML_ELEMENT_CONTENT_ONCE:
				break;
			case XML_ELEMENT_CONTENT_OPT:
				xmlAutomataNewEpsilon
				(m_priv->automata, old_state,
				 m_priv->cur_state) ;
				break ;
			case XML_ELEMENT_CONTENT_MULT:
				xmlAutomataNewEpsilon
				(m_priv->automata, old_state,
				 m_priv->cur_state) ;

				xmlAutomataNewEpsilon
				(m_priv->automata,
				 old_end,
				 old_state) ;
				break ;
			case XML_ELEMENT_CONTENT_PLUS:
				xmlAutomataNewEpsilon
				(m_priv->automata,
				 old_end, old_state) ;
				break ;
			}
			break ;

		}
	case XML_ELEMENT_CONTENT_OR: {
			xmlAutomataState *old_state= NULL,
			                             *old_end= NULL;
			xmlElementContentOccur occur = a_model->ocur ;
			if (  (occur == XML_ELEMENT_CONTENT_PLUS)
			        ||(occur == XML_ELEMENT_CONTENT_MULT)) {
				m_priv->cur_state = xmlAutomataNewEpsilon
				                    (m_priv->automata,
				                     m_priv->cur_state,
				                     NULL) ;
			}
			old_state = m_priv->cur_state ;
			old_end = xmlAutomataNewState (m_priv->automata) ;


			xmlElementContent *cur_model = a_model ;
			do {
				m_priv->cur_state = old_state ;
				compile_element_content_model_into_automata_real
				(cur_model->c1) ;
				cur_model = cur_model->c2 ;
			} while ((a_model->type == XML_ELEMENT_CONTENT_OR)
			         && (a_model->ocur == XML_ELEMENT_CONTENT_ONCE));
			m_priv->cur_state = old_state ;
			compile_element_content_model_into_automata_real
			(cur_model) ;
			xmlAutomataNewEpsilon (m_priv->automata,
			                       m_priv->cur_state,
			                       old_end) ;
			m_priv->cur_state = xmlAutomataNewEpsilon
			                    (m_priv->automata, old_end, NULL) ;
			switch (occur) {
			case XML_ELEMENT_CONTENT_ONCE:
				break;
			case XML_ELEMENT_CONTENT_OPT:
				xmlAutomataNewEpsilon
				(m_priv->automata,
				 old_state, m_priv->cur_state) ;
				break ;

			case XML_ELEMENT_CONTENT_MULT:
				xmlAutomataNewEpsilon
				(m_priv->automata,
				 old_state, m_priv->cur_state) ;
				xmlAutomataNewEpsilon
				(m_priv->automata,
				 old_end, old_state) ;

				break ;
			case XML_ELEMENT_CONTENT_PLUS:
				xmlAutomataNewEpsilon (m_priv->automata,
				                       old_end,
				                       old_state) ;
				break ;
			}

		}
		break ;
	default:
		g_assert_not_reached () ;
	}
	return status ;
}

enum MlViewStatus
Validator::load_a_dtd (const UString &a_dtd_path, xmlDtd**a_dtd)
{
	MlViewExtSubsDef subset_def ;

	subset_def.system_id = const_cast<gchar*>(a_dtd_path.c_str ()) ;
	return MLVIEW_OK ;
	//TODO: finish this

}

}//end namespace mlview
