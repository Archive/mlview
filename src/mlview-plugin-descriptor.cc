/* -*- Mode: C++; indent-tabs-mode: true; c-basic-offset: 4-*- */

/*This file is part of GNU MlView
 *
 *GNU MlView is free software; 
 *you can redistribute it and/or modify it under the terms of 
 *the GNU General Public License as published by 
 *the Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope that it will be useful, 
 *but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General 
 *Public License along with MlView; 
 *see the file COPYING. If not, write to the 
 *Free Software Foundation, Inc., 59 
 *Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright informations.
 */

#include <string.h>
#include <stdio.h>

#include <libxml/xmlreader.h>
#include "mlview-plugin-descriptor.h"
#include "mlview-exception.h"
#include "mlview-utils.h"
#include "mlview-safe-ptr-utils.h"

using namespace std ;
#define BEGIN_NAMESPACE_MLVIEW namespace mlview {
#define END_NAMESPACE_MLVIEW }

BEGIN_NAMESPACE_MLVIEW

struct PluginDescriptorPriv {
	UString file ;
	UString name ;
	UString description ;
	UString load_hook_function_name ;
	UString unload_hook_function_name ;

	PluginDescriptorPriv () {}

	PluginDescriptorPriv (const PluginDescriptorPriv &a_p):
		file (a_p.file),
		name (a_p.name),
		description (a_p.description),
		load_hook_function_name (a_p.load_hook_function_name),
		unload_hook_function_name (a_p.unload_hook_function_name)
	{
	}

	PluginDescriptorPriv& operator= (const PluginDescriptorPriv &a_p)
	{
		if (this == &a_p)
			return *this ;
		file = a_p.file ;
		name = a_p.name ;
		description = a_p.description ;
		load_hook_function_name = a_p.load_hook_function_name ;
		unload_hook_function_name = a_p.unload_hook_function_name ;
		return *this ;
	}
};//end PluginDescriptorPriv

#define NO_NAME 2
#define NO_VALUE 4
#define NO_TYPE 8
#define NO_DEPTH 16

static int
get_current_node (xmlTextReader *a_reader,
                  UString &a_name, UString &a_value,
                  int &a_type, int &a_depth)
{
	int depth = -1, type = -1;

	THROW_IF_FAIL (a_reader) ;

	depth = xmlTextReaderDepth (a_reader);

	const gchar* name = (const gchar*) xmlTextReaderConstName (a_reader);
	const gchar* value = (const gchar*) xmlTextReaderConstValue (a_reader);
	type = xmlTextReaderNodeType (a_reader);

	if (depth == -1 || type == -1)
		return -1;

	a_name = name ;
	a_value = value ;
	a_depth = depth ;
	a_type = type ;

	return 1;
}

static int
check_current_node (xmlTextReader *a_reader,
                    const UString &a_name, const UString &a_value,
                    int a_type, int a_depth, int a_mask)
{
	int ret = -1, depth = -1, type = -1;
	UString name, value ;

	THROW_IF_FAIL (a_reader);

	ret = get_current_node (a_reader, name, value, type, depth);

	if (ret != 1)
		return ret;

	if (
			(((a_mask & NO_NAME) != 0) || ((name == "" && a_name == "")
				|| ((name == a_name) && name != "")))
			&&
	        (((a_mask & NO_VALUE) != 0) || ((value == "" && a_value == "")
				|| ((value == a_value) && name != "")))
			&&
	        ((a_mask & NO_TYPE) != 0 || a_type == type)
			&&
	        ((a_mask & NO_DEPTH) != 0 || a_depth == depth)
	   )
		return 1;

	return -1;
}


static void
skip_significant_white_spaces (xmlTextReaderPtr a_reader)
{
	int ret = -1;

	while (check_current_node (a_reader, NULL, NULL, 14, 0,
	                           NO_NAME | NO_VALUE | NO_DEPTH) == 1) {
		ret = xmlTextReaderRead (a_reader);

		if (ret != 1)
			return;
	}
}

static gint
read_text_in_element_simple (xmlTextReader *a_reader,
                             const UString &a_element_name,
                             gint a_depth,
                             UString &a_value)
{
	gint ret = 0;

	THROW_IF_FAIL (a_reader);

	//space
	skip_significant_white_spaces (a_reader);

	//<ELEMENT_NAME>
	ret = check_current_node (a_reader, a_element_name, NULL,
	                          XML_READER_TYPE_ELEMENT, a_depth, 0);

	if (ret != 1)
		return -1;

	//space
	skip_significant_white_spaces (a_reader);

	ret = xmlTextReaderRead (a_reader);

	if (ret != 1)
		return -1;

	//#text
	ret = check_current_node (a_reader, NULL, NULL, XML_READER_TYPE_TEXT,
	                          a_depth + 1, NO_NAME | NO_VALUE);
	if (ret != 1)
		return -1;

	UString name, value ;
	int type, depth ;
	ret = get_current_node (a_reader, name, value, type, depth);

	if (ret != 1)
		return -1;

	//=> value
	a_value = value ;
	ret = xmlTextReaderRead (a_reader);

	if (ret != 1)
		return -1;

	//</ELEMENT_NAME>
	ret = check_current_node (a_reader, a_element_name, NULL,
	                          XML_READER_TYPE_END_ELEMENT, a_depth, 0);
	if (ret != 1)
		return -1;
	return 1;
}

PluginDescriptor::~PluginDescriptor ()
{
	if (m_priv) {
		delete m_priv ;
		m_priv = NULL ;
	} else {
		THROW ("Is a double delete happening here or what ?") ;
	}
}

PluginDescriptor::PluginDescriptor (const UString &a_file_name)
{
	m_priv = new PluginDescriptorPriv () ;
	THROW_IF_FAIL (m_priv) ;

	int ret = 0;

	XMLTextReaderSafePtr reader = xmlReaderForFile
					(a_file_name.c_str (), NULL, XML_PARSE_NONET);
	THROW_IF_FAIL (reader) ;

	ret = xmlTextReaderRead (reader);
	THROW_IF_FAIL (ret == 1) ;

	ret = check_current_node (reader, "plugin", NULL,
	                          XML_READER_TYPE_ELEMENT, 0, 0);
	THROW_IF_FAIL (ret == 1) ;

	ret = xmlTextReaderRead (reader);

	THROW_IF_FAIL (ret == 1) ;

	UString val ;
	ret = read_text_in_element_simple (reader, "file", 1, val);
	THROW_IF_FAIL (ret == 1) ;

	m_priv->file = val;
	ret = xmlTextReaderRead (reader);
	THROW_IF_FAIL (ret == 1) ;

	ret = read_text_in_element_simple (reader, "name", 1, val);
	THROW_IF_FAIL (ret == 1) ;

	m_priv->name = val;
	ret = xmlTextReaderRead (reader);
	THROW_IF_FAIL (ret == 1) ;

	ret = read_text_in_element_simple (reader, "description", 1, val);
	THROW_IF_FAIL (ret == 1) ;

	m_priv->description = val;

	ret = xmlTextReaderRead (reader);
	THROW_IF_FAIL (ret == 1) ;

	ret = read_text_in_element_simple (reader, "load", 1, val) ;
	THROW_IF_FAIL (ret == 1) ;

	m_priv->load_hook_function_name = val;
	THROW_IF_FAIL (ret == 1) ;

	ret = xmlTextReaderRead (reader);
	THROW_IF_FAIL (ret == 1) ;

	ret = read_text_in_element_simple (reader, "unload", 1, val);
	THROW_IF_FAIL (ret == 1) ;

	m_priv->unload_hook_function_name = val;

	ret = xmlTextReaderRead (reader);
	THROW_IF_FAIL (ret == 1) ;

	skip_significant_white_spaces (reader);

	ret = check_current_node (reader, "plugin", NULL,
	                          XML_READER_TYPE_END_ELEMENT, 0, 0);

	THROW_IF_FAIL (ret == 1) ;

	return;
}

PluginDescriptor::PluginDescriptor (const PluginDescriptor &an_other)
{
	m_priv = new PluginDescriptorPriv ;
	THROW_IF_FAIL (m_priv) ;

	*m_priv = *an_other.m_priv ;
}

PluginDescriptor&
PluginDescriptor::operator= (const PluginDescriptor &a_plugin)
{
	if (this == &a_plugin)
		return *this ;
	*m_priv = *a_plugin.m_priv ;
	return *this ;
}

const UString&
PluginDescriptor::get_plugin_file_path () const
{
	THROW_IF_FAIL (m_priv) ;
	return m_priv->file ;
}

const UString&
PluginDescriptor::get_load_hook_function_name () const
{
	THROW_IF_FAIL (m_priv) ;
	return m_priv->load_hook_function_name ;
}

const UString&
PluginDescriptor::get_unload_hook_function_name () const
{
	THROW_IF_FAIL (m_priv) ;
	return m_priv->unload_hook_function_name ;
}

const UString&
PluginDescriptor::get_plugin_name () const
{
	THROW_IF_FAIL (m_priv) ;
	return m_priv->name ;
}

END_NAMESPACE_MLVIEW
