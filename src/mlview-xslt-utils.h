/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 8-*- */

/*
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute it 
 *and/or modify it under the terms of 
 *the GNU General Public License as published 
 *by the Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *MlView is distributed in the hope that it will be useful, 
 *but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. If not, write to the 
 *Free Software Foundation, Inc.,
 *59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */

#ifndef __MLVIEW_XSLT_UTILS_H__
#define __MLVIEW_XSLT_UTILS_H__

#include <glib-object.h>
#include "mlview-xml-document.h"
#include "mlview-editor.h"

namespace mlview
{
/* xslt namespace uri */
#define XSLT_NAMESPACE_URI "http://www.w3.org/1999/XSL/Transform"

/* select xslt form response */
#define MLVIEW_XSLT_UTILS_RESPONSE_BROWSE 3

gboolean xslt_utils_is_xslt_doc (MlViewXMLDocument* a_mlv_xml_doc) ;

MlViewXMLDocument *_xslt_utils_select_xsl_doc  (Editor *a_this);

MlViewXMLDocument *xslt_utils_transform_document (MlViewXMLDocument *src_doc,
        MlViewXMLDocument *xsl_doc);

}//namespace mlview

#endif /* __mlview_xslt_utils_XSLT_UTILS_H__ */

