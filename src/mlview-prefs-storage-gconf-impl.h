/* -*- Mode: C++; indent-tabs-mode: true; c-basic-offset: 4 -*- */
/*
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute 
 *it and/or modify it under the terms of 
 *the GNU General Public License as published by the 
 *Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYING file for copyright information.
 */
#ifndef MLVIEW_PREFS_STORAGE_GCONF_IMPL_H
#define MLVIEW_PREFS_STORAGE_GCONF_IMPL_H

#include "mlview-prefs-storage-manager.h"

#define MLVIEW_PREFS_STORAGE_BACKEND_GCONF "GCONF"

namespace mlview
{
struct PrefsStorageGConfImplPriv;

///
/// GConf implementation of the storage manager.
///
/// \see PrefsStorageManager
///
class PrefsStorageGConfImpl : public PrefsStorageManager
{
    friend struct PrefsStorageGConfImplPriv;
    PrefsStorageGConfImplPriv *m_priv;

	//forbid copy/assignation
	PrefsStorageGConfImpl (PrefsStorageGConfImpl const&) ;
	PrefsStorageGConfImpl& operator= (PrefsStorageGConfImpl const&) ;

public:

    PrefsStorageGConfImpl ();
    virtual ~PrefsStorageGConfImpl ();

    void set_int_value (const UString& key, int value) ;
    int  get_int_value (const UString& key) ;
    int  get_default_int_value (const UString& key) ;

    void set_string_value (const UString& key,
			   const UString& value) ;
    UString get_string_value (const UString& key) ;
    UString get_default_string_value (const UString& key) ;

    void set_bool_value (const UString& key,
			 bool value) ;
    bool get_bool_value (const UString& key) ;
    bool get_default_bool_value (const UString& key) ;
};

} // namespace mlview

#endif
