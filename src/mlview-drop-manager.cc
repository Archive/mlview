/* -*- Mode: C++; indent-tabs-mode: nil; c-basic-offset: 4-*- */

/*
 *This file is part of MlView
 *
 *MlView is free software; you can redistribute it and/or 
 *modify it under the terms of 
 *the GNU General Public License as published 
 *by the Free Software Foundation; either version 2
 *or (at your option) any later version.
 *
 *MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView.
 *see the file COPYING. 
 *If not, write to the 
 *Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */

#include <stdio.h>
#include <gnome.h>
#include <libgnomevfs/gnome-vfs-mime-utils.h>
#include "config.h"
#include "mlview-editor.h"
#include "mlview-app.h"
#include "mlview-app-context.h"
#include "mlview-drop-manager.h"
/*
 * Allowed drag types
 */
static const GtkTargetEntry drag_types [] =
    {
        {
            (gchar*)"text/uri-list", 0, 1337
        },
    };
static gint n_drag_types = sizeof (drag_types) / sizeof (drag_types [0]);

/*
 * Viewable mime types
 */
static const gchar * allowed_mime_types[] =
    {
        "text/xml",
        "text/html",
        "text/x-xslt",
        "application/xml",
    };
static gint n_allowed_mime_types = sizeof (allowed_mime_types) / sizeof (allowed_mime_types[0]);

/*
 * Simple mime-type filter
 */
static gboolean
is_mime_type_readable (const gchar * mime_type)
{
	int i = 0;

	if (mime_type == NULL)
		return FALSE;

	for (i = 0; i < n_allowed_mime_types; i++) {
		if (mime_type
		        && allowed_mime_types[i]
		        && (strcmp (mime_type, allowed_mime_types[i]) == 0))
			return TRUE;
	}

	return FALSE;
}

/*
 * Handler used when an uri-list is dropped
 */
static void
drag_data_received_handler (GtkWidget *widget, GdkDragContext *context,
                            gint x, gint y, GtkSelectionData *selection_data,
                            guint info, guint time, gpointer data)
{
	GList *uri_list = NULL;
	GList *p = NULL;
	gchar * vfs_uri = NULL;
	gchar * mime_type = NULL;
	mlview::Editor* editor = NULL;

	uri_list = gnome_vfs_uri_list_parse ((const gchar *) selection_data->data);
	p = uri_list;

	mlview::AppContext *app_context = mlview::AppContext::get_instance () ;
	THROW_IF_FAIL (app_context) ;
	if (p != NULL)
		editor = (mlview::Editor*) app_context->get_element ("MlViewEditor");

	while (p != NULL) {
		vfs_uri = gnome_vfs_uri_to_string
		          ((const GnomeVFSURI*)(p->data), GNOME_VFS_URI_HIDE_NONE);
		mime_type = gnome_vfs_get_mime_type (vfs_uri);

		g_warning ("Loading document with mime-type '%s'", mime_type);

		if (is_mime_type_readable (mime_type) == TRUE) {
			editor->load_xml_file (vfs_uri, TRUE);
		}
		p= g_list_next (p);
	}
	gnome_vfs_uri_list_free (uri_list);
}

/******************
 * PUBLIC METHODS *
 ******************/

/*
 * Register the widget as a drop target
 */
void
mlview_drop_manager_register_target (GtkWidget *widget)
{
	gtk_drag_dest_set (GTK_WIDGET (widget),
	                   (GtkDestDefaults) (GTK_DEST_DEFAULT_MOTION
										  | GTK_DEST_DEFAULT_HIGHLIGHT
										  | GTK_DEST_DEFAULT_DROP),
	                   drag_types, n_drag_types,
	                   GDK_ACTION_COPY);

	g_signal_connect (G_OBJECT (widget), "drag_data_received",
	                  G_CALLBACK (drag_data_received_handler),
	                  NULL);
}
