/* -*- C++ -*-; indent-tabs-mode:nil; c-basic-offset: 4-*- */
/*
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute 
 *it and/or modify it under the terms of 
 *the GNU General Public License as published by the 
 *Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYING file for copyright information.
 */
#ifndef __MLVIEW_USTRING_H__
#define __MLVIEW_USTRING_H__

#include "glibmm.h"

namespace mlview
{
class UString: public Glib::ustring
{

public:

	UString () ;
	UString (const char *a_cstr) ;
	UString (UString const &an_other_string) ;
	UString (const Glib::ustring &an_other_string) ;
	virtual ~UString () ;
	UString& operator= (const char *a_cstr) ;
	UString& operator= (UString const &a_cstr) ;
	operator const gchar* () const ;
	operator const guchar* () const ;
} ;
}//namespace mlview
#endif //__MLVIEW_USTRING_H__

