/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 8-*- */

/*
 *This file is part of MlView
 *
 *MlView is free software; you can redistribute 
 *it and/or modify it under the terms of 
 *the GNU General Public License as published 
 *by the Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *MlView is distributed in the hope that it 
 *will be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. If not, write to the 
 *Free Software Foundation, Inc., 59 Temple Place 
 *- Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */

#ifndef __MLVIEW_FILE_DESCRIPTOR_H__
#define __MLVIEW_FILE_DESCRIPTOR_H__

#include <sys/types.h>
#include <glib.h>
/*#include <sys/stat.h>
  #include <fcntl.h>*/

G_BEGIN_DECLS

/**
 *@file
 *The declarations of #MlViewFileDescriptor class.
 */
typedef struct _MlViewFileDescriptor MlViewFileDescriptor;
typedef struct _MlViewFileDescriptorPrivate MlViewFileDescriptorPrivate;

/**
 *An abstraction of a file.
 *
 *This class is an encapsulation (at least for future portability issues)
 *of a file handle. All the access to file system (either local or remote)
 *should be made via the apis exposed by this object. The api is far 
 *from being complete but MlView uses it to have infos about the local files.
 */
struct _MlViewFileDescriptor
{
	MlViewFileDescriptorPrivate *priv;
};

MlViewFileDescriptor *mlview_file_descriptor_new (const gchar *a_file_path);

void mlview_file_descriptor_destroy (MlViewFileDescriptor * a_file_desc);

gchar *mlview_file_descriptor_get_uri (const MlViewFileDescriptor *a_file_desc);

gchar *mlview_file_descriptor_get_file_path (const MlViewFileDescriptor *a_file_desc);

void mlview_file_descriptor_set_file_path (MlViewFileDescriptor * a_file_desc,
        const gchar * file_path);

void mlview_file_descriptor_update_modified_time (MlViewFileDescriptor * a_file_desc);

gint mlview_file_descriptor_is_modified (const MlViewFileDescriptor * a_file_desc,
        gboolean * a_is_modified);

gint mlview_file_descriptor_is_regular_file (const MlViewFileDescriptor * a_file_desc,
        gboolean * a_is_reg);

gint mlview_file_descriptor_is_local (MlViewFileDescriptor * a_file_desc,
                                      gboolean * a_is_local);

gchar * mlview_file_descriptor_get_mime_type (MlViewFileDescriptor * a_file_desc);

G_END_DECLS

#endif
