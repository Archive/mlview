/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 8-*- */

/*
 *This file is part of MlView
 *
 *MlView is free software; you can redistribute it and/or 
 *modify it under the terms of 
 *the GNU General Public License as published 
 *by the Free Software Foundation; either version 2
 *or (at your option) any later version.
 *
 *MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView.
 *see the file COPYING. 
 *If not, write to the 
 *Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */
#ifndef __MLVIEW_PING_DBC_H__
#define __MLVIEW_PING_DBC_H__

#include "mlview-utils.h"

G_BEGIN_DECLS

#ifdef MLVIEW_WITH_DBUS

#define MLVIEW_TYPE_PING_DBC (mlview_ping_dbc_get_type ())
#define MLVIEW_PING_DBC(widget) (G_TYPE_CHECK_INSTANCE_CAST ((widget), MLVIEW_TYPE_PING_DBC, MlViewPingDBC))
#define MLVIEW_PING_DBC_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), MLVIEW_TYPE_PING_DBC, MlViewPingDBCClass))
#define MLVIEW_IS_PING_DBC(widget) (G_TYPE_CHECK_INSTANCE_TYPE ((widget), MLVIEW_TYPE_PING_DBC))
#define MLVIEW_IS_PING_DBC_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), MLVIEW_TYPE_PING_DBC))

typedef struct _MlViewPingDBC MlViewPingDBC ;
typedef struct _MlViewPingDBCPriv MlViewPingDBCPriv ;
typedef struct _MlViewPingDBCClass MlViewPingDBCClass ;

#define PATH_ORG_MLVIEW_PING_OBJECT "/org/mlview/PingObject"

#define INTERFACE_ORG_MLVIEW_PING_OBJECT_IFACE "org.mlview.PingObjectIface"

struct _MlViewPingDBC
{
	GObject parent_object ;
	MlViewPingDBCPriv *priv ;
} ;

struct _MlViewPingDBCClass
{
	GObjectClass parent_class ;
} ;

GType mlview_ping_dbc_get_type (void ) ;

MlViewPingDBC * mlview_ping_dbc_new (void) ;

enum MlViewStatus mlview_ping_dbc_ping (MlViewPingDBC *a_this,
                                        const gchar *a_service_name) ;

enum MlViewStatus mlview_ping_dbc_list_active_services (MlViewPingDBC *a_this,
        gchar ***a_active_services,
        guint *a_nb_of_services) ;

enum MlViewStatus mlview_ping_dbc_free_list_of_service_names (gchar **a_list,
        guint a_list_len) ;

enum MlViewStatus mlview_ping_dbc_close_application (MlViewPingDBC *a_this,
        const gchar *a_service_name) ;

#endif /*MLVIEW_WITH_DBUS*/

G_END_DECLS

#endif /*__MLVIEW_PING_DBC_H__*/
