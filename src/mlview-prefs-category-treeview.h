/* -*- Mode: C++; indent-tabs-mode: true; c-basic-offset: 4 -*- */
/*
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute 
 *it and/or modify it under the terms of 
 *the GNU General Public License as published by the 
 *Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYING file for copyright information.
 */
#ifndef MLVIEW_PREFS_CATEGORY_TREEVIEW_H
#define MLVIEW_PREFS_CATEGORY_TREEVIEW_H

#include <gtkmm.h>
#include "mlview-app-context.h"
#include "mlview-prefs-storage-manager.h"
#include "mlview-prefs-category.h"


namespace mlview
{

struct PrefsCategoryTreeviewPriv;

///
/// This class is the category handling treeview-specific preferences.
///
class PrefsCategoryTreeview : public PrefsCategory
{
    friend struct PrefsCategoryTreeviewPriv;
    PrefsCategoryTreeviewPriv *m_priv;

	//forbid copy/assignation
	PrefsCategoryTreeview (PrefsCategoryTreeview const &) ;
	PrefsCategoryTreeview& operator= (PrefsCategoryTreeview const &) ;

public:
  ///
  /// Constructor for type PrefsCategoryTreeview
  ///
  /// \param manager the storage manager to use to access preferences
  ///
  PrefsCategoryTreeview (PrefsStorageManager *manager);
  ///
  /// Destructor
  ///
  virtual ~PrefsCategoryTreeview ();

  ///
  /// Get the tree expansion depth property default value
  ///
  /// \return int the tree expansion depth default value
  ///
  int  get_default_tree_expansion_depth_default ();
  ///
  /// Get the tree expansion depth property value
  ///
  /// \return int the tree expansion depth value
  ///
  int  get_default_tree_expansion_depth ();
  ///
  /// Set the tree expansion depth property value
  ///
  /// \param depth the new tree expansion depth value
  ///
  void set_default_tree_expansion_depth (int depth);

  ///
  /// Whether treeview should use a completion box or not (default value)
  ///
  /// \return TRUE if completion box should be used. FALSE otherwise.
  ///
  bool enable_completion_box_default ();
  ///
  /// Whether treeview should use a completion box or not
  ///
  /// \return TRUE if completion box should be used. FALSE otherwise.
  ///
  bool enable_completion_box ();
  ///
  /// Set the flag that enable/disable the use of a completion box.
  ///
  /// \param flag TRUE to enable completion box, FALSE to disable it
  ///
  void set_enable_completion_box (bool flag);

  ///
  /// Get the fontname property default value
  ///
  /// \return a string representation of the font
  ///
  Glib::ustring get_default_font_name ();
  ///
  /// Get the fontname property value
  ///
  /// \return a string representation of the font
  ///
  Glib::ustring get_font_name ();
  ///
  /// Set the fontname property value
  ///
  /// \param fontname a string representation of the font
  ///
  void set_font_name (const Glib::ustring& fontname);

  ///
  /// Get the default value of a color property for a given type
  ///
  /// \param type_id the type for which we search the color
  ///
  /// \return a string representation of the default color
  ///
  Glib::ustring get_default_color_for_type (const Glib::ustring& type_id);
  ///
  /// Get the default value of a color property for a given type
  ///
  /// \param colour an mlview::TreeEditorsNodeColour colour
  ///
  /// \return a string representation of the default color
  ///
    Glib::ustring get_default_color_for_type (mlview::TreeEditorsNodeColour colour);
  ///
  /// Get the value of a color property for a given type
  ///
  /// \param type_id the type for which we search the color
  ///
  /// \return a string representation of the color
  ///
  Glib::ustring get_color_for_type (const Glib::ustring& type_id);
  ///
  /// Get the value of a color property for a given type
  ///
  /// \param colour an mlview::TreeEditorsNodeColour colour
  ///
  /// \return a string representation of the color
  ///
    Glib::ustring get_color_for_type (mlview::TreeEditorsNodeColour colour);
  ///
  /// Set the value of a color property for a given type
  ///
  /// \param type_id the type for which we search the color
  /// \param color_str a string representation of the color
  ///
  void set_color_for_type (const Glib::ustring& type_id,
			   const Glib::ustring& color_str);
  ///
  /// Set the value of a color property for a given type
  ///
  /// \param type_id the type for which we search the color
  /// \param color a Gdk::Color
  ///
  void set_color_for_type (const Glib::ustring& type_id,
			   const Gdk::Color color);

  ///
  /// This method returns a reference to the font_changed signal
  /// This signal is emitted when the font property has been changed
  ///
  sigc::signal0<void>& signal_font_changed ();

  ///
  /// This method returns a reference to the colour_changed signal
  /// This signal is emitted when a colour is changed in the preferences
  ///
    sigc::signal0<void>& signal_colour_changed ();

  /// The category id
    static const char* CATEGORY_ID;
};

} // namespace mlview

#endif
