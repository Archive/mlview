/* -*- Mode: C++; indent-tabs-mode:true; c-basic-offset: 4-*- */
/*
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute 
 *it and/or modify it under the terms of 
 *the GNU General Public License as published by the 
 *Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYING file for copyright information.
 */
#ifndef __MLVIEW_EXCEPTION_H__
#define __MLVIEW_EXCEPTION_H__

#include <exception>
#include <iostream>
#include "mlview-utils.h"
#include "mlview-ustring.h"

using namespace std ;

namespace mlview
{

class Exception : public std::exception
{
	friend std::ostream& operator<< (std::ostream&, Exception& an_exception) ;

protected:
	UString m_reason ;

public:
	//constuctors
	Exception (const gchar *a_reason="") ;

	Exception (const UString &a_reason="") ;

	Exception (const Exception &an_exception) ;

	virtual ~Exception () throw ();

	Exception& operator= (Exception const &) ;

	//get the reason of the exception
	const gchar * what () const throw ();

	//gchar cast operator
	operator const gchar* () const ;
} ;

ostream& operator<< (std::ostream&, Exception& an_exception) ;


#define THROW_IF_FAIL(a_cond) \
if (!(a_cond)) { \
LOG_TO_ERROR_STREAM ("condition (" << #a_cond << ") failed; raising exception "  << endl) ;\
throw mlview::Exception ("Assertion failed")  ;\
}

#define THROW_IF_FAIL2(a_cond, a_reason) \
if (!(a_cond)) { \
LOG_TO_ERROR_STREAM \
("condition (" << #a_cond << ") failed; raising exception " << #a_reason << endl);\
throw mlview::Exception (a_reason)  ;\
}

#define THROW(a_reason) \
LOG_TO_ERROR_STREAM \
("raised exception: "<< #a_reason << endl); \
throw mlview::Exception (a_reason)  ;

#define THROW_EMPTY \
LOG_TO_ERROR_STREAM \
("raised empty exception " << endl) ; \
throw ;

#define THROW_EXCEPTION(type, message) \
LOG_TO_ERROR_STREAM \
("raised " << #type << ": "<< message) ; \
throw type (message) ;

#define TRACE_EXCEPTION(exception) \
LOG_TO_ERROR_STREAM \
("catched exception: " << exception.what () << endl);

} //namespace mlview

#endif //__MLVIEW_EXCEPTION_H__
