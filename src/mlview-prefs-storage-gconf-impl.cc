/* -*- Mode: C++; indent-tabs-mode:true; c-basic-offset: 4-*- */
/*
  PrefsStorageGConfImpl : Implementation
 
  This file is part of MlView.
 
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include <gconf/gconf-client.h>
#include "mlview-prefs-storage-gconf-impl.h"
#include "mlview-safe-ptr-utils.h"

namespace mlview
{
struct PrefsStorageGConfImplPriv
{
    GConfClient *m_gconf_client;
};

PrefsStorageGConfImpl::PrefsStorageGConfImpl ()
{
    m_priv = new PrefsStorageGConfImplPriv ();
    m_priv->m_gconf_client = gconf_client_get_default ();
}

PrefsStorageGConfImpl::~PrefsStorageGConfImpl ()
{
    if (m_priv != NULL) {
	delete m_priv;
	m_priv = NULL ;
    }
}

void
PrefsStorageGConfImpl::set_int_value (const UString& key,
				      int value)
{
    SafePtr<GError, GErrorRef, GErrorUnref> err_ptr ;
    GError *err = NULL ;

    gconf_client_set_int (m_priv->m_gconf_client,
			  key.c_str(),
			  value,
			  &err) ;
    err_ptr = err ;
    err = NULL ;
    if (err_ptr) {
	THROW (UString ("gconf_client_set_int() returned error: ") +
	       UString (err_ptr->message)) ;
    }
}

int
PrefsStorageGConfImpl::get_int_value (const UString& key)
{
    SafePtr<GError, GErrorRef, GErrorUnref> err_ptr;
    GError *err = NULL ;
    gint res = 0 ;

    res = gconf_client_get_int (m_priv->m_gconf_client,
				key.c_str(),
				&err) ;
    err_ptr = err ;
    err = NULL ;
    if (err_ptr) {
	THROW (UString ("gconf_client_get_int() returned error: ") +
	       UString (err_ptr->message)) ;
    }

    return res ;
}

int
PrefsStorageGConfImpl::get_default_int_value (const UString& key)
{
    SafePtr<GError, GErrorRef, GErrorUnref> err_ptr;
    GError *err = NULL ;
    GConfValue *value = NULL;

    value = gconf_client_get_default_from_schema (m_priv->m_gconf_client,
						  key.c_str(),
						  &err);

    err_ptr = err;
    err = NULL;
    if (err_ptr) {
	THROW (UString ("gconf_client_get_default_int() returned error: ") +
	       UString (err_ptr->message));
    }

    return gconf_value_get_int (value);

}

void
PrefsStorageGConfImpl::set_string_value(const UString& key,
					const UString& value)
{
    SafePtr<GError, GErrorRef, GErrorUnref> err_ptr;
    GError *err = NULL ;

    gconf_client_set_string (m_priv->m_gconf_client,
			     key.c_str(),
			     value.c_str(),
			     &err) ;
    err_ptr = err;
    err = NULL;

    if (err_ptr) {
		THROW (UString ("gconf_client_set_string() returned error: ") +
			   UString (err_ptr->message)) ;
    }
}

UString
PrefsStorageGConfImpl::get_string_value (const UString& key)
{
    SafePtr<gchar, GMemRef, GMemUnref> value = NULL ;

    value = gconf_client_get_string (m_priv->m_gconf_client,
				     key,
				     NULL) ;
    if (!value) {
		THROW (UString ("gconf_client_get_string() failed for key ") +
						UString (key)) ;
    }
    return value.get () ;
}

UString
PrefsStorageGConfImpl::get_default_string_value (const UString& key)
{
    SafePtr<GError, GErrorRef, GErrorUnref> err_ptr;
    GError *err = NULL ;
    GConfValue *value = NULL;

    value = gconf_client_get_default_from_schema (m_priv->m_gconf_client,
						  key.c_str(),
						  &err);
    err_ptr = err;
    err = NULL;
    if (err_ptr) {
	THROW (UString ("gconf_client_get_default_string() returned error: ") +
	       UString (err_ptr->message));
    }

    UString str_value = gconf_value_get_string (value);

    return str_value;
}

void
PrefsStorageGConfImpl::set_bool_value (const UString& key,
				       bool value)
{
    SafePtr<GError, GErrorRef, GErrorUnref> err_ptr;
    GError *err = NULL ;

    gconf_client_set_bool (m_priv->m_gconf_client,
			   key.c_str(),
			   value,
			   &err) ;
    err_ptr = err;
    err = NULL;
    if (err_ptr) {
	THROW (UString ("gconf_client_set_bool() returned error: ") +
	       UString (err_ptr->message)) ;
    }
}

bool
PrefsStorageGConfImpl::get_bool_value (const UString& key)
{
    SafePtr<GError, GErrorRef, GErrorUnref> err_ptr;
    GError *err = NULL ;
    bool res = false ;

    res = static_cast<bool> (gconf_client_get_bool (m_priv->m_gconf_client,
						    key.c_str(),
						    &err)) ;
    err_ptr = err;
    err = NULL ;
    if (err_ptr) {
	THROW (UString ("gconf_client_get_bool() returned error: ") +
	       UString (err_ptr->message)) ;
    }

    return res ;
}

bool
PrefsStorageGConfImpl::get_default_bool_value (const UString& key)
{
    SafePtr<GError, GErrorRef, GErrorUnref> err_ptr;
    GError *err = NULL ;
    GConfValue *value = NULL;

    value = gconf_client_get_default_from_schema (m_priv->m_gconf_client,
						  key.c_str(),
						  &err);
    err_ptr = err;
    err = NULL;
    if (err_ptr) {
	THROW (UString ("gconf_client_get_default_bool() returned error: ") +
	       UString (err_ptr->message));
    }

    return static_cast<bool>
	(gconf_value_get_bool (value));
}

} // namespace mlview
