/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 8-*- */

/*This file is part of MlView
 *
 *MlView is free software; you can redistribute 
 *it and/or modify it under the terms of 
 *the GNU General Public License as 
 *published by the Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *MlView is distributed in the hope that 
 *it will be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. If not, write to the 
 *Free Software Foundation, Inc., 59 Temple Place - Suite 330, 
 *Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */
#include <string.h>
#include <libxml/parser.h>
#include <libxml/valid.h>
#include <libxml/tree.h>

#include "mlview-attribute-picker.h"
#include "mlview-parsing-utils.h"
#include "mlview-utils.h"

/**
 *@file
 *The definition of the class #MlViewAttributePicker.
 *This class is a business widget. It lets the user enter the name/value of
 *an attribute. It can for example perform an attribute name completion given
 *the xmlNode on which the attribute is to be defined.
 */


/**
 *The private instance members of #MlViewAttributePicker.
 *Theses member should not be accessed directly. They
 *must only be accessed through the available accessors.
 */
struct _MlViewAttributePickerPrivate {
 /**the entry used to edit attribute name*/
        GtkCombo *name_edit_entry;

 /**the entry used to edit attribute type*/
        GtkCombo *type_edit_entry;

 /**the entry used to edit the attribute value*/
        GtkEntry *value_edit_entry;

 /**The attribute values list*/
        GtkList *values_list;

 /**used to set attr value to the selected attr val*/
        GtkButton *set_value_button;

 /**adding a string to the attr value*/
        GtkButton *add_to_value_button;

        GtkTable *value_list_table;

 /**the list of possible attr names*/
        GList *names_completion_list;

 /**the list of possible attribute values*/
        GList *values_completion_list;

 /**the current xml node*/
        xmlNode *cur_xml_node;

 /**the current selected attribute value*/
        gchar *current_attribute_value;
};

#define PRIVATE(object) (object)->priv

/*a pointer to the parent class. Necessary for the gtk typing system framework*/
static GtkDialogClass *parent_class = NULL;

static gboolean gv_attributes_completion = TRUE;

static void mlview_attribute_picker_destroy (GtkObject *
        a_object);
static void
mlview_attribute_picker_init_class (MlViewAttributePickerClass *
                                    a_klass);
static void mlview_attribute_picker_init (MlViewAttributePicker *
        a_this);

static void
mlview_attribute_picker_show_attr_values (MlViewAttributePicker *
        a_this);
static void
mlview_attribute_picker_hide_attr_values (MlViewAttributePicker *
        a_this);
static xmlAttributeType
mlview_attribute_picker_parse_attr_type (const gchar * a_string);

static void attribute_name_changed_cb (GtkEditable *
                                       a_text_entry,
                                       gpointer a_this);
static void add_to_value_button_cb (GtkButton * a_button,
                                    gpointer a_this);
static void set_value_button_cb (GtkButton * a_button,
                                 gpointer a_this);
static void attribute_value_selected_cb (GtkList *
        a_attribute_values_list,
        GtkWidget * a_list_item,
        gpointer * a_this);
static void attribute_type_changed_cb (GtkEditable *
                                       a_text_entry,
                                       gpointer a_this);

/*----------------------------------------------------
 *Private functions
 *----------------------------------------------------*/

/**
 *The class structure initialyzer. 
 *@param a_klass the current vtable.
 */
static void
mlview_attribute_picker_init_class (MlViewAttributePickerClass *
                                    a_klass)
{
        GtkObjectClass *object_class;

        THROW_IF_FAIL (a_klass != NULL);

        parent_class = (GtkDialogClass*) g_type_class_peek_parent (a_klass);
        THROW_IF_FAIL (GTK_IS_DIALOG_CLASS (a_klass));
        object_class = GTK_OBJECT_CLASS (a_klass);
        object_class->destroy = mlview_attribute_picker_destroy;
}


/**
 *The allocator of the instances of #MlViewAttributePicker.
 *Allocates memory for the members (and initialyzes them) 
 *of  MlViewAttributePicker.
 *
 *@param a_this the current instance of #MlViewAttributePicker.
 */
static void
mlview_attribute_picker_init (MlViewAttributePicker * a_this)
{
        GtkWidget *label = NULL,
                *table = NULL,
                *vbox = NULL,
                *separator = NULL;

        THROW_IF_FAIL (a_this != NULL);
        THROW_IF_FAIL (MLVIEW_IS_ATTRIBUTE_PICKER (a_this));

        gtk_dialog_add_buttons (GTK_DIALOG (a_this),
                                _("OK"),
                                GTK_RESPONSE_ACCEPT,
                                _("Cancel"),
                                GTK_RESPONSE_REJECT, NULL);

        gtk_window_set_modal (GTK_WINDOW (a_this), TRUE);

        PRIVATE (a_this) = (MlViewAttributePickerPrivate*)g_try_malloc
                (sizeof (MlViewAttributePickerPrivate));
        if (!PRIVATE (a_this)) {
                g_warning ("System may be out of memory");
                return;
        }

        memset (PRIVATE (a_this), 0,
                sizeof (MlViewAttributePickerPrivate));

        label = gtk_label_new (_("attribute name"));
        PRIVATE (a_this)->name_edit_entry =
                GTK_COMBO (gtk_combo_new ());

        g_signal_connect (G_OBJECT
                          (PRIVATE (a_this)->name_edit_entry->
                           entry), "changed",
                          G_CALLBACK (attribute_name_changed_cb),
                          a_this);

        table = gtk_table_new (1, 2, TRUE);
        gtk_table_attach_defaults (GTK_TABLE (table), label,
                                   0, 1, 0, 1);
        gtk_table_attach_defaults (GTK_TABLE (table),
                                   GTK_WIDGET (PRIVATE
                                               (a_this)->
                                               name_edit_entry),
                                   1, 2, 0, 1);
        gtk_box_pack_start (GTK_BOX
                            (GTK_DIALOG (a_this)->vbox), table,
                            FALSE, TRUE, 0);
        gtk_widget_show_all (table);

        label = gtk_label_new (_("attribute type"));
        PRIVATE (a_this)->type_edit_entry =
                GTK_COMBO (gtk_combo_new ());

        g_signal_connect (G_OBJECT
                          (PRIVATE (a_this)->type_edit_entry->
                           entry), "changed",
                          G_CALLBACK (attribute_type_changed_cb),
                          a_this);

        table = gtk_table_new (1, 2, TRUE);
        gtk_table_attach_defaults (GTK_TABLE (table), label,
                                   0, 1, 0, 1);
        gtk_table_attach_defaults (GTK_TABLE (table),
                                   GTK_WIDGET (PRIVATE
                                               (a_this)->
                                               type_edit_entry),
                                   1, 2, 0, 1);
        gtk_box_pack_start (GTK_BOX
                            (GTK_DIALOG (a_this)->vbox), table,
                            FALSE, TRUE, 0);
        gtk_widget_show_all (table);

        separator = gtk_hseparator_new ();
        gtk_box_pack_start (GTK_BOX
                            (GTK_DIALOG (a_this)->vbox),
                            separator, FALSE, TRUE, 0);
        gtk_widget_show (separator);

        label = gtk_label_new (_("attribute value:"));
        PRIVATE (a_this)->value_edit_entry =
                GTK_ENTRY (gtk_entry_new ());
        table = gtk_table_new (1, 2, FALSE);
        gtk_table_attach_defaults (GTK_TABLE (table), label,
                                   0, 1, 0, 1);
        gtk_table_attach_defaults (GTK_TABLE (table),
                                   GTK_WIDGET (PRIVATE
                                               (a_this)->
                                               value_edit_entry),
                                   1, 2, 0, 1);
        gtk_box_pack_start (GTK_BOX
                            (GTK_DIALOG (a_this)->vbox), table,
                            FALSE, TRUE, 0);
        gtk_widget_show_all (table);


        PRIVATE (a_this)->values_list =
                GTK_LIST (gtk_list_new ());
        g_signal_connect (G_OBJECT
                          (PRIVATE (a_this)->values_list),
                          "select-child",
                          G_CALLBACK
                          (attribute_value_selected_cb),
                          a_this);
        PRIVATE (a_this)->set_value_button =
                GTK_BUTTON (gtk_button_new_with_label
                            (_("set value")));
        PRIVATE (a_this)->add_to_value_button =
                GTK_BUTTON (gtk_button_new_with_label
                            (_("add to value")));
        g_signal_connect (G_OBJECT
                          (PRIVATE (a_this)->set_value_button),
                          "clicked",
                          G_CALLBACK (set_value_button_cb),
                          a_this);
        g_signal_connect (G_OBJECT
                          (PRIVATE (a_this)->
                           add_to_value_button), "clicked",
                          G_CALLBACK (add_to_value_button_cb),
                          a_this);

        vbox = gtk_vbox_new (FALSE, 0);
        gtk_box_pack_start (GTK_BOX (vbox),
                            GTK_WIDGET (PRIVATE (a_this)->
                                        set_value_button), FALSE,
                            TRUE, 0);
        gtk_box_pack_start (GTK_BOX (vbox),
                            GTK_WIDGET (PRIVATE (a_this)->
                                        add_to_value_button),
                            FALSE, TRUE, 0);
        PRIVATE (a_this)->value_list_table =
                GTK_TABLE (gtk_table_new (1, 2, FALSE));

        gtk_table_attach_defaults (PRIVATE (a_this)->
                                   value_list_table,
                                   GTK_WIDGET (PRIVATE
                                               (a_this)->
                                               values_list), 0,
                                   1, 1, 2);
        gtk_table_attach_defaults (PRIVATE (a_this)->
                                   value_list_table, vbox, 1, 2,
                                   1, 2);

        gtk_widget_ref (GTK_WIDGET
                        (PRIVATE (a_this)->value_list_table));
}


/**
 *The destroy method.
 *@param a_object the instance of #MlViewAttributePicker to 
 *destroy.
 */
static void
mlview_attribute_picker_destroy (GtkObject * a_object)
{
        MlViewAttributePicker *picker;

        THROW_IF_FAIL (a_object != NULL);
        THROW_IF_FAIL (MLVIEW_IS_ATTRIBUTE_PICKER (a_object));

        picker = MLVIEW_ATTRIBUTE_PICKER (a_object);

        if (PRIVATE (picker) == NULL)
                return;

        gtk_widget_unref (GTK_WIDGET
                          (PRIVATE (picker)->value_list_table));

        if (PRIVATE (picker)->names_completion_list) {
                g_list_free (PRIVATE (picker)->
                             names_completion_list);
                PRIVATE (picker)->names_completion_list = NULL;
        }

        if (PRIVATE (picker)->values_completion_list) {
                g_list_free (PRIVATE (picker)->
                             values_completion_list);
                PRIVATE (picker)->values_completion_list = NULL;
        }

        if (PRIVATE (picker)) {
                g_free (PRIVATE (picker));
                PRIVATE (picker) = NULL;
        }

        if (GTK_OBJECT_CLASS (parent_class)->destroy) {
                GTK_OBJECT_CLASS (parent_class)->
                        destroy (a_object);
        }

}


/**
 *Converts the xmlAttributeType into a string that can be displayed
 *to the user.
 *@param a_attribute_type the xmlAttributeType to convert into a string.
 *@return the converted string or NULL if the conversion could not be made.
 */
static gchar *
mlview_attribute_picker_attr_type_to_str (xmlAttributeType
        a_attribute_type)
{
	gchar *result = NULL;

	switch (a_attribute_type) {
	case XML_ATTRIBUTE_CDATA:
		result = g_strdup ("CDATA");
		break;
	case XML_ATTRIBUTE_ID:
		result = g_strdup ("ID");
		break;
	case XML_ATTRIBUTE_IDREF:
		result = g_strdup ("IDREF");
		break;
	case XML_ATTRIBUTE_IDREFS:
		result = g_strdup ("IDREFS");
		break;
	case XML_ATTRIBUTE_ENTITY:
		result = g_strdup ("ENTITY");
		break;
	case XML_ATTRIBUTE_ENTITIES:
		result = g_strdup ("ENTITIES");
		break;
	case XML_ATTRIBUTE_NMTOKEN:
		result = g_strdup ("NMTOKEN");
		break;
	case XML_ATTRIBUTE_NMTOKENS:
		result = g_strdup ("NMTOKENS");
		break;
	case XML_ATTRIBUTE_ENUMERATION:
		result = g_strdup ("ENUMERATION");
		break;
	case XML_ATTRIBUTE_NOTATION:
		result = g_strdup ("NOTATION");
		break;
	default:
		break;
	}
	return result;
}

/**
 *Parses the string a_string and return the matching xmlAttributeType.
 *@param a_string the string to parse.
 *@return the matching xmlAttributeType. Note that if
 *the string could not be parsed, this function returns the xmlAttributeType
 *XML_ATTRIBUTE_CDATA.
 */
static xmlAttributeType
mlview_attribute_picker_parse_attr_type (const gchar * a_string)
{
	if (a_string == NULL
	        || mlview_utils_is_white_string (a_string))
		return XML_ATTRIBUTE_CDATA;

	if (!strcmp (a_string, "CDATA"))
		return XML_ATTRIBUTE_CDATA;
	if (!strcmp (a_string, "ID"))
		return XML_ATTRIBUTE_ID;
	if (!strcmp (a_string, "IDREF"))
		return XML_ATTRIBUTE_IDREF;
	if (!strcmp (a_string, "IDREFS"))
		return XML_ATTRIBUTE_IDREFS;
	if (!strcmp (a_string, "ENTITY"))
		return XML_ATTRIBUTE_ENTITY;
	if (!strcmp (a_string, "ENTITIES"))
		return XML_ATTRIBUTE_ENTITIES;
	if (!strcmp (a_string, "NMTOKEN"))
		return XML_ATTRIBUTE_NMTOKEN;
	if (!strcmp (a_string, "NMTOKENS"))
		return XML_ATTRIBUTE_NMTOKENS;
	if (!strcmp (a_string, "ENUMERATION"))
		return XML_ATTRIBUTE_ENUMERATION;
	if (!strcmp (a_string, "NOTATION"))
		return XML_ATTRIBUTE_NOTATION;

	return XML_ATTRIBUTE_CDATA;
}


/**
 *Shows the attribute values list that has been build and cached into
 *the object a_this.
 *@param a_this the current instance of #MlViewAttributePicker.
 */
static void
mlview_attribute_picker_show_attr_values (MlViewAttributePicker *a_this)
{
        THROW_IF_FAIL (a_this != NULL);
        THROW_IF_FAIL (MLVIEW_IS_ATTRIBUTE_PICKER (a_this));
        THROW_IF_FAIL (PRIVATE (a_this) != NULL);

        /*make sure the attribute list table is not there already... */
        if (GTK_WIDGET (PRIVATE (a_this)->value_list_table)->
            parent == GTK_WIDGET (GTK_DIALOG (a_this)->vbox))
                gtk_container_remove (GTK_CONTAINER
                                      (GTK_DIALOG (a_this)->
                                       vbox),
                                      GTK_WIDGET (PRIVATE
                                                  (a_this)->
                                                  value_list_table));

        /*now, pack the attr list table */
        gtk_box_pack_start (GTK_BOX
                            (GTK_DIALOG (a_this)->vbox),
                            GTK_WIDGET (PRIVATE (a_this)->
                                        value_list_table), FALSE,
                            TRUE, 0);

        gtk_widget_show_all (GTK_WIDGET
                             (PRIVATE (a_this)->
                              value_list_table));
        gtk_widget_show_all (GTK_WIDGET
                             (GTK_DIALOG (a_this)->vbox));
}


/**
 *Hides the attribute values list that has been build and shown.
 *@param a_this the current instance of #MlViewAttributePicker.
 */
static void
mlview_attribute_picker_hide_attr_values (MlViewAttributePicker *
                                          a_this)
{
        THROW_IF_FAIL (a_this != NULL);
        THROW_IF_FAIL (MLVIEW_IS_ATTRIBUTE_PICKER (a_this));
        THROW_IF_FAIL (PRIVATE (a_this) != NULL);

        /*Make sure the value_list_table is packed in the dialog vbox */
        if (GTK_WIDGET (PRIVATE (a_this)->value_list_table)->
            parent == GTK_WIDGET (GTK_DIALOG (a_this)->vbox)) {
                gtk_container_remove (GTK_CONTAINER
                                      (GTK_DIALOG (a_this)->
                                       vbox),
                                      GTK_WIDGET (PRIVATE
                                                  (a_this)->
                                                  value_list_table));
                gtk_widget_show_all (GTK_WIDGET
                                     (GTK_DIALOG (a_this)->
                                      vbox));
        }
}


/*----------------------------------------------------
 *Private function callbacks.
 *----------------------------------------------------*/


/**
 *Callback function invoked when the attribute name
 *changes. It actually sets the type of the attribute,
 *computes the attribute value and displays that choice list if necessary. 
 *
 */
static void
attribute_name_changed_cb (GtkEditable * a_text_entry,
                           gpointer a_this)
{
        gchar *content = NULL;
        MlViewAttributePicker *picker = NULL;

        THROW_IF_FAIL (a_text_entry != NULL);
        THROW_IF_FAIL (GTK_IS_EDITABLE (a_text_entry));
        THROW_IF_FAIL (a_this != NULL);
        THROW_IF_FAIL (MLVIEW_IS_ATTRIBUTE_PICKER (a_this));

        picker = MLVIEW_ATTRIBUTE_PICKER (a_this);

        if (gv_attributes_completion == FALSE) {
                return;
        }

        gtk_entry_set_text (PRIVATE (picker)->value_edit_entry,
                            "");

        content = gtk_editable_get_chars (a_text_entry, 0, -1);

        if (!content) {
                mlview_attribute_picker_hide_attr_values
                        (picker);
                return;
        }

        if (PRIVATE (picker)->cur_xml_node
            && PRIVATE (picker)->cur_xml_node->name) {
                xmlAttribute *attr_desc = NULL;

                /*test if the current attr is a defined attribute */
                if (PRIVATE (picker)->cur_xml_node->doc->
                    intSubset) {
                        attr_desc =
                                xmlGetDtdAttrDesc 
				(PRIVATE (picker)->cur_xml_node->doc->intSubset,
			         PRIVATE (picker)->cur_xml_node->name,
			         (xmlChar*)content);
                }
                if (attr_desc == NULL
                    && PRIVATE (picker)->cur_xml_node->doc->
                    extSubset) {
                        attr_desc =
                                xmlGetDtdAttrDesc 
				(PRIVATE (picker)->cur_xml_node->doc->extSubset,
			         PRIVATE (picker)->cur_xml_node->name, 
				 (xmlChar*)content);
                }
                if (attr_desc == NULL) { /*attribute is unknown by the dtd */
                        gchar *attr_type = NULL;

                        mlview_attribute_picker_hide_attr_values
                                (picker);
                        attr_type =
                                mlview_attribute_picker_attr_type_to_str
                                (XML_ATTRIBUTE_CDATA);

                        gtk_entry_set_text (GTK_ENTRY
                                            (PRIVATE
                                             (picker)->
                                             type_edit_entry->
                                             entry), attr_type);
                        if (attr_type) {
                                g_free (attr_type);
                                attr_type = NULL;
                        }
                } else {        /*set the the attribute type and the attribute list */
                        GList *attr_vals = NULL;
                        gchar *attr_type = NULL;
						mlview::AppContext *context =
								mlview::AppContext::get_instance () ;
						THROW_IF_FAIL (context) ;

                        gint *last_id_ptr =
                                context->get_last_id_ptr () ;
                        THROW_IF_FAIL (last_id_ptr != NULL);

                        attr_type =
                                mlview_attribute_picker_attr_type_to_str
                                (attr_desc->atype);

                        gtk_entry_set_text
                                (GTK_ENTRY
                                 (PRIVATE (picker)->type_edit_entry->entry),
                                 attr_type);

                        if (attr_type) {
                                g_free (attr_type);
                                attr_type = NULL;
                        }

                        attr_vals =
                                mlview_parsing_utils_build_graphical_attr_values
                                (attr_desc, last_id_ptr);

                        if (attr_vals != NULL) {

                                gtk_list_clear_items
                                        (PRIVATE (picker)->
                                         values_list, 0, -1);

                                gtk_list_append_items
                                        (PRIVATE (picker)->
                                         values_list, attr_vals);

                                mlview_attribute_picker_show_attr_values
                                        (picker);

                        } else {
                                mlview_attribute_picker_hide_attr_values
                                        (picker);
                        }
                }
        }
        g_free (content);
}


/**
 *Callback invoked when the attribute type changes. It just enable/disable the
 *add to value button when necessary. Actually, if the type of the attribute
 *is either "IDREFS" or "ENTITIES", this function enable the "add to value" button. 
 *
 */
static void
attribute_type_changed_cb (GtkEditable * a_text_entry,
                           gpointer a_this)
{
        gchar *content = NULL;
        MlViewAttributePicker *picker = (MlViewAttributePicker*) a_this;

        THROW_IF_FAIL (a_text_entry != NULL);
        THROW_IF_FAIL (GTK_IS_EDITABLE (a_text_entry));
        THROW_IF_FAIL (a_this != NULL);
        THROW_IF_FAIL (MLVIEW_IS_ATTRIBUTE_PICKER (a_this));

        content =
                gtk_editable_get_chars (GTK_EDITABLE
                                        (a_text_entry), 0, -1);

        if (!strcmp (content, "IDRREFS")
            || !strcmp (content, "ENTITIES")) {
                gtk_widget_set_sensitive (GTK_WIDGET
                                          (PRIVATE (picker)->
                                           add_to_value_button),
                                          TRUE);
        } else {
                gtk_widget_set_sensitive
                        (GTK_WIDGET
                         (PRIVATE (picker)->add_to_value_button),
                         FALSE);
        }
}


/**
 *The callback executed when the user clicks on the "add to value" button. 
 *
 */
static void
add_to_value_button_cb (GtkButton * a_button, gpointer a_this)
{
        MlViewAttributePicker *picker =
                (MlViewAttributePicker *) a_this;

        THROW_IF_FAIL (a_button != NULL);
        THROW_IF_FAIL (GTK_IS_BUTTON (a_button));
        THROW_IF_FAIL (a_this != NULL);
        THROW_IF_FAIL (MLVIEW_IS_ATTRIBUTE_PICKER (picker));
        THROW_IF_FAIL (PRIVATE (picker) != NULL);

        if (PRIVATE (picker)->current_attribute_value) {
                gchar *str = NULL;

                str = gtk_editable_get_chars
                        (GTK_EDITABLE
                         (PRIVATE (picker)->value_edit_entry), 0,
                         -1);
                str = g_strconcat (str, " ",
                                   PRIVATE (picker)->
                                   current_attribute_value,
                                   NULL);
                gtk_entry_set_text (PRIVATE (picker)->
                                    value_edit_entry, str);

                if (str) {
                        g_free (str);
                        str = NULL;
                }
        }
}

/**
 *Callback called when the user clicks on the "set value" button to set
 *the attribute value to the currently selected value. 
 *
 */
static void
set_value_button_cb (GtkButton * a_button, gpointer a_this)
{
        MlViewAttributePicker *picker =
                (MlViewAttributePicker *) a_this;

        THROW_IF_FAIL (a_button != NULL);
        THROW_IF_FAIL (GTK_IS_BUTTON (a_button));
        THROW_IF_FAIL (picker != NULL);
        THROW_IF_FAIL (MLVIEW_IS_ATTRIBUTE_PICKER (picker));
        THROW_IF_FAIL (PRIVATE (picker) != NULL);

        if (PRIVATE (picker)->current_attribute_value) {
                gtk_entry_set_text (PRIVATE (picker)->
                                    value_edit_entry,
                                    PRIVATE (picker)->
                                    current_attribute_value);
        }
}


/**
 *
 */
static void
attribute_value_selected_cb (GtkList * a_attribute_values_list,
                             GtkWidget * a_list_item,
                             gpointer * a_this)
{
        MlViewAttributePicker *picker =
                (MlViewAttributePicker *) a_this;
        GList *list = NULL;

        THROW_IF_FAIL (a_attribute_values_list != NULL);
        THROW_IF_FAIL (a_list_item != NULL);
        THROW_IF_FAIL (picker != NULL);
        THROW_IF_FAIL (MLVIEW_IS_ATTRIBUTE_PICKER (picker));
        THROW_IF_FAIL (PRIVATE (picker) != NULL);

        /*get the label contained in the GtkItem */
        list = gtk_container_children (GTK_CONTAINER
                                       (a_list_item));

        if (list && list->data && GTK_IS_LABEL (list->data)) {
                GtkLabel *label = NULL;

                label = GTK_LABEL (list->data);
                gtk_label_get (label,
                               &PRIVATE (picker)->
                               current_attribute_value);

        } else {
                PRIVATE (picker)->current_attribute_value = NULL;
        }
}


/*----------------------------------------------------
 *Public functions
 *----------------------------------------------------*/


/**
 *Type builder. 
 *@return the id of the type MlViewAttribute.
 */
gint
mlview_attribute_picker_get_type (void)
{
	static gint type = 0;

	if (!type) {
		static const GTypeInfo type_info = {
		                                       sizeof (MlViewAttributePickerClass),
		                                       NULL,   /* base_init */
		                                       NULL,   /* base_finalize */
		                                       (GClassInitFunc)
		                                       mlview_attribute_picker_init_class,
		                                       NULL,   /* class_finalize */
		                                       NULL,   /* class_data */
		                                       sizeof (MlViewAttributePicker),
		                                       0,
		                                       (GInstanceInitFunc)
		                                       mlview_attribute_picker_init
		                                   };

		type = g_type_register_static (GTK_TYPE_DIALOG,
		                               "MlViewAttributePicker",
		                               &type_info, (GTypeFlags)0);
	}
	return type;
}

/**
 *Builds a brand new attribute picker. 
 *@param a_title the title the window of the attribute picker.
 *@return the widget of the attribute picker. 
 */
GtkWidget *
mlview_attribute_picker_new (gchar * a_title)
{
        MlViewAttributePicker *result = NULL;

        result = (MlViewAttributePicker*)gtk_type_new
				(MLVIEW_TYPE_ATTRIBUTE_PICKER);
        gtk_window_set_title (GTK_WINDOW (result), a_title);

        return GTK_WIDGET (result);
}



/**
 *Sets the focus to the name entry of 
 *#MlViewAttributePicker widget. 
 *@param a_this the current instance of #MlViewAttributePicker.
 */
void
mlview_attribute_picker_grab_focus_to_name_entry (MlViewAttributePicker * a_this)
{
        THROW_IF_FAIL (a_this != NULL);
        THROW_IF_FAIL (MLVIEW_IS_ATTRIBUTE_PICKER (a_this));
        THROW_IF_FAIL (PRIVATE (a_this) != NULL);

        if (PRIVATE (a_this)->name_edit_entry)
                gtk_widget_grab_focus (PRIVATE (a_this)->name_edit_entry->entry);
}


/**
 *Sets the focus to the value entry of 
 *#MlViewAttributePicker widget. 
 *@param a_this the current instance 
 *of #MlViewAttributePicker.
 */
void
mlview_attribute_picker_grab_focus_to_value_entry (MlViewAttributePicker * a_this) 
{
        THROW_IF_FAIL (a_this != NULL);
        THROW_IF_FAIL (MLVIEW_IS_ATTRIBUTE_PICKER (a_this));
        THROW_IF_FAIL (PRIVATE (a_this) != NULL);

        if (PRIVATE (a_this)->value_edit_entry)
                gtk_widget_grab_focus (GTK_WIDGET
                                       (PRIVATE (a_this)->
                                        value_edit_entry));
}


/**
 *Gets the content of the name entry of attribute picker. 
 *@param a_this the current instance of #MlViewAttributePicker.
 *@return a pointer to the name entry of attribute picker. 
 *DO NOT FREE THIS POINTER or you will hang the process. 
 */
gchar *
mlview_attribute_picker_get_attribute_name (MlViewAttributePicker* a_this)
{
	g_return_val_if_fail (a_this != NULL, NULL);
	g_return_val_if_fail (MLVIEW_IS_ATTRIBUTE_PICKER
	                      (a_this), NULL);
	g_return_val_if_fail (PRIVATE (a_this) != NULL, NULL);

	if (PRIVATE (a_this)->name_edit_entry
	        && PRIVATE (a_this)->name_edit_entry->entry)
		return (gchar *) gtk_entry_get_text
		       (GTK_ENTRY
		        (PRIVATE
		         (a_this)->name_edit_entry->entry));
	else
		return NULL;
}

/**
 *Gets the content of the value entry of the attribute picker. 
 *@param a_this the current instance of #MlViewAttributePicker.
 *@return a pointer to the content of the value entry of the #MlViewAttributePicker widget. 
 */
gchar *
mlview_attribute_picker_get_attribute_value (MlViewAttributePicker * a_this)
{
	g_return_val_if_fail (a_this != NULL, NULL);
	g_return_val_if_fail (MLVIEW_IS_ATTRIBUTE_PICKER
	                      (a_this), NULL);
	g_return_val_if_fail (PRIVATE (a_this) != NULL, NULL);

	if (PRIVATE (a_this)->value_edit_entry)
		return (gchar *) gtk_entry_get_text
		       (PRIVATE (a_this)->value_edit_entry);
	else
		return NULL;
}

/**
 *Gets the type of the last attribute the user has entered in the picker.
 *@param a_this the current instance of #MlViewAttributePicker.
 *@return the type of the last attribute entered by the user in the picker a_this.
 */
xmlAttributeType
mlview_attribute_picker_get_attribute_type (MlViewAttributePicker* a_this)
{
	gchar *type_str = NULL;

	g_return_val_if_fail (a_this != NULL,
	                      XML_ATTRIBUTE_CDATA);
	g_return_val_if_fail (MLVIEW_IS_ATTRIBUTE_PICKER
	                      (a_this), XML_ATTRIBUTE_CDATA);
	g_return_val_if_fail (PRIVATE (a_this) != NULL,
	                      XML_ATTRIBUTE_CDATA);

	if (PRIVATE (a_this)->type_edit_entry
	        && PRIVATE (a_this)->type_edit_entry->entry)
		type_str = (gchar *) gtk_entry_get_text
		           (GTK_ENTRY
		            (PRIVATE
		             (a_this)->type_edit_entry->entry));

	if (type_str != NULL
	        && mlview_utils_is_white_string (type_str) ==
	        FALSE) {
		return mlview_attribute_picker_parse_attr_type
		       (type_str);
	} else {
		return XML_ATTRIBUTE_CDATA;
	}
}

/**
 *Graphically selects all the content of the name entry of the #MlViewAttributePicker widget. 
 *@param a_this the current instance of #MlViewAttributePicker.
 */
void
mlview_attribute_picker_select_attribute_name (MlViewAttributePicker * a_this) 
{
        THROW_IF_FAIL (a_this != NULL);
        THROW_IF_FAIL (MLVIEW_IS_ATTRIBUTE_PICKER (a_this));
        THROW_IF_FAIL (PRIVATE (a_this) != NULL);

        if (PRIVATE (a_this)->name_edit_entry
            && PRIVATE (a_this)->name_edit_entry->entry)
                gtk_entry_select_region (GTK_ENTRY
                                         (PRIVATE (a_this)->
                                          name_edit_entry->
                                          entry), 0, -1);
}

/**
 *Graphically selects all the content of 
 *the value entry of the #MlViewAttributePicker
 *widget. 
 *@param a_this the current instance of #MlViewAttributePicker.
 */
void
mlview_attribute_picker_select_attribute_value (MlViewAttributePicker * a_this) 
{
        THROW_IF_FAIL (a_this != NULL);
        THROW_IF_FAIL (MLVIEW_IS_ATTRIBUTE_PICKER (a_this));
        THROW_IF_FAIL (PRIVATE (a_this) != NULL);

        if (PRIVATE (a_this)->value_edit_entry)
                gtk_entry_select_region (PRIVATE (a_this)->
                                         value_edit_entry, 0,
                                         -1);
}

/**
 *Associates an xml node to this picker.
 *That xml node is used for example to build the attribute name
 *completion list if needed.
 *
 *@param a_this the current instance of #MlViewAttributePicker.
 *@param a_xml_node the xmlNode to associate to the current node.
 */
void
mlview_attribute_picker_set_current_xml_node (MlViewAttributePicker * a_this, 
                                              xmlNode * a_xml_node)
{
        THROW_IF_FAIL (a_this != NULL);
        THROW_IF_FAIL (MLVIEW_IS_ATTRIBUTE_PICKER (a_this));
        THROW_IF_FAIL (PRIVATE (a_this) != NULL);

        PRIVATE (a_this)->cur_xml_node = a_xml_node;

}

/**
 *Sets the attribute completion on/off.
 *All the instances of #MlViewAttributePicker of the current process
 *will be affected by this method.
 *
 *@param a_completion_on wether to set the completion on or not.
 */
void
mlview_attribute_picker_set_attribute_completion (gboolean a_completion_on)
{
	gv_attributes_completion = a_completion_on;
}


/**
 *Builds the attribute name completion list.
 *The list is built, cached in the current instance of #MlViewAttributePicker, and displayed to the user
 *whenever she clicks in the attribute names completion list.
 *This function uses mlview_parsing_utils_build_attribute_name_completion_list() to 
 *actually build the completion list.
 *@param a_this the current instance of #MlViewAttributePicker.
 *@param a_xml_node the xmlNode used to build the attribute name completion list.
 *
 */
void
mlview_attribute_picker_build_attribute_name_choice_list (MlViewAttributePicker * a_this, 
                                                          xmlNode * a_xml_node)
{
        gint nb_of_attribute_names = 0;

        THROW_IF_FAIL (a_this != NULL);
        THROW_IF_FAIL (MLVIEW_IS_ATTRIBUTE_PICKER (a_this));
        THROW_IF_FAIL (PRIVATE (a_this) != NULL);

        gtk_list_clear_items (GTK_LIST
                              (PRIVATE (a_this)->values_list),
                              0, -1);
        gtk_list_clear_items (GTK_LIST
                              (PRIVATE (a_this)->
                               name_edit_entry->list), 0, -1);
        gtk_list_clear_items (GTK_LIST
                              (PRIVATE (a_this)->
                               type_edit_entry->list), 0, -1);
        /*FIXME: clear the two GktEntry widgets too. This is a bug. */

        if (a_xml_node == NULL || a_xml_node->doc == NULL
            || gv_attributes_completion == FALSE) {
                return;
        }

        nb_of_attribute_names =
                mlview_parsing_utils_build_attribute_name_completion_list
                (a_xml_node,
				 &PRIVATE (a_this)->names_completion_list,
				 FALSE /*get all the attributes, even the optional */);

        if (nb_of_attribute_names > 0) {
                if (PRIVATE (a_this)->names_completion_list)
                        gtk_combo_set_popdown_strings
                                (PRIVATE (a_this)->
                                 name_edit_entry,
                                 PRIVATE (a_this)->
                                 names_completion_list);
        }
}
