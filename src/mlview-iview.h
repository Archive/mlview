/* -*- Mode: C++; indent-tabs-mode:nil; c-basic-offset: 4-*- */

/*
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute 
 *it and/or modify it under the terms of 
 *the GNU General Public License as published by the 
 *Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */

#ifndef __MLVIEW_IVIEW_H__
#define __MLVIEW_IVIEW_H__

#include <gtkmm.h>
#include "mlview-utils.h"
#include "mlview-object.h"
#include "mlview-ustring.h"
#include "mlview-action.h"
#include "mlview-xml-document.h"

namespace mlview
{
    
    struct IViewPriv ;
    
class IView : public Object
{

	friend struct IViewPriv ;

	IViewPriv *m_priv ;
public:

	IView () ;

	IView (MlViewXMLDocument *a_doc,
	       const UString &a_name,
	       const UString &a_desc_type_name) ;

	static IView* create_instance (MlViewXMLDocument *a_doc,
	                               const UString &a_view_name) ;

	virtual ~IView () ;

	virtual void set_view_name (const UString &a_name) ;

	virtual UString get_view_name () const ;

	virtual MlViewXMLDocument* get_document () const ;

	virtual void set_document (MlViewXMLDocument *a_doc) ;

	virtual void set_desc_type_name (const UString &a_name) ;

	virtual enum MlViewStatus notify_swapped_out () ;

	virtual enum MlViewStatus notify_swapped_in () ;

	virtual enum MlViewStatus request_application_menu_populating () ;

	virtual UString get_desc_type_name () const ;

	virtual void set_name_interactive () = 0;

	virtual Gtk::Widget* get_view_widget () const ;

	virtual void set_view_widget (Gtk::Widget *a_widget) ;

	virtual enum MlViewStatus connect_to_doc (MlViewXMLDocument *a_this) = 0;

	virtual enum MlViewStatus disconnect_from_doc (MlViewXMLDocument *a_this) = 0;

	virtual enum MlViewStatus update_contextual_menu () = 0;

	virtual enum MlViewStatus execute_action (MlViewAction &an_action) = 0 ;

	virtual bool get_must_rebuild_upon_document_reload () = 0;

	virtual enum MlViewStatus undo () = 0 ;

	virtual enum MlViewStatus redo () = 0 ;

	virtual bool can_undo () = 0 ;

	virtual bool can_redo () = 0 ;

	//****************
	// signals
	//****************

	sigc::signal1<void, IView*> signal_view_name_changed () ;
	sigc::signal0<void> signal_is_swapped_out () ;
	sigc::signal0<void> signal_is_swapped_in () ;
	sigc::signal0<void> signal_application_menu_populating_requested () ;

protected:
	//forbid copy
	IView (const IView &) ;
	IView& operator= (const IView&) ;

}
;//class IView

struct ViewDescriptor
{
	gchar *view_type_name ;
	gchar *translated_view_name ;
	gchar *view_description ;
} ;
} //namespacae mlview

#endif //__MLVIEW_IVIEW_H__

