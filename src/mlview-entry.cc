/* -*- Mode: C; indent-tabs-mode:nil; c-basic-offset: 8-*- */
/*
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute 
 *it and/or modify it under the terms of 
 *the GNU General Public License as published by the 
 *Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */

/**
 *This class is a custom GtkEntry.
 *It does completion for separate words of
 *a "sentence" while the user types.
 */

#include <string.h>
#include <gdk/gdkkeysyms.h>
#include "mlview-entry.h"


#define PRIVATE(obj) (obj->priv)

static GtkEntryClass *gv_parent_class = NULL ;

struct _MlViewEntryPrivate
{
	GtkWindow *popup_win ;
	GtkTreeView *popup_tree_view ;
	GList *completion_list ;
	gboolean dispose_has_run ;
	gchar * cur_compl_str ;
	gint cur_byte_index ;
} ;

static void mlview_entry_dispose (GObject *a_this) ;
static void mlview_entry_finalize (GObject *a_this) ;

static enum MlViewStatus compute_completion_list_popup_menu_position (MlViewEntry *a_this,
        gint a_word_start_index,
        gint a_word_end_index,
        gint *a_x,
        gint *a_y) ;

static enum MlViewStatus build_new_completion_menu (MlViewEntry *a_this,
        GList *menu_strings,
        GtkTreeView **a_menu) ;

static enum MlViewStatus set_completion_menu_content (MlViewEntry *a_this,
        GList *a_menu_strings,
        GtkTreeView *a_menu) ;

static enum MlViewStatus get_completion_menu (MlViewEntry *a_this,
        GList *a_menu_strings,
        GtkWindow **a_popup_win,
        GtkTreeView **a_popup_tree_view) ;

static enum MlViewStatus select_next_or_prev_menu_item (MlViewEntry *a_this,
        gboolean a_next) ;

static void mlview_entry_construct (MlViewEntry *a_this) ;

static gboolean key_press_event_cb (MlViewEntry *a_this,
                                    GdkEventKey *a_event,
                                    gpointer a_user_data) ;

static void signal_changed_cb (GtkTreeSelection *a_sel,
                               MlViewEntry *a_entry) ;

static void
mlview_entry_class_init (MlViewEntryClass *a_klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (a_klass);
	GtkEntryClass *entry_class = GTK_ENTRY_CLASS (a_klass) ;

	g_return_if_fail (a_klass) ;
	g_return_if_fail (entry_class) ;

	gv_parent_class = (GtkEntryClass *) g_type_class_peek_parent (a_klass);
	g_return_if_fail (GTK_IS_ENTRY_CLASS (gv_parent_class));

	object_class->dispose = mlview_entry_dispose ;
	object_class->finalize = mlview_entry_finalize ;

}

static void
mlview_entry_init (MlViewEntry *a_this)
{
	g_return_if_fail (a_this && MLVIEW_IS_ENTRY (a_this)) ;
	g_return_if_fail (a_this && !PRIVATE (a_this)) ;

	PRIVATE (a_this) = (MlViewEntryPrivate *) g_try_malloc (sizeof (MlViewEntryPrivate)) ;
	if (!PRIVATE (a_this)) {
		mlview_utils_trace_debug ("Could not instanciate MlViewEntryPrivate") ;
		return ;
	}
	memset (PRIVATE (a_this), 0, sizeof (MlViewEntryPrivate)) ;
	mlview_entry_construct (a_this) ;
}

static void
mlview_entry_construct (MlViewEntry *a_this)
{
	g_return_if_fail (a_this && MLVIEW_IS_ENTRY (a_this)
	                  && PRIVATE (a_this)) ;

	g_signal_connect (G_OBJECT (a_this),
	                  "key-press-event",
	                  G_CALLBACK (key_press_event_cb),
	                  NULL) ;
}

static void
mlview_entry_dispose (GObject *a_this)
{
	MlViewEntry *thiz = NULL ;

	thiz = MLVIEW_ENTRY (a_this) ;
	g_return_if_fail (thiz
	                  && MLVIEW_IS_ENTRY (thiz)
	                  && PRIVATE (thiz)) ;

	if (PRIVATE (thiz)->dispose_has_run == TRUE)
		return ;

	if (PRIVATE (thiz)->popup_win) {
		gtk_widget_destroy (GTK_WIDGET (PRIVATE (thiz)->popup_win)) ;
		PRIVATE (thiz)->popup_win = NULL ;
		PRIVATE (thiz)->popup_tree_view = NULL ;
	}
	if (PRIVATE (thiz)->completion_list) {
		g_list_free (PRIVATE (thiz)->completion_list) ;
		PRIVATE (thiz)->completion_list = NULL ;
	}

	if (G_OBJECT_CLASS (gv_parent_class)->dispose)
		G_OBJECT_CLASS (gv_parent_class)->dispose (a_this) ;

	PRIVATE (thiz)->dispose_has_run = TRUE ;
}

static void
mlview_entry_finalize (GObject *a_this)
{
	MlViewEntry *thiz = NULL ;

	thiz = MLVIEW_ENTRY (a_this) ;
	g_return_if_fail (thiz && MLVIEW_IS_ENTRY (thiz)
	                  && PRIVATE (thiz)) ;

	if (PRIVATE (thiz)) {
		g_free (PRIVATE (thiz)) ;
		PRIVATE (thiz) = NULL ;
	}
	if (G_OBJECT_CLASS (gv_parent_class)->finalize)
		G_OBJECT_CLASS (gv_parent_class)->finalize (a_this) ;
}

static enum MlViewStatus
compute_completion_list_popup_menu_position (MlViewEntry *a_this,
        gint a_word_start_index,
        gint a_word_end_index,
        gint *a_x,
        gint *a_y)
{
	GtkWidget *toplevel = NULL ;
	GdkWindow *win = NULL ;
	PangoLayout *layout = NULL ;
	gint layout_x_offset = 0, layout_y_offset = 0,
	                       root_x = 0 , root_y = 0,
	                                             x = 0, y = 0 ;
	PangoRectangle rect = {0}, rect2 = {0}, rect3 = {0} ;

	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_ENTRY (a_this)
	                      && a_x && a_y,
	                      MLVIEW_BAD_PARAM_ERROR) ;

	toplevel = gtk_widget_get_toplevel (GTK_WIDGET (a_this)) ;
	g_return_val_if_fail (toplevel, MLVIEW_BAD_PARAM_ERROR) ;
	win = toplevel->window ;
	g_return_val_if_fail (win, MLVIEW_BAD_PARAM_ERROR) ;
	gdk_window_get_root_origin (win, &root_x, &root_y) ;


	layout = gtk_entry_get_layout (GTK_ENTRY (a_this)) ;
	gtk_entry_get_layout_offsets (GTK_ENTRY (a_this), &layout_x_offset,
	                              &layout_y_offset) ;

	pango_layout_index_to_pos (layout,
	                           a_word_start_index,
	                           &rect) ;

	rect.x = PANGO_PIXELS (rect.x) ;
	rect.y = PANGO_PIXELS (rect.y) ;
	rect.x += layout_x_offset ;
	rect.y += layout_y_offset ;
	/*increment rect.y by the layout height*/
	pango_layout_get_pixel_extents (layout,
	                                &rect3,
	                                &rect2) ;
	rect.y += rect2.height * 2 +
	          GTK_WIDGET (a_this)->style->ythickness * 2 ;

#ifdef MLVIEW_VERBOSE

	g_print ("Word start index: %d", a_word_start_index) ;
	g_print ("rect.x: %d, rect.y: %d. In file %s at line %d\n",
	         rect.x, rect.y, __FILE__, __LINE__) ;
#endif

	gtk_widget_translate_coordinates (GTK_WIDGET (a_this),
	                                  toplevel,
	                                  rect.x, rect.y,
	                                  &x, &y) ;

	rect.x = x + root_x ;
	rect.y = y + root_y ;


#ifdef MLVIEW_VERBOSE

	g_print ("After translation => rect.x: %d, rect.y: %d. In file %s at line %d\n",
	         rect.x, rect.y, __FILE__, __LINE__) ;
#endif

	*a_x = rect.x ;
	*a_y = rect.y ;

	return MLVIEW_OK ;
}

static enum MlViewStatus
get_completion_menu (MlViewEntry *a_this,
                     GList *a_menu_strings,
                     GtkWindow **a_popup_win,
                     GtkTreeView **a_popup_tree_view)
{
	enum MlViewStatus status = MLVIEW_OK ;
	gboolean menu_initialized = FALSE ;

	g_return_val_if_fail (a_this && MLVIEW_IS_ENTRY (a_this)
	                      && PRIVATE (a_this),
	                      MLVIEW_BAD_PARAM_ERROR) ;

	if (!PRIVATE (a_this)->popup_win) {
		GtkWindow *win = NULL ;

		status = build_new_completion_menu (a_this, a_menu_strings,
		                                    &PRIVATE (a_this)->popup_tree_view) ;
		g_return_val_if_fail (status == MLVIEW_OK
		                      && PRIVATE (a_this)->popup_tree_view,
		                      MLVIEW_BAD_PARAM_ERROR) ;

		win = GTK_WINDOW (gtk_window_new (GTK_WINDOW_POPUP)) ;
		gtk_container_add (GTK_CONTAINER (win),
		                   GTK_WIDGET (PRIVATE (a_this)->popup_tree_view)) ;
		PRIVATE (a_this)->popup_win = win ;
		menu_initialized = TRUE ;
	}
	if (menu_initialized == FALSE)
		set_completion_menu_content
		(a_this, a_menu_strings,
		 PRIVATE (a_this)->popup_tree_view) ;

	*a_popup_win = PRIVATE (a_this)->popup_win ;
	*a_popup_tree_view = PRIVATE (a_this)->popup_tree_view ;

	return MLVIEW_OK ;
}

static enum MlViewStatus
set_completion_menu_content (MlViewEntry *a_this,
                             GList *a_menu_strings,
                             GtkTreeView *a_menu)
{
	GtkTreeModel *model = NULL ;
	GtkTreeIter iter = {0} ;
	GList *cur_list_item = NULL ;
	gboolean is_empty = TRUE ;

	g_return_val_if_fail (a_this && MLVIEW_IS_ENTRY (a_this),
	                      MLVIEW_BAD_PARAM_ERROR) ;
	g_return_val_if_fail (a_menu_strings && a_menu,
	                      MLVIEW_BAD_PARAM_ERROR) ;

	model = gtk_tree_view_get_model (a_menu) ;
	g_return_val_if_fail (model, MLVIEW_ERROR) ;

	gtk_list_store_clear (GTK_LIST_STORE (model)) ;

	for (cur_list_item = a_menu_strings ;
	        cur_list_item; cur_list_item = cur_list_item->next) {
		if (cur_list_item->data) {
			gtk_list_store_append (GTK_LIST_STORE (model),
			                       &iter) ;
			gtk_list_store_set (GTK_LIST_STORE (model),
			                    &iter,
			                    0, cur_list_item->data,
			                    -1) ;
			if (is_empty == TRUE)
				is_empty = FALSE ;
		}
	}
	if (is_empty == TRUE)
		return MLVIEW_ERROR ;

	return MLVIEW_OK ;
}

static enum MlViewStatus
build_new_completion_menu (MlViewEntry *a_this,
                           GList *a_menu_strings,
                           GtkTreeView **a_menu)
{
	GtkTreeModel *model = NULL ;
	GtkTreeView *tree_view = NULL ;
	GtkTreeIter iter = {0} ;
	GList *cur_list_item = NULL ;
	gboolean is_empty = TRUE ;
	GtkCellRenderer *renderer = NULL ;
	GtkTreeSelection *selection = NULL ;

	g_return_val_if_fail (a_this && MLVIEW_IS_ENTRY (a_this)
	                      && a_menu_strings
	                      && a_menu,
	                      MLVIEW_BAD_PARAM_ERROR) ;

	model = GTK_TREE_MODEL (gtk_list_store_new (1, G_TYPE_STRING)) ;
	for (cur_list_item = a_menu_strings ;
	        cur_list_item; cur_list_item = cur_list_item->next) {
		if (cur_list_item->data) {
			gtk_list_store_append (GTK_LIST_STORE (model),
			                       &iter) ;
			gtk_list_store_set (GTK_LIST_STORE (model),
			                    &iter,
			                    0, cur_list_item->data,
			                    -1) ;
			if (is_empty == TRUE)
				is_empty = FALSE ;
		}
	}
	if (is_empty == TRUE)
		return MLVIEW_ERROR ;
	tree_view = GTK_TREE_VIEW (gtk_tree_view_new_with_model (model)) ;
	gtk_tree_view_set_headers_visible (tree_view, FALSE) ;
	renderer = gtk_cell_renderer_text_new () ;
	gtk_tree_view_insert_column_with_attributes (tree_view,
	        -1, NULL,
	        renderer,
	        "text", 0,
	        NULL) ;
	selection = gtk_tree_view_get_selection (tree_view) ;
	g_return_val_if_fail (selection, MLVIEW_ERROR) ;
	gtk_tree_selection_set_mode (selection, GTK_SELECTION_SINGLE) ;
	g_signal_connect (G_OBJECT (selection),
	                  "changed",
	                  G_CALLBACK (signal_changed_cb),
	                  a_this) ;
	*a_menu = tree_view ;
	if (a_menu_strings->data) {
		PRIVATE (a_this)->cur_compl_str = (gchar *) a_menu_strings->data ;
	}
	return MLVIEW_OK ;
}

static enum MlViewStatus
select_next_or_prev_menu_item (MlViewEntry *a_this,
                               gboolean a_next)
{
	enum MlViewStatus status = MLVIEW_OK ;
	GtkTreeModel *model = NULL ;
	GtkTreeSelection *selection = NULL ;
	GtkTreeIter iter = {0} ;
	gchar *str = NULL ;
	GtkTreePath *tree_path = NULL ;

	g_return_val_if_fail (a_this
	                      && MLVIEW_ENTRY (a_this)
	                      && PRIVATE (a_this),
	                      MLVIEW_BAD_PARAM_ERROR) ;

	model = gtk_tree_view_get_model (PRIVATE (a_this)->popup_tree_view) ;
	if (!model) {
		status = MLVIEW_ERROR ;
		mlview_utils_trace_debug ("model failed") ;
		goto cleanup ;
	}

	if (mlview_entry_is_popup_win_visible (a_this) == FALSE)
		return MLVIEW_OK ;

	selection = gtk_tree_view_get_selection
	            (PRIVATE (a_this)->popup_tree_view) ;

	if (gtk_tree_selection_get_selected (selection, NULL, &iter) == FALSE) {
		gtk_tree_model_get_iter_first (model, &iter) ;
		gtk_tree_selection_select_iter (selection, &iter) ;
	}

	str = gtk_tree_model_get_string_from_iter (model, &iter) ;
	g_return_val_if_fail (str, MLVIEW_ERROR) ;
	tree_path = gtk_tree_path_new_from_string (str) ;
	if (!tree_path) {
		status = MLVIEW_ERROR ;
		mlview_utils_trace_debug ("model failed") ;
		goto cleanup ;
	}
	if (a_next == TRUE) {
		gtk_tree_path_next (tree_path) ;
	} else {
		gtk_tree_path_prev (tree_path) ;
	}
	gtk_tree_selection_select_path (selection, tree_path) ;

cleanup:
	if (str) {
		g_free (str) ;
		str = NULL ;
	}
	if (tree_path) {
		gtk_tree_path_free (tree_path) ;
		tree_path = NULL ;
	}
	return status ;
}

static gboolean
key_press_event_cb (MlViewEntry *a_this,
                    GdkEventKey *a_event,
                    gpointer a_user_data)
{
	switch (a_event->keyval) {

	case GDK_Up:
		mlview_entry_select_prev_popup_menu_item (a_this) ;
		return TRUE ;

	case GDK_Down:
		mlview_entry_select_next_popup_menu_item (a_this) ;
		return TRUE ;
	case GDK_Return:
		if (mlview_entry_is_popup_win_visible (a_this) == TRUE) {
#ifdef MLVIEW_VERBOSE
			g_print ("Gonna hide the menu \n") ;
#endif

			mlview_entry_hide_word_completion_menu (a_this) ;
			mlview_entry_set_current_word_to_current_completion_string (a_this) ;

#ifdef MLVIEW_VERBOSE

			g_print ("hidded the menu\n") ;
#endif

			return TRUE ;
		}
		break ;
	case GDK_Escape:
		break ;
	default:
		break ;
	}
	return FALSE ;
}

static void
signal_changed_cb (GtkTreeSelection *a_sel,
                   MlViewEntry *a_entry)
{
	GtkTreeIter iter = {0} ;
	GtkTreeModel *model = NULL ;

	g_return_if_fail (a_sel && a_entry
	                  && MLVIEW_IS_ENTRY (a_entry)
	                  && PRIVATE (a_entry)) ;

	if (PRIVATE (a_entry)->popup_tree_view) {
		model = gtk_tree_view_get_model
		        (PRIVATE (a_entry)->popup_tree_view) ;
	} else {
		return ;
	}
	if (gtk_tree_selection_get_selected (a_sel, NULL, &iter) == FALSE)
		return ;
	gtk_tree_model_get (model, &iter,
	                    0, &PRIVATE (a_entry)->cur_compl_str,  -1) ;
}


/****************
 *Public methods
 ****************/

GType
mlview_entry_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo type_info = {
		                                       sizeof (MlViewEntryClass),
		                                       NULL, NULL,
		                                       (GClassInitFunc)
		                                       mlview_entry_class_init,
		                                       NULL, NULL,
		                                       sizeof (MlViewEntry),
		                                       0,
		                                       (GInstanceInitFunc)
		                                       mlview_entry_init
		                                   };
		type = g_type_register_static (GTK_TYPE_ENTRY,
		                               "MlViewEntry",
		                               &type_info, (GTypeFlags) 0);
	}
	return type;
}

/**
 *Instanciates the #MlViewEntry
 *@return the newly instantiated #MlViewEntry
 */
GtkWidget *
mlview_entry_new (void)
{
	GtkWidget *result = NULL ;
	result = (GtkWidget *) g_object_new (MLVIEW_TYPE_ENTRY, NULL) ;
	g_return_val_if_fail (result, NULL) ;
	return result ;
}


enum MlViewStatus
mlview_entry_popup_word_completion_menu (MlViewEntry *a_this,
        gint a_word_start_index,
        gint a_word_end_index)
{
	GtkTreeView *tree_view = NULL ;
	GtkWindow * win = NULL ;
	gint x = 0, y = 0 ;

	g_return_val_if_fail (a_this && MLVIEW_IS_ENTRY (a_this)
	                      && PRIVATE (a_this),
	                      MLVIEW_BAD_PARAM_ERROR) ;

	if (!PRIVATE (a_this)->completion_list)
		return MLVIEW_OK ;

	win = GTK_WINDOW (gtk_window_new (GTK_WINDOW_POPUP)) ;

	get_completion_menu (a_this, PRIVATE (a_this)->completion_list,
	                     &win, &tree_view) ;
	g_return_val_if_fail (tree_view, MLVIEW_ERROR) ;

	/*now, position the window win and pop it up*/
	compute_completion_list_popup_menu_position (a_this, 0,
	        0, &x, &y) ;
	gtk_window_move (win, x, y) ;
	gtk_widget_show_all (GTK_WIDGET (win)) ;

	return MLVIEW_OK ;
}

enum MlViewStatus
mlview_entry_hide_word_completion_menu (MlViewEntry *a_this)
{
	g_return_val_if_fail (a_this && MLVIEW_IS_ENTRY (a_this)
	                      && PRIVATE (a_this),
	                      MLVIEW_BAD_PARAM_ERROR) ;

	if (PRIVATE (a_this)->popup_win) {
		/*gtk_widget_hide (GTK_WIDGET (PRIVATE (a_this)->popup_win)) ;*/
		gtk_widget_destroy (GTK_WIDGET (PRIVATE (a_this)->popup_win)) ;
		PRIVATE (a_this)->popup_win = NULL ;
		PRIVATE (a_this)->popup_tree_view = NULL ;
		PRIVATE (a_this)->completion_list = NULL ;

	}
	return MLVIEW_OK ;
}

gboolean
mlview_entry_is_popup_win_visible (MlViewEntry *a_this)
{
	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_ENTRY (a_this)
	                      && PRIVATE (a_this),
	                      FALSE) ;

	if (PRIVATE (a_this)->popup_win == NULL)
		return FALSE ;
	if (!GTK_WIDGET_VISIBLE (PRIVATE (a_this)->popup_win))
		return FALSE ;
	return TRUE ;
}



enum MlViewStatus
mlview_entry_select_next_popup_menu_item (MlViewEntry *a_this)
{

	return select_next_or_prev_menu_item (a_this, TRUE) ;
}


enum MlViewStatus
mlview_entry_select_prev_popup_menu_item (MlViewEntry *a_this)
{
	return select_next_or_prev_menu_item (a_this, FALSE) ;
}


enum MlViewStatus
mlview_entry_set_current_word_to_current_completion_string (MlViewEntry *a_this)
{
	gchar *entry_text = NULL, *word_start = NULL, *word_end = NULL, *str = NULL ;
	gint pos = 0, character_index = 0, byte_index = 0 ;

	g_return_val_if_fail (a_this && MLVIEW_IS_ENTRY (a_this),
	                      MLVIEW_BAD_PARAM_ERROR) ;

	entry_text = gtk_editable_get_chars (GTK_EDITABLE (a_this), 0, -1) ;

	character_index = gtk_editable_get_position (GTK_EDITABLE (a_this)) ;
	if (character_index)
		character_index -- ;
	str = g_utf8_offset_to_pointer (entry_text, character_index) ;
	byte_index = str - entry_text ;
	mlview_utils_get_current_word_bounds
	(entry_text, strlen (entry_text),
	 byte_index, &word_start, &word_end) ;
	g_return_val_if_fail (word_start && word_end,
	                      MLVIEW_BAD_PARAM_ERROR) ;

	if (word_start == word_end) {
		if (*word_start == '/'
		        || *word_start == '>'
		        || *word_end == '<') {
			goto cleanup ;
		}
	}
	gtk_editable_delete_text (GTK_EDITABLE (a_this),
	                          word_start - entry_text,
	                          word_end - word_start + 1) ;
	pos = word_start - entry_text ;
	gtk_editable_insert_text (GTK_EDITABLE (a_this),
	                          PRIVATE (a_this)->cur_compl_str,
	                          strlen (PRIVATE (a_this)->cur_compl_str),
	                          &pos) ;
cleanup:
	if (entry_text) {
		g_free (entry_text)  ;
		entry_text = NULL ;
	}

	return MLVIEW_OK ;
}

enum MlViewStatus
mlview_entry_set_completion_list (MlViewEntry *a_this,
                                  GList *a_completion_list)
{
	g_return_val_if_fail (a_this && MLVIEW_IS_ENTRY (a_this)
	                      && PRIVATE (a_this),
	                      MLVIEW_BAD_PARAM_ERROR) ;

	if (PRIVATE (a_this)->completion_list) {
		g_list_free (PRIVATE (a_this)->completion_list) ;
		PRIVATE (a_this)->completion_list = NULL ;
	}
	PRIVATE (a_this)->completion_list = a_completion_list ;
	return MLVIEW_OK ;
}

enum MlViewStatus
mlview_entry_get_completion_list (MlViewEntry *a_this,
                                  GList **a_completion_list)
{
	g_return_val_if_fail (a_this && MLVIEW_IS_ENTRY (a_this)
	                      && PRIVATE (a_this)
	                      && a_completion_list,
	                      MLVIEW_BAD_PARAM_ERROR) ;

	*a_completion_list = PRIVATE (a_this)->completion_list ;
	return MLVIEW_OK ;
}

