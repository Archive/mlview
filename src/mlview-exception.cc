/* -*- Mode: C++; indent-tabs-mode:nil; c-basic-offset: 8-*- */
/*
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute 
 *it and/or modify it under the terms of 
 *the GNU General Public License as published by the 
 *Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYING file for copyright information.
 */
#include "mlview-exception.h"

namespace mlview
{

Exception::Exception (const gchar *a_reason): m_reason (a_reason)
{}

Exception::Exception (const UString &a_reason): m_reason (a_reason)
{}

Exception::Exception (const Exception &an_exception)
{
	m_reason = an_exception.m_reason ;
}

Exception::~Exception () throw ()
{}

Exception&
Exception::operator= (Exception const &an_other)
{
	if (this == &an_other)
		return *this ;
	m_reason = an_other.m_reason ;
	return *this ;
}

const char *
Exception::what () const throw ()
{
	return m_reason.c_str () ;
}

std::ostream& operator<< (std::ostream &a_outstream, Exception& an_exception)
{
	a_outstream << "Exception: " << an_exception ;
	return a_outstream ;
}

Exception::operator const gchar* () const
{
	return m_reason.c_str () ;
}

}//namespace mlview

