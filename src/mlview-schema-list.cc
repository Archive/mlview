/* -*- Mode: C; indent-tabs-mode:nil; c-basic-offset: 8-*- */
/*
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute 
 *it and/or modify it under the terms of 
 *the GNU General Public License as published by the 
 *Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */

#include <string.h>

#include "mlview-schema-list.h"
#include "mlview-marshal.h"
#include "mlview-utils.h"

#define PRIVATE(mlview_schema_list) ((mlview_schema_list)->priv)

/**
 * @file
 * This is the definition of the #MlViewSchemaList class. 
 */
static GObjectClass *gv_parent_class = NULL;

struct _MlViewSchemaListPrivate
{
	gboolean dispose_has_run;
	GHashTable *schemas;
};

enum {
    SCHEMA_ASSOCIATED = 0,
    SCHEMA_UNASSOCIATED,
    NUMBER_OF_SIGNALS
};

static guint gv_signals [NUMBER_OF_SIGNALS] = { 0 };

struct ForeachData
{
	MlViewSchemaListFunc func;
	gpointer user_data;
};

static void
mlview_schema_list_finalize (GObject *a_this)
{
	MlViewSchemaList *thiz = NULL;

	g_return_if_fail (a_this &&
	                  MLVIEW_IS_SCHEMA_LIST (a_this));

	thiz = MLVIEW_SCHEMA_LIST (a_this);

	g_return_if_fail (thiz);

	if (PRIVATE (thiz)) {
		g_free (PRIVATE (thiz));

		PRIVATE (thiz) = NULL;
	}
}

static gboolean
say_yes (gpointer a_key, gpointer a_value,
         gpointer a_user_data)
{
	return TRUE;
}

static void
mlview_schema_list_dispose (GObject *a_this)
{
	MlViewSchemaList *sl = NULL;

	g_return_if_fail (a_this &&
	                  MLVIEW_IS_SCHEMA_LIST (a_this));

	sl = MLVIEW_SCHEMA_LIST (a_this);

	g_return_if_fail (sl);

	g_return_if_fail (PRIVATE (sl));

	if (PRIVATE (sl)->dispose_has_run)
		return;

	PRIVATE (sl)->dispose_has_run = TRUE;

	if (PRIVATE (sl)->schemas) {
		g_hash_table_foreach_remove (PRIVATE (sl)->schemas,
		                             (GHRFunc) say_yes,
		                             NULL);

		g_hash_table_destroy (PRIVATE (sl)->schemas);
		PRIVATE (sl)->schemas = NULL;
	}

	if (gv_parent_class->dispose)
		gv_parent_class->dispose (a_this);
}

static void
mlview_schema_list_class_init (MlViewSchemaListClass *a_klass)
{
	GObjectClass *gobject_class = NULL;

	g_return_if_fail (a_klass);

	gv_parent_class = (GObjectClass *) g_type_class_peek_parent (a_klass);

	gobject_class = G_OBJECT_CLASS (a_klass);

	g_return_if_fail (gobject_class);

	gobject_class->dispose = mlview_schema_list_dispose;
	gobject_class->finalize = mlview_schema_list_finalize;

	gv_signals [SCHEMA_ASSOCIATED] =
	    g_signal_new ("schema-associated",
	                  G_TYPE_FROM_CLASS (gobject_class),
	                  G_SIGNAL_RUN_FIRST,
	                  G_STRUCT_OFFSET
	                  (MlViewSchemaListClass,
	                   schema_associated),
	                  NULL, NULL,
	                  mlview_marshal_VOID__POINTER,
	                  G_TYPE_NONE, 1, G_TYPE_POINTER);

	gv_signals [SCHEMA_UNASSOCIATED] =
	    g_signal_new ("schema-unassociated",
	                  G_TYPE_FROM_CLASS (gobject_class),
	                  G_SIGNAL_RUN_FIRST,
	                  G_STRUCT_OFFSET
	                  (MlViewSchemaListClass,
	                   schema_unassociated),
	                  NULL, NULL,
	                  mlview_marshal_VOID__POINTER,
	                  G_TYPE_NONE, 1, G_TYPE_POINTER);
}

static void
mlview_schema_list_init (MlViewSchemaList *a_schemas)
{
	g_return_if_fail (a_schemas && !PRIVATE (a_schemas));

	PRIVATE (a_schemas) = (MlViewSchemaListPrivate *) g_try_malloc (sizeof (MlViewSchemaListPrivate));

	if (!PRIVATE (a_schemas)) {
		mlview_utils_trace_debug ("malloc () failed, system may be out of memory");

		return;
	}

	memset (PRIVATE (a_schemas), 0,
	        sizeof (MlViewSchemaListPrivate));

	PRIVATE (a_schemas)->schemas =
	    g_hash_table_new_full (g_str_hash, g_str_equal,
	                           NULL,
	                           (GDestroyNotify) mlview_schema_unref);

	g_return_if_fail (PRIVATE (a_schemas)->schemas);
}

GType
mlview_schema_list_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo type_info = {
		                                       sizeof (MlViewSchemaListClass),
		                                       NULL, NULL,
		                                       (GClassInitFunc) mlview_schema_list_class_init,
		                                       NULL, NULL,
		                                       sizeof (MlViewSchemaList),
		                                       0,
		                                       (GInstanceInitFunc) mlview_schema_list_init
		                                   };

		type = g_type_register_static (G_TYPE_OBJECT,
		                               "MlViewSchemaList",
		                               &type_info, (GTypeFlags) 0);
	}

	return type;
}

gboolean
mlview_schema_list_add_schema (MlViewSchemaList *a_this,
                               MlViewSchema *a_schema)
{
	MlViewSchema *found = NULL;
	gchar *url = NULL;

	g_return_val_if_fail (a_this && MLVIEW_IS_SCHEMA_LIST (a_this)
	                      && PRIVATE (a_this) && PRIVATE (a_this)->schemas,
	                      FALSE);
	g_return_val_if_fail (a_schema, FALSE);

	url = mlview_schema_get_url (a_schema);

	g_return_val_if_fail (url, FALSE);

	found = (MlViewSchema *)  g_hash_table_lookup (PRIVATE (a_this)->schemas, url);

	if (found)
		return FALSE;

	g_hash_table_insert (PRIVATE (a_this)->schemas, url, a_schema);

	mlview_schema_ref (a_schema);

	g_signal_emit (G_OBJECT (a_this),
	               gv_signals [SCHEMA_ASSOCIATED], 0, a_schema);

	return TRUE;
}

MlViewSchema *
mlview_schema_list_lookup_by_url (MlViewSchemaList *a_this,
                                  const gchar *a_url)
{
	MlViewSchema *schema = NULL;

	g_return_val_if_fail (a_this && MLVIEW_IS_SCHEMA_LIST (a_this)
	                      && PRIVATE (a_this) && PRIVATE (a_this)->schemas,
	                      NULL);
	g_return_val_if_fail (a_url, NULL);

	schema = (MlViewSchema *) g_hash_table_lookup (PRIVATE (a_this)->schemas, a_url);

	return schema;
}

gboolean
mlview_schema_list_remove_schema_by_url (MlViewSchemaList *a_this,
        const gchar *a_url)
{
	gboolean found = FALSE;
	MlViewSchema *schema = NULL;

	g_return_val_if_fail (a_this && MLVIEW_IS_SCHEMA_LIST (a_this)
	                      && PRIVATE (a_this) && PRIVATE (a_this)->schemas,
	                      FALSE);
	g_return_val_if_fail (a_url, FALSE);

	schema = (MlViewSchema *) g_hash_table_lookup (PRIVATE (a_this)->schemas, a_url);

	if (schema) {
		mlview_schema_ref (schema);

		found = g_hash_table_remove (PRIVATE (a_this)->schemas, a_url);

		g_return_val_if_fail (found, FALSE);

		g_signal_emit (G_OBJECT (a_this),
		               gv_signals [SCHEMA_UNASSOCIATED], 0, schema);

		mlview_schema_unref (schema);
		schema = NULL;

		return TRUE;
	}

	return FALSE;
}

MlViewSchemaList *
mlview_schema_list_new (void)
{
	GObject *obj = NULL;
	MlViewSchemaList *list = NULL;

	obj = (GObject *) g_object_new (MLVIEW_TYPE_SCHEMA_LIST, NULL);

	g_return_val_if_fail (obj && MLVIEW_IS_SCHEMA_LIST (obj), NULL);

	list = MLVIEW_SCHEMA_LIST (obj);

	g_return_val_if_fail (list, NULL);

	return list;
}

static void
foreach_func (gchar *a_url, MlViewSchema *a_schema,
              struct ForeachData *a_data)
{
	g_return_if_fail (a_schema && a_data &&
	                  a_data->func);

	a_data->func (a_schema, a_data->user_data);
}

void
mlview_schema_list_foreach (MlViewSchemaList *a_schemas,
                            MlViewSchemaListFunc a_func, gpointer a_user_data)
{
	struct ForeachData *data = NULL;

	g_return_if_fail (a_schemas && MLVIEW_IS_SCHEMA_LIST (a_schemas));
	g_return_if_fail (PRIVATE (a_schemas) && PRIVATE (a_schemas)->schemas);
	g_return_if_fail (a_func);

	data = (ForeachData *) g_try_malloc (sizeof (struct ForeachData));

	if (!data) {
		mlview_utils_trace_debug ("g_try_malloc failed");

		return;
	}

	data->user_data = a_user_data;
	data->func = a_func;

	g_hash_table_foreach (PRIVATE (a_schemas)->schemas, (GHFunc) foreach_func, data);

	g_free (data);
	data = NULL;
}


enum MlViewStatus
mlview_schema_list_get_size (MlViewSchemaList *a_this,
                             guint *a_size)
{
	gint result = 0 ;
	g_return_val_if_fail (a_this && MLVIEW_IS_SCHEMA_LIST (a_this)
	                      && PRIVATE (a_this) && a_size,
	                      MLVIEW_BAD_PARAM_ERROR) ;

	if (PRIVATE (a_this)->schemas) {
		result = g_hash_table_size (PRIVATE (a_this)->schemas) ;
	}
	if (result >= 0)
		*a_size = result ;
	else
		return MLVIEW_ERROR ;

	return MLVIEW_OK ;
}

