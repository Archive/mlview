/* -*- Mode: C++; indent-tabs-mode: true ; c-basic-offset: 4-*- */

/*This file is part of GNU MlView
 *
 *GNU MlView is free software; 
 *you can redistribute it and/or modify it under the terms of 
 *the GNU General Public License as 
 *published by the Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope 
 *that it will be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */

#ifndef __MLVIEW_VALIDATOR_H__
#define __MLVIEW_VALIDATOR_H__

#include <libxml/xmlerror.h>

#include "mlview-xml-document.h"
#include "mlview-validation-output.h"
#include "mlview-file-descriptor.h"
#include "mlview-app-context.h"
#include "mlview-schema.h"
#include "mlview-exception.h"
#include "mlview-object.h"

namespace mlview
{
struct ValidatorPriv ;

class Validator: public Object
{
	friend struct ValidatorPriv ;

	ValidatorPriv *m_priv ;

	enum MlViewValidatorStatus {
		VALIDATOR_ERROR = -1,
		VALIDATOR_VALID_DOC,
		VALIDATOR_INVALID_DOC
	} ;

	//forbid assignation
	Validator& operator= (Validator const&) ;

public:

	Validator () ;
	Validator (Validator const &a_validator) ;

	virtual ~Validator () ;

	enum MlViewStatus compile_element_content_model_into_automata
	(xmlElementContent* a_elementContentModel) ;

	enum MlViewStatus evaluate_element_name_seq_validity
	(const vector<UString> &a_name_seq,
	 long &a_error_index) ;

	static enum MlViewStatus validate_with_dtd (MlViewXMLDocument *a_doc,
			xmlDtdPtr a_dtd,
			ValidationOutput **a_output);

	static enum MlViewStatus validate_with_rng (MlViewXMLDocument *a_doc,
			xmlRelaxNGPtr a_rng,
			ValidationOutput **a_output);

	static enum MlViewStatus validate_with_xsd (MlViewXMLDocument *a_doc,
			xmlSchemaPtr a_xsd,
			ValidationOutput **a_output);

	static enum MlViewStatus validate_with_schema (MlViewXMLDocument *a_doc,
			MlViewSchema *a_schema,
			ValidationOutput **a_output);
protected:

	enum MlViewStatus compile_element_content_model_into_automata_real
	(xmlElementContent* a_elementContentModel) ;

	enum MlViewStatus load_a_dtd (const UString &a_dtd_path,
			xmlDtd**a_dtd) ;

}
; //end class Validator
}//end namespace mlview

#endif
