/*
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute 
 *it and/or modify it under the terms of 
 *the GNU General Public License as published by the 
 *Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */

#include <gtk/gtk.h>

#ifndef __MLVIEW_FILE_SELECTION_DIALOG_H__
#define __MLVIEW_FILE_SELECTION_DIALOG_H__

/**
 *@file
 *The declaration of the file selector class used
 *in mlview.
 */

#define MLVIEW_TYPE_FILE_SELECTION (mlview_file_selection_get_type())
#define MLVIEW_FILE_SELECTION(widget) (GTK_CHECK_CAST((widget), MLVIEW_TYPE_FILE_SELECTION, MlViewFileSelection))
#define MLVIEW_FILE_SELECTION_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass), MLVIEW_TYPE_FILE_SELECTION, MlViewFileSelectionClass))
#define MLVIEW_IS_FILE_SELECTION(widget)  (GTK_CHECK_TYPE((widget), MLVIEW_TYPE_FILE_SELECTION))
#define MLVIEW_IS_FILE_SELECTION_CLASS(klass) (GTK_CHECK_CLASS_TYPE)((klass), MLVIEW_TYPE_FILE_SELECTION)

enum MLVIEW_SELECTED_BUTTON {
    OK_BUTTON = 1,
    CANCEL_BUTTON,
    WINDOW_CLOSED,
    NO_BUTTON_SELECTED
};

typedef struct _MlViewFileSelection MlViewFileSelection;
typedef struct _MlViewFileSelectionClass
			MlViewFileSelectionClass;
typedef struct _MlViewFileSelectionPrivate
			MlViewFileSelectionPrivate;


/**
 *The file selector class used by mlview.
 *This class derives from GtkFileSelection.
 */
struct _MlViewFileSelection
{
	GtkFileSelection file_selection;
	MlViewFileSelectionPrivate *priv;
};

guint mlview_file_selection_get_type (void);
GtkWidget *mlview_file_selection_new (void);
gint mlview_file_selection_run (MlViewFileSelection * a_file_selection,
                                gboolean a_close_after);

struct _MlViewFileSelectionClass
{
	GtkFileSelectionClass parent_class;
};


#endif /*__MLVIEW_FILE_SELECTION_DIALOG_H__*/
