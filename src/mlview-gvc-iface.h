/* -*- Mode: C++; indent-tabs-mode: true; c-basic-offset: 4-*- */

/**
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute it
 *and/or modify it under the terms of
 *the GNU General Public License as published
 *by the Free Software Foundation; either version 2,
 *or (at your option) any later version.
 *
 *MlView is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY;
 *without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the
 *GNU General Public License along with MlView;
 *see the file COPYING. If not, write to the Free Software Foundation, Inc.,
 *59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */
#ifndef __MLVIEW_GVC_IFACE_H__
#define __MLVIEW_GVC_IFACE_H__

#include <gtkmm.h>
#include "mlview-object.h"
#include "mlview-app-context.h"
#include "mlview-iview.h"

namespace mlview
{

        struct GVCIfacePriv ;
/**
 * MlView Graphical View Container class declaration
 */
class GVCIface : public Object
{
	friend struct GVCIfacePriv ;
	friend class GVCFactory ;

	GVCIfacePriv *m_priv ;
protected:

	//forbid instanciation, copy, assignment
	GVCIface () ;
	GVCIface (GVCIface const&) ;
	GVCIface& operator= (GVCIface const&) ;

public :
	virtual ~GVCIface () ;

	virtual enum MlViewStatus insert_view (IView *a_view,
	                                       long a_index=-1/*append*/) = 0;

	virtual enum MlViewStatus remove_view (IView *a_view) = 0;

	virtual Gtk::Widget* get_embeddable_container_widget () = 0 ;

	virtual IView* get_cur_view () ;

	virtual void set_cur_view (IView *a_view, bool a_emit_signal) ;

	//*******
	//signals
	//********
	sigc::signal2<void,
	IView* /*swapped in*/,
	IView* /*swapped out*/>& signal_views_swapped () ;

}
;//end GVCIface

}//end namespace mlview
#endif //__MLVIEW_GVC_IFACE_H__
