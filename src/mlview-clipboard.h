/* -*- Mode: C++; indent-tabs-mode:nil; c-basic-offset: 8-*- */
/*
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute 
 *it and/or modify it under the terms of 
 *the GNU General Public License as published by the 
 *Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */

#ifndef __MLVIEW_CLIPBOARD_H__
#define __MLVIEW_CLIPBOARD_H__

#include <list>
#include "mlview-utils.h"
#include "mlview-ustring.h"
#include "mlview-object.h"
#include "mlview-parsing-utils.h"

#ifndef DEFAULT_CLIPBOARD_BUFFER_NAME
# define DEFAULT_CLIPBOARD_BUFFER_NAME "default-clipboard-buffer"
#else
# error "argh!! the macro DEFAULT_CLIPBOARD_BUFFER_NAME is already define"
#endif
using namespace std ;

namespace mlview
{

struct ClipboardPriv ;
///******************************************
/// \brief the clipboard abstraction used in MlView
/// this class is used to store text that has
/// been cut or copied. The text is actually
/// copied into, or from named buffers.
///***************************************
class Clipboard: public Object
{

	friend struct ClipboardPriv ;
	ClipboardPriv *m_priv ;

public:

	Clipboard () ;

	Clipboard (Clipboard const &a_clipboard) ;

	Clipboard& operator= (Clipboard const &a_clipboard) ;

	virtual ~Clipboard () ;

	//*********************************
	///\brief put text in clipboard
	///
	/// puts a text in a buffer in the clipboard.
	///
	/// \param a_content the text to put in the clipboard
	///
	/// \param a_buffer_name the name of the
	/// buffer where the text is stored
	/// into the clipboard. The buffer
	/// that has the name DEFAULT_CLIPBOARD_BUFFER_NAME
	/// exports its content to the X clipboard.
	/// Also, any text that has been
	/// copied into the default X clipboard
	/// will be available in the buffer
	/// named DEFAULT_CLIPBOARD_BUFFER_NAME
	///
	/// \return MLVIEW_OK upon successful
	/// completion, an error code otherwise.
	//**********************************
	enum MlViewStatus put (const UString &a_content,
			       const UString &a_buffer_name) ;

	//*********************************
	///\brief put text in clipboard
	///
	/// put a text in the buffer that has the name
	/// DEFAULT_CLIPBOARD_BUFFER_NAME. The content of
	/// this buffer is put in the X clipboard.
	///
	/// \param a_content the text to put in the clipboard
	///
	/// \return MLVIEW_OK upon successful
	/// completion, an error code otherwise.
	//**********************************
	enum MlViewStatus put (const UString &a_content) ;

	//***************
	/// \brief puts an xml node in the clipboard
	/// serializes the xml fragment and put it into
	/// a clipboard buffer
	///
	/// \param a_xml_node the node tu put in clipboard
	/// \param a_buffer_name the name of the buffer to
	///        to put the node into
	/// \return MLVIEW_OK upon successful completion, an error
	///  code otherwise
	//****************
	enum MlViewStatus put (const xmlNode *a_xml_node,
		               const UString &a_buffer_name) ;

	//***************
	/// \brief puts an xml node in the default clipboard buffer
	/// serializes the xml fragment and put it into
	/// the default buffer
	///
	/// \param a_xml_node the node tu put in clipboard
	/// \return MLVIEW_OK upon successful completion, an error
	///  code otherwise
	//****************
	enum MlViewStatus put (const xmlNode *a_xml_node) ;

	//******************
	///try to deserialize the content the default clipboard buffer
	/// \param the xml document the deserialized node will belong to.
	///        this is needed for proper deserialisation.
	/// \return the  deserialized xml node, NULL if no deserialisation was
	///         possible
	//******************
	xmlNode* get (const xmlDoc *a_doc) ;

	//******************
	///try to deserialize the content the default clipboard buffer
	/// \param the xml document the deserialized node will belong to.
	///        this is needed for proper deserialisation.
	/// \return the  deserialized xml node, NULL if no deserialisation was
	///         possible
	//******************
	xmlNode* get (const MlViewXMLDocument*a_doc) ;

	//******************
	///try to deserialize the content a named clipboard buffer
	/// \param the xml document the deserialized node will belong to.
	///        this is needed for proper deserialisation.
	/// \param a_buf_name the name of the buffer to get the node from
	/// \return the  deserialized xml node, NULL if no deserialisation was
	///         possible
	//******************
	xmlNode* get (const xmlDoc *a_doc, const UString &a_buf_name) ;

	//******************
	///try to deserialize the content a named clipboard buffer
	/// \param the xml document the deserialized node will belong to.
	///        this is needed for proper deserialisation.
	/// \param a_buf_name the name of the buffer to get the node from
	/// \return the  deserialized xml node, NULL if no deserialisation was
	///         possible
	//******************
	xmlNode* get (const MlViewXMLDocument*a_doc, const UString &a_buf_name) ;

	//************************************
	/// \brief get text from a buffer
	///
	/// \param a_buffer_name the name
	///  of the buffer to get the text from
	/// \return the content of the buffer
	//*******************************
	UString get (const UString &a_buffer_name) ;

	//************************************
	/// \brief get text from the  default clipboard buffer
	///
	/// get text from the clipboard buffer named 
	/// DEFAULT_CLIPBOARD_BUFFER_NAME.
	/// Text that has been put in the X clipboard
	/// by other processes is available in this buffer.
	/// \return the content of the buffer
	//***********************************
	UString get (void) ;

	//****************************
	/// \brief get the list of buffer names allocated in this clipboard
	///
	/// \return the list of buffer names
	//*****************************
	list<UString> get_list_of_buffer_names () ;

	//********************
	/// \brief test if the clipboard has a certain
	///
	/// \param a_buffer_name the name of
	///   buffer to test.
	/// \return true if the clipboard has a buffer
	/// with the name a_buffer_name
	//********************
	bool has_buffer (const UString &a_buffer_name) ;

protected:

	//********************
	/// \brief puts a text in a native clipboard
	///
	/// \param a_text the text to put the in clipboard
	//*************************
	void put_text_in_default_native_clipboard (const UString &a_text) ;

	//********************
	/// \brief get text from a native clipboard
	/// \return the content of the clipboard
	//************************
	UString get_text_from_default_native_clipboard (void) ;

};//end Class Clipboard
}//namespace mlview

#endif //__MLVIEW_CLIPBOARD_H__
