/* -*- Mode: C; indent-tabs-mode:nil; c-basic-offset: 8-*- */
/*
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute 
 *it and/or modify it under the terms of 
 *the GNU General Public License as published by the 
 *Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */

/**
 *This class is a custom GtkTreeView cell renderer used in the #MlViewTreeEditor
 *Widget. It provides "completion while typing" features.
 */

#ifndef __MLVIEW_CELL_RENDERER_H__
#define __MLVIEW_CELL_RENDERER_H__

#include "mlview-utils.h"

G_BEGIN_DECLS

#define MLVIEW_TYPE_CELL_RENDERER (mlview_cell_renderer_get_type())
#define MLVIEW_CELL_RENDERER(object) (G_TYPE_CHECK_INSTANCE_CAST((object),MLVIEW_TYPE_CELL_RENDERER,MlViewCellRenderer))
#define MLVIEW_CELL_RENDERER_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST((klass),MLVIEW_TYPE_CELL_RENDERER,MlViewCellRendererClass))
#define MLVIEW_IS_CELL_RENDERER(object) (G_TYPE_CHECK_INSTANCE_TYPE((object),MLVIEW_TYPE_CELL_RENDERER))
#define MLVIEW_IS_CELL_RENDERER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass),MLVIEW_TYPE_CELL_RENDERER))

typedef struct _MlViewCellRenderer MlViewCellRenderer ;
typedef struct _MlViewCellRendererClass MlViewCellRendererClass ;
typedef struct _MlViewCellRendererPrivate MlViewCellRendererPrivate ;

/**
 *The #MlViewCellRenderer widget.
 *It provides facilities to do completion when the user
 *types in some text. This widget is used in the element name completion
 *feature of the #MlViewTreeEditor widget.
 */
struct _MlViewCellRenderer
{
	GtkCellRenderer parent ;
	/*<private fields>*/
	MlViewCellRendererPrivate *priv ;
} ;

struct _MlViewCellRendererClass
{
	GtkCellRendererClass parent_class ;

	/**************
	        *signals
	        *************/
	void (*edited) (MlViewCellRenderer *a_this,
	                const gchar         *a_path,
	                const gchar         *a_new_text,
	                gpointer             a_user_data) ;

	/*
	 *this signal is emited each time the
	 *user types in a character during editing
	 */
	void (*word_changed) (MlViewCellRenderer *a_this,
	                      GtkEditable *a_editable /*the editing widget of the cell*/,
	                      const gchar *a_word_start /*the start of the word being edited*/,
	                      const gchar *a_word_end /*end of the word being edited*/,
	                      gboolean a_char_added /*is a character added or suppressed ?*/,
	                      gint a_position /*position (offset of the character added*/,
	                      gint a_word_start_position,
	                      gint a_word_end_position,
	                      gpointer a_user_data) ;

	void (*editing_has_started) (MlViewCellRenderer *a_this,
	                             GtkTreePath *a_path,
	                             GtkEditable *a_editable,
	                             gpointer a_user_data) ;

	gboolean (*select_editable_region) (MlViewCellRenderer *a_this,
	                                    GtkEditable *a_editable,
	                                    gpointer a_user_data) ;

	/*reserved for future extensions*/
	gpointer rfe1 ;
	gpointer rfe2 ;
	gpointer rfe3 ;
	gpointer rfe4 ;
} ;

GType mlview_cell_renderer_get_type (void) ;

GtkCellRenderer *mlview_cell_renderer_new (void) ;

G_END_DECLS

#endif
