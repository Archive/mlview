/* -*- Mode: C++; indent-tabs-mode:true ; c-basic-offset: 4-*- */

/*
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute 
 *it and/or modify it under the terms of 
 *the GNU General Public License as published by the 
 *Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */
#ifndef __MLVIEW_PREFS_CATEGORY_SIZES_H__
#define __MLVIEW_PREFS_CATEGORY_SIZES_H__

#include "mlview-prefs-storage-manager.h"
#include "mlview-prefs-category.h"

namespace mlview
{

struct PrefsCategorySizesPriv;

class PrefsCategorySizes : public mlview::PrefsCategory
{
private:
    friend struct PrefsCategorySizesPriv;
    PrefsCategorySizesPriv *m_priv;

	//forbid assignation/copy
	PrefsCategorySizes (PrefsCategorySizes const&) ;
	PrefsCategorySizes& operator= (PrefsCategorySizes const&) ;

public:
    PrefsCategorySizes (PrefsStorageManager *manager);
    virtual ~PrefsCategorySizes ();

    int  get_main_window_width_default ();
    int  get_main_window_width ();
    void set_main_window_width (int width);

    int  get_main_window_height_default ();
    int  get_main_window_height ();
    void set_main_window_height (int height);

    int  get_tree_editor_size_default ();
    int  get_tree_editor_size ();
    void set_tree_editor_size (int size);

    int  get_completion_box_size_default ();
    int  get_completion_box_size ();
    void set_completion_box_size (int size);

    static const char* CATEGORY_ID;
};

} // namespace mlview

#endif // __MLVIEW_PREFS_CATEGORY_SIZES_H__
