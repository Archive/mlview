/* -*- Mode: C++; indent-tabs-mode:true ; c-basic-offset: 4-*- */

/*
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute 
 *it and/or modify it under the terms of 
 *the GNU General Public License as published by the 
 *Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */

#ifndef __MLVIEW_VIEW_ADAPTER_H__
#define __MLVIEW_VIEW_ADAPTER_H__

#include <gtkmm.h>
#include "mlview-app-context.h"
#include "mlview-exception.h"
#include "mlview-iview.h"
#include "mlview-xml-document.h"

/**
 *@file
 *The declaration of the #MlViewViewAdapter class.
 */
namespace mlview {
struct ViewAdapterPriv ;
class ViewAdapter : public IView {
friend struct ViewAdapterPriv ;

	ViewAdapterPriv *m_priv ;

	//forbid copy
	ViewAdapter (ViewAdapter const &a_view) ;
	ViewAdapter& operator= (ViewAdapter const &) ;

	public:
	ViewAdapter (MlViewXMLDocument *a_doc,
				 const UString &a_name,
				 const UString &a_desc_type_name) ;

	virtual ~ViewAdapter () ;

	static IView *create_instance (MlViewXMLDocument *a_doc,
								   const UString &a_name) ;

	MlViewFileDescriptor* get_file_descriptor () ;

	//*****************************
	// implement pure virtual
	// methods of IView
	//****************************

	void set_name_interactive () ;

	Gtk::Widget* get_implementation () ;

	virtual enum MlViewStatus connect_to_doc (MlViewXMLDocument *a_this) ;

	enum MlViewStatus disconnect_from_doc (MlViewXMLDocument *a_this) ;

	enum MlViewStatus update_contextual_menu () ;

	enum MlViewStatus execute_action (MlViewAction &an_action) ;

	bool get_must_rebuild_upon_document_reload () ;
	enum MlViewStatus undo ();

	enum MlViewStatus redo ();

	bool can_undo () ;

	bool can_redo () ;

	protected:

	GtkWidget* build_name_editing_dialog () ; 
	void set_name_editing_widget_value (GtkWidget *a_edition_widget,
			                    UString &a_value) ; 
	UString get_name_editing_widget_value (GtkWidget * a_edition_widget) ;

};//class ViewAdapter
}//namespace mlview

#endif /*__MLVIEW_VIEW_ADAPTER_H__*/
