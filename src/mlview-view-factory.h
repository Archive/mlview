/* -*- Mode: C++; indent-tabs-mode: true; c-basic-offset: 4-*- */

/**
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute it 
 *and/or modify it under the terms of 
 *the GNU General Public License as published 
 *by the Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *MlView is distributed in the hope that it will be useful, 
 *but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. If not, write to the Free Software Foundation, Inc., 
 *59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */
#ifndef __MLVIEW_VIEW_FACTORY_H__
#define __MLVIEW_VIEW_FACTORY_H__

#include "mlview-object.h"
#include "mlview-iview.h"

namespace mlview {

class ViewFactory : Object {
	//forbid instanciation, copy, assignation
	ViewFactory () ;
	ViewFactory (ViewFactory const &) ;
	ViewFactory& operator= (const ViewFactory &) ;
	~ViewFactory () ;

	public:

	static IView* create_view (MlViewXMLDocument *a_doc,
							   const UString &a_view_type_name,
							   const UString &a_view_name) ;

	static ViewDescriptor const* get_view_descriptors () ;

	static ViewDescriptor* get_default_view_descriptor () ;

	static ViewDescriptor*  peek_editing_view_descriptor
				(const UString &a_view_type_name) ;

	static guint get_number_of_view_desc (void)  ;

	static ViewDescriptor * get_view_descriptor_at (guint a_offset) ;

}; //class ViewFactory 
}//namespace mlview

#endif //__MLVIEW_VIEW_FACTORY_H__

