/* -*- Mode: C++; indent-tabs-mode:true ; c-basic-offset: 4-*- */

/*
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute 
 *it and/or modify it under the terms of 
 *the GNU General Public License as published by the 
 *Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */
#include "mlview-exception.h"
#include "mlview-prefs-category-general.h"


namespace mlview
{

struct PrefsCategoryGeneralPriv
{
	static const char* DEFAULT_EDITING_VIEW_KEY;
	static const char* USE_VALIDATION_KEY;
};

const char* PrefsCategoryGeneral::CATEGORY_ID = "general";

const char* PrefsCategoryGeneralPriv::DEFAULT_EDITING_VIEW_KEY =
    "/apps/mlview/default-editing-view-type";
const char* PrefsCategoryGeneralPriv::USE_VALIDATION_KEY =
    "/apps/mlview/validation-is-on";


PrefsCategoryGeneral::PrefsCategoryGeneral
(PrefsStorageManager *a_storage_manager)
		: PrefsCategory::PrefsCategory (PrefsCategoryGeneral::CATEGORY_ID,
		                                a_storage_manager)
{
	m_priv = new PrefsCategoryGeneralPriv ();
}

PrefsCategoryGeneral::~PrefsCategoryGeneral ()
{
	if (m_priv != NULL) {
		delete m_priv;
		m_priv = NULL ;
	}
}

const UString
PrefsCategoryGeneral::get_default_edition_view_default ()
{
	try {
		return get_storage_manager ().get_default_string_value
		       (PrefsCategoryGeneralPriv::DEFAULT_EDITING_VIEW_KEY);
	} catch (mlview::Exception& e) {
		TRACE_EXCEPTION (e);
	}
	return "source-view";
}

const UString
PrefsCategoryGeneral::get_default_edition_view ()
{
	try {

		return get_storage_manager ().get_string_value
		       (PrefsCategoryGeneralPriv::DEFAULT_EDITING_VIEW_KEY);

	} catch (mlview::Exception& e) {
		TRACE_EXCEPTION (e);
	}

	return get_default_edition_view_default ();
}

void
PrefsCategoryGeneral::set_default_edition_view
(const UString &a_name)
{
	get_storage_manager ().set_string_value
	(PrefsCategoryGeneralPriv::DEFAULT_EDITING_VIEW_KEY, a_name);
}

bool
PrefsCategoryGeneral::use_validation_default ()
{
	try {

		return get_storage_manager ().get_default_bool_value
		       (PrefsCategoryGeneralPriv::USE_VALIDATION_KEY);

	} catch (mlview::Exception& e) {
		TRACE_EXCEPTION (e);
	}

	return TRUE;
}

bool
PrefsCategoryGeneral::use_validation ()
{
	try {

		return get_storage_manager ().get_bool_value
		       (PrefsCategoryGeneralPriv::USE_VALIDATION_KEY);

	} catch (mlview::Exception& e) {
		TRACE_EXCEPTION (e);
	}

	return use_validation_default ();
}

void
PrefsCategoryGeneral::set_use_validation (bool a_flag)
{
	get_storage_manager ().set_bool_value
	(PrefsCategoryGeneralPriv::USE_VALIDATION_KEY, a_flag);
}
} // namespace mlview
