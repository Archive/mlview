/* -*- Mode: C++; indent-tabs-mode: true; c-basic-offset: 4-*- */

/*This file is part of GNU MlView
 *
 *GNU MlView is free software; 
 *you can redistribute it and/or modify it under the terms of 
 *the GNU General Public License as published by 
 *the Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope that it will be useful, 
 *but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General 
 *Public License along with MlView; 
 *see the file COPYING. If not, write to the 
 *Free Software Foundation, Inc., 59 
 *Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright informations.
 */

#ifndef __MLVIEW_PLUGIN_DESCRIPTOR_H__
#define __MLVIEW_PLUGIN_DESCRIPTOR_H__

#include <list>

#include <glib-object.h>

#include "mlview-ustring.h"

/*
 *@file
 *The declaration of the #PluginDescriptor structure.
 */

using namespace std ;

namespace mlview
{
struct PluginDescriptorPriv ;
class PluginDescriptor
{
	friend struct PluginDescriptorPriv ;
	PluginDescriptorPriv *m_priv ;

public:
	PluginDescriptor () {} ;
	PluginDescriptor (const UString &a_file_name) ;
	PluginDescriptor (const PluginDescriptor &) ;
	PluginDescriptor& operator= (const PluginDescriptor &a_plugin) ;

	const UString& get_plugin_file_path () const ;
	const UString& get_load_hook_function_name () const ;
	const UString& get_unload_hook_function_name () const ;
	const UString& get_plugin_name () const ;

	virtual ~PluginDescriptor () ;
};//end class plugin descriptor

typedef list<PluginDescriptor> ListOfPluginDescriptors ;
} // end namespace mlview

#endif /* __MLVIEW_PLUGIN_DESCRIPTOR_H__ */
