/* -*- Mode: C++; indent-tabs-mode:nil; c-basic-offset: 4-*- */
/*
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute 
 *it and/or modify it under the terms of 
 *the GNU General Public License as published by the 
 *Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */

#include <string.h>
#include "mlview-prefs.h"
#include "mlview-prefs-category-treeview.h"
#include "mlview-icon-tree.h"
#ifdef MLVIEW_WITH_CUSTOM_CELL_RENDERER
#include "mlview-cell-renderer.h"
#endif

/**
 *@file
 *The definition of a tree editor that embedds nice icons.
 */


#define ELEMENT_ICON      "mlview/xml-element-node.png"
#define OPEN_ELEMENT_ICON "mlview/xml-element-node-open.png"
#define TEXT_ICON         "mlview/xml-text-node.png"
#define ROOT_ICON         "mlview/xml-root.png"
#define OPEN_ROOT_ICON    "mlview/xml-root-open.png"
#define COMMENT_ICON      "mlview/xml-comment-node.png"
#define PI_ICON           "mlview/xml-pi-node.png"
#define ENTITY_REF_ICON   "mlview/xml-entity-ref-node.png"

enum MlViewIconTreeColumns  {
    /*hidden column, where the xml node is stored.*/
    XML_NODE_COLUMN = 0,
    /*
     *contains a boolean that says
     *if the column is editable or not.
     */
    IS_EDITABLE_COLUMN,
    ARE_ATTRIBUTES_EDITABLE_COLUMN,
    /*the first visible column, etc.*/
    ICON_COLUMN,
    OPEN_ICON_COLUMN,
    NODE_COLUMN,
    ATTRIBUTES_COLUMN,
    /*
     *This must be the last element
     *of the enum.
     */
    NB_COLUMNS
};

static gchar *node_to_string_tag (MlViewIconTree *a_this,
                                  xmlNode * a_node,
								  gboolean a_selected);

static enum MlViewStatus build_tree_model_from_xml_tree
		(MlViewTreeEditor * a_this,
         const xmlNode * a_node,
         GtkTreeIter * a_ref_iter,
         enum MlViewTreeInsertType a_type,
         GtkTreeModel ** a_model);

static void mlview_icon_tree_load_icons (MlViewIconTreeClass *a_klass);

static enum MlViewStatus
update_visual_node (MlViewTreeEditor *a_this,
                    GtkTreeIter *a_iter,
                    gboolean a_selected)
{
    gchar *start_tag_str = NULL, *attr_str = NULL;
    GtkTreeModel *model = NULL;
    xmlNode *xml_node = NULL;

    g_return_val_if_fail (a_this
                          && MLVIEW_IS_ICON_TREE (a_this)
                          && MLVIEW_IS_TREE_EDITOR (a_this)
                          && a_iter, MLVIEW_BAD_PARAM_ERROR);

    model = mlview_tree_editor_get_model
        (MLVIEW_TREE_EDITOR (a_this));
    g_return_val_if_fail (model, MLVIEW_ERROR);
    gtk_tree_model_get (model, a_iter, XML_NODE_COLUMN,
                        &xml_node, -1);

    start_tag_str = node_to_string_tag
        (MLVIEW_ICON_TREE (a_this), xml_node,
         a_selected) ;
    if (xml_node->type == XML_ELEMENT_NODE) {
        attr_str = mlview_tree_editor_build_attrs_list_str
            (MLVIEW_TREE_EDITOR (a_this), xml_node, a_selected);
    }
    if (!start_tag_str)
        return MLVIEW_OK ;

    if (attr_str)
        gtk_tree_store_set (GTK_TREE_STORE (model),
                            a_iter, NODE_COLUMN,
                            start_tag_str, ATTRIBUTES_COLUMN,
                            attr_str, -1);
    else
        gtk_tree_store_set (GTK_TREE_STORE (model),
                            a_iter, NODE_COLUMN,
                            start_tag_str, ATTRIBUTES_COLUMN,
                            "", -1);

    if (start_tag_str) {
        g_free (start_tag_str) ;
        start_tag_str = NULL ;
    }

    if (attr_str) {
        g_free (attr_str);
        attr_str = NULL;
    }

    return MLVIEW_OK;
}

static enum MlViewStatus
build_tree_model_from_xml_doc (MlViewTreeEditor * a_this,
                               const xmlDoc * a_doc,
                               GtkTreeModel ** a_model)
{
	GtkTreeIter iter = { 0 };
	GtkTreeStore *model = NULL, **tree_store_ptr = NULL;
	GtkTreeRowReference *row_ref = NULL;
	GtkTreePath *tree_path = NULL;
	xmlNode *xml_tree = NULL;
	enum MlViewStatus status = MLVIEW_OK;
	GHashTable *nodes_rows_hash = NULL ;

	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_TREE_EDITOR (a_this)
	                      && a_doc && a_model
	                      && *a_model == NULL,
	                      MLVIEW_BAD_PARAM_ERROR);

	nodes_rows_hash = mlview_tree_editor_get_nodes_rows_hash
	                  (MLVIEW_TREE_EDITOR (a_this));

	if (!nodes_rows_hash) {
		nodes_rows_hash =
		    g_hash_table_new (g_direct_hash,
		                      g_direct_equal);
		if (!nodes_rows_hash) {
			mlview_utils_trace_debug
			("The system may be out of memory");
			return MLVIEW_ERROR;
		}
		mlview_tree_editor_set_nodes_rows_hash
		(MLVIEW_TREE_EDITOR (a_this),
		 nodes_rows_hash) ;
	}
	model = gtk_tree_store_new (NB_COLUMNS,
	                            G_TYPE_POINTER,
	                            G_TYPE_BOOLEAN,
	                            G_TYPE_BOOLEAN,
	                            GDK_TYPE_PIXBUF,
	                            GDK_TYPE_PIXBUF,
	                            G_TYPE_STRING,
	                            G_TYPE_STRING);
	g_return_val_if_fail (model, MLVIEW_BAD_PARAM_ERROR);
	*a_model = GTK_TREE_MODEL (model);
	g_return_val_if_fail (model, MLVIEW_BAD_PARAM_ERROR);

	gtk_tree_store_append (model, &iter, NULL);
	tree_path =
	    gtk_tree_model_get_path (GTK_TREE_MODEL (model),
	                             &iter);
	g_return_val_if_fail (tree_path, MLVIEW_BAD_PARAM_ERROR);
	row_ref = gtk_tree_row_reference_new
	          (GTK_TREE_MODEL (model), tree_path);
	if (!row_ref) {
		mlview_utils_trace_debug ("!row_ref failed") ;
		goto cleanup ;
	}
	g_hash_table_insert (nodes_rows_hash,
	                     (gpointer) a_doc, row_ref);
	gtk_tree_store_set (model, &iter, XML_NODE_COLUMN, a_doc, -1);
	gtk_tree_store_set (model, &iter, NODE_COLUMN,
	                    "<span foreground=\"#bbbb00\">XML Document Root</span>", -1);
	gtk_tree_store_set (model, &iter, ICON_COLUMN,
	                    MLVIEW_ICON_TREE_CLASS
	                    (G_OBJECT_GET_CLASS (a_this))->icons.root,
	                    OPEN_ICON_COLUMN,
	                    MLVIEW_ICON_TREE_CLASS
	                    (G_OBJECT_GET_CLASS (a_this))->icons.open_root,
	                    ARE_ATTRIBUTES_EDITABLE_COLUMN, FALSE, -1);
	xml_tree = a_doc->children;
	tree_store_ptr = &model ;
	status = build_tree_model_from_xml_tree
	         (a_this, xml_tree,
	          &iter, INSERT_TYPE_APPEND_CHILD_VISIT_NEXT,
	          (GtkTreeModel **) tree_store_ptr);
cleanup:
	if (tree_path) {
		gtk_tree_path_free (tree_path) ;
		tree_path = NULL ;
	}
	return status;
}

static enum MlViewStatus
build_tree_model_from_xml_tree (MlViewTreeEditor * a_this,
                                const xmlNode * a_node,
                                GtkTreeIter * a_ref_iter,
                                enum MlViewTreeInsertType a_type,
                                GtkTreeModel ** a_model)
{
	GtkTreeStore *model = NULL;
	GtkTreeIter iter = {0} ;
	GtkTreeIter parent_iter = {0} ;
	GtkTreePath *tree_path = NULL;
	xmlNode *cur_node = NULL,
	                    *parent_node = NULL;
	gchar *start_tag = NULL;
	enum MlViewStatus status = MLVIEW_OK;
	GHashTable *nodes_rows_hash = NULL ;

	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_ICON_TREE (a_this)
	                      && MLVIEW_IS_TREE_EDITOR (a_this)
	                      && a_node && a_model && *a_model,
	                      MLVIEW_BAD_PARAM_ERROR);

	model = GTK_TREE_STORE (*a_model);
	g_return_val_if_fail (model, MLVIEW_BAD_PARAM_ERROR);
	nodes_rows_hash = mlview_tree_editor_get_nodes_rows_hash
	                  (MLVIEW_TREE_EDITOR (a_this)) ;
	if (!nodes_rows_hash) {
		nodes_rows_hash =
		    g_hash_table_new (g_direct_hash,
		                      g_direct_equal);
		if (!nodes_rows_hash) {
			mlview_utils_trace_debug
			("The system may be out of memory");
			return MLVIEW_ERROR;
		}
		mlview_tree_editor_set_nodes_rows_hash
		(MLVIEW_TREE_EDITOR (a_this),
		 nodes_rows_hash) ;
	}

	for (cur_node = (xmlNode *) a_node;
	        cur_node; cur_node = cur_node->next) {
		GtkTreeRowReference *row_ref = NULL;

		start_tag = node_to_string_tag
		            (MLVIEW_ICON_TREE (a_this), cur_node, FALSE);
		switch (a_type) {
		case INSERT_TYPE_APPEND_CHILD_VISIT_NEXT:
			gtk_tree_store_append (model, &iter,
			                       a_ref_iter);
			break;
		case INSERT_TYPE_PREPEND_CHILD:
			gtk_tree_store_prepend (model, &iter,
			                        a_ref_iter);
			break ;
		case INSERT_TYPE_INSERT_BEFORE:
		case INSERT_TYPE_INSERT_AFTER:
			parent_node = cur_node->parent;
			if (!parent_node) {
				mlview_utils_trace_debug
				("parent_node failed") ;
				status = MLVIEW_ERROR ;
				goto cleanup ;

			}
			status = mlview_tree_editor_get_iter
			         (a_this, parent_node,
			          &parent_iter);
			if (status != MLVIEW_OK) {
				mlview_utils_trace_debug
				("status == MLVIEW_OK failed") ;
				status = MLVIEW_ERROR ;
				goto cleanup ;
			}
			model = GTK_TREE_STORE
			        (mlview_tree_editor_get_model
			         (a_this));
			if (!model) {
				mlview_utils_trace_debug
				("model failed") ;
				status = MLVIEW_ERROR ;
				goto cleanup ;
			}
			if (a_type == INSERT_TYPE_INSERT_BEFORE)
				gtk_tree_store_insert_before
				(model, &iter,
				 &parent_iter,
				 a_ref_iter);
			else
				gtk_tree_store_insert_after
				(model, &iter,
				 &parent_iter,
				 a_ref_iter);
			break;
		default:
			break;
		}
		tree_path = gtk_tree_model_get_path
		            (GTK_TREE_MODEL (model), &iter);
		if (!tree_path) {
			mlview_utils_trace_debug ("tree_path failed") ;
			status = MLVIEW_ERROR ;
			goto cleanup ;
		}
		row_ref = gtk_tree_row_reference_new
		          (GTK_TREE_MODEL (model), tree_path);
		if (!row_ref) {
			mlview_utils_trace_debug ("row_ref failed") ;
			status = MLVIEW_ERROR ;
			goto cleanup ;
		}
		g_hash_table_insert
		(nodes_rows_hash,
		 cur_node, row_ref);
		gtk_tree_store_set (model, &iter,
		                    XML_NODE_COLUMN, cur_node, -1);

		if (start_tag) {
			gtk_tree_store_set (model, &iter,
			                    NODE_COLUMN,
			                    start_tag, -1);
		}

                if (cur_node->type == XML_ELEMENT_NODE) {
                    gtk_tree_store_set (model, &iter,
                                        IS_EDITABLE_COLUMN,
                                        TRUE, ICON_COLUMN,
                                        MLVIEW_ICON_TREE_CLASS
                                        (G_OBJECT_GET_CLASS (a_this))->icons.element,
                                        OPEN_ICON_COLUMN,
                                        MLVIEW_ICON_TREE_CLASS
                                        (G_OBJECT_GET_CLASS (a_this))->icons.open_element,
                                        ATTRIBUTES_COLUMN,
                                        mlview_tree_editor_build_attrs_list_str
                                        (a_this, cur_node, FALSE),
                                        ARE_ATTRIBUTES_EDITABLE_COLUMN,
                                        TRUE,
                                        -1);

			if (cur_node->children) {
				build_tree_model_from_xml_tree
				(a_this,
				 cur_node->children,
				 &iter,
				 INSERT_TYPE_APPEND_CHILD_VISIT_NEXT,
				 a_model);
			}
		} else if (cur_node->type == XML_TEXT_NODE) {
			gtk_tree_store_set (model, &iter,
			                    IS_EDITABLE_COLUMN,
			                    TRUE, OPEN_ICON_COLUMN,
			                    MLVIEW_ICON_TREE_CLASS
			                    (G_OBJECT_GET_CLASS (a_this))->icons.text,
			                    ARE_ATTRIBUTES_EDITABLE_COLUMN,
			                    FALSE,
			                    -1);
		} else if (cur_node->type == XML_COMMENT_NODE) {
			gtk_tree_store_set (model, &iter,
			                    IS_EDITABLE_COLUMN,
			                    TRUE,
			                    OPEN_ICON_COLUMN,
			                    MLVIEW_ICON_TREE_CLASS
			                    (G_OBJECT_GET_CLASS (a_this))->icons.comment,
			                    ARE_ATTRIBUTES_EDITABLE_COLUMN,
			                    FALSE,
			                    -1);
		} else if (cur_node->type == XML_PI_NODE) {
			gtk_tree_store_set (model, &iter,
			                    IS_EDITABLE_COLUMN,
			                    FALSE,
			                    OPEN_ICON_COLUMN,
			                    MLVIEW_ICON_TREE_CLASS
			                    (G_OBJECT_GET_CLASS
			                     (a_this))->icons.pi,
			                    ARE_ATTRIBUTES_EDITABLE_COLUMN,
			                    FALSE,
			                    -1);
		} else if (cur_node->type == XML_DTD_NODE) {
			gtk_tree_store_set (model, &iter,
			                    IS_EDITABLE_COLUMN,
			                    TRUE,
			                    ARE_ATTRIBUTES_EDITABLE_COLUMN,
			                    FALSE,
			                    -1);
			if (cur_node->children) {
				build_tree_model_from_xml_tree
				(a_this,
				 cur_node->children,
				 &iter,
				 INSERT_TYPE_APPEND_CHILD_VISIT_NEXT,
				 a_model);
			}
		} else if (cur_node->type == XML_ENTITY_DECL) {
			gtk_tree_store_set
			(model, &iter,
			 IS_EDITABLE_COLUMN, TRUE,
			 ARE_ATTRIBUTES_EDITABLE_COLUMN,
			 TRUE,
			 -1);
		} else if (cur_node->type == XML_ENTITY_REF_NODE) {
			gtk_tree_store_set (model, &iter,
			                    IS_EDITABLE_COLUMN,
			                    FALSE,
			                    OPEN_ICON_COLUMN,
			                    MLVIEW_ICON_TREE_CLASS
			                    (G_OBJECT_GET_CLASS
			                     (a_this))->icons.entity_ref,
			                    ARE_ATTRIBUTES_EDITABLE_COLUMN,
			                    FALSE,
			                    -1);
		} else if (cur_node->type == XML_CDATA_SECTION_NODE) {
			gtk_tree_store_set
			(model, &iter,
			 IS_EDITABLE_COLUMN, TRUE,
			 ARE_ATTRIBUTES_EDITABLE_COLUMN,
			 TRUE,
			 -1);
		} else {
			mlview_utils_trace_debug
			("unknown type of node");
		}
		if (start_tag) {
			g_free (start_tag) ;
			start_tag = NULL ;
		}
		if (tree_path) {
			gtk_tree_path_free (tree_path) ;
			tree_path = NULL ;
		}
		if (a_type == INSERT_TYPE_INSERT_BEFORE
		        || a_type == INSERT_TYPE_INSERT_AFTER
		        || a_type == INSERT_TYPE_PREPEND_CHILD) {
			/*
			 *we are building a tree model which root
			 *node is cur_node so we should not
			 *visit cur_node->next.
			 */
			break ;
		}
	}
	if (*a_model) {
		g_object_set_data (G_OBJECT (*a_model),
		                   "MlViewTreeEditor",
		                   a_this) ;
	}
cleanup:
	if (tree_path) {
		gtk_tree_path_free (tree_path) ;
		tree_path = NULL ;
	}
	if (start_tag) {
		g_free (start_tag) ;
		start_tag = NULL ;
	}
	return status ;
}

/**
 *Builds a start tag string 
 *(e.g: <node-name attr0="val0" attr1="val1">)out of
 *an instance of xmlNode *
 *@param a_node the instance of xmlNode * to consider.
 *@return the newly built start tag string.
 */
static gchar *
node_to_string_tag (MlViewIconTree *a_this,
                    xmlNode * a_node,
                    gboolean selected)
{
	gchar *result = NULL,
	*content = NULL,
	*escaped_content = NULL;
	const gchar *colour_str = NULL ;

	g_return_val_if_fail (a_node != NULL, NULL) ;
	g_return_val_if_fail (a_this
			&& MLVIEW_IS_ICON_TREE (a_this)
			&& MLVIEW_IS_TREE_EDITOR (a_this),
			NULL) ;

	if (selected) {
		GtkStyle *style =
		mlview_tree_editor_get_style (MLVIEW_TREE_EDITOR
				(a_this));
		GdkColor color = style->fg[GTK_STATE_SELECTED];
		char *colstr = (char*)g_try_malloc (8 * sizeof (char));
		sprintf (colstr,
				"#%02x%02x%02x",
				(color.red / G_MAXUSHORT) * 255,
				(color.green / G_MAXUSHORT) * 255,
				(color.blue / G_MAXUSHORT) * 255);
		colour_str = g_strdup (colstr);
	} else {
		colour_str = mlview_tree_editor_get_colour_string
		(MLVIEW_TREE_EDITOR (a_this), a_node->type);
	}
	if (a_node->type == XML_ELEMENT_NODE) {
		gchar *ns_prefix = NULL,
			  *name = NULL;
		if (a_node->ns != NULL && a_node->ns->prefix) {
			ns_prefix = g_strdup_printf ("%s:", a_node->ns->prefix);
		} else {
			ns_prefix = NULL;
		}

		if (ns_prefix) {
			name = g_strconcat (ns_prefix, a_node->name, NULL);
		} else
			name = g_strdup ((gchar*)a_node->name);

		if (ns_prefix) {
			g_free (ns_prefix);
			ns_prefix = NULL;
		}

		result = g_strdup_printf ("<span foreground=\"%s\">%s</span>",
				colour_str, name);
		if (name) {
			g_free (name);
			name = NULL;
		}

	} else if (xmlNodeIsText (a_node)) {
		gchar *str = NULL;
		guint esc_content_len = 0 ;
		enum MlViewStatus status = MLVIEW_OK ;
		content = (gchar*)xmlNodeGetContent (a_node);
		if (content == NULL) {
			xmlNodeSetContent (a_node, (xmlChar*)"text") ;
			content = (gchar*)xmlNodeGetContent (a_node);
		}

		status = mlview_utils_escape_predef_entities_in_str
		(content, &escaped_content,
		 &esc_content_len) ;

		if (status != MLVIEW_OK) {
			escaped_content = NULL;
		}
		if (!escaped_content) {
			str = content ;
		} else {
			str = escaped_content ;
		}

		result = g_strdup_printf
		("<span foreground=\"%s\">%s</span>",
		 colour_str, str);

		xmlFree (content);
		if (escaped_content) {
			g_free (escaped_content) ;
			escaped_content = NULL ;
		}
	} else if (a_node->type == XML_COMMENT_NODE) {
		content = (gchar*)xmlNodeGetContent (a_node);
		if (content == NULL) {
			xmlNodeSetContent (a_node, (xmlChar*)"<!--comment-->");
			content = (gchar*) xmlNodeGetContent (a_node);
		}

		escaped_content = g_markup_escape_text (content,
				strlen (content));
		if (!escaped_content) {
			mlview_utils_trace_debug
			("g_markup_escape_text") ;
			xmlFree (content) ;
			return NULL ;
		}
		result = g_strdup_printf
		("<span foreground=\"%s\">%s</span>",
		 colour_str, escaped_content);
		xmlFree (content);
		content = NULL ;
		g_free (escaped_content) ;

	} else if (a_node->type == XML_PI_NODE) {
		content = (gchar*) xmlNodeGetContent (a_node);
		if (content == NULL) {
			xmlNodeSetContent
			(a_node, (xmlChar*)"processing instruction node");
			content = (gchar*) xmlNodeGetContent (a_node);
			if (!content) {
				mlview_utils_trace_debug
				("xmlNodeGetContent() failed") ;
				return NULL ;
			}
		}
		escaped_content = g_markup_escape_text
		(content, strlen (content)) ;
		if (!escaped_content) {
			mlview_utils_trace_debug
			("g_markup_escape_text() failed") ;
			xmlFree (content) ;
			return NULL ;
		}
		result = g_strdup_printf
		("<span foreground=\"%s\">%s %s</span>",
		 colour_str, a_node->name, escaped_content);
		if (content) {
			xmlFree (content);
			content = NULL ;
		}
		if (escaped_content) {
			g_free (escaped_content) ;
			escaped_content = NULL ;
		}
	} else if (a_node->type == XML_DTD_NODE) {
		mlview_tree_editor_dtd_node_to_string
		(MLVIEW_TREE_EDITOR (a_this),
		 (xmlDtd*)a_node, selected, &result) ;
	} else if (a_node->type == XML_ENTITY_DECL) {
		xmlEntity *entity = (xmlEntity*) a_node ;
		switch (entity->etype) {
			case XML_INTERNAL_GENERAL_ENTITY:
				mlview_tree_editor_internal_general_entity_to_string
				(MLVIEW_TREE_EDITOR (a_this),
				 entity, selected, &result) ;
				break ;
			case XML_EXTERNAL_GENERAL_PARSED_ENTITY:
				mlview_tree_editor_external_general_parsed_entity_to_string
				(MLVIEW_TREE_EDITOR (a_this),
				 entity, selected, &result) ;
				break ;
			case XML_EXTERNAL_GENERAL_UNPARSED_ENTITY:
				mlview_tree_editor_external_general_unparsed_entity_to_string
				(MLVIEW_TREE_EDITOR (a_this),
				 entity, selected, &result) ;
				break ;
			case XML_INTERNAL_PARAMETER_ENTITY:
				mlview_tree_editor_internal_parameter_entity_to_string
				(MLVIEW_TREE_EDITOR (a_this),
				 entity, selected, &result) ;
				break ;
			case XML_EXTERNAL_PARAMETER_ENTITY:
				mlview_tree_editor_external_parameter_entity_to_string
				(MLVIEW_TREE_EDITOR (a_this),
				 entity, selected, &result) ;
				break ;

			case XML_INTERNAL_PREDEFINED_ENTITY:
				mlview_utils_trace_debug
				("Oops, dunno how to render "
				 "XML_INTERNAL_PREDEFINED_ENTITY "
				 "type of xml entity decl node") ;
				break ;
			default:
				mlview_utils_trace_debug
				("Unknown entity type") ;
		}
        } else if (a_node->type == XML_ENTITY_REF_NODE) {
            mlview_tree_editor_entity_ref_to_string
                    (MLVIEW_TREE_EDITOR (a_this),
                 a_node, selected, &result) ;
        } else if (a_node->type == XML_CDATA_SECTION_NODE) {
            mlview_tree_editor_cdata_section_to_string
                    (MLVIEW_TREE_EDITOR (a_this),
                 a_node, &result) ;
        } else if (a_node->type == XML_DOCUMENT_NODE) {
            mlview_tree_editor_document_node_to_string
                (MLVIEW_TREE_EDITOR (a_this),
                 a_node, selected, &result) ;

        }
	else {
		mlview_utils_trace_debug ("Unknown type of node") ;
	}
	return result;
}

static gchar *
build_xml_attrs_list_str (xmlAttrPtr attr_iter)
{
	xmlChar *attr_val = NULL ;
	gchar *escaped_attr_val = NULL, *str = NULL;
	enum MlViewStatus status = MLVIEW_OK ;
	guint length = 0 ;
	gchar *result = NULL, *attribute=NULL, *tmp=NULL;

	g_return_val_if_fail (attr_iter && attr_iter->name, NULL) ;

	while (attr_iter) {
		attr_val = xmlGetProp (attr_iter->parent,
		                       attr_iter->name) ;

		if (attr_val) {
			status =
			    mlview_utils_escape_predef_entities_in_str
			    ((gchar*)attr_val, &escaped_attr_val, &length) ;
			if (status != MLVIEW_OK
			        || !escaped_attr_val) {
				str = (gchar*)attr_val ;
			} else {
				str = escaped_attr_val ;
			}
			attribute = g_strdup_printf
			            ("%s=\"%s\"", attr_iter->name,
			             str) ;
			if (attr_val) {
				xmlFree (attr_val) ;
				attr_val = NULL ;
			}
			if (escaped_attr_val) {
				g_free (escaped_attr_val) ;
				escaped_attr_val = NULL ;
			}
		}
		if (result) {
			tmp = g_strdup_printf ("%s %s", result, attribute);
			g_free (result);
			result = tmp;
		} else
			result = attribute;

		attr_iter = attr_iter->next;
	}

	return result;
}

static gchar *
node_to_xml_tag_w_attr (const xmlChar *node_name,
                        xmlElementType type,
                        xmlNode *children, xmlNs *ns,
                        xmlChar *content, gchar *attributes)
{
	gchar *result = NULL;

	if (type == XML_ELEMENT_NODE) {
		gchar *ns_prefix = NULL,
		                   *attr_str = NULL,
		                               *name = NULL;

		if (attributes)
			attr_str = strlen (attributes) > 0 ? attributes : NULL;

		if (ns != NULL && ns->prefix) {
			ns_prefix = g_strconcat ((gchar*)ns->prefix,
			                         ":", NULL);
		} else {
			ns_prefix = NULL;
		}

		if (ns_prefix) {
			name = g_strconcat (ns_prefix,
			                    node_name, NULL);
		} else {
			name = g_strdup ((gchar*)node_name);
		}
		if (ns_prefix) {
			g_free (ns_prefix);
			ns_prefix = NULL;
		}

		if (children != NULL) {
			if (attr_str)
				result = g_strconcat
				         ("<", name, " ", attr_str, ">", NULL);
			else
				result = g_strconcat
				         ("<", name, ">", NULL);
		} else {        /*empty tag */
			if (attr_str)
				result = g_strconcat
				         ("<", name, " ", attr_str, " />", NULL);
			else
				result = g_strconcat
				         ("<", name, " />", NULL);

			if (name) {
				g_free (name);
				name = NULL;
			}
		}
	} else if (type == XML_TEXT_NODE) {
		result = g_strdup ((gchar*)content);

	} else if (type == XML_COMMENT_NODE) {
		result = g_strconcat
		         ("<!--", content, "-->", NULL);
	} else if (type == XML_PI_NODE) {
		result = g_strconcat
		         ("<?", node_name, " ", content, ">", NULL);
	}

	return result;
}

static gchar *
node_to_xml_tag (const xmlChar *node_name,
                 xmlElementType type, xmlNode *children, xmlNs *ns,
                 xmlChar *content, xmlAttrPtr attributes)
{
	gchar *attr=NULL, *result=NULL;

	if (attributes && attributes->name)
		attr = build_xml_attrs_list_str (attributes);
	result = node_to_xml_tag_w_attr (node_name, type, children, ns, content, attr);

	if (attr)
		g_free (attr);

	return result;
}

/*
   NOT USED ATM
 *
static gchar *
build_attrs_list_str_from_nv_pairs (MlViewIconTree *a_this,
                                    GList *a_list)
{
        gchar *result = NULL, *tmp = NULL;
        struct NameValuePair *pair = NULL;
        const gchar *attval_col = "#00FF00",
                *attname_col = NULL ;
        
        g_return_val_if_fail (a_this 
                              && MLVIEW_IS_ICON_TREE (a_this)
                              && MLVIEW_IS_TREE_EDITOR (a_this),
                              NULL) ;
 
        attname_col = mlview_tree_editor_get_colour_string
                (MLVIEW_TREE_EDITOR (a_this), XML_ATTRIBUTE_NODE) ;
        
        while (a_list)
        {
                pair = a_list->data;
 
                if (result)
                {
                        tmp = g_strdup_printf 
                                ("%s <span foreground=\"%s\">%s</span>=<span foreground=\"%s\">\"%s\"</span>", 
                                 result, attname_col, pair->name->str, attval_col, pair->value->str);
                        g_free (result);
                        result = tmp;
                }
                else
                        result = g_strdup_printf 
                                ("<span foreground=\"%s\">%s</span>=<span foreground=\"%s\">\"%s\"</span>", 
                                 attname_col, pair->name->str, attval_col, pair->value->str);
      
                a_list = g_list_next (a_list);
        }
 
        return result;
}
*/

static void
node_attributes_edited_cb (GtkCellRendererText *a_renderer,
                           gchar *a_cell_path,
                           gchar *a_new_text,
                           gpointer a_data)
{
	MlViewTreeEditor *tree_editor = NULL ;
	GtkTreePath *tree_path = NULL ;
	GtkTreeIter iter = {0} ;
	GtkTreeModel *model = NULL ;
	MlViewXMLDocument *mlview_xml_doc = NULL ;
	enum MlViewStatus status = MLVIEW_OK ;
	GString *element_name = NULL ;
	GList *nv_pair_list = NULL ;
	xmlNode *cur_node = NULL ;
	gchar *start_tag = NULL ;
	gchar *node_path = NULL ;

	g_return_if_fail (a_renderer && a_data && a_cell_path) ;
	g_return_if_fail (MLVIEW_IS_TREE_EDITOR (a_data)
	                  && GTK_IS_CELL_RENDERER (a_renderer)) ;
	tree_editor = (MlViewTreeEditor*)a_data ;
	model = mlview_tree_editor_get_model (tree_editor) ;
	g_return_if_fail (model) ;
	tree_path = gtk_tree_path_new_from_string (a_cell_path) ;
	g_return_if_fail (tree_path) ;
	status = mlview_tree_editor_get_cur_sel_start_iter
	         (tree_editor, &iter) ;
	g_return_if_fail (status == MLVIEW_OK) ;
	cur_node = mlview_tree_editor_get_cur_sel_xml_node (tree_editor) ;
	if (!cur_node) {
		mlview_utils_trace_debug ("cur_node failed") ;
		goto cleanup ;
	}
	mlview_xml_doc = mlview_tree_editor_get_mlview_xml_doc (tree_editor);
	if (!mlview_xml_doc) {
		mlview_utils_trace_debug
		("mlview_xml_doc failed") ;
		goto cleanup ;
	}
	mlview_xml_document_get_node_path (mlview_xml_doc,
	                                   cur_node,
	                                   &node_path) ;
	if (!node_path) {
		mlview_utils_trace_debug ("Could not XPATH expr from node") ;
		goto cleanup ;
	}

	start_tag = node_to_xml_tag_w_attr (cur_node->name, cur_node->type, cur_node->children,
	                                    cur_node->ns, cur_node->content, a_new_text);

	status = mlview_utils_parse_start_tag
	         (start_tag, &element_name, &nv_pair_list);

	if (status == MLVIEW_OK)
		if (nv_pair_list)
			mlview_xml_document_synch_attributes (mlview_xml_doc,
			                                      node_path,
			                                      nv_pair_list);

cleanup:
	if (start_tag) {
		g_free (start_tag) ;
		start_tag = NULL ;
	}
	if (element_name) {
		g_string_free (element_name, TRUE) ;
		element_name = NULL ;
	}
	if (nv_pair_list) {
		GList *cur_item = NULL;
		for (cur_item = nv_pair_list; cur_item ;
		        cur_item = cur_item->next) {
			if (cur_item->data) {
				mlview_utils_name_value_pair_free
				((struct NameValuePair*)cur_item->data,
				 TRUE) ;
			}
		}
		g_list_free (nv_pair_list) ;
		nv_pair_list = NULL ;
	}
	if (tree_path) {
		gtk_tree_path_free (tree_path) ;
		tree_path = NULL ;
	}
	if (node_path) {
		g_free (node_path) ;
		node_path = NULL ;
	}
}

static void
node_cell_edited_cb (GtkCellRendererText *a_renderer,
                     gchar *a_cell_path,
                     gchar *a_new_text,
                     gpointer a_data)
{
	MlViewTreeEditor *tree_editor = NULL ;
	GtkTreePath *tree_path = NULL ;
	GtkTreeIter iter = {0} ;
	GtkTreeModel *model = NULL ;
	MlViewXMLDocument *mlview_xml_doc = NULL ;
	enum MlViewStatus status = MLVIEW_OK ;
	GString *element_name = NULL ;
	GList *nv_pair_list = NULL ;
	xmlNode *cur_node = NULL ;
	gchar *start_tag = NULL ;
	gchar *node_path = NULL ;

	g_return_if_fail (a_renderer && a_data && a_cell_path) ;
	g_return_if_fail (MLVIEW_IS_TREE_EDITOR (a_data)
	                  && GTK_IS_CELL_RENDERER (a_renderer)) ;
	tree_editor = (MlViewTreeEditor*) a_data ;
	model = mlview_tree_editor_get_model (tree_editor) ;
	g_return_if_fail (model) ;
	tree_path = gtk_tree_path_new_from_string (a_cell_path) ;
	g_return_if_fail (tree_path) ;
	status = mlview_tree_editor_get_cur_sel_start_iter
	         (tree_editor, &iter) ;
	g_return_if_fail (status == MLVIEW_OK) ;
	cur_node = mlview_tree_editor_get_cur_sel_xml_node
	           (tree_editor) ;
	if (!cur_node) {
		mlview_utils_trace_debug ("cur_node failed") ;
		goto cleanup ;
	}
	mlview_xml_doc =
	    mlview_tree_editor_get_mlview_xml_doc (tree_editor);
	if (!mlview_xml_doc) {
		mlview_utils_trace_debug
		("mlview_xml_doc failed") ;
		goto cleanup ;
	}

	if (cur_node->type == XML_ELEMENT_NODE) {

		start_tag = node_to_xml_tag
		            ((xmlChar*)a_new_text,
		             cur_node->type,
		             cur_node->children, cur_node->ns,
		             cur_node->content, cur_node->properties);

		status = mlview_utils_parse_start_tag
		         (start_tag, &element_name, &nv_pair_list) ;

		if (status == MLVIEW_OK) {
			/*TODO: finish this!*/
			mlview_xml_document_get_node_path (mlview_xml_doc,
			                                   cur_node,
			                                   &node_path) ;
			status = mlview_xml_document_set_node_name
			         (mlview_xml_doc,
			          node_path, element_name->str, TRUE);
			if (node_path) {
				g_free (node_path) ;
				node_path = NULL ;
			}
		}
	} else if (cur_node->type == XML_TEXT_NODE) {
		mlview_xml_document_get_node_path (mlview_xml_doc,
		                                   cur_node,
		                                   &node_path) ;
		mlview_xml_document_set_node_content
		(mlview_xml_doc, node_path,
		 a_new_text, TRUE) ;
		if (node_path) {
			g_free (node_path) ;
			node_path = NULL ;
		}
	} else if (cur_node->type == XML_ENTITY_DECL) {
		mlview_tree_editor_edit_xml_entity_decl_node
		(MLVIEW_TREE_EDITOR (tree_editor), (xmlEntity*)cur_node, a_new_text) ;
	} else if (cur_node->type == XML_DTD_NODE) {
		mlview_tree_editor_edit_dtd_node
		(MLVIEW_TREE_EDITOR (tree_editor),
		 (xmlDtd*)cur_node, a_new_text) ;
	} else if (cur_node->type == XML_CDATA_SECTION_NODE) {
		mlview_tree_editor_edit_cdata_section_node
		(MLVIEW_TREE_EDITOR (tree_editor),
		 cur_node, a_new_text) ;
	}

cleanup:
	if (start_tag) {
		g_free (start_tag) ;
		start_tag = NULL ;
	}
	if (element_name) {
		g_string_free (element_name, TRUE) ;
		element_name = NULL ;
	}
	if (nv_pair_list) {
		GList *cur_item = NULL;
		for (cur_item = nv_pair_list; cur_item ;
		        cur_item = cur_item->next) {
			if (cur_item->data) {
				mlview_utils_name_value_pair_free
				((struct NameValuePair*)cur_item->data,
				 TRUE) ;
			}
		}
		g_list_free (nv_pair_list) ;
		nv_pair_list = NULL ;
	}
	if (tree_path) {
		gtk_tree_path_free (tree_path) ;
		tree_path = NULL ;
	}
}

static GtkTreeView *
build_tree_view_from_xml_doc (MlViewTreeEditor * a_this,
                              xmlDoc * a_doc)
{
	GtkTreeView *tree_view = NULL ;
	GtkTreeModel *model = NULL ;
	GtkCellRenderer *renderer = NULL ;
	GtkTreeIter iter = {0} ;
	GtkTreeViewColumn *column = NULL ;
	enum MlViewStatus status = MLVIEW_OK ;
	gboolean is_ok = TRUE ;

	g_return_val_if_fail (a_this
			&& MLVIEW_IS_TREE_EDITOR (a_this),
			NULL);

	mlview::AppContext *context = mlview::AppContext::get_instance () ;
	THROW_IF_FAIL (context) ;

	status = build_tree_model_from_xml_doc (a_this, a_doc, &model);
	THROW_IF_FAIL (model);

	is_ok = gtk_tree_model_get_iter_first (model, &iter) ;

	g_return_val_if_fail (is_ok == TRUE, NULL) ;

	tree_view = GTK_TREE_VIEW
	(gtk_tree_view_new_with_model (model));

	THROW_IF_FAIL (tree_view);

	column = gtk_tree_view_column_new ();
	gtk_tree_view_column_set_title (column, _("Element name"));
	renderer = gtk_cell_renderer_pixbuf_new ();
	gtk_tree_view_column_pack_start (column, renderer, FALSE);
	gtk_tree_view_column_set_attributes (column, renderer,
			"pixbuf", OPEN_ICON_COLUMN,
			"pixbuf-expander-open", OPEN_ICON_COLUMN,
			"pixbuf-expander-closed", ICON_COLUMN,
			NULL);
#ifndef MLVIEW_WITH_CUSTOM_CELL_RENDERER

	renderer = gtk_cell_renderer_text_new ();
#else

	renderer = mlview_cell_renderer_new () ;
#endif

	gtk_tree_view_column_pack_start (column, renderer, FALSE);
	gtk_tree_view_column_set_attributes (column, renderer,
			"markup", NODE_COLUMN,
			"editable", IS_EDITABLE_COLUMN,
			NULL);
	gtk_tree_view_append_column (tree_view, column);
	g_signal_connect (G_OBJECT (renderer),
			"edited", G_CALLBACK (node_cell_edited_cb),
			a_this);
#ifndef MLVIEW_WITH_CUSTOM_CELL_RENDERER

	renderer = gtk_cell_renderer_text_new ();
#else

	renderer = mlview_cell_renderer_new () ;
#endif

	gtk_tree_view_insert_column_with_attributes
	(tree_view, 1, _("Attributes"),
	 renderer, "markup", ATTRIBUTES_COLUMN, "editable",
	 ARE_ATTRIBUTES_EDITABLE_COLUMN, NULL);

	if (column) {
		gtk_tree_view_column_set_resizable
		(column, TRUE) ;
	}
	g_signal_connect (G_OBJECT (renderer),
			"edited", G_CALLBACK (node_attributes_edited_cb),
			a_this);

	mlview::PrefsCategoryTreeview *prefs =
	    dynamic_cast<mlview::PrefsCategoryTreeview*> (
		mlview::Preferences::get_instance ()
		->get_category_by_id (mlview::PrefsCategoryTreeview::CATEGORY_ID));
	THROW_IF_FAIL (prefs);

	mlview_utils_gtk_tree_view_expand_row_to_depth2
	(tree_view, &iter,
	 prefs->get_default_tree_expansion_depth ());
	mlview_tree_editor_set_style (MLVIEW_TREE_EDITOR (a_this),
				      gtk_widget_get_style
				      (GTK_WIDGET (tree_view)));

	// Set font from preferences
	PangoFontDescription *font_desc =
	    pango_font_description_from_string (
	    prefs->get_font_name ().c_str ());
	if (font_desc != NULL)
		gtk_widget_modify_font (GTK_WIDGET (tree_view),
				font_desc);
	pango_font_description_free (font_desc);

	return tree_view;
}

static void
mlview_icon_tree_load_icons (MlViewIconTreeClass *a_klass)
{
	gchar *path = NULL ;

	path = gnome_program_locate_file (NULL, GNOME_FILE_DOMAIN_APP_DATADIR,
	                                  ELEMENT_ICON, TRUE, NULL);
	if (!path) {
		mlview_utils_trace_debug ("lookup of icon file failed:") ;
		mlview_utils_trace_debug (ELEMENT_ICON) ;
	} else {
		a_klass->icons.element = gdk_pixbuf_new_from_file (path, NULL);
		g_free (path);
	}

	path = gnome_program_locate_file (NULL, GNOME_FILE_DOMAIN_APP_DATADIR,
	                                  OPEN_ELEMENT_ICON, TRUE, NULL);
	if (!path) {
		mlview_utils_trace_debug ("lookup of icon file failed:") ;
		mlview_utils_trace_debug (OPEN_ELEMENT_ICON) ;
	} else {
		a_klass->icons.open_element = gdk_pixbuf_new_from_file (path, NULL);
		g_free (path);
	}

	path = gnome_program_locate_file (NULL, GNOME_FILE_DOMAIN_APP_DATADIR,
	                                  TEXT_ICON, TRUE, NULL);
	if (!path) {
		mlview_utils_trace_debug ("lookup of icon file failed:") ;
		mlview_utils_trace_debug (TEXT_ICON) ;
	} else {
		a_klass->icons.text = gdk_pixbuf_new_from_file (path, NULL);
		g_free (path);
	}

	path = gnome_program_locate_file (NULL, GNOME_FILE_DOMAIN_APP_DATADIR,
	                                  ROOT_ICON, TRUE, NULL);
	if (!path) {
		mlview_utils_trace_debug ("lookup of icon file failed:") ;
		mlview_utils_trace_debug (ROOT_ICON) ;
	} else {
		a_klass->icons.root = gdk_pixbuf_new_from_file (path, NULL);
		g_free (path);
	}

	path = gnome_program_locate_file (NULL, GNOME_FILE_DOMAIN_APP_DATADIR,
	                                  OPEN_ROOT_ICON, TRUE, NULL) ;
	if (!path) {
		mlview_utils_trace_debug ("lookup of icon file failed:") ;
		mlview_utils_trace_debug (OPEN_ROOT_ICON) ;
	} else {
		a_klass->icons.open_root = gdk_pixbuf_new_from_file (path, NULL);
		g_free (path);
	}

	path = gnome_program_locate_file (NULL, GNOME_FILE_DOMAIN_APP_DATADIR,
	                                  COMMENT_ICON, TRUE, NULL);
	if (!path) {
		mlview_utils_trace_debug ("lookup of icon file failed:") ;
		mlview_utils_trace_debug (COMMENT_ICON) ;
	} else {
		a_klass->icons.comment = gdk_pixbuf_new_from_file (path, NULL);
		g_free (path);
	}

	path = gnome_program_locate_file (NULL, GNOME_FILE_DOMAIN_APP_DATADIR,
	                                  ENTITY_REF_ICON, TRUE, NULL);
	if (!path) {
		mlview_utils_trace_debug ("lookup of icon file failed:") ;
		mlview_utils_trace_debug (ENTITY_REF_ICON) ;
	} else {
		a_klass->icons.entity_ref = gdk_pixbuf_new_from_file (path, NULL);
		g_free (path);
	}

	path = gnome_program_locate_file (NULL, GNOME_FILE_DOMAIN_APP_DATADIR,
	                                  PI_ICON, TRUE, NULL);
	if (!path) {
		mlview_utils_trace_debug ("lookup of icon file failed:") ;
		mlview_utils_trace_debug (PI_ICON) ;
	} else {
		a_klass->icons.pi = gdk_pixbuf_new_from_file (path, NULL);
		g_free (path);
	}

}

static void
mlview_icon_tree_free_icons (MlViewIconTreeClass *a_klass)
{
	if (a_klass->icons.element) {
		g_object_unref (G_OBJECT (a_klass->icons.element));
		a_klass->icons.element = NULL ;
	}
	if (a_klass->icons.open_element) {
		g_object_unref (G_OBJECT (a_klass->icons.open_element));
		a_klass->icons.open_element = NULL ;
	}
	if (a_klass->icons.text) {
		g_object_unref (G_OBJECT (a_klass->icons.text));
		a_klass->icons.text = NULL ;
	}
	if (a_klass->icons.root) {
		g_object_unref (G_OBJECT (a_klass->icons.root));
		a_klass->icons.root = NULL ;
	}
	if (a_klass->icons.open_root) {
		g_object_unref (G_OBJECT (a_klass->icons.open_root));
		a_klass->icons.open_root = NULL ;
	}
	if (a_klass->icons.comment) {
		g_object_unref (G_OBJECT (a_klass->icons.comment));
		a_klass->icons.comment = NULL ;
	}
	if (a_klass->icons.pi) {
		g_object_unref (G_OBJECT (a_klass->icons.pi));
		a_klass->icons.pi = NULL ;
	}
	if (a_klass->icons.entity_ref) {
		g_object_unref (G_OBJECT (a_klass->icons.entity_ref));
		a_klass->icons.entity_ref = NULL ;
	}

}

static void
mlview_icon_tree_class_init (MlViewIconTreeClass *a_klass)
{
	MlViewTreeEditorClass *p_klass = MLVIEW_TREE_EDITOR_CLASS (a_klass);

	p_klass->build_tree_view_from_xml_doc = build_tree_view_from_xml_doc;
	p_klass->update_visual_node = update_visual_node;
	p_klass->build_tree_model_from_xml_tree = build_tree_model_from_xml_tree;

	a_klass->icons.refcount = 0;
}

static void
destroy_cb (MlViewIconTree *tree, MlViewIconTreeClass *klass)
{
	klass->icons.refcount--;

	if (!klass->icons.refcount)
		mlview_icon_tree_free_icons (klass);
}

/**
 *The type builder/getter of the #MlViewIconTree class.
 *@return the type indentifier of this class.
 */
GType
mlview_icon_tree_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo type_info = {
		                                       sizeof (MlViewIconTreeClass),
		                                       NULL, NULL,
		                                       (GClassInitFunc)
		                                       mlview_icon_tree_class_init,
		                                       NULL, NULL,
		                                       sizeof (MlViewTreeEditor),
		                                       0,
		                                       NULL, NULL
		                                   };
		type = g_type_register_static (mlview_tree_editor_get_type (),
		                               "MlViewIconTree",
		                               &type_info, (GTypeFlags)0);
	}

	return type;
}


static void
mlview_tree_editor_repaint_tree (MlViewIconTree *a_this, xmlNode *a_node)
{
    if (a_node == NULL)
        return;

    xmlNode *cur_node = NULL;

    for (cur_node = a_node; cur_node; cur_node = cur_node->next) {
        mlview_tree_editor_update_visual_node2 (MLVIEW_TREE_EDITOR (a_this),
                                                cur_node,
                                                FALSE);
        mlview_tree_editor_repaint_tree (a_this, cur_node->children);
    }
}

static void
mlview_icon_tree_prefs_colour_changed_cb (MlViewIconTree *a_this)
{
    mlview::PrefsCategoryTreeview *m_prefs =
        dynamic_cast<mlview::PrefsCategoryTreeview*> (
            mlview::Preferences::get_instance ()
            ->get_category_by_id (mlview::PrefsCategoryTreeview::CATEGORY_ID));

    if (m_prefs == NULL)
        return;

    MlViewXMLDocument *document =
        mlview_tree_editor_get_mlview_xml_doc (MLVIEW_TREE_EDITOR (a_this));
    THROW_IF_FAIL (document);

    xmlDocPtr docptr = mlview_xml_document_get_native_document (document);
    THROW_IF_FAIL (docptr);

    xmlNodePtr root_node = xmlDocGetRootElement (docptr);
    THROW_IF_FAIL (root_node);

    mlview_tree_editor_repaint_tree (a_this, root_node);
}



static void
mlview_icon_tree_prefs_font_changed_cb (MlViewIconTree *a_this)
{
    mlview::PrefsCategoryTreeview *m_prefs =
        dynamic_cast<mlview::PrefsCategoryTreeview*> (
            mlview::Preferences::get_instance ()
            ->get_category_by_id (mlview::PrefsCategoryTreeview::CATEGORY_ID));

    if (m_prefs == NULL)
        return;

    const char* fontname = const_cast<char*> (
        m_prefs->get_font_name ().c_str ());

    if (fontname == NULL)
        return;

    PangoFontDescription *font_desc =
        pango_font_description_from_string (fontname);

    if (font_desc != NULL) {
        GtkTreeView *tv =
            mlview_tree_editor_get_tree_view (MLVIEW_TREE_EDITOR (a_this));
        gtk_widget_modify_font (GTK_WIDGET (tv),
                                font_desc);
        pango_font_description_free (font_desc);
    }
}

/**
 *
 */
GtkWidget *
mlview_icon_tree_new ()
{
    MlViewIconTree *editor;
    MlViewIconTreeClass *klass;

    editor = (MlViewIconTree*)g_object_new
        (mlview_icon_tree_get_type (), NULL);

    mlview_tree_editor_construct (MLVIEW_TREE_EDITOR (editor));

    klass = MLVIEW_ICON_TREE_CLASS (G_OBJECT_GET_CLASS (editor));

    g_signal_connect (G_OBJECT (editor), "destroy", G_CALLBACK (destroy_cb), klass);

    if (!klass->icons.refcount)
        mlview_icon_tree_load_icons (klass);
    klass->icons.refcount++;

    mlview::PrefsCategoryTreeview *m_prefs =
        dynamic_cast<mlview::PrefsCategoryTreeview*> (
            mlview::Preferences::get_instance ()
            ->get_category_by_id (mlview::PrefsCategoryTreeview::CATEGORY_ID));

    if (m_prefs != NULL)
    {
        m_prefs->signal_font_changed ().connect
            (sigc::bind (
                sigc::ptr_fun (&mlview_icon_tree_prefs_font_changed_cb),
                editor));

        m_prefs->signal_colour_changed ().connect
            (sigc::bind (
                sigc::ptr_fun (&mlview_icon_tree_prefs_colour_changed_cb),
                editor));
    }

    return GTK_WIDGET (editor);
}
