/* -*- Mode: C; indent-tabs-mode:nil; c-basic-offset: 8-*- */

/*
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute 
 *it and/or modify it under the terms of 
 *the GNU General Public License as published by the 
 *Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */

#include <string.h>
#include "mlview-ping-dbo.h"
#include "mlview-idbo.h"
#include "mlview-app.h"

#ifdef MLVIEW_WITH_DBUS
#define PRIVATE(obj) (obj)->priv

struct _MlViewPingDBOPriv
{
	MlViewAppContext *app_context ;
	gboolean dispose_has_run ;
} ;

static void mlview_ping_dbo_class_init (MlViewPingDBOClass *a_klass) ;

static void mlview_ping_dbo_init (MlViewPingDBO *a_this) ;

static void mlview_ping_dbo_idbo_init (MlViewIDBO *a_this) ;

static void mlview_ping_dbo_dispose (GObject *a_this) ;

static void mlview_ping_dbo_finalize (GObject *a_this) ;

static DBusHandlerResult message_handler (DBusConnection *a_connection,
        DBusMessage *a_message,
        void *a_data) ;

static void message_unregister_handler (DBusConnection *a_connection,
                                        void *a_data) ;

static DBusHandlerResult mlview_ping_object_iface_ping
(DBusConnection *a_connection,
 DBusMessage *a_message,
 void *a_data) ;

static DBusHandlerResult mlview_ping_object_iface_close_application
(DBusConnection *a_connection,
 DBusMessage *a_message,
 void *a_data) ;

static enum MlViewStatus mlview_ping_dbo_construct (MlViewPingDBO *a_this,
        MlViewAppContext *a_ctxt) ;

static enum MlViewStatus mlview_ping_dbo_get_app_from_context
(MlViewPingDBO *a_this,
 MlViewApp **a_app) ;
GObjectClass *gv_parent_class = NULL ;

/**********************
 * private methods
 **********************/

static void
mlview_ping_dbo_class_init (MlViewPingDBOClass *a_klass)
{
	GObjectClass *gobject_class = NULL ;

	g_return_if_fail (a_klass != NULL) ;
	gv_parent_class = g_type_class_peek_parent (a_klass) ;
	g_return_if_fail (gv_parent_class) ;
	gobject_class = G_OBJECT_CLASS (a_klass) ;

	gobject_class->dispose = mlview_ping_dbo_dispose ;
	gobject_class->finalize = mlview_ping_dbo_finalize ;
}

static void
mlview_ping_dbo_init (MlViewPingDBO *a_this)
{
	g_return_if_fail (a_this && MLVIEW_IS_PING_DBO (a_this)) ;

	PRIVATE (a_this) = g_try_malloc (sizeof (MlViewPingDBOPriv));
	if (!PRIVATE (a_this)) {
		mlview_utils_trace_debug ("malloc failed") ;
		return ;
	}
	memset (PRIVATE (a_this), 0, sizeof (MlViewPingDBOPriv)) ;
}

static void
mlview_ping_dbo_dispose (GObject *a_this)
{
	MlViewPingDBO *thiz = NULL ;

	g_return_if_fail (a_this && MLVIEW_IS_PING_DBO (a_this)) ;

	thiz = MLVIEW_PING_DBO (a_this) ;
	g_return_if_fail (thiz) ;

	if (!PRIVATE (thiz)) {
		return ;
	}

	if (PRIVATE (thiz)->dispose_has_run) {
		return ;
	}

	if (gv_parent_class->dispose) {
		gv_parent_class->dispose (a_this) ;
	}

	PRIVATE (thiz)->dispose_has_run = TRUE ;
}

static void
mlview_ping_dbo_finalize (GObject *a_this)
{
	MlViewPingDBO *thiz = NULL ;

	g_return_if_fail (a_this && MLVIEW_IS_PING_DBO (a_this)) ;

	thiz  = MLVIEW_PING_DBO (a_this) ;

	if (!PRIVATE (thiz)) {
		return ;
	}
	g_free (PRIVATE (thiz)) ;
	PRIVATE (thiz) = NULL ;
}

static void
mlview_ping_dbo_idbo_init (MlViewIDBO *a_this)
{
	/*put here some iface init stuffs*/
	g_return_if_fail (a_this) ;

	a_this->message_handler = message_handler ;
	a_this->message_unregister_handler = message_unregister_handler ;
}

static DBusHandlerResult
message_handler (DBusConnection *a_connection,
                 DBusMessage *a_message,
                 void *a_data)
{
	DBusHandlerResult dbus_result = DBUS_HANDLER_RESULT_NOT_YET_HANDLED ;

	mlview_utils_trace_debug
	("hit message handler MlViewPingDBO::message_handler\n") ;

	g_return_val_if_fail (a_connection && a_message,
	                      DBUS_HANDLER_RESULT_NOT_YET_HANDLED) ;


	if (dbus_message_is_method_call
	        (a_message,
	         INTERFACE_ORG_MLVIEW_PING_OBJECT_IFACE,
	         "ping")) {

		mlview_utils_trace_debug
		("method ping of interface %s is called\n",
		 INTERFACE_ORG_MLVIEW_PING_OBJECT_IFACE) ;

		return mlview_ping_object_iface_ping (a_connection,
		                                      a_message,
		                                      a_data) ;
	} else if (dbus_message_is_method_call
	           (a_message,
	            INTERFACE_ORG_MLVIEW_PING_OBJECT_IFACE,
	            "close_application")) {

		mlview_utils_trace_debug
		("method %s of interface %s is called",
		 "close_application",
		 INTERFACE_ORG_MLVIEW_PING_OBJECT_IFACE) ;

		return mlview_ping_object_iface_close_application (a_connection,
		        a_message,
		        a_data) ;
	}
	return dbus_result ;
}

static void
message_unregister_handler (DBusConnection *a_connection,
                            void *a_data)
{}

static DBusHandlerResult
mlview_ping_object_iface_ping (DBusConnection *a_connection,
                               DBusMessage *a_message,
                               void *a_data)
{
	DBusHandlerResult dbus_result = DBUS_HANDLER_RESULT_NOT_YET_HANDLED ;
	DBusError dbus_error = {0} ;
	DBusMessage *reply = NULL ;

	mlview_utils_trace_debug ("in mlview_ping_object_iface_ping\n") ;

	g_return_val_if_fail (a_connection && a_message && a_data,
	                      DBUS_HANDLER_RESULT_NOT_YET_HANDLED) ;

	dbus_error_init (&dbus_error) ;
	reply = dbus_message_new_method_return (a_message) ;
	if (!reply) {
		dbus_result = DBUS_HANDLER_RESULT_NOT_YET_HANDLED ;
		mlview_utils_trace_debug ("could not create dbus reply") ;
		goto cleanup ;
	}

	if (!dbus_message_append_args (reply,
	                               DBUS_TYPE_STRING, "pong",
	                               DBUS_TYPE_INVALID)) {
		dbus_result = DBUS_HANDLER_RESULT_NOT_YET_HANDLED ;
		mlview_utils_trace_debug ("Could not set args to dbus reply") ;
		goto cleanup ;
	}

	if (!dbus_connection_send (a_connection, reply, NULL)) {
		dbus_result = DBUS_HANDLER_RESULT_NOT_YET_HANDLED ;
		mlview_utils_trace_debug ("could not send dbus reply") ;
		goto cleanup ;
	}
	dbus_result = DBUS_HANDLER_RESULT_HANDLED ;

cleanup:
	if (reply) {
		dbus_message_unref (reply) ;
		reply = NULL ;
	}
	return dbus_result ;
}

static DBusHandlerResult
mlview_ping_object_iface_close_application (DBusConnection *a_connection,
        DBusMessage *a_message,
        void *a_data)
{
	MlViewPingDBO *thiz = NULL;
	DBusHandlerResult result = DBUS_HANDLER_RESULT_NOT_YET_HANDLED ;
	DBusMessage *reply = NULL ;
	MlViewApp *app = NULL ;

	g_return_val_if_fail (a_connection && a_message,
	                      DBUS_HANDLER_RESULT_NOT_YET_HANDLED) ;
	g_return_val_if_fail (a_data && MLVIEW_IS_PING_DBO (a_data),
	                      DBUS_HANDLER_RESULT_NOT_YET_HANDLED) ;

	thiz = MLVIEW_PING_DBO (a_data) ;
	g_return_val_if_fail (thiz, DBUS_HANDLER_RESULT_NOT_YET_HANDLED) ;

	reply = dbus_message_new_method_return (a_message) ;
	if (!reply) {
		mlview_utils_trace_debug ("failed to instanciate a dbus reply") ;
		return DBUS_HANDLER_RESULT_NOT_YET_HANDLED ;
	}

	mlview_ping_dbo_get_app_from_context (thiz, &app) ;
	if (!app) {
		mlview_utils_trace_debug ("could not get app from context") ;
		result = DBUS_HANDLER_RESULT_NOT_YET_HANDLED ;
		goto cleanup ;
	}
	mlview_app_close_all_docs (app, FALSE) ;

	if (!dbus_message_append_args (reply,
	                               DBUS_TYPE_STRING, "okay",
	                               DBUS_TYPE_INVALID) ) {
		mlview_utils_trace_debug ("could not append args to the dbus "
		                          "reply") ;
		result = DBUS_HANDLER_RESULT_NOT_YET_HANDLED ;
		goto cleanup ;
	}
	if (!dbus_connection_send (a_connection, reply, NULL)) {
		result = DBUS_HANDLER_RESULT_NOT_YET_HANDLED ;
		mlview_utils_trace_debug ("could not send dbus reply back") ;
		goto cleanup ;
	}
	result = DBUS_HANDLER_RESULT_HANDLED ;
	g_idle_add ((GSourceFunc)gtk_main_quit, NULL) ;

cleanup:
	if (reply) {
		dbus_message_unref (reply) ;
		reply = NULL ;
	}
	return result ;

}

static enum MlViewStatus
mlview_ping_dbo_construct (MlViewPingDBO *a_this,
                           MlViewAppContext *a_ctxt)
{
	g_return_val_if_fail (a_this && MLVIEW_IS_PING_DBO (a_this)
	                      && PRIVATE (a_this),
	                      MLVIEW_BAD_PARAM_ERROR) ;

	PRIVATE (a_this)->app_context = a_ctxt ;
	return MLVIEW_OK ;
}

static enum MlViewStatus
mlview_ping_dbo_get_app_from_context (MlViewPingDBO *a_this,
                                      MlViewApp **a_app)
{
	MlViewApp *app = NULL ;
	enum MlViewStatus status = MLVIEW_OK ;

	g_return_val_if_fail (a_this && MLVIEW_IS_PING_DBO (a_this)
	                      && PRIVATE (a_this) && a_app,
	                      MLVIEW_BAD_PARAM_ERROR) ;

	g_return_val_if_fail (PRIVATE (a_this)->app_context,
	                      MLVIEW_BAD_PARAM_ERROR) ;
	app = mlview_app_context_get_element (PRIVATE (a_this)->app_context,
	                                      "MlViewApp") ;
	if (!app) {
		mlview_utils_trace_debug ("could not get app from context") ;
		status = MLVIEW_ERROR ;
		*a_app = NULL ;
		goto out ;
	}
	*a_app = app ;
	status = MLVIEW_OK ;
out:
	return MLVIEW_ERROR ;
}
/*****************
 * public methods
 *****************/

GType
mlview_ping_dbo_get_type (void)
{
	static GType type = 0 ;

	if (!type) {
		static const GTypeInfo type_info = {
		                                       sizeof (MlViewPingDBOClass),
		                                       NULL, NULL,
		                                       (GClassInitFunc) mlview_ping_dbo_class_init,
		                                       NULL, NULL,
		                                       sizeof (MlViewPingDBO),
		                                       0,
		                                       (GInstanceInitFunc) mlview_ping_dbo_init
		                                   } ;
		type = g_type_register_static (G_TYPE_OBJECT,
		                               "MlViewPingDBO",
		                               &type_info, 0) ;
		static const GInterfaceInfo idbo_info = {
		                                            (GInterfaceInitFunc) mlview_ping_dbo_idbo_init,
		                                            NULL, NULL
		                                        } ;
		g_type_add_interface_static (type, MLVIEW_TYPE_IDBO,
		                             &idbo_info) ;
	}
	return type ;
}

MlViewPingDBO *
mlview_ping_dbo_new (MlViewAppContext *a_ctxt)
{
	MlViewPingDBO *result = NULL ;

	result = g_object_new (MLVIEW_TYPE_PING_DBO, NULL) ;
	if (result) {
		mlview_ping_dbo_construct (result, a_ctxt) ;
	}
	return result ;
}

#endif /*MLVIEW_WITH_DBUS*/
