/* -*- Mode: C++; indent-tabs-mode: nil; c-basic-offset: 4-*- */

/**
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute it 
 *and/or modify it under the terms of 
 *the GNU General Public License as published 
 *by the Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *MlView is distributed in the hope that it will be useful, 
 *but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. If not, write to the Free Software Foundation, Inc., 
 *59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */
#include "mlview-old-gvc.h"
#include "mlview-safe-ptr-utils.h"
#include "mlview-view-manager.h"
#include "mlview-view-factory.h"
#include "mlview-editor.h"

namespace mlview
{

struct OldGVCPriv
{
	SafePtr<ViewManager, ObjectRef, ObjectUnref> view_mgr_ptr ;
	sigc::connection switch_page_connection ;
	bool during_insert_view ;

	OldGVCPriv ():
		during_insert_view (false)
	{}
}
;//struct OldGVCPriv

OldGVC::OldGVC (): GVCIface ()
{

	m_priv = new OldGVCPriv ;
	m_priv->switch_page_connection = signal_switch_page ().connect
	                                 (sigc::mem_fun (*this, &OldGVC::on_switch_page)) ;
}

OldGVC::~OldGVC ()
{
	if (!m_priv)
		return ;

	delete m_priv ;
	m_priv = NULL ;
}

//*******************
//implem of GVCIface
//*******************
enum MlViewStatus
OldGVC::insert_view (IView *a_view, long a_index)
{
	m_priv->during_insert_view = true ;
	m_priv->switch_page_connection.block () ;

	Gtk::Widget *view_container = manage (new Gtk::VBox (true, 0)) ;
	static_cast<Gtk::Box*> (view_container)->pack_start 
	(*a_view->get_view_widget ()) ;
	view_container->set_data ("ViewImpl", (Gtk::Widget*)a_view) ;
	UString view_name = a_view->get_view_name () ;
	Gtk::Widget *tab_label = create_tab_title (a_view, view_name) ;
	view_container->show_all () ;
	insert_page (*view_container, *tab_label, a_index) ;
	m_priv->switch_page_connection.unblock () ;

	a_view->signal_view_name_changed ().connect
	(sigc::mem_fun (*this, &OldGVC::on_view_name_changed)) ;

	//notify the view that it is "swapped in"
	a_view->notify_swapped_in () ;

	//make sure the newly added view comes on "front"
	int page_index = page_num (*view_container) ;
	set_current_page (page_index) ;
	set_cur_view (a_view, true) ;

	show_all () ;

	m_priv->during_insert_view = false ;
	return MLVIEW_OK ;
}

enum MlViewStatus
OldGVC::remove_view (IView *a_view)
{

	SafePtr<IView, ObjectRef, ObjectUnref> view_ptr = a_view, other_view_ptr ;

	//***********************
	//now, really remove the
	//view from the container
	//**********************
	Gtk::Widget *view_impl = NULL;
	Gtk::Widget *view_container = NULL ;

	view_impl = view_ptr->get_view_widget () ;
	THROW_IF_FAIL (view_impl) ;

	view_container = view_impl->get_parent () ;
	THROW_IF_FAIL (view_container) ;

	gint page_number = page_num (*view_container) ;
	THROW_IF_FAIL (page_number != -1) ;
	remove_page (page_number) ;

	other_view_ptr = retrieve_current_view_from_notebook () ;
	if (other_view_ptr == a_view)
		set_cur_view (NULL, true) ;
	else
		set_cur_view (other_view_ptr, true) ;

	return MLVIEW_OK ;
}

Gtk::Widget*
OldGVC::get_embeddable_container_widget ()
{
	return this ;
}

//*****************
//protected methods
//*****************

IView*
OldGVC::retrieve_current_view_from_notebook (void)
{
	gint page_num = 0 ;
	Gtk::Widget *view_container = NULL ;
	IView *result= NULL ;

	page_num = get_current_page () ;
	if (page_num == -1) {
		gint nb_pages = 0 ;
		nb_pages = get_n_pages () ;
		if (nb_pages  == 0) {
			return NULL ;
		} else {
			LOG_TO_ERROR_STREAM ("It seems notebook is broken") ;
			return NULL ;
		}
	}
	view_container = get_nth_page (page_num) ;
	THROW_IF_FAIL (view_container) ;
	result = (IView*)view_container->get_data ("ViewImpl") ;
	return result ;
}

/**
 * Create an hbox for tab titles which contains:
 * a file icon representing the mime type
 * the label representing the current uri
 * a close button
 */
Gtk::Widget *
OldGVC::create_tab_title (IView *a_view, UString &a_title)
{
	MlViewXMLDocument *mlview_xml_document = NULL;
	MlViewFileDescriptor *file_desc = NULL;
	gchar *filename = NULL, *mime_type = NULL;
	gint w, h;
	Gtk::Widget *tab_title_box = NULL, *tab_title_ficon = NULL,
				*tab_title_label = NULL, *tab_title_cbutton = NULL, 
				*tab_title_cicon = NULL;
	GdkPixbuf *ficon_pixbuf_native = NULL;
	Glib::RefPtr<Gdk::Pixbuf> ficon_pixbuf ;

	/* get icon file name from mime type */
	mlview_xml_document = a_view->get_document () ;
	if (mlview_xml_document != NULL) {
		file_desc = mlview_xml_document_get_file_descriptor
		            (mlview_xml_document) ;
		if (file_desc != NULL) {
			mime_type = mlview_file_descriptor_get_mime_type
			            (file_desc);
			filename = g_strconcat ("gnome-mime-",
			                        replace_slashes (mime_type),
			                        NULL);
			g_free (mime_type);
		}
	}

	if (filename == NULL) {
		filename = g_strdup ("gnome-mime-text-xml");
	}
	gtk_icon_size_lookup (GTK_ICON_SIZE_MENU, &w, &h);

	tab_title_box = manage (new Gtk::HBox (false, 2)) ;

	ficon_pixbuf_native = gtk_icon_theme_load_icon
	                      (gtk_icon_theme_get_default (),
	                       filename,
	                       w,
	                       (GtkIconLookupFlags)0,
	                       NULL);
	ficon_pixbuf = Glib::wrap (ficon_pixbuf_native) ;
	tab_title_ficon = manage (new Gtk::Image (ficon_pixbuf)) ;
	tab_title_ficon->show_all () ;

	((Gtk::Box*)tab_title_box)->pack_start (*tab_title_ficon,
	                                        false, false, 0) ;

	tab_title_label = manage (new Gtk::Label (a_title));
	tab_title_label->show ();
	((Gtk::Box*)tab_title_box)->pack_start (*tab_title_label, false, true, 2);

	tab_title_cicon = manage (new Gtk::Image (Gtk::StockID ("gtk-close"),
	                          Gtk::ICON_SIZE_BUTTON)) ;

	tab_title_cicon->show ();

	tab_title_cbutton = manage (new Gtk::Button ());
	((Gtk::Button*)tab_title_cbutton)->set_relief (Gtk::RELIEF_NONE) ;

	tab_title_cbutton->set_size_request (w + 4, h + 4);

	((Gtk::Container*)tab_title_cbutton)->add
	(*tab_title_cicon) ;
	tab_title_cbutton->show () ;
	((Gtk::Box*)tab_title_box)->pack_start (*tab_title_cbutton,
	                                        false, false, 0) ;

	tab_title_box->show_all () ;

	((Gtk::Button*)tab_title_cbutton)->signal_clicked ().connect
	(mem_fun (*this, &OldGVC::on_close_tab_button_clicked)) ;

	return tab_title_box;
}

char*
OldGVC::replace_slashes (gchar* a_str)
{
	THROW_IF_FAIL (a_str != NULL);

	for (int i = 0; a_str[i] != '\0'; i++) {
		if (a_str[i] == '/')
			a_str[i] = '-';
	}
	return a_str;
}

//****************
//signal callbacks
//****************

void
OldGVC::on_switch_page (GtkNotebookPage *a_page,
                        gint a_page_num)
{
	SafePtr<IView, ObjectRef, ObjectUnref> doc_view_ptr, prev_view_ptr ;
	Gtk::Widget *cur_child_widget = NULL;
	typedef Glib::ListHandle<Widget*> Widgets ;
	THROW_IF_FAIL (m_priv) ;

	cur_child_widget = get_nth_page (a_page_num) ;
	THROW_IF_FAIL (cur_child_widget);

	doc_view_ptr = (IView*)cur_child_widget->get_data ("ViewImpl") ;
	THROW_IF_FAIL (doc_view_ptr);

	prev_view_ptr = get_cur_view () ;
	if (prev_view_ptr == doc_view_ptr)
		return ;
	signal_views_swapped ().emit (doc_view_ptr, prev_view_ptr) ;
}

void
OldGVC::on_view_name_changed (IView* a_view)
{
	UString new_view_name ;
	Gtk::Widget *view_container = NULL;
	Gtk::Widget *view_impl = NULL ;

	THROW_IF_FAIL (a_view) ;

	new_view_name = a_view->get_view_name () ;

	view_impl = a_view->get_view_widget ();
	THROW_IF_FAIL (view_impl) ;

	view_container = view_impl->get_parent () ;
	THROW_IF_FAIL (view_container) ;

	set_tab_label (*view_container, *create_tab_title (a_view, new_view_name)) ;
}

void
OldGVC::on_close_tab_button_clicked ()
{
	AppContext *app_context = AppContext::get_instance () ;
	THROW_IF_FAIL (app_context) ;

	Editor *editor = (Editor*) app_context->get_element ("MlViewEditor") ;
	THROW_IF_FAIL (editor) ;
	editor->close_xml_document () ;
}

}//namespace mlview
