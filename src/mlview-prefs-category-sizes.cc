/* -*- Mode: C++; indent-tabs-mode:true ; c-basic-offset: 4-*- */

/*
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute 
 *it and/or modify it under the terms of 
 *the GNU General Public License as published by the 
 *Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */
#include "mlview-exception.h"
#include "mlview-prefs-category-sizes.h"

namespace mlview
{

struct PrefsCategorySizesPriv
{
	static const char *MAIN_WINDOW_WIDTH_KEY;
	static const char *MAIN_WINDOW_HEIGHT_KEY;
	static const char *TREE_EDITOR_SIZE_KEY;
	static const char *COMPLETION_BOX_SIZE_KEY;
};

const char* PrefsCategorySizes::CATEGORY_ID =
    "sizes";

const char* PrefsCategorySizesPriv::MAIN_WINDOW_WIDTH_KEY =
    "/apps/mlview/sizes/main-window-width";
const char* PrefsCategorySizesPriv::MAIN_WINDOW_HEIGHT_KEY =
    "/apps/mlview/sizes/main-window-height";
const char* PrefsCategorySizesPriv::TREE_EDITOR_SIZE_KEY =
    "/apps/mlview/sizes/tree-editor-size";
const char* PrefsCategorySizesPriv::COMPLETION_BOX_SIZE_KEY =
    "/apps/mlview/sizes/completion-size";


PrefsCategorySizes::PrefsCategorySizes (PrefsStorageManager *manager)
		: PrefsCategory (PrefsCategorySizes::CATEGORY_ID, manager)
{}

PrefsCategorySizes::~PrefsCategorySizes ()
{}

int
PrefsCategorySizes::get_main_window_width_default ()
{
	try {

		return get_storage_manager ().get_default_int_value (
		           PrefsCategorySizesPriv::MAIN_WINDOW_WIDTH_KEY);
	} catch (Exception& e) {
		TRACE_EXCEPTION (e);
	}
	return 800;
}

int
PrefsCategorySizes::get_main_window_width ()
{
	try {

		return get_storage_manager ().get_int_value (
		           PrefsCategorySizesPriv::MAIN_WINDOW_WIDTH_KEY);

	} catch (Exception& e) {
		TRACE_EXCEPTION (e);
	}

	return get_main_window_width_default ();
}

void
PrefsCategorySizes::set_main_window_width (int width)
{
	try {
		get_storage_manager ().set_int_value (
				PrefsCategorySizesPriv::MAIN_WINDOW_WIDTH_KEY,
				width);
	} catch (Exception& e) {
		TRACE_EXCEPTION (e);
	}
}


int
PrefsCategorySizes::get_main_window_height_default ()
{
	try {

		return get_storage_manager ().get_default_int_value (
		           PrefsCategorySizesPriv::MAIN_WINDOW_HEIGHT_KEY);

	} catch (Exception& e) {
		TRACE_EXCEPTION (e);
	}

	return 600;
}

int
PrefsCategorySizes::get_main_window_height ()
{
	try {

		return get_storage_manager ().get_int_value (
		           PrefsCategorySizesPriv::MAIN_WINDOW_HEIGHT_KEY);

	} catch (Exception& e) {
		TRACE_EXCEPTION (e);
	}

	return get_main_window_height_default ();
}

void
PrefsCategorySizes::set_main_window_height (int height)
{
	try {

		get_storage_manager ().set_int_value (
		    PrefsCategorySizesPriv::MAIN_WINDOW_HEIGHT_KEY,
		    height);

	} catch (Exception& e) {
		TRACE_EXCEPTION (e);
	}
}


int
PrefsCategorySizes::get_tree_editor_size_default ()
{
	try {

		return get_storage_manager ().get_default_int_value (
		           PrefsCategorySizesPriv::TREE_EDITOR_SIZE_KEY);

	} catch (Exception& e) {
		TRACE_EXCEPTION (e);
	}

	return 300;
}

int
PrefsCategorySizes::get_tree_editor_size ()
{
	try {

		return get_storage_manager ().get_int_value (
		           PrefsCategorySizesPriv::TREE_EDITOR_SIZE_KEY);

	} catch (Exception& e) {
		TRACE_EXCEPTION (e);
	}

	return get_tree_editor_size_default ();
}

void
PrefsCategorySizes::set_tree_editor_size (int size)
{
	try {

		get_storage_manager ().set_int_value (
		    PrefsCategorySizesPriv::TREE_EDITOR_SIZE_KEY,
		    size);
	} catch (Exception& e) {
		TRACE_EXCEPTION (e);
	}
}


int
PrefsCategorySizes::get_completion_box_size_default ()
{
	try {

		return get_storage_manager ().get_default_int_value (
		           PrefsCategorySizesPriv::COMPLETION_BOX_SIZE_KEY);

	} catch (Exception& e) {
		TRACE_EXCEPTION (e);
	}
	return 200;
}

int
PrefsCategorySizes::get_completion_box_size ()
{
	try {

		return get_storage_manager ().get_int_value (
		           PrefsCategorySizesPriv::COMPLETION_BOX_SIZE_KEY);

	} catch (Exception& e) {
		TRACE_EXCEPTION (e);
	}
	return get_completion_box_size_default ();
}

void
PrefsCategorySizes::set_completion_box_size (int size)
{
	try {
		get_storage_manager ().set_int_value (
		    PrefsCategorySizesPriv::COMPLETION_BOX_SIZE_KEY,
		    size);
	} catch (Exception& e) {
		TRACE_EXCEPTION (e);
	}
}
} // namespace mlview
