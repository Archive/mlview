/* -*- Mode: C++; indent-tabs-mode: true; c-basic-offset: 4 -*- */
/*
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute 
 *it and/or modify it under the terms of 
 *the GNU General Public License as published by the 
 *Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYING file for copyright information.
 */
#ifndef MLVIEW_PREFS_WINDOW_H
#define MLVIEW_PREFS_WINDOW_H

#include "mlview-object.h"

namespace mlview
{

struct PrefsWindowPriv;

///
/// The preferences window.
///
class PrefsWindow : public Object
{
	friend class PrefsWindowPriv;
	PrefsWindowPriv *m_priv;

	PrefsWindow (PrefsWindow const&) ;
	PrefsWindow& operator= (PrefsWindow const&) ;

public:
	///
	/// Default constructor
	///
	PrefsWindow ();
	///
	/// Default destructor
	///
	virtual ~PrefsWindow ();

	///
	/// Display window
	///
	void show ();
	///
	/// Hide window
	///
	void hide ();
};

}

#endif
