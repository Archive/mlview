/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 8-*- */

/*This file is part of GNU MlView
 *
 *GNU MlView is free software;
 *you can redistribute it and/or modify it under the terms of
 *the GNU General Public License as
 *published by the Free Software Foundation; either version 2,
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope
 *that it will be useful, but WITHOUT ANY WARRANTY;
 *without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the
 *GNU General Public License along with MlView;
 *see the file COPYING.
 *If not, write to the Free Software Foundation,
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */

#include "mlview-validation-output.h"

namespace mlview {

typedef vector<ValidationOutput::Message*> VectorOfMessages ;

struct ValidationOutputPriv
{
        VectorOfMessages messages ;
        MlViewXMLDocument *doc ;
};


ValidationOutput::Message::~Message ()
{
}

void
ValidationOutput::Message::set (const Message &a_message)
{
        m_node = a_message.m_node ;
        m_text = a_message.m_text ;
        m_priority = a_message.m_priority ;
        m_type = a_message.m_type ;
}

ValidationOutput::Message::Message (const Message &a_message)
{
        set (a_message) ;
}

ValidationOutput::Message&
ValidationOutput::Message::operator= (Message& a_message)
{
        set (a_message) ;
        return *this ;
}

xmlNode*
ValidationOutput::Message::get_node () const
{
        return m_node ;
}

void
ValidationOutput::Message::set_node (xmlNode *a_node)
{
        m_node = a_node ;
}

const UString&
ValidationOutput::Message::get_text () const
{
        return m_text ;
}

xmlErrorLevel
ValidationOutput::Message::get_priority () const
{
        return m_priority ;
}

xmlElementType
ValidationOutput::Message::get_node_type () const
{
        return m_type ;
}

ValidationOutput::ValidationOutput (MlViewXMLDocument *a_doc)
{
        m_priv = new ValidationOutputPriv () ;
        THROW_IF_FAIL (m_priv) ;

        m_priv->doc = a_doc ;
        connect_to_doc ();
        g_object_ref (G_OBJECT (m_priv->doc));
}

ValidationOutput::~ValidationOutput ()
{
        THROW_IF_FAIL (m_priv) ;

        if (m_priv->doc) {
                disconnect_from_doc () ;
                g_object_unref (G_OBJECT (m_priv->doc)) ;
                m_priv->doc = NULL;
        }

        VectorOfMessages::iterator it ;
        for (it = m_priv->messages.begin ();
             it != m_priv->messages.end ();
             ++it) {
                if (*it) {
                        delete *it ;
                }
        }

        delete m_priv ;
        m_priv = NULL ;
}

MlViewXMLDocument *
ValidationOutput::get_document ()
{
        THROW_IF_FAIL (m_priv) ;
        return m_priv->doc ;
}

vector<mlview::ValidationOutput::Message*>&
ValidationOutput::get_messages ()
{
        THROW_IF_FAIL (m_priv) ;
        return m_priv->messages ;
}

void
ValidationOutput::append_message (ValidationOutput::Message *a_message)
{
        THROW_IF_FAIL (m_priv) ;
        m_priv->messages.push_back (a_message) ;
}

void
ValidationOutput::append_message (const UString &a_text,
                                  xmlNode *a_node,
                                  xmlErrorLevel a_priority)
{
        Message *message = new Message (a_text, a_node, a_priority,
                                        a_node?a_node->type:XML_ELEMENT_NODE) ;
        m_priv->messages.push_back (message) ;
}

void
ValidationOutput::connect_to_doc ()
{
        THROW_IF_FAIL (m_priv && MLVIEW_IS_XML_DOCUMENT (m_priv->doc));
        THROW_IF_FAIL (m_priv);

        g_signal_connect (G_OBJECT (m_priv->doc), "node-cut",
                          G_CALLBACK (ValidationOutput::xml_node_cut_cb),
                          this);

        g_signal_connect (G_OBJECT (m_priv->doc), "document-closed",
                          G_CALLBACK (ValidationOutput::xml_document_closed_cb),
                          this);

        g_signal_connect (G_OBJECT (m_priv->doc), "name-changed",
                          G_CALLBACK (ValidationOutput::xml_node_name_changed_cb),
                          this);
}

void
ValidationOutput::disconnect_from_doc ()
{
        THROW_IF_FAIL (m_priv) ;
        THROW_IF_FAIL (m_priv->doc &&
                       MLVIEW_IS_XML_DOCUMENT (m_priv->doc));

        g_signal_handlers_disconnect_by_func
                (G_OBJECT (m_priv->doc),
                 (void*)ValidationOutput::xml_node_cut_cb,
                 this);
        g_signal_handlers_disconnect_by_func
                (G_OBJECT (m_priv->doc),
                (void*) ValidationOutput::xml_document_closed_cb,
                this);
        g_signal_handlers_disconnect_by_func
                (G_OBJECT (m_priv->doc),
                 (void*) ValidationOutput::xml_node_name_changed_cb,
                 this);
}

void
ValidationOutput::xml_node_cut_cb (MlViewXMLDocument *a_xml_doc,
                                   xmlNode *a_parent_node,
                                   xmlNode *a_cut_node,
                                   ValidationOutput *a_output)
{
        THROW_IF_FAIL (a_xml_doc && MLVIEW_IS_XML_DOCUMENT (a_xml_doc));
        THROW_IF_FAIL (a_output);

        VectorOfMessages::const_iterator it ;
        for (it = a_output->get_messages ().begin ();
             it != a_output->get_messages ().end ();
             ++it) {
                ValidationOutput::Message* message = *it ;
                if (!message)
                        continue ;
                xmlNode* node = message->get_node () ;
                if (node && xmlNodeIsChildOf (node, a_cut_node)) {
                        message->set_node (NULL) ;
                }
        }
}

void
ValidationOutput::xml_document_closed_cb (MlViewXMLDocument *a_xml_doc,
                                          ValidationOutput *a_output)
{
        THROW_IF_FAIL (a_output) ;
        THROW_IF_FAIL (a_xml_doc && MLVIEW_IS_XML_DOCUMENT (a_xml_doc));

        VectorOfMessages::const_iterator it ;
        for (it = a_output->get_messages ().begin ();
             it != a_output->get_messages ().end ();
             ++it) {
                ValidationOutput::Message* message = *it ;
                if (message)
                        message->set_node (NULL);
        }
}

void
ValidationOutput::xml_node_name_changed_cb (MlViewXMLDocument *a_xml_doc,
                                            xmlNode *a_node,
                                            ValidationOutput *a_output)
{
        THROW_IF_FAIL (a_xml_doc && MLVIEW_IS_XML_DOCUMENT (a_xml_doc));
        THROW_IF_FAIL (a_output);

        VectorOfMessages::const_iterator it ;
        for (it = a_output->get_messages ().begin ();
             it != a_output->get_messages ().end ();
             ++it) {
                ValidationOutput::Message* message = *it ;
                if (!message)
                        continue ;
                if (message->get_node () && a_node)
                        message->set_node (NULL) ;
        }
}
}//end namespace mlview

