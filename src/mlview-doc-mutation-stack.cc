/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 8-*- */

/*
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute 
 *it and/or modify it under the terms of 
 *the GNU General Public License as published by the 
 *Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY;
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file copyright information.
 */

#include <string.h>
#include "mlview-doc-mutation-stack.h"

static void mlview_doc_mutation_stack_dispose (GObject *a_this) ;
static void mlview_doc_mutation_stack_finalize (GObject *a_this) ;

#define PRIVATE(object) (object)->priv

struct _MlViewDocMutationStackPrivate
{
	GList *mutations ;
	guint mutations_len ;
	gboolean dispose_has_run ;
} ;

static GObjectClass *gv_parent_class = NULL ;

static void
mlview_doc_mutation_stack_class_init (MlViewDocMutationStackClass *a_klass)

{
	GObjectClass *object_class = G_OBJECT_CLASS (a_klass) ;

	gv_parent_class = (GObjectClass *) g_type_class_peek_parent (a_klass) ;
	g_return_if_fail (G_IS_OBJECT_CLASS (gv_parent_class)) ;
	object_class->dispose =  mlview_doc_mutation_stack_dispose ;
	object_class->finalize = mlview_doc_mutation_stack_finalize ;

	/*signal creation code goes here*/
}

static void
mlview_doc_mutation_stack_init (MlViewDocMutationStack *a_this)
{
	g_return_if_fail (a_this && MLVIEW_IS_DOC_MUTATION_STACK (a_this)) ;

	if (PRIVATE (a_this))
		return ;

	PRIVATE (a_this) = (MlViewDocMutationStackPrivate *) g_try_malloc (sizeof (MlViewDocMutationStackPrivate)) ;
	if (!PRIVATE (a_this)) {
		mlview_utils_trace_debug ("System may be out of memory") ;
	}
	memset (PRIVATE (a_this), 0, sizeof (MlViewDocMutationStackPrivate)) ;
}

static void
mlview_doc_mutation_stack_dispose (GObject *a_this)
{
	MlViewDocMutationStack *thiz = NULL ;

	g_return_if_fail (a_this && MLVIEW_IS_DOC_MUTATION_STACK (a_this)) ;
	thiz = MLVIEW_DOC_MUTATION_STACK (a_this) ;
	g_return_if_fail (thiz) ;

	if (!PRIVATE (thiz) || PRIVATE (thiz)->dispose_has_run == TRUE)
		return ;

	PRIVATE (thiz)->dispose_has_run = TRUE ;
}

static void
mlview_doc_mutation_stack_finalize (GObject *a_this)
{
	MlViewDocMutationStack *thiz = NULL ;

	g_return_if_fail (a_this && MLVIEW_IS_DOC_MUTATION_STACK (a_this)) ;
	thiz = MLVIEW_DOC_MUTATION_STACK (a_this) ;
	g_return_if_fail (thiz);

	if (!PRIVATE (thiz))
		return ;
	g_free (PRIVATE (thiz)) ;
	PRIVATE (thiz) = NULL ;
}

/******************************
 *Private methods and helpers
 ******************************/


/*******************************
 *Public methods and helpers
 *******************************/

GType
mlview_doc_mutation_stack_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo type_info = {
		                                       sizeof (MlViewDocMutationStackClass),
		                                       NULL, NULL,
		                                       (GClassInitFunc)
		                                       mlview_doc_mutation_stack_class_init,
		                                       NULL, NULL,
		                                       sizeof (MlViewDocMutationStack),
		                                       0,
		                                       (GInstanceInitFunc)
		                                       mlview_doc_mutation_stack_init
		                                   };
		type = g_type_register_static (G_TYPE_OBJECT,
		                               "MlViewDocMutationStack",
		                               &type_info, (GTypeFlags) 0);
	}
	return type;
}

MlViewDocMutationStack *
mlview_doc_mutation_stack_new (void)
{
	MlViewDocMutationStack *result = NULL ;

	result = (MlViewDocMutationStack *) g_object_new (MLVIEW_TYPE_DOC_MUTATION_STACK, NULL) ;
	return result ;
}

enum MlViewStatus
mlview_doc_mutation_stack_push (MlViewDocMutationStack *a_this,
                                MlViewDocMutation *a_mutation)
{
	g_return_val_if_fail (a_this && MLVIEW_IS_DOC_MUTATION_STACK (a_this)
	                      && PRIVATE (a_this) && a_mutation,
	                      MLVIEW_BAD_PARAM_ERROR) ;

	PRIVATE (a_this)->mutations = g_list_prepend (PRIVATE (a_this)->mutations,
	                              a_mutation) ;
	PRIVATE (a_this)->mutations_len ++ ;
	mlview_doc_mutation_ref (a_mutation) ;
	return MLVIEW_OK ;
}

enum MlViewStatus
mlview_doc_mutation_stack_pop (MlViewDocMutationStack *a_this,
                               MlViewDocMutation **a_mutation)
{
	g_return_val_if_fail (a_this && MLVIEW_IS_DOC_MUTATION_STACK (a_this)
	                      && PRIVATE (a_this) && a_mutation,
	                      MLVIEW_BAD_PARAM_ERROR) ;

	if (!PRIVATE (a_this)->mutations)
		return MLVIEW_STACK_EMPTY_ERROR ;
	*a_mutation = (MlViewDocMutation *) PRIVATE (a_this)->mutations->data ;
	PRIVATE (a_this)->mutations = g_list_delete_link (PRIVATE (a_this)->mutations,
	                              PRIVATE (a_this)->mutations) ;
	PRIVATE (a_this)->mutations_len -- ;
	mlview_doc_mutation_unref (*a_mutation) ;
	return MLVIEW_OK ;
}

enum MlViewStatus
mlview_doc_mutation_stack_peek_nth (MlViewDocMutationStack *a_this,
                                    guint a_nth,
                                    MlViewDocMutation **a_mutation)
{
	g_return_val_if_fail (a_this && MLVIEW_IS_DOC_MUTATION_STACK (a_this)
	                      && PRIVATE (a_this) && a_mutation,
	                      MLVIEW_BAD_PARAM_ERROR) ;

	if (!PRIVATE (a_this)->mutations)
		return MLVIEW_STACK_EMPTY_ERROR ;

	if (a_nth > PRIVATE (a_this)->mutations_len)
		return MLVIEW_STACK_TOO_SHORT_ERROR ;

	*a_mutation = (MlViewDocMutation *) g_list_nth_data (PRIVATE (a_this)->mutations,
	              a_nth) ;
	return MLVIEW_OK ;
}

enum MlViewStatus
mlview_doc_mutation_stack_peek (MlViewDocMutationStack *a_this,
                                MlViewDocMutation **a_mutation)
{
	g_return_val_if_fail (a_this && MLVIEW_IS_DOC_MUTATION_STACK (a_this)
	                      && PRIVATE (a_this) && a_mutation,
	                      MLVIEW_BAD_PARAM_ERROR) ;

	if (!PRIVATE (a_this)->mutations || !PRIVATE (a_this)->mutations_len)
		return MLVIEW_STACK_EMPTY_ERROR ;

	return mlview_doc_mutation_stack_peek_nth (a_this, 0, a_mutation) ;
}

enum MlViewStatus
mlview_doc_mutation_stack_get_size (MlViewDocMutationStack *a_this,
                                    guint *a_size)
{
	g_return_val_if_fail (a_this && MLVIEW_IS_DOC_MUTATION_STACK (a_this)
	                      && PRIVATE (a_this) && a_size,
	                      MLVIEW_BAD_PARAM_ERROR) ;

	*a_size = PRIVATE (a_this)->mutations_len ;
	return MLVIEW_OK ;
}


enum MlViewStatus
mlview_doc_mutation_stack_clear (MlViewDocMutationStack *a_this)
{
	GList *cur_mutation = NULL ;

	g_return_val_if_fail (a_this && MLVIEW_IS_DOC_MUTATION_STACK (a_this)
	                      && PRIVATE (a_this),
	                      MLVIEW_BAD_PARAM_ERROR) ;

	if (!PRIVATE (a_this)->mutations)
		return MLVIEW_OK ;

	for (cur_mutation = PRIVATE (a_this)->mutations;
	        cur_mutation ;
	        cur_mutation = cur_mutation->next) {
		if (!cur_mutation->data)
			continue ;
		mlview_doc_mutation_unref ((MlViewDocMutation *) cur_mutation->data) ;
		cur_mutation->data = NULL ;
	}
	g_list_free (PRIVATE (a_this)->mutations) ;
	PRIVATE (a_this)->mutations = NULL ;
	PRIVATE (a_this)->mutations_len = 0 ;
	return MLVIEW_OK ;
}
