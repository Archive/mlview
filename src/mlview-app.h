/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 8-*- */

/*
 *This file is part of MlView
 *
 *MlView is free software; you can redistribute it and/or 
 *modify it under the terms of 
 *the GNU General Public License as published 
 *by the Free Software Foundation; either version 2
 *or (at your option) any later version.
 *
 *MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView.
 *see the file COPYING. 
 *If not, write to the 
 *Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */

#ifndef __MLVIEW_APP_H__
#define __MLVIEW_APP_H__

#include <glade/glade.h>
#include "mlview-object.h"
#include "mlview-editor.h"

/**
 *@file 
 *the mlview application class definition.
 *It instanciates mlview's instance of  GnomeApp, 
 *fills it with an instance of MlViewEditor, builds the application menus
 *and toolbars and creates an instance of AppContext.
 */

#define MLVIEW_ROOTWINDOW_WIDTH 800
#define MLVIEW_ROOTWINDOW_HEIGHT 600
#define MLVIEW_MAINPANED_POSITION 400

namespace mlview
{

struct AppPriv ;
/**
 *This struct
 *is a handle to access
 *widgets items build using glade.
 *e.g: menubar, menu items etc ...
 */
class App : public mlview::Object
{
friend struct AppPriv ;

	//***************
	//private member
	//***************
AppPriv * m_priv ;

	//***************
	//inner classes
	//***************
struct WidgetsHandlePriv ;
class WidgetsHandle : public Object
{

		WidgetsHandlePriv *m_priv ;

		//forbid copy construction and assignation
		WidgetsHandle (WidgetsHandle &a_handle) ;
		WidgetsHandle& operator= (WidgetsHandle &a_handle) ;

	public:
		WidgetsHandle () ;
		~WidgetsHandle () ;

		//******************************
		//getter and setters
		//******************************
		GtkWidget* get_app_win () ;
		void set_app_win (GtkWidget *a_widget) ;

		GtkWidget *get_menu_bar_container () ;
		void set_menu_bar_container (GtkWidget *a_widget) ;

		GtkWidget* get_toolbar_container () ;
		void set_toolbar_container (GtkWidget *a_widget) ;

		GtkWidget * get_main_menu_bar () ;
		void set_main_menu_bar (GtkWidget *a_widget) ;

		Editor* get_editor () ;
		void set_editor (Editor *a_widget) ;


} ;

	//***************
	//public methods
	//***************

public:

	App (const Glib::ustring& a_appname) ;

	~App () ;

	void set_visible ();

	Editor *get_editor ();

	WidgetsHandle* get_widgets_handle () ;

	enum MlViewStatus set_main_window_title (const Glib::ustring &a_title) ;

	GtkUIManager * get_ui_manager () ;

	void close_application (bool a_interactive) ;

	void close_all_docs (bool a_interactive) ;

protected:
	enum MlViewStatus init_from_glade (GladeXML *a_glade_xml) ;
	enum MlViewStatus build_widgets_handle (GladeXML *a_glade_xml) ;
	enum MlViewStatus init_editor (GladeXML *a_glade) ;
	enum MlViewStatus init_menu_and_toolbar (GladeXML *a_glade_xml) ;
	enum MlViewStatus build_and_init_menus (GladeXML *a_glade_xml) ;
	enum MlViewStatus build_view_types_choice_submenu () ;
	void set_editing_enabled (bool a_enable) ;

	//******************
	//signal handlers
	//******************
	 void on_signal_first_view_added (IView *a_view) ;
	 void on_signal_last_view_removed () ;
	
private:
	//an app can't be copied or assigned
	App (App &an_app) ;
	App& operator= (App& an_app) ;

	//*****************
	//signal callbacks
	//*****************
	void on_application_initialized () ;
	void on_document_name_changed (gpointer a_document) ;
	void on_view_swapped (Object *a_old_view, Object *a_new_view) ;
	void on_view_undo_state_changed () ;

} ;
} //namespace mlview

#endif
