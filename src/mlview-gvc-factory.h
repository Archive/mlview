/* -*- Mode: C++; indent-tabs-mode: true; c-basic-offset: 4-*- */

/*This file is part of GNU MlView
 *
 *GNU MlView is free software; 
 *you can redistribute it and/or modify it under the terms of 
 *the GNU General Public License as 
 *published by the Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope 
 *that it will be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */
#ifndef __MLVIEW_GVC_FACTORY_H__
#define __MLVIEW_GVC_FACTORY_H__

#include "mlview-app-context.h"
#include "mlview-object.h"
#include "mlview-gvc-iface.h"
#include "mlview-ustring.h"

namespace mlview {
class GVCFactory: public Object {

	//forbid instanciation, copy, assignation
	GVCFactory () ;
	GVCFactory (const GVCFactory &) ;
	GVCFactory& operator= (const GVCFactory &) ;

	public:

	static GVCIface* create_gvc (const UString &a_gvc_type_name) ;
};//end Object
}
#endif //__MLVIEW8GVC_FACTORY_H__
