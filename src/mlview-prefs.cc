/*
  Preferences : Handle application preferences
 
  This file is part of MlView
 
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include <map>
#include <string>

#include "mlview-prefs.h"
#include "mlview-prefs-category.h"
#include "mlview-prefs-category-general.h"
#include "mlview-prefs-category-search.h"
#include "mlview-prefs-category-sourceview.h"
#include "mlview-prefs-category-treeview.h"
#include "mlview-prefs-category-sizes.h"

#include "mlview-prefs-storage-manager.h"
#include "mlview-prefs-storage-gconf-impl.h"

namespace mlview
{
Preferences*
Preferences::m_instance = 0;

struct PreferencesPriv
{
	std::map<Glib::ustring,PrefsCategory*> m_categories_map;
	PrefsStorageManager *m_storage_manager;

	void setup_categories ();
};

Preferences::Preferences ()
{
	m_priv = new PreferencesPriv ();
	m_priv->m_storage_manager = new PrefsStorageGConfImpl ();
	m_priv->setup_categories ();
}

Preferences::~Preferences ()
{
	if (m_priv->m_storage_manager != NULL)
		delete m_priv->m_storage_manager;

	if (m_priv != NULL)
		delete m_priv;
}

Preferences*
Preferences::get_instance ()
{
	if (m_instance == 0)
		m_instance = new Preferences ();

	return m_instance;
}

PrefsCategory*
Preferences::get_category_by_id (const Glib::ustring& cat_id_)
{
	return m_priv->m_categories_map[cat_id_];
}

void
PreferencesPriv::setup_categories ()
{
	PrefsCategory *category = NULL;

	// General
	category = new PrefsCategoryGeneral (m_storage_manager);
	m_categories_map[category->get_id ()] = category;

	// Search
	category = new PrefsCategorySearch (m_storage_manager);
	m_categories_map[category->get_id ()] = category;

       // Sourceview
	category = new PrefsCategorySourceView (m_storage_manager);
	m_categories_map[category->get_id ()] = category;

	// Treeview
	category = new PrefsCategoryTreeview (m_storage_manager);
	m_categories_map[category->get_id ()] = category;

       // Sizes
	category = new PrefsCategorySizes (m_storage_manager);
	m_categories_map[category->get_id ()] = category;

}
} // namespace mlview
