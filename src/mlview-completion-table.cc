/* -*- Mode: C++; indent-tabs-mode:nil; c-basic-offset: 4-*- */

/*
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute 
 *it and/or modify it under the terms of 
 *the GNU General Public License as published by the 
 *Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */

#include <string.h>
#include "mlview-completion-table.h"

/**
 *@file
 *The definition of the #MlViewCompletionTable class.
 */

#define PRIVATE(completion_table) ((completion_table)->priv)

struct _MlViewCompletionTablePrivate
{
	struct
	{
		GtkListStore *feasible_children;
		GtkListStore *feasible_prev_siblings;
		GtkListStore *feasible_next_siblings;
		GtkListStore *feasible_attributes;
	}
	models;

	struct
	{
		GtkTreeSelection *feasible_children;
		GtkTreeSelection *feasible_prev_siblings;
		GtkTreeSelection *feasible_next_siblings;
		GtkTreeSelection *feasible_attributes;
	}
	selections;

	struct
	{
		GtkWidget *feasible_children;
		GtkWidget *feasible_prev_siblings;
		GtkWidget *feasible_next_siblings;
		GtkWidget *feasible_attributes;
	}
	widgets;

	MlViewXMLDocument *xml_doc;
	xmlNode *cur_node;

	gboolean dispose_has_run;
	gboolean foo ;
};

static GtkTableClass *gv_parent_class;

static void
feasible_attribute_selected_cb (GtkTreeSelection *a_tree_selection,
                                gpointer a_user_data) ;
static void
feasible_next_sibling_selected_cb (GtkTreeSelection *a_tree_selection,
                                   gpointer a_user_data) ;

static void
feasible_prev_sibling_selected_cb (GtkTreeSelection *a_tree_selection,
                                   gpointer a_user_data) ;

static void
feasible_child_selected_cb (GtkTreeSelection *a_tree_selection,
                            gpointer a_user_data) ;

static void
mlview_completion_table_finalize (GObject *a_this)
{
	MlViewCompletionTable *table;

	g_return_if_fail (a_this && MLVIEW_COMPLETION_TABLE (a_this));
	table = MLVIEW_COMPLETION_TABLE (a_this);
	g_return_if_fail (PRIVATE (table));
	g_free (PRIVATE (table));
	PRIVATE (table) = NULL;

	if (gv_parent_class
	        && G_OBJECT_CLASS (gv_parent_class)->finalize) {
		G_OBJECT_CLASS (gv_parent_class)->finalize (a_this) ;
	}
}

static void
mlview_completion_table_dispose (GObject *a_this)
{
	MlViewCompletionTable *table = NULL;

	g_return_if_fail (a_this && MLVIEW_COMPLETION_TABLE (a_this));
	table = MLVIEW_COMPLETION_TABLE (a_this);
	g_return_if_fail (PRIVATE (table));

	if (PRIVATE (table)->dispose_has_run == TRUE)
		return;

	PRIVATE (table)->dispose_has_run = TRUE;

	if (gv_parent_class
	        && G_OBJECT_CLASS (gv_parent_class)->dispose)
		G_OBJECT_CLASS (gv_parent_class)->dispose (a_this);
}

static void
mlview_completion_table_class_init (MlViewCompletionTableClass *a_klass)
{
	GObjectClass *gobject_class = NULL;

	gobject_class = G_OBJECT_CLASS (a_klass);
	g_return_if_fail (gobject_class);

	gobject_class->dispose = mlview_completion_table_dispose;
	gobject_class->finalize = mlview_completion_table_finalize;

	gv_parent_class = (GtkTableClass*) gtk_type_class (GTK_TYPE_TABLE);
}



static void
update_list_store (MlViewCompletionTable *a_this,
                   GtkWidget *a_tree_view,
                   GList *a_list)
{
	GtkTreeModel *store = NULL ;
	GtkTreeIter iter = {0} ;
	GtkTreeSelection *selection = NULL ;

	g_return_if_fail (GTK_IS_TREE_VIEW (a_tree_view)) ;

	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (a_tree_view)) ;
	gtk_tree_selection_unselect_all (selection) ;
	store = gtk_tree_view_get_model (GTK_TREE_VIEW (a_tree_view)) ;
	gtk_list_store_clear (GTK_LIST_STORE (store));
	while (a_list) {
		gtk_list_store_append (GTK_LIST_STORE (store), &iter);
		gtk_list_store_set (GTK_LIST_STORE (store), &iter,
		                    0, a_list->data, -1);
		a_list = g_list_next (a_list);
	}

}

static void
feasible_attribute_selected_cb (GtkTreeSelection *a_tree_selection,
                                gpointer a_user_data)
{
	MlViewCompletionTable *widget;
	GtkTreeIter iter;
	gchar *str = NULL;
	GList * selected_rows = NULL ;
	GtkTreeModel *model = NULL ;
	gchar *node_path = NULL ;
	enum MlViewStatus status = MLVIEW_OK ;

	g_return_if_fail (a_tree_selection && GTK_IS_TREE_SELECTION (a_tree_selection));
	g_return_if_fail (a_user_data && MLVIEW_IS_COMPLETION_TABLE (a_user_data));

	widget = MLVIEW_COMPLETION_TABLE (a_user_data);

	g_return_if_fail (PRIVATE (widget));
	g_return_if_fail (PRIVATE (widget)->cur_node);

	model = GTK_TREE_MODEL (PRIVATE (widget)->models.feasible_attributes) ;
	g_return_if_fail (model) ;
	selected_rows = gtk_tree_selection_get_selected_rows (a_tree_selection,
	                &model) ;
	if (!selected_rows)
		return ;

	mlview_xml_document_get_node_path (PRIVATE (widget)->xml_doc,
	                                   PRIVATE (widget)->cur_node,
	                                   &node_path) ;
	if (!node_path) {
		mlview_utils_trace_debug ("Could not get XPATH expr from node") ;
		return ;
	}
	if (gtk_tree_model_get_iter (model, &iter, (GtkTreePath*)selected_rows->data)) {
		gtk_tree_model_get
		(GTK_TREE_MODEL (PRIVATE (widget)->models.feasible_attributes),
		 &iter, 0, &str, -1);

		status = mlview_xml_document_set_attribute
		         (PRIVATE (widget)->xml_doc, node_path,
		          (xmlChar*)str, (xmlChar*)"value", TRUE);

		if (status != MLVIEW_OK) {
			mlview_utils_trace_debug ("Could not set attribute") ;
		}
	}
	/*cleanup:*/
	if (selected_rows) {
		g_list_foreach (selected_rows, (GFunc)gtk_tree_path_free, NULL) ;
		g_list_free (selected_rows) ;
	}
	if (node_path) {
		g_free (node_path) ;
		node_path = NULL ;
	}
}

static void
feasible_next_sibling_selected_cb (GtkTreeSelection *a_tree_selection,
                                   gpointer a_user_data)
{
	enum MlViewStatus status = MLVIEW_OK ;
	MlViewCompletionTable *widget;
	GtkTreeIter iter;
	gchar *str = NULL;
	xmlNode *new_node = NULL ;
	GList *selected_rows = NULL ;
	GtkTreeModel *model = NULL ;
	gchar *node_path = NULL ;

	g_return_if_fail (a_tree_selection
	                  && GTK_IS_TREE_SELECTION (a_tree_selection));
	g_return_if_fail (a_user_data
	                  && MLVIEW_IS_COMPLETION_TABLE (a_user_data));

	widget = MLVIEW_COMPLETION_TABLE (a_user_data);
	g_return_if_fail (widget && PRIVATE (widget)) ;
	PRIVATE (widget)->foo = TRUE ;
	g_return_if_fail (PRIVATE (widget)->cur_node);

	mlview_xml_document_get_node_path (PRIVATE (widget)->xml_doc,
	                                   PRIVATE (widget)->cur_node,
	                                   &node_path) ;
	if (!node_path) {
		mlview_utils_trace_debug ("Could not get XPATH expr from node") ;
		return ;
	}
	model = GTK_TREE_MODEL (PRIVATE (widget)->models.feasible_next_siblings) ;
	g_return_if_fail (model) ;

	selected_rows = gtk_tree_selection_get_selected_rows (a_tree_selection,
	                &model) ;
	if (!selected_rows)
		return ;

	if (gtk_tree_model_get_iter (model, &iter,
	                             (GtkTreePath*)selected_rows->data)) {
		gtk_tree_selection_unselect_iter (a_tree_selection, &iter) ;

		gtk_tree_model_get
		(GTK_TREE_MODEL (PRIVATE (widget)->models.feasible_next_siblings),
		 &iter, 0, &str, -1);

		if (strcmp (str, "#PCDATA") == 0) {
			new_node = xmlNewNode (NULL, (xmlChar*)"text");
			new_node->type = XML_TEXT_NODE;
		} else
			new_node = xmlNewNode (NULL, (xmlChar*)str);
#ifndef MLVIEW_WITH_LIBXML_EXPERIMENTAL_COMPLETION

		status = mlview_xml_document_insert_next_sibling_node
		         (PRIVATE (widget)->xml_doc,
		          node_path,
		          new_node,
		          TRUE, TRUE);
#else

		status = mlview_xml_document_insert_next_sibling_node
		         (PRIVATE (widget)->xml_doc,
		          node_path,
		          new_node, FALSE, TRUE);
#endif

		g_return_if_fail (status == MLVIEW_OK);
	}
	gtk_tree_selection_unselect_all (a_tree_selection) ;

	/*cleanup*/
	if (selected_rows) {
		g_list_foreach (selected_rows, (GFunc)gtk_tree_path_free, NULL) ;
		g_list_free (selected_rows) ;
	}
	if (node_path) {
		g_free (node_path) ;
		node_path = NULL ;
	}
}

static void
feasible_prev_sibling_selected_cb (GtkTreeSelection *a_tree_selection,
                                   gpointer a_user_data)
{
	enum MlViewStatus status = MLVIEW_OK ;
	MlViewCompletionTable *widget;
	GtkTreeIter iter;
	gchar *str = NULL;
	xmlNode *new_node = NULL ;
	GList *selected_rows = NULL ;
	GtkTreeModel *model = NULL ;
	gchar *node_path = NULL ;

	g_return_if_fail (a_tree_selection
	                  && GTK_IS_TREE_SELECTION (a_tree_selection));
	g_return_if_fail (a_user_data
	                  && MLVIEW_IS_COMPLETION_TABLE (a_user_data));

	widget = MLVIEW_COMPLETION_TABLE (a_user_data);
	g_return_if_fail (widget) ;
	g_return_if_fail (PRIVATE (widget));
	g_return_if_fail (PRIVATE (widget)->cur_node);

	model = GTK_TREE_MODEL (PRIVATE (widget)->models.feasible_prev_siblings) ;
	g_return_if_fail (model) ;

	mlview_xml_document_get_node_path (PRIVATE (widget)->xml_doc,
	                                   PRIVATE (widget)->cur_node,
	                                   &node_path) ;
	if (!node_path) {
		mlview_utils_trace_debug ("could not get XPATH expr from node") ;
		return ;
	}
	selected_rows = gtk_tree_selection_get_selected_rows (a_tree_selection,
	                &model) ;
	if (!selected_rows)
		return ;

	if (gtk_tree_model_get_iter (model, &iter,
	                             (GtkTreePath*)selected_rows->data)) {
		gtk_tree_model_get
		(GTK_TREE_MODEL (PRIVATE (widget)->models.feasible_prev_siblings),
		 &iter, 0, &str, -1);

		if (strcmp (str, "#PCDATA") == 0) {
			new_node = xmlNewNode (NULL, (xmlChar*)"text");
			new_node->type = XML_TEXT_NODE;
		} else
			new_node = xmlNewNode (NULL, (xmlChar*)str);
#ifndef MLVIEW_WITH_LIBXML_EXPERIMENTAL_COMPLETION

		status = mlview_xml_document_insert_prev_sibling_node
		         (PRIVATE (widget)->xml_doc,
		          node_path, new_node,
		          TRUE, TRUE);
#else

		status = mlview_xml_document_insert_prev_sibling_node
		         (PRIVATE (widget)->xml_doc,
		          node_path, new_node,
		          FALSE, TRUE);
#endif

		g_return_if_fail (status == MLVIEW_OK);
	}
	/*cleanup*/
	if (selected_rows) {
		g_list_foreach (selected_rows, (GFunc)gtk_tree_path_free, NULL) ;
		g_list_free (selected_rows) ;
	}
}

static void
feasible_child_selected_cb (GtkTreeSelection *a_tree_selection,
                            gpointer a_user_data)
{
	MlViewCompletionTable *widget;
	GtkTreeIter iter;
	gchar *str = NULL, *cur_node_path = NULL;
	xmlNode *new_node = NULL ;
	enum MlViewStatus status = MLVIEW_OK ;
	GList *selected_rows = NULL ;
	GtkTreeModel *model = NULL ;

	g_return_if_fail (a_tree_selection && GTK_IS_TREE_SELECTION (a_tree_selection));
	g_return_if_fail (a_user_data && MLVIEW_IS_COMPLETION_TABLE (a_user_data));

	widget = MLVIEW_COMPLETION_TABLE (a_user_data);

	g_return_if_fail (PRIVATE (widget));
	g_return_if_fail (PRIVATE (widget)->cur_node);

	model = GTK_TREE_MODEL (PRIVATE (widget)->models.feasible_children) ;
	g_return_if_fail (model) ;

	selected_rows = gtk_tree_selection_get_selected_rows (a_tree_selection,
	                &model) ;
	if (!selected_rows)
		return ;

	if (gtk_tree_model_get_iter (model, &iter,
	                             (GtkTreePath*)selected_rows->data)) {
		gtk_tree_model_get
		(GTK_TREE_MODEL (PRIVATE (widget)->models.feasible_children),
		 &iter, 0, &str, -1);

		if (strcmp (str, "#PCDATA") == 0) {
			new_node = xmlNewNode (NULL, (xmlChar*)"text");
			new_node->type = XML_TEXT_NODE;
		} else
			new_node = xmlNewNode (NULL, (xmlChar*)str);

		mlview_xml_document_get_node_path (PRIVATE (widget)->xml_doc,
		                                   PRIVATE (widget)->cur_node,
		                                   &cur_node_path) ;
		if (!cur_node_path) {
			mlview_utils_trace_debug ("Faild to get node path") ;
			goto cleanup ;
		}

#ifndef MLVIEW_WITH_LIBXML_EXPERIMENTAL_COMPLETION
		status = mlview_xml_document_add_child_node
		         (PRIVATE (widget)->xml_doc,
		          cur_node_path,
		          new_node, TRUE, TRUE);
#else

		status = mlview_xml_document_add_child_node
		         (PRIVATE (widget)->xml_doc,
		          cur_node_path,
		          new_node, FALSE, TRUE);
#endif

		if (cur_node_path) {
			g_free (cur_node_path) ;
			cur_node_path = NULL ;
		}
		g_return_if_fail (status == MLVIEW_OK);
	}
	new_node = NULL ;
cleanup:
	if (selected_rows) {
		g_list_foreach (selected_rows,
		                (GFunc)gtk_tree_path_free, NULL) ;
		g_list_free (selected_rows) ;
	}
	if (new_node) {
		xmlFreeNode (new_node) ;
		new_node = NULL ;
	}
}

static void
mlview_completion_table_init (MlViewCompletionTable *a_this)
{
	GtkWidget *scrolled = NULL;
	GtkCellRenderer *renderer = NULL;
	GtkTreeSelection *selection = NULL;

	gtk_table_set_col_spacings (GTK_TABLE (a_this), 3);
	gtk_table_set_row_spacings (GTK_TABLE (a_this), 3);

	if (PRIVATE (a_this) == NULL) {
		PRIVATE (a_this) = (MlViewCompletionTablePrivate*) g_try_malloc
		                   (sizeof (MlViewCompletionTablePrivate));
		g_return_if_fail (PRIVATE (a_this));
	}
	memset (PRIVATE (a_this), 0, sizeof (MlViewCompletionTablePrivate));

	PRIVATE (a_this)->models.feasible_children =
	    gtk_list_store_new (1, G_TYPE_STRING);
	PRIVATE (a_this)->widgets.feasible_children =
	    gtk_tree_view_new_with_model
	    (GTK_TREE_MODEL (PRIVATE (a_this)->models.feasible_children));
	renderer = gtk_cell_renderer_text_new ();
	gtk_tree_view_insert_column_with_attributes
	(GTK_TREE_VIEW
	 (PRIVATE (a_this)->widgets.feasible_children), 1,
	 _("Possible children"), renderer, "text", 0, NULL);
	selection = gtk_tree_view_get_selection
	            (GTK_TREE_VIEW
	             (PRIVATE (a_this)->widgets.feasible_children));
	/*
	 *set the selection to GTK_SELECTION_MULTIPLE to workaround
	 *a gtktreeview re-entrency bug.
	 */
	gtk_tree_selection_set_mode (selection, GTK_SELECTION_MULTIPLE) ;
	g_signal_connect (G_OBJECT (selection), "changed",
	                  G_CALLBACK (feasible_child_selected_cb),
	                  a_this);
	PRIVATE (a_this)->selections.feasible_children = selection;
	gtk_tree_selection_set_mode (selection, GTK_SELECTION_MULTIPLE) ;


	PRIVATE (a_this)->models.feasible_prev_siblings =
	    gtk_list_store_new (1, G_TYPE_STRING);
	PRIVATE (a_this)->widgets.feasible_prev_siblings =
	    gtk_tree_view_new_with_model
	    (GTK_TREE_MODEL (PRIVATE (a_this)->models.feasible_prev_siblings));
	renderer = gtk_cell_renderer_text_new ();
	gtk_tree_view_insert_column_with_attributes
	(GTK_TREE_VIEW
	 (PRIVATE (a_this)->widgets.feasible_prev_siblings),
	 1,
	 _("Possible previous siblings"),
	 renderer, "text", 0,
	 NULL);
	selection = gtk_tree_view_get_selection
	            (GTK_TREE_VIEW
	             (PRIVATE (a_this)->widgets.feasible_prev_siblings));
	gtk_tree_selection_set_mode (selection, GTK_SELECTION_MULTIPLE) ;
	g_signal_connect
	(G_OBJECT (selection), "changed",
	 G_CALLBACK (feasible_prev_sibling_selected_cb),
	 a_this);
	gtk_tree_selection_set_mode (selection, GTK_SELECTION_MULTIPLE) ;
	PRIVATE (a_this)->selections.feasible_prev_siblings = selection;


	PRIVATE (a_this)->models.feasible_next_siblings = gtk_list_store_new (1, G_TYPE_STRING);
	PRIVATE (a_this)->widgets.feasible_next_siblings = gtk_tree_view_new_with_model
	        (GTK_TREE_MODEL (PRIVATE (a_this)->models.feasible_next_siblings));
	renderer = gtk_cell_renderer_text_new ();
	gtk_tree_view_insert_column_with_attributes
	(GTK_TREE_VIEW
	 (PRIVATE (a_this)->widgets.feasible_next_siblings),
	 1,
	 _("Possible next siblings"),
	 renderer,
	 "text", 0, NULL);
	selection = gtk_tree_view_get_selection
	            (GTK_TREE_VIEW
	             (PRIVATE (a_this)->widgets.feasible_next_siblings));
	gtk_tree_selection_set_mode (selection, GTK_SELECTION_MULTIPLE) ;

	g_signal_connect (G_OBJECT (selection), "changed",
	                  G_CALLBACK (feasible_next_sibling_selected_cb),
	                  a_this);
	PRIVATE (a_this)->selections.feasible_next_siblings =
	    selection;

	PRIVATE (a_this)->models.feasible_attributes =
	    gtk_list_store_new (1, G_TYPE_STRING);
	PRIVATE (a_this)->widgets.feasible_attributes =
	    gtk_tree_view_new_with_model
	    (GTK_TREE_MODEL (PRIVATE (a_this)->models.feasible_attributes));
	renderer = gtk_cell_renderer_text_new ();
	gtk_tree_view_insert_column_with_attributes
	(GTK_TREE_VIEW
	 (PRIVATE (a_this)->widgets.feasible_attributes), 1,
	 _("Possible attributes"), renderer, "text", 0, NULL);
	selection = gtk_tree_view_get_selection
	            (GTK_TREE_VIEW (PRIVATE (a_this)->widgets.feasible_attributes));
	gtk_tree_selection_set_mode (selection, GTK_SELECTION_MULTIPLE) ;
	g_signal_connect (G_OBJECT (selection), "changed",
	                  G_CALLBACK (feasible_attribute_selected_cb),
	                  a_this);

	PRIVATE (a_this)->selections.feasible_attributes = selection;

	gtk_table_resize (GTK_TABLE (a_this), 4, 1);
	scrolled = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy
	(GTK_SCROLLED_WINDOW (scrolled),
	 GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
	gtk_container_add (GTK_CONTAINER (scrolled),
	                   PRIVATE (a_this)->widgets.feasible_children);
	gtk_table_attach_defaults (GTK_TABLE (a_this),
	                           scrolled, 0, 1, 1, 2);

	scrolled = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW
	                                (scrolled),
	                                GTK_POLICY_AUTOMATIC,
	                                GTK_POLICY_AUTOMATIC);
	gtk_container_add (GTK_CONTAINER (scrolled),
	                   PRIVATE (a_this)->widgets.feasible_prev_siblings);
	gtk_table_attach_defaults (GTK_TABLE (a_this),
	                           scrolled, 0, 1, 2, 3);

	scrolled = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy
	(GTK_SCROLLED_WINDOW (scrolled),
	 GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
	gtk_container_add (GTK_CONTAINER (scrolled),
	                   PRIVATE (a_this)->widgets.feasible_next_siblings);
	gtk_table_attach_defaults (GTK_TABLE (a_this),
	                           scrolled, 0, 1, 3, 4);

	scrolled = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy
	(GTK_SCROLLED_WINDOW (scrolled),
	 GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
	gtk_container_add (GTK_CONTAINER (scrolled),
	                   PRIVATE (a_this)->widgets.feasible_attributes);
	gtk_table_attach_defaults (GTK_TABLE (a_this),
	                           scrolled, 0, 1, 0, 1);
}

GType
mlview_completion_table_get_type (void)
{
	static GType mlview_completion_table_type = 0;

	if (!mlview_completion_table_type) {
		static const GTypeInfo
		mlview_completion_table_type_info = {
		                                        sizeof (MlViewCompletionTableClass),
		                                        NULL,   /* base_init */
		                                        NULL,   /* base_finalize */
		                                        (GClassInitFunc)
		                                        mlview_completion_table_class_init,
		                                        NULL,   /* class_finalize */
		                                        NULL,   /* class_data */
		                                        sizeof (MlViewCompletionTable),
		                                        0,
		                                        (GInstanceInitFunc)
		                                        mlview_completion_table_init
		                                    };
		mlview_completion_table_type =
		    g_type_register_static (GTK_TYPE_TABLE,
		                            "MlViewCompletionTable",
		                            &mlview_completion_table_type_info,
		                            (GTypeFlags)0);
	}
	return mlview_completion_table_type;
}

/**
 *Changes the currently selected node for the completion table.
 *@param a_widget the current #MlViewCompletionTable instance. 
 *@param a_node_found the node to be selected.
 */
void
mlview_completion_table_select_node (MlViewCompletionTable *a_widget,
                                     xmlNode *a_node_found)
{
	GList *list = NULL;

	g_return_if_fail (a_widget
	                  && MLVIEW_IS_COMPLETION_TABLE (a_widget));
	g_return_if_fail (PRIVATE (a_widget)
	                  && PRIVATE (a_widget)->xml_doc &&
	                  MLVIEW_IS_XML_DOCUMENT
	                  (PRIVATE (a_widget)->xml_doc));
	g_return_if_fail (a_node_found);


#ifndef MLVIEW_WITH_LIBXML_EXPERIMENTAL_COMPLETION

	if (a_node_found->type == XML_ELEMENT_NODE &&
	        mlview_xml_document_is_node_valid
	        (PRIVATE (a_widget)->xml_doc,
	         a_node_found))
#else

	if (a_node_found->type == XML_ELEMENT_NODE)

#endif

	{
		mlview_parsing_utils_build_element_name_completion_list
		(ADD_CHILD, a_node_found, &list);
		update_list_store (a_widget,
		                   PRIVATE (a_widget)->widgets.feasible_children,
		                   list);
		g_list_free (list);
		list = NULL;
		mlview_parsing_utils_build_element_name_completion_list
		(INSERT_BEFORE, a_node_found, &list);
		update_list_store (a_widget,
		                   PRIVATE (a_widget)->widgets.feasible_prev_siblings,
		                   list);
		g_list_free (list);
		list = NULL;
		mlview_parsing_utils_build_element_name_completion_list
		(INSERT_AFTER, a_node_found, &list);
		update_list_store (a_widget,
		                   PRIVATE (a_widget)->widgets.feasible_next_siblings,
		                   list);
		g_list_free (list);
		list = NULL;
		mlview_parsing_utils_build_attribute_name_completion_list
		(a_node_found, &list, FALSE);
		update_list_store (a_widget,
		                   PRIVATE (a_widget)->widgets.feasible_attributes,
		                   list);
		g_list_free (list);
		list = NULL;

		PRIVATE (a_widget)->cur_node = a_node_found;

		gtk_widget_set_sensitive (GTK_WIDGET (a_widget), TRUE);
	} else {
		update_list_store (a_widget,
		                   PRIVATE (a_widget)->widgets.feasible_children,
		                   NULL);
		update_list_store (a_widget,
		                   PRIVATE (a_widget)->widgets.feasible_prev_siblings,
		                   NULL);
		update_list_store (a_widget,
		                   PRIVATE (a_widget)->widgets.feasible_next_siblings,
		                   NULL);
		update_list_store (a_widget,
		                   PRIVATE (a_widget)->widgets.feasible_attributes,
		                   NULL);

		PRIVATE (a_widget)->cur_node = NULL;
		gtk_widget_set_sensitive (GTK_WIDGET (a_widget), FALSE);
	}
}

/**
 *A constructor of the #MlViewCompletionTable class.
 *@param a_xml_doc the xml document object model 
 *the #MlViewCompletionTable must work on.
 *@return the newly created #MlViewCompletionTable, or NULL in case of an error.
 */
GtkWidget *
mlview_completion_table_new (MlViewXMLDocument *a_xml_doc)
{
	MlViewCompletionTable *table = NULL;

	g_return_val_if_fail (a_xml_doc, NULL);

	table = (MlViewCompletionTable*)g_object_new
	        (MLVIEW_TYPE_COMPLETION_TABLE, NULL);
	PRIVATE (table)->xml_doc = a_xml_doc;

	return GTK_WIDGET (table);
}
