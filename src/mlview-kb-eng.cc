/* -*- Mode: C; indent-tabs-mode:nil; c-basic-offset: 8-*- */
/*
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute 
 *it and/or modify it under the terms of 
 *the GNU General Public License as published by the 
 *Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */

#include <string.h>
#include "mlview-kb-eng.h"


#define PRIVATE(kb_eng) (kb_eng)->priv

static const gint gv_keybindings_alloc_chunck_size = 16 ;
static const gint gv_keyinputs_alloc_chunck_size = 16 ;

struct _MlViewKBEngPriv
{
	struct MlViewKBDef *keybindings ;
	gint keybindings_size ;
	gint keybindings_len ;
	struct MlViewKeyInput *keyinputs ;
	gint keyinputs_size ;
	gint keyinputs_len ;
} ;

static enum MlViewStatus mlview_kb_eng_grow_keybindings_space (MlViewKBEng *a_this) ;

static enum MlViewStatus mlview_kb_eng_alloc_keybindings_space (MlViewKBEng *a_this) ;

static enum MlViewStatus mlview_kb_eng_alloc_keyinputs_space (MlViewKBEng *a_this) ;

/*
   static enum MlViewStatus mlview_kb_eng_grow_keyinputs_space (MlViewKBEng *a_this) ;
*/

static enum MlViewStatus mlview_kb_eng_append_key_input_to_queue (MlViewKBEng *a_this,
        GdkEventKey *a_event,
        struct MlViewKeyInput **a_created_input) ;

static enum MlViewStatus mlview_kb_eng_is_keyinputs_queue_full (MlViewKBEng *a_this,
        gboolean *a_result) ;

static enum MlViewStatus mlview_kb_eng_is_keyinputs_queue_empty (MlViewKBEng *a_this,
        gboolean *a_result) ;


/*************************
 *private helper methods
 *************************/

static enum  MlViewStatus
mlview_kb_eng_is_keyinputs_queue_empty (MlViewKBEng *a_this,
                                        gboolean *a_result)
{

	g_return_val_if_fail (a_this && PRIVATE (a_this),
	                      MLVIEW_BAD_PARAM_ERROR) ;

	if (PRIVATE (a_this)->keyinputs_len == 0) {
		*a_result = TRUE ;
	} else {
		*a_result = FALSE ;
	}
	return MLVIEW_OK ;
}

static enum MlViewStatus
mlview_kb_eng_is_keyinputs_queue_full (MlViewKBEng *a_this,
                                       gboolean *a_result)
{
	g_return_val_if_fail (a_this && PRIVATE (a_this),
	                      MLVIEW_BAD_PARAM_ERROR) ;

	if (PRIVATE (a_this)->keyinputs_len
	        >= PRIVATE (a_this)->keyinputs_size) {
		*a_result = TRUE ;
	} else {
		*a_result = FALSE ;
	}
	return MLVIEW_OK ;
}

static enum MlViewStatus
mlview_kb_eng_append_key_input_to_queue (MlViewKBEng *a_this,
        GdkEventKey *a_event,
        struct MlViewKeyInput **a_added_key_input)
{
	enum MlViewStatus status = MLVIEW_OK ;
	gboolean is_keyinputs_queue_full = TRUE ;
	gboolean is_keyinputs_empty = TRUE ;

	g_return_val_if_fail (a_this && PRIVATE (a_this),
	                      MLVIEW_BAD_PARAM_ERROR) ;

	/*
	 *if the key inputs buffer is nil, allocate
	 */
	if (mlview_kb_eng_is_keyinputs_queue_empty (a_this, &is_keyinputs_empty) != MLVIEW_OK)
		return MLVIEW_ERROR ;

	if (is_keyinputs_empty == TRUE)
		mlview_kb_eng_alloc_keyinputs_space (a_this) ;

	/*
	 *if the key input buffer is full, cycle !
	 */
	if (mlview_kb_eng_is_keyinputs_queue_full (a_this,
	        &is_keyinputs_queue_full) != MLVIEW_OK)
		return MLVIEW_ERROR ;

	if (is_keyinputs_queue_full == TRUE)
	{
		status = mlview_kb_eng_clear_key_inputs_queue (a_this) ;
		if (status != MLVIEW_OK)
			return status ;
	}

	/*
	 *Now, really append the key input in the buffer.
	 */
	PRIVATE (a_this)->keyinputs[PRIVATE (a_this)->keyinputs_len].key = a_event->keyval ;
	PRIVATE (a_this)->keyinputs[PRIVATE (a_this)->keyinputs_len].modifier_mask = (GdkModifierType) a_event->state ;
	PRIVATE (a_this)->keyinputs[PRIVATE (a_this)->keyinputs_len].date = a_event->time ;

	PRIVATE (a_this)->keyinputs_len ++ ;

	/*
	 *Return a pointer on the newly added instance of MlViewKeyInput
	 *if the user requested it.
	 */
	if (a_added_key_input)
	{
		*a_added_key_input = &PRIVATE (a_this)->keyinputs[PRIVATE (a_this)->keyinputs_len - 1 ] ;
	}
	return MLVIEW_OK ;
}

static enum MlViewStatus
mlview_kb_eng_alloc_keyinputs_space (MlViewKBEng *a_this)
{
	gint size = 0 ;

	g_return_val_if_fail (a_this && PRIVATE (a_this),
	                      MLVIEW_BAD_PARAM_ERROR) ;

	if (PRIVATE (a_this)->keyinputs)
		return MLVIEW_OK;

	size = gv_keyinputs_alloc_chunck_size * sizeof (struct MlViewKeyInput) ;

	PRIVATE (a_this)->keyinputs = (MlViewKeyInput *) g_try_malloc (size) ;
	if (!PRIVATE (a_this)->keyinputs) {
		mlview_utils_trace_debug ("system may be out of memory") ;
		return MLVIEW_OUT_OF_MEMORY_ERROR ;
	}
	memset (PRIVATE (a_this)->keyinputs, 0, size) ;
	PRIVATE (a_this)->keyinputs_size = gv_keyinputs_alloc_chunck_size ;
	return MLVIEW_OK ;
}

/****************
   NOT USED ATM
 ****************
static enum MlViewStatus 
mlview_kb_eng_grow_keyinputs_space (MlViewKBEng *a_this)
{
        gint size = 0, size2copy = 0 ;
        struct MlViewKeyInput *new_keyinputs = NULL ;
 
        g_return_val_if_fail (a_this && PRIVATE (a_this)
                              && PRIVATE (a_this)->keyinputs,
                              MLVIEW_BAD_PARAM_ERROR) ;
 
        
        size = (PRIVATE (a_this)->keyinputs_size 
                + gv_keyinputs_alloc_chunck_size) 
                * sizeof (struct MlViewKeyInput);
        
        new_keyinputs = g_try_malloc (size) ;
        if (!new_keyinputs) {
                mlview_utils_trace_debug ("System may be out of memory") ;
                return MLVIEW_OUT_OF_MEMORY_ERROR ;
        }
        memset (new_keyinputs, 0, size) ;
 
        size2copy = PRIVATE (a_this)->keyinputs_len 
                * sizeof (struct MlViewKeyInput) ;
        memmove (PRIVATE (a_this)->keyinputs,                  
                 new_keyinputs, size2copy) ;
 
        PRIVATE (a_this)->keyinputs_size += gv_keyinputs_alloc_chunck_size ;
 
        return MLVIEW_OK ;
}
*/

static enum MlViewStatus
mlview_kb_eng_alloc_keybindings_space (MlViewKBEng *a_this)
{
	g_return_val_if_fail (a_this && PRIVATE (a_this),
	                      MLVIEW_BAD_PARAM_ERROR) ;

	if (PRIVATE (a_this)->keybindings) {
		mlview_utils_trace_debug ("PRIVATE (a_this)->keybindings already allocated") ;
		return MLVIEW_ERROR ;
	}
	PRIVATE (a_this)->keybindings = (MlViewKBDef *) g_try_malloc
	                                (gv_keybindings_alloc_chunck_size
	                                 * sizeof (struct MlViewKBDef)) ;
	if (!PRIVATE (a_this)->keybindings) {
		mlview_utils_trace_debug ("System may be out of memory") ;
		return MLVIEW_OUT_OF_MEMORY_ERROR ;
	}
	memset (PRIVATE (a_this)->keybindings, 0,
	        gv_keybindings_alloc_chunck_size
	        * sizeof (struct MlViewKBDef)) ;
	PRIVATE (a_this)->keybindings_size =
	    gv_keybindings_alloc_chunck_size ;
	PRIVATE (a_this)->keybindings_len = 0 ;
	return MLVIEW_OK ;
}

static enum MlViewStatus
mlview_kb_eng_grow_keybindings_space (MlViewKBEng *a_this)
{
	struct MlViewKBDef *new_keybindings = NULL ;
	gint size = 0 ;

	g_return_val_if_fail (a_this && PRIVATE (a_this)
	                      && PRIVATE (a_this)->keybindings,
	                      MLVIEW_BAD_PARAM_ERROR) ;

	g_return_val_if_fail (PRIVATE (a_this)->keybindings_len
	                      <=
	                      PRIVATE (a_this)->keybindings_size,
	                      MLVIEW_BAD_PARAM_ERROR) ;

	size = (PRIVATE (a_this)->keybindings_size
	        +
	        gv_keybindings_alloc_chunck_size )
	       *
	       sizeof (struct MlViewKBDef) ;

	new_keybindings = (MlViewKBDef *) g_try_malloc (size) ;
	if (!new_keybindings) {
		mlview_utils_trace_debug ("System may be out of memory") ;
		return MLVIEW_OUT_OF_MEMORY_ERROR ;
	}
	memset (new_keybindings, 0, size) ;
	memmove (new_keybindings,
	         PRIVATE (a_this)->keybindings,
	         PRIVATE (a_this)->keybindings_len * sizeof (struct MlViewKBDef)) ;
	g_free (PRIVATE (a_this)->keybindings) ;
	PRIVATE (a_this)->keybindings = new_keybindings ;
	new_keybindings = NULL ;
	PRIVATE (a_this)->keybindings_size = size ;
	return MLVIEW_OK ;
}

/******************************************
 *Public methods of the MlViewKBEng class
 ******************************************/

MlViewKBEng *
mlview_kb_eng_new (void)
{
	MlViewKBEng *result = NULL ;

	result = (MlViewKBEng *) g_try_malloc (sizeof (MlViewKBEng)) ;
	if (!result) {
		mlview_utils_trace_debug ("System may be out of memory") ;
	}
	memset (result, 0, sizeof (MlViewKBEng)) ;
	PRIVATE (result) = (MlViewKBEngPriv *) g_try_malloc (sizeof (MlViewKBEngPriv)) ;
	if (!PRIVATE (result)) {
		mlview_utils_trace_debug ("System may be out of memory") ;
	}
	memset (PRIVATE (result), 0, sizeof (MlViewKBEngPriv)) ;

	return result ;
}

enum MlViewStatus
mlview_kb_eng_register_a_key_binding (MlViewKBEng *a_this,
                                      const struct MlViewKBDef *a_kb_def)
{
	enum MlViewStatus status = MLVIEW_OK ;

	g_return_val_if_fail (a_this && PRIVATE (a_this),
	                      MLVIEW_BAD_PARAM_ERROR) ;

	if (!PRIVATE (a_this)->keybindings)
	{
		status = mlview_kb_eng_alloc_keybindings_space (a_this) ;
		if (status != MLVIEW_OK)
			return status ;
	}
	if (PRIVATE (a_this)->keybindings_len >= PRIVATE (a_this)->keybindings_size)
	{
		status = mlview_kb_eng_grow_keybindings_space (a_this) ;
		if (status != MLVIEW_OK)
			return status ;
	}

	memmove (&PRIVATE (a_this)->keybindings[PRIVATE (a_this)->keybindings_len],
	         a_kb_def, sizeof (struct MlViewKBDef)) ;
	PRIVATE (a_this)->keybindings_len ++ ;
	return MLVIEW_OK ;
}

enum MlViewStatus
mlview_kb_eng_register_key_bindings (MlViewKBEng *a_this,
                                     const struct MlViewKBDef *a_kb_defs_tab,
                                     gint a_kb_defs_tab_len)
{
	gint i = 0 ;

	g_return_val_if_fail (a_this && PRIVATE (a_this),
	                      MLVIEW_BAD_PARAM_ERROR) ;

	for (i = 0 ; i < a_kb_defs_tab_len; i++)
	{
		mlview_kb_eng_register_a_key_binding (a_this,
		                                      &a_kb_defs_tab[i]) ;
	}
	return MLVIEW_OK ;
}

enum MlViewStatus
mlview_kb_eng_lookup_a_key_binding (MlViewKBEng *a_this,
                                    const struct MlViewKeyInput *a_key_input_tab,
                                    gint a_key_input_tab_len,
                                    struct MlViewKBDef **a_key_binding_found)
{
	enum MlViewStatus status = MLVIEW_OK ;
	gboolean found = FALSE,
	                 key_sequence_too_short = FALSE,
	                                          global_key_sequence_too_short = FALSE ;
	guint default_mod_mask = 0 ;
	gint i = 0, j = 0 ;

	g_return_val_if_fail (a_this && PRIVATE (a_this),
	                      MLVIEW_BAD_PARAM_ERROR) ;

	g_return_val_if_fail (a_this && PRIVATE (a_this)->keybindings,
	                      MLVIEW_BAD_PARAM_ERROR) ;

	g_return_val_if_fail (a_key_input_tab && a_key_binding_found,
	                      MLVIEW_BAD_PARAM_ERROR) ;

	default_mod_mask = gtk_accelerator_get_default_mod_mask () ;

	for (i = 0 ; i < PRIVATE (a_this)->keybindings_len ; i++)
	{
		gboolean get_out = FALSE ;

		for (j = 0;
		        j < PRIVATE (a_this)->keybindings[i].key_inputs_len ;
		        j++) {

			if (j >= a_key_input_tab_len) {
				key_sequence_too_short = TRUE ;
				global_key_sequence_too_short = TRUE ;
				mlview_utils_trace_debug
				("end of input key buf\n") ;
				mlview_utils_trace_debug
				("index in input key buf:%d \n", j) ;
				break ;

			}

			mlview_utils_trace_debug
			("trying ro recon key: %x; "
			 "mod & default mod mask:%x ...\n",
			 a_key_input_tab[j].key,
			 a_key_input_tab[j].modifier_mask & default_mod_mask) ;

			mlview_utils_trace_debug
			("Current key in lookup sequence: %x, "
			 "modifier & default_mode_mask: %x\n",
			 PRIVATE (a_this)->keybindings[i].key_inputs[j].key,
			 PRIVATE (a_this)->keybindings[i].key_inputs[j].modifier_mask & default_mod_mask) ;

			if (PRIVATE (a_this)->keybindings[i].key_inputs[j].key
			        != a_key_input_tab[j].key
			        || ((PRIVATE (a_this)->keybindings[i].key_inputs[j].modifier_mask & default_mod_mask)
			            != (a_key_input_tab[j].modifier_mask & default_mod_mask))) {
				get_out = TRUE ;
				break ;
			}
		}
		if (get_out == FALSE
		        && key_sequence_too_short == FALSE) {
			found = TRUE ;
			mlview_utils_trace_debug ("recognized key sequence (ok, woohoo)\n") ;
			break ;
		} else {
			mlview_utils_trace_debug ("didn't recognized key (failed)\n") ;
		}
		key_sequence_too_short = FALSE ;
	}
	if (found == TRUE)
	{
		*a_key_binding_found = &PRIVATE (a_this)->keybindings[i] ;
		mlview_utils_trace_debug ("found keybinding %s\n",
		                          PRIVATE (a_this)->keybindings[i].name) ;
	} else if (global_key_sequence_too_short == TRUE)
	{
		status = MLVIEW_KEY_SEQUENCE_TOO_SHORT_ERROR ;
		mlview_utils_trace_debug ("input sequence too short\n") ;
	} else
	{
		status = MLVIEW_KEY_BINDING_NOT_FOUND_ERROR ;
		mlview_utils_trace_debug ("didn't find keybinding\n") ;
	}
	return status ;
}

enum MlViewStatus
mlview_kb_lookup_key_binding_from_key_press (MlViewKBEng *a_this,
        GdkEventKey *a_event,
        struct MlViewKBDef **a_key_binding_found)
{
	enum MlViewStatus status = MLVIEW_OK ;
	struct MlViewKeyInput *key_input = NULL ;
	struct MlViewKBDef *keybinding = NULL ;

	/*append the key in the key queue*/
	status = mlview_kb_eng_append_key_input_to_queue (a_this,
	         a_event,
	         &key_input) ;
	if (status != MLVIEW_OK || !key_input)
		return status ;

	/*
	 *walk through the keys queue to see if we find a register 
	 *keybinding that matches the sequence of keys in the key queue.
	 */
	status = mlview_kb_eng_lookup_a_key_binding (a_this,
	         PRIVATE (a_this)->keyinputs,
	         PRIVATE (a_this)->keyinputs_len,
	         &keybinding) ;
	if (status == MLVIEW_OK)
	{
		if (keybinding) {
			mlview_utils_trace_debug ("found a keybinding\n") ;
			if (keybinding->action) {
				mlview_utils_trace_debug
				("Found an action assicated to "
				 "keybinding: %s\n",
				 keybinding->name) ;
				*a_key_binding_found = keybinding ;
				mlview_kb_eng_clear_key_inputs_queue (a_this) ;
			} else {
				mlview_utils_trace_debug
				("No action was associated "
				 "to the keybinding found\n") ;
			}
		} else {
			mlview_utils_trace_debug
			("Found a NULL keybinding ... weird\n") ;
			status = MLVIEW_ERROR ;
		}
	} else if (status != MLVIEW_KEY_SEQUENCE_TOO_SHORT_ERROR)
	{
		mlview_utils_trace_debug
		("No associated keybinding were found\n") ;
		mlview_kb_eng_clear_key_inputs_queue (a_this) ;
	} else
	{
		mlview_utils_trace_debug
		("Only Found the begining of a keybinding\n") ;
	}
	return status ;
}

enum MlViewStatus
mlview_kb_eng_clear_key_inputs_queue (MlViewKBEng *a_this)
{
	g_return_val_if_fail (a_this && PRIVATE (a_this)
	                      && PRIVATE (a_this)->keyinputs,
	                      MLVIEW_BAD_PARAM_ERROR) ;

	memset (PRIVATE (a_this)->keyinputs, 0,
	        PRIVATE (a_this)->keyinputs_size * sizeof (struct MlViewKeyInput)) ;
	PRIVATE (a_this)->keyinputs_len = 0 ;
	return MLVIEW_OK ;
}

void
mlview_kb_eng_destroy (MlViewKBEng *a_this)
{
	g_return_if_fail (a_this) ;

	if (PRIVATE (a_this)) {

		if (PRIVATE (a_this)->keybindings) {
			g_free (PRIVATE (a_this)->keybindings) ;
			PRIVATE (a_this)->keybindings = NULL ;
		}
		g_free (PRIVATE (a_this)) ;
		PRIVATE (a_this) = NULL ;
	}

	g_free (a_this) ;
}
