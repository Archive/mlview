/* -*- Mode: C++; indent-tabs-mode:true ; c-basic-offset: 4-*- */
/*
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute 
 *it and/or modify it under the terms of 
 *the GNU General Public License as published by the 
 *Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */

#include <string.h>
#include <gtk/gtk.h>
#include <gtksourceview/gtksourceview.h>
#include <gtksourceview/gtksourcelanguage.h>
#include <gtksourceview/gtksourcelanguagesmanager.h>
#include <gtksourceview/gtksourcebuffer.h>
#include "mlview-safe-ptr-utils.h"
#include "mlview-source-view.h"
#include "mlview-clipboard.h"
#include "mlview-xml-document.h"
#include "mlview-prefs.h"
#include "mlview-prefs-category-sourceview.h"

namespace mlview
{
struct SourceViewPriv
{

	/*
	 * the gtksourceview we use to actually implement 
	 * this source editing view
	 */
	SafePtr<GtkSourceView, GtkSourceViewRef, GtkSourceViewUnref> native_sv_ptr ;
	SafePtr<Gtk::Widget, GtkWidgetMMRef, GtkWidgetMMUnref> native_sv_wrapper_ptr ;
	/* The language manager, used by native_sv */
	GtkSourceLanguagesManager *languages_manager ;

	GtkUIManager *ui_manager ;

	GtkActionGroup *action_group ;

	guint popup_edit_menu_merge_id ;

	guint edit_menu_merge_id ;

	/* Preferences */
	PrefsCategorySourceView *m_prefs;

	/* <gtksourceviewparameters> */
	bool show_line_numbers ;
	guint tabs_width ;
	bool replace_tabs_with_spaces ;
	bool set_autoindent ;
	bool set_show_margin ;
	guint margin ;
	/*</gtksourceviewparameters>*/

	/*is set if the doc has been modified, but not this view*/
	bool document_changed ;

	/*is set if this view has been modified but not the doc (yet)*/
	bool view_changed ;
	static GtkActionEntry edit_menu_actions [] ;

	SourceViewPriv () :
			native_sv_ptr (NULL),
			native_sv_wrapper_ptr (NULL),
			languages_manager (NULL),
			ui_manager (NULL),
			action_group (NULL),
			popup_edit_menu_merge_id (0),
			edit_menu_merge_id (0),
			show_line_numbers (0) ,
			tabs_width (0) ,
			set_autoindent (false),
			set_show_margin (false),
			margin (0),
			document_changed (false),
			view_changed (false)
	{}

	void load_preferences ();
	void prefs_show_line_numbers_changed ();
	void prefs_tabs_width_changed ();
	void prefs_autoindent_changed ();
	void prefs_replace_tabs_changed ();
	void prefs_show_margin_changed ();
	void prefs_margin_changed ();
	void prefs_font_changed ();
};


GtkActionEntry SourceViewPriv::edit_menu_actions [] = {
            {
                "CloseTagAction", NULL, N_("Close last tag"),
                "<control>t", N_("Closes the last opened tag, if any"),
                G_CALLBACK (SourceView::close_tag_action_cb)
            },

            {
                "CloseAllTagsAction", NULL, N_("Close all tags"),
                "<control>T", N_("Closes all the relevant opened tags, if any"),
                G_CALLBACK (SourceView::close_all_tag_action_cb)
            },

            {
                "CutAction", GTK_STOCK_CUT, NULL,
                "<control>C", NULL ,
                G_CALLBACK (SourceView::on_cut_menu_action)
            },

            {
                "CopyAction", GTK_STOCK_COPY, NULL,
                "<control>C", NULL ,
                G_CALLBACK (SourceView::on_copy_menu_action)
            },

            {
                "PasteAction", GTK_STOCK_PASTE, NULL ,
                "<control>V", NULL,
                G_CALLBACK (SourceView::on_paste_menu_action)
            },
            {
                "DeleteAction", GTK_STOCK_DELETE, NULL ,
                NULL, NULL,
                G_CALLBACK (SourceView::on_delete_menu_action)
            }
        };

IView *
create_source_view_instance (MlViewXMLDocument *a_doc,
                             const gchar *a_name)
{
	THROW_IF_FAIL (a_doc && MLVIEW_IS_XML_DOCUMENT (a_doc)) ;

	IView *view = new SourceView (a_doc, a_name) ;
	return view ;
}

/*************************************************
 * Private methods and signal callbacks
 ************************************************/

void
SourceViewPriv::load_preferences ()
{
	// Initialize from preferences
	GtkSourceView *source_view = (GtkSourceView*)native_sv_ptr;

	gtk_source_view_set_show_line_numbers (source_view,
	                                       m_prefs->show_line_numbers ());

	if (m_prefs->get_tabs_width () > 0) {
		gtk_source_view_set_tabs_width (source_view,
										m_prefs->get_tabs_width ());
	}

	gtk_source_view_set_auto_indent (source_view,
	                                 m_prefs->auto_indent ());

	gtk_source_view_set_insert_spaces_instead_of_tabs
	(source_view,
	 m_prefs->replace_tabs_with_spaces ());

	gtk_source_view_set_show_margin (source_view,
	                                 m_prefs->show_margin ());

	if (m_prefs->get_margin_position () > 1) {
		gtk_source_view_set_margin (source_view,
									m_prefs->get_margin_position ());
	}

	UString fontname (m_prefs->get_font_name ()) ;
	if (fontname == "")
		return;

	PangoFontDescription *font_desc =
	    pango_font_description_from_string (fontname);

	if (font_desc != NULL) {
		gtk_widget_modify_font (GTK_WIDGET (source_view),
		                        font_desc);
		pango_font_description_free (font_desc);
	}

	// Connect preferences change signal
	m_prefs->signal_show_line_number_changed ().connect (
	    sigc::mem_fun (*this,
	                   &SourceViewPriv::prefs_show_line_numbers_changed));

	m_prefs->signal_tabs_width_changed ().connect (
	    sigc::mem_fun (*this,
	                   &SourceViewPriv::prefs_tabs_width_changed));

	m_prefs->signal_auto_indent_changed ().connect (
	    sigc::mem_fun (*this,
	                   &SourceViewPriv::prefs_autoindent_changed));

	m_prefs->signal_replace_tabs_changed ().connect (
	    sigc::mem_fun (*this,
	                   &SourceViewPriv::prefs_replace_tabs_changed));

	m_prefs->signal_show_margin_changed ().connect (
	    sigc::mem_fun (*this,
	                   &SourceViewPriv::prefs_show_margin_changed));
m_prefs->signal_margin_position_changed ().connect ( sigc::mem_fun (*this, &SourceViewPriv::prefs_margin_changed)); 
	m_prefs->signal_font_name_changed ().connect (
	    sigc::mem_fun (*this,
	                   &SourceViewPriv::prefs_font_changed));
}

void
SourceViewPriv::prefs_show_line_numbers_changed ()
{
	GtkSourceView *source_view = (GtkSourceView*)native_sv_ptr;

	gtk_source_view_set_show_line_numbers (source_view,
	                                       m_prefs->show_line_numbers ());
}

void
SourceViewPriv::prefs_tabs_width_changed ()
{
	GtkSourceView *source_view = (GtkSourceView*)native_sv_ptr;

	gtk_source_view_set_tabs_width (source_view,
	                                m_prefs->get_tabs_width ());
}

void
SourceViewPriv::prefs_autoindent_changed ()
{
	GtkSourceView *source_view = (GtkSourceView*)native_sv_ptr;

	gtk_source_view_set_auto_indent (source_view,
	                                 m_prefs->auto_indent ());
}

void
SourceViewPriv::prefs_replace_tabs_changed ()
{
	GtkSourceView *source_view = (GtkSourceView*)native_sv_ptr;

	gtk_source_view_set_insert_spaces_instead_of_tabs
	(source_view,
	 m_prefs->replace_tabs_with_spaces ());
}

void
SourceViewPriv::prefs_show_margin_changed ()
{
	GtkSourceView *source_view = (GtkSourceView*)native_sv_ptr;

	gtk_source_view_set_show_margin (source_view,
	                                 m_prefs->show_margin ());
}

void
SourceViewPriv::prefs_margin_changed ()
{
	GtkSourceView *source_view = (GtkSourceView*)native_sv_ptr;

	gtk_source_view_set_margin (source_view,
	                            m_prefs->get_margin_position ());
}

void
SourceViewPriv::prefs_font_changed ()
{
	GtkSourceView *source_view = (GtkSourceView*)native_sv_ptr;

	const char* fontname = const_cast<char*> (
	                           m_prefs->get_font_name ().c_str ());

	if (fontname == NULL)
		return;

	PangoFontDescription *font_desc =
	    pango_font_description_from_string (fontname);

	if (font_desc != NULL) {
		gtk_widget_modify_font (GTK_WIDGET (source_view),
		                        font_desc);
		pango_font_description_free (font_desc);
	}
}



//******************************
//static legacy signal handlers
//******************************

void
SourceView::document_changed_cb (MlViewXMLDocument *a_doc,
                                 SourceView *a_view)
{
	THROW_IF_FAIL (a_doc && MLVIEW_IS_XML_DOCUMENT (a_doc)) ;
	THROW_IF_FAIL (a_view) ;

	a_view->m_priv->document_changed = true ;
}


void
SourceView::close_all_tag_action_cb (GtkAction *a_action,
                                     gpointer a_user_data)
{
	SourceView *view = NULL ;
	bool tag_has_been_closed = FALSE ;

	view = (SourceView*) a_user_data;
	THROW_IF_FAIL (view) ;

	while (TRUE) {

		tag_has_been_closed = view->close_currently_opened_tag () ;

		if (tag_has_been_closed == FALSE)
			break ;
		else
			tag_has_been_closed = FALSE ;
	}
}

void
SourceView::close_tag_action_cb (GtkAction *a_action,
                                 gpointer a_user_data)
{
	SourceView *view = NULL ;
	gboolean tag_has_been_closed = FALSE ;

	view = (SourceView*) a_user_data ;
	THROW_IF_FAIL (view) ;

	/*Try to close the last opened tag*/
	tag_has_been_closed = view->close_currently_opened_tag () ;
}

void
SourceView::on_cut_menu_action (GtkAction *a_action,
                                gpointer a_user_data)
{
	THROW_IF_FAIL (GTK_IS_ACTION (a_action)) ;
	THROW_IF_FAIL (a_user_data) ;

	SourceView *source_view = static_cast<SourceView*> (a_user_data) ;
	source_view->cut_selected_text () ;
}

void
SourceView::on_copy_menu_action (GtkAction *a_action,
                                 gpointer a_user_data)
{
	THROW_IF_FAIL (GTK_IS_ACTION (a_action)) ;
	THROW_IF_FAIL (a_user_data) ;

	SourceView *source_view = static_cast<SourceView*> (a_user_data) ;
	source_view->copy_selected_text () ;
}

void
SourceView::on_paste_menu_action (GtkAction *a_action,
                                  gpointer a_user_data)
{
	THROW_IF_FAIL (GTK_IS_ACTION (a_action)) ;
	THROW_IF_FAIL (a_user_data) ;

	SourceView *source_view = static_cast<SourceView*> (a_user_data) ;
	source_view->paste_text () ;
}

void
SourceView::on_delete_menu_action (GtkAction *a_action, gpointer a_user_data)
{
	THROW_IF_FAIL (GTK_IS_ACTION (a_action)) ;
	THROW_IF_FAIL (a_user_data) ;

	SourceView *source_view = static_cast<SourceView*> (a_user_data) ;
	source_view->delete_selected_text () ;
}

void
SourceView::going_to_save_cb (MlViewXMLDocument *a_doc,
                              SourceView *a_view)
{
	THROW_IF_FAIL (a_doc && MLVIEW_IS_XML_DOCUMENT (a_doc)
	               && a_view) ;

	if (a_view->has_changed ()) {
		a_view->save_text_buffer_into_xml_doc () ;
	}
}

void
SourceView::changed_cb (GtkTextBuffer *a_text_buffer,
                        gpointer a_data)
{
	SourceView *view = NULL ;

	THROW_IF_FAIL (a_text_buffer && GTK_IS_TEXT_BUFFER (a_text_buffer)
	               && a_data) ;

	view = (SourceView*) a_data ;
	view->m_priv->view_changed  = true ;
}

void
SourceView::undo_state_changed_cb (GtkSourceBuffer *source_buffer,
                                   gboolean an_arg,
                                   gpointer a_source_view)
{
	SourceView *view = NULL ;

	THROW_IF_FAIL (source_buffer
	               && GTK_IS_SOURCE_BUFFER (source_buffer)) ;

	view = (SourceView*)a_source_view ;

	THROW_IF_FAIL (view) ;

	AppContext *context = AppContext::get_instance () ;
	THROW_IF_FAIL (context) ;
	context->notify_view_undo_state_changed () ;
}

bool
SourceView::native_sv_button_press_cb (GtkSourceBuffer *a_native_sv,
                                       GdkEventButton *a_event)
{
	THROW_IF_FAIL (a_native_sv && GTK_IS_SOURCE_VIEW (a_native_sv)) ;
	THROW_IF_FAIL (a_event) ;

	AppContext * context = AppContext::get_instance () ;
	THROW_IF_FAIL (context) ;

	switch (a_event->type) {
	case GDK_BUTTON_PRESS:
		if (a_event->button == 3) {
			/*
			 * user pressed the right mouse button.
			 * Let's trigger the contextual menu mecanism.
			 */
			context->notify_contextual_menu_request (GTK_WIDGET (a_native_sv),
			        (GdkEvent*)a_event) ;
			return true;

		}
	default:
		break ;
	}
	return false;
}

GtkSourceLanguagesManager *
SourceView::get_languages_manager ()
{
	THROW_IF_FAIL (m_priv) ;

	if (!m_priv->languages_manager) {
		m_priv->languages_manager =
		    gtk_source_languages_manager_new () ;
		THROW_IF_FAIL (m_priv->languages_manager) ;
	}
	return m_priv->languages_manager ;
}

enum MlViewStatus
SourceView::set_language (GtkSourceLanguage *a_language)
{
	GtkSourceBuffer *source_buffer = NULL ;

	THROW_IF_FAIL (a_language) ;
	source_buffer = GTK_SOURCE_BUFFER
	                (gtk_text_view_get_buffer (GTK_TEXT_VIEW (m_priv->native_sv_ptr.get ()))) ;
	THROW_IF_FAIL (source_buffer) ;

	gtk_source_buffer_set_highlight (source_buffer, TRUE) ;
	gtk_source_buffer_set_language (source_buffer, a_language) ;

	return MLVIEW_OK ;
}

enum MlViewStatus
SourceView::set_language_from_mime_type (const UString &a_mime_type)
{
	GtkSourceLanguagesManager *lm = NULL ;
	GtkSourceLanguage *language = NULL ;

	THROW_IF_FAIL (m_priv) ;
	lm = get_languages_manager () ;
	THROW_IF_FAIL (lm) ;
	language = gtk_source_languages_manager_get_language_from_mime_type
	           (lm, a_mime_type.c_str ()) ;
	LOG_TO_ERROR_STREAM ("language associated to '"
	                     << a_mime_type
	                     <<"'mime type: "
	                     << language) ;
	if (!language) {
		language =
		    gtk_source_languages_manager_get_language_from_mime_type
		    (lm,"text/xml") ;
		THROW_IF_FAIL (language) ;
		LOG_TO_ERROR_STREAM ("falling back to the language associated to"
		                     "mime type 'text/xml'") ;
	}
	set_language (language) ;
	return MLVIEW_OK ;
}

enum MlViewStatus
SourceView::set_default_language ()
{
	UString mime_type = NULL ;
	MlViewXMLDocument *doc = NULL ;

	THROW_IF_FAIL (m_priv) ;

	doc = get_document () ;
	THROW_IF_FAIL (doc && MLVIEW_IS_XML_DOCUMENT (doc)) ;
	mime_type = mlview_xml_document_get_mime_type (doc) ;
	LOG_TO_ERROR_STREAM ("mime_type: " << mime_type) ;
	set_language_from_mime_type (mime_type) ;
	return MLVIEW_OK ;
}

//***************
//protected methods
//*****************

Clipboard&
SourceView::get_clipboard ()
{
	AppContext *app_context = AppContext::get_instance () ;
	THROW_IF_FAIL (app_context) ;
	Clipboard *cb = app_context->get_clipboard () ;
	THROW_IF_FAIL (cb) ;
	return *cb ;
}

enum MlViewStatus
SourceView::get_selected_text (UString &a_selected_text)
{
	THROW_IF_FAIL (m_priv) ;
	THROW_IF_FAIL (m_priv->native_sv_ptr) ;
	GtkTextBuffer *text_buffer = NULL ;
	text_buffer = get_text_buffer () ;
	THROW_IF_FAIL (text_buffer) ;

	GtkTextIter start={0}, end={0} ;
	gboolean has_selection = gtk_text_buffer_get_selection_bounds
	                         (text_buffer, &start, &end);
	if (has_selection == FALSE) {
		a_selected_text = "" ;
		return MLVIEW_OK ;
	}
	SafePtr <gchar, GMemRef, GMemUnref> c_str = gtk_text_buffer_get_text
	        (text_buffer, &start,
	         &end, FALSE) ;
	a_selected_text = static_cast <gchar*> (c_str) ;
	return MLVIEW_OK ;
}

enum MlViewStatus
SourceView::delete_selected_text ()
{
	THROW_IF_FAIL (m_priv) ;
	THROW_IF_FAIL (m_priv->native_sv_ptr) ;

	GtkTextBuffer *text_buffer = NULL ;
	text_buffer = get_text_buffer () ;
	THROW_IF_FAIL (text_buffer) ;

	GtkTextIter start={0}, end={0} ;
	gboolean has_selection = gtk_text_buffer_get_selection_bounds
	                         (text_buffer, &start, &end) ;
	if (has_selection == TRUE) {
		gtk_text_buffer_delete (text_buffer, &start, &end) ;
	}
	return MLVIEW_OK ;
}

enum MlViewStatus
SourceView::handle_contextual_menu_request (GtkWidget *a_source_widget,
        GdkEvent *a_event)
{
	GtkWidget *menu = NULL ;
	GdkEventButton *event_button = NULL ;
	AppContext *ctxt = NULL ;

	THROW_IF_FAIL (m_priv) ;

	/*
	 * make sure the widget that was the source of the request is
	 * the native source view widget
	 */
	if (a_source_widget != GTK_WIDGET (m_priv->native_sv_ptr.get ())) {
		return MLVIEW_ERROR ;
	}

	/*make sure the event is really a button press*/
	if (a_event->type != GDK_BUTTON_PRESS) {
		return MLVIEW_ERROR ;
	}
	event_button = (GdkEventButton*) a_event ;
	menu = get_contextual_menu () ;
	if (!menu) {
		mlview_utils_trace_debug ("menu construction failed !") ;
		return MLVIEW_ERROR ;
	}

	ctxt = AppContext::get_instance () ;
	gtk_menu_popup (GTK_MENU (menu), NULL, NULL, NULL,
	                ctxt, ((GdkEventButton*)a_event)->button,
	                ((GdkEventButton*)a_event)->time) ;
	return MLVIEW_OK ;
}

enum MlViewStatus
SourceView::build_contextual_menu ()
{
	enum MlViewStatus status = MLVIEW_OK ;

	THROW_IF_FAIL (m_priv) ;

	UString menu_root_path = build_edit_menu_root_path (true) ;
	status = build_edit_menu_body (menu_root_path) ;

	return status;
}

UString
SourceView::build_edit_menu_root_path (bool a_popup)
{
	UString menu_root_path ;

	if (a_popup) {
		menu_root_path = "/SourceViewPopupEditMenu";
	} else {
		menu_root_path = "MainMenubar/EditMenu";
	}
	return menu_root_path ;
}

enum MlViewStatus
SourceView::build_edit_menu_body (const UString &a_menu_root_path)
{
	guint *merge_id = NULL ;
	/*gchar *parent_menu_path = NULL ;*/
	GtkUIManager *ui_manager = NULL ;

	/*TODO: finish this !*/
	ui_manager = get_ui_manager () ;
	THROW_IF_FAIL (ui_manager) ;

	if (a_menu_root_path == "/MainMenubar/EditMenu") {
		if (!m_priv->edit_menu_merge_id) {
			m_priv->edit_menu_merge_id =
			    gtk_ui_manager_new_merge_id  (ui_manager);
		}
		merge_id = &m_priv->edit_menu_merge_id ;
	} else if (a_menu_root_path == "/SourceViewPopupEditMenu") {
		if (!m_priv->popup_edit_menu_merge_id) {
			m_priv->popup_edit_menu_merge_id =
			    gtk_ui_manager_new_merge_id (ui_manager) ;
		}
		merge_id = &m_priv->popup_edit_menu_merge_id ;
	} else {
		LOG_TO_ERROR_STREAM ("Unknow menu root path: "
		                     << a_menu_root_path) ;
		return MLVIEW_ERROR ;
	}

	THROW_IF_FAIL (*merge_id) ;
	/*now the real meat of the contextual menu creation goes here !*/

	gtk_ui_manager_add_ui (ui_manager,
	                       *merge_id,
	                       a_menu_root_path.c_str (),
	                       "CloseTagMenuitem",
	                       "CloseTagAction",
	                       GTK_UI_MANAGER_AUTO,
	                       FALSE) ;

	gtk_ui_manager_add_ui (ui_manager,
	                       *merge_id,
	                       a_menu_root_path.c_str (),
	                       "CloseAllTagsMenuitem",
	                       "CloseAllTagsAction",
	                       GTK_UI_MANAGER_AUTO,
	                       FALSE) ;

	gtk_ui_manager_add_ui (ui_manager,
	                       *merge_id,
	                       a_menu_root_path.c_str (),
	                       "SourceViewEditMenuSeparator1",
	                       NULL,
	                       GTK_UI_MANAGER_SEPARATOR,
	                       FALSE) ;

	gtk_ui_manager_add_ui (ui_manager,
	                       *merge_id,
	                       a_menu_root_path.c_str (),
	                       "CutMenuitem",
	                       "CutAction",
	                       GTK_UI_MANAGER_AUTO,
	                       FALSE) ;

	gtk_ui_manager_add_ui (ui_manager,
	                       *merge_id,
	                       a_menu_root_path.c_str (),
	                       "CopyMenuitem",
	                       "CopyAction",
	                       GTK_UI_MANAGER_AUTO,
	                       FALSE) ;

	gtk_ui_manager_add_ui (ui_manager,
	                       *merge_id,
	                       a_menu_root_path.c_str (),
	                       "PasteMenuitem",
	                       "PasteAction",
	                       GTK_UI_MANAGER_AUTO,
	                       FALSE) ;

	gtk_ui_manager_add_ui (ui_manager,
	                       *merge_id,
	                       a_menu_root_path.c_str (),
	                       "DeleteMenuitem",
	                       "DeleteAction",
	                       GTK_UI_MANAGER_AUTO,
	                       FALSE) ;

	gtk_ui_manager_ensure_update (ui_manager) ;

	return MLVIEW_OK ;
}

enum MlViewStatus
SourceView::undo ()
{
	GtkSourceBuffer *source_buffer = NULL ;

	THROW_IF_FAIL (m_priv) ;

	source_buffer = get_source_buffer () ;
	gtk_source_buffer_undo (source_buffer) ;

	return MLVIEW_OK ;
}

enum MlViewStatus
SourceView::redo ()
{
	GtkSourceBuffer *source_buffer = NULL ;

	THROW_IF_FAIL (m_priv) ;

	source_buffer = get_source_buffer () ;
	gtk_source_buffer_redo (source_buffer) ;

	return MLVIEW_OK ;
}

bool
SourceView::can_undo ()
{
	GtkSourceBuffer *source_buffer = NULL ;

	THROW_IF_FAIL (m_priv) ;

	source_buffer= get_source_buffer () ;
	return gtk_source_buffer_can_undo (source_buffer) ;
}

bool
SourceView::can_redo ()
{
	GtkSourceBuffer *source_buffer = NULL ;

	THROW_IF_FAIL (m_priv) ;

	source_buffer = get_source_buffer () ;
	return gtk_source_buffer_can_redo (source_buffer) ;
}

enum MlViewStatus
SourceView::get_last_dangling_opened_tag (GtkTextIter *a_iter,
        gchar **a_tag_name)
{
	GtkTextMark *insert_mark = NULL ;
	GtkTextIter cur = {0} , *stag_end = NULL,*tmp_iter = NULL ;
	GtkTextBuffer *text_buffer = NULL ;
	gboolean found_lt = FALSE, is_an_empty_tag ;
	enum MlViewStatus status = MLVIEW_OK ;
	GString *tag_name = NULL ;
	GList *attrs = NULL ;/*a list of struc NameValuePair for the attributes*/
	GList *closed_tags_stack = NULL ;

	THROW_IF_FAIL (m_priv) ;

	text_buffer = get_text_buffer () ;
	if (!text_buffer) {
		mlview_utils_trace_debug ("could not get text buffer") ;
		return MLVIEW_ERROR ;
	}
	insert_mark = gtk_text_buffer_get_insert (text_buffer) ;
	THROW_IF_FAIL (insert_mark) ;
	gtk_text_buffer_get_iter_at_mark (text_buffer, &cur,
	                                  insert_mark) ;

fetch: /*go find the previous opened tag*/

	while (1) {
		gunichar cur_char = 0 ;

		cur_char = gtk_text_iter_get_char (&cur) ;
		if (cur_char == '<') {
			found_lt = TRUE ;
			break ;
		}

		if (!gtk_text_iter_backward_char (&cur)) {
			found_lt = FALSE ;
			break ;
		}
	}
	if (!found_lt) {
		status = MLVIEW_NO_STAG_ERROR ;
		goto cleanup ;
	}
	status = mlview_utils_parse_start_tag2 (&cur, &tag_name,
	                                        &attrs, &tmp_iter,
	                                        &is_an_empty_tag) ;
	/*END OF FETCH*/

	if (status == MLVIEW_OK && is_an_empty_tag == FALSE) {
		gchar *tag = NULL ;
		gchar** ptr = NULL ;
		/*
		 * the first tag we see when walking backward is an open tag
		 * so we found the last tag start we were looking for. 
		 */

		/*
		 * check if this tag doesn't have a matching closing tag ...
		 */
		if (closed_tags_stack) {
			ptr = &tag ;
			mlview_utils_peek_from_stack (closed_tags_stack,
			                              (gpointer*)ptr) ;
		}
		if (tag && !strcmp (tag, tag_name->str)) {
			/*
			 * yes, this stag has a matching closing tag.
			 * pop the closed_tags_staack and go backward fetch
			 * another tag.
			 */
			ptr = &tag ;
			closed_tags_stack = mlview_utils_pop_from_stack
			                    (closed_tags_stack,
			                     (gpointer*)ptr) ;
			if (tag) {
				g_free (tag) ;
				tag = NULL ;
			}
			if (!gtk_text_iter_backward_char (&cur)) {
				status = MLVIEW_ERROR ;
				goto cleanup ;
			}
			goto fetch ;
		}

		/*now prepare data for the caller and get out gently.*/
		if (a_iter) {
			a_iter = tmp_iter ;
		}
		tmp_iter = NULL ;
		*a_tag_name = g_strdup (tag_name->str) ;

		status =  MLVIEW_OK ;
		goto cleanup ;
	} else if (status == MLVIEW_OK && is_an_empty_tag == TRUE) {
		/*
		 * hmmh, we found an empty tag. we need to go backward fetch
		 * an open empty tag ...
		 */
		if (!gtk_text_iter_backward_char (&cur)) {
			status = MLVIEW_NO_DANGLING_OPEN_TAG_FOUND_ERROR ;
			goto cleanup ;
		}
		goto fetch ;
	} else {
		/*what we found was not an empty tag.*/

		/*is it a closing tag ?*/
		if (tag_name) {
			g_string_free (tag_name, TRUE) ;
			tag_name = NULL ;
		}
		status = mlview_utils_parse_closing_tag2 (&cur, &tag_name) ;
		if (status == MLVIEW_OK) {
			/*yes, it is a closing tag. let's push it on a stack*/
			gchar *tag_name_str = NULL ;
			tag_name_str = g_strdup (tag_name->str) ;
			closed_tags_stack = mlview_utils_push_on_stack
			                    (closed_tags_stack, tag_name_str) ;
			tag_name = NULL ;
		}

		if (!gtk_text_iter_backward_char (&cur)) {
			status = MLVIEW_ERROR ;
			goto cleanup ;
		}
		goto fetch ;
	}

cleanup:
	if (stag_end) {
		gtk_text_iter_free (stag_end) ;
		stag_end = NULL ;
	}
	if (tmp_iter) {
		gtk_text_iter_free (tmp_iter) ;
		tmp_iter = NULL ;
	}
	if (tag_name) {
		g_string_free (tag_name, TRUE) ;
		tag_name = NULL ;
	}
	return status ;

}

enum MlViewStatus
SourceView::connect_to_doc (MlViewXMLDocument *a_doc)
{
	g_signal_connect (G_OBJECT (a_doc),
	                  "document-changed",
	                  G_CALLBACK (SourceView::document_changed_cb),
	                  this) ;

	g_signal_connect (G_OBJECT (a_doc),
	                  "going-to-save",
	                  G_CALLBACK (SourceView::going_to_save_cb),
	                  this) ;
	return MLVIEW_OK ;
}

enum MlViewStatus
SourceView::disconnect_from_doc (MlViewXMLDocument *a_doc)
{
	g_signal_handlers_disconnect_by_func
	(G_OBJECT (a_doc),
	 (void*)SourceView::document_changed_cb,
	 this) ;

	g_signal_handlers_disconnect_by_func (G_OBJECT (a_doc),
	                                      (void*)SourceView::going_to_save_cb,
	                                      this) ;
	return MLVIEW_OK ;
}
/**
 * Gets the source buffer hold by the source view.
 * @param a_source_buffer, the result
 * @return MLVIEW_OK upon sucessful completion, an error code otherwise.
 */
GtkSourceBuffer *
SourceView::get_source_buffer ()
{
	GtkTextBuffer *text_buffer = NULL ;
	GtkSourceBuffer *source_buffer = NULL ;

	THROW_IF_FAIL (m_priv) ;

	THROW_IF_FAIL 	(m_priv->native_sv_ptr
	                && GTK_IS_SOURCE_VIEW (m_priv->native_sv_ptr.get ())) ;

	text_buffer =
	    gtk_text_view_get_buffer
	    (GTK_TEXT_VIEW (m_priv->native_sv_ptr.get ())) ;
	THROW_IF_FAIL (text_buffer) ;
	source_buffer = GTK_SOURCE_BUFFER (text_buffer) ;

	return source_buffer ;
}

/**
 * Serializes the document object model into an in memory buffer, and
 * load that in memory buffer into the source view.
 * @return MLVIEW_OK upon successful completion, an error code otherwise.
 */
enum MlViewStatus
SourceView::serialize_and_load_doc ()
{
	MlViewXMLDocument *doc = NULL ;
	GtkSourceBuffer *source_buffer = NULL ;
	gchar *doc_buffer = NULL ;
	gint nb_bytes_writen = 0 ;
	enum MlViewStatus status = MLVIEW_OK ;

	THROW_IF_FAIL (m_priv) ;

	doc = get_document () ;
	if (!doc) {
		return MLVIEW_ERROR ;
	}
	mlview_xml_document_save_xml_doc2 (doc, &doc_buffer,
	                                   &nb_bytes_writen) ;
	if (nb_bytes_writen <= 0 || ! doc_buffer) {
		g_warning ("(nb_bytes_writen <= 0 || !doc_buffer) failed") ;
		status = MLVIEW_OK ;
		goto cleanup ;
	}
	source_buffer = get_source_buffer () ;
	if (!source_buffer) {
		g_warning ("source_buffer failed") ;
		status = MLVIEW_ERROR ;
		goto cleanup ;
	}
	THROW_IF_FAIL (source_buffer) ;

	gtk_source_buffer_begin_not_undoable_action (source_buffer) ;
	gtk_text_buffer_set_text (GTK_TEXT_BUFFER (source_buffer),
	                          doc_buffer, nb_bytes_writen) ;
	gtk_source_buffer_end_not_undoable_action (source_buffer) ;

	m_priv->document_changed = FALSE ;

cleanup:
	if (doc_buffer) {
		g_free (doc_buffer) ;
		doc_buffer = NULL ;
	}
	return status ;
}

/**
 * Slam the changes that occured into the view into the document object model.
 * @return MLVIEW_OK upon successful completion, an error code otherwise.
 */
enum MlViewStatus
SourceView::save_text_buffer_into_xml_doc ()
{
	enum MlViewStatus status = MLVIEW_OK ;
	GtkTextIter start = {0} ;
	GtkTextIter end = {0} ;
	gchar *raw_buffer = NULL ;
	MlViewXMLDocument *doc = NULL ;
	GtkSourceBuffer *source_buffer = NULL ;

	THROW_IF_FAIL (m_priv) ;

	/* do not use the SourceView::get_document() function here, otherwise,
	 * it can loop forever because that function calls 
	 * serialize_and_load_doc ().
	 */
	doc = IView::get_document ();
	THROW_IF_FAIL (doc) ;
	source_buffer = get_source_buffer () ;
	THROW_IF_FAIL (source_buffer) ;
	gtk_text_buffer_get_iter_at_offset (GTK_TEXT_BUFFER (source_buffer),
	                                    &start, 0) ;
	gtk_text_buffer_get_iter_at_offset (GTK_TEXT_BUFFER (source_buffer),
	                                    &end, -1) ;
	raw_buffer = gtk_text_buffer_get_text (GTK_TEXT_BUFFER (source_buffer),
	                                       &start, &end, TRUE) ;
	THROW_IF_FAIL (raw_buffer) ;
	/*slam the changes that occured at view level into the document*/
	status = mlview_xml_document_reload_from_buffer (doc, raw_buffer,
	         TRUE) ;
	if (status == MLVIEW_OK) {
		/*flag the view as being in sync with the doc*/
		m_priv->view_changed = FALSE ;
	}

	/*out:*/
	if (raw_buffer) {
		g_free (raw_buffer) ;
		raw_buffer = NULL ;
	}

	return status ;
}

//******************
// signal callbacks
//******************

void
SourceView::on_is_swapped_in ()
{
	THROW_IF_FAIL (m_priv) ;
	if (m_priv->document_changed) {
		serialize_and_load_doc () ;
	}
}

void
SourceView::on_is_swapped_out ()
{
	THROW_IF_FAIL (m_priv) ;
	save_text_buffer_into_xml_doc () ;
}

void
SourceView::on_native_source_view_realize ()
{
	THROW_IF_FAIL (m_priv && m_priv->native_sv_wrapper_ptr) ;

	if (!m_priv->native_sv_wrapper_ptr->has_no_window ()) {
		AppContext *app_context = AppContext::get_instance () ;
		THROW_IF_FAIL (app_context) ;

		gtk_widget_add_events (GTK_WIDGET (m_priv->native_sv_ptr.get ()),
		                       GDK_BUTTON3_MOTION_MASK) ;
		g_signal_connect (G_OBJECT (m_priv->native_sv_ptr.get ()),
		                  "button-press-event",
		                  G_CALLBACK
		                  (&SourceView::native_sv_button_press_cb),
		                  app_context) ;
	} else {
		LOG_TO_ERROR_STREAM ("GtkSourceView widget has no window !!") ;
	}
}

bool
SourceView::on_native_sv_button_press_event (GdkEventButton *a_event)
{
	AppContext *app_context = NULL ;
	THROW_IF_FAIL (m_priv) ;

	THROW_IF_FAIL (a_event) ;
	app_context = AppContext::get_instance () ;
	THROW_IF_FAIL (app_context) ;

	switch (a_event->type) {
	case Gdk::BUTTON_PRESS:
		if (a_event->button == 3) {
			/*
			 * user pressed the right mouse button.
			 * Let's trigger the contextual menu mecanism.
			 */
			app_context->notify_contextual_menu_request
			(GTK_WIDGET (m_priv->native_sv_ptr.get ()),
			 (GdkEvent*)a_event) ;
			return true;

		}
	default:
		break ;
	}
	return false;
}

void
SourceView::on_contextual_menu_requested (GtkWidget *a_source_widget,
        GdkEvent *a_event)
{
	THROW_IF_FAIL (a_source_widget) ;

	handle_contextual_menu_request (a_source_widget, a_event) ;
}

//****************
//public methods
//****************
SourceView::SourceView (MlViewXMLDocument *a_doc,
                        const UString &a_name) :
		ViewAdapter::ViewAdapter (a_doc, a_name, "source-view")
{
	GtkSourceBuffer *source_buffer = NULL ;

	m_priv = new SourceViewPriv () ;

	THROW_IF_FAIL (a_doc && MLVIEW_IS_XML_DOCUMENT (a_doc)) ;

	/*
	 * Now build the native_sv, feed it with the serialized xml,
	 * connect to the appropriate signals.
	 */
	m_priv->native_sv_ptr = GTK_SOURCE_VIEW (gtk_source_view_new ()) ;
	THROW_IF_FAIL (m_priv->native_sv_ptr) ;

	set_document (a_doc) ;
	mlview_xml_document_ref (a_doc) ;

	set_default_options () ;
	serialize_and_load_doc () ;

	/*
	 * Load preferences and initialize widgets state
	 */
	m_priv->m_prefs = dynamic_cast<mlview::PrefsCategorySourceView*> (
	                      mlview::Preferences::get_instance ()
	                      ->get_category_by_id ("sourceview"));
	m_priv->load_preferences ();

	/*
	 * Connect to the other signals of MlViewIView
	 */
	signal_is_swapped_in ().connect
	(sigc::mem_fun (*this, &SourceView::on_is_swapped_in)) ;
	signal_is_swapped_out ().connect
	(sigc::mem_fun (*this, &SourceView::on_is_swapped_out)) ;

	source_buffer = get_source_buffer () ;

	if (source_buffer) {
		g_signal_connect (source_buffer,
		                  "changed",
		                  G_CALLBACK (SourceView::changed_cb),
		                  this) ;
		g_signal_connect (source_buffer,
		                  "can-undo",
		                  G_CALLBACK (undo_state_changed_cb),
		                  this) ;
		g_signal_connect (source_buffer,
		                  "can-redo",
		                  G_CALLBACK (undo_state_changed_cb),
		                  this) ;
	} else {
		mlview_utils_trace_debug ("Could not get source buffer") ;
	}

	/*
	 * connect to the AppContext signals
	 */
	AppContext *context = AppContext::get_instance () ;
	THROW_IF_FAIL (context) ;
	context->signal_contextual_menu_requested ().connect
	(sigc::mem_fun (*this, &SourceView::on_contextual_menu_requested)) ;

	Gtk::ScrolledWindow * scrolled_win =
	    manage (new Gtk::ScrolledWindow ()) ;
	scrolled_win->set_policy (Gtk::POLICY_AUTOMATIC,
	                          Gtk::POLICY_AUTOMATIC) ;

	Gtk::VBox *view_impl = dynamic_cast<Gtk::VBox*>(get_view_widget ()) ;
	THROW_IF_FAIL (view_impl) ;
	view_impl->pack_start (*scrolled_win, TRUE, TRUE, 0) ;

	m_priv->native_sv_wrapper_ptr  =
	    Glib::wrap (GTK_WIDGET (m_priv->native_sv_ptr.get ())) ;

	m_priv->native_sv_wrapper_ptr->signal_realize ().connect
	(sigc::mem_fun (*this,
	                &SourceView::on_native_source_view_realize)) ;

	scrolled_win->add
	(*m_priv->native_sv_wrapper_ptr) ;

	connect_to_doc (a_doc) ;

	get_view_widget ()->show_all () ;
}

SourceView::~SourceView ()
{
	if (!m_priv)
		return ;

	if (get_document ())  {
		disconnect_from_doc (get_document ()) ;
	}

	delete m_priv ;
	m_priv = NULL ;
}

GtkTextBuffer *
SourceView::get_text_buffer ()
{
	THROW_IF_FAIL (m_priv) ;

	return gtk_text_view_get_buffer
	       (GTK_TEXT_VIEW (m_priv->native_sv_ptr.get ())) ;
}


enum MlViewStatus
SourceView::set_default_options ()
{
	THROW_IF_FAIL (m_priv) ;

	m_priv->show_line_numbers = FALSE;
	gtk_source_view_set_show_line_numbers
	(m_priv->native_sv_ptr,
	 m_priv->show_line_numbers) ;

	m_priv->tabs_width = 4 ;
	gtk_source_view_set_tabs_width
	(m_priv->native_sv_ptr,
	 m_priv->tabs_width) ;

	m_priv->set_autoindent = FALSE ;
	gtk_source_view_set_auto_indent (m_priv->native_sv_ptr,
	                                 m_priv->set_autoindent) ;

	m_priv->set_show_margin = FALSE ;
	gtk_source_view_set_show_margin (m_priv->native_sv_ptr,
	                                 m_priv->set_show_margin) ;
	m_priv->margin = 2 ;
	gtk_source_view_set_margin (m_priv->native_sv_ptr,
	                            m_priv->margin) ;

	set_default_language () ;

	return MLVIEW_OK ;
}

GtkUIManager *
SourceView::get_ui_manager ()
{
	GtkActionGroup *action_group = NULL ;

	THROW_IF_FAIL (m_priv) ;
	AppContext *app_context = AppContext::get_instance ();
	THROW_IF_FAIL (app_context) ;

	if (!m_priv->ui_manager) {
		/*
		 * get the application wide ui manager from the
		 * application context.
		 *
		 */
		m_priv->ui_manager =
		    (GtkUIManager*)app_context->get_element ("MlViewUIManager") ;
		;
		THROW_IF_FAIL (m_priv->ui_manager) ;
		/*
		 * check if an action group of name "SourceViewEditMenuAction"
		 * has been inserted into the ui manager. If yes, use that one.
		 * If not, create a new one.
		 */
		mlview_utils_lookup_action_group (m_priv->ui_manager,
		                                  "SourceViewEditMenuActions",
		                                  &action_group) ;
		if (!action_group) {
			action_group = gtk_action_group_new
			               ("SourceViewEditMenuActions") ;
			gtk_action_group_set_translation_domain
			(action_group, GETTEXT_PACKAGE) ;

			gtk_action_group_add_actions
			(action_group,
			 SourceViewPriv::edit_menu_actions,
			 sizeof (SourceViewPriv::edit_menu_actions)
			 /
			 sizeof (GtkActionEntry),
			 this) ;

			m_priv->action_group = action_group ;
			gtk_ui_manager_insert_action_group
			(m_priv->ui_manager,
			 action_group, 0);
		}
		gchar* file_path = mlview_utils_locate_file
		                   ("source-view-edit-menu.xml") ;
		THROW_IF_FAIL (file_path) ;
		gtk_ui_manager_add_ui_from_file (m_priv->ui_manager,
		                                 file_path, 0) ;
		if (file_path) {
			g_free (file_path) ;
			file_path = NULL ;
		}
	}
	return m_priv->ui_manager ;
}

bool
SourceView::is_there_an_opened_tag ()
{
	enum MlViewStatus status = MLVIEW_OK ;
	GtkTextIter iter = {0} ;
	gchar *tag_name = NULL ;
	bool result = false ;

	THROW_IF_FAIL (m_priv) ;

	status = get_last_dangling_opened_tag (&iter, &tag_name) ;
	THROW_IF_FAIL (status == MLVIEW_OK) ;
	if (tag_name) {
		result = true ;
	} else {
		result = false ;
	}
	if (tag_name) {
		g_free (tag_name) ;
		tag_name = NULL ;
	}
	return result ;
}

/**
 * Closes the last opened dangling tag in the opened tag stack.
 * @return  true if a dangling tag has effectively been closed,
 * false otherwise.
 * Note that if the opened tag stack is empty (no dangling opened tag is 
 * present in the buffer) then the function won't close any tag, obviously.
 */
bool
SourceView::close_currently_opened_tag ()
{
	enum MlViewStatus status = MLVIEW_OK ;
	GtkTextIter iter = {0} , insert_iter = {0} ;
	GtkTextMark *text_mark = NULL ;
	GtkTextBuffer *text_buffer = NULL;
	gchar *tag_name = NULL, *closing_tag = NULL ;
	gint len = 0 ;

	status = get_last_dangling_opened_tag (&iter, &tag_name) ;
	if (status != MLVIEW_OK || !tag_name) {
		return false ;
	}

	/*
	 * get an iter at the current pointer location.
	 */
	text_buffer = GTK_TEXT_BUFFER (get_source_buffer ()) ;
	THROW_IF_FAIL ((status == MLVIEW_OK) && text_buffer)  ;
	text_mark = gtk_text_buffer_get_insert (text_buffer) ;
	THROW_IF_FAIL (text_mark) ;
	gtk_text_buffer_get_iter_at_mark (text_buffer, &insert_iter, text_mark) ;
	/*No need to delete special mark "insert"*/
	text_mark = NULL ;

	/*
	 * build the closing tag 
	 */
	closing_tag = g_strjoin ("", "</", tag_name, ">", NULL) ;
	len = strlen (closing_tag) ;
	gtk_text_buffer_insert (GTK_TEXT_BUFFER (text_buffer),
	                        &insert_iter, closing_tag, len) ;
	if (closing_tag) {
		g_free (closing_tag) ;
		closing_tag = NULL ;
	}
	return true ;//the tag has been closed
}

bool
SourceView::get_must_rebuild_upon_document_reload ()
{
	return false ;
}

/**
 * Overrides the mlview::IView::get_document() methods.
 * Actually, this methods tries to parse the content of
 * the source view buffer, and update the dom before giving it
 * back. Of course, this should be done only if 
 * the buffer is "dirty". Dirty means the content of the buffer
 * hasn't been parsed to update the dom since the user last modified
 * it.
 * @param a_doc the returned xml document
 */
MlViewXMLDocument*
SourceView::get_document ()
{
	enum MlViewStatus status = MLVIEW_OK ;

	THROW_IF_FAIL (m_priv) ;

	if (m_priv->view_changed == TRUE) {
		status = save_text_buffer_into_xml_doc () ;
	}
	return IView::get_document () ;
}

GtkWidget *
SourceView::get_contextual_menu ()
{
	GtkUIManager *ui_manager = NULL ;
	GtkWidget *tmp_widget = NULL, *menu = NULL ;

	THROW_IF_FAIL (m_priv) ;

	ui_manager = get_ui_manager () ;
	THROW_IF_FAIL (ui_manager) ;
	tmp_widget = gtk_ui_manager_get_widget
	             (ui_manager,
	              "/SourceViewPopupEditMenu/CloseTagMenuitem") ;
	if (!tmp_widget) {
		build_contextual_menu () ;
	}
	menu = gtk_ui_manager_get_widget
	       (ui_manager, "/SourceViewPopupEditMenu") ;
	THROW_IF_FAIL (menu) ;
	gtk_widget_show_all (menu) ;
	return menu ;
}

bool
SourceView::has_changed ()
{
	THROW_IF_FAIL (m_priv) ;
	return m_priv->view_changed ;
}

enum MlViewStatus
SourceView::cut_selected_text ()
{
	MlViewStatus status = MLVIEW_OK ;

	try {
		UString selected_text ;

		get_selected_text (selected_text) ;
		if (selected_text == "")
			return MLVIEW_OK ;

		//don't rely on the native GtkTextBuffer cut implementation
		//because we may change the destination clipboard.
		//So, copy the text and delete it from the GtkTextBuffer if it's
		//editable
		Clipboard clipboard = get_clipboard () ;
		clipboard.put (selected_text) ;
		GtkTextBuffer * text_buffer = NULL;
		text_buffer = get_text_buffer () ;
		THROW_IF_FAIL (text_buffer) ;
		gboolean has_been_deleted = gtk_text_buffer_delete_selection
		                            (text_buffer, FALSE, TRUE) ;
		THROW_IF_FAIL (has_been_deleted == TRUE) ;
	} catch (Exception &a_e) {
		TRACE_EXCEPTION (a_e) ;
		status = MLVIEW_ERROR ;
	}
	return status ;
}

enum MlViewStatus
SourceView::copy_selected_text ()
{
	MlViewStatus status = MLVIEW_OK ;

	try {
		UString selected_text ;

		get_selected_text (selected_text) ;
		if (selected_text == "")
			return MLVIEW_OK ;

		//don't rely on the native GtkTextBuffer cut implementation
		//because we may change the destination clipboard.
		Clipboard clipboard = get_clipboard () ;
		clipboard.put (selected_text) ;
	} catch (Exception &a_e) {
		TRACE_EXCEPTION (a_e) ;
		status = MLVIEW_ERROR ;
	}
	return status ;
}

enum MlViewStatus
SourceView::paste_text ()
{
	MlViewStatus status = MLVIEW_OK ;

	try {
		Clipboard clipboard = get_clipboard () ;
		UString text = clipboard.get () ;
		if (text == "")
			return MLVIEW_OK ;
		GtkTextBuffer *text_buffer= GTK_TEXT_BUFFER (get_source_buffer ()) ;
		THROW_IF_FAIL (text_buffer) ;
		gtk_text_buffer_insert_at_cursor (text_buffer, text.c_str(),
		                                  text.size ()) ;
	} catch (Exception &a_e) {
		TRACE_EXCEPTION (a_e) ;
		status = MLVIEW_ERROR ;
	} catch (...) {
		LOG_TO_ERROR_STREAM ("Got an unknown exception") ;
		status = MLVIEW_ERROR ;
	}
	return status ;
}

} //end namespace mlview

