/* -*- Mode: C++; indent-tabs-mode: true; c-basic-offset: 4-*- */

/*This file is part of GNU MlView
 *
 *GNU MlView is free software; 
 *you can redistribute it and/or modify it under the terms of 
 *the GNU General Public License as published by 
 *the Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope that it will be useful, 
 *but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General 
 *Public License along with MlView; 
 *see the file COPYING. If not, write to the 
 *Free Software Foundation, Inc., 59 
 *Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright informations.
 */

#ifndef __MLVIEW_APP_CONTEXT_H__
#define __MLVIEW_APP_CONTEXT_H__

#include "config.h"
#include <stdarg.h>
#include <libxml/parser.h>
#include <libxml/catalog.h>
#include <glib.h>
#include <glib-object.h>
#include <gtk/gtkfilechooser.h>
#include "mlview-utils.h"
#include "mlview-object.h"

namespace mlview
{
//*********************
/// \file
///#AppContext class declaration file.
//************************

class Clipboard ;

/* used to change gtk_file_chooser mode (ie open/save) */
enum FileChooserMode {
	 MLVIEW_FILE_CHOOSER_OPEN_MODE = 0,
	 MLVIEW_FILE_CHOOSER_SAVE_MODE
} ;

enum TreeEditorsNodeColour {
        MLVIEW_XML_ELEMENT_NODE_COLOUR = 0,
        MLVIEW_XML_ATTR_NAME_COLOUR,
        MLVIEW_XML_ATTR_VAL_COLOUR,
        MLVIEW_XML_TEXT_NODE_COLOUR,
        MLVIEW_XML_COMMENT_NODE_COLOUR,
        MLVIEW_XML_DOCUMENT_NODE_COLOUR,
        MLVIEW_XML_PI_NODE_COLOUR,
        MLVIEW_XML_DTD_NODE_COLOUR,
        MLVIEW_XML_ENTITY_DECL_NODE_COLOUR,
        NB_OF_TREE_NODE_COLOURS /*this must remain the last entry of the enum*/
} ;

//***********************
/// The structure which contains icons to
/// represent different types of XML nodes
/// used across different components of
/// the application.
//**************************
struct TypeIcons
{
	GdkPixbuf *element;
	GdkPixbuf *open_element;
	GdkPixbuf *text;
	GdkPixbuf *root;
	GdkPixbuf *open_root;
	GdkPixbuf *comment;
	GdkPixbuf *pi;
	GdkPixbuf *entity_ref;
};

struct AppContextPriv ;

//***************************
/// \brief the application wide context class.
//***************************
class AppContext: public Object
{
	friend struct AppContextPriv ;
	AppContextPriv *m_priv;

	//forbid copy and assignation
	AppContext (AppContext const &a_context) ;
	AppContext& operator= (AppContext const &a_context) ;

public:
	AppContext () ;
	virtual ~AppContext () ;

	gint *get_last_id_ptr ();

	gint get_last_id ();

	void set_last_id (gint a_new_id);

	void set_element (const gchar * a_element_name,
	                  gpointer a_context_element);

	gpointer get_element (const gchar *a_element_name);

	static AppContext *get_instance (void);

	/*display a message in the app bar*/
	void sbar_push_message (const gchar * a_msg_format, ...);

	void sbar_pop_message ();

	void sbar_set_default_message (const gchar * a_msg_format, ...);

	void message (const gchar * a_msg_format, ...);

	void error (const gchar *a_msg_forma, va_list a_params) ;

	void error (const gchar * a_msg_format, ...);

	void warning (const gchar * a_msg_format, ...);

	void bufferize_error (const gchar *a_msg_format, ...);

	void bufferize_error (const gchar *a_msg_format, va_list a_params);

	void display_buffered_error ();

	gboolean error_buffer_is_empty ();

	void set_error_dialog_title (const gchar * a_title);

	gchar *get_error_dialog_title ();

	void save_window_state (gint width, gint height);

	void save_treeview_state (gint treeview_height,
	                          gint completion_box_size);

	GtkFileChooser * get_file_chooser (const gchar *a_title, 
									   FileChooserMode a_mode) ;

	gint get_xpm (const gchar * a_xpm_name,
	              GdkPixmap ** a_pixmap,
	              GdkBitmap ** a_bitmap);

	void set_xml_catalog (xmlCatalog * a_xml_catalog) ;

	xmlCatalog *get_xml_catalog ();

	//***********************************************
	/// \brief get the instance of mlview::Clipboard used by the application
	///
	/// \param a_this the current instance of #MlViewApplication
	/// \return the instance of mlview::Clipboard used by the application
	//************************************************
	Clipboard* get_clipboard () ;

	enum MlViewStatus notify_contextual_menu_request (GtkWidget *a_source_widget,
													  GdkEvent *a_event) ;

	enum MlViewStatus notify_view_swapped (Object *a_old_view,
										   Object *a_new_view) ;

	enum MlViewStatus notify_application_initialized () ;

	enum MlViewStatus notify_document_name_changed (gpointer a_document) ;

	enum MlViewStatus  notify_view_undo_state_changed () ;

	static gboolean ask_internal_subset_node_name (gchar **a_name) ;

	struct TypeIcons * type_icons_ref () ;

	void type_icons_unref ();


	//***************
	//signals
	//***************
	 //Signal emited when the MlViewApp has been initialized.
	sigc::signal0<void>& signal_application_initialized ();

	/**
	 *signal emited when an editing
	 *widget has been requested to popup
	 *a contextual menu.
	 */
	sigc::signal2<void,
				  GtkWidget* /*request_source*/,
				  GdkEvent* /*event*/>& signal_contextual_menu_requested () ;

	sigc::signal2<void,
	              Object* /*a_old_view*/,
				  Object* /*a_new_view*/>& signal_view_swapped ();

	sigc::signal1<void, gpointer /*doc*/>& signal_document_name_changed () ;

	sigc::signal0<void>& signal_view_undo_state_changed () ;

protected:
	void unload_type_icons () ;

	struct TypeIcons * load_type_icons () ;

};

}//namespace mlview
#endif //__MLVIEW_APP_CONTEXT_H__
