/*
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute 
 *it and/or modify it under the terms of 
 *the GNU General Public License as published by the 
 *Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */

#ifndef __MLVIEW_COMPLETION_TABLE_H__
#define __MLVIEW_COMPLETION_TABLE_H__

#include <gtk/gtk.h>

#include "mlview-xml-document.h"

/**
 *@file
 *The declaration of the #MlViewCompletionTable class.
 */

G_BEGIN_DECLS

#define MLVIEW_TYPE_COMPLETION_TABLE (mlview_completion_table_get_type())
#define MLVIEW_COMPLETION_TABLE(object) (GTK_CHECK_CAST((object), MLVIEW_TYPE_COMPLETION_TABLE, MlViewCompletionTable))
#define MLVIEW_COMPLETION_TABLE_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass), MLVIEW_TYPE_COMPLETION_TABLE, MlViewCompletionTableClass))
#define MLVIEW_IS_COMPLETION_TABLE(object) (GTK_CHECK_TYPE((object), MLVIEW_TYPE_COMPLETION_TABLE))
#define MLVIEW_IS_COMPLETION_TABLE_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), MLVIEW_TYPE_COMPLETION_TABLE))

typedef struct _MlViewCompletionTablePrivate MlViewCompletionTablePrivate;
typedef struct _MlViewCompletionTable MlViewCompletionTable;
typedef struct _MlViewCompletionTableClass MlViewCompletionTableClass;

struct
			_MlViewCompletionTable
{
	GtkTable parent;
	MlViewCompletionTablePrivate *priv;
};

struct
			_MlViewCompletionTableClass
{
	GtkTableClass parent;
};

GType mlview_completion_table_get_type (void);

GtkWidget *mlview_completion_table_new (MlViewXMLDocument *a_xml_doc);
void mlview_completion_table_select_node (MlViewCompletionTable *a_widget, xmlNode *a_node_found);

G_END_DECLS

#endif
