/* -*- Mode: C++; indent-tabs-mode:nil; c-basic-offset: 4-*- */

/*
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute 
 *it and/or modify it under the terms of 
 *the GNU General Public License as published by the 
 *Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */

#include <glade/glade.h>
#include <libgnomeui/libgnomeui.h>
#include "mlview-view-adapter.h"

#define NAME_EDITING_ENTRY_FIELD_KEY "name-editing-entry-field"

/**
 *@file
 *The definition of the #MlViewViewAdapter class.
 */
namespace mlview
{
struct ViewAdapterPriv
{
	GtkWidget *set_name_dialog ;
	GtkDialog *name_editing_dialog ;

	ViewAdapterPriv ():
			set_name_dialog (NULL),
			name_editing_dialog (NULL)
	{}
}
; //ViewAdapterPriv

ViewAdapter::ViewAdapter (MlViewXMLDocument *a_doc,
                          const UString &a_name,
                          const UString &a_desc_type_name = "foo-view"):
		IView::IView (a_doc, a_name, a_desc_type_name)
{
	THROW_IF_FAIL (a_doc) ;

	m_priv = new ViewAdapterPriv () ;
	Gtk::Widget *view_impl = manage (new Gtk::VBox ()) ;
	set_view_widget (view_impl) ;
}

ViewAdapter::~ViewAdapter ()
{
	THROW_IF_FAIL (m_priv) ;

	if (!m_priv)
		return ;

	delete m_priv ;
	m_priv = NULL ;
}


IView *
ViewAdapter::create_instance (MlViewXMLDocument *a_doc,
                              const UString &a_name)
{

	THROW_EXCEPTION (MethodNotImplementedException,
	                 "ViewAdapter::create_instance() not implemented") ;
}

MlViewFileDescriptor*
ViewAdapter::get_file_descriptor ()
{
	THROW_IF_FAIL (m_priv) ;

	if (!get_document ()) {
		return NULL ;
	}
	return mlview_xml_document_get_file_descriptor (get_document ()) ;
}

void
ViewAdapter::set_name_interactive ()
{
	THROW_IF_FAIL (m_priv) ;

	if (m_priv->name_editing_dialog == NULL) {
		m_priv->name_editing_dialog =
		    GTK_DIALOG (build_name_editing_dialog ());
	}
	THROW_IF_FAIL (m_priv->name_editing_dialog) ;

	UString view_name = get_view_name () ;

	set_name_editing_widget_value
	(GTK_WIDGET (m_priv->name_editing_dialog), view_name);
	gint button = gtk_dialog_run (m_priv->name_editing_dialog);
	switch (button) {
	case GTK_RESPONSE_ACCEPT: /*user clicked ok */
		view_name = get_name_editing_widget_value
		            (GTK_WIDGET (m_priv->name_editing_dialog));

		if (view_name != "")
			set_view_name (view_name);
		break;
	default:
		break;
	}
	gtk_widget_hide
	(GTK_WIDGET (m_priv->name_editing_dialog));
}


enum MlViewStatus
ViewAdapter::connect_to_doc (MlViewXMLDocument *a_this)
{
	THROW_EXCEPTION (MethodNotImplementedException,
	                 "ViewAdapter::connect_to_doc() not implemented") ;
}

enum MlViewStatus
ViewAdapter::disconnect_from_doc (MlViewXMLDocument *a_this)
{
	THROW_EXCEPTION (MethodNotImplementedException,
	                 "ViewAdapter::disconnect_from_doc() not implemented") ;

}

enum MlViewStatus
ViewAdapter::update_contextual_menu ()
{
	THROW_EXCEPTION (MethodNotImplementedException,
	                 "ViewAdapter::update_contextual_menu() not implemented") ;

}

enum MlViewStatus
ViewAdapter::execute_action (MlViewAction &an_action)
{
	THROW_EXCEPTION (MethodNotImplementedException,
	                 "ViewAdapter::excecute_action() not implemented") ;
}

bool
ViewAdapter::get_must_rebuild_upon_document_reload ()
{
	THROW_EXCEPTION (MethodNotImplementedException,
	                 "ViewAdapter::get_must_rebuild_upon_document() "
	                 "not implemented") ;
}

enum MlViewStatus
ViewAdapter::undo ()
{
	THROW_EXCEPTION (MethodNotImplementedException,
	                 "ViewAdapter::undo() not implemented") ;
}

enum MlViewStatus
ViewAdapter::redo ()
{
	THROW_EXCEPTION (MethodNotImplementedException,
	                 "ViewAdapter::redo() not implemented") ;
}

bool
ViewAdapter::can_undo ()
{
	THROW_EXCEPTION (MethodNotImplementedException,
	                 "ViewAdapter::can_redo() not implemented") ;
}

bool
ViewAdapter::can_redo ()
{
	THROW_EXCEPTION (MethodNotImplementedException,
	                 "ViewAdapter::can_redo() not implemented") ;
}

//******************
// private methods
//******************

GtkWidget *
ViewAdapter::build_name_editing_dialog ()
{
	GtkWidget *result = NULL;
	GtkWidget *name_entry_field = NULL;

	result = gtk_dialog_new_with_buttons
	         (_("Type the name of the current view"), NULL,
	          GTK_DIALOG_MODAL,
	          GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
	          GTK_STOCK_OK, GTK_RESPONSE_ACCEPT,
	          NULL);

	name_entry_field = gtk_entry_new ();

	gtk_object_set_data (GTK_OBJECT (result),
	                     NAME_EDITING_ENTRY_FIELD_KEY,
	                     name_entry_field);
	gtk_box_pack_start_defaults (GTK_BOX
	                             (GTK_DIALOG (result)->vbox),
	                             name_entry_field) ;

	gtk_entry_set_activates_default (GTK_ENTRY (name_entry_field),
	                                 TRUE) ;

	gtk_dialog_set_default_response (GTK_DIALOG (result),
	                                 GTK_RESPONSE_ACCEPT) ;
	gtk_widget_show_all (result);

	return result;
}

void
ViewAdapter::set_name_editing_widget_value (GtkWidget *a_edition_widget,
        UString &a_value)
{
	GtkEntry *name_entry = NULL;

	THROW_IF_FAIL (a_edition_widget != NULL);
	THROW_IF_FAIL (GTK_IS_DIALOG (a_edition_widget));

	name_entry = (GtkEntry*)
	             gtk_object_get_data (GTK_OBJECT
	                                  (a_edition_widget),
	                                  NAME_EDITING_ENTRY_FIELD_KEY);

	THROW_IF_FAIL (name_entry != NULL);
	THROW_IF_FAIL (GTK_IS_ENTRY (name_entry));

	gtk_entry_set_text (name_entry, a_value.c_str ());

	gtk_dialog_set_default_response (GTK_DIALOG (a_edition_widget),
	                                 GTK_RESPONSE_ACCEPT) ;
}

UString
ViewAdapter::get_name_editing_widget_value (GtkWidget * a_edition_widget)
{
	GtkEntry *name_entry = NULL;

	THROW_IF_FAIL (a_edition_widget != NULL) ;
	THROW_IF_FAIL (GTK_IS_DIALOG (a_edition_widget)) ;

	name_entry = (GtkEntry*)
	             gtk_object_get_data (GTK_OBJECT
	                                  (a_edition_widget),
	                                  NAME_EDITING_ENTRY_FIELD_KEY);

	THROW_IF_FAIL (name_entry != NULL);
	THROW_IF_FAIL (GTK_IS_ENTRY (name_entry));

	return  gtk_entry_get_text (name_entry);
}
}//namespace mlview

