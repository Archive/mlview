/* -*- Mode: C++; indent-tabs-mode:true; c-basic-offset: 4-*- */

/*This file is part of GNU MlView
 *
 *GNU MlView is free software; 
 *you can redistribute it and/or modify it under the terms of 
 *the GNU General Public License as 
 *published by the Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope 
 *that it will be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */
#ifndef __MLVIEW_SAFE_PTR_H__
#define __MLVIEW_SAFE_PTR_H__
namespace mlview
{

struct DefaultReferenceFunctor
{
	void
	operator () (const void* a_ptr)
	{}
}
; //end struct DefaultReference

struct FreeFunctor
{
	void
	operator () (const void* a_ptr)
	{
		if (a_ptr)
			free (const_cast<void *> (a_ptr)) ;
	}
}
;//end struct DefaultUnreference

template <typename PointerType>
struct DeleteFunctor
{
	void
	operator () (const PointerType* a_ptr)
	{
		if (a_ptr)
			delete (a_ptr) ;
	}
};

template<typename PointerType,
typename ReferenceFunctor = DefaultReferenceFunctor,
typename UnreferenceFunctor = DeleteFunctor<PointerType> >

class SafePtr
{
protected:
	PointerType* m_pointer ;
	ReferenceFunctor m_reference ;
	UnreferenceFunctor m_unreference ;

public:
	SafePtr (PointerType *a_pointer) :m_pointer (a_pointer)
	{
		if (m_pointer) {
			m_reference (static_cast<PointerType*>(m_pointer)) ;
		}
	}

	SafePtr () :m_pointer (NULL)
	{
		if (m_pointer) {
			m_reference (static_cast<PointerType*>(m_pointer)) ;
		}
	}

	SafePtr (const SafePtr<PointerType,
	         ReferenceFunctor,
	         UnreferenceFunctor> &a_safe_ptr) :
			m_pointer (a_safe_ptr.m_pointer)
	{
		if (m_pointer)
			m_reference ((PointerType*)m_pointer) ;
	}

	~SafePtr ()
	{
		if (m_pointer) {
			m_unreference ((PointerType*)m_pointer);
			m_pointer = NULL ;
		}
	}

	SafePtr<PointerType, ReferenceFunctor, UnreferenceFunctor>
	operator= (const SafePtr<PointerType,
	           ReferenceFunctor,
	           UnreferenceFunctor> &a_safe_ptr)
	{
		SafePtr<PointerType,
		ReferenceFunctor,
		UnreferenceFunctor> temp (a_safe_ptr) ;
		swap (temp) ;
		return *this ;
	}

	SafePtr<PointerType, ReferenceFunctor, UnreferenceFunctor>&
	operator = (PointerType *a_pointer)
	{
		reset (a_pointer) ;
		return *this ;
	}

	PointerType&
	operator* () const
	{
		return  *((PointerType*)m_pointer) ;
	}

	PointerType*
	operator-> () const
	{
		return (PointerType*)m_pointer ;
	}

	operator PointerType* ()
	{
		return (PointerType*)m_pointer ;
	}

	bool operator== (PointerType *a_pointer)
	{
		return m_pointer == a_pointer ;
	}

	bool operator== (SafePtr<PointerType,
							 ReferenceFunctor,
							 UnreferenceFunctor> const &a_safe_ptr)
	{
		return m_pointer == a_safe_ptr.m_pointer ;
	}

	bool operator! (void)
	{
		if (!m_pointer)
			return true ;
		else
			return false ;
	}

	PointerType*
	get () const
	{
			return (PointerType*)m_pointer ;
	}

	PointerType*
	release () 
	{
		PointerType* pointer = (PointerType*)m_pointer;
		m_pointer = NULL ;
		return (PointerType*) pointer ;
	}

	void
	reset (PointerType *a_pointer = NULL)
	{
		if (a_pointer != m_pointer) {
			if (m_pointer)
				m_unreference ((PointerType*)m_pointer) ;
			m_pointer = a_pointer ;
			if (m_pointer)
				m_reference ((PointerType*)m_pointer) ;
		}
	}

	void
	swap (SafePtr<PointerType,
	      ReferenceFunctor,
	      UnreferenceFunctor> &a_safe_ptr)
	{
		PointerType *const tmp(m_pointer) ;
		m_pointer = a_safe_ptr.m_pointer ;
		a_safe_ptr.m_pointer = tmp ;
	}
}
;//end class SafePtr

}//end namespace mlview
#endif //__MLVIEW_SAFR_PTR_H__

