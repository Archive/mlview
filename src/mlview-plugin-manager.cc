/* -*- Mode: C++; indent-tabs-mode: true; c-basic-offset: 4-*- */

/**
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute it 
 *and/or modify it under the terms of 
 *the GNU General Public License as published 
 *by the Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *MlView is distributed in the hope that it will be useful, 
 *but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. If not, write to the Free Software Foundation, Inc., 
 *59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */

#include <map>

#include <string.h>

#include <sys/types.h>
#include <dirent.h>
#include <sys/stat.h>
#include <unistd.h>

#include <glib/gstdio.h>

#include "mlview-plugin-manager.h"
#include "mlview-utils.h"
#include "mlview-marshal.h"

#define MLVIEW_PLUGIN_DESCRIPTOR_EXTENSION ".xml"

using namespace std ;

/**
 *@file
 *The definition of the methods of #PluginManager class.
 */

BEGIN_NAMESPACE_MLVIEW

typedef map<UString, Plugin*> StringToPluginMap ;
typedef list<UString> ListOfPaths ;

struct PluginManagerPriv {
	StringToPluginMap plugins_map ;

	PluginManagerPriv () {} ;
} ;

static bool
file_name_is_xml (const gchar *a_file_name)
{
	guint path_size = 0;
	const gchar *extension = NULL;
	
	THROW_IF_FAIL (a_file_name);

	path_size = strlen (a_file_name);
	
	extension = a_file_name + path_size - 4;
	
	if (!strcmp (extension, MLVIEW_PLUGIN_DESCRIPTOR_EXTENSION))
		return true ;
	
	return false ;
}

//******************************************
// protected methods
//******************************************

void
PluginManager::load_all_plugins_from_default_plugins_dir ()
{
	UString plugin_path ;
	ListOfPluginDescriptors::iterator it ;
	ListOfPluginDescriptors descriptors ;
	
	gchar *plugin_path_str = NULL ;
	
	plugin_path_str = g_build_filename (LIBDIR, "mlview", "plugins", NULL) ;
	
	plugin_path = plugin_path_str ;
	
	g_free (plugin_path_str) ;
	plugin_path_str = NULL ;
	
	get_available_plugins (plugin_path, descriptors) ;
	it = descriptors.begin () ;
	
	while (it != descriptors.end ()) {
		try {
			load_plugin (*it) ;
		}catch (exception &e) {
			TRACE_EXCEPTION (e) ;
		}catch (...) {
			LOG_TO_ERROR_STREAM ("caught unknown exception") ;
		}

		it++ ;
	}
}

//******************************************
// public methods
//******************************************

PluginManager::PluginManager ()
{
	m_priv = new PluginManagerPriv () ;
	THROW_IF_FAIL (m_priv) ;
}

PluginManager::~PluginManager ()
{
	if (m_priv) {
		delete m_priv ;
		m_priv = NULL ;
	} else {
		THROW ("double delete") ;
	}
}

const map<UString, Plugin*>&
PluginManager::get_map_of_plugins () const
{
	return m_priv->plugins_map ;
}

const Plugin *
PluginManager::load_plugin (const PluginDescriptor &a_descr)
{
	Plugin *plugin = NULL ;
	
	THROW_IF_FAIL (m_priv) ;
	
	plugin = new Plugin (a_descr) ;
	
	THROW_IF_FAIL (plugin) ;
	
	m_priv->plugins_map.insert (StringToPluginMap::value_type
							(a_descr.get_plugin_name (), plugin)) ;
	
	signal_plugin_loaded.emit (plugin) ;
	
	return plugin ;
}

const Plugin *
PluginManager::load_plugin (const UString &a_url)
{
	Plugin *plugin = NULL ;
	
	THROW_IF_FAIL (m_priv) ;
	
	plugin = new Plugin (a_url) ;
	
	THROW_IF_FAIL (plugin) ;
	
	m_priv->plugins_map.insert (StringToPluginMap::value_type
			(plugin->get_descriptor ().get_plugin_name (), plugin)) ;
	
	signal_plugin_loaded.emit (plugin) ;
	
	return plugin ;
}


void
PluginManager::get_available_plugins (ListOfPaths &a_dir_names,
				      ListOfPluginDescriptors &a_list)
{
	ListOfPaths::iterator it ;
	it = a_dir_names.begin () ;
	
	while (it != a_dir_names.end ()) {
		try {
			ListOfPluginDescriptors sublist ;

			get_available_plugins (*it, sublist) ;
			
			a_list.splice (a_list.end (),
				       sublist) ;
		} catch (exception &e){
			TRACE_EXCEPTION (e) ;
		} catch (...) {
			LOG_TO_ERROR_STREAM ("caught unknow exception") ;
		}
	}
}

void
PluginManager::get_available_plugins (const UString &a_dir_name,
									  ListOfPluginDescriptors &a_list)
{
	GDirSafePtr dir ;
	GErrorSafePtr err_ptr ;
	StatSafePtr stat_buf ;
	GMemSafePtr path ;
	
	GError *err = NULL ;
	const gchar *name = NULL ;
	int ret = -1 ;
	
	stat_buf = new struct stat ;
	
	dir = g_dir_open (a_dir_name.c_str (), 0, &err) ;
	
	err_ptr = err ;
	err = NULL ;
	
	if (err_ptr) {
		THROW ((UString ("g_dir_open () returned error : ") +
			a_dir_name + UString (", ") + UString (err_ptr->message)).c_str ()) ;
	}
	name = g_dir_read_name (dir) ;
	
	while (name) {
		if (file_name_is_xml (name)) {
			path = g_build_filename (a_dir_name.c_str (), name, NULL) ;
			
			THROW_IF_FAIL (path) ;
			
			ret = g_stat (path, stat_buf) ;
			
			if (ret == 0 && S_ISREG (stat_buf->st_mode))
				a_list.push_front (PluginDescriptor (UString (path))) ;
		}
		
		name = g_dir_read_name (dir) ;
	}
}

END_NAMESPACE_MLVIEW
