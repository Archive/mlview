/* -*- Mode: C++; indent-tabs-mode:true ; c-basic-offset: 4-*- */

/*
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute 
 *it and/or modify it under the terms of 
 *the GNU General Public License as published by the 
 *Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */

#include "mlview-prefs.h"
#include "mlview-prefs-category-search.h"
#include "mlview-prefs-category-frame-search.h"

namespace mlview
{
struct PrefsCategoryFrameSearchPriv
{
	PrefsCategorySearch *m_prefs;
	Gtk::CheckButton *m_node_names_check_button;
	Gtk::CheckButton *m_node_values_check_button;
	Gtk::CheckButton *m_attribute_names_check_button;
	Gtk::CheckButton *m_attribute_values_check_button;
	Gtk::Button *m_reset_to_default_button;

	void setup_ui (Glib::RefPtr<Gnome::Glade::Xml> i_glade_xml);
	void setup_event_handlers ();

	// Callbacks
	void search_node_names_cb_toggled_callback ();
	void search_node_values_cb_toggled_callback ();
	void search_attr_names_cb_toggled_callback ();
	void search_attr_values_cb_toggled_callback ();
	void reset_to_default ();

	PrefsCategoryFrameSearchPriv  () :
		m_node_names_check_button (NULL),
		m_node_values_check_button (NULL),
		m_attribute_names_check_button (NULL),
		m_attribute_values_check_button (NULL),
		m_reset_to_default_button (NULL)
	{}

};

PrefsCategoryFrameSearch::PrefsCategoryFrameSearch ()
    : PrefsCategoryFrame ("prefs_category_box_search")
{
    Glib::RefPtr<Gnome::Glade::Xml> l_refxml = this->get_gladexml_ref ();

    m_priv = new PrefsCategoryFrameSearchPriv ();
    m_priv->m_prefs = dynamic_cast<PrefsCategorySearch*>
	(Preferences::get_instance ()->get_category_by_id ("search"));
	THROW_IF_FAIL (m_priv->m_prefs) ;

    m_priv->setup_ui (l_refxml);
    m_priv->setup_event_handlers ();
}

PrefsCategoryFrameSearch::~PrefsCategoryFrameSearch ()
{
    if (m_priv != NULL) {
		delete m_priv;
		m_priv = NULL ;
	}
}

void
PrefsCategoryFrameSearchPriv::setup_ui (
    Glib::RefPtr<Gnome::Glade::Xml> a_glade_xml_ref)
{
	THROW_IF_FAIL (a_glade_xml_ref) ;

    a_glade_xml_ref->get_widget ("nodenames_checkbutton",
				 m_node_names_check_button);
	THROW_IF_FAIL (m_node_names_check_button) ;

    a_glade_xml_ref->get_widget ("nodevalues_checkbutton",
				 m_node_values_check_button);
	THROW_IF_FAIL (m_node_values_check_button) ;

    a_glade_xml_ref->get_widget ("attributenames_checkbutton",
				 m_attribute_names_check_button);
	THROW_IF_FAIL (m_attribute_names_check_button) ;

    a_glade_xml_ref->get_widget ("attributevalues_checkbutton",
				 m_attribute_values_check_button);
	THROW_IF_FAIL (m_attribute_values_check_button) ;

    a_glade_xml_ref->get_widget ("prefs_category_search_reset_button",
				 m_reset_to_default_button);
	THROW_IF_FAIL (m_reset_to_default_button) ;

    m_node_names_check_button->set_active
	(m_prefs->search_among_node_names ());

    m_node_values_check_button->set_active
	(m_prefs->search_among_node_values ());

    m_attribute_names_check_button->set_active
	(m_prefs->search_among_attr_names ());

    m_attribute_values_check_button->set_active
	(m_prefs->search_among_attr_values ());
}

void
PrefsCategoryFrameSearchPriv::setup_event_handlers ()
{
    m_node_names_check_button->signal_toggled ().connect
	(sigc::mem_fun (*this,
			&PrefsCategoryFrameSearchPriv::
			search_node_names_cb_toggled_callback));

    m_node_values_check_button->signal_toggled ().connect
	(sigc::mem_fun (*this,
			&PrefsCategoryFrameSearchPriv::
			search_node_values_cb_toggled_callback));

    m_attribute_names_check_button->signal_toggled ().connect
	(sigc::mem_fun (*this,
			&PrefsCategoryFrameSearchPriv::
			search_attr_names_cb_toggled_callback));

    m_attribute_values_check_button->signal_toggled ().connect
	(sigc::mem_fun (*this,
			&PrefsCategoryFrameSearchPriv::
			search_attr_values_cb_toggled_callback));

    m_reset_to_default_button->signal_clicked ().connect
	(sigc::mem_fun (*this,
			&PrefsCategoryFrameSearchPriv::
			reset_to_default));
}

void
PrefsCategoryFrameSearchPriv::
search_node_names_cb_toggled_callback ()
{
    m_prefs->set_search_among_node_names
	(m_node_names_check_button->get_active ());
}

void
PrefsCategoryFrameSearchPriv::
search_node_values_cb_toggled_callback ()
{
    m_prefs->set_search_among_node_values
	(m_node_values_check_button->get_active ());
}

void
PrefsCategoryFrameSearchPriv::
search_attr_names_cb_toggled_callback ()
{
    m_prefs->set_search_among_attr_names
	(m_attribute_names_check_button->get_active ());
}

void
PrefsCategoryFrameSearchPriv::
search_attr_values_cb_toggled_callback ()
{
    m_prefs->set_search_among_attr_values
	(m_attribute_values_check_button->get_active ());
}

void
PrefsCategoryFrameSearchPriv::
reset_to_default ()
{
    m_node_names_check_button->set_active
	(m_prefs->search_among_node_names_default ());
    m_node_values_check_button->set_active
	(m_prefs->search_among_node_values_default ());
    m_attribute_names_check_button->set_active
	(m_prefs->search_among_attr_names_default ());
    m_attribute_values_check_button->set_active
	(m_prefs->search_among_attr_values_default ());
}

} // namespace mlview
