/* -*- Mode: C++; indent-tabs-mode: true; c-basic-offset: 4-*- */

/*
 *This file is part of MlView
 *
 *MlView is free software; you can redistribute it and/or 
 *modify it under the terms of 
 *the GNU General Public License as published 
 *by the Free Software Foundation; either version 2
 *or (at your option) any later version.
 *
 *MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView.
 *see the file COPYING. 
 *If not, write to the 
 *Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */

#ifndef __MLVIEW_OBJECT_H__
#define __MLVIEW_OBJECT_H__

#include "mlview-exception.h"

namespace mlview {

/**
 * The root of all the objects in mlview. 
 */
struct ObjectPriv ;
class Object {
	friend struct ObjectPriv  ;
	ObjectPriv * m_priv ;

	public:

	class MethodNotImplementedException : public Exception {
		public:
		MethodNotImplementedException (const gchar *a_reason) ;
		MethodNotImplementedException 
			(const MethodNotImplementedException &an_e) ;
		virtual ~MethodNotImplementedException () throw () ;
	};

	Object () ;
	Object (Object const &an_object) ;
	virtual ~Object () ;
	Object& operator= (Object const&) ;

	virtual void ref () const ;
	virtual void unref () const ;
};
}//namespace mlview
#endif //__MLVIEW_OBJECT_H__

