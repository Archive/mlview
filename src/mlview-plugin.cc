/* -*- Mode: C++; indent-tabs-mode: true; c-basic-offset: 8-*- */

/**
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute it 
 *and/or modify it under the terms of 
 *the GNU General Public License as published 
 *by the Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *MlView is distributed in the hope that it will be useful, 
 *but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. If not, write to the Free Software Foundation, Inc., 
 *59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */

#include <gmodule.h>

#include "mlview-plugin.h"
#include "mlview-utils.h"
#include "mlview-marshal.h"

using namespace std ;


/**
 *@file
 *The definition of the methods of #Plugin class.
 */

BEGIN_NAMESPACE_MLVIEW

struct PluginPriv {
	Plugin *parent ;
	const PluginDescriptor *descriptor ;
	GModule *module ;
	PluginPriv (Plugin *a_parent) {
		parent = a_parent ;
	}
	
	void load () ;
	void unload () ;
} ;

//************************************************
// static functions
// **********************************************

void
PluginPriv::load ()
{
	PluginSymbol load_sym = NULL ;
	gboolean res = FALSE ;
	
	THROW_IF_FAIL (g_module_supported ()) ;
	THROW_IF_FAIL (parent) ;
	THROW_IF_FAIL (descriptor) ;
	
	module = g_module_open
	(descriptor->get_plugin_file_path ().c_str (),
	 static_cast<GModuleFlags> (G_MODULE_BIND_LAZY | G_MODULE_BIND_LOCAL)) ;
	
	THROW_IF_FAIL (module) ;
	
	res = g_module_symbol (module,
						   descriptor->get_load_hook_function_name (),
						   (gpointer*) &load_sym) ;
	
	THROW_IF_FAIL (res) ;
	THROW_IF_FAIL (load_sym) ;
	res = load_sym (parent) ;
	THROW_IF_FAIL (res) ;
}

void
PluginPriv::unload ()
{
	gboolean res = FALSE ;
	PluginSymbol unload_sym = NULL ;
	
	THROW_IF_FAIL (parent) ;
	
	if (module) {
		res = g_module_symbol (module,
							   descriptor->get_unload_hook_function_name (),
							   (gpointer*) &unload_sym) ;
		
		THROW_IF_FAIL (res) ;
		THROW_IF_FAIL (unload_sym) ;
		
		res = unload_sym (parent) ;
		THROW_IF_FAIL (res) ;
		
		res = g_module_close (module) ;
		
		THROW_IF_FAIL (res) ;
		
		module = NULL ;
	}
}

//******************************************
// public methods
//******************************************

Plugin::Plugin (const PluginDescriptor &a_descr)
{
	m_priv = new PluginPriv (this) ;
	
	THROW_IF_FAIL (m_priv) ;
	
	m_priv->descriptor = new PluginDescriptor (a_descr) ;
	
	m_priv->load () ;
}

Plugin::Plugin (const UString &a_url)
{
	m_priv = new PluginPriv (this) ;
	
	THROW_IF_FAIL (m_priv) ;
	
	m_priv->descriptor = new PluginDescriptor (a_url) ;
	
	THROW_IF_FAIL (m_priv->descriptor) ;
	
	m_priv->load () ;
}

Plugin::~Plugin ()
{
	THROW_IF_FAIL (m_priv) ;
	
	signal_unloaded.emit () ;
	
	if (m_priv->module)
		m_priv->unload () ;
	
	delete m_priv ;
	
	m_priv = NULL ;
}

const PluginDescriptor&
Plugin::get_descriptor () const
{
	return *m_priv->descriptor ;
}

END_NAMESPACE_MLVIEW
