/* -*- Mode: C++; indent-tabs-mode:true; c-basic-offset: 4-*- */
/*
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute 
 *it and/or modify it under the terms of 
 *the GNU General Public License as published by the 
 *Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYING file for copyright information.
 */
#ifndef MLVIEW_PREFS_STORAGE_MANAGER_H
#define MLVIEW_PREFS_STORAGE_MANAGER_H

#include "mlview-object.h"
namespace mlview
{

///
/// This interface defines methods to access preferences transparently
/// from a persistant store.\n
/// Preferences depends on this interface to store/retrieve preferences
///
class PrefsStorageManager : public Object
{

	//forbid copy/assignation
	PrefsStorageManager (PrefsStorageManager const&) ;
	PrefsStorageManager& operator= (PrefsStorageManager const&) ;

public:
///
/// Store an int value
/// \param key the key for which to store the value
/// \param value the value to store
///
	PrefsStorageManager () {}
	virtual ~PrefsStorageManager () {} ;
    virtual void set_int_value (const UString& key,
				int value) = 0 ;
///
/// Retrieve an int value
/// \param key the key from which to retrieve the value
/// \return the value
///
    virtual int get_int_value (const UString& key) = 0 ;

///
/// Retrieve the default value as an int
/// \param key the key from which to retrieve the default value
/// \return the value
///
    virtual int get_default_int_value (const UString& key) = 0 ;


///
/// Store an int value
/// \param key the key for which to store the value
/// \param value the value to store
///
    virtual void set_string_value (const UString& key,
				   const UString& value) = 0 ;

///
/// Retrieve a string value
/// \param key the key from which to retrieve the value
/// \return the value stored in the preferences
///
    virtual UString
    get_string_value (const UString& key) = 0 ;

///
/// Retrieve the default value as a string
/// \param key the key from which to retrieve the default value
/// \return the value stored in the preferences
///
    virtual UString
    get_default_string_value (const UString& key) = 0 ;

///
/// Store an bool value
/// \param key the key for which to store the value
/// \param value the value to store
///
    virtual void set_bool_value (const UString& key,
				 bool value) = 0;

///
/// Retrieve a bool value
/// \param key the key from which to retrieve the value
/// \return the value
///
    virtual bool get_bool_value (const UString& key) = 0;

///
/// Retrieve the default value as a boolean
/// \param key the key from which to retrieve the default value
/// \return the value
///
    virtual bool get_default_bool_value (const UString& key) = 0;

};

} // namespace mlview
#endif
