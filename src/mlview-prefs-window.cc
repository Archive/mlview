/*
  mlview-prefs-window.cc: This file is part of the MlView source tree
  Philippe Mechaï (2005)
 
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include <iostream>

#include <gtkmm.h>
#include <libglademm.h>
#include "mlview-prefs-window.h"
#include "mlview-prefs-category-frame.h"
#include "mlview-prefs-category-frame-general.h"
#include "mlview-prefs-category-frame-search.h"
#include "mlview-prefs-category-frame-sourceview.h"
#include "mlview-prefs-category-frame-treeview.h"

namespace mlview
{

class ModelColumns : public Gtk::TreeModel::ColumnRecord
{
public:
	ModelColumns()
	{
		add
			(m_col_id);
		add
			(m_col_name);
		add
			(m_col_frame);
	}

	Gtk::TreeModelColumn<Glib::ustring> m_col_id;
	Gtk::TreeModelColumn<Glib::ustring> m_col_name;
	Gtk::TreeModelColumn<PrefsCategoryFrame*> m_col_frame;
};

struct PrefsWindowPriv
{
	Glib::RefPtr
	<Gnome::Glade::Xml> m_refxml;
	Gtk::Window     *m_window;
	Gtk::Viewport   *m_viewport;
	Gtk::Button     *m_close_button;
	Gtk::TreeView   *m_treeview;
	ModelColumns     m_tree_columns;
	Glib::RefPtr
	<Gtk::TreeStore> m_ref_treemodel;
	Glib::RefPtr
	<Gtk::TreeSelection> m_ref_treeselection;

	void setup_ui ();
	void setup_treeview ();
	void setup_event_handlers ();

	// Callbacks
	void on_category_selection ();
	void on_close_button_click ();
};


//////////////////////////////////////
// MlViewPrefsWindow Implementation //
//////////////////////////////////////

PrefsWindow::PrefsWindow ()
{
	m_priv = new PrefsWindowPriv ();
	m_priv->setup_ui ();
	m_priv->setup_event_handlers ();
}

PrefsWindow::~PrefsWindow ()
{
	if (m_priv) {
		delete (m_priv);
		m_priv = NULL ;
	}
}

void
PrefsWindow::show ()
{
	m_priv->m_window->show_all ();
}

void
PrefsWindow::hide ()
{
	m_priv->m_window->hide ();
}


/////////////////////
// Private methods //
/////////////////////
void
PrefsWindowPriv::on_category_selection ()
{
	const Gtk::TreeModel::iterator selected =
	    m_treeview->get_selection()->get_selected();
	Glib::ustring l_category_id =
	    (*selected)[m_tree_columns.m_col_id];
	PrefsCategoryFrame *l_category_frame =
	    (*selected)[m_tree_columns.m_col_frame];

	m_viewport->remove
	();

	if (l_category_frame == NULL) {
		std::cerr << "Category '"
		<< l_category_id
		<< "' hasn't been registered."
		<< std::endl;
	} else {
		m_viewport->add
		(l_category_frame->widget_ref ());
	}
}

void
PrefsWindowPriv::on_close_button_click ()
{
	m_window->hide ();
}

void
PrefsWindowPriv::setup_ui ()
{
	gchar *glade_file =
	    gnome_program_locate_file (NULL,
	                               GNOME_FILE_DOMAIN_APP_DATADIR,
	                               PACKAGE "/mlview-prefs-window.glade",
	                               TRUE,
	                               NULL) ;
	m_refxml = Gnome::Glade::Xml::create (glade_file,
	                                      "prefs_window");
	m_refxml->get_widget ("prefs_window",
	                      m_window);
	m_refxml->get_widget ("pages_treeview",
	                      m_treeview);
	m_refxml->get_widget ("viewport",
	                      m_viewport);
	m_refxml->get_widget ("close_button",
	                      m_close_button);

	m_ref_treemodel = Gtk::TreeStore::create (m_tree_columns);
	m_treeview->set_model (m_ref_treemodel);
	m_treeview->append_column ("name",
	                           m_tree_columns.m_col_name);
	setup_treeview ();
	m_treeview->expand_all ();

	m_ref_treeselection = m_treeview->get_selection();
	m_ref_treeselection->set_mode (Gtk::SELECTION_SINGLE);


}

void
PrefsWindowPriv::setup_treeview ()
{
	Gtk::TreeModel::Row row;
	Gtk::TreeModel::Row childrow;
	PrefsCategoryFrame *l_frame = NULL;

	// General
	l_frame = new PrefsCategoryFrameGeneral ();
	row = *(m_ref_treemodel->append ());
	row[m_tree_columns.m_col_id]    = "general";
	row[m_tree_columns.m_col_name]  = "General";
	row[m_tree_columns.m_col_frame] = l_frame;
	m_viewport->add
	(l_frame->widget_ref ());

	// Search
	row = *(m_ref_treemodel->append ());
	row[m_tree_columns.m_col_id]    = "search";
	row[m_tree_columns.m_col_name]  = "Search";
	row[m_tree_columns.m_col_frame] =
	    new PrefsCategoryFrameSearch ();

	// Views
	row = *(m_ref_treemodel->append ());
	row[m_tree_columns.m_col_id]    = "views";
	row[m_tree_columns.m_col_name]  = "Views";
	row[m_tree_columns.m_col_frame] =
	    new PrefsCategoryFrame ("prefs_category_box_views");

	// Views > Source View
	childrow = *(m_ref_treemodel->append (row.children ()));
	childrow[m_tree_columns.m_col_id]    = "source-view";
	childrow[m_tree_columns.m_col_name]  = "Source view";
	childrow[m_tree_columns.m_col_frame] =
	    new PrefsCategoryFrameSourceView ();

	// Views > Tree View
	childrow = *(m_ref_treemodel->append (row.children ()));
	childrow[m_tree_columns.m_col_id]    = "tree-view";
	childrow[m_tree_columns.m_col_name]  = "Tree view";
	childrow[m_tree_columns.m_col_frame] =
	    new PrefsCategoryFrameTreeview ();

}

void
PrefsWindowPriv::setup_event_handlers ()
{
	m_ref_treeselection->signal_changed ().connect
	(sigc::mem_fun (*this,
	                &PrefsWindowPriv::on_category_selection));

	m_close_button->signal_clicked ().connect
	(sigc::mem_fun (*this,
	                &PrefsWindowPriv::on_close_button_click));

}

} // namespace mlview
