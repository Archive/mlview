/* -*- Mode: C; indent-tabs-mode:nil; c-basic-offset: 8-*- */

/*
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute 
 *it and/or modify it under the terms of 
 *the GNU General Public License as published by the 
 *Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */

#ifndef __MLVIEW_IDBO_H__
#define __MLVIEW_IDBO_H__

#include "mlview-utils.h"
#include "mlview-app-context.h"

#ifdef MLVIEW_WITH_DBUS
#include <dbus/dbus.h>
#include <dbus/dbus-glib-lowlevel.h>

/**
 *@file
 *The declaration of the #MlViewIDBO interface.
 */
G_BEGIN_DECLS

#define MLVIEW_TYPE_IDBO (mlview_idbo_get_type ())
#define MLVIEW_IDBO(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), MLVIEW_TYPE_IDBO, MlViewIDBO))
#define MLVIEW_IS_IDBO(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), MLVIEW_TYPE_IDBO))
#define MLVIEW_IDBO_GET_IFACE(obj) (G_TYPE_INSTANCE_GET_INTERFACE ((obj), MLVIEW_TYPE_IDBO, MlViewIDBO))


typedef struct _MlViewIDBO MlViewIDBO ;

typedef MlViewIDBO * (*MlViewIDBOCreateInstanceFunc) (MlViewAppContext *a_this) ;

struct _MlViewIDBO
{
	GTypeInterface parent_iface ;

	DBusHandlerResult (*message_handler) (DBusConnection *a_connection,
	                                      DBusMessage *a_message,
	                                      void *a_data);

	void (*message_unregister_handler) (DBusConnection *a_connection,
	                                    void *a_data);
} ;

GType mlview_idbo_get_type (void) ;

DBusObjectPathMessageFunction
mlview_idbo_get_message_handler (MlViewIDBO *a_this) ;

DBusObjectPathUnregisterFunction
mlview_idbo_get_message_unregister_handler (MlViewIDBO *a_this) ;

#endif /*MLVIEW_WITH_DBUS*/
#endif /*__MLVIEW_IDBO_H__*/

