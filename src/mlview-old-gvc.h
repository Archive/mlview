/* -*- Mode: C++; indent-tabs-mode: true; c-basic-offset: 4-*- */

/**
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute it 
 *and/or modify it under the terms of 
 *the GNU General Public License as published 
 *by the Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *MlView is distributed in the hope that it will be useful, 
 *but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. If not, write to the Free Software Foundation, Inc., 
 *59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */
#ifndef __MLVIEW_OLD_GVC_H__
#define __MLVIEW_OLD_GVC_H__

#include <gtkmm.h>
#include "mlview-gvc-iface.h"

namespace mlview
{
	struct OldGVCPriv ;

	class OldGVC : public GVCIface, public Gtk::Notebook {
	friend struct OldGVCPriv ;

	OldGVCPriv *m_priv ;

	//forbid copy/assignation
	OldGVC (OldGVC const&a_gvc) ;
	OldGVC& operator= (OldGVC const &a_gvc) ;

	public:

	OldGVC () ;
	virtual ~OldGVC () ;

	//*******************
	//implem of GVCIface
	//*******************
	enum MlViewStatus insert_view (IView *a_view,
			               long a_index=-1/*append*/) ;

	enum MlViewStatus remove_view (IView *a_view) ;

	Gtk::Widget* get_embeddable_container_widget () ;

	protected:

	IView* retrieve_current_view_from_notebook (void) ;
	Gtk::Widget * create_tab_title (IView *a_view, UString &a_title) ;
	char* replace_slashes (gchar* a_str) ;

	//*****************
	//signal callbacks
	//****************

	void on_switch_page (GtkNotebookPage *a_page,
						 gint a_page_num) ;

	void on_view_name_changed (IView* a_view) ;

	void on_close_tab_button_clicked () ;

} ;//class OldGVC
}//end namespace mlview
#endif
