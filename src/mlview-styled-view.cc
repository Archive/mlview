/* -*- Mode: C; indent-tabs-mode:nil; c-basic-offset: 8-*- */
/*
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute 
 *it and/or modify it under the terms of 
 *the GNU General Public License as published by the 
 *Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING.
 *If not, write to the Free Software Foundation,
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHTS file for copyright information.
 */

#include <string.h>
#include <gnome.h>
#include <glade/glade.h>
#include "mlview-styled-view.h"
#include "mlview-tree-editor.h"

/**
 *Implementation of the #MlViewStyledView class.
 */
#ifdef MLVIEW_WITH_STYLE

#define PRIVATE(obj) ((obj)->priv)

struct _MlViewStyledViewPriv
{
	GtkPaned *main_paned ;
	MlViewTreeEditor *tree_editor ;
	SXBoxView * styled_editor ;
	gboolean dispose_has_run ;
	gboolean connected_to_doc ;
	MlViewXMLDocument *xml_doc ;
	MlViewAppContext *app_context ;
	GtkDialog * css_picker_dialog ;
	GtkEntry *css_picker_author_css_path ;
	GtkEntry *css_picker_user_css_path ;
	GtkEntry *css_picker_ua_css_path ;
	GtkWidget *sewfox_contextual_menu ;
	GtkWidget *tree_editor_contextual_menu ;
} ;

enum {
    FIRST_SIGNAL,
    /*this must be the last member of this enum*/
    NUMBER_OF_SIGNALS
} ;

static MlViewViewAdapterClass *gv_parent_class = NULL ;
static guint gv_signals[NUMBER_OF_SIGNALS] = {0} ;

static GtkWidget * get_sewfox_contextual_menu (MlViewStyledView *a_this) ;

static GtkWidget * get_tree_editor_contextual_menu (MlViewStyledView *a_this) ;

/*****************
 *private methods
 *****************/


static void
mlview_styled_view_dispose (GObject *a_this)
{
	MlViewStyledView *thiz = NULL ;

	g_return_if_fail (a_this
	                  && MLVIEW_IS_STYLED_VIEW (a_this)) ;

	thiz = MLVIEW_STYLED_VIEW (a_this) ;
	if (PRIVATE (thiz)->dispose_has_run)
		return ;

	if (PRIVATE (thiz)
	        && PRIVATE (thiz)->connected_to_doc == TRUE
	        && PRIVATE (thiz)->xml_doc) {
		mlview_styled_view_disconnect_from_doc
		(MLVIEW_IVIEW (thiz),
		 PRIVATE (thiz)->xml_doc) ;
	}
	if (PRIVATE (thiz)
	        && PRIVATE (thiz)->xml_doc) {
		mlview_xml_document_unref (PRIVATE (thiz)->xml_doc) ;
		PRIVATE (thiz)->xml_doc = NULL ;
	}
	PRIVATE (thiz)->dispose_has_run = TRUE ;
}

static void
mlview_styled_view_finalize (GObject *a_this)
{
	MlViewStyledView *thiz = NULL ;

	g_return_if_fail (a_this && MLVIEW_IS_STYLED_VIEW (a_this)) ;

	thiz = MLVIEW_STYLED_VIEW (a_this) ;
	if (PRIVATE (thiz)) {
		g_free (PRIVATE (thiz));
		PRIVATE (thiz) = NULL ;
	}
}

static void
mlview_styled_view_class_init (MlViewStyledViewClass *a_class)
{
	GObjectClass *gobject_class = NULL ;

	g_return_if_fail (a_class != NULL);

	gv_parent_class = g_type_class_peek_parent (a_class);
	g_return_if_fail (gv_parent_class) ;
	gobject_class = G_OBJECT_CLASS (a_class);
	g_return_if_fail (gobject_class) ;

	gobject_class->dispose = mlview_styled_view_dispose ;
	gobject_class->finalize = mlview_styled_view_finalize ;
}

static void
mlview_styled_view_init (MlViewStyledView *a_this)
{
	MlViewIView *view_iface = NULL ;
	g_return_if_fail (a_this
	                  && MLVIEW_IS_STYLED_VIEW (a_this)
	                  && MLVIEW_IS_IVIEW (a_this)) ;

	if (PRIVATE (a_this) == NULL) {
		PRIVATE (a_this) = g_try_malloc
		                   (sizeof (MlViewStyledViewPriv)) ;
		if (!PRIVATE (a_this)) {
			mlview_utils_trace_debug ("g_try_malloc failed") ;
			return ;
		}
		memset (PRIVATE (a_this), 0,
		        sizeof (MlViewStyledViewPriv)) ;
	}
	view_iface = MLVIEW_IVIEW_GET_IFACE (a_this) ;
	g_return_if_fail (view_iface) ;
	view_iface->execute_action = mlview_styled_view_execute_action ;
	view_iface->connect_to_doc = mlview_styled_view_connect_to_doc ;
	view_iface->disconnect_from_doc = mlview_styled_view_disconnect_from_doc ;
}

static void
browse_author_css_button_clicked_cb (GtkWidget *a_button,
                                     gpointer a_user_data)
{
	MlViewFileSelection *file_sel = NULL ;
	MlViewStyledView *view = NULL;
	gchar *path = NULL ;
	gint button = 0 ;

	g_return_if_fail (a_user_data && MLVIEW_IS_STYLED_VIEW (a_user_data)) ;

	view = a_user_data ;
	g_return_if_fail (PRIVATE (view) && PRIVATE (view)->app_context) ;

	file_sel = mlview_app_context_get_file_selector
	           (PRIVATE (view)->app_context, "_(Choose a CSS stylesheet)") ;
	g_return_if_fail (file_sel) ;
	button = mlview_file_selection_run (file_sel, TRUE) ;

	switch (button) {
	case OK_BUTTON:
		path = g_strdup (gtk_file_selection_get_filename
		                 (GTK_FILE_SELECTION (file_sel))) ;
		gtk_entry_set_text (PRIVATE (view)->css_picker_author_css_path,
		                    path) ;
		if (path) {
			g_free (path) ;
			path = NULL ;
		}
		break ;
	case CANCEL_BUTTON:
	case WINDOW_CLOSED:
	default:
		break ;
	}
}

static void
browse_user_css_button_clicked_cb (GtkWidget *a_button,
                                   gpointer a_user_data)
{
	MlViewFileSelection *file_sel = NULL ;
	MlViewStyledView *view = NULL;
	gchar *path = NULL ;
	gint button = 0 ;

	g_return_if_fail (a_user_data && MLVIEW_IS_STYLED_VIEW (a_user_data)) ;

	view = a_user_data ;
	g_return_if_fail (PRIVATE (view) && PRIVATE (view)->app_context) ;

	file_sel = mlview_app_context_get_file_selector
	           (PRIVATE (view)->app_context, "_(Choose a CSS stylesheet)") ;
	g_return_if_fail (file_sel) ;
	button = mlview_file_selection_run (file_sel, TRUE) ;

	switch (button) {
	case OK_BUTTON:
		path = g_strdup (gtk_file_selection_get_filename
		                 (GTK_FILE_SELECTION (file_sel))) ;
		gtk_entry_set_text (PRIVATE (view)->css_picker_user_css_path,
		                    path) ;
		if (path) {
			g_free (path) ;
			path = NULL ;
		}
		break ;
	case CANCEL_BUTTON:
	case WINDOW_CLOSED:
	default:
		break ;
	}
}

static void
browse_ua_css_button_clicked_cb (GtkWidget *a_button,
                                 gpointer a_user_data)
{
	MlViewFileSelection *file_sel = NULL ;
	MlViewStyledView *view = NULL;
	gchar *path = NULL ;
	gint button = 0 ;

	g_return_if_fail (a_user_data && MLVIEW_IS_STYLED_VIEW (a_user_data)) ;

	view = a_user_data ;
	g_return_if_fail (PRIVATE (view) && PRIVATE (view)->app_context) ;

	file_sel = mlview_app_context_get_file_selector
	           (PRIVATE (view)->app_context, "_(Choose a CSS stylesheet)") ;
	g_return_if_fail (file_sel) ;
	button = mlview_file_selection_run (file_sel, TRUE) ;

	switch (button) {
	case OK_BUTTON:
		path = g_strdup (gtk_file_selection_get_filename
		                 (GTK_FILE_SELECTION (file_sel))) ;
		gtk_entry_set_text (PRIVATE (view)->css_picker_ua_css_path,
		                    path) ;
		if (path) {
			g_free (path) ;
			path = NULL ;
		}
		break ;
	case CANCEL_BUTTON:
	case WINDOW_CLOSED:
	default:
		break ;
	}
}

static gboolean
button_press_event_cb (GtkWidget *a_box_view,
                       GdkEvent *a_event,
                       gpointer a_user_data)
{
	MlViewAppContext *ctxt = NULL ;
	MlViewStyledView *view = NULL ;
	enum MlViewStatus status = MLVIEW_OK ;

	g_return_val_if_fail (a_event && a_box_view && SX_IS_BOX_VIEW (a_box_view)
	                      && a_user_data && a_event
	                      && MLVIEW_IS_STYLED_VIEW (a_user_data),
	                      FALSE) ;

	view = MLVIEW_STYLED_VIEW (a_user_data) ;
	ctxt = mlview_styled_view_get_app_context (view) ;

	switch (a_event->type) {
	case GDK_BUTTON_PRESS:
		if (a_event->button.button == 3) {
			/*
			 *user pressed the right mouse button.
			 *Notify the application (at least the
			 *editing view that contains use) that
			 *a request for a contextual menu request
			 *has been made
			 */
			status = mlview_app_context_notify_contextual_menu_request
			         (ctxt, a_box_view, a_event) ;
			g_return_val_if_fail (status == MLVIEW_OK, FALSE) ;
			return TRUE ;
		}
		break ;
	default:
		break ;
	}
	return FALSE ;
}

static void
contextual_menu_request_cb (MlViewAppContext *a_this,
                            GtkWidget *a_request_source,
                            GdkEvent *a_event,
                            gpointer a_user_data)
{
	MlViewStyledView *view = NULL ;
	GtkWidget * contextual_menu = NULL ;
	GdkEventButton *button_event = NULL ;

	g_return_if_fail (a_this && MLVIEW_IS_APP_CONTEXT (a_this)
	                  && a_request_source
	                  && GTK_IS_WIDGET (a_request_source)
	                  && a_event
	                  && a_user_data
	                  && MLVIEW_IS_STYLED_VIEW (a_user_data)) ;

	view = MLVIEW_STYLED_VIEW (a_user_data) ;
	g_return_if_fail (view && PRIVATE (view)) ;
	if (a_event->type == GDK_BUTTON_PRESS) {
		button_event = (GdkEventButton*) a_event;
		if (GTK_WIDGET (PRIVATE (view)->styled_editor)
		        == a_request_source) {
			contextual_menu = get_sewfox_contextual_menu (view) ;
		} else if (GTK_WIDGET (PRIVATE (view)->tree_editor)
		           == a_request_source) {
			contextual_menu = get_tree_editor_contextual_menu
			                  (view) ;
		}

		if (contextual_menu && GTK_IS_MENU (contextual_menu)) {
			gtk_menu_popup (GTK_MENU (contextual_menu),
			                NULL, NULL, NULL,
			                a_this, button_event->button,
			                button_event->time) ;
		} else {
			mlview_utils_trace_debug ("Null menu returned") ;
		}
	}
}

static void
load_a_cascade_menuitem_clicked_cb (GtkMenuItem *a_item,
                                    gpointer a_user_data)
{
	MlViewStyledView *view = NULL ;

	g_return_if_fail (a_item && GTK_IS_MENU_ITEM (a_item)) ;
	g_return_if_fail (a_user_data && MLVIEW_IS_STYLED_VIEW (a_user_data)) ;

	view = MLVIEW_STYLED_VIEW (a_user_data) ;
	mlview_styled_view_update_from_css_paths_interactive (view) ;
}

static void
dump_box_model_menuitem_clicked_cb (GtkMenuItem *a_item,
                                    gpointer a_user_data)
{
	enum CRStatus status = CR_OK ;
	SXBoxModel *box_model = NULL ;
	MlViewStyledView *view = NULL ;

	g_return_if_fail (a_item && GTK_IS_MENU_ITEM (a_item)) ;
	g_return_if_fail (a_user_data && MLVIEW_IS_STYLED_VIEW (a_user_data)) ;

	view = MLVIEW_STYLED_VIEW (a_user_data) ;
	status = sx_box_view_get_box_model (PRIVATE (view)->styled_editor,
	                                    &box_model) ;
	if (status == CR_OK && box_model) {
		sx_box_dump_to_file ((SXBox*)box_model, 2, stdout) ;
	} else {
		mlview_utils_trace_debug ("Could not get box model") ;
	}

}


static void
add_child_node_menuitem_activated_cb (GtkMenuItem *a_menu_item,
                                      gpointer a_user_data)
{
	MlViewAction action = {0} ;
	MlViewStyledView *tree_view = a_user_data ;

	g_return_if_fail (tree_view
	                  && MLVIEW_STYLED_VIEW (tree_view)) ;

	action.name = (gchar*)"add-child-node-interactive" ;
	mlview_styled_view_execute_action (MLVIEW_IVIEW(tree_view),
	                                   &action) ;

}


static void
insert_previous_node_menuitem_activated_cb (GtkMenuItem *a_menu_item,
        gpointer a_user_data)
{
	MlViewAction action = {0} ;
	MlViewStyledView *tree_view = a_user_data ;

	g_return_if_fail (tree_view
	                  && MLVIEW_STYLED_VIEW (tree_view)) ;

	action.name = (gchar*)"insert-prev-sibling-node-interactive" ;
	mlview_styled_view_execute_action (MLVIEW_IVIEW(tree_view),
	                                   &action) ;
}

static void
insert_next_node_menuitem_activated_cb (GtkMenuItem *a_menu_item,
                                        gpointer a_user_data)
{
	MlViewAction action = {0} ;
	MlViewStyledView *tree_view = a_user_data ;

	g_return_if_fail (tree_view
	                  && MLVIEW_STYLED_VIEW (tree_view)) ;

	action.name = (gchar*) "insert-next-sibling-node-interactive" ;
	mlview_styled_view_execute_action (MLVIEW_IVIEW(tree_view),
	                                   &action) ;
}

static void
copy_node_menuitem_activated_cb (GtkMenuItem *a_menu_item,
                                 gpointer a_user_data)
{
	MlViewAction action = {0} ;
	MlViewStyledView *tree_view = a_user_data ;

	g_return_if_fail (tree_view
	                  && MLVIEW_STYLED_VIEW (tree_view)) ;

	action.name = (gchar*) "copy-node" ;
	mlview_styled_view_execute_action (MLVIEW_IVIEW(tree_view),
	                                   &action) ;
}


static void
cut_node_menuitem_activated_cb (GtkMenuItem *a_menu_item,
                                gpointer a_user_data)
{
	MlViewAction action = {0} ;
	MlViewStyledView *tree_view = a_user_data ;

	g_return_if_fail (tree_view
	                  && MLVIEW_STYLED_VIEW (tree_view)) ;

	action.name = (gchar*) "cut-node" ;
	mlview_styled_view_execute_action (MLVIEW_IVIEW(tree_view),
	                                   &action) ;
}

static void
paste_node_as_child_menuitem_activated_cb (GtkMenuItem *a_menu_item,
        gpointer a_user_data)
{
	MlViewAction action = {0} ;
	MlViewStyledView *tree_view = a_user_data ;

	g_return_if_fail (tree_view
	                  && MLVIEW_STYLED_VIEW (tree_view)) ;

	action.name = (gchar*) "paste-node-as-child" ;
	mlview_styled_view_execute_action (MLVIEW_IVIEW(tree_view),
	                                   &action) ;
}

static void
paste_node_as_prev_menuitem_activated_cb (GtkMenuItem *a_menu_item,
        gpointer a_user_data)
{
	MlViewAction action = {0} ;
	MlViewStyledView *tree_view = a_user_data ;

	g_return_if_fail (tree_view
	                  && MLVIEW_STYLED_VIEW (tree_view)) ;

	action.name = (gchar*) "paste-node-as-prev-sibling" ;
	mlview_styled_view_execute_action (MLVIEW_IVIEW(tree_view),
	                                   &action) ;
}

static void
paste_node_as_next_menuitem_activated_cb (GtkMenuItem *a_menu_item,
        gpointer a_user_data)
{
	MlViewAction action = {0} ;
	MlViewStyledView *tree_view = a_user_data ;

	g_return_if_fail (tree_view
	                  && MLVIEW_STYLED_VIEW (tree_view)) ;

	action.name = (gchar*) "paste-node-as-next-sibling" ;
	mlview_styled_view_execute_action (MLVIEW_IVIEW(tree_view),
	                                   &action) ;
}

static void
search_an_xml_node_menuitem_activated_cb (GtkMenuItem *a_menu_item,
        gpointer a_user_data)
{
	MlViewAction action = {0} ;
	MlViewStyledView *tree_view = a_user_data ;

	g_return_if_fail (tree_view
	                  && MLVIEW_STYLED_VIEW (tree_view)) ;

	action.name = (gchar*) "find-node-that-contains-str-interactive" ;
	mlview_styled_view_execute_action (MLVIEW_IVIEW(tree_view),
	                                   &action) ;
}

static void
expand_current_node_menuitem_activated_cb (GtkMenuItem *a_item,
        gpointer a_user_data)
{
	MlViewAction action = {0} ;
	MlViewStyledView *tree_view = a_user_data ;

	g_return_if_fail (tree_view
	                  && MLVIEW_STYLED_VIEW (tree_view)) ;

	action.name = (gchar*) "expand-tree-to-depth-interactive" ;
	mlview_styled_view_execute_action (MLVIEW_IVIEW (tree_view),
	                                   &action) ;
}

static GtkWidget *
get_css_paths_picker_dialog (MlViewStyledView *a_this)
{
	gchar *path = NULL ;
	GladeXML *glade_xml = NULL ;

	g_return_val_if_fail (a_this && MLVIEW_IS_STYLED_VIEW (a_this)
	                      && PRIVATE (a_this),
	                      NULL) ;

	if (!PRIVATE (a_this)->css_picker_dialog) {
		path = gnome_program_locate_file (NULL, GNOME_FILE_DOMAIN_APP_DATADIR,
		                                  "mlview/mlview-css-picker.glade",
		                                  TRUE, NULL) ;
		if (!path) {
			mlview_utils_trace_debug
			("could not find mlview-css-picker.glade file") ;
			return NULL ;
		}
		glade_xml = glade_xml_new (path, "CSSPicker", NULL) ;
		if (!glade_xml) {
			mlview_utils_trace_debug ("could not find mlview-css-picker.glade file") ;
			goto cleanup ;
		}
		PRIVATE (a_this)->css_picker_dialog = GTK_DIALOG
		                                      (glade_xml_get_widget (glade_xml, "CSSPicker")) ;
		if (!PRIVATE (a_this)->css_picker_dialog
		        || !GTK_IS_DIALOG (PRIVATE (a_this)->css_picker_dialog)) {
			mlview_utils_trace_debug ("glade_xml_get_widget() made an unexpected result") ;
		}
		PRIVATE (a_this)->css_picker_author_css_path =
		    GTK_ENTRY (glade_xml_get_widget (glade_xml,
		                                     "AuthorCSSPathEntry")) ;
		g_return_val_if_fail (PRIVATE (a_this)->css_picker_author_css_path,
		                      NULL) ;
		PRIVATE (a_this)->css_picker_user_css_path =
		    GTK_ENTRY (glade_xml_get_widget (glade_xml,
		                                     "UserCSSPathEntry")) ;
		g_return_val_if_fail (PRIVATE (a_this)->css_picker_author_css_path,
		                      NULL) ;
		PRIVATE (a_this)->css_picker_ua_css_path =
		    GTK_ENTRY (glade_xml_get_widget (glade_xml,
		                                     "UACSSPathEntry")) ;
		g_return_val_if_fail (PRIVATE (a_this)->css_picker_author_css_path,
		                      NULL) ;
		glade_xml_signal_connect_data (glade_xml, "browse_author_css_button_clicked",
		                               G_CALLBACK (browse_author_css_button_clicked_cb),
		                               a_this) ;
		glade_xml_signal_connect_data (glade_xml, "browse_user_css_button_clicked",
		                               G_CALLBACK (browse_user_css_button_clicked_cb),
		                               a_this) ;
		glade_xml_signal_connect_data (glade_xml, "browse_ua_css_button_clicked",
		                               G_CALLBACK (browse_ua_css_button_clicked_cb),
		                               a_this) ;
		g_object_unref (glade_xml) ;
		glade_xml = NULL;
	}
	return GTK_WIDGET (PRIVATE (a_this)->css_picker_dialog) ;

cleanup:
	if (path) {
		g_free (path) ;
		path = NULL ;
	}
	if (glade_xml) {
		g_object_unref (glade_xml) ;
		glade_xml = NULL ;
	}
	return NULL ;
}

static GtkWidget *
get_tree_editor_contextual_menu (MlViewStyledView *a_this)
{
	GtkWidget *menuitem = NULL, *menu = NULL ;

	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_STYLED_VIEW (a_this),
	                      NULL) ;

	if (!PRIVATE (a_this)->tree_editor_contextual_menu) {
		menu = gtk_menu_new () ;
		menuitem = gtk_menu_item_new_with_label
		           (_("Add child node")) ;
		gtk_menu_shell_append (GTK_MENU_SHELL (menu),
		                       menuitem) ;
		gtk_widget_show (menuitem) ;
		g_signal_connect (G_OBJECT (menuitem),
		                  "activate",
		                  G_CALLBACK
		                  (add_child_node_menuitem_activated_cb),
		                  a_this) ;
		menuitem = gtk_menu_item_new_with_label
		           (_("Insert next node")) ;
		gtk_menu_shell_append (GTK_MENU_SHELL (menu),
		                       menuitem) ;
		gtk_widget_show (menuitem) ;
		g_signal_connect (G_OBJECT (menuitem),
		                  "activate",
		                  G_CALLBACK
		                  (insert_next_node_menuitem_activated_cb),
		                  a_this) ;

		menuitem = gtk_menu_item_new_with_label
		           (_("Insert previous node")) ;
		gtk_menu_shell_append (GTK_MENU_SHELL (menu),
		                       menuitem) ;
		gtk_widget_show (menuitem) ;
		g_signal_connect (G_OBJECT (menuitem),
		                  "activate",
		                  G_CALLBACK
		                  (insert_previous_node_menuitem_activated_cb),
		                  a_this) ;

		menuitem = gtk_separator_menu_item_new () ;
		gtk_menu_shell_append (GTK_MENU_SHELL (menu),
		                       menuitem) ;
		gtk_widget_show (GTK_WIDGET (menuitem)) ;

		menuitem = gtk_menu_item_new_with_label
		           (_("Copy node")) ;
		gtk_menu_shell_append (GTK_MENU_SHELL (menu),
		                       menuitem) ;
		gtk_widget_show (GTK_WIDGET (menuitem)) ;
		g_signal_connect (G_OBJECT (menuitem),
		                  "activate",
		                  G_CALLBACK
		                  (copy_node_menuitem_activated_cb),
		                  a_this) ;

		menuitem = gtk_menu_item_new_with_label
		           (_("Cut node")) ;
		gtk_menu_shell_append (GTK_MENU_SHELL (menu),
		                       menuitem) ;
		gtk_widget_show (menuitem) ;
		g_signal_connect (G_OBJECT (menuitem),
		                  "activate",
		                  G_CALLBACK (cut_node_menuitem_activated_cb),
		                  a_this) ;

		menuitem = gtk_separator_menu_item_new () ;
		gtk_menu_shell_append (GTK_MENU_SHELL (menu),
		                       menuitem) ;
		gtk_widget_show (menuitem) ;

		menuitem = gtk_menu_item_new_with_label
		           (_("Paste node as child")) ;
		gtk_menu_shell_append (GTK_MENU_SHELL (menu),
		                       menuitem) ;
		g_signal_connect (G_OBJECT (menuitem),
		                  "activate",
		                  G_CALLBACK
		                  (paste_node_as_child_menuitem_activated_cb),
		                  a_this) ;
		gtk_widget_show (menuitem) ;

		menuitem = gtk_menu_item_new_with_label
		           (_("Paste node as prev")) ;
		gtk_menu_shell_append (GTK_MENU_SHELL (menu),
		                       menuitem) ;
		g_signal_connect (G_OBJECT (menuitem),
		                  "activate",
		                  G_CALLBACK
		                  (paste_node_as_prev_menuitem_activated_cb),
		                  a_this) ;
		gtk_widget_show (menuitem) ;

		menuitem = gtk_menu_item_new_with_label
		           (_("Paste node as next")) ;
		gtk_menu_shell_append (GTK_MENU_SHELL (menu),
		                       menuitem) ;
		g_signal_connect (G_OBJECT (menuitem),
		                  "activate",
		                  G_CALLBACK
		                  (paste_node_as_next_menuitem_activated_cb),
		                  a_this) ;
		gtk_widget_show (menuitem) ;

		menuitem = gtk_separator_menu_item_new () ;
		gtk_menu_shell_append (GTK_MENU_SHELL (menu),
		                       menuitem) ;
		gtk_widget_show (menuitem) ;

		menuitem = gtk_menu_item_new_with_label
		           (_("Expand current node")) ;
		gtk_menu_shell_append (GTK_MENU_SHELL (menu),
		                       menuitem) ;
		g_signal_connect (G_OBJECT (menuitem),
		                  "activate",
		                  G_CALLBACK
		                  (expand_current_node_menuitem_activated_cb),
		                  a_this) ;
		gtk_widget_show (menuitem) ;

		menuitem = gtk_menu_item_new_with_label
		           (_("Find an xml node")) ;
		gtk_menu_shell_append (GTK_MENU_SHELL (menu),
		                       menuitem) ;
		g_signal_connect (G_OBJECT (menuitem),
		                  "activate",
		                  G_CALLBACK
		                  (search_an_xml_node_menuitem_activated_cb),
		                  a_this) ;
		gtk_widget_show (menuitem) ;
		PRIVATE (a_this)->tree_editor_contextual_menu = menu ;
	}
	return  PRIVATE (a_this)->tree_editor_contextual_menu ;
}

/**
 *Builds the contextual menu that should be popup'ed
 *when the user right clicks on the sewfox widget embedded
 *in the styled editing view
 *@param a_this the current instance of #MlViewStyledView
 *@return the contextual menu or NULL if a problem arises
 */
static GtkWidget *
get_sewfox_contextual_menu (MlViewStyledView *a_this)
{
	GtkWidget *menuitem = NULL ;

	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_STYLED_VIEW (a_this),
	                      NULL) ;

	if (!PRIVATE (a_this)->sewfox_contextual_menu) {
		PRIVATE (a_this)->sewfox_contextual_menu = gtk_menu_new () ;
		menuitem = gtk_menu_item_new_with_label (_("Load a Cascade")) ;
		gtk_widget_show (menuitem) ;
		gtk_menu_shell_append (GTK_MENU_SHELL (PRIVATE (a_this)->sewfox_contextual_menu),
		                       menuitem) ;
		g_signal_connect (G_OBJECT (menuitem),
		                  "activate",
		                  G_CALLBACK (load_a_cascade_menuitem_clicked_cb),
		                  a_this) ;
#ifdef MLVIEW_VERBOSE

		menuitem = gtk_menu_item_new () ;
		gtk_widget_show (menuitem) ;
		gtk_menu_shell_append (GTK_MENU_SHELL (PRIVATE (a_this)->sewfox_contextual_menu),
		                       menuitem) ;
		menuitem = gtk_menu_item_new_with_label (_("Dump box model")) ;
		gtk_menu_shell_append (GTK_MENU_SHELL (PRIVATE (a_this)->sewfox_contextual_menu),
		                       menuitem) ;
		gtk_widget_show (menuitem) ;
		g_signal_connect (G_OBJECT (menuitem),
		                  "activate",
		                  G_CALLBACK (dump_box_model_menuitem_clicked_cb),
		                  a_this) ;
#endif

	}
	return PRIVATE (a_this)->sewfox_contextual_menu ;
}

/****************
 *public methods
 ****************/

/*
 *TODO: 
 *1/code the methods to edit the document using the left hand
 *widget, wire them in mlview_styled_view_execute_action().
 *2/go code some facilities to dump the style structures for easier debuging.
 */

/**
 *Standard gobject based type getter of the
 *#MlViewStyledView class.
 *@return the type identifier of the #MlViewStyledView class.
 */
GType
mlview_styled_view_get_type (void)
{
	static guint type = 0;

	if (!type) {
		static const GTypeInfo type_info = {
		                                       sizeof (MlViewStyledViewClass),
		                                       NULL, /*base_init*/
		                                       NULL, /*base_finalize*/
		                                       (GClassInitFunc)
		                                       mlview_styled_view_class_init,
		                                       NULL, /*class_finalize*/
		                                       NULL, /*class data*/
		                                       sizeof (MlViewStyledView),
		                                       0, /*n_prealloc*/
		                                       (GInstanceInitFunc)
		                                       mlview_styled_view_init
		                                   } ;
		type = g_type_register_static
		       (MLVIEW_TYPE_VIEW_ADAPTER,
		        "MlViewStyledView", &type_info, 0);
	}
	return type;
}

/**
 *Constructor of the #MlViewStyledView class
 *@param a_doc the xml document to "edit"
 *@param a_name an arbitrary name for the document (or view)
 *@param a_app_context the application context. (should not be NULL)
 *@return the newly created instance of #MlViewStyledView or NULL in case
 *of error.
 */
GtkWidget*
mlview_styled_view_new (MlViewXMLDocument *a_doc,
                        const gchar *a_name,
                        MlViewAppContext *a_app_context)
{
	MlViewStyledView *view = NULL ;

	g_return_val_if_fail (a_doc
	                      && MLVIEW_IS_XML_DOCUMENT (a_doc)
	                      && PRIVATE (a_doc),
	                      NULL) ;
	view = g_object_new (MLVIEW_TYPE_STYLED_VIEW, NULL) ;
	mlview_styled_view_construct (view, a_doc, a_name, a_app_context) ;
	return GTK_WIDGET (view) ;
}

/**
 *Instance initializer of #MlViewStyleView; This should be called by
 *the constructor of #MlViewStyledView
 *@param a_doc the xml document we want to edit
 *@param a_view_name an arbitrary name of the view
 *@param a_app_context the application context to be associated to
 *the editing view
 *@return MLVIEW_OK upon successful completion, an erroro code otherwise.
 */
enum MlViewStatus
mlview_styled_view_construct (MlViewStyledView *a_this,
                              MlViewXMLDocument *a_doc,
                              const gchar *a_view_name,
                              MlViewAppContext *a_app_context)
{
	enum CRStatus cr_status = CR_OK ;
	enum MlViewStatus status = MLVIEW_OK ;
	CRStyleSheet *sheet = NULL ;
	CRCascade * cascade = NULL ;
	xmlDoc *xml_doc = NULL ;
	GtkWidget *scroll = NULL ;

	if (PRIVATE (a_this)->main_paned
	        || PRIVATE (a_this)->tree_editor
	        || PRIVATE (a_this)->styled_editor) {
		return MLVIEW_OK ;
	}
	status = mlview_view_adapter_construct
	         (MLVIEW_VIEW_ADAPTER (a_this),
	          a_doc);
	g_return_val_if_fail (status == MLVIEW_VIEW_ADAPTER_OK,
	                      status) ;

	cr_status = sx_box_view_get_default_stylesheet (&sheet, FALSE) ;
	if (cr_status != CR_OK || ! sheet)
		return MLVIEW_ERROR ;
	cascade = cr_cascade_new (NULL, NULL, sheet) ;
	if (!cascade) {
		mlview_utils_trace_debug ("cr_cascade_new() failed") ;
		status = MLVIEW_ERROR ;
		goto cleanup ;
	}
	xml_doc = mlview_xml_document_get_xml_document (a_doc) ;
	if (!xml_doc) {
		mlview_utils_trace_debug
		("mlview_xml_document_get_xml_document () failed") ;
		status = MLVIEW_OK ;
		goto cleanup ;
	}
	PRIVATE (a_this)->styled_editor = sx_box_view_new (cascade, xml_doc) ;
	if (!PRIVATE (a_this)->styled_editor) {
		mlview_utils_trace_debug ("sx_box_new() failed") ;
		status = MLVIEW_OK ;
		goto cleanup ;
	}
	PRIVATE (a_this)->tree_editor = MLVIEW_TREE_EDITOR
	                                (mlview_tree_editor_new (a_app_context)) ;
	mlview_tree_editor_edit_xml_doc (PRIVATE (a_this)->tree_editor,
	                                 a_doc, (gchar*)"Experimental") ;
	PRIVATE (a_this)->main_paned = GTK_PANED (gtk_hpaned_new ()) ;
	gtk_box_pack_start (GTK_BOX (a_this),
	                    GTK_WIDGET (PRIVATE (a_this)->main_paned),
	                    TRUE, TRUE, 0) ;
	gtk_paned_add1 (GTK_PANED (PRIVATE (a_this)->main_paned),
	                GTK_WIDGET (PRIVATE (a_this)->tree_editor)) ;
	scroll = gtk_scrolled_window_new (NULL, NULL) ;
	gtk_container_add (GTK_CONTAINER (scroll),
	                   GTK_WIDGET (PRIVATE (a_this)->styled_editor)) ;
	gtk_paned_add2 (GTK_PANED (PRIVATE (a_this)->main_paned), scroll) ;
	PRIVATE (a_this)->xml_doc = a_doc ;
	mlview_xml_document_ref (a_doc) ;
	gtk_widget_show_all (GTK_WIDGET (PRIVATE (a_this)->main_paned)) ;
	PRIVATE (a_this)->app_context = a_app_context ;

	mlview_iview_connect_to_doc (MLVIEW_IVIEW (view),
	                             a_doc) ;
	g_signal_connect (G_OBJECT (PRIVATE (a_this)->styled_editor),
	                  "button-press-event",
	                  G_CALLBACK (button_press_event_cb),
	                  a_this) ;
	g_signal_connect (G_OBJECT (a_app_context),
	                  "contextual-menu-requested",
	                  G_CALLBACK (contextual_menu_request_cb),
	                  a_this) ;

	sheet = NULL ;
	cascade = NULL ;
	xml_doc = NULL ;

cleanup:
	if (sheet) {
		cr_stylesheet_unref (sheet) ;
		sheet = NULL ;
	}
	if (cascade) {
		cr_cascade_unref (cascade) ;
		cascade = NULL ;
	}
	if (xml_doc) {
		xmlFreeDoc (xml_doc) ;
		xml_doc = NULL ;
	}
	return status ;
}

/**
 *Creates an internal subset node 
 *(<DOCTYPE elname PUBLIC "blah" "blabla">)
 *@param a_this the current instance of #MlViewStyledView
 *@return MLVIEW_OK upon successful completion,
 *an error code otherwise.
 */
enum MlViewStatus
mlview_styled_view_create_internal_subset_node_interactive (MlViewStyledView *a_this)
{
	xmlDoc *native_doc = NULL ;
	gchar *name = NULL ;

	g_return_val_if_fail (a_this && MLVIEW_IS_STYLED_VIEW (a_this)
	                      && PRIVATE (a_this)
	                      && PRIVATE (a_this)->app_context,
	                      MLVIEW_BAD_PARAM_ERROR) ;

	if (!PRIVATE (a_this)->xml_doc)
		return MLVIEW_NO_DOC_IN_VIEW_ERROR ;
	native_doc = mlview_xml_document_get_xml_document
	             (PRIVATE (a_this)->xml_doc) ;
	g_return_val_if_fail (native_doc, MLVIEW_ERROR) ;
	if (native_doc->intSubset) {
		mlview_app_context_error
		(PRIVATE (a_this)->app_context,
		 _("The document already has an internal subset defined !")) ;
		return MLVIEW_INT_SUBSET_ALREADY_PRESENT_ERROR ;
	}
	mlview_app_context_ask_internal_subset_node_name (&name) ;
	if (name) {
		return mlview_xml_document_create_internal_subset
		       (PRIVATE (a_this)->xml_doc, name,
		        (xmlChar*)"default-public-id",
		        (xmlChar*)"default-system-id",
		        TRUE) ;
	}
	return MLVIEW_OK ;
}

/**
 *Interactively add a child node to the currently selected xml node.
 *@param a_this the current instance of #MlViewStyledView
 *@return MLVIEW_OK upon succesful completion, an error code otherwise.
 */
enum MlViewStatus
mlview_styled_view_add_child_node_interactive (MlViewStyledView *a_this)
{
	MlViewTreeEditor *tree_editor = NULL ;

	g_return_val_if_fail (a_this && MLVIEW_IS_STYLED_VIEW (a_this)
	                      && PRIVATE (a_this)
	                      && PRIVATE (a_this)->app_context,
	                      MLVIEW_BAD_PARAM_ERROR) ;

	tree_editor = mlview_styled_view_get_tree_editor (a_this) ;
	g_return_val_if_fail (tree_editor, MLVIEW_ERROR) ;
	mlview_tree_editor_add_child_node_interactive (tree_editor) ;
	return MLVIEW_OK ;
}

/**
 *Interactively adds a previous sibling node to the currently selected
 *xml node.
 *@param a_this the current instance of #MlViewStyledView
 *@return MLVIEW_OK upon successful completion, an error code otherwise
 */
enum MlViewStatus
mlview_styled_view_insert_prev_sibling_node_interactive (MlViewStyledView *a_this)
{
	MlViewTreeEditor *tree_editor = NULL ;

	g_return_val_if_fail (a_this && MLVIEW_IS_STYLED_VIEW (a_this),
	                      MLVIEW_BAD_PARAM_ERROR) ;

	tree_editor = mlview_styled_view_get_tree_editor (a_this) ;
	g_return_val_if_fail (tree_editor, MLVIEW_ERROR) ;
	mlview_tree_editor_insert_prev_sibling_node_interactive (tree_editor) ;
	return MLVIEW_OK ;
}

/**
 *Interactively insert a next sibling node to the currently selected
 *xml node.
 *@param a_this the current instance of #MlViewStyledView
 *@return MLVIEW_OK upon succesful completion, an error code otherwise.
 */
enum MlViewStatus
mlview_styled_view_insert_next_sibling_node_interactive (MlViewStyledView * a_this)
{
	MlViewTreeEditor *tree_editor = NULL;

	g_return_val_if_fail (a_this && MLVIEW_IS_STYLED_VIEW (a_this),
	                      MLVIEW_BAD_PARAM_ERROR) ;
	tree_editor =
	    mlview_styled_view_get_tree_editor
	    (a_this);

	if (tree_editor) {
		mlview_tree_editor_insert_next_sibling_node_interactive
		(tree_editor);
	}
	return MLVIEW_OK ;
}

/**
 *Cuts the currently selected xml node.
 *That is, unlink it from the tree and put it in
 *the xml node clipboard.
 *@param a_this the current instance of #MlViewStyledView
 */
enum MlViewStatus
mlview_styled_view_cut_node (MlViewStyledView *a_this)
{
	MlViewTreeEditor *tree_editor = NULL;
	GtkTreeIter cur_sel_start = {0};
	enum MlViewStatus status = MLVIEW_OK ;

	tree_editor =
	    mlview_styled_view_get_tree_editor (a_this);

	if (tree_editor == NULL)
		return MLVIEW_OK;
	status = mlview_tree_editor_get_cur_sel_start_iter
	         (tree_editor, &cur_sel_start);
	g_return_val_if_fail (status == MLVIEW_OK, status) ;
	mlview_tree_editor_cut_node (tree_editor,
	                             &cur_sel_start);
	return MLVIEW_OK ;
}


/**
 *Copy the currently selected xml node into the local clipboard.
 *@param a_this the current instance of #MlViewStyledView
 *@return MLVIEW_OK upon succesful completion, an error code otherwise.
 */
enum MlViewStatus
mlview_styled_view_copy_node (MlViewStyledView *a_this)
{
	MlViewTreeEditor *tree_editor = NULL;
	GtkTreeIter cur_sel_start = {0} ;
	enum MlViewStatus status = MLVIEW_OK ;

	g_return_val_if_fail (a_this && MLVIEW_IS_STYLED_VIEW (a_this),
	                      MLVIEW_BAD_PARAM_ERROR);

	tree_editor =
	    mlview_styled_view_get_tree_editor
	    (a_this);
	if (tree_editor == NULL)
		return MLVIEW_OK;
	status = mlview_tree_editor_get_cur_sel_start_iter
	         (tree_editor, &cur_sel_start) ;
	g_return_val_if_fail (status == MLVIEW_OK, status) ;
	mlview_tree_editor_copy_node (tree_editor,
	                              &cur_sel_start) ;
	return MLVIEW_OK ;
}


/**
 *Paste the last node found in the clipboard into the xml document
 *model.
 *The pasted node is pasted as a child node of the currently selected
 *xml node.
 *@param a_this the current instance of #MlViewStyledView
 *@return MLVIEW_OK upon succesful completion, an error code otherwise
 */
enum MlViewStatus
mlview_styled_view_paste_node_as_child (MlViewStyledView * a_this)
{
	GtkTreeIter cur_sel_start={0} ;
	enum MlViewStatus status = MLVIEW_OK;

	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_STYLED_VIEW (a_this)
	                      && PRIVATE (a_this)
	                      && PRIVATE (a_this)->tree_editor,
	                      MLVIEW_BAD_PARAM_ERROR);

	status = mlview_tree_editor_get_cur_sel_start_iter
	         (PRIVATE (a_this)->tree_editor,
	          &cur_sel_start) ;
	g_return_val_if_fail (status == MLVIEW_OK,
	                      status) ;
	mlview_tree_editor_paste_node_as_child
	(PRIVATE (a_this)->tree_editor,
	 &cur_sel_start);
	return MLVIEW_OK ;
}


/**
 *Paste the last node found in the clipboard into the xml document
 *model.
 *The pasted node is pasted as a previous sibling node of the 
 *currently selected xml node.
 *@param a_this the current instance of #MlViewStyledView
 */
enum MlViewStatus
mlview_styled_view_paste_node_as_prev_sibling (MlViewStyledView * a_this)
{
	GtkTreeIter cur_sel_start={0} ;
	enum MlViewStatus status=MLVIEW_OK ;

	g_return_val_if_fail (a_this != NULL
	                      && MLVIEW_IS_STYLED_VIEW (a_this)
	                      && PRIVATE (a_this),
	                      MLVIEW_BAD_PARAM_ERROR) ;

	status = mlview_tree_editor_get_cur_sel_start_iter
	         (PRIVATE (a_this)->tree_editor,
	          &cur_sel_start) ;
	g_return_val_if_fail (status == MLVIEW_OK, status) ;
	status = mlview_tree_editor_paste_node_as_sibling
	         (PRIVATE (a_this)->tree_editor,
	          &cur_sel_start, TRUE) ;
	return status ;
}

/**
 *Pastes the last node found in the clipboard at
 *the place of the the currently selected xml node.
 *@param a_this a pointer to the current instance
 *of MlViewStyledView.
 */
enum MlViewStatus
mlview_styled_view_paste_node_as_next_sibling (MlViewStyledView * a_this)
{
	GtkTreeIter cur_sel_start={0} ;
	enum MlViewStatus status = MLVIEW_OK ;

	g_return_val_if_fail (a_this != NULL, MLVIEW_BAD_PARAM_ERROR);
	g_return_val_if_fail (PRIVATE (a_this) != NULL, MLVIEW_BAD_PARAM_ERROR);

	status = mlview_tree_editor_get_cur_sel_start_iter
	         (PRIVATE (a_this)->tree_editor,
	          &cur_sel_start) ;
	g_return_val_if_fail (status == MLVIEW_OK, status) ;
	mlview_tree_editor_paste_node_as_sibling
	(PRIVATE (a_this)->tree_editor,
	 &cur_sel_start, FALSE) ;
	return MLVIEW_OK ;
}

enum MlViewStatus
mlview_styled_view_execute_action (MlViewIView *a_this,
                                   MlViewAction *a_action)
{
	MlViewStyledView *view = NULL ;

	g_return_val_if_fail (a_this && MLVIEW_IS_IVIEW (a_this),
	                      MLVIEW_BAD_PARAM_ERROR) ;

	view = MLVIEW_STYLED_VIEW (a_this) ;
	g_return_val_if_fail (PRIVATE (view), MLVIEW_BAD_PARAM_ERROR) ;

	if (!strcmp (a_action->name, "add-child-node-interactive")) {
		mlview_styled_view_add_child_node_interactive (view) ;
	} else if (!strcmp (a_action->name, "insert-prev-sibling-node-interactive")) {
		mlview_styled_view_insert_prev_sibling_node_interactive (view) ;
	} else if (!strcmp (a_action->name, "insert-next-sibling-node-interactive")) {
		mlview_styled_view_insert_next_sibling_node_interactive (view) ;
	} else if (!strcmp (a_action->name, "cut-node")) {
		mlview_styled_view_cut_node (view) ;
	} else if (!strcmp (a_action->name, "copy-node")) {
		mlview_styled_view_copy_node (view) ;
	} else if (!strcmp (a_action->name, "paste-node-as-child")) {
		mlview_styled_view_paste_node_as_child (view) ;
	} else if (!strcmp (a_action->name, "paste-node-as-prev-sibling")) {
		mlview_styled_view_paste_node_as_prev_sibling (view) ;
	} else if (!strcmp (a_action->name, "paste-node-as-next-sibling")) {
		mlview_styled_view_paste_node_as_next_sibling (view) ;
	}
	return MLVIEW_OK ;
}

/**
 *Connects the view to the signals of the document object model.
 *This implements the MlViewIView::connect_to_doc() method.
 *@param a_this the current instance of #MlViewStyledView
 *@param a_doc the document object model to connect to.
 *@return MLVIEW_OK upon successful completion, an error code otherwise.
 */
enum MlViewStatus
mlview_styled_view_connect_to_doc (MlViewIView *a_this,
                                   MlViewXMLDocument *a_doc)
{
	MlViewStyledView *view = NULL ;

	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_IVIEW (a_this)
	                      && MLVIEW_IS_STYLED_VIEW (a_this)
	                      && a_doc
	                      && MLVIEW_IS_XML_DOCUMENT (a_doc),
	                      MLVIEW_BAD_PARAM_ERROR) ;

	view = MLVIEW_STYLED_VIEW (a_this) ;
	g_return_val_if_fail (view
	                      && PRIVATE (view)
	                      && PRIVATE (view)->tree_editor
	                      && PRIVATE (view)->styled_editor,
	                      MLVIEW_BAD_PARAM_ERROR) ;

	if (PRIVATE (view)->connected_to_doc == TRUE)
		return MLVIEW_OK ;

	mlview_tree_editor_connect_to_doc (PRIVATE (view)->tree_editor,
	                                   a_doc) ;
	/*
	 *TODO: connect the styled editor to the document signals
	 */
	PRIVATE (view)->connected_to_doc = TRUE ;
	return MLVIEW_OK ;
}

/**
 *Disonnects the view from the signals of the document object model.
 *This implements the MlViewIView::disconnect_from_view() method.
 *@param a_this the current instance of #MlViewStyledView
 *@param a_doc the document object model to connect to.
 *@return MLVIEW_OK upon successful completion, an error code otherwise.
 */
enum MlViewStatus
mlview_styled_view_disconnect_from_doc (MlViewIView *a_this,
                                        MlViewXMLDocument *a_doc)
{

	MlViewStyledView *view = NULL ;

	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_IVIEW (a_this)
	                      && MLVIEW_IS_STYLED_VIEW (a_this)
	                      && a_doc
	                      && MLVIEW_IS_XML_DOCUMENT (a_doc),
	                      MLVIEW_BAD_PARAM_ERROR) ;


	view = MLVIEW_STYLED_VIEW (a_this) ;
	g_return_val_if_fail (view
	                      && PRIVATE (view)
	                      && PRIVATE (view)->tree_editor
	                      && PRIVATE (view)->styled_editor,
	                      MLVIEW_BAD_PARAM_ERROR) ;

	if (PRIVATE (view)->connected_to_doc == FALSE)
		return MLVIEW_OK ;

	mlview_tree_editor_disconnect_from_doc (PRIVATE (view)->tree_editor,
	                                        a_doc) ;
	/*
	 *TODO: disconnect the styled editor to the document signals
	 */
	return MLVIEW_OK ;
}


enum MlViewStatus
mlview_styled_view_update_from_css_paths_interactive (MlViewStyledView *a_this)
{
	GtkDialog *dialog = NULL ;
	gint response = 0 ;
	enum CRStatus status = CR_OK ;
	gchar *author_css=NULL, *user_css=NULL, *ua_css=NULL ;

	g_return_val_if_fail (a_this && MLVIEW_IS_STYLED_VIEW (a_this),
	                      MLVIEW_BAD_PARAM_ERROR) ;

	dialog = GTK_DIALOG (get_css_paths_picker_dialog (a_this)) ;
	g_return_val_if_fail (dialog, MLVIEW_ERROR) ;
	response = gtk_dialog_run (dialog) ;
	switch (response) {
	case GTK_RESPONSE_OK:
		author_css = g_strdup
		             (gtk_entry_get_text
		              (PRIVATE (a_this)->css_picker_author_css_path)) ;
		user_css = g_strdup
		           (gtk_entry_get_text
		            (PRIVATE (a_this)->css_picker_user_css_path)) ;
		ua_css = g_strdup
		         (gtk_entry_get_text
		          (PRIVATE (a_this)->css_picker_ua_css_path)) ;
		status = sx_box_view_update_box_model_attrs_from_css
		         (PRIVATE (a_this)->styled_editor, author_css,
		          user_css, ua_css, CR_ASCII) ;
		if (author_css) {
			g_free (author_css) ;
			author_css = NULL ;
		}
		if (user_css) {
			g_free (user_css) ;
			user_css = NULL ;
		}
		if (ua_css) {
			g_free (ua_css) ;
			ua_css = NULL ;
		}
		break ;
	case GTK_RESPONSE_CANCEL:
	default:
		break ;
	}
	gtk_widget_hide (GTK_WIDGET (dialog)) ;
	return MLVIEW_OK ;
}

enum MlViewStatus
mlview_styled_view_update_from_css_paths (MlViewStyledView *a_this,
        gchar *a_author_css_path,
        gchar *a_user_css_path,
        gchar *a_ua_css_path,
        enum CREncoding a_enc)
{
	enum CRStatus status = CR_OK ;

	g_return_val_if_fail (a_this && MLVIEW_IS_STYLED_VIEW (a_this)
	                      && PRIVATE (a_this)->styled_editor,
	                      MLVIEW_BAD_PARAM_ERROR) ;

	status = sx_box_view_update_box_model_attrs_from_css
	         (PRIVATE (a_this)->styled_editor,
	          a_author_css_path, a_user_css_path,
	          a_ua_css_path, CR_ASCII) ;

	g_return_val_if_fail (status == CR_OK, MLVIEW_ERROR) ;

	return MLVIEW_OK ;

}

/**
 *Getter of the application context associated to the
 *current instance of #MlViewStyledView
 *@param a_this the current instance of #MlViewStyledView
 *@return the application context associated to the current
 *instance of #MlViewStyledView
 */
MlViewAppContext *
mlview_styled_view_get_app_context (MlViewStyledView *a_this)
{
	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_STYLED_VIEW (a_this)
	                      && PRIVATE (a_this),
	                      NULL) ;

	return PRIVATE (a_this)->app_context ;
}

/**
 *@param a_this the current instance of #MlViewStyledView
 *@param a_app_context the instance of #MlViewAppContext associated
 *to the current instance of #MlViewStyledView
 *@return MLVIEW_OK upon successful completion, an error code
 *otherwise.
 */
enum MlViewStatus
mlview_styled_view_set_app_context (MlViewStyledView *a_this,
                                    MlViewAppContext *a_app_context)
{
	g_return_val_if_fail (a_this && MLVIEW_IS_STYLED_VIEW (a_this)
	                      && PRIVATE (a_this),
	                      MLVIEW_BAD_PARAM_ERROR) ;

	PRIVATE (a_this)->app_context = a_app_context ;
	return MLVIEW_OK ;
}


MlViewTreeEditor *
mlview_styled_view_get_tree_editor (MlViewStyledView *a_this)
{
	g_return_val_if_fail (a_this && MLVIEW_IS_STYLED_VIEW (a_this),
	                      NULL) ;

	return PRIVATE (a_this)->tree_editor ;
}

#endif /*MLVIEW_WITH_STYLE*/
