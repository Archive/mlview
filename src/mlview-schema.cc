/* -*- Mode: C++; indent-tabs-mode: nil; c-basic-offset: 4-*- */

/*This file is part of GNU MlView
 *
 *GNU MlView is free software; 
 *you can redistribute it and/or modify it under the terms of 
 *the GNU General Public License as 
 *published by the Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope 
 *that it will be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */

#include <string.h>

#include "mlview-schema.h"
#include "mlview-parsing-utils.h"
#include "mlview-utils.h"

#define PRIVATE(object) ((object)->priv)

struct _MlViewSchemaPrivate
{
	/*the url of the schema*/
	gchar *url;

	/*The refcount of the schema*/
	guint ref;

	/*the type of the underlying native schema (rng, xsd or dtd)*/
	enum MlViewSchemaType type;

	/* whether to free the native underlying schema upon destruction
	 * of the MlViewSchema
	 */
	gboolean free_native_schema ;

	/*the storage of the native schema*/
	union {
		xmlDtdPtr dtd;
		xmlRelaxNGPtr rng;
		xmlSchemaPtr xsd;
	} schema;
};

MlViewSchema *
mlview_schema_new_from_dtd (xmlDtdPtr a_dtd,
                            const gchar *a_url,
                            gboolean a_free_dtd_on_destroy)
{
	MlViewSchema *schema = NULL;

	g_return_val_if_fail (a_dtd && a_url, NULL);

	schema = (MlViewSchema *) g_try_malloc (sizeof (MlViewSchema));

	if (!schema)
		goto cleanup;

	memset (schema, 0, sizeof (MlViewSchema));

	schema->priv = (MlViewSchemaPrivate *) g_try_malloc (sizeof (MlViewSchemaPrivate));

	if (!schema->priv)
		goto cleanup;

	memset (schema->priv, 0, sizeof (MlViewSchemaPrivate));

	schema->priv->url = g_strdup (a_url);

	if (!schema->priv->url)
		goto cleanup;

	schema->priv->type = SCHEMA_TYPE_DTD;

	schema->priv->schema.dtd = a_dtd;
	schema->priv->free_native_schema =
	    a_free_dtd_on_destroy ;

	mlview_schema_ref (schema);

	return schema;

cleanup:
	if (schema) {
		if (schema->priv) {
			if (schema->priv->url) {
				g_free (schema->priv->url);
				schema->priv->url = NULL;
			}

			g_free (schema->priv);
			schema->priv = NULL;
		}

		g_free (schema);
		schema = NULL;
	}

	return NULL;
}

MlViewSchema *
mlview_schema_load_from_file (const gchar *a_url,
                              enum MlViewSchemaType a_type)
{
	MlViewSchema *schema = NULL;

	g_return_val_if_fail (a_url, NULL);

	schema = (MlViewSchema *) g_try_malloc (sizeof (MlViewSchema));

	if (!schema)
		goto cleanup;

	memset (schema, 0, sizeof (MlViewSchema));

	schema->priv = (MlViewSchemaPrivate *) g_try_malloc (sizeof (MlViewSchemaPrivate));

	if (!schema->priv)
		goto cleanup;

	memset (schema->priv, 0, sizeof (MlViewSchemaPrivate));

	schema->priv->type = a_type;

	schema->priv->url = g_strdup (a_url);

	if (!schema->priv->url)
		goto cleanup;

	switch (a_type) {
	case SCHEMA_TYPE_DTD:
		schema->priv->schema.dtd =
		    mlview_parsing_utils_load_dtd (a_url);

		if (!schema->priv->schema.dtd)
			goto cleanup;

		break;
	case SCHEMA_TYPE_RNG:
		schema->priv->schema.rng =
		    mlview_parsing_utils_load_rng (a_url);

		if (!schema->priv->schema.rng)
			goto cleanup;

		break;
	case SCHEMA_TYPE_XSD:
		schema->priv->schema.xsd =
		    mlview_parsing_utils_load_xsd (a_url) ;

		if (!schema->priv->schema.xsd)
			goto cleanup;

		break;
	default:
		g_assert_not_reached () ;
		break ;
	}

	schema->priv->free_native_schema = TRUE ;
	mlview_schema_ref (schema);

	return schema;

cleanup:
	if (schema) {
		if (schema->priv) {
			if (schema->priv->url) {
				g_free (schema->priv->url);
				schema->priv->url = NULL;
			}

			g_free (schema->priv);
			schema->priv = NULL;
		}

		g_free (schema);
		schema = NULL;
	}

	return NULL;
}

void
mlview_schema_ref (MlViewSchema *a_this)
{
	g_return_if_fail (a_this && PRIVATE (a_this));

	PRIVATE (a_this)->ref++;
}

void
mlview_schema_unref (MlViewSchema *a_this)
{
	g_return_if_fail (a_this && PRIVATE (a_this));

	PRIVATE (a_this)->ref--;
	if (PRIVATE (a_this)->ref)
		return ;
	if (PRIVATE (a_this)->free_native_schema == TRUE) {
		mlview_schema_destroy (a_this, FALSE) ;
	} else {
		mlview_schema_destroy (a_this, TRUE) ;
	}
}

void
mlview_schema_destroy (MlViewSchema *a_this,
                       gboolean a_free_native_schema)
{
	g_return_if_fail (a_this && PRIVATE (a_this)) ;


	switch (PRIVATE (a_this)->type) {
	case SCHEMA_TYPE_DTD:
		if (a_free_native_schema == TRUE
		        && PRIVATE (a_this)->schema.dtd) {
			if (PRIVATE (a_this)->free_native_schema) {
				xmlFreeDtd (PRIVATE (a_this)->schema.dtd);
			}
			PRIVATE (a_this)->schema.dtd = NULL;
		}
		break;
	case SCHEMA_TYPE_RNG:
		if (a_free_native_schema == TRUE
		        && PRIVATE (a_this)->schema.rng) {
			if (PRIVATE (a_this)->free_native_schema) {
				xmlRelaxNGFree (PRIVATE (a_this)->schema.rng);
			}
			PRIVATE (a_this)->schema.rng = NULL;
		}
		break;
	case SCHEMA_TYPE_XSD:
		if (a_free_native_schema == TRUE
		        && PRIVATE (a_this)->schema.xsd) {
			if (PRIVATE (a_this)->free_native_schema) {
				xmlSchemaFree (PRIVATE (a_this)->schema.xsd);
			}
			PRIVATE (a_this)->schema.xsd = NULL;
		}
		break;

	default:
		g_assert_not_reached () ;
		break ;
	}

	if (PRIVATE (a_this)) {
		if (PRIVATE (a_this)->url) {
			g_free (PRIVATE (a_this)->url);
			PRIVATE (a_this)->url = NULL;
		}

		g_free (PRIVATE (a_this));
		PRIVATE (a_this) = NULL;
	}
	g_free (a_this);
	a_this = NULL;
}

gchar *
mlview_schema_get_url (MlViewSchema *a_schema)
{
	g_return_val_if_fail (a_schema && a_schema->priv, NULL);
	g_return_val_if_fail (a_schema->priv->url, NULL);

	return a_schema->priv->url;
}


MlViewSchema *
mlview_schema_load_interactive (enum MlViewSchemaType a_type)
{
	GtkWidget *fs = NULL;
	gint res = 0;
	gchar *file = NULL;
	MlViewSchema *schema = NULL;

	mlview::AppContext *context = mlview::AppContext::get_instance () ;
	THROW_IF_FAIL (context) ;

	fs = GTK_WIDGET (context->get_file_chooser
			(_("Open a DTD"), mlview::MLVIEW_FILE_CHOOSER_OPEN_MODE)) ;
	g_return_val_if_fail (fs, NULL);

	res = gtk_dialog_run (GTK_DIALOG (fs)) ;
	gtk_widget_hide (fs) ;

	switch (res) {
	case GTK_RESPONSE_OK:
		file = (gchar *)gtk_file_chooser_get_filename
		       (GTK_FILE_CHOOSER (fs));
		if (file && strncmp (file, "", 1)) {
			switch (a_type) {
			case SCHEMA_TYPE_DTD:
				schema = mlview_schema_load_from_file
				         (file, SCHEMA_TYPE_DTD);
				break;
			case SCHEMA_TYPE_RNG:
				schema = mlview_schema_load_from_file
				         (file, SCHEMA_TYPE_RNG);
				break;
			case SCHEMA_TYPE_XSD:
				schema = mlview_schema_load_from_file
				         (file, SCHEMA_TYPE_XSD);
				break;
			default:
				g_assert_not_reached () ;
				break ;
			}
		}

		break;
	default:
		goto cleanup;
	}

	if (!schema) {
		context->warning ((const gchar*)_("Unable to open the selected schema."));
	}

cleanup:
	return schema;
}

enum MlViewStatus
mlview_schema_get_type (MlViewSchema *a_this,
                        enum MlViewSchemaType *a_type)
{
	g_return_val_if_fail (a_this
	                      && a_this->priv
	                      && a_type,
	                      MLVIEW_OK) ;

	*a_type = a_this->priv->type ;

	return MLVIEW_OK ;
}

enum MlViewStatus
mlview_schema_get_native_schema (MlViewSchema *a_this,
                                 gpointer *a_nativeSchema)
{
	enum MlViewStatus status = MLVIEW_OK ;

	g_return_val_if_fail (a_this && a_this->priv
	                      && a_nativeSchema,
	                      MLVIEW_BAD_PARAM_ERROR) ;

	switch (a_this->priv->type) {
	case SCHEMA_TYPE_DTD:
		*a_nativeSchema = a_this->priv->schema.dtd ;
		break ;
	case SCHEMA_TYPE_RNG:
		*a_nativeSchema = a_this->priv->schema.rng ;
		break ;
	case SCHEMA_TYPE_XSD:
		*a_nativeSchema = a_this->priv->schema.xsd ;
		break ;
	default:
		status = MLVIEW_ERROR ;
		break ;
	}
	return status ;
}
