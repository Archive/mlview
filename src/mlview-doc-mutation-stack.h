/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 8-*- */

/*
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute 
 *it and/or modify it under the terms of 
 *the GNU General Public License as published by the 
 *Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY;
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file copyright information.
 */

#include "mlview-doc-mutation.h"

#ifndef __MLVIEW_DOC_MUTATION_STACK_STACK_H__
#define __MLVIEW_DOC_MUTATION_STACK_STACK_H__

#define MLVIEW_TYPE_DOC_MUTATION_STACK (mlview_doc_mutation_stack_get_type())
#define MLVIEW_DOC_MUTATION_STACK(object) (G_TYPE_CHECK_INSTANCE_CAST((object), MLVIEW_TYPE_DOC_MUTATION_STACK, MlViewDocMutationStack))
#define MLVIEW_DOC_MUTATION_STACK_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST((klass), MLVIEW_TYPE_DOC_MUTATION_STACK, MlViewDocMutationStackClass))
#define MLVIEW_IS_DOC_MUTATION_STACK(object) (G_TYPE_CHECK_INSTANCE_TYPE((object), MLVIEW_TYPE_DOC_MUTATION_STACK))
#define MLVIEW_IS_DOC_MUTATION_STACK_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass), MLVIEW_TYPE_DOC_MUTATION_STACK))

typedef struct _MlViewDocMutationStack MlViewDocMutationStack ;
typedef struct _MlViewDocMutationStackClass MlViewDocMutationStackClass ;
typedef struct _MlViewDocMutationStackPrivate MlViewDocMutationStackPrivate ;

G_BEGIN_DECLS

struct _MlViewDocMutationStack
{
	GObject parent_object ;
	MlViewDocMutationStackPrivate *priv ;
} ;

struct _MlViewDocMutationStackClass
{
	GObjectClass parent_class ;

} ;

GType mlview_doc_mutation_stack_get_type (void) ;

MlViewDocMutationStack * mlview_doc_mutation_stack_new (void) ;

enum MlViewStatus mlview_doc_mutation_stack_push (MlViewDocMutationStack *a_this,
        MlViewDocMutation *a_mutation) ;

enum MlViewStatus mlview_doc_mutation_stack_pop (MlViewDocMutationStack *a_this,
        MlViewDocMutation **a_mutation) ;

enum MlViewStatus mlview_doc_mutation_stack_peek_nth (MlViewDocMutationStack *a_this,
        guint a_nth,
        MlViewDocMutation **a_mutation) ;

enum MlViewStatus mlview_doc_mutation_stack_peek (MlViewDocMutationStack *a_this,
        MlViewDocMutation **a_mutation) ;

enum MlViewStatus mlview_doc_mutation_stack_get_size (MlViewDocMutationStack *a_this,
        guint *a_size) ;

enum MlViewStatus mlview_doc_mutation_stack_clear (MlViewDocMutationStack *a_this) ;

G_END_DECLS

#endif
