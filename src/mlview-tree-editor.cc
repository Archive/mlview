/* -*- Mode: C++; indent-tabs-mode:true; c-basic-offset: 4-*- */
/*
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute
 *it and/or modify it under the terms of
 *the GNU General Public License as published by the
 *Free Software Foundation; either version 2,
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope that it will
 *be useful, but WITHOUT ANY WARRANTY;
 *without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the
 *GNU General Public License along with MlView;
 *see the file COPYING.
 *If not, write to the Free Software Foundation,
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */

/**
 *@file
 *The definition of the #MlViewTreeEditor widget.
 */

#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <gdk/gdkkeysyms.h>
#include <glade/glade.h>
#include "mlview-prefs.h"
#include "mlview-tree-editor.h"
#include "mlview-node-type-picker.h"
#include "mlview-marshal.h"
#include "mlview-utils.h"
#include "mlview-xml-document.h"
#include "mlview-node-type-picker.h"
#include "mlview-kb-eng.h"
#include "mlview-prefs.h"
#include "mlview-prefs-category-search.h"
#include "mlview-prefs-category-treeview.h"
#include "mlview-app-context.h"
#include "mlview-clipboard.h"
#include "mlview-ui-utils.h"

#ifdef MLVIEW_WITH_CUSTOM_CELL_RENDERER
#include "mlview-cell-renderer.h"
#include "mlview-entry.h"
#endif

using namespace mlview ;

#define PRIVATE(mlview_tree_editor) ((mlview_tree_editor)->priv)

#define SEARCH_IN_NAMES_CHECK_BUTTON "search-in-names-check-box"
#define SEARCH_IN_ATTRIBUTES_NAMES_CHECK_BUTTON "search-in-attributes-names-check-box"
#define SEARCH_IN_ATTRIBUTES_VALUES_CHECK_BUTTON "search-in-attributes-values-check-box"
#define SEARCH_IN_CONTENT_CHECK_BUTTON "search-in-content-check-box"
#define SEARCHED_TEXT_ENTRY "searched-text-entry"
#define IS_THE_FIND_DIALOG "is-the-find-dialog"
#define KEY_INPUTS_SIZE 30
#define KEY_BINDINGS_SIZE 10 ;

struct _MlViewTreeEditorPrivate
{
    MlViewXMLDocument *mlview_xml_doc;

/*the viewable tree that matches xml_doc */
    GtkTreeView *tree_view;

/* the associated GtkStyle */
    GtkStyle *style;

    GtkWidget *scrolled_win ;

/*
 *the menu poped up when the user
 *right clicks on an xml_node
 */
    GtkWidget *xml_node_popup_menu;

/*a reference to the first selected row */
    GtkTreeRowReference *cur_sel_start;

/* a reference to the previously selected xml node */
    xmlNode *previously_selected_node;

/*
 *The node type picker used when creating a new node.
 *MlViewTreeEditor does not have to handle it lifetime
 */
    MlViewNodeTypePicker *node_type_picker;

/*the search dialog*/
    GtkWidget *search_dialog ;

/*
 *a cache that contains
 *a list of xmlNode/GtkTreeRowReference
 *pair. This is to speed up searches.
 */
    GHashTable *nodes_rows_hash;
    struct MlViewKeyInput keyinputs[KEY_INPUTS_SIZE] ;
    gint last_key_input_offset ;
    MlViewKBEng *kb_eng ;
    GList *cur_node_completion_list ;
    GCompletion *completion_engine ;
    gboolean dispose_has_run ;
    gpointer backup_drag_data_delete ;
    gpointer backup_drag_data_received ;
    gboolean select_issued_by_model ;
};


/*===============================================
 *The struct defining the private attributes of
 *MlViewTreeEditor and it associated macros
 *==============================================*/

enum MlViewTreeEditorColumns  {
    /*hidden column, where the xml node is stored.*/
    XML_NODE_COLUMN = 0,
    /*
     *contains a boolean that says
     *if the column is editable or not.
     */
    IS_EDITABLE_COLUMN,
    /*the first visible column*/
    START_TAG_COLUMN,
    /*the second visible column*/
    NODE_TYPE_COLUMN,
    /*
     *This must be the last element
     *of the enum.
     */
    NB_COLUMNS
};

/**
 *The different signals emited
 *by #MlViewTreeEditor.
 */
enum {
    TREE_CHANGED = 1,
    MARK_SET_TO_NODE,
    MARK_REMOVED_FROM_NODE,
    NODE_CUT,
    NODE_PASTED,
    NODE_ADDED,
    NODE_SELECTED,
    NODE_UNSELECTED,
    UNGRAB_FOCUS_REQUESTED,
    NUMBER_OF_SIGNALS
};

enum MlViewTreeEditorColumnsOffsets {
    START_TAG_COLUMN_OFFSET,
    NODE_TYPE_COLUMN_OFFSET
} ;

static guint gv_signals[NUMBER_OF_SIGNALS] = { 0 };
static GtkVBoxClass *gv_parent_class = NULL;
static GtkTargetEntry row_targets[] =
	{
		{(gchar *)"GTK_TREE_MODEL_ROW", GTK_TARGET_SAME_WIDGET, 0}
	} ;

static struct MlViewKBDef gv_keybindings [] =
    {
	    {
		    /*One entry of the gv_key_bindings table*/
		    {
			    /*the key_inputs field of the MlViewKBDef*/
			    {GDK_less, (GdkModifierType)0}
		    },
		    /*the key_input_s_len field of the MlViewKBDef*/
		    1,
		    /*the editing action of the MlViewKBDef*/
		    (MlViewKBEngAction)mlview_tree_editor_insert_next_sibling_element_interactive,
		    "insert-next-sibling-element"
	    },

	    {
	        /*One entry of the gv_key_bindings table*/
	        {
	            /*the key_inputs field of the MlViewKBDef*/
	            {GDK_Shift_L, (GdkModifierType)0},
	            {GDK_greater, GDK_SHIFT_MASK}
	        },
	        /*the key_input_s_len field of the MlViewKBDef*/
	        2,
	        /*the editing action of the MlViewrKBDef*/
	        (MlViewKBEngAction)mlview_tree_editor_insert_prev_sibling_element_interactive,
	        "insert-prev-sibling-element"
	    },

	    {
	        /*One entry of the gv_key_bindings table*/
	        {
	            /*the key_inputs field of the MlViewBDef*/
	            {GDK_c, (GdkModifierType)0}
	        },
	        /*the key_input_s_len field of the MlViewKBDef*/
	        1,
	        /*the editing action of the MlViewKBDef*/
	        (MlViewKBEngAction)mlview_tree_editor_add_child_element_interactive,
	        "insert-child-element"
	    },

	    {
	        /*One entry of the gv_key_bindings table*/
	        {
	            /*the key_inputs field of the MlViewKBDef*/
	            {GDK_t, (GdkModifierType)0}
	        },
	        /*the key_input_s_len field of the MlViewKBDef*/
	        1,
	        /*the editing action of the MlViewKBDef*/
	        (MlViewKBEngAction)mlview_tree_editor_add_child_text_node_interactive,
	        "insert-child-text-node"
	    },
	    {
	        /*One entry of the gv_key_bindings table*/
	        {
	            /*the key_inputs field of the MlViewKBDef*/
	            {GDK_Delete, (GdkModifierType)0}
	        },
	        /*the key_input_s_len field of the MlViewKBDef*/
	        1,
	        /*the editing action of the MlViewKBDef*/
	        (MlViewKBEngAction)mlview_tree_editor_cut_cur_node,
	        "delete-node"
	    },
	    {
	        /*One entry of the gv_key_bindings table*/
	        {
	            /*the key_inputs field of the MlViewKBDef*/
	            {GDK_f, (GdkModifierType)0}
	        },
	        /*the key_input_s_len field of the MlViewKBDef*/
	        1,
	        /*the editing action of the MlViewKBDef*/
	        (MlViewKBEngAction)mlview_tree_editor_toggle_node_folding,
	        "fold-unfold-node"
	    },
	    {
	        /*One entry of the gv_key_bindings table*/
	        {
	            /*the key_inputs field of the MlViewKBDef*/
	            {GDK_o, (GdkModifierType)0}
	        },
	        /*the key_input_s_len field of the MlViewKBDef*/
	        1,
	        /*the editing action of the MlViewKBDef*/
	        (MlViewKBEngAction)mlview_tree_editor_request_ungrab_focus,
	        "request-ungrab-focus"
	    },
	    {
	        /*One entry of the gv_key_bindings table*/
	        {
	            /*the key_inputs field of the MlViewKBDef*/
	            {GDK_y, (GdkModifierType)0}
	        },
	        /*the key_input_s_len field of the MlViewKBDef*/
	        1,
	        /*the editing action of the MlViewKBDef*/
	        (MlViewKBEngAction)mlview_tree_editor_copy_current_node,
	        "copy-node"
	    },
	    {
	        /*One entry of the gv_key_bindings table*/
	        {
	            /*the key_inputs field of the MlViewKBDef*/
	            {GDK_p, (GdkModifierType)0},
	            {GDK_c, (GdkModifierType)0}
	        },
	        /*the key_input_s_len field of the MlViewKBDef*/
	        2,
	        /*the editing action of the MlViewKBDef*/
	        (MlViewKBEngAction)mlview_tree_editor_paste_node_as_child2,
	        "paste-node-as-child"
	    },
	    {
	        /*One entry of the gv_key_bindings table*/
	        {
	            /*the key_inputs field of the MlViewKBDef*/
	            {GDK_p, (GdkModifierType)0}, {GDK_n, (GdkModifierType)0}
	        },
	        /*the key_input_s_len field of the MlViewKBDef*/
	        2,
	        /*the editing action of the MlViewKBDef*/
	        (MlViewKBEngAction)mlview_tree_editor_paste_node_as_next_sibling,
	        "paste-node-as-next-sibling"
	    },
	    {
	        /*One entry of the gv_key_bindings table*/
	        {
	            /*the key_inputs field of the MlViewKBDef*/
	            {GDK_p, (GdkModifierType)0}, {GDK_p, (GdkModifierType)0}
	        },
	        /*the key_input_s_len field of the MlViewKBDef*/
	        2,
	        /*the editing action of the MlViewKBDef*/
	        (MlViewKBEngAction)mlview_tree_editor_paste_node_as_prev_sibling,
	        "paste-node-as-prev-sibling"
	    },
	    {
	        /*One entry of the gv_key_bindings table*/
	        {
	            /*the key_inputs field of the MlViewKBDef*/
	            {GDK_Escape, (GdkModifierType)0},
	            {GDK_c, (GdkModifierType)0}
	        },
	        /*the key_input_s_len field of the MlViewKBDef*/
	        2,
	        /*the editing action of the MlViewKBDef*/
	        (MlViewKBEngAction)mlview_tree_editor_comment_current_node,
	        "comment-current-node"
	    },
	    {
	        /*One entry of the gv_key_bindings table*/
	        {
	            /*the key_inputs field of the MlViewKBDef*/
	            {GDK_Down, GDK_CONTROL_MASK}
	        },
	        /*the key_input_s_len field of the MlViewKBDef*/
	        1,
	        /*the editing action of the MlViewKBDef*/
	        (MlViewKBEngAction)mlview_tree_editor_select_next_sibling_node,
	        "select-next-sibling-node"
	    },
	    {
	        /*One entry of the gv_key_bindings table*/
	        {
	            /*the key_inputs field of the MlViewKBDef*/
	            {GDK_Up, GDK_CONTROL_MASK}
	        },
	        /*the key_input_s_len field of the MlViewKBDef*/
	        1,
	        /*the editing action of the MlViewKBDef*/
	        (MlViewKBEngAction)mlview_tree_editor_select_prev_sibling_node,
	        "select-prev-sibling-node"
	    },
	    {
	        /*One entry of the gv_key_bindings table*/
	        {
	            /*the key_inputs field of the MlViewKBDef*/
	            {GDK_Escape, (GdkModifierType)0},
	            {GDK_Up, (GdkModifierType)0}
	        },
	        /*the key_input_s_len field of the MlViewKBDef*/
	        2,
	        /*the editing action of the MlViewKBDef*/
	        (MlViewKBEngAction)mlview_tree_editor_select_parent_node,
	        "select-parent-node"
	    }
    } ;

static void xml_doc_prev_sibling_node_inserted_cb (MlViewXMLDocument *a_this,
        xmlNode *a_sibling_node,
        xmlNode *a_inserted_node,
        MlViewTreeEditor *a_editor) ;

static void xml_doc_next_sibling_node_inserted_cb (MlViewXMLDocument *a_this,
        xmlNode *a_sibling_node,
        xmlNode *a_inserted_node,
        MlViewTreeEditor *a_editor) ;

static void xml_doc_child_node_added_cb (MlViewXMLDocument *a_this,
        xmlNode *a_parent_node,
        xmlNode *a_added_node,
        MlViewTreeEditor *a_editor) ;

static void xml_doc_selected_node_cb (MlViewXMLDocument *a_doc,
                                      xmlNode *a_node,
                                      MlViewTreeEditor *a_editor) ;


static void xml_doc_node_commented_cb (MlViewXMLDocument *a_this,
                                       xmlNode *a_node,
                                       xmlNode *a_commented_node,
                                       MlViewTreeEditor *a_tree_editor) ;

static gboolean button_press_event_cb (GtkWidget * a_widget,
                                       GdkEventButton * a_event,
                                       gpointer a_user_data) ;

static void xml_doc_document_undo_state_changed_cb (MlViewXMLDocument *a_doc,
        gpointer a_tree_editor) ;

static gboolean key_press_event_cb (GtkWidget *a_widget,
                                    GdkEvent *a_event,
                                    gpointer a_user_data) ;


static enum MlViewStatus set_our_dnd_callbacks (MlViewTreeEditor *a_this) ;

static void node_cell_edited_cb (GtkCellRendererText *a_renderer,
                                 gchar *a_cell_path,
                                 gchar *a_new_text,
                                 gpointer a_data) ;

#ifdef MLVIEW_WITH_CUSTOM_CELL_RENDERER
static void word_changed_cb (MlViewCellRenderer *a_cell_renderer,
                             GtkEditable *a_editable,
                             const gchar *a_word_start,
                             const gchar *a_word_end,
                             gboolean a_char_added,
                             gint a_position,
                             gint a_word_start_position,
                             gint a_word_end_postion,
                             gpointer a_user_data) ;

static void editing_has_started_cb (MlViewCellRenderer *a_renderer,
                                    GtkTreePath *a_path,
                                    GtkEditable *a_editable,
                                    gpointer a_user_data) ;

static gboolean select_editable_region_cb (MlViewCellRenderer *a_cell_renderer,
        MlViewEntry *a_editable,
        gpointer a_user_data) ;
#endif


static enum MlViewStatus get_search_config (GtkWidget *a_search_dialog,
        struct SearchConfig *a_config) ;

static GtkTreeView *build_tree_view_from_xml_doc (MlViewTreeEditor * a_this,
        xmlDoc * a_doc);

static enum MlViewStatus update_visual_node (MlViewTreeEditor *a_this,
					     GtkTreeIter *a_iter,
					     gboolean selected);

static enum MlViewStatus build_tree_model_from_xml_tree
			(MlViewTreeEditor * a_this,
			 const xmlNode * a_node,
			 GtkTreeIter * a_ref_iter,
			 enum MlViewTreeInsertType a_type,
			 GtkTreeModel ** a_model);

static gboolean idle_add_grab_focus_on_tree_view (MlViewTreeEditor *a_this) ;

static gboolean idle_add_scroll_to_cell (MlViewTreeEditor *a_this) ;

static gboolean foreach_remove_func (gpointer a_key, gpointer a_value) ;

static enum MlViewStatus clear (MlViewTreeEditor *a_this) ;

static enum MlViewStatus reload_from_doc (MlViewTreeEditor *a_this) ;

static Clipboard* get_clipboard_from_app_context () ;

static xmlNode* get_node_from_clipboard (MlViewXMLDocument *a_doc) ;

static void put_node_to_clipboard (xmlNode *a_node) ;

/***************************************************
 *private methods required by the GTK typing system
 ***************************************************/

/**
 *The dispose method is suppose to unref all the external
 *reference this object may hold, and free all the members
 *(if applicable) of this object.
 */
static void
mlview_tree_editor_dispose (GObject *a_this)
{
	MlViewTreeEditor *ed=NULL ;

	THROW_IF_FAIL (a_this && MLVIEW_IS_TREE_EDITOR (a_this)) ;
	ed = MLVIEW_TREE_EDITOR (a_this) ;
	THROW_IF_FAIL (ed && PRIVATE (ed)) ;

	if (PRIVATE (ed)->dispose_has_run == TRUE) {
		return ;
	}

	if (PRIVATE (ed)->mlview_xml_doc) {
		mlview_tree_editor_disconnect_from_doc
		(ed, PRIVATE (ed)->mlview_xml_doc) ;
	}

	if (PRIVATE (ed)->node_type_picker) {
		gtk_widget_destroy (GTK_WIDGET
		                    (PRIVATE (ed)->node_type_picker));
		PRIVATE (ed)->node_type_picker = NULL ;
	}
	if (PRIVATE (ed)->search_dialog) {
		gtk_widget_destroy
		(GTK_WIDGET (PRIVATE (ed)->search_dialog)) ;
		PRIVATE (ed)->search_dialog = NULL ;
	}
	g_idle_remove_by_data (ed) ;

	PRIVATE (ed)->dispose_has_run = TRUE ;
	if (gv_parent_class
	        && G_OBJECT_CLASS (gv_parent_class)->dispose) {
		G_OBJECT_CLASS (gv_parent_class)->dispose (a_this) ;
	}
}

/**
 *Instane finalyzer, responsible of freeing the instance's memory.
 *Is called at the end of the object's destruction process.
 *@param a_this the current instance of #MlViewTreeEditor
 */
static void
mlview_tree_editor_finalize (GObject *a_this)
{
    MlViewTreeEditor *ed=NULL ;

    THROW_IF_FAIL (a_this
		   && MLVIEW_IS_TREE_EDITOR (a_this)) ;
    ed = MLVIEW_TREE_EDITOR (a_this) ;
    THROW_IF_FAIL (ed && PRIVATE (ed)) ;
    g_free (PRIVATE (ed)) ;
    PRIVATE (ed) = NULL ;
    if (gv_parent_class
	&& G_OBJECT_CLASS (gv_parent_class)->finalize) {
	G_OBJECT_CLASS (gv_parent_class)->finalize (a_this) ;
    }
}

/**
 *The Gtk standard class initialyzer of the
 *MlViewTreeEditorClass class.
 *@param a_klass
 */
static void
mlview_tree_editor_class_init (MlViewTreeEditorClass * a_klass)
{
    GObjectClass *gobject_class = NULL ;

    THROW_IF_FAIL (a_klass != NULL);

    gv_parent_class = (GtkVBoxClass*) g_type_class_peek_parent (a_klass);
    THROW_IF_FAIL (gv_parent_class) ;
    gobject_class = G_OBJECT_CLASS (a_klass);
    THROW_IF_FAIL (gobject_class);
    gobject_class->dispose = mlview_tree_editor_dispose  ;
    gobject_class->finalize = mlview_tree_editor_finalize ;
/*object_class->destroy = NULL ;mlview_tree_editor_destroy;*/

    gv_signals[TREE_CHANGED] =
	g_signal_new ("tree-changed",
		      G_TYPE_FROM_CLASS (gobject_class),
		      G_SIGNAL_RUN_FIRST,
		      G_STRUCT_OFFSET
		      (MlViewTreeEditorClass,
		       tree_changed), NULL, NULL,
		      mlview_marshal_VOID__VOID,
		      G_TYPE_NONE, 0, NULL);

    gv_signals[NODE_CUT] =
	g_signal_new ("node-cut",
		      G_TYPE_FROM_CLASS (gobject_class),
		      G_SIGNAL_RUN_FIRST,
		      G_STRUCT_OFFSET
		      (MlViewTreeEditorClass, node_cut),
		      NULL, NULL,
		      mlview_marshal_VOID__POINTER,
		      G_TYPE_NONE, 1, G_TYPE_POINTER);

    gv_signals[NODE_PASTED] =
	g_signal_new ("node-pasted",
		      G_TYPE_FROM_CLASS (gobject_class),
		      G_SIGNAL_RUN_FIRST,
		      G_STRUCT_OFFSET
		      (MlViewTreeEditorClass,
		       node_pasted), NULL, NULL,
		      gtk_marshal_VOID__POINTER,
		      G_TYPE_NONE, 1, G_TYPE_POINTER);

    gv_signals[NODE_ADDED] =
	g_signal_new ("node-added",
		      G_TYPE_FROM_CLASS (gobject_class),
		      G_SIGNAL_RUN_FIRST,
		      G_STRUCT_OFFSET
		      (MlViewTreeEditorClass,
		       node_added), NULL, NULL,
		      mlview_marshal_VOID__POINTER,
		      G_TYPE_NONE, 1, G_TYPE_POINTER);

    gv_signals[NODE_SELECTED] =
	g_signal_new ("node-selected",
		      G_TYPE_FROM_CLASS (gobject_class),
		      G_SIGNAL_RUN_FIRST,
		      G_STRUCT_OFFSET
		      (MlViewTreeEditorClass,
		       node_selected), NULL, NULL,
		      mlview_marshal_VOID__POINTER,
		      G_TYPE_NONE, 1, G_TYPE_POINTER);

    gv_signals[UNGRAB_FOCUS_REQUESTED] =
	g_signal_new ("ungrab_focus_requested",
		      G_TYPE_FROM_CLASS (gobject_class),
		      G_SIGNAL_RUN_FIRST,
		      G_STRUCT_OFFSET
		      (MlViewTreeEditorClass,
		       ungrab_focus_requested), NULL, NULL,
		      mlview_marshal_VOID__VOID,
		      G_TYPE_NONE, 0, NULL);

    a_klass->tree_changed = NULL;
    a_klass->node_cut = NULL;
    a_klass->node_added = NULL;
    a_klass->node_pasted = NULL;
    a_klass->node_selected = NULL;

    a_klass->build_tree_view_from_xml_doc = build_tree_view_from_xml_doc;
    a_klass->update_visual_node = update_visual_node;
    a_klass->build_tree_model_from_xml_tree = build_tree_model_from_xml_tree;
    a_klass->clear = clear ;
    a_klass->reload_from_doc = reload_from_doc ;
}

static void
mlview_tree_editor_prefs_font_changed_cb (MlViewTreeEditor *a_this)
{
    mlview::PrefsCategoryTreeview *m_prefs =
	dynamic_cast<mlview::PrefsCategoryTreeview*> (
	    mlview::Preferences::get_instance ()
	    ->get_category_by_id ("treeview"));

    if (m_prefs == NULL)
	return;

    const char* fontname = const_cast<char*> (
	m_prefs->get_font_name ().c_str ());

    if (fontname == NULL)
	return;

    PangoFontDescription *font_desc =
	pango_font_description_from_string (fontname);

    if (font_desc != NULL) {
	gtk_widget_modify_font (GTK_WIDGET (PRIVATE (a_this)->tree_view),
				font_desc);
	pango_font_description_free (font_desc);
    }
}

static void
mlview_tree_editor_repaint_tree (MlViewTreeEditor *a_this, xmlNode *a_node)
{
    if (a_node == NULL)
	return;

    xmlNode *cur_node = NULL;

    for (cur_node = a_node; cur_node; cur_node = cur_node->next) {
	mlview_tree_editor_update_visual_node2 (a_this,
						cur_node,
						FALSE);
	mlview_tree_editor_repaint_tree (a_this, cur_node->children);
    }
}

static void
mlview_tree_editor_prefs_colour_changed_cb (MlViewTreeEditor *a_this)
{
    mlview::PrefsCategoryTreeview *m_prefs =
	dynamic_cast<mlview::PrefsCategoryTreeview*> (
	    mlview::Preferences::get_instance ()
	    ->get_category_by_id ("treeview"));

    if (m_prefs == NULL)
	return;

    MlViewXMLDocument *document = PRIVATE (a_this)->mlview_xml_doc;
    THROW_IF_FAIL (document);

    xmlDocPtr docptr = mlview_xml_document_get_native_document (document);
    THROW_IF_FAIL (docptr);

    xmlNodePtr root_node = xmlDocGetRootElement (docptr);
    THROW_IF_FAIL (root_node);

    mlview_tree_editor_repaint_tree (a_this, root_node);
}

/**
 *The instance initialyzer of the MlViewTreeEditor.
 */
static void
mlview_tree_editor_init (MlViewTreeEditor * a_editor)
{
    THROW_IF_FAIL (a_editor != NULL);
    THROW_IF_FAIL (PRIVATE (a_editor) == NULL);

    PRIVATE (a_editor) =
	(MlViewTreeEditorPrivate*)g_try_malloc
	(sizeof (MlViewTreeEditorPrivate));

    if (!PRIVATE (a_editor)) {
	mlview_utils_trace_debug
	    ("malloc failed, system may be out of memory");
	return;
    }
    memset (PRIVATE (a_editor),
	    0,
	    sizeof (MlViewTreeEditorPrivate));

    mlview::PrefsCategoryTreeview *m_prefs =
	dynamic_cast<mlview::PrefsCategoryTreeview*> (
	    mlview::Preferences::get_instance ()
	    ->get_category_by_id ("treeview"));

    if (m_prefs != NULL)
    {
	m_prefs->signal_font_changed ().connect
	    (sigc::bind (
		sigc::ptr_fun (&mlview_tree_editor_prefs_font_changed_cb),
		a_editor));

	m_prefs->signal_colour_changed ().connect
	    (sigc::bind (
		sigc::ptr_fun (&mlview_tree_editor_prefs_colour_changed_cb),
		a_editor));
    }
}

/*******************************
 *Private methods and callbacks.
 *******************************/
static void
put_node_to_clipboard (xmlNode *a_node)
{
	Clipboard *cb = get_clipboard_from_app_context () ;
	THROW_IF_FAIL (cb) ;
	cb->put (a_node) ;
}

static xmlNode*
get_node_from_clipboard (MlViewXMLDocument *a_doc)
{
	THROW_IF_FAIL (a_doc) ;
	THROW_IF_FAIL (MLVIEW_IS_XML_DOCUMENT (a_doc)) ;

	Clipboard *cb = get_clipboard_from_app_context () ;
	THROW_IF_FAIL (cb) ;
	xmlNode * node = cb->get (a_doc) ;
	return node ;
}

static Clipboard*
get_clipboard_from_app_context ()
{
	AppContext *context = AppContext::get_instance () ;
	THROW_IF_FAIL (context) ;
	Object *object = context->get_clipboard () ;
	THROW_IF_FAIL (object) ;
	Clipboard *cb = dynamic_cast<Clipboard*> (object) ;
	THROW_IF_FAIL (cb) ;
	return cb ;
}

static enum MlViewStatus
clear (MlViewTreeEditor *a_this)
{
	THROW_IF_FAIL (a_this && MLVIEW_IS_TREE_EDITOR (a_this)) ;
	THROW_IF_FAIL (PRIVATE (a_this)) ;

	if (PRIVATE (a_this)->mlview_xml_doc) {
		mlview_tree_editor_disconnect_from_doc
			(a_this, PRIVATE (a_this)->mlview_xml_doc) ;
	}
	if (PRIVATE (a_this)->tree_view) {
		gtk_widget_destroy (GTK_WIDGET (PRIVATE (a_this)->tree_view)) ;
		PRIVATE (a_this)->tree_view = NULL ;
	}
	if (PRIVATE (a_this)->scrolled_win) {
		gtk_widget_destroy (GTK_WIDGET (PRIVATE (a_this)->scrolled_win)) ;
		PRIVATE (a_this)->scrolled_win = NULL ;
	}
	if (PRIVATE (a_this)->nodes_rows_hash) {
		g_hash_table_foreach_remove
							(PRIVATE (a_this)->nodes_rows_hash,
							 (GHRFunc)foreach_remove_func,
							 NULL) ;
	}
	if (PRIVATE (a_this)->cur_sel_start) {
		PRIVATE (a_this)->cur_sel_start = NULL ;
	}
	return MLVIEW_OK ;
}

static enum MlViewStatus
reload_from_doc (MlViewTreeEditor *a_this)
{
	enum MlViewStatus status = MLVIEW_OK ;

	THROW_IF_FAIL (a_this && MLVIEW_IS_TREE_EDITOR (a_this)) ;
	THROW_IF_FAIL (PRIVATE (a_this)) ;

	status = clear (a_this) ;
	if (status != MLVIEW_OK)
		return status ;
	if (PRIVATE (a_this)->mlview_xml_doc) {
		status = mlview_tree_editor_edit_xml_doc
					(a_this, PRIVATE (a_this)->mlview_xml_doc) ;
		mlview_tree_editor_connect_to_doc (a_this,
										   PRIVATE (a_this)->mlview_xml_doc) ;
	}
	return status ;
}

static gboolean
foreach_remove_func (gpointer a_key, gpointer a_value)
{
	return true ;
}

static gboolean
start_editing_node_in_idle_time (MlViewTreeEditor *a_this)
{
	xmlNode *new_node = NULL ;

	g_return_val_if_fail (a_this && MLVIEW_IS_TREE_EDITOR (a_this),
	                      FALSE) ;
	new_node = (xmlNode*) g_object_get_data (G_OBJECT (a_this), "new-node") ;
	mlview_tree_editor_start_editing_node (a_this, new_node) ;
	return FALSE ;
}

static void
nodeset_selected_cb (GtkTreeSelection * a_sel, gpointer * a_data)
{
	guint nb_row_selected = 0;
	GtkTreeView *tree_view = NULL;
	MlViewTreeEditor *tree_ed = NULL;
	GtkTreeRowReference *row_ref = NULL;
	GtkTreeModel *model = NULL;
	GList *row_sel = NULL;
	GtkTreeIter iter={0} ;
	gboolean is_ok=FALSE ;
	xmlNode *cur_node = NULL;

	THROW_IF_FAIL (a_sel && GTK_IS_TREE_SELECTION (a_sel));
	THROW_IF_FAIL (a_data && MLVIEW_IS_TREE_EDITOR (a_data));

	tree_ed = MLVIEW_TREE_EDITOR (a_data);
	tree_view = gtk_tree_selection_get_tree_view (a_sel);
	THROW_IF_FAIL (tree_view);
	model = gtk_tree_view_get_model (tree_view);
	THROW_IF_FAIL (model);
	nb_row_selected = gtk_tree_selection_count_selected_rows
	                  (a_sel);
	/*we just support one row selected at a time now */
	THROW_IF_FAIL (nb_row_selected <= 1);
	if (nb_row_selected == 0) {
		/*no node is selected anymore*/
		PRIVATE (tree_ed)->cur_sel_start = NULL ;
		return ;
	}
	/*
	 *Now, lets get an iterator on the row selected
	 */
	row_sel = gtk_tree_selection_get_selected_rows
	          (a_sel, &model);
	THROW_IF_FAIL (row_sel && row_sel->data);
	is_ok = gtk_tree_model_get_iter
	        (model, &iter, (GtkTreePath*)row_sel->data) ;
	THROW_IF_FAIL (is_ok == TRUE) ;
	row_ref = mlview_tree_editor_iter_2_row_ref
	          (tree_ed, &iter) ;
	THROW_IF_FAIL (row_ref) ;
	PRIVATE (tree_ed)->cur_sel_start = row_ref ;
	cur_node = mlview_tree_editor_get_xml_node
	           (tree_ed, &iter) ;
	THROW_IF_FAIL (cur_node) ;

	if (PRIVATE (tree_ed)->previously_selected_node != NULL) {
		mlview_tree_editor_update_visual_node2
		(tree_ed,
		 PRIVATE (tree_ed)->previously_selected_node, FALSE);
	}

	PRIVATE (tree_ed)->previously_selected_node = cur_node;
	mlview_tree_editor_update_visual_node2 (tree_ed, cur_node, TRUE) ;
	if (PRIVATE (tree_ed)->select_issued_by_model == TRUE) {
		PRIVATE(tree_ed)->select_issued_by_model = FALSE ;
	} else {
		mlview_tree_editor_select_node (tree_ed, cur_node,
		                                FALSE, TRUE) ;
	}
	g_signal_emit (G_OBJECT (tree_ed),
	               gv_signals[NODE_SELECTED], 0, row_ref) ;

	g_list_foreach (row_sel, (GFunc)gtk_tree_path_free, NULL) ;
	g_list_free (row_sel) ;
}

static void
xml_doc_node_cut_cb (MlViewXMLDocument *a_this,
                     xmlNode *a_parent_node,
                     xmlNode *a_cut_node,
                     MlViewTreeEditor *a_editor)
{
	THROW_IF_FAIL (a_this
	                  && MLVIEW_IS_XML_DOCUMENT (a_this)
	                  && MLVIEW_IS_TREE_EDITOR (a_editor)) ;

	mlview_tree_editor_update_node_cut (a_editor, a_parent_node,
	                                    a_cut_node) ;
}

static void
xml_doc_prev_sibling_node_inserted_cb (MlViewXMLDocument *a_this,
                                       xmlNode *a_sibling_node,
                                       xmlNode *a_inserted_node,
                                       MlViewTreeEditor *a_editor)
{
	THROW_IF_FAIL (a_this
	                  && MLVIEW_IS_XML_DOCUMENT (a_this)
	                  && MLVIEW_IS_TREE_EDITOR (a_editor)) ;

	mlview_tree_editor_update_sibling_node_inserted (a_editor, a_sibling_node,
	        a_inserted_node,
	        TRUE, TRUE) ;
}

static void
xml_doc_next_sibling_node_inserted_cb (MlViewXMLDocument *a_this,
                                       xmlNode *a_sibling_node,
                                       xmlNode *a_inserted_node,
                                       MlViewTreeEditor *a_editor)
{
	THROW_IF_FAIL (a_this
	                  && MLVIEW_IS_XML_DOCUMENT (a_this)
	                  && MLVIEW_IS_TREE_EDITOR (a_editor)) ;

	mlview_tree_editor_update_sibling_node_inserted (a_editor, a_sibling_node,
	        a_inserted_node,
	        FALSE, TRUE) ;
}

static void
xml_doc_child_node_added_cb (MlViewXMLDocument *a_this,
                             xmlNode *a_parent_node,
                             xmlNode *a_added_node,
                             MlViewTreeEditor *a_editor)
{
	THROW_IF_FAIL (a_this
	                  && MLVIEW_IS_XML_DOCUMENT (a_this)
	                  && MLVIEW_IS_TREE_EDITOR (a_editor)) ;

	mlview_tree_editor_update_child_node_added (a_editor, a_parent_node,
	        a_added_node, TRUE) ;
}

static void
xml_doc_internal_subset_node_added_cb (MlViewXMLDocument *a_this,
				       xmlDtd *a_internal_subset,
				       MlViewTreeEditor *a_editor)
{
	THROW_IF_FAIL (a_this && MLVIEW_IS_XML_DOCUMENT (a_this)
	                  && a_internal_subset && a_editor
	                  && MLVIEW_IS_TREE_EDITOR (a_editor)) ;

	mlview_tree_editor_update_internal_subset_added
	(a_editor,
	 a_internal_subset) ;
}

static void
xml_doc_dtd_node_changed_cb (MlViewXMLDocument *a_this,
			     xmlDtd *a_dtd_node,
			     MlViewTreeEditor *a_editor)
{
	THROW_IF_FAIL (a_this
	                  && MLVIEW_IS_XML_DOCUMENT (a_this)
	                  && PRIVATE (a_this)
	                  && a_dtd_node && a_editor) ;

	mlview_tree_editor_update_visual_node2
	(a_editor,
	 (xmlNode*)a_dtd_node,
	 FALSE) ;
}

static void
xml_doc_content_changed_cb (MlViewXMLDocument *a_this,
                            xmlNode *a_node,
                            MlViewTreeEditor *a_editor)
{
	THROW_IF_FAIL (a_this
	                  && MLVIEW_IS_XML_DOCUMENT (a_this)
	                  && a_editor
	                  && MLVIEW_IS_TREE_EDITOR (a_editor)) ;

	mlview_tree_editor_update_visual_node2 (a_editor,
	                                        a_node, FALSE) ;
}

static void
xml_doc_name_changed_cb (MlViewXMLDocument *a_this,
                         xmlNode *a_node,
                         MlViewTreeEditor *a_editor)
{
	THROW_IF_FAIL (a_this
	                  && MLVIEW_IS_XML_DOCUMENT (a_this)
	                  && a_editor
	                  && MLVIEW_IS_TREE_EDITOR (a_editor)) ;

	mlview_tree_editor_update_visual_node2 (a_editor, a_node, FALSE) ;
}

static void
xml_doc_file_path_changed_cb (MlViewXMLDocument *a_this,
                              MlViewTreeEditor *a_editor)
{
	mlview::AppContext *app_context = mlview::AppContext::get_instance ();
	THROW_IF_FAIL (app_context) ;

	THROW_IF_FAIL (a_this && MLVIEW_IS_XML_DOCUMENT (a_this)
				   && a_editor && MLVIEW_IS_TREE_EDITOR (a_editor)) ;
	app_context->notify_document_name_changed (a_this) ;
}

static void
xml_doc_document_reloaded_cb (MlViewXMLDocument *a_this,
							  gpointer a_user_data)
{
	MlViewTreeEditor *editor = NULL ;
	THROW_IF_FAIL (a_this && MLVIEW_IS_XML_DOCUMENT (a_this)) ;
	THROW_IF_FAIL (a_user_data && MLVIEW_IS_TREE_EDITOR (a_user_data)) ;
	editor = MLVIEW_TREE_EDITOR (a_user_data) ;
	THROW_IF_FAIL (editor) ;

	mlview_tree_editor_reload_from_doc (editor) ;
}

static void
xml_doc_document_changed_cb (MlViewXMLDocument *a_this,
                             MlViewTreeEditor *a_editor)
{
	THROW_IF_FAIL (a_this && MLVIEW_IS_XML_DOCUMENT (a_this)
	                  && PRIVATE (a_this)
	                  && a_editor && MLVIEW_IS_TREE_EDITOR (a_editor)
	                  && PRIVATE (a_editor)) ;

	mlview_tree_editor_set_to_modified (a_editor, TRUE) ;
}

static void
xml_doc_document_undo_state_changed_cb (MlViewXMLDocument *a_doc,
                                        gpointer a_tree_editor)
{
	MlViewTreeEditor *thiz = NULL ;

	THROW_IF_FAIL (a_doc && MLVIEW_IS_XML_DOCUMENT (a_doc) && PRIVATE (a_doc)) ;

	thiz = MLVIEW_TREE_EDITOR (a_tree_editor) ;
	THROW_IF_FAIL (thiz) ;

	mlview::AppContext *context = mlview::AppContext::get_instance () ;
	THROW_IF_FAIL (context) ;
	context->notify_view_undo_state_changed () ;
}

static void
xml_doc_node_attribute_name_changed_cb (MlViewXMLDocument *a_this,
					xmlAttr *a_attr,
					MlViewTreeEditor *a_editor)
{
	THROW_IF_FAIL (a_this
	                  && MLVIEW_XML_DOCUMENT (a_this)
	                  && a_editor) ;

	if (!a_attr)
		return ;
	THROW_IF_FAIL (a_attr->parent) ;
	mlview_tree_editor_update_visual_node2
	(a_editor, a_attr->parent, FALSE) ;
}

static void
xml_doc_node_attribute_value_changed_cb (MlViewXMLDocument *a_this,
        xmlAttr *a_attr,
        MlViewTreeEditor *a_editor)
{
	THROW_IF_FAIL (a_this
	                  && MLVIEW_IS_XML_DOCUMENT (a_this)
	                  && a_attr
	                  && MLVIEW_IS_TREE_EDITOR (a_editor)) ;

	mlview_tree_editor_update_visual_node2 (a_editor,
	                                        a_attr->parent, FALSE) ;
}

static void
xml_doc_node_attribute_removed_cb (MlViewXMLDocument *a_this,
                                   xmlNode *a_node,
                                   xmlChar *a_name,
                                   MlViewTreeEditor *a_editor)
{
	THROW_IF_FAIL (a_this
	                  && MLVIEW_IS_XML_DOCUMENT (a_this)
	                  && MLVIEW_IS_TREE_EDITOR (a_editor)) ;

	mlview_tree_editor_update_visual_node2 (a_editor, a_node, FALSE) ;
}

static void
xml_doc_node_namespace_added_cb (MlViewXMLDocument *a_this,
                                 xmlNode *a_node,
                                 xmlNs *a_ns,
                                 MlViewTreeEditor *a_editor)
{
	THROW_IF_FAIL (a_this
	                  && a_editor
	                  && a_node
	                  && MLVIEW_IS_XML_DOCUMENT (a_this)
	                  && MLVIEW_IS_TREE_EDITOR (a_editor)) ;

	mlview_tree_editor_update_visual_node2 (a_editor, a_node, FALSE) ;
}

static void
xml_doc_node_namespace_changed_cb (MlViewXMLDocument *a_this,
                                   xmlNode *a_node,
                                   xmlNs *a_ns,
                                   MlViewTreeEditor *a_editor)
{
	THROW_IF_FAIL (a_this
	                  && MLVIEW_IS_XML_DOCUMENT (a_this)
	                  && MLVIEW_IS_TREE_EDITOR (a_editor)) ;

	mlview_tree_editor_update_visual_node2 (a_editor, a_node, FALSE);
}

static void
xml_doc_node_namespace_removed_cb (MlViewXMLDocument *a_this,
                                   xmlNode *a_node,
                                   xmlNs *a_ns,
                                   MlViewTreeEditor *a_editor)
{
	THROW_IF_FAIL (a_this
	                  && MLVIEW_IS_XML_DOCUMENT (a_this)
	                  && MLVIEW_IS_TREE_EDITOR (a_editor)) ;

	mlview_tree_editor_update_visual_node2 (a_editor, a_node, FALSE) ;
}

static void
xml_doc_searched_node_found_cb (MlViewXMLDocument *a_this,
				xmlNode *a_node_found,
				MlViewTreeEditor *a_editor)
{
	THROW_IF_FAIL (a_this
	                  && MLVIEW_IS_XML_DOCUMENT (a_this)
	                  && MLVIEW_IS_TREE_EDITOR (a_editor)
	                  && a_node_found) ;

	mlview_tree_editor_select_node (a_editor, a_node_found,
	                                TRUE, FALSE) ;
}

static void
xml_doc_selected_node_cb (MlViewXMLDocument *a_doc,
			  xmlNode *a_node,
			  MlViewTreeEditor *a_editor)
{
	THROW_IF_FAIL (a_doc && MLVIEW_XML_DOCUMENT (a_doc)
	                  && a_node && a_editor
	                  && MLVIEW_IS_TREE_EDITOR (a_editor)
	                  && PRIVATE (a_editor)) ;

	mlview_tree_editor_select_node (a_editor, a_node, TRUE,
	                                FALSE) ;
}

static void
entity_node_public_id_changed_cb (MlViewXMLDocument *a_doc,
				  xmlEntity *a_entity,
				  MlViewTreeEditor *a_editor)
{
	THROW_IF_FAIL (a_doc
	                  && MLVIEW_XML_DOCUMENT (a_doc)
	                  && a_editor
	                  && MLVIEW_IS_TREE_EDITOR (a_editor)) ;

	mlview_tree_editor_update_visual_node2
	(a_editor, (xmlNode *)a_entity, FALSE) ;
}

static void
entity_node_system_id_changed_cb (MlViewXMLDocument *a_doc,
				  xmlEntity *a_entity,
				  MlViewTreeEditor *a_editor)
{
	THROW_IF_FAIL (a_doc
	                  && MLVIEW_XML_DOCUMENT (a_doc)
	                  && a_editor
	                  && MLVIEW_IS_TREE_EDITOR (a_editor)) ;

	mlview_tree_editor_update_visual_node2
	(a_editor,
	 (xmlNode *)a_entity,
	 FALSE) ;
}

static void
entity_node_content_changed_cb (MlViewXMLDocument *a_doc,
				xmlEntity *a_entity,
				MlViewTreeEditor *a_editor)
{
	THROW_IF_FAIL (a_doc
	                  && MLVIEW_XML_DOCUMENT (a_doc)
	                  && a_editor
	                  && MLVIEW_IS_TREE_EDITOR (a_editor)) ;

	mlview_tree_editor_update_visual_node2
	(a_editor, (xmlNode *)a_entity, FALSE) ;
}

static void
xml_doc_node_commented_cb (MlViewXMLDocument *a_this,
                           xmlNode *a_node,
                           xmlNode *a_commented_node,
                           MlViewTreeEditor *a_tree_editor)
{
	THROW_IF_FAIL (a_this
	                  && MLVIEW_IS_XML_DOCUMENT (a_this)) ;
	THROW_IF_FAIL (a_tree_editor
	                  && MLVIEW_IS_TREE_EDITOR (a_tree_editor)) ;
	THROW_IF_FAIL (a_node) ;

	mlview_tree_editor_update_node_commented (a_tree_editor,
	        a_node,
	        a_commented_node) ;
}


static void
search_win_cancel_button_clicked_cb (GtkButton *a_this,
                                     MlViewTreeEditor *a_editor)
{
	THROW_IF_FAIL (a_this && GTK_IS_BUTTON (a_this)) ;
	THROW_IF_FAIL (a_editor
	                  && MLVIEW_IS_TREE_EDITOR (a_editor)
	                  && PRIVATE (a_editor)
	                  && PRIVATE (a_editor)->search_dialog) ;

	gtk_widget_hide (PRIVATE (a_editor)->search_dialog) ;
}

static void
do_search_node (MlViewTreeEditor *a_this,
                gboolean a_downward,
                xmlNode **a_node_found)
{
	enum MlViewStatus status = MLVIEW_OK ;
	struct SearchConfig search_config  ;

	THROW_IF_FAIL (a_this
	                  && MLVIEW_IS_TREE_EDITOR (a_this)
	                  && PRIVATE (a_this)
	                  && PRIVATE (a_this)->search_dialog) ;

	if (!GTK_WIDGET_VISIBLE (PRIVATE (a_this)->search_dialog))
		return ;

	memset ((char*)&search_config, 0, sizeof (struct SearchConfig)) ;

	status = get_search_config
	         (PRIVATE (a_this)->search_dialog,
	          &search_config) ;

	THROW_IF_FAIL (status == MLVIEW_OK) ;

	search_config.downward = a_downward ;

	mlview_tree_editor_search (a_this,
	                           PRIVATE (a_this)->cur_sel_start,
	                           &search_config,
	                           a_node_found) ;
}

static void
search_win_prev_button_clicked_cb (GtkButton *a_this,
                                   MlViewTreeEditor *a_editor)
{
	xmlNode *node_found = NULL ;

	THROW_IF_FAIL (a_this && GTK_IS_BUTTON (a_this));
	THROW_IF_FAIL (a_editor && MLVIEW_IS_TREE_EDITOR (a_editor)
				   && PRIVATE (a_editor))

	do_search_node (a_editor, FALSE, &node_found) ;

	if (!node_found) {
		mlview::AppContext *context = mlview::AppContext::get_instance () ;
		THROW_IF_FAIL (context) ;
		context->message (_("Reached the beginning of the document")) ;
	}
}

static void
search_win_next_button_clicked_cb (GtkButton *a_this,
                                   MlViewTreeEditor *a_editor)
{
	xmlNode *node_found = NULL;

	THROW_IF_FAIL (a_this && GTK_IS_BUTTON (a_this));
	THROW_IF_FAIL (a_editor && MLVIEW_IS_TREE_EDITOR (a_editor)) ;

	do_search_node (a_editor, TRUE, &node_found) ;

	if (!node_found) {
		mlview::AppContext *context = mlview::AppContext::get_instance () ;
		THROW_IF_FAIL (context) ;
		context->message (_("Reached the end of the document")) ;
	}
}

/**
 *Helper function. Given an attributes node,
 * builds the string "attr1=val1 ... attrN=valN"
 *Note that this function is recursive. 
 *@param a_attr_node the instance of xmlAttr to consider.
 *@param a_result out parameter the resulting string.
 */
static void
xml_attr_to_string (void *a_attr_node, gchar ** a_result)
{
	xmlAttrPtr xml_attr = (xmlAttrPtr) a_attr_node;
	xmlNodePtr xml_node = (xmlNodePtr) a_attr_node;

	static int num_of_use = 0;

	if (num_of_use++ == 0)
		*a_result = NULL;

	if (a_attr_node == NULL)
		return;

	if (xml_attr->type == XML_ATTRIBUTE_NODE) {
		gchar *tmp_str = *a_result,
		                 *name;

		if (xml_attr->ns != NULL
		        && xml_attr->ns->prefix != NULL) {
			name = g_strconcat
			       ((gchar*)xml_attr->ns->prefix, ":",
			        (gchar*)xml_attr->name, NULL);
		} else {
			name = g_strdup ((gchar*)xml_attr->name);
		}
		if (tmp_str == NULL)
			*a_result = g_strdup (name);
		else
			*a_result = g_strconcat
			            (tmp_str, " ", name, NULL);
		if (tmp_str) {
			g_free (tmp_str);
			tmp_str = NULL;
		}
		if (name) {
			g_free (name);
			name = NULL;
		}
		if (xml_attr->children)
			xml_attr_to_string
			(xml_attr->children, a_result);

		if (xml_attr->next)
			xml_attr_to_string
			(xml_attr->next, a_result);
	} else if (xml_node->type == XML_TEXT_NODE) {
		gchar *tmp_str = *a_result;

		if (tmp_str) {
			*a_result = g_strconcat
			            (tmp_str, "=\"",
			             xml_node->content, "\"", NULL);
			g_free (tmp_str);
			tmp_str = NULL;
		}
	}
}


/**
 *Builds a start tag string
 *(e.g: <node-name attr0="val0" attr1="val1">)out of
 *an instance of xmlNode *
 *@param a_node the instance of xmlNode * to consider.
 *@return the newly built start tag string.
 */
static gchar *
node_to_string_tag (MlViewTreeEditor *a_this,
		    xmlNode * a_node,
		    gboolean selected)
{
    gchar *result = NULL, *content = NULL, *escaped_content = NULL;
    gchar *colour_str = NULL ;

    THROW_IF_FAIL (a_node != NULL);
    THROW_IF_FAIL (a_this && MLVIEW_IS_TREE_EDITOR (a_this)) ;

    if (selected) {
	GdkColor color =
	    PRIVATE (a_this)->style->fg[GTK_STATE_SELECTED];
	colour_str = (gchar*)mlview::gdk_color_to_html_string (color).c_str ();
    } else {
	colour_str = (gchar*) mlview_tree_editor_get_colour_string
	    (a_this, a_node->type);
    }


    if (a_node->type == XML_ELEMENT_NODE) {
	gchar *ns_prefix = NULL, *attr_str = NULL, *name = NULL;

	gchar *attr_colour_str = NULL;
	if (selected) {
	    attr_colour_str = g_strdup (colour_str);
	} else {
	    attr_colour_str = (gchar*)mlview_tree_editor_get_colour_string
		(a_this, XML_ATTRIBUTE_NODE) ;
	}

	attr_str = mlview_tree_editor_build_attrs_list_str
	    (a_this, a_node, selected) ;
	if (a_node->ns != NULL && a_node->ns->prefix) {
	    ns_prefix = g_strconcat ((gchar*)a_node->ns->prefix,
				     ":", NULL);
	} else {
	    ns_prefix = NULL;
	}

	if (ns_prefix) {
	    name = g_strconcat ((gchar*)ns_prefix,
				(gchar*)a_node->name, NULL);
	} else {
	    name = g_strdup ((gchar*)a_node->name);
	}
	if (ns_prefix) {
	    g_free (ns_prefix);
	    ns_prefix = NULL;
	}

	if (a_node->children != NULL) {
	    if (attr_str) {
		result = g_strconcat
		    ("<span foreground=\"",
		     colour_str, "\">&lt;",
		     name,
		     "</span> <span foreground=\"",
		     attr_colour_str, "\">",
		     attr_str,
		     "</span><span foreground=\"",
		     colour_str, "\">&gt;</span>",
		     NULL);
	    }
	    else {
		result = g_strconcat
		    ("<span foreground=\"",
		     colour_str, "\">&lt;",
		     name, "&gt;</span>", NULL);
	    }
	} else {       /*empty tag */
	    if (attr_str)
		result = g_strconcat
		    ("<span foreground=\"",
		     colour_str, "\">&lt;",
		     name,
		     "</span> <span foreground=\"",
		     attr_colour_str,
		     "\">", attr_str,
		     "</span><span foreground=\"",
		     colour_str,
		     "\"> /&gt;</span>", NULL);
	    else
		result = g_strconcat
		    ("<span foreground=\"",
		     colour_str, "\">&lt;", name,
		     " /&gt;</span>", NULL);
	}

	if (name) {
	    g_free (name);
	    name = NULL;
	}


    } else if (xmlNodeIsText (a_node)) {
	gchar *str = NULL;
	guint esc_content_len = 0 ;
	enum MlViewStatus status = MLVIEW_OK ;

	content = (gchar*)xmlNodeGetContent (a_node);
	if (content == NULL) {
	    xmlNodeSetContent (a_node, (xmlChar*)"text");
	    content = (gchar*)xmlNodeGetContent (a_node);
	}

	status = mlview_utils_escape_predef_entities_in_str
	    (content, &escaped_content,
	     &esc_content_len) ;
	if (status != MLVIEW_OK) {
	    escaped_content = NULL;
	}
	if (!escaped_content) {
	    str = content ;
	} else {
	    str = escaped_content ;
		}
	result = g_strconcat
	    ("<span foreground=\"", colour_str,
	     "\">", str, "</span>", NULL);
	xmlFree (content);
	if (escaped_content) {
	    g_free (escaped_content) ;
	    escaped_content = NULL ;
	}
    } else if (a_node->type == XML_COMMENT_NODE) {
	content = (gchar*)xmlNodeGetContent (a_node);
	if (content == NULL) {
	    xmlNodeSetContent (a_node, (xmlChar*)"<!--comment-->");
	    content = (gchar*)xmlNodeGetContent (a_node);
	    if (!content) {
		mlview_utils_trace_debug
		    ("xmlNodeGetContent() failed") ;
		return NULL ;
	    }
	}
	escaped_content = g_markup_escape_text (content,
						strlen (content));
	result = g_strconcat
	    ("<span foreground=\"",
	     colour_str, "\">&lt;!--",
	     escaped_content,
	     "--&gt;</span>", NULL);
	if (escaped_content) {
	    g_free (escaped_content);
	    escaped_content = NULL ;
	}
	if (content) {
	    xmlFree (content) ;
	    content = NULL ;
	}
    } else if (a_node->type == XML_PI_NODE) {
	content = (gchar*)xmlNodeGetContent (a_node);
	if (content == NULL) {
	    xmlNodeSetContent
		(a_node,
		 (xmlChar*)"&lt;?processing instruction node&gt;");
	    content = (gchar*) xmlNodeGetContent (a_node);
	    if (!content) {
		mlview_utils_trace_debug
		    ("xmlNodeGetContent() failed") ;
		return NULL ;
	    }
	}
	escaped_content = g_markup_escape_text
	    (content, strlen (content)) ;
	result = g_strconcat
	    ("<span foreground=\"",
	     colour_str, "\">&lt;?", a_node->name, " ",
	     escaped_content, "?&gt;</span>", NULL);
	if (escaped_content) {
	    g_free (escaped_content) ;
	    escaped_content = NULL ;
	}
	if (content) {
	    xmlFree (content);
	    content = NULL ;
	}
    } else if (a_node->type == XML_DTD_NODE) {
	mlview_tree_editor_dtd_node_to_string
	    (a_this, (xmlDtd*)a_node, selected, &result) ;
    } else if (a_node->type == XML_ENTITY_DECL) {
	xmlEntity *entity = (xmlEntity*) a_node ;
	switch (entity->etype) {
	case XML_INTERNAL_GENERAL_ENTITY:
	    mlview_tree_editor_internal_general_entity_to_string
		(a_this, entity, selected, &result) ;
	    break ;
	case XML_EXTERNAL_GENERAL_PARSED_ENTITY:
	    mlview_tree_editor_external_general_parsed_entity_to_string
		(a_this, entity, selected, &result) ;
	    break ;
	case XML_EXTERNAL_GENERAL_UNPARSED_ENTITY:
	    mlview_tree_editor_external_general_unparsed_entity_to_string
		(a_this, entity, selected, &result) ;
	    break ;
	case XML_INTERNAL_PARAMETER_ENTITY:
	    mlview_tree_editor_internal_parameter_entity_to_string
		(a_this, entity, selected, &result) ;
	    break ;
	case XML_EXTERNAL_PARAMETER_ENTITY:
	    mlview_tree_editor_external_parameter_entity_to_string
		(a_this, entity, selected, &result) ;
	    break ;

	case XML_INTERNAL_PREDEFINED_ENTITY:
	    mlview_utils_trace_debug
		("Oops, dunno how to render "
		 "XML_INTERNAL_PREDEFINED_ENTITY "
		 "type of xml entity decl node") ;
	    break ;
	default:
	    mlview_utils_trace_debug
		("Unknown entity type") ;
	}
    } else if (a_node->type == XML_ENTITY_REF_NODE) {
	mlview_tree_editor_entity_ref_to_string
	    (a_this, a_node, selected, &result) ;
    } else if (a_node->type == XML_CDATA_SECTION_NODE) {
	mlview_tree_editor_cdata_section_to_string
	    (a_this, a_node, &result) ;
    } else if (a_node->type == XML_DOCUMENT_NODE) {
	mlview_tree_editor_document_node_to_string
	    (a_this, a_node, selected, &result) ;
    }
    else {
	mlview_utils_trace_debug ("Unknown type of node") ;
    }

    return result;
}



static enum MlViewStatus
build_tree_model_from_xml_tree (MlViewTreeEditor * a_this,
                                const xmlNode * a_node,
                                GtkTreeIter * a_ref_iter,
                                enum MlViewTreeInsertType a_type,
                                GtkTreeModel ** a_model)
{
	GtkTreeStore *model = NULL;
	GtkTreeIter iter = {0} ;
	GtkTreeIter parent_iter = {0} ;
	GtkTreePath *tree_path = NULL;
	xmlNode *cur_node = NULL,
	                    *parent_node = NULL;
	gchar *start_tag = NULL;
	enum MlViewStatus status = MLVIEW_OK;

	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_TREE_EDITOR (a_this)
	                      && PRIVATE (a_this)
	                      && a_node && a_model && *a_model,
	                      MLVIEW_BAD_PARAM_ERROR);

	model = GTK_TREE_STORE (*a_model);
	THROW_IF_FAIL (model);

	if (!PRIVATE (a_this)->nodes_rows_hash) {
		PRIVATE (a_this)->nodes_rows_hash =
		    g_hash_table_new (g_direct_hash,
		                      g_direct_equal);
		if (!PRIVATE (a_this)->nodes_rows_hash) {
			mlview_utils_trace_debug
			("The system may be out of memory");
			return MLVIEW_ERROR;
		}
	}

	for (cur_node = (xmlNode *) a_node;
	        cur_node; cur_node = cur_node->next) {
		GtkTreeRowReference *row_ref = NULL;

		start_tag = node_to_string_tag
		            (a_this, cur_node, FALSE);
		switch (a_type) {
		case INSERT_TYPE_PREPEND_CHILD:
			gtk_tree_store_prepend (model, &iter,
			                        a_ref_iter) ;
			break ;
		case INSERT_TYPE_APPEND_CHILD_VISIT_NEXT:
			gtk_tree_store_append (model, &iter,
			                       a_ref_iter);
			break;
		case INSERT_TYPE_INSERT_BEFORE:
		case INSERT_TYPE_INSERT_AFTER:
			parent_node = cur_node->parent;
			if (!parent_node) {
				mlview_utils_trace_debug
				("parent_node failed") ;
				status = MLVIEW_ERROR ;
				goto cleanup ;

			}
			status = mlview_tree_editor_get_iter
			         (a_this, parent_node,
			          &parent_iter);
			if (status != MLVIEW_OK) {
				mlview_utils_trace_debug
				("status == MLVIEW_OK failed") ;
				status = MLVIEW_ERROR ;
				goto cleanup ;
			}
			model = GTK_TREE_STORE
			        (mlview_tree_editor_get_model
			         (a_this));
			if (!model) {
				mlview_utils_trace_debug
				("model failed") ;
				status = MLVIEW_ERROR ;
				goto cleanup ;
			}
			if (a_type == INSERT_TYPE_INSERT_BEFORE)
				gtk_tree_store_insert_before
				(model, &iter,
				 &parent_iter,
				 a_ref_iter);
			else
				gtk_tree_store_insert_after
				(model, &iter,
				 &parent_iter,
				 a_ref_iter);
			break;
		default:
			break;
		}
		tree_path = gtk_tree_model_get_path
		            (GTK_TREE_MODEL (model), &iter);
		if (!tree_path) {
			mlview_utils_trace_debug ("tree_path failed") ;
			status = MLVIEW_ERROR ;
			goto cleanup ;
		}
		row_ref = gtk_tree_row_reference_new
		          (GTK_TREE_MODEL (model), tree_path);
		if (!row_ref) {
			mlview_utils_trace_debug ("row_ref failed") ;
			status = MLVIEW_ERROR ;
			goto cleanup ;
		}
		g_hash_table_insert
		(PRIVATE (a_this)->nodes_rows_hash,
		 cur_node, row_ref);
		gtk_tree_store_set (model, &iter,
		                    XML_NODE_COLUMN, cur_node, -1);
		if (start_tag) {
			gtk_tree_store_set (model, &iter,
			                    START_TAG_COLUMN,
			                    start_tag, -1);
		}
		if (cur_node->type == XML_ELEMENT_NODE) {
			gtk_tree_store_set (model, &iter,
			                    NODE_TYPE_COLUMN,
			                    "Element Node",
			                    IS_EDITABLE_COLUMN,
			                    TRUE, -1);
			if (cur_node->children) {
				mlview_tree_editor_build_tree_model_from_xml_tree
				(a_this,
				 cur_node->children,
				 &iter,
				 INSERT_TYPE_APPEND_CHILD_VISIT_NEXT,
				 a_model);
			}
		} else if (cur_node->type == XML_TEXT_NODE) {
			gtk_tree_store_set (model, &iter,
			                    NODE_TYPE_COLUMN,
			                    "Text Node",
			                    IS_EDITABLE_COLUMN,
			                    TRUE, -1);
		} else if (cur_node->type == XML_COMMENT_NODE
		           || cur_node->type == XML_PI_NODE) {
			gtk_tree_store_set
			(model, &iter,
			 NODE_TYPE_COLUMN, "Comment or PI Node",
			 IS_EDITABLE_COLUMN, TRUE,
			 -1);
		} else if (cur_node->type == XML_DTD_NODE) {
			gtk_tree_store_set
			(model, &iter,
			 NODE_TYPE_COLUMN, "DTD Node",
			 IS_EDITABLE_COLUMN, TRUE,
			 -1);
			if (cur_node->children) {
				mlview_tree_editor_build_tree_model_from_xml_tree
				(a_this,
				 cur_node->children,
				 &iter,
				 INSERT_TYPE_APPEND_CHILD_VISIT_NEXT,
				 a_model);
			}
		} else if (cur_node->type == XML_ENTITY_DECL) {
			gtk_tree_store_set
			(model, &iter,
			 NODE_TYPE_COLUMN, "ENTITY Declaration Node",
			 IS_EDITABLE_COLUMN, TRUE,
			 -1);
		} else if (cur_node->type == XML_ENTITY_REF_NODE) {
			gtk_tree_store_set
			(model, &iter,
			 NODE_TYPE_COLUMN, "ENTITY Reference Node",
			 IS_EDITABLE_COLUMN, FALSE,
			 -1);
		} else if (cur_node->type == XML_CDATA_SECTION_NODE) {
			gtk_tree_store_set
			(model, &iter,
			 NODE_TYPE_COLUMN, "CDATA Section Node",
			 IS_EDITABLE_COLUMN, TRUE,
			 -1);
		} else {
			mlview_utils_trace_debug ("unknown type of node") ;
		}
		if (start_tag) {
			g_free (start_tag) ;
			start_tag = NULL ;
		}
		if (tree_path) {
			gtk_tree_path_free (tree_path) ;
			tree_path = NULL ;
		}
		if (a_type == INSERT_TYPE_INSERT_BEFORE
		        || a_type == INSERT_TYPE_INSERT_AFTER
		        || a_type == INSERT_TYPE_PREPEND_CHILD) {
			/*
			 *we are building a tree model which root
			 *node is cur_node so we should not
			 *visit cur_node->next.
			 */
			break ;
		}
	}
	if (*a_model) {
		g_object_set_data (G_OBJECT (*a_model),
		                   "MlViewTreeEditor",
		                   a_this) ;
	}
cleanup:
	if (tree_path) {
		gtk_tree_path_free (tree_path) ;
		tree_path = NULL ;
	}
	if (start_tag) {
		g_free (start_tag) ;
		start_tag = NULL ;
	}
	return status ;
}

/**
 *Builds an instance of GtkTreeModel* out of
 *an instance of xmlDoc*.
 *@param a_doc the instance of xmlDoc* to consider.
 *@param a_model out parameter the resulting model.
 *@return MLVIEW_OK upon successful completion, an error
 *code otherwise.
 */
static enum MlViewStatus
build_tree_model_from_xml_doc (MlViewTreeEditor * a_this,
                               const xmlDoc * a_doc,
                               GtkTreeModel ** a_model)
{
	GtkTreeIter iter = { 0 };
	GtkTreeStore *model = NULL, **tree_store_ptr = NULL;
	GtkTreeRowReference *row_ref = NULL;
	GtkTreePath *tree_path = NULL;
	xmlNode *xml_tree = NULL;
	enum MlViewStatus status = MLVIEW_OK;

	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_TREE_EDITOR (a_this)
	                      && PRIVATE (a_this)
	                      && a_doc && a_model
	                      && *a_model == NULL,
	                      MLVIEW_BAD_PARAM_ERROR);

	if (!PRIVATE (a_this)->nodes_rows_hash) {
		PRIVATE (a_this)->nodes_rows_hash =
		    g_hash_table_new (g_direct_hash,
		                      g_direct_equal);
		if (!PRIVATE (a_this)->nodes_rows_hash) {
			mlview_utils_trace_debug
			("The system may be out of memory");
			return MLVIEW_ERROR;
		}
	}
	model = gtk_tree_store_new (NB_COLUMNS,
	                            G_TYPE_POINTER,
	                            G_TYPE_BOOLEAN,
	                            G_TYPE_STRING,
	                            G_TYPE_STRING);
	THROW_IF_FAIL (model);
	*a_model = GTK_TREE_MODEL (model);
	THROW_IF_FAIL (model);

	gtk_tree_store_append (model, &iter, NULL);
	tree_path =
	    gtk_tree_model_get_path (GTK_TREE_MODEL (model),
	                             &iter);
	THROW_IF_FAIL (tree_path);
	row_ref = gtk_tree_row_reference_new
	          (GTK_TREE_MODEL (model), tree_path);
	if (!row_ref) {
		mlview_utils_trace_debug ("!row_ref failed") ;
		goto cleanup ;
	}
	g_hash_table_insert (PRIVATE (a_this)->nodes_rows_hash,
	                     (gpointer) a_doc, row_ref);
	gtk_tree_store_set (model, &iter, XML_NODE_COLUMN, a_doc, -1);
	gtk_tree_store_set (model, &iter, START_TAG_COLUMN,
	                    "<span foreground=\"#bbbb00\">XML Document Root</span>", -1);
	gtk_tree_store_set (model, &iter, NODE_TYPE_COLUMN, "", -1);
	xml_tree = a_doc->children;

	tree_store_ptr = &model ;
	status = mlview_tree_editor_build_tree_model_from_xml_tree
	         (a_this, xml_tree,
	          &iter, INSERT_TYPE_APPEND_CHILD_VISIT_NEXT,
	          (GtkTreeModel **) tree_store_ptr);
cleanup:
	if (tree_path) {
		gtk_tree_path_free (tree_path) ;
		tree_path = NULL ;
	}
	return status;
}


static GtkTreeView *
build_tree_view_from_xml_doc (MlViewTreeEditor * a_this,
                              xmlDoc * a_doc)
{
	GtkTreeView *tree_view = NULL ;
	GtkTreeModel *model = NULL ;
	GtkCellRenderer *renderer = NULL ;
	GtkTreeIter iter = {0} ;
	GtkTreeViewColumn *column = NULL ;
	enum MlViewStatus status = MLVIEW_OK ;
	gboolean is_ok = TRUE ;
	guint nb_menus = 0 ;

	THROW_IF_FAIL (a_this && MLVIEW_IS_TREE_EDITOR (a_this) && PRIVATE (a_this));

	mlview::AppContext *context = mlview::AppContext::get_instance () ;
	THROW_IF_FAIL (context) ;

	mlview::PrefsCategoryTreeview *m_prefs =
	    dynamic_cast<mlview::PrefsCategoryTreeview*> (
		mlview::Preferences::get_instance ()
		->get_category_by_id ("treeview"));


	status = build_tree_model_from_xml_doc (a_this, a_doc, &model);
	THROW_IF_FAIL (model);
	is_ok = gtk_tree_model_get_iter_first (model, &iter) ;
	THROW_IF_FAIL (is_ok == TRUE) ;
	tree_view = GTK_TREE_VIEW
	            (gtk_tree_view_new_with_model (model));
	THROW_IF_FAIL (tree_view);
#ifndef MLVIEW_WITH_CUSTOM_CELL_RENDERER

	renderer = gtk_cell_renderer_text_new ();
#else

	renderer = mlview_cell_renderer_new () ;
	g_signal_connect (G_OBJECT (renderer),
	                  "word-changed",
	                  G_CALLBACK (word_changed_cb),
	                  a_this) ;
	g_signal_connect (G_OBJECT (renderer),
	                  "editing-has-started",
	                  G_CALLBACK (editing_has_started_cb),
	                  a_this) ;
	g_signal_connect (G_OBJECT (renderer),
	                  "select-editable-region",
	                  G_CALLBACK (select_editable_region_cb),
	                  a_this) ;
#endif

	g_object_set (G_OBJECT (renderer), "single-paragraph-mode", FALSE,
	              NULL) ;

	nb_menus = gtk_tree_view_insert_column_with_attributes
	           (tree_view, START_TAG_COLUMN, _("Element start tag"),
	            renderer, "markup", START_TAG_COLUMN,
	            "editable", IS_EDITABLE_COLUMN,
	            NULL);
	if (nb_menus)
		column = gtk_tree_view_get_column (tree_view,
		                                   nb_menus -1) ;
	if (column) {
		gtk_tree_view_column_set_resizable
		(column, TRUE) ;
	}
	g_signal_connect (G_OBJECT (renderer),
	                  "edited", G_CALLBACK (node_cell_edited_cb),
	                  a_this) ;
#ifndef MLVIEW_WITH_CUSTOM_CELL_RENDERER

	renderer = gtk_cell_renderer_text_new ();
#else

	renderer = mlview_cell_renderer_new () ;
#endif

	gtk_tree_view_insert_column_with_attributes
	(tree_view, NODE_TYPE_COLUMN, _("Element type"),
	 renderer, "text", NODE_TYPE_COLUMN, NULL) ;
	mlview_utils_gtk_tree_view_expand_row_to_depth2
	(tree_view, &iter,
	 m_prefs->get_default_tree_expansion_depth ());

// Set font from preferences
	PRIVATE (a_this)->style =
		gtk_widget_get_style (GTK_WIDGET (tree_view));

	PangoFontDescription *font_desc =
		pango_font_description_from_string (
		    m_prefs->get_font_name ().c_str());

	if (font_desc != NULL) {
		gtk_widget_modify_font (GTK_WIDGET (tree_view),
					font_desc);
		pango_font_description_free (font_desc);
	}

	return tree_view;
}

static gboolean
widget_realized_cb (GtkWidget *a_widget,
		    gpointer a_user_data)
{
	MlViewTreeEditor *editor = NULL ;

	g_return_val_if_fail (a_user_data
	                      && MLVIEW_IS_TREE_EDITOR (a_user_data),
	                      FALSE) ;

	editor = MLVIEW_TREE_EDITOR (a_user_data) ;

	THROW_IF_FAIL (a_widget) ;

	if (!GTK_WIDGET_NO_WINDOW (a_widget)) {
		gtk_widget_add_events (GTK_WIDGET (a_widget),
		                       GDK_BUTTON3_MOTION_MASK);
		g_signal_connect (G_OBJECT (a_widget),
		                  "button-press-event",
		                  G_CALLBACK (button_press_event_cb),
		                  editor);
		g_signal_connect (G_OBJECT (a_widget),
		                  "key-press-event",
		                  G_CALLBACK (key_press_event_cb),
		                  editor) ;
	} else {
		mlview_utils_trace_debug
		("Hmmh, weird, this widget doesn't have an associated window") ;
	}
	return FALSE ;
}


static gboolean
button_press_event_cb (GtkWidget * a_widget,
                       GdkEventButton * a_event,
                       gpointer a_user_data)
{
	MlViewTreeEditor *tree_editor = NULL;
	mlview::AppContext *ctxt = mlview::AppContext::get_instance () ;
	THROW_IF_FAIL (ctxt) ;
	GtkTreePath *tree_path = NULL ;
	GtkTreeView *tree_view = NULL ;

	THROW_IF_FAIL (a_widget != NULL);
	THROW_IF_FAIL (GTK_IS_WIDGET (a_widget));
	THROW_IF_FAIL (a_user_data != NULL);
	THROW_IF_FAIL (MLVIEW_IS_TREE_EDITOR (a_user_data));
	THROW_IF_FAIL (a_event != NULL);

	tree_editor = MLVIEW_TREE_EDITOR (a_user_data);
	THROW_IF_FAIL (tree_editor != NULL);
	THROW_IF_FAIL (PRIVATE (tree_editor));

	tree_view = mlview_tree_editor_get_tree_view (tree_editor) ;
	THROW_IF_FAIL (tree_view) ;

	switch (a_event->type) {
	case GDK_BUTTON_PRESS:
		if (a_event->button == 3) {
			/*
			 *User pressed the right mouse
			 *button. Select the node selected that way.
			 */
			gtk_tree_view_get_path_at_pos (tree_view,
			                               (int)a_event->x,
			                               (int)a_event->y,
			                               &tree_path,
			                               NULL,
			                               NULL,
			                               NULL) ;
			if (tree_path) {
				mlview_tree_editor_select_node2 (tree_editor,
				                                 tree_path,
				                                 FALSE, TRUE) ;
				gtk_tree_path_free (tree_path) ;
				tree_path = NULL ;
			}
			/*
			 *user pressed the right mouse button 
			 *Notify the application (at least the
			 *editing view that contains us) that
			 *a request for a contextual menu request
			 *request been made.
			 */
			ctxt->notify_contextual_menu_request (GTK_WIDGET (tree_editor),
												  (GdkEvent*)a_event) ;
			return TRUE ;
		}
		break;
	default:
		break;
	}
	return FALSE ;
}

static gboolean
key_press_event_cb (GtkWidget *a_widget,
                    GdkEvent *a_event,
                    gpointer a_user_data)
{
	enum MlViewStatus status = MLVIEW_OK ;
	struct MlViewKBDef *keybinding = NULL ;
	MlViewTreeEditor *thiz = NULL ;
	gboolean result = FALSE ;

	g_return_val_if_fail (a_widget
	                      && a_user_data
	                      && MLVIEW_IS_TREE_EDITOR (a_user_data)
	                      && a_event,
	                      FALSE) ;

	thiz = MLVIEW_TREE_EDITOR (a_user_data) ;
	g_return_val_if_fail (thiz && PRIVATE (thiz)
	                      && PRIVATE (thiz)->kb_eng,
	                      FALSE) ;
	THROW_IF_FAIL (a_event->type == GDK_KEY_PRESS) ;

	/*
	 *here, try to recon one of the MlViewTreeEditor keybindings
	 */
	status = mlview_kb_lookup_key_binding_from_key_press
	         (PRIVATE (thiz)->kb_eng,
	          (GdkEventKey*)a_event,
	          &keybinding) ;

	if (status == MLVIEW_OK && keybinding) {
		/*
		 *We found a keybinding, with associated action.
		 *Let's execute that action and clear the input keys queue.
		 */
		if (keybinding->action) {
			keybinding->action (thiz) ;
		}
		result = TRUE ;
	} else if (status != MLVIEW_KEY_SEQUENCE_TOO_SHORT_ERROR) {
		/*
		 *We didn't recognize any keybinding, just clear the
		 *input keys queue, and let's forward this key to the
		 *rest of the widget ... maybe one of them will know
		 *what to do about it.
		 */
		result = FALSE ;
	} else {
		/*
		 *We recognized the begining of a keybinding.
		 *let's just wait for the user to type more keys to see
		 *if we really recon a keybinding ...
		 */
		result= TRUE ;
	}

	return result ;
}

static void
node_cell_edited_cb (GtkCellRendererText *a_renderer,
                     gchar *a_cell_path,
                     gchar *a_new_text,
                     gpointer a_data)
{
	MlViewTreeEditor *tree_editor = NULL ;
	GtkTreePath *tree_path = NULL ;
	GtkTreeIter iter = {0} ;
	GtkTreeModel *model = NULL ;
	MlViewXMLDocument *mlview_xml_doc = NULL ;
	enum MlViewStatus status = MLVIEW_OK ;
	GString *element_name = NULL ;
	GList *nv_pair_list = NULL ;
	xmlNode *cur_node = NULL ;
	gchar *start_tag = NULL ;
	gchar *node_path = NULL ;

	THROW_IF_FAIL (a_renderer && a_data && a_cell_path) ;
	THROW_IF_FAIL (MLVIEW_IS_TREE_EDITOR (a_data)
	                  && GTK_IS_CELL_RENDERER (a_renderer)) ;
	tree_editor = (MlViewTreeEditor*)a_data ;
	model = mlview_tree_editor_get_model (tree_editor) ;
	THROW_IF_FAIL (model) ;
	tree_path = gtk_tree_path_new_from_string (a_cell_path) ;
	THROW_IF_FAIL (tree_path) ;
	status = mlview_tree_editor_get_cur_sel_start_iter
	         (tree_editor, &iter) ;
	THROW_IF_FAIL (status == MLVIEW_OK) ;
	mlview_xml_doc = mlview_tree_editor_get_mlview_xml_doc (tree_editor);
	if (!mlview_xml_doc) {
		mlview_utils_trace_debug
		("mlview_xml_doc failed") ;
		goto cleanup ;
	}
	cur_node = mlview_tree_editor_get_cur_sel_xml_node (tree_editor) ;
	if (!cur_node) {
		mlview_utils_trace_debug ("cur_node failed") ;
		goto cleanup ;
	}
	mlview_xml_document_get_node_path (mlview_xml_doc,
	                                   cur_node,
	                                   &node_path) ;
	if (!node_path) {
		mlview_utils_trace_debug ("Could not get node path") ;
		goto cleanup ;
	}
	start_tag = node_to_string_tag (tree_editor, cur_node, FALSE) ;
	if (cur_node->type == XML_ELEMENT_NODE) {
		status = mlview_utils_parse_start_tag
		         (a_new_text, &element_name, &nv_pair_list) ;
		if (status != MLVIEW_OK) {
			g_signal_handlers_block_by_func
			(a_renderer,
			 (void*) node_cell_edited_cb,
			 a_data) ;
			gtk_tree_store_set (GTK_TREE_STORE (model),
			                    &iter, START_TAG_COLUMN,
			                    start_tag, -1) ;
			g_signal_handlers_unblock_by_func
			(a_renderer,
			 (void*)node_cell_edited_cb,
			 a_data) ;
		} else {
			status = mlview_xml_document_set_node_name
			         (mlview_xml_doc, node_path,
			          element_name->str, TRUE) ;

			if (status == MLVIEW_OK) {
				if (node_path) {
					g_free (node_path) ;
					node_path = NULL ;
					mlview_xml_document_get_node_path
					(mlview_xml_doc, cur_node, &node_path) ;
					if (!node_path) {
						mlview_utils_trace_debug ("Could not get node path") ;
						goto cleanup ;
					}
				}
				mlview_xml_document_synch_attributes
				(mlview_xml_doc,
				 node_path,
				 nv_pair_list) ;
			}
		}
	} else if (cur_node->type == XML_TEXT_NODE) {
		mlview_xml_document_set_node_content
		(mlview_xml_doc, node_path,
		 a_new_text, TRUE) ;
	} else if (cur_node->type == XML_COMMENT_NODE) {
		GString *comment = NULL ;
		status = mlview_utils_parse_comment
		         (a_new_text, &comment) ;
		if (status != MLVIEW_OK) {
			g_signal_handlers_block_by_func
			(a_renderer,
			 (void*)node_cell_edited_cb,
			 a_data) ;
			gtk_tree_store_set (GTK_TREE_STORE (model),
			                    &iter, START_TAG_COLUMN,
			                    start_tag, -1) ;
			g_signal_handlers_unblock_by_func
			(a_renderer,
			 (void*)node_cell_edited_cb,
			 a_data) ;
		} else {
			mlview_xml_document_set_node_content
			(mlview_xml_doc, node_path,
			 comment->str, TRUE) ;
		}
		if (comment) {
			g_string_free (comment, TRUE) ;
			comment = NULL ;
		}
	} else if (cur_node->type == XML_PI_NODE) {
		GString *pi_target = NULL,
		                     *pi_param = NULL ;
		status = mlview_utils_parse_pi (a_new_text,
		                                &pi_target,
		                                &pi_param) ;
		if (pi_target && pi_target->str) {
			mlview_xml_document_get_node_path (mlview_xml_doc,
			                                   cur_node,
			                                   &node_path) ;
			if (!node_path) {
				mlview_utils_trace_debug ("Could not get node path") ;
				goto cleanup ;
			}
			mlview_xml_document_set_node_name
			(mlview_xml_doc, node_path,
			 pi_target->str, TRUE) ;
			if (node_path) {
				g_free (node_path) ;
				node_path = NULL ;
			}
			if (pi_param && pi_param->str) {
				mlview_xml_document_set_node_content
				(mlview_xml_doc, node_path,
				 pi_param->str, TRUE) ;
			}
		} else {
			g_signal_handlers_block_by_func
			(a_renderer,
			 (void*)node_cell_edited_cb,
			 a_data) ;
			gtk_tree_store_set (GTK_TREE_STORE (model),
			                    &iter, START_TAG_COLUMN,
			                    start_tag, -1) ;
			g_signal_handlers_unblock_by_func
			(a_renderer,
			 (void*)node_cell_edited_cb,
			 a_data) ;
		}
	} else if (cur_node->type == XML_DTD_NODE) {
		mlview_tree_editor_edit_dtd_node
		(tree_editor, (xmlDtd*)cur_node, a_new_text) ;
	} else if (cur_node->type == XML_ENTITY_DECL) {
		mlview_tree_editor_edit_xml_entity_decl_node
		(tree_editor, (xmlEntity*)cur_node,
		 a_new_text) ;
	} else if (cur_node->type == XML_CDATA_SECTION_NODE) {
		mlview_tree_editor_edit_cdata_section_node (tree_editor,
		        cur_node,
		        a_new_text) ;
	}

cleanup:
	if (start_tag) {
		g_free (start_tag) ;
		start_tag = NULL ;
	}
	if (element_name) {
		g_string_free (element_name, TRUE) ;
		element_name = NULL ;
	}
	if (nv_pair_list) {
		mlview_utils_name_value_pair_list_free (nv_pair_list, TRUE) ;
		nv_pair_list = NULL ;
	}
	if (tree_path) {
		gtk_tree_path_free (tree_path) ;
		tree_path = NULL ;
	}
	if (node_path) {
		g_free (node_path) ;
		node_path = NULL ;
	}
}

#ifdef MLVIEW_WITH_CUSTOM_CELL_RENDERER
static void
editing_has_started_cb (MlViewCellRenderer *a_renderer,
                        GtkTreePath *a_path,
                        GtkEditable *a_editable,
                        gpointer a_user_data)
{
	xmlNode *cur_node = NULL ;
	MlViewTreeEditor *thiz = NULL ;

	THROW_IF_FAIL (a_renderer
	                  && a_path
	                  && a_user_data
	                  && MLVIEW_IS_TREE_EDITOR (a_user_data)) ;

	thiz = MLVIEW_TREE_EDITOR (a_user_data) ;
	THROW_IF_FAIL (thiz) ;
	cur_node = mlview_tree_editor_get_xml_node3 (thiz, a_path) ;
	THROW_IF_FAIL (cur_node) ;

	/*
	 *Clear the existing completion list, if any
	 */
	if (PRIVATE (thiz)->cur_node_completion_list) {
		g_list_free (PRIVATE (thiz)->cur_node_completion_list) ;
		PRIVATE (thiz)->cur_node_completion_list = NULL ;
	}
	if (PRIVATE (thiz)->completion_engine) {
		g_completion_clear_items (PRIVATE (thiz)->completion_engine) ;
	}
	/*
	 *Compute a new completion list.
	 */
	mlview_parsing_utils_build_element_name_completion_list
	(CHANGE_CUR_ELEMENT_NAME, cur_node,
	 &PRIVATE (thiz)->cur_node_completion_list) ;

	if (!PRIVATE (thiz)->completion_engine) {
		PRIVATE (thiz)->completion_engine = g_completion_new (NULL) ;
	}
	g_completion_add_items (PRIVATE (thiz)->completion_engine,
	                        PRIVATE (thiz)->cur_node_completion_list) ;
}

void
word_changed_cb (MlViewCellRenderer *a_cell_renderer,
                 GtkEditable *a_editable,
                 const gchar *a_word_start,
                 const gchar *a_word_end,
                 gboolean a_char_added,
                 gint a_position,
                 gint a_word_start_position,
                 gint a_word_end_position,
                 gpointer a_data)
{
	enum MlViewStatus status = MLVIEW_OK ;
	MlViewTreeEditor *editor = NULL ;
	xmlNode *cur_node = NULL;
	gchar *text = NULL ;
	GString*element_name = NULL ;
	GList *nv_pair_list = NULL ;
	GList *completion_list = NULL ;

	THROW_IF_FAIL (a_cell_renderer
	                  && MLVIEW_IS_CELL_RENDERER (a_cell_renderer)) ;
	THROW_IF_FAIL (a_editable && MLVIEW_IS_ENTRY (a_editable)) ;
	THROW_IF_FAIL (a_word_start && a_word_end) ;
	THROW_IF_FAIL (a_data && MLVIEW_IS_TREE_EDITOR (a_data)) ;

#ifdef MLVIEW_VERBOSE

	g_print ("Detected keypressed in the cell editing widget\n") ;
#endif

	editor = MLVIEW_TREE_EDITOR (a_data) ;
	THROW_IF_FAIL (editor) ;

	cur_node = mlview_tree_editor_get_cur_sel_xml_node (editor) ;

	if (!cur_node) {
		mlview_utils_trace_debug ("No current node selected\n") ;
		return ;
	}
	text = gtk_editable_get_chars (a_editable, 0, -1) ;
	switch (cur_node->type) {
	case XML_ELEMENT_NODE:
		status = mlview_utils_parse_start_tag
		         (text, &element_name, &nv_pair_list) ;

		if (element_name && element_name->str
		        && !strncmp (element_name->str, a_word_start,
		                     element_name->len)
		   ) {
			completion_list = g_completion_complete
			                  (PRIVATE (editor)->completion_engine,
			                   element_name->str,
			                   NULL) ;

#ifdef MLVIEW_VERBOSE

			{
				GList *cur_elem = NULL ;
				g_print ("Completion list for current node is:\n") ;
				g_print ("====================\n") ;
				for (cur_elem = completion_list;
				        cur_elem;
				        cur_elem = g_list_next (cur_elem))
				{
					g_print ("%s\n",
					         (gchar*) cur_elem->data) ;
				}
				g_print ("====================\n") ;
			}
#endif
			mlview_entry_set_completion_list
			(MLVIEW_ENTRY (a_editable),
			 completion_list) ;

			/*now popup a damn menu*/
			mlview_entry_popup_word_completion_menu
			(MLVIEW_ENTRY (a_editable),
			 a_word_start_position,
			 a_word_end_position) ;
		} else {
			g_print ("Didn't detect the element name\n") ;
		}

		break ;
	default:
		break ;
	}
	if (text) {
		g_free (text) ;
		text = NULL ;
	}
	if (nv_pair_list) {
		mlview_utils_name_value_pair_list_free (nv_pair_list, TRUE) ;
		nv_pair_list = NULL ;
	}
}

static gboolean
key_pressed_in_search_dialog_cb (GtkWidget *a_dialog_widget,
                                 GdkEventKey *a_event,
                                 gpointer a_user_data)
{
	GtkButton *cancel_button = NULL ;
	THROW_IF_FAIL (a_dialog_widget && a_event) ;

	if (a_event->type != GDK_KEY_PRESS)
		return FALSE ;

	if (a_event->keyval != GDK_Escape)
		return FALSE ;
	cancel_button = (GtkButton*)g_object_get_data
	                (G_OBJECT (a_dialog_widget), "CancelButton") ;
	THROW_IF_FAIL (cancel_button) ;
	gtk_button_clicked (GTK_BUTTON (cancel_button)) ;
	return TRUE ;
}

static gboolean
select_editable_region_cb (MlViewCellRenderer *a_cell_renderer,
                           MlViewEntry *a_editable,
                           gpointer a_user_data)
{
	MlViewTreeEditor *thiz = NULL ;
	gchar *str = NULL, *str_ptr = NULL ;
	gint start_index = 0, end_index = 0, len = 0, i=0 ;
	guint32 unichar = 0 ;

	g_return_val_if_fail (a_cell_renderer
	                      && MLVIEW_IS_CELL_RENDERER (a_cell_renderer)
	                      && a_editable
	                      && MLVIEW_IS_ENTRY (a_editable),
	                      FALSE) ;
	g_return_val_if_fail (a_user_data
	                      && MLVIEW_IS_TREE_EDITOR (a_user_data),
	                      FALSE) ;
	thiz = MLVIEW_TREE_EDITOR (a_user_data) ;
	THROW_IF_FAIL (thiz) ;


	str = gtk_editable_get_chars (GTK_EDITABLE (a_editable), 0, -1) ;
	str_ptr = str ;
	len = g_utf8_strlen (str, -1) ;
	unichar = g_utf8_get_char (str_ptr) ;
	while (str_ptr && mlview_utils_is_name_char (unichar) == FALSE) {
		str_ptr = g_utf8_next_char (str_ptr) ;
		unichar = g_utf8_get_char (str_ptr) ;
		i++ ;
	}
	start_index = i ;
	str_ptr = &str[len - 1] ;
#ifdef MLVIEW_VERBOSE

	g_print ("Content of the MlViewEntry: %s\n", str) ;
	g_print ("Length of the content of the MlViewEntry: %d\n", len) ;
#endif

	unichar  = g_utf8_get_char (str_ptr) ;
	i = len - 1;
	while (str && mlview_utils_is_name_char (unichar) == FALSE) {
		str_ptr = g_utf8_prev_char (str_ptr) ;
		unichar = g_utf8_get_char (str_ptr) ;
		i-- ;
	}
	end_index = i ;
#ifdef MLVIEW_VERBOSE

	g_print ("Gonna select the content of the MlViewEntry:\n") ;
	g_print ("start offset: %d\n", start_index) ;
	g_print ("end offset: %d\n", end_index) ;
#endif

	if (str) {
		g_free (str) ;
		str = NULL ;
	}
	gtk_editable_select_region (GTK_EDITABLE (a_editable),
	                            start_index, end_index) ;
	return TRUE ;
}

#endif

/**
 *Builds a new xml node which type is given in argument.
 *@param a_node_type the node type.
 *@param the instance of #MlViewXMLDocument that holds the
 *newly created instance of xmlNode.
 *@return the newly created instance of xmlNode, or NULL if something
 *bad happened.
 */
static xmlNode *
new_xml_node (NodeTypeDefinition* a_node_type_def,
              MlViewXMLDocument * a_xml_doc)
{
	xmlNodePtr result = NULL;
	xmlDoc *doc = NULL;

	THROW_IF_FAIL (a_node_type_def) ;

	if (a_xml_doc)
		doc = mlview_xml_document_get_native_document
		      (a_xml_doc);
	switch (a_node_type_def->node_type) {
	case XML_ELEMENT_NODE:
		result = xmlNewNode (NULL, (xmlChar*)"");
		break;
	case XML_TEXT_NODE:
		result = xmlNewText ((xmlChar*)"");
		break;
	case XML_CDATA_SECTION_NODE:
		THROW_IF_FAIL (doc != NULL);
		result = xmlNewCDataBlock (doc, (xmlChar*)"", 128);
		break;
	case XML_DTD_NODE:
		result = (xmlNode*)xmlCreateIntSubset
		         (doc, (xmlChar*)"",
		          (xmlChar*)"default-public-id",
		          (xmlChar*)"default-system-id") ;
		break ;
	case XML_PI_NODE:
		result = xmlNewPI ((xmlChar*)"", (xmlChar*)"");
		break;
	case XML_COMMENT_NODE:
		result = xmlNewComment ((xmlChar*)"");
		break;
	case XML_ENTITY_DECL:
		switch (a_node_type_def->entity_type) {
		case XML_INTERNAL_GENERAL_ENTITY:
			result = (xmlNode*)xmlAddDocEntity
			         (doc, (xmlChar*)"",
			          XML_INTERNAL_GENERAL_ENTITY,
			          NULL, NULL,
			          (xmlChar*)"default-param-entity-value") ;
			break ;
		case XML_EXTERNAL_GENERAL_PARSED_ENTITY:
			result = (xmlNode*) xmlAddDocEntity
			         (doc, (xmlChar*)"",
			          XML_EXTERNAL_GENERAL_PARSED_ENTITY,
			          (xmlChar*)"default-public-id",
			          (xmlChar*)"default-system-id",
			          NULL) ;
			break ;
		case XML_EXTERNAL_GENERAL_UNPARSED_ENTITY:
			result = (xmlNode*) xmlAddDocEntity
			         (doc, (xmlChar*)"",
			          XML_EXTERNAL_GENERAL_UNPARSED_ENTITY,
			          (xmlChar*)"default-public-id",
			          (xmlChar*)"default-system-id",
			          (xmlChar*)"default-ndata") ;
			break ;
		case XML_INTERNAL_PARAMETER_ENTITY:
			result = (xmlNode*) xmlAddDocEntity
			         (doc, (xmlChar*)"",
			          XML_INTERNAL_PARAMETER_ENTITY,
			          NULL, NULL,
			          (xmlChar*)"default-param-value") ;
			break ;
		case XML_EXTERNAL_PARAMETER_ENTITY:
			result = (xmlNode*) xmlAddDocEntity
			         (doc, (xmlChar*)"",
			          XML_EXTERNAL_PARAMETER_ENTITY,
			          NULL, NULL,
			          (xmlChar*)"default-param-value") ;
			break ;
		default:
			break ;
		}
		break ;
	case XML_DOCUMENT_NODE:
	case XML_DOCUMENT_TYPE_NODE:
	case XML_DOCUMENT_FRAG_NODE:
	case XML_NOTATION_NODE:
	case XML_ELEMENT_DECL:
	case XML_ATTRIBUTE_DECL:
	case XML_NAMESPACE_DECL:
	case XML_XINCLUDE_START:
	case XML_XINCLUDE_END:
	default:
		result = xmlNewNode (NULL, (xmlChar*)"");
		break;
	}
	return result;
}

/**
 *This function is to be called once the user
 *clicks the OK button of the node type picker,
 *which means she wants to add a child node to the
 *currently selected xml node.
 *@param a_this the current instance of #MlViewTreeEditor .
 */
static void
handle_nt_picker_ok_button_clicked_to_add_child (MlViewTreeEditor *a_this)
{
	NodeTypeDefinition *node_type_def = NULL ;
	guint node_addion_status=0;
	MlViewNodeTypePicker *picker;
	xmlNodePtr xml_node = NULL;
	MlViewXMLDocument *xml_doc = NULL;
	xmlDoc *native_doc = NULL ;
	xmlNs *ns = NULL;
	gchar *node_name_or_content = NULL, *local_name = NULL ;
	gchar *node_path =NULL, *parsed_name = NULL,
		  *parsed_name_end, **str_ptr = NULL;

	mlview::AppContext *context = mlview::AppContext::get_instance () ;
	THROW_IF_FAIL (context) ;
	GtkTreeIter iter={0} ;
	enum MlViewStatus status=MLVIEW_OK ;

	THROW_IF_FAIL (a_this
	                  && MLVIEW_IS_TREE_EDITOR (a_this)
	                  && PRIVATE (a_this)) ;
	picker =
	    mlview_tree_editor_get_node_type_picker
	    (a_this);
	THROW_IF_FAIL (picker != NULL);
	node_name_or_content =
	    mlview_node_type_picker_get_node_name_or_content
	    (picker);

	node_type_def =
	    mlview_node_type_picker_get_selected_node_type
	    (picker);
	THROW_IF_FAIL (node_type_def) ;

	status = mlview_tree_editor_get_cur_sel_start_iter
	         (a_this, &iter) ;
	THROW_IF_FAIL (status == MLVIEW_OK) ;

	xml_doc =
	    mlview_tree_editor_get_mlview_xml_doc (a_this);
	THROW_IF_FAIL (xml_doc) ;

	if (node_name_or_content != NULL
	        && !mlview_utils_is_white_string (node_name_or_content)) {

		xml_node =
		    new_xml_node (node_type_def, xml_doc);
		mlview_xml_document_get_node_path (xml_doc,
		                                   xml_node,
		                                   &node_path) ;
		if (!node_path) {
			mlview_utils_trace_debug ("Could not get node path") ;
			return ;
		}
		native_doc = mlview_xml_document_get_native_document
		             (xml_doc);
		THROW_IF_FAIL (native_doc) ;

		switch (node_type_def->node_type) {
		case XML_ELEMENT_NODE:
		case XML_PI_NODE:
		case XML_DTD_NODE:
			str_ptr = &parsed_name_end ;
			mlview_utils_parse_element_name (node_name_or_content,
			                                 (gchar**)str_ptr) ;
			if (parsed_name_end) {
				parsed_name =
				    g_strndup (node_name_or_content,
				               (gulong)parsed_name_end - (gulong)node_name_or_content  + 1) ;
			} else {
				context->error (_("Node name is not well formed")) ;
				return ;
			}
			mlview_utils_parse_full_name (xml_node, parsed_name, &ns, &local_name);
			if (local_name != NULL) {
				/*
				 *A hack to make sure
				 *the node has a name before
				 *we add it to the tree otherwise
				 *we won't be able to address it via xpath.
				 *TODO: do the same for the other types
				 *of node and other type of node addition
				 *to the tree.
				 */
				xmlNodeSetName (xml_node, (xmlChar*)local_name) ;
				/*
				mlview_xml_document_get_node_path (xml_doc,
				                                   xml_node,
				                                   &node_path) ;
				if (!node_path) {
				        mlview_utils_trace_debug ("Could not get node path") ;
				        return ;
				}
				mlview_xml_document_set_node_name
				        (xml_doc, node_path,
				         local_name, TRUE);
				g_free (local_name);
				local_name = NULL;
				if (node_path) {
				        g_free (node_path) ;
				        node_path = NULL ;
				}
				*/
			}
			break;
		case XML_ENTITY_DECL:
			mlview_utils_parse_full_name
			(xml_node, node_name_or_content,
			 &ns, &local_name);
			if (local_name != NULL) {
				xmlNodeSetName (xml_node, (xmlChar*)local_name) ;
				/*
				mlview_xml_document_set_entity_node_name
				        (xml_doc, (xmlEntity*)xml_node,
				         native_doc->intSubset,
				         local_name, TRUE);
				g_free (local_name);
				local_name = NULL;
				*/
			}
			break ;
		default:
			mlview_xml_document_set_node_content
			(xml_doc, node_path,
			 node_name_or_content, FALSE);
			break;
		}
	} else if (node_type_def->node_type == XML_TEXT_NODE &&
	           mlview_utils_is_white_string (node_name_or_content)) {
		xml_node =
		    new_xml_node (node_type_def, xml_doc);
		xmlNodeSetContent (xml_node, (xmlChar*)node_name_or_content) ;

	} else {
		context->error (_("Nodes of the selected "
						  "type cannot have an empty content."));
	}

	if (xml_node) {
		node_addion_status =
		    mlview_tree_editor_add_child_node (a_this,
		                                       &iter,
		                                       xml_node);
		if (!node_addion_status
		        && (node_type_def->node_type == XML_PI_NODE
		            || node_type_def->node_type == XML_ELEMENT_NODE)) {
			mlview_utils_parse_full_name
			(xml_node, node_name_or_content,
			 &ns, &local_name);
			if (ns) {
				xmlSetNs (xml_node, ns);
			} else {
				xml_node->ns = NULL;
			}
			if (local_name) {
				g_free (local_name);
				local_name = NULL;
			}
		}
	}
}

/**
 *This function is to be called once the user
 *clicks the OK button of the node type picker,
 *which means she wants to insert a sibling node to
 *the currently selected xml node.
 *@param a_this the current instance of #MlViewTreeEditor .
 */
static void
handle_nt_picker_ok_button_clicked_to_insert_sibling_node (MlViewTreeEditor *a_this)
{
	NodeTypeDefinition * node_type_def=0 ;
	MlViewNodeTypePicker *picker;
	xmlNodePtr xml_node = NULL;
	MlViewXMLDocument *xml_doc = NULL;
	xmlNs *ns = NULL;
	gchar *node_name_or_content = NULL, *local_name = NULL, 
		  *parsed_name = NULL, *parsed_name_end = NULL;
	GtkTreeIter iter={0} ;
	gboolean *prev_ptr=FALSE ;
	xmlDoc *native_doc = NULL ;
	enum MlViewStatus status=MLVIEW_OK ;

	mlview::AppContext *context = mlview::AppContext::get_instance () ;
	THROW_IF_FAIL (context) ;
	THROW_IF_FAIL (a_this
	                  && MLVIEW_IS_TREE_EDITOR (a_this)
	                  && PRIVATE (a_this)) ;
	picker = mlview_tree_editor_get_node_type_picker (a_this);
	THROW_IF_FAIL (picker != NULL);
	node_name_or_content =
	    mlview_node_type_picker_get_node_name_or_content
	    (picker);
	if (node_name_or_content != NULL
	        && !mlview_utils_is_white_string (node_name_or_content)) {
		node_type_def =
		    mlview_node_type_picker_get_selected_node_type
		    (picker);
		xml_doc =
		    mlview_tree_editor_get_mlview_xml_doc
		    (a_this);
		THROW_IF_FAIL (xml_doc) ;
		native_doc = mlview_xml_document_get_native_document
		             (xml_doc) ;
		THROW_IF_FAIL (native_doc) ;
		xml_node =
		    new_xml_node (node_type_def, xml_doc);
		switch (node_type_def->node_type) {
		case XML_ELEMENT_NODE:
		case XML_PI_NODE:
		case XML_ENTITY_DECL:
			mlview_utils_parse_element_name (node_name_or_content,
			                                 (gchar**)&parsed_name_end) ;
			if (parsed_name_end) {
				parsed_name =
				    g_strndup (node_name_or_content,
				               (gulong)parsed_name_end - (gulong)node_name_or_content  + 1) ;
			} else {
				context->error (_("Node name is not well formed")) ;
				return ;
			}
			mlview_utils_parse_full_name (xml_node, parsed_name, 
										  &ns, &local_name);
			if (local_name != NULL) {
				xmlNodeSetName (xml_node, (xmlChar*)local_name) ;
			}
			break;
		default:
			xmlNodeSetContent (xml_node,
			                   (xmlChar*)node_name_or_content) ;
			/*
			mlview_xml_document_get_node_path (xml_doc,
			                                   xml_node,
			                                   &node_path) ;
			if (!node_path) {
			        mlview_utils_trace_debug ("Could not get node path") ;
			        return ;
			}
			mlview_xml_document_set_node_content
			        (xml_doc, node_path,
			         node_name_or_content, FALSE);
			if (node_path) {
			        g_free (node_path) ;
			        node_path = NULL ;
			}
			*/
			break;
		}
		/*
		 *retrieve a flag set to indicate 
		 *if the sibling has to be inserted 
		 *before or after the current xml node
		 */
		prev_ptr = (gboolean*)
		           gtk_object_get_data (GTK_OBJECT (a_this),
		                                "prev");
		status = mlview_tree_editor_get_cur_sel_start_iter
		         (a_this, &iter) ;
		THROW_IF_FAIL (status == MLVIEW_OK) ;
		status = mlview_tree_editor_insert_sibling_node
		         (a_this, &iter, xml_node,
		          GPOINTER_TO_INT (prev_ptr)) ;

		if (status == MLVIEW_OK
		        && (node_type_def->node_type == XML_ELEMENT_NODE
		            || node_type_def->node_type == XML_PI_NODE)) {
			mlview_utils_parse_full_name
			(xml_node, node_name_or_content,
			 &ns, &local_name);
			if (ns) {
				xmlSetNs (xml_node, ns);
			} else {
				xml_node->ns = NULL;
			}
			if (local_name) {
				g_free (local_name);
				local_name = NULL;
			}
			mlview_tree_editor_update_visual_node
			(a_this, &iter, FALSE) ;
		}
	}
}

/**
 *Returns a copy of the search string typed by the user.
 *@param a GtkDialog that represents the "search" dialog. Must
 *have been constructed by a call to 
 *get_search_dialog().
 *@return a pointer to the search string typed by the user.
 *The return string must _NOT_ be freed by the user.
 */
static const gchar *
get_search_string (GtkWidget *a_search_dialog)
{
	GtkWidget *text_entry = NULL ;

	THROW_IF_FAIL (a_search_dialog) ;

	text_entry = (GtkWidget*)g_object_get_data (G_OBJECT (a_search_dialog),
	             "SearchEntry") ;
	if (!text_entry || !GTK_IS_ENTRY (text_entry)) {
		mlview_utils_trace_debug
		("Retrieving data associated to "
		 "SearchEntry from the Search Dialog failed. "
		 "The Search dialog may not be a valid one.");
		return NULL ;
	}
	return gtk_entry_get_text (GTK_ENTRY (text_entry)) ;
}

/**
 *Gets the search configuration from what the
 *user has selected through the search dialog.
 *@param a_search_dialog the search dialog. Must have
 *been constructed by a call to get_search_dialog() .
 *@param a_config the search configuration (i.e: whether to
 *look into node names, content, attribute names etc ...)
 *@return MLVIEW_OK upon successful completion, an error code
 *otherwise.
 */
static enum MlViewStatus
get_search_config (GtkWidget *a_search_dialog,
		   struct SearchConfig *a_config)
{
    GtkWidget *widget = NULL ;

    g_return_val_if_fail (a_search_dialog
			  && a_config,
			  MLVIEW_BAD_PARAM_ERROR) ;

    widget = (GtkWidget*)g_object_get_data (G_OBJECT (a_search_dialog),
					    "MatchCaseButton") ;
    g_return_val_if_fail (widget
			  && GTK_IS_CHECK_BUTTON (widget),
			  MLVIEW_ERROR) ;
    a_config->ignore_case =
	gtk_toggle_button_get_active
	(GTK_TOGGLE_BUTTON (widget)) ;
    if (a_config->ignore_case == TRUE)
    {
	a_config->ignore_case = FALSE ;
    } else
    {
	a_config->ignore_case = TRUE ;
    }
/*
  widget = (GtkWidget*)g_object_get_data (G_OBJECT (a_search_dialog),
  "SearchInNodeNamesButton") ;
  g_return_val_if_fail (widget
  && GTK_IS_CHECK_BUTTON (widget),
  MLVIEW_ERROR) ;
  if (gtk_toggle_button_get_active
  (GTK_TOGGLE_BUTTON (widget)) == TRUE) {
  a_config->where   = (enum WhereInTheNodeBitmap)
  (a_config->where | NODE_NAME);
  }
  widget = (GtkWidget*)g_object_get_data (G_OBJECT (a_search_dialog),
  "SearchInAttrNamesButton") ;
  g_return_val_if_fail (widget
  && GTK_IS_CHECK_BUTTON (widget),
  MLVIEW_ERROR) ;
  if (gtk_toggle_button_get_active
  (GTK_TOGGLE_BUTTON (widget)) == TRUE) {
  a_config->where = (enum WhereInTheNodeBitmap)
  (a_config->where | NODE_ATTRIBUTE_NAME) ;
  }
  widget = (GtkWidget*)g_object_get_data (G_OBJECT (a_search_dialog),
  "SearchInAttrValuesButton") ;
  g_return_val_if_fail (widget
  && GTK_IS_CHECK_BUTTON (widget),
  MLVIEW_ERROR) ;
  if (gtk_toggle_button_get_active
  (GTK_TOGGLE_BUTTON (widget)) == TRUE) {
  a_config->where = (enum WhereInTheNodeBitmap)
  (a_config->where | NODE_ATTRIBUTE_VALUE) ;
  }
  widget = (GtkWidget*)g_object_get_data (G_OBJECT (a_search_dialog),
  "SearchInNodeContentButton") ;
  g_return_val_if_fail (widget
  && GTK_IS_CHECK_BUTTON (widget),
  MLVIEW_ERROR) ;
  if (gtk_toggle_button_get_active
  (GTK_TOGGLE_BUTTON (widget)) == TRUE) {
  a_config->where = (enum WhereInTheNodeBitmap)
  (a_config->where| NODE_CONTENT) ;
  }
*/

// Define search option mask
    int bitmap = 0;

    mlview::PrefsCategorySearch *m_prefs =
	dynamic_cast<mlview::PrefsCategorySearch*> (
	    mlview::Preferences::get_instance ()
	    ->get_category_by_id ("search"));

    if (m_prefs != NULL)
    {
	if (m_prefs->search_among_node_names ())
	    bitmap |= NODE_NAME;
	if (m_prefs->search_among_node_values ())
	    bitmap |= NODE_CONTENT;
	if (m_prefs->search_among_attr_names ())
	    bitmap |= NODE_ATTRIBUTE_NAME;
	if (m_prefs->search_among_attr_values ())
	    bitmap |= NODE_ATTRIBUTE_VALUE;
    }
    else
    {
	bitmap = NODE_NAME | NODE_CONTENT;
    }

    a_config->where = (enum WhereInTheNodeBitmap) (a_config->where
						   | bitmap) ;

    a_config->search_string = (gchar*)
	get_search_string (a_search_dialog) ;

    return MLVIEW_OK ;
}

/**
 *Builds and returns the "search" dialog.
 *@param a_this the current instance of #MlViewTreeEditor.
 *@return the dialog.
 */
static GtkWidget*
get_search_dialog (MlViewTreeEditor *a_this)
{
	GladeXML *glade_xml = NULL ;
	GtkWidget *widget = NULL, *dialog_widget = NULL ;

	THROW_IF_FAIL (a_this && MLVIEW_IS_TREE_EDITOR (a_this) && PRIVATE (a_this)) ;
	mlview::AppContext *context = mlview::AppContext::get_instance () ;
	THROW_IF_FAIL (context) ;

	if (!PRIVATE (a_this)->search_dialog) {
		gchar *glade_file_path=NULL ;
		GtkWidget *main_win = NULL ;

		glade_file_path =
		    gnome_program_locate_file
		    (NULL,
		     GNOME_FILE_DOMAIN_APP_DATADIR,
		     PACKAGE "/mlview-search-box.glade", TRUE,
		     NULL) ;
		THROW_IF_FAIL (glade_file_path) ;
		glade_xml = glade_xml_new
		            (glade_file_path,
		             "MlViewSearchBox", NULL) ;
		if (!glade_xml) {
			mlview_utils_trace_debug
			("glade xml file loading failed") ;
			return NULL ;
		}
		dialog_widget =
		    glade_xml_get_widget
		    (glade_xml,
		     "MlViewSearchBox") ;
		if (!dialog_widget) {
			mlview_utils_trace_debug
			("getting widget from glade failed") ;
			goto cleanup ;
		}
		/*
		 * Now set a callback to detect when the user
		 * types esc
		 */
		g_signal_connect (G_OBJECT (dialog_widget),
		                  "key-press-event",
		                  G_CALLBACK (key_pressed_in_search_dialog_cb),
		                  NULL) ;

		widget = glade_xml_get_widget (glade_xml,
		                               "SearchEntry") ;
		if (!widget) {
			mlview_utils_trace_debug
			("getting SearchEntry "
			 "from glade file failed") ;
			goto cleanup ;
		}
		g_object_set_data (G_OBJECT (dialog_widget),
		                   "SearchEntry", widget) ;
		gtk_entry_set_activates_default (GTK_ENTRY (widget), TRUE) ;

		widget = glade_xml_get_widget
		         (glade_xml,
		          "MatchCaseButton") ;
		if (!widget) {
			mlview_utils_trace_debug
			("getting MatchCaseButton from "
			 "glade file failed") ;
			goto cleanup ;
		}
		g_object_set_data (G_OBJECT (dialog_widget),
		                   "MatchCaseButton",
		                   widget) ;
		/*
		widget = glade_xml_get_widget 
		        (glade_xml, 
		         "SearchInNodeNamesButton") ;
		if (!widget) {
		        mlview_utils_trace_debug 
		                ("getting from SearchInNodeNamesButton "
		                 "glade file failed") ;
		        goto cleanup ;
		}
		g_object_set_data (G_OBJECT (dialog_widget),
		                   "SearchInNodeNamesButton",
		                   widget) ;
		widget = glade_xml_get_widget 
		        (glade_xml, 
		         "SearchInAttrNamesButton") ;
		if (!widget) {
		        mlview_utils_trace_debug 
		                ("getting SearchInAttrNamesButton from"
		                 "glade file failed") ;
		        goto cleanup ;
		}
		g_object_set_data (G_OBJECT (dialog_widget),
		                   "SearchInAttrNamesButton",
		                   widget) ;
		widget = glade_xml_get_widget 
		        (glade_xml, 
		         "SearchInAttrValuesButton") ;
		if (!widget) {
		        mlview_utils_trace_debug 
		                ("getting SearchInAttrValuesButton from"
		                 "glade file failed") ;
		        goto cleanup ;
		}
		g_object_set_data (G_OBJECT (dialog_widget),
		                   "SearchInAttrValuesButton", 
		                   widget) ;
		widget = glade_xml_get_widget 
		        (glade_xml, 
		         "SearchInNodeContentButton") ;
		if (!widget) {
		        mlview_utils_trace_debug 
		                ("getting SearchInNodeContentButton from"
		                 "glade file failed") ;
		        goto cleanup ;
		}
		g_object_set_data (G_OBJECT (dialog_widget),
		                   "SearchInNodeContentButton", 
		                   widget) ;
		*/
		/*
		 *Connect the appropriate callbacks to the buttons signals
		 */
		glade_xml_signal_connect_data (glade_xml,
		                               "search_win_cancel_button_clicked_cb",
		                               G_CALLBACK (search_win_cancel_button_clicked_cb),
		                               a_this) ;
		glade_xml_signal_connect_data (glade_xml,
		                               "search_win_prev_button_clicked_cb",
		                               G_CALLBACK (search_win_prev_button_clicked_cb),
		                               a_this) ;
		glade_xml_signal_connect_data (glade_xml,
		                               "search_win_next_button_clicked_cb",
		                               G_CALLBACK (search_win_next_button_clicked_cb),
		                               a_this) ;

		/*
		 *Make the "search next" button be the default widget, i.e
		 *the one to be activated when the user hits the 'enter' button.
		 */
		widget = glade_xml_get_widget (glade_xml, "NextButton") ;
		if (!widget) {
			mlview_utils_trace_debug
			("getting NextButton from"
			 "glade file failed") ;
			goto cleanup ;
		}
		/*gtk_widget_grab_default (widget) ;*/

		widget = glade_xml_get_widget (glade_xml, "CancelButton") ;
		if (!widget) {
			mlview_utils_trace_debug ("getting CancelButton from"
			                          "glade file failed") ;
			goto cleanup ;
		}
		g_object_set_data (G_OBJECT (dialog_widget),
		                   "CancelButton",
		                   widget) ;
		/*
		 *When the user clicks the close button, hide the window
		 *instead of destroying it.
		 */
		g_signal_connect (G_OBJECT (dialog_widget),
		                  "delete-event",
		                  G_CALLBACK (gtk_widget_hide_on_delete),
		                  NULL) ;

		/*
		 *If we can access the main window of the application,
		 *make sure this dialog always stays on top of it
		 */
		main_win = (GtkWidget*) context->get_element ("MlViewMainWindow") ;

		if (main_win) {
			GList *list = NULL;

			list = gtk_container_get_children (GTK_CONTAINER (main_win));
			if (list == NULL) {
				mlview_utils_trace_debug ("getting children of "
				                          "main window failed");
				goto cleanup;
			}

			gtk_box_pack_start (GTK_BOX (list->data),
					    GTK_WIDGET (dialog_widget),
					    FALSE,
					    FALSE,
					    0);
			gtk_box_reorder_child (GTK_BOX (list->data),
					       GTK_WIDGET (dialog_widget),
					       2);
			gtk_widget_hide (GTK_WIDGET (dialog_widget));
		} else {
			mlview_utils_trace_debug ("getting main window "
			                          "from context failed");
			goto cleanup;
		}

		if (main_win && GTK_IS_WINDOW (main_win)) {
			gtk_window_set_transient_for
			(GTK_WINDOW (dialog_widget),
			 GTK_WINDOW (main_win)) ;
		}
		PRIVATE (a_this)->search_dialog = dialog_widget ;
		dialog_widget = NULL ;

	}

cleanup:
	if (dialog_widget) {
		gtk_widget_destroy (dialog_widget) ;
		dialog_widget = NULL ;
	}
	if (glade_xml) {
		g_object_unref (glade_xml) ;
		glade_xml = NULL ;
	}
	return  GTK_WIDGET (PRIVATE (a_this)->search_dialog) ;
}



typedef gboolean (*DragDataReceivedFunc) (GtkTreeDragDest *,
										  GtkTreePath *,
										  GtkSelectionData *) ;
static gboolean
drag_data_received (GtkTreeDragDest *a_drag_dest,
                    GtkTreePath *a_dest_path,
                    GtkSelectionData *a_sel_data)
{
	GtkTreeModel *src_model, *dest_model = NULL ;
	GtkTreePath *src_path = NULL ;
	MlViewTreeEditor *editor = NULL ;
	gboolean is_ok = TRUE, result = FALSE ;
	enum MlViewStatus status = MLVIEW_OK ;

	g_return_val_if_fail (a_drag_dest && a_dest_path
	                      && a_sel_data, FALSE) ;
	dest_model = GTK_TREE_MODEL (a_drag_dest) ;
	THROW_IF_FAIL (dest_model) ;
	is_ok = gtk_tree_get_row_drag_data
	        (a_sel_data, &src_model, &src_path) ;
	g_return_val_if_fail (is_ok == TRUE
	                      && src_model == dest_model,
	                      FALSE);
	editor = (MlViewTreeEditor*)g_object_get_data (G_OBJECT (a_drag_dest),
	         "MlViewTreeEditor") ;
	if (!editor) {
		mlview_utils_trace_debug ("editor != NULL failed.") ;
		goto cleanup ;
	}
	/*copy the xml node pointed to by src_path to the clipboard.*/
	status = mlview_tree_editor_copy_node2 (editor, src_path) ;
	if (status != MLVIEW_OK) {
		mlview_utils_trace_debug
		("status == MLVIEW_OK failed.") ;
		goto cleanup ;
	}
	/*
	 *paste the newly copied node from the clipboard
	 *to the new location
	 */
	status = mlview_tree_editor_paste_node_as_sibling2
	         (editor, a_dest_path,
	          TRUE/*paste as previous*/);
	if (status != MLVIEW_OK) {
		mlview_utils_trace_debug
		("status == MLVIEW_OK failed") ;
		goto cleanup ;
	}
	result = TRUE ;
cleanup:
	if (src_path) {
		gtk_tree_path_free (src_path) ;
		src_path = NULL ;
	}
	return result ;
}

typedef gboolean (*DragDataDeleteFunc) (GtkTreeDragSource *,
                                        GtkTreePath *) ;

static gboolean
drag_data_delete (GtkTreeDragSource *a_drag_src,
                  GtkTreePath *a_path)
{
	GtkTreeModel *model = NULL ;
	MlViewTreeEditor *editor = NULL ;
	enum MlViewStatus status =  MLVIEW_OK ;

	editor = (MlViewTreeEditor*)g_object_get_data (G_OBJECT (a_drag_src),
	         "MlViewTreeEditor") ;
	THROW_IF_FAIL (editor) ;
	model = GTK_TREE_MODEL (a_drag_src) ;
	THROW_IF_FAIL (model) ;
	status = mlview_tree_editor_cut_node2 (editor, a_path) ;
	if (status == MLVIEW_OK) {
		return TRUE ;
	}
	return FALSE ;
}

static enum MlViewStatus
backup_original_dnd_callbacks (MlViewTreeEditor *a_this)
{
	GtkTreeDragSourceIface *drag_source_iface = NULL;
	GtkTreeDragDestIface *drag_dest_iface = NULL;
	GtkTreeModel *model = NULL ;

	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_TREE_EDITOR (a_this)
	                      && PRIVATE (a_this),
	                      MLVIEW_BAD_PARAM_ERROR) ;
	model = mlview_tree_editor_get_model (a_this) ;
	g_return_val_if_fail (model && GTK_IS_TREE_STORE (model),
	                      MLVIEW_ERROR) ;
	drag_source_iface = GTK_TREE_DRAG_SOURCE_GET_IFACE (model) ;
	THROW_IF_FAIL (drag_source_iface) ;
	drag_dest_iface = GTK_TREE_DRAG_DEST_GET_IFACE (model) ;

	if (!PRIVATE (a_this)->backup_drag_data_delete) {
		PRIVATE (a_this)->backup_drag_data_delete =
		    (void*)drag_source_iface->drag_data_delete ;
	}
	if (!PRIVATE (a_this)->backup_drag_data_received) {
		PRIVATE (a_this)->backup_drag_data_received =
		    (void*)drag_dest_iface->drag_data_received ;
	}
	return MLVIEW_OK ;
}

/*
   NOT USED ATM
 *
static enum MlViewStatus
restore_original_dnd_callbacks (MlViewTreeEditor *a_this)
{
        GtkTreeDragSourceIface *drag_source_iface ;
        GtkTreeDragDestIface *drag_dest_iface ;
        GtkTreeModel *model = NULL ;
 
        g_return_val_if_fail (a_this 
                              && MLVIEW_IS_TREE_EDITOR (a_this)
                              && PRIVATE (a_this),
                              MLVIEW_BAD_PARAM_ERROR) ;
        model = mlview_tree_editor_get_model (a_this) ;
        g_return_val_if_fail (model && GTK_IS_TREE_STORE (model),
                              MLVIEW_ERROR) ;
        drag_source_iface = GTK_TREE_DRAG_SOURCE_GET_IFACE (model) ;
        THROW_IF_FAIL (drag_source_iface) ;
        drag_dest_iface = GTK_TREE_DRAG_DEST_GET_IFACE (model) ;
 
        drag_source_iface->drag_data_delete  =
                PRIVATE (a_this)->backup_drag_data_delete ;
        PRIVATE (a_this)->backup_drag_data_delete = NULL ;
        drag_dest_iface->drag_data_received =
                PRIVATE (a_this)->backup_drag_data_received ;
        PRIVATE (a_this)->backup_drag_data_received = NULL ;
 
        return MLVIEW_OK ;
}
*/

/**
 *Enables the Drad and drop functionality
 *on the PRIVATE (a_this)->tree_view.
 *This function must be called once a
 *PRIVATE (a_this)->tree_view has been allocated.
 */
static enum MlViewStatus
set_our_dnd_callbacks (MlViewTreeEditor *a_this)
{
	GtkTreeDragSourceIface *drag_source_iface ;
	GtkTreeDragDestIface *drag_dest_iface ;
	GtkTreeModel *model = NULL ;

	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_TREE_EDITOR (a_this)
	                      && PRIVATE (a_this),
	                      MLVIEW_BAD_PARAM_ERROR) ;
	model = mlview_tree_editor_get_model (a_this) ;
	g_return_val_if_fail (model && GTK_IS_TREE_STORE (model),
	                      MLVIEW_ERROR) ;
	drag_source_iface = GTK_TREE_DRAG_SOURCE_GET_IFACE (model) ;
	THROW_IF_FAIL (drag_source_iface) ;
	drag_dest_iface = GTK_TREE_DRAG_DEST_GET_IFACE (model) ;
	THROW_IF_FAIL (drag_dest_iface) ;
	backup_original_dnd_callbacks (a_this) ;
	drag_source_iface->drag_data_delete  = drag_data_delete ;
	drag_dest_iface->drag_data_received = drag_data_received ;
	return MLVIEW_OK ;
}

static gboolean
idle_add_grab_focus_on_tree_view (MlViewTreeEditor *a_this)
{
	GtkTreeView *tree_view = NULL ;

	g_return_val_if_fail (a_this && MLVIEW_IS_TREE_EDITOR (a_this),
	                      FALSE) ;

	tree_view = mlview_tree_editor_get_tree_view (a_this) ;

	THROW_IF_FAIL (tree_view) ;
	if (GTK_WIDGET_CAN_FOCUS (tree_view)) {
		gtk_widget_grab_focus (GTK_WIDGET (tree_view)) ;
	} else {
		mlview_utils_trace_debug ("Args !!, tree_view can't focus\n") ;
	}
	return FALSE ;
}

static gboolean
idle_add_scroll_to_cell (MlViewTreeEditor *a_this)
{
	GtkTreePath *tree_path = NULL ;
	GtkTreeView *tree_view = NULL ;

	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_TREE_EDITOR (a_this),
	                      FALSE) ;
#ifdef MLVIEW_VERBOSE

	g_print ("in idle_add_scroll_to_cell(): \n") ;
#endif

	tree_path = (GtkTreePath*)g_object_get_data (G_OBJECT (a_this),
	            "tree-path-to-scroll-to") ;
	if (!tree_path) {
#ifdef MLVIEW_VERBOSE
		g_print ("in idle_add_scroll_to_cell(): oups, no tree_path given\n") ;
#endif

		return FALSE ;
	}

	tree_view = mlview_tree_editor_get_tree_view (a_this) ;
	THROW_IF_FAIL (tree_view) ;


	gtk_tree_view_scroll_to_cell (tree_view, tree_path,
	                              NULL, FALSE, 0, 0) ;

#ifdef MLVIEW_VERBOSE

	g_print ("in idle_add_scroll_to_cell(): Scrolled to cell OK\n") ;
#endif

	/*
	        if (tree_path) {
	                gtk_tree_path_free (tree_path) ;
	                tree_path = NULL ;
	        }
	*/
	return FALSE ;
}





/*********************
 *Public methods
 ********************/

/**
 *the standard type id builder of the MlViewTreeEditor object. 
 *@return the type id of the MlViewTreeEditor object. 
 */
guint
mlview_tree_editor_get_type (void)
{
	static guint type = 0;

	if (!type) {
		static const GTypeInfo type_info = {
		                                       sizeof (MlViewTreeEditorClass),
		                                       NULL, NULL,
		                                       (GClassInitFunc)
		                                       mlview_tree_editor_class_init,
		                                       NULL, NULL,
		                                       sizeof (MlViewTreeEditor),
		                                       0,
		                                       (GInstanceInitFunc)
		                                       mlview_tree_editor_init
		                                   };
		type = g_type_register_static (GTK_TYPE_VBOX,
		                               "MlViewTreeEditor",
		                               &type_info, (GTypeFlags)0);
	}
	return type;
}

/**
 *Helper function that builds a GtkTreeModel out of a
 *libxml2 infoset tree.
 *@param a_node the xml node to build the gtktreemodel from.
 *@param a_parent_iter an tree iterator that points to the 
 *current parent tree row
 *@param a_model in/out parameter. The built treemodel.
 *@return MLVIEW_OK upon successful completion, an error
 *code otherwise.
 */
enum MlViewStatus
mlview_tree_editor_build_tree_model_from_xml_tree (MlViewTreeEditor * a_this,
        const xmlNode * a_node,
        GtkTreeIter * a_ref_iter,
        enum MlViewTreeInsertType a_type,
        GtkTreeModel ** a_model)
{
	return MLVIEW_TREE_EDITOR_CLASS
	       (G_OBJECT_GET_CLASS (a_this))->build_tree_model_from_xml_tree
	       (a_this, a_node, a_ref_iter, a_type, a_model);
}


/**
 *Walks through the list of attributes of
 *the instance of xmlNode and
 *construct a string which looks like
 *'attrname0="attrval0" attrname1=attrvall'
 *where each attrnamei is
 *@param a_node the  node to build the attribute list from.
 *@return the newly built attribute list string, or NULL if something
 *bad happened.
 */
gchar *
mlview_tree_editor_build_attrs_list_str (MlViewTreeEditor *a_this,
					 xmlNode * a_node,
					 bool selected)
{
    enum MlViewStatus status = MLVIEW_OK ;
    xmlAttrPtr attr_iter = NULL ;
    gchar *attval_col = NULL;
    gchar *attname_col = NULL ;
    gchar *attribute=NULL, *result = NULL, *tmp = NULL ;
    gchar *escaped_content = NULL;
    guint esc_content_len = 0 ;

    THROW_IF_FAIL (a_node && a_node->type == XML_ELEMENT_NODE && a_this
		   && MLVIEW_IS_TREE_EDITOR (a_this));

    if (selected) {
	GdkColor color =
	    PRIVATE (a_this)->style->fg[GTK_STATE_SELECTED];
	attname_col  = (gchar*)mlview::gdk_color_to_html_string (color).c_str();

	attval_col = g_strdup (attname_col);
    } else {
	attname_col = (gchar*)mlview_tree_editor_get_colour_string
	    (a_this, XML_ATTRIBUTE_NODE);
// lame hack as there is no way to know when it's the attribute value
	attval_col = (gchar*)mlview_tree_editor_get_colour_string
	    (a_this, XML_ATTRIBUTE_DECL);
    }

    for (attr_iter = a_node->properties;
	 attr_iter;
	 attr_iter = attr_iter->next) {
	xmlChar *content = NULL;
	if (!attr_iter->name)
	    continue ;
	content = xmlGetProp (a_node, attr_iter->name) ;
	if (content) {
	    status = mlview_utils_escape_predef_entities_in_str
		((gchar*)content, &escaped_content,
		 &esc_content_len) ;
	    if (status != MLVIEW_OK)
		goto out ;
	    if (!escaped_content) {
		escaped_content = g_strdup ((gchar*)content) ;
	    }
	    attribute = g_strdup_printf
		("<span foreground=\"%s\">%s="
		 "<span foreground=\"%s\">\"%s\"</span></span>",
		 attname_col, attr_iter->name,
		 attval_col, escaped_content);
	} else {
	    attribute = g_strdup_printf
		("<span foreground=\"%s\">%s</span>",
		 attname_col, attr_iter->name);
	}

    out:
	if (content) {
	    xmlFree (content) ;
	    content = NULL ;
	}
	if (escaped_content) {
	    g_free (escaped_content) ;
	    escaped_content = NULL ;
	}

	if (result) {
	    tmp = g_strdup_printf ("%s %s", result, attribute);
	    g_free (result);
	    result = tmp;
	} else {
	    result = attribute;
	}
    }

    return result;
}

/**
 *MlViewTreeEditor instance builder.
 *@param a_context the application context.
 *@return the newly built app context or NULL
 *if an error arises.
 */
GtkWidget *
mlview_tree_editor_new ()
{
	MlViewTreeEditor *editor = NULL;

	editor = (MlViewTreeEditor*)g_object_new (MLVIEW_TYPE_TREE_EDITOR, NULL);

	mlview_tree_editor_construct (editor);

	return GTK_WIDGET (editor);
}

/**
 *The instance initialyzer of #MlViewTreeEditor.
 *Should be called by the constructor.
 *@param a_editor the current instance of #MlViewTreeEditor.
 *@param a_context the mlview application context.
 */
void
mlview_tree_editor_construct (MlViewTreeEditor *a_this)
{
	gint keybindings_size = sizeof (gv_keybindings) / sizeof (struct MlViewKBDef) ;
	THROW_IF_FAIL (a_this && PRIVATE (a_this)) ;

	PRIVATE (a_this)->kb_eng = mlview_kb_eng_new () ;
	if (!PRIVATE (a_this)->kb_eng) {
		LOG_TO_ERROR_STREAM ("Could not instanciate MlviewKBEng") ;
		return ;
	}
	mlview_kb_eng_register_key_bindings (PRIVATE (a_this)->kb_eng,
	                                     gv_keybindings,
	                                     keybindings_size) ;

}

/**
 *Returns the color code of a given type of xml node.
 *@param a_type the type of xml node to consider.  
 *@return string representing the color code of the node
 *in the form "#006FFF" for example.
 */
const gchar*
mlview_tree_editor_get_colour_string (MlViewTreeEditor *a_this,
				      xmlElementType a_type)
{
    gchar *result = NULL ;

    THROW_IF_FAIL (a_this && MLVIEW_IS_TREE_EDITOR (a_this)) ;

    mlview::PrefsCategoryTreeview *m_prefs =
	dynamic_cast<mlview::PrefsCategoryTreeview*> (
	    mlview::Preferences::get_instance ()
	    ->get_category_by_id ("treeview"));

    switch (a_type) {
    case XML_ELEMENT_NODE:
	result =
	    g_strdup (m_prefs->get_color_for_type
		      (mlview::MLVIEW_XML_ELEMENT_NODE_COLOUR).c_str());
	break ;
    case XML_ATTRIBUTE_NODE:
	result =
	    g_strdup (m_prefs->get_color_for_type
		      (mlview::MLVIEW_XML_ATTR_NAME_COLOUR).c_str());
	break ;
    case XML_ATTRIBUTE_DECL: // Hackily used as attribute values colour
	result =
	    g_strdup (m_prefs->get_color_for_type
		      (mlview::MLVIEW_XML_ATTR_VAL_COLOUR).c_str());
	break ;
    case XML_TEXT_NODE:
	result =
	    g_strdup (m_prefs->get_color_for_type
		      (mlview::MLVIEW_XML_TEXT_NODE_COLOUR).c_str());
	break ;
    case XML_CDATA_SECTION_NODE:
	result = (gchar*)"#000000" ;
	break ;
    case XML_ENTITY_REF_NODE:
	result = (gchar*)"#000000" ;
	break ;
    case XML_ENTITY_NODE:
	result = (gchar*)"#000000";
	break ;
    case XML_PI_NODE:
	result =
	    g_strdup (m_prefs->get_color_for_type
		      (mlview::MLVIEW_XML_PI_NODE_COLOUR).c_str());
	break ;
    case XML_COMMENT_NODE:
	result =
	    g_strdup (m_prefs->get_color_for_type
		      (mlview::MLVIEW_XML_COMMENT_NODE_COLOUR).c_str());
	break ;
    case XML_DOCUMENT_NODE:
	result =
	    g_strdup (m_prefs->get_color_for_type
		      (mlview::MLVIEW_XML_DOCUMENT_NODE_COLOUR).c_str());
	break ;
    case XML_DOCUMENT_TYPE_NODE:
	result = (gchar*)"#000000" ;
	break ;
    case XML_DOCUMENT_FRAG_NODE:
	result = (gchar*)"#000000" ;
	break ;
    case XML_NOTATION_NODE:
	result = (gchar*)"#000000";
	break ;
    case XML_HTML_DOCUMENT_NODE:
	result = (gchar*)"#000000";
	break ;
    case XML_DTD_NODE:
	result =
	    g_strdup (m_prefs->get_color_for_type
		      (mlview::MLVIEW_XML_DTD_NODE_COLOUR).c_str());
	break ;
    case XML_ELEMENT_DECL:
	result = (gchar*) "#000000";
	break ;
/*
  case XML_ATTRIBUTE_DECL:
  result = (gchar*)"#000000";
  break ;
*/
    case XML_ENTITY_DECL:
	result =
	    g_strdup (m_prefs->get_color_for_type
		      (mlview::MLVIEW_XML_ENTITY_DECL_NODE_COLOUR).c_str());
	break ;
    case XML_NAMESPACE_DECL:
	result = (gchar*)"#000000";
	break ;
    case XML_XINCLUDE_START:
	result = (gchar*)"#000000";
	break ;
    case XML_XINCLUDE_END:
	result = (gchar*) "#000000";
	break ;
    default:
	result = (gchar*)"#000000" ;
    }

    if (!result)
	result = (gchar*)"#000000" ;
    return result ;
}

/**
 *Builds a string of the form 
 *<!ENTITY  internal-general-entity "abracadabra">
 *from a given internal general entity data structure.
 *@param a_entity the input internal general entity.
 *Its ->etype field must be of type XML_INTERNAL_GENERAL_ENTITY, 
 *otherwise the MLVIEW_BAD_PARAM_ERROR is returned.
 *@param a_string out parameter. A pointer to the returned string.
 *@return MLVIEW_OK upon successful completion, an error code
 *otherwise.
 */
enum MlViewStatus
mlview_tree_editor_internal_general_entity_to_string (MlViewTreeEditor *a_this,
						      xmlEntity *a_entity,
						      bool selected,
						      gchar **a_string)
{
    gchar *sep = NULL,*tmp_str = NULL ;
    gchar *colour = NULL, *esc_name = NULL, *esc_content = NULL ;

    THROW_IF_FAIL (a_entity && a_this && MLVIEW_IS_TREE_EDITOR (a_this)
		   && a_entity->etype == XML_INTERNAL_GENERAL_ENTITY
		   && a_entity->name && a_entity->content && a_string) ;

    if (selected) {
	GdkColor color =
	    PRIVATE (a_this)->style->fg[GTK_STATE_SELECTED];
	colour = (gchar*) mlview::gdk_color_to_html_string (color).c_str();
    } else {
	colour = (gchar*) mlview_tree_editor_get_colour_string
	    (a_this, XML_ENTITY_DECL) ;
    }
    THROW_IF_FAIL (colour) ;
    if (strchr ((char*)a_entity->content, '"'))
	sep = (gchar*) "'" ;
    else
	sep = (gchar*) "\"" ;

    esc_name = g_markup_escape_text ((gchar*)a_entity->name,
				     strlen ((char*)a_entity->name)) ;
    esc_content = g_markup_escape_text
	((gchar*)a_entity->content,
	 strlen ((char*)a_entity->content)) ;
    tmp_str = g_strconcat ("<span foreground=\"",
			   colour,"\">"
			   "&lt;!ENTITY ",
			   esc_name,
			   " ",
			   sep,
			   esc_content,
			   sep,
			   "&gt;",
			   "</span>",
			   NULL) ;
    if (esc_content) {
	g_free (esc_content) ;
	esc_content = NULL ;
    }
    if (esc_name) {
	g_free (esc_name) ;
	esc_name = NULL ;
    }
    if (tmp_str) {
	*a_string = tmp_str ;
	tmp_str = NULL ;
    } else {
	return MLVIEW_OUT_OF_MEMORY_ERROR ;
    }

    return MLVIEW_OK ;
}


/**
 *Builds a string of the form
 *<!ENTITY  external-general-parsed-entity SYSTEM "entity-text.xml">
 *from an external general parsed entity data structure.
 *
 *@param a_entity the entity to consider
 *@param a_string out parameter the result string
 *@return MLVIEW_OK upon successful completion, an error code
 *otherwise.
 */
enum MlViewStatus
mlview_tree_editor_external_general_parsed_entity_to_string
								(MlViewTreeEditor *a_this,
								 xmlEntity *a_entity,
								 bool selected,
								 gchar **a_string)
{
	gchar * tmp_str=NULL, *sys_id_sep=NULL, *pub_id_sep=NULL;
	gchar *colour = NULL, *esc_name = NULL, *esc_sys_id = NULL,
		  *esc_pub_id = NULL ;

	THROW_IF_FAIL (a_entity && a_this && MLVIEW_IS_TREE_EDITOR (a_this)
				   && a_entity->etype == XML_EXTERNAL_GENERAL_PARSED_ENTITY
				   && a_entity->name && a_entity && a_entity->SystemID
				   && a_string) ;

    if (selected) {
	GdkColor color =
	    PRIVATE (a_this)->style->fg[GTK_STATE_SELECTED];
	colour = (gchar*) mlview::gdk_color_to_html_string (color).c_str();
    } else {
	colour = (gchar*) mlview_tree_editor_get_colour_string
	    (a_this, XML_ENTITY_DECL) ;
    }
    THROW_IF_FAIL (colour) ;

    if (a_entity->SystemID && strchr ((char*)a_entity->SystemID, '"'))
	sys_id_sep = (gchar*) "'";
    else
	sys_id_sep = (gchar*) "\"";

    if (a_entity->ExternalID
	&& strchr ((char*)a_entity->ExternalID, '"'))
	pub_id_sep = (gchar*) "'";
    else
	pub_id_sep = (gchar*) "\"";

    esc_name = g_markup_escape_text ((gchar*)a_entity->name,
				     strlen ((char*)a_entity->name)) ;
    
    if (a_entity->SystemID) {
	esc_sys_id = g_markup_escape_text
	    ((gchar*)a_entity->SystemID,
	     strlen ((char*)a_entity->SystemID)) ;
    }
    if (a_entity->ExternalID) {
	esc_pub_id = g_markup_escape_text
	    ((gchar*)a_entity->ExternalID,
	     strlen ((char*)a_entity->ExternalID)) ;
    }
    if (esc_pub_id) {
	if (esc_sys_id) {
	    tmp_str = g_strconcat ("<span foreground=\"",
				   colour,
				   "\">",
				   "&lt;!ENTITY ",
				   esc_name,
				   " PUBLIC ",
				   pub_id_sep,
				   esc_pub_id,
				   pub_id_sep,
				   " ",
				   sys_id_sep,
				   esc_sys_id,
				   sys_id_sep,
				   "&gt;",
				   "</span>",
				   NULL) ;
	} else {
	    tmp_str = g_strconcat ("<span foreground=\"",
				   colour,
				   "\">",
				   "&lt;!ENTITY ",
				   esc_name,
				   " PUBLIC ",
				   pub_id_sep,
				   esc_pub_id,
				   pub_id_sep,
				   "&gt;",
				   "</span>",
				   NULL) ;
	}
    } else {
	if (esc_sys_id) {
	    tmp_str = g_strconcat ("<span foreground=\"",
				   colour,
				   "\">",
				   "&lt;!ENTITY ",
				   esc_name,
				   " SYSTEM ",
				   sys_id_sep,
				   esc_sys_id,
				   sys_id_sep,
				   "&gt;",
				   "</span>",
				   NULL) ;
	} else {
	    return MLVIEW_ERROR ;
	}
    }
	if (tmp_str) {
		*a_string = tmp_str ;
		tmp_str = NULL ;
	}
	if (esc_name) {
		g_free (esc_name) ;
		esc_name = NULL ;
	}
	if (esc_pub_id) {
		g_free (esc_pub_id) ;
		esc_pub_id = NULL ;
	}
	if (esc_sys_id) {
		g_free (esc_sys_id) ;
		esc_sys_id = NULL ;
	}
	if (!*a_string) {
		return MLVIEW_OUT_OF_MEMORY_ERROR ;
	}
	return MLVIEW_OK ;
}

/**
 *Builds a string of the form 
 *<!ENTITY  external-general-unparsed-entity SYSTEM "/a/path" NDATA "a-tag">
 *from a given internal general entity data structure.
 *@param a_entity the input internal general entity.
 *Its ->etype field must be of type XML_EXTERNAL_GENERAL_UNPARSED_ENTITY,
 *otherwise the MLVIEW_BAD_PARAM_ERROR is returned.
 *@param a_string out parameter. A pointer to the returned string.
 *@return MLVIEW_OK upon successful completion, an error code
 *otherwise.
 */
enum MlViewStatus
mlview_tree_editor_external_general_unparsed_entity_to_string
							(MlViewTreeEditor *a_this,
							 xmlEntity *a_entity,
							 bool selected,
							 gchar **a_string)
{
    gchar * tmp_str = NULL, *sys_id_sep=NULL, *pub_id_sep=NULL;
    gchar *colour = NULL, *esc_name = NULL, *esc_content = NULL,
	*esc_sysid = NULL, *esc_pub_id = NULL ;

    THROW_IF_FAIL (a_entity && a_this && MLVIEW_IS_TREE_EDITOR (a_this)
		   && a_entity->etype == XML_EXTERNAL_GENERAL_UNPARSED_ENTITY
		   && a_entity->name && a_entity->SystemID && a_string) ;

    if (selected) {
	GdkColor color =
	    PRIVATE (a_this)->style->fg[GTK_STATE_SELECTED];
	colour = (gchar*) mlview::gdk_color_to_html_string (color).c_str();
    } else {
	colour = (gchar*) mlview_tree_editor_get_colour_string
	    (a_this, XML_ENTITY_DECL) ;
    }
    THROW_IF_FAIL (colour) ;

    esc_name = g_markup_escape_text ((gchar*)a_entity->name,
				     strlen ((char*)a_entity->name)) ;
    THROW_IF_FAIL (esc_name) ;

    if (a_entity->SystemID) {
	esc_sysid = g_markup_escape_text
	    ((gchar*)a_entity->SystemID,
	     strlen ((char*)a_entity->SystemID)) ;
    }
    if (a_entity->ExternalID) {
	esc_pub_id = g_markup_escape_text
	    ((gchar*)a_entity->ExternalID,
	     strlen ((char*)a_entity->ExternalID)) ;
    }

    if (a_entity->content) {
	esc_content = g_markup_escape_text
	    ((gchar*)a_entity->content,
	     strlen ((char*)a_entity->content) ) ;
	THROW_IF_FAIL (esc_content) ;
    }

    if (a_entity->SystemID && strchr ((char*)a_entity->SystemID, '"'))
	sys_id_sep = (gchar*) "'";
    else
	sys_id_sep = (gchar*) "\"";

    if (a_entity->ExternalID
	&& strchr ((char*)a_entity->ExternalID, '"'))
	pub_id_sep = (gchar*) "'";
    else
	pub_id_sep = (gchar*) "\"";

    if (esc_pub_id) {
	if (esc_sysid) {
	    if (esc_content)
		tmp_str = g_strconcat ("<span foreground=\"",
				       colour,
				       "\">"
				       "&lt;!ENTITY ",
				       esc_name,
				       " PUBLIC ",
				       pub_id_sep,
				       esc_pub_id,
				       pub_id_sep,
				       " ",
				       sys_id_sep,
				       esc_sysid,
				       sys_id_sep,
				       " NDATA ",
				       esc_content,
				       "&gt;",
				       "</span>",
				       NULL) ;
	    else
		tmp_str = g_strconcat ("<span foreground=\"",
				       colour,
				       "\">",
				       "&lt;!ENTITY ",
				       esc_name,
				       " PUBLIC ",
				       pub_id_sep,
				       esc_pub_id,
				       pub_id_sep,
				       " ",
				       sys_id_sep,
				       esc_sysid,
				       sys_id_sep,
				       "&gt;",
				       "</span>",
				       NULL) ;
	} else {
	    if (esc_content)
		tmp_str = g_strconcat ("<span foreground=\"",
				       colour,
				       "\">"
				       "&lt;!ENTITY ",
				       esc_name,
				       "  PUBLIC ",
				       pub_id_sep,
				       esc_pub_id,
				       pub_id_sep,
				       " NDATA ",
				       esc_content,
				       "&gt;",
				       "</span>",
				       NULL) ;
	    else
		tmp_str = g_strconcat ("<span foreground=\"",
				       colour,
				       "\">",
				       "&lt;!ENTITY ",
				       esc_name,
				       " PUBLIC ",
				       pub_id_sep,
				       esc_pub_id,
				       pub_id_sep,
				       "&gt;",
				       "</span>",
				       NULL) ;
	}
    } else {
	if (esc_sysid) {
	    if (a_entity->content)
		tmp_str = g_strconcat ("<span foreground=\"",
				       colour,
				       "\">"
				       "&lt;!ENTITY ",
				       esc_name,
				       " SYSTEM ",
				       sys_id_sep,
				       esc_sysid,
				       sys_id_sep,
				       " NDATA ",
				       esc_content,
				       "&gt;",
				       "</span>",
				       NULL) ;
	    else
		tmp_str = g_strconcat ("<span foreground=\"",
				       colour,
				       "\">",
				       "&lt;!ENTITY ",
				       esc_name,
				       " SYSTEM ",
				       sys_id_sep,
				       esc_sysid,
				       sys_id_sep,
				       "&gt;",
				       "</span>",
				       NULL) ;
	} else {
	    return MLVIEW_ERROR ;
	}
    }
    if (tmp_str) {
	*a_string = tmp_str ;
	tmp_str = NULL ;
    }
    if (esc_name) {
	g_free (esc_name) ;
	esc_name = NULL ;
    }
    if (esc_content) {
	g_free (esc_content) ;
	esc_content = NULL ;
    }
    if (esc_sysid) {
	g_free (esc_sysid) ;
	esc_sysid = NULL ;
    }
    if (!*a_string) {
	return MLVIEW_ERROR ;
    }
    return MLVIEW_OK ;
}


/**
 *Builds an instance of GtkTreeView from an instance
 *of xmlDoc .
 *@param the application context.
 *@param a_doc the instance of xmlDoc to consider.
 *@return the newly built instance of GtkTreeView, or NULL
 *if a probleme arose.
 */

GtkTreeView *
mlview_tree_editor_build_tree_view_from_xml_doc (MlViewTreeEditor * a_this,
						 xmlDoc * a_doc)
{
    return MLVIEW_TREE_EDITOR_CLASS
	(G_OBJECT_GET_CLASS (a_this))->build_tree_view_from_xml_doc (a_this,
								     a_doc);
}

/**
 *Builds a string of the form
 *<!ENTITY  % internal-parameter-entity "the-entity-content">
 *from a given internal general entity data structure.
 *@param a_entity the input internal parameter entity.
 *Its ->etype field must be of type XML_INTERNAL_PARAMETER_ENTITY,
 *otherwise the MLVIEW_BAD_PARAM_ERROR is returned.
 *@param a_string out parameter. A pointer to the returned string.
 *@return MLVIEW_OK upon successful completion, an error code
 *otherwise.
 */
enum MlViewStatus
mlview_tree_editor_internal_parameter_entity_to_string (MlViewTreeEditor *a_this,
							xmlEntity *a_entity,
							bool selected,
							gchar **a_string)
{
    gchar * tmp_str = NULL, *sep = NULL ;
    gchar *colour = NULL, *esc_name = NULL, *esc_content = NULL ;

    THROW_IF_FAIL (a_entity && a_this && MLVIEW_IS_TREE_EDITOR (a_this)
		   && a_entity->etype == XML_INTERNAL_PARAMETER_ENTITY
		   && a_entity->name && a_entity->content && a_string) ;

    if (strchr ((char*)a_entity->content, '"'))
	sep = (gchar*) "'";
    else
	sep = (gchar*) "\"";

    if (selected) {
	GdkColor color =
	    PRIVATE (a_this)->style->fg[GTK_STATE_SELECTED];
	colour = (gchar*) mlview::gdk_color_to_html_string (color).c_str();
    } else {
	colour = (gchar*) mlview_tree_editor_get_colour_string
	    (a_this, XML_ENTITY_DECL) ;
    }
    THROW_IF_FAIL (colour) ;

    esc_name = g_markup_escape_text ((gchar*)a_entity->name,
				     strlen ((char*)a_entity->name)) ;
    esc_content = g_markup_escape_text
	((gchar*)a_entity->content,
	 strlen ((char*)a_entity->content)) ;
    tmp_str = g_strconcat ("<span foreground=\"",
			   colour,
			   "\">",
			   "&lt;!ENTITY % ",
			   esc_name,
			   " ",
			   sep,
			   esc_content,
			   sep,
			   "&gt;",
			   "</span>",
			   NULL) ;

    if (tmp_str) {
	*a_string = tmp_str ;
    }
    if (esc_name) {
	g_free (esc_name) ;
	esc_name = NULL ;
    }
    if (esc_content) {
	g_free (esc_content) ;
	esc_content = NULL ;
    }
    if (!*a_string) {
	return MLVIEW_ERROR ;
    }

    return MLVIEW_OK ;
}

/**
 *Builds a string of the form 
 *<!ENTITY  % external-parameter-entity SYSTEM "/a/path">
 *from a given external parameter entity data structure.
 *@param a_entity the input external parameter entity.
 *Its ->etype field must be of type XML_EXTERNAL_PARAMETER_ENTITY,
 *otherwise the MLVIEW_BAD_PARAM_ERROR is returned.
 *@param a_string out parameter. A pointer to the returned string.
 *@return MLVIEW_OK upon successful completion, an error code
 *otherwise.
 */
enum MlViewStatus
mlview_tree_editor_external_parameter_entity_to_string (MlViewTreeEditor *a_this,
							xmlEntity *a_entity,
							bool selected,
							gchar **a_string)
{
    gchar * tmp_str = NULL, *pub_id_sep=NULL, *sys_id_sep=NULL ;
    gchar *colour = NULL, *esc_name = NULL, *esc_sysid = NULL, *esc_pub_id=NULL ;

    THROW_IF_FAIL (a_entity && a_this && MLVIEW_IS_TREE_EDITOR (a_this)
		   && a_entity->etype == XML_EXTERNAL_PARAMETER_ENTITY
		   && a_entity->name && a_entity->SystemID && a_string) ;

    if (a_entity->SystemID && strchr ((char*)a_entity->SystemID, '"'))
	sys_id_sep = (gchar*) "'";
    else
	sys_id_sep = (gchar*) "\"";

    if (a_entity->ExternalID
	&& strchr ((gchar*)a_entity->ExternalID, '"'))
	pub_id_sep = (gchar*) "'";
    else
	pub_id_sep = (gchar*) "\"";

    if (selected) {
	GdkColor color =
	    PRIVATE (a_this)->style->fg[GTK_STATE_SELECTED];
	colour = (gchar*) mlview::gdk_color_to_html_string (color).c_str();
    } else {
	colour = (gchar*) mlview_tree_editor_get_colour_string
	    (a_this, XML_ENTITY_DECL) ;
    }
    THROW_IF_FAIL (colour) ;

    esc_name = g_markup_escape_text ((gchar*)a_entity->name,
				     strlen ((char*)a_entity->name)) ;
    THROW_IF_FAIL (esc_name) ;

    if (a_entity->SystemID) {
	esc_sysid = g_markup_escape_text
	    ((gchar*)a_entity->SystemID,
	     strlen ((char*)a_entity->SystemID)) ;
    }
    if (a_entity->ExternalID) {
	esc_pub_id = g_markup_escape_text
	    ((gchar*)a_entity->ExternalID,
	     strlen ((char*)a_entity->ExternalID)) ;
    }
    THROW_IF_FAIL (esc_sysid) ;

    if (esc_pub_id) {
	if (esc_sysid) {
	    tmp_str = g_strconcat ("<span foreground=\"",
				   colour,
				   "\">",
				   "&lt;!ENTITY % ",
				   esc_name,
				   " PUBLIC ",
				   pub_id_sep,
				   esc_pub_id,
				   pub_id_sep,
				   " ",
				   sys_id_sep,
				   esc_sysid,
				   sys_id_sep,
				   "&gt;",
				   "</span>",
				   NULL) ;
	} else {
	    tmp_str = g_strconcat ("<span foreground=\"",
				   colour,
				   "\">",
				   "&lt;!ENTITY % ",
				   esc_name,
				   " PUBLIC ",
				   pub_id_sep,
				   esc_pub_id,
				   pub_id_sep,
				   "&gt;",
				   "</span>",
				   NULL) ;
	}
    } else {
	if (esc_sysid) {
	    tmp_str = g_strconcat ("<span foreground=\"",
				   colour,
				   "\">",
				   "&lt;!ENTITY % ",
				   esc_name,
				   " SYSTEM ",
				   sys_id_sep,
				   esc_sysid,
				   sys_id_sep,
				   "&gt;",
				   "</span>",
				   NULL) ;
	} else {
	    return MLVIEW_ERROR ;
	}
    }

    if (tmp_str) {
	*a_string = tmp_str ;
	tmp_str = NULL ;
    }

    if (esc_name) {
	g_free (esc_name) ;
	esc_name = NULL ;
    }
    if (esc_sysid) {
	g_free (esc_sysid) ;
	esc_sysid = NULL ;
    }
    if (!*a_string) {
	return MLVIEW_ERROR ;
    }
    return MLVIEW_OK ;
}

enum MlViewStatus
mlview_tree_editor_dtd_node_to_string (MlViewTreeEditor *a_this,
				       xmlDtd *a_dtd_node,
				       bool selected,
				       gchar **a_string)
{
    gchar *content = NULL ;
    xmlDtd *node = (xmlDtd*)a_dtd_node ;
    gchar *tmp_str = NULL, *escaped_content = NULL;
    gchar *dtd_color = NULL ;

    THROW_IF_FAIL (a_dtd_node && a_this && MLVIEW_IS_TREE_EDITOR (a_this)
		   && a_string) ;

    if (selected) {
	GdkColor color =
	    PRIVATE (a_this)->style->fg[GTK_STATE_SELECTED];
	dtd_color = (gchar*) mlview::gdk_color_to_html_string (color).c_str();
    } else {
	dtd_color = (gchar*) mlview_tree_editor_get_colour_string
	    (a_this, a_dtd_node->type) ;
    }
    THROW_IF_FAIL (dtd_color) ;

    if (!a_dtd_node->name) {
	mlview_utils_trace_debug
	    ("a node of type XML_DTD_NODE must have a ->name field set!!") ;
	return MLVIEW_ERROR ;
    }
    content = g_strconcat ("<span foreground=\"",
			   dtd_color, "\">"
			   "&lt;!DOCTYPE ",
			   a_dtd_node->name, NULL) ;
    if (!content) {
	mlview_utils_trace_debug
	    ("g_strconcat failed") ;
	return MLVIEW_ERROR ;
    }
    if (node->ExternalID) {
	escaped_content = g_markup_escape_text
	    ((gchar*)node->ExternalID,
	     strlen ((char*)node->ExternalID)) ;
	tmp_str = g_strconcat (content,
			       " PUBLIC \"",
			       escaped_content,
			       "\"",
			       NULL) ;
	if (escaped_content) {
	    g_free (escaped_content) ;
	    escaped_content = NULL ;
	}
	if (content) {
	    g_free (content) ;
	}
	content = tmp_str ;
	tmp_str = NULL ;
	if (node->SystemID) {
	    escaped_content = g_markup_escape_text
		((gchar*)node->SystemID,
		 strlen ((char*)node->SystemID)) ;
	    tmp_str = g_strconcat
		(content,
		 " \"",
		 escaped_content,
		 "\"&gt;</span>",
		 NULL) ;
	    if (escaped_content) {
		g_free (escaped_content) ;
		escaped_content = NULL ;
	    }
	    if (!tmp_str) {
		mlview_utils_trace_debug
		    ("g_strconcat() failed.");
		return MLVIEW_ERROR ;
	    }
	    if (content) {
		g_free (content) ;
		content = NULL ;
	    }
	    content = tmp_str ;
	    tmp_str = NULL ;
	}
    } else {
	if (node->SystemID) {
	    escaped_content = g_markup_escape_text
		((gchar*)node->SystemID,
		 strlen ((char*)node->SystemID)) ;
	    tmp_str = g_strconcat
		(content,
		 " SYSTEM \"",
		 escaped_content,
		 "\"&gt;</span>",
		 NULL) ;
	    if (escaped_content) {
		g_free (escaped_content) ;
		escaped_content = NULL ;
	    }
	    if (!tmp_str) {
		mlview_utils_trace_debug
		    ("g_strconcat failed") ;
		return MLVIEW_ERROR ;
	    }
	    content = tmp_str ;
	    tmp_str = NULL ;
	}
    }
    *a_string = content ;
    content = NULL ;
    return MLVIEW_ERROR ;
}

enum MlViewStatus
mlview_tree_editor_entity_ref_to_string (MlViewTreeEditor *a_this,
					 xmlNode *a_node,
					 bool selected,
					 gchar **a_string)
{
    gchar *colour_str = NULL, *name = NULL, *result = NULL ;
    enum MlViewStatus status = MLVIEW_OK ;

    THROW_IF_FAIL (a_this && MLVIEW_IS_TREE_EDITOR (a_this)
		   && PRIVATE (a_this) && a_node
		   && a_node->type == XML_ENTITY_REF_NODE
		   && a_node->name && a_string) ;

    if (selected) {
	GdkColor color =
	    PRIVATE (a_this)->style->fg[GTK_STATE_SELECTED];
	colour_str = (gchar*) mlview::gdk_color_to_html_string (color).c_str ();
    } else {
	colour_str = (gchar*)mlview_tree_editor_get_colour_string
	    (a_this, a_node->type) ;
    }
    THROW_IF_FAIL (colour_str) ;

    name = g_markup_escape_text ((gchar*)a_node->name,
				 strlen ((char*)a_node->name)) ;

    result = g_strconcat ("<span foreground=\"",
			  colour_str, "\">",
			  "&amp;",
			  name,
			  ";",
			  "</span>",
			  NULL) ;
    if (!result) {
	status = MLVIEW_ERROR ;
	goto out ;
    }

    *a_string = result ;
    result = NULL ;
    status = MLVIEW_OK ;
 out:
    if (name) {
	g_free (name) ;
	name = NULL ;
    }
    if (result) {
	g_free (result) ;
	result = NULL ;
    }

    return status ;
}

enum MlViewStatus
mlview_tree_editor_document_node_to_string (MlViewTreeEditor *a_this,
					    xmlNode *a_node,
					    bool selected,
					    gchar **a_string)
{
    gchar *colour_str = NULL, *name = NULL, *result = NULL ;
    enum MlViewStatus status = MLVIEW_OK ;

    THROW_IF_FAIL (a_this && MLVIEW_IS_TREE_EDITOR (a_this)
		   && PRIVATE (a_this) && a_node
		   && a_node->type == XML_DOCUMENT_NODE
		   && a_node->name && a_string) ;

    if (selected) {
	GdkColor color =
	    PRIVATE (a_this)->style->fg[GTK_STATE_SELECTED];
	colour_str = (gchar*) mlview::gdk_color_to_html_string (color).c_str ();
    } else {
	colour_str = (gchar*)mlview_tree_editor_get_colour_string
	    (a_this, a_node->type) ;
    }
    THROW_IF_FAIL (colour_str) ;

    name = g_markup_escape_text ((gchar*)a_node->name,
				 strlen ((char*)a_node->name)) ;

    result = g_strconcat ("<span foreground=\"",
			  colour_str, "\">",
			  "XML Document Node",
			  "</span>",
			  NULL) ;
    if (!result) {
	status = MLVIEW_ERROR ;
	goto out ;
    }

    *a_string = result ;
    result = NULL ;
    status = MLVIEW_OK ;
 out:
    if (name) {
	g_free (name) ;
	name = NULL ;
    }
    if (result) {
	g_free (result) ;
	result = NULL ;
    }

    return status ;
}

enum MlViewStatus
mlview_tree_editor_cdata_section_to_string (MlViewTreeEditor *a_this,
					    xmlNode *a_node,
					    gchar **a_result)
{
    gchar *tmp_str = NULL ;
    GString *stryng = NULL ;
    enum MlViewStatus status = MLVIEW_OK ;

    THROW_IF_FAIL (a_this && MLVIEW_IS_TREE_EDITOR (a_this)
		   && PRIVATE (a_this)) ;
    THROW_IF_FAIL (a_result) ;
    THROW_IF_FAIL (a_node && a_node->type == XML_CDATA_SECTION_NODE) ;

    stryng = g_string_new (NULL) ;

    g_string_append (stryng, "<![CDATA[") ;
    tmp_str = (gchar*)xmlNodeGetContent (a_node) ;
    if (tmp_str) {
	g_string_append (stryng, tmp_str) ;
	g_free (tmp_str) ;
	tmp_str = NULL ;
    }
    g_string_append (stryng, "]]>") ;
    tmp_str = g_markup_escape_text (stryng->str,
				    stryng->len) ;
    if (tmp_str) {
	*a_result = tmp_str ;
	tmp_str = NULL ;
    }
    if (stryng) {
	g_string_free (stryng, TRUE) ;
	stryng = NULL ;
    }
    return status ;
}

enum MlViewStatus
mlview_tree_editor_edit_xml_entity_decl_node (MlViewTreeEditor *a_this,
					      xmlEntity *a_entity_node,
					      gchar *a_new_text)
{
    enum MlViewStatus status = MLVIEW_OK ;
    MlViewXMLDocument *mlview_xml_doc = NULL ;
    gchar *name_start = NULL, *name_end = NULL,
	*public_id_start = NULL, *public_id_end = NULL,
	*system_id_start = NULL, *system_id_end = NULL,
	*value_start = NULL, *value_end = NULL, *ndata_start = NULL,
	*ndata_end = NULL, *name=NULL, *public_id=NULL, *system_id=NULL,
	*value=NULL, *ndata=NULL; xmlDoc *native_doc = NULL ;

    THROW_IF_FAIL (a_this && MLVIEW_IS_TREE_EDITOR (a_this) && a_entity_node
		   && a_new_text) ;

    mlview_xml_doc = mlview_tree_editor_get_mlview_xml_doc (a_this) ;
    THROW_IF_FAIL (mlview_xml_doc) ;
    native_doc = mlview_xml_document_get_native_document
	(mlview_xml_doc) ;
    THROW_IF_FAIL (mlview_xml_doc) ;
    switch (a_entity_node->etype) {
    case XML_INTERNAL_GENERAL_ENTITY:
	status = mlview_utils_parse_internal_general_entity
	    (a_new_text, &name_start, &name_end,
	     &value_start, &value_end) ;
	if (status != MLVIEW_OK)
	    return MLVIEW_ERROR;
	if (name_start && name_end) {
	    name = g_strndup
		(name_start,
		 name_end - name_start + 1) ;
	}
	if (value_start && value_end) {
	    value = g_strndup
		(value_start,
		 value_end - value_start + 1);
	}
	mlview_xml_document_set_entity_node_name
	    (mlview_xml_doc, a_entity_node,
	     native_doc->intSubset, name, TRUE);
	mlview_xml_document_set_entity_content
	    (mlview_xml_doc, a_entity_node,
	     (xmlChar*)value, TRUE) ;
	if (name) {
	    g_free (name) ;
	    name = NULL ;
	}
	if (value) {
	    g_free (value) ;
	    value = NULL ;
	}
	break ;
    case XML_EXTERNAL_GENERAL_PARSED_ENTITY:
	status = mlview_utils_parse_external_general_parsed_entity
	    (a_new_text, &name_start, &name_end,
	     &public_id_start, &public_id_end,
	     &system_id_start, &system_id_end) ;
	if (status != MLVIEW_OK)
	    return MLVIEW_ERROR;
	if (name_start && name_end) {
	    name = g_strndup
		(name_start,
		 name_end - name_start + 1) ;
	}
	if (public_id_start && public_id_end) {
	    public_id = g_strndup
		(public_id_start,
		 public_id_end - public_id_start + 1) ;
	}
	if (system_id_start && system_id_end) {
	    system_id = g_strndup
		(system_id_start,
		 system_id_end - system_id_start +1) ;
	}
	mlview_xml_document_set_entity_node_name
	    (mlview_xml_doc, a_entity_node,
	     native_doc->intSubset, name, TRUE) ;
	mlview_xml_document_set_entity_public_id
	    (mlview_xml_doc, a_entity_node,
	     (xmlChar*)public_id, TRUE) ;
	mlview_xml_document_set_entity_system_id
	    (mlview_xml_doc, a_entity_node,
	     (xmlChar*)system_id, TRUE) ;
	if (name) {
	    g_free (name) ;
	    name = NULL ;
	}
	if (public_id) {
	    g_free (public_id) ;
	    public_id = NULL ;
	}
	if (system_id) {
	    g_free (system_id) ;
	    system_id = NULL ;
	}
	break ;
    case XML_EXTERNAL_GENERAL_UNPARSED_ENTITY:
	status = mlview_utils_parse_external_general_unparsed_entity
	    (a_new_text, &name_start, &name_end,
	     &public_id_start, &public_id_end,
	     &system_id_start, &system_id_end,
	     &ndata_start, &ndata_end) ;
	if (status != MLVIEW_OK)
	    return MLVIEW_ERROR;
	if (name_start && name_end) {
	    name = g_strndup
		(name_start,
		 name_end - name_start + 1) ;
	}
	if (public_id_start && public_id_end) {
	    public_id = g_strndup
		(public_id_start,
		 public_id_end - public_id_start + 1) ;
	}
	if (system_id_start && system_id_end) {
	    system_id = g_strndup
		(system_id_start,
		 system_id_end - system_id_start +1) ;
	}
	if (ndata_start && ndata_end) {
	    ndata = g_strndup (ndata_start,
			       ndata_end - ndata_start + 1);
	}
	mlview_xml_document_set_entity_node_name
	    (mlview_xml_doc,
	     a_entity_node,
	     native_doc->intSubset, name, TRUE) ;
	mlview_xml_document_set_entity_public_id
	    (mlview_xml_doc, a_entity_node,
	     (xmlChar*)public_id, TRUE) ;
	mlview_xml_document_set_entity_system_id
	    (mlview_xml_doc, a_entity_node,
	     (xmlChar*)system_id, TRUE) ;
	mlview_xml_document_set_entity_content
	    (mlview_xml_doc, a_entity_node,
	     (xmlChar*)ndata, TRUE) ;
	if (name) {
	    g_free (name) ;
	    name = NULL ;
	}
	if (public_id) {
	    g_free (public_id) ;
	    public_id = NULL ;
	}
	if (system_id) {
	    g_free (system_id) ;
	    system_id = NULL ;
	}
	if (ndata) {
	    g_free (ndata) ;
	    ndata = NULL ;
	}
	break ;
    case XML_INTERNAL_PARAMETER_ENTITY:
	status = mlview_utils_parse_internal_parameter_entity
		         (a_new_text, &name_start, &name_end,
		          &value_start, &value_end) ;
		if (status != MLVIEW_OK)
			return MLVIEW_ERROR;
		if (name_start && name_end) {
			name = g_strndup
			       (name_start,
			        name_end - name_start + 1) ;
		}
		if (value_start && value_end) {
			value = g_strndup
			        (value_start,
			         value_end - value_start + 1);
		}
		mlview_xml_document_set_entity_node_name
		(mlview_xml_doc, a_entity_node,
		 native_doc->intSubset, name, TRUE);
		mlview_xml_document_set_entity_content
		(mlview_xml_doc, a_entity_node,
		 (xmlChar*)value, TRUE) ;
		if (name) {
			g_free (name) ;
			name = NULL ;
		}
		if (value) {
			g_free (value) ;
			value = NULL ;
		}
		break ;
	case XML_EXTERNAL_PARAMETER_ENTITY:
		status = mlview_utils_parse_external_parameter_entity
		         (a_new_text, &name_start, &name_end,
		          &public_id_start, &public_id_end,
		          &system_id_start, &system_id_end) ;
		if (status != MLVIEW_OK)
			return MLVIEW_ERROR ;
		if (name_start && name_end) {
			name = g_strndup
			       (name_start,
			        name_end - name_start + 1) ;
		}
		if (public_id_start && public_id_end) {
			public_id = g_strndup
			            (public_id_start,
			             public_id_end - public_id_start + 1) ;
		}
		if (system_id_start && system_id_end) {
			system_id = g_strndup
			            (system_id_start,
			             system_id_end - system_id_start +1) ;
		}
		mlview_xml_document_set_entity_node_name
		(mlview_xml_doc, a_entity_node,
		 native_doc->intSubset, name, TRUE) ;
		mlview_xml_document_set_entity_public_id
		(mlview_xml_doc, a_entity_node,
		 (xmlChar*)public_id, TRUE) ;
		mlview_xml_document_set_entity_system_id
		(mlview_xml_doc, a_entity_node,
		 (xmlChar*)system_id, TRUE) ;
		if (name) {
			g_free (name) ;
			name = NULL ;
		}
		if (public_id) {
			g_free (public_id) ;
			public_id = NULL ;
		}
		if (system_id) {
			g_free (system_id) ;
			system_id = NULL ;
		}
		break ;
	default:
		break ;
	}
	return MLVIEW_OK ;
}

enum MlViewStatus
mlview_tree_editor_edit_dtd_node (MlViewTreeEditor *a_this,
                                  xmlDtd *a_dtd_node,
                                  gchar *a_new_text)
{
	MlViewXMLDocument *mlview_xml_doc = NULL ;
	enum MlViewStatus status = MLVIEW_OK ;
	gchar *root_elem_start = NULL,
	                         *root_elem_end = NULL,
	                                          *public_id_start = NULL,
	                                                             *public_id_end = NULL,
	                                                                              *system_id_start = NULL,
	                                                                                                 *system_id_end = NULL ;
	gchar *root_elem = NULL, *public_id = NULL,
	                                      *system_id = NULL ;

	mlview_xml_doc = mlview_tree_editor_get_mlview_xml_doc
	                 (a_this) ;
	THROW_IF_FAIL (mlview_xml_doc) ;
	status = mlview_utils_parse_doctype_decl
	         (a_new_text, &root_elem_start,
	          &root_elem_end, &public_id_start,
	          &public_id_end, &system_id_start,
	          &system_id_end) ;
	if (status == MLVIEW_OK) {
		if (!root_elem_start
		        || !root_elem_start) {
			mlview_utils_trace_debug
			("mlview_utils_parse_doctype_decl"
			 " failed") ;
			return MLVIEW_ERROR ;
		}
		root_elem = (gchar*)xmlStrndup
		            ((xmlChar*)root_elem_start,
		             root_elem_end - root_elem_start + 1);
		if (root_elem) {
			mlview_xml_document_set_node_name_without_xpath
			(mlview_xml_doc, (xmlNode*)a_dtd_node,
			 root_elem, TRUE) ;
		}
		if (root_elem) {
			xmlFree (root_elem) ;
			root_elem = NULL ;
		}
		if (public_id_start && public_id_end) {
			public_id = (gchar*)xmlStrndup
			            ((xmlChar*)public_id_start,
			             public_id_end - public_id_start + 1) ;
		}
		mlview_xml_document_set_dtd_node_public_id
		(mlview_xml_doc, a_dtd_node,
		 (xmlChar*)public_id, TRUE) ;
		if (public_id) {
			xmlFree (public_id) ;
			public_id = NULL ;
		}
		if (system_id_start && system_id_end) {
			system_id = (gchar*) xmlStrndup
			            ((xmlChar*)system_id_start,
			             system_id_end - system_id_start + 1) ;
		}
		mlview_xml_document_set_dtd_node_system_id
		(mlview_xml_doc, a_dtd_node,
		 (xmlChar*)system_id, TRUE) ;
		if (system_id) {
			xmlFree (system_id) ;
			system_id = NULL ;
		}
	}
	return MLVIEW_OK ;
}

enum MlViewStatus
mlview_tree_editor_edit_cdata_section_node (MlViewTreeEditor *a_this,
        xmlNode *a_node,
        gchar *a_new_text)
{
	enum MlViewStatus status = MLVIEW_OK ;
	gchar *cdata_start = NULL, *cdata_end = NULL,
	                                        *node_path = NULL;
	GString *stryng = NULL ;

	status = mlview_utils_parse_cdata_section (a_new_text,
	         &cdata_start,
	         &cdata_end) ;
	if (status != MLVIEW_OK)
		return MLVIEW_PARSING_ERROR ;
	if (cdata_start && cdata_end) {
		stryng = g_string_new_len (cdata_start,
		                           cdata_end - cdata_start + 1) ;
		mlview_xml_document_get_node_path (PRIVATE (a_this)->mlview_xml_doc,
		                                   a_node,
		                                   &node_path) ;
		if (!node_path) {
			mlview_utils_trace_debug ("Could not get node path") ;
			return MLVIEW_ERROR ;
		}
		mlview_xml_document_set_node_content
		(PRIVATE (a_this)->mlview_xml_doc,
		 node_path, stryng->str, TRUE) ;
		g_string_free (stryng, TRUE) ;
		stryng = NULL ;
		if (node_path) {
			g_free (node_path) ;
			node_path = NULL ;
		}
	} else {
		return MLVIEW_PARSING_ERROR ;
	}
	return MLVIEW_OK ;
}

/**
 *Gets the instance of GtkTreeModel (namely an instance of
 *GtkTreeStore) of the current xml document.
 *@param a_this the current instance of #MlViewEditor.
 *@return the instance of GtkTreeModel, or NULL.
 */
GtkTreeModel *
mlview_tree_editor_get_model (MlViewTreeEditor * a_this)
{
	GtkTreeView *tree_view = NULL;
	GtkTreeModel *model = NULL;

	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_TREE_EDITOR (a_this),
	                      NULL);
	tree_view = mlview_tree_editor_get_tree_view (a_this);
	THROW_IF_FAIL (tree_view);
	model = gtk_tree_view_get_model (tree_view);
	THROW_IF_FAIL (model);
	return model;
}

/**
 *Gets the instance of GtkTreeView that
 *features the xml document graphically.
 *@param a_this the current instance of #MlViewTreeEditor
 *@return the GtkTreeView graphical tree.
 */
GtkTreeView *
mlview_tree_editor_get_tree_view (MlViewTreeEditor * a_this)
{
	THROW_IF_FAIL (a_this && PRIVATE (a_this));
	g_return_val_if_fail
	        (MLVIEW_IS_TREE_EDITOR (a_this), NULL);

	return PRIVATE (a_this)->tree_view;
}


/**
 *Getter of the private nodes_row_hash hashtable
 *@param a_this the current instance of #MlViewTreeEditor
 *@return return the private nodes_row_hash hashtable
 */
GHashTable *
mlview_tree_editor_get_nodes_rows_hash (MlViewTreeEditor *a_this)
{
	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_TREE_EDITOR (a_this),
	                      NULL) ;
	return PRIVATE (a_this)->nodes_rows_hash ;
}

/**
 *Setter of the private nodes_row_hash hastable
 *@param a_this the current instance of #MlViewTreeEditor
 *@param a_nodes_rows_hash the new instance of the hashtable to set.
 */
void
mlview_tree_editor_set_nodes_rows_hash (MlViewTreeEditor *a_this,
                                        GHashTable *a_nodes_rows_hash)
{
	THROW_IF_FAIL (a_this
	                  && MLVIEW_IS_TREE_EDITOR (a_this)
	                  && PRIVATE (a_this)) ;

	PRIVATE (a_this)->nodes_rows_hash = a_nodes_rows_hash ;
}

/**
 *Sets the title of the xml DOM to @a_file_path.
 *Updates this information in the tree editor and in the
 *XML DOM. 
 *@param a_file_path the new file path of xml document.
 *@param a_this the current tree editor.
 */
void
mlview_tree_editor_set_xml_document_path (MlViewTreeEditor * a_this,
        const gchar * a_file_path)
{
	xmlDoc *native_doc = NULL ;
	gboolean is_modified = FALSE ;

	THROW_IF_FAIL (a_this != NULL);
	THROW_IF_FAIL (PRIVATE (a_this) != NULL);
	THROW_IF_FAIL (a_file_path != NULL);

	if (!PRIVATE (a_this)->mlview_xml_doc)
		return;

	native_doc = mlview_xml_document_get_native_document
	             (PRIVATE (a_this)->mlview_xml_doc) ;
	THROW_IF_FAIL (PRIVATE (a_this)->mlview_xml_doc) ;

	xmlNodeSetName ((xmlNode*)native_doc, (xmlChar*)a_file_path) ;

	if (mlview_xml_document_needs_saving (PRIVATE (a_this)->mlview_xml_doc) == TRUE) {
		is_modified = TRUE ;
	} else {
		is_modified = FALSE ;
	}
	mlview_tree_editor_set_to_modified (a_this, is_modified) ;
}


/**
 *Appends a " (modified)" string or a " (saved)" string 
 *to the title of the
 *START_TAG_COLUMN_OFFSET column of the #MlViewTreeEditor
 *@param a_this the current instance of #MlViewTreeEditor
 *@param a_is_modified if TRUE, the string " (modified)"
 *is added. The string " (saved)" is added otherwise
 */
void
mlview_tree_editor_set_to_modified (MlViewTreeEditor *a_this,
                                    gboolean a_is_modified)
{
	xmlDoc *native_doc = NULL ;
	GtkTreeViewColumn *tree_column = NULL;
	gchar *doc_path = NULL,
	                  *column_title = NULL,
	                                  *escaped_column_title = NULL;

	THROW_IF_FAIL (a_this && MLVIEW_IS_TREE_EDITOR (a_this)
	                  && PRIVATE (a_this)
	                  && PRIVATE (a_this)->tree_view) ;

	native_doc =  mlview_xml_document_get_native_document
	              (PRIVATE (a_this)->mlview_xml_doc) ;

	if (native_doc && native_doc->name) {
		doc_path = native_doc->name ;
	} else {
		doc_path = (gchar*)"untitled" ;
	}
	if (a_is_modified == TRUE) {
		column_title = g_strconcat (doc_path, " (modified)", NULL) ;
	} else {
		column_title = g_strconcat (doc_path, " (saved)", NULL) ;
	}

	tree_column = gtk_tree_view_get_column
	              (PRIVATE (a_this)->tree_view,
	               START_TAG_COLUMN_OFFSET) ;
	escaped_column_title = mlview_utils_escape_underscore_for_gtk_widgets
	                       (column_title) ;
	if (!escaped_column_title) {
		mlview_utils_trace_debug ("escaping failed") ;
		goto cleanup ;
	}
	gtk_tree_view_column_set_title
	(tree_column, escaped_column_title);

	if (!tree_column)
		goto cleanup ;

cleanup:
	if (column_title) {
		g_free (column_title) ;
		column_title = NULL ;
	}
	if (escaped_column_title) {
		g_free (escaped_column_title) ;
		escaped_column_title = NULL ;
	}
}

/**
 *Return the first row of the currently selected row.
 *@param a_this the current instance of #MlViewTreeEditor.
 *@return the first row of the set of selected rows.
 */
GtkTreeRowReference *
mlview_tree_editor_get_sel_start (MlViewTreeEditor * a_this)
{
	GtkTreeRowReference *row_ref = NULL;

	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_TREE_EDITOR (a_this)
	                      && PRIVATE (a_this), NULL);
	return row_ref = PRIVATE (a_this)->cur_sel_start;
}

/**
 *Get the instance of #MlViewXMLDocument associated to this
 *instance of #MlViewTreeEditor
 *@param a_this the current instance of #MlViewTreeEditor.
 *@return the instance of #MlViewXMLDocument associated to this
 *the tree editor or NULL.
 */
MlViewXMLDocument *
mlview_tree_editor_get_mlview_xml_doc (MlViewTreeEditor *a_this)
{
	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_TREE_EDITOR (a_this)
	                      && PRIVATE (a_this),
	                      NULL) ;
	return PRIVATE (a_this)->mlview_xml_doc ;
}

/**
 *Gets the node type picker associated to this
 *instance of #MlViewTreeEditor. If the picker doesn't
 *exists, this function creates it.
 *@param a_this the current instance of #MlViewTreeEditor.
 *@return the node type picker, or NULL if something bad happened.
 */
MlViewNodeTypePicker *
mlview_tree_editor_get_node_type_picker (MlViewTreeEditor *a_this)
{
	GtkWidget *res=NULL ;
	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_TREE_EDITOR (a_this)
	                      && PRIVATE (a_this), NULL) ;

	if (!PRIVATE (a_this)->node_type_picker) {
		res = mlview_node_type_picker_new () ;
		THROW_IF_FAIL (res) ;
		if (! MLVIEW_IS_NODE_TYPE_PICKER (res)) {
			mlview_utils_trace_debug
			("Expected a Node type picker, found "
			 "an unknown type" ) ;
			return NULL ;
		}
		gtk_window_set_modal (GTK_WINDOW (res), TRUE) ;
		mlview_tree_editor_set_node_type_picker
		(a_this, MLVIEW_NODE_TYPE_PICKER (res)) ;
	}
	res = GTK_WIDGET (PRIVATE (a_this)->node_type_picker) ;
	return MLVIEW_NODE_TYPE_PICKER (res) ;
}

/**
 *Sets the node type picker associated to
 *the current instance of #MlViewTreeEditor.
 *If a node type picker was already associated to
 *the current instance of #MlViewTreeEditor, unref it
 *before associating the new one.
 *@return MLVIEW_OK upon successful completion, an error code
 *otherwise.
 */
enum MlViewStatus
mlview_tree_editor_set_node_type_picker (MlViewTreeEditor *a_this,
        MlViewNodeTypePicker *a_picker)

{
	g_return_val_if_fail (a_this
	                      && MLVIEW_TREE_EDITOR (a_this)
	                      && PRIVATE (a_this)
	                      && a_picker
	                      && MLVIEW_IS_NODE_TYPE_PICKER (a_picker),
	                      MLVIEW_BAD_PARAM_ERROR) ;

	if (PRIVATE (a_this)->node_type_picker) {
		g_object_unref
		(G_OBJECT (PRIVATE (a_this)->node_type_picker)) ;
	}
	PRIVATE (a_this)->node_type_picker = a_picker ;
	return MLVIEW_OK ;
}

/**
 *Gets the GtkTreeRowReference associated
 *to the node pointed to by a_iter. User doesn't
 *need to free the returned GtkTreeRowReference because
 *MlViewTreeEditor takes care of it.
 *@param a_this the current instance of #MlViewTreeEditor
 *@param a_iter the node iterator to consider.
 *@return the relevant GtkTreeRowReference, or NULL in case
 *of an error.
 */
GtkTreeRowReference *
mlview_tree_editor_iter_2_row_ref (MlViewTreeEditor *a_this,
                                   GtkTreeIter *a_iter)
{
	GtkTreeRowReference *result=NULL ;
	xmlNode *xml_node=NULL ;
	GtkTreeModel *model=NULL ;

	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_TREE_EDITOR (a_this)
	                      && PRIVATE (a_this)
	                      && PRIVATE (a_this)->nodes_rows_hash
	                      && a_iter, NULL) ;

	model = mlview_tree_editor_get_model (a_this) ;
	gtk_tree_model_get (model, a_iter, XML_NODE_COLUMN,
	                    &xml_node, -1) ;
	THROW_IF_FAIL (xml_node) ;
	result = (GtkTreeRowReference*) g_hash_table_lookup
	         (PRIVATE (a_this)->nodes_rows_hash, xml_node) ;
	return result ;
}

GtkTreeRowReference *
mlview_tree_editor_xml_node_2_row_reference (MlViewTreeEditor *a_this,
        xmlNode *a_node)
{
	GtkTreeRowReference *result = NULL ;
	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_TREE_EDITOR (a_this)
	                      && PRIVATE (a_this)
	                      && a_node,
	                      NULL) ;

	if (PRIVATE (a_this)->nodes_rows_hash) {
		result = (GtkTreeRowReference*) g_hash_table_lookup
		         (PRIVATE (a_this)->nodes_rows_hash, a_node) ;
	}
	return result  ;
}

/**
 *A very important method in this class 
 *(and even in the whole MlView software).
 *It takes an xml document in parameter, 
 *builds the graphical view that matches it and displays it. 
 *
 *@param a_xmldoc the xml document to edit.
 *@param a_editor the current instance of MlViewTreeEditor.
 *@return MLVIEW_OK upon successful completion,
 *an error code otherwise.
 */
enum MlViewStatus
mlview_tree_editor_edit_xml_doc (MlViewTreeEditor * a_this,
                                 MlViewXMLDocument * a_doc)
{
	GtkTreeView *tree_view = NULL;
	GtkTreeSelection *selection = NULL;
	xmlDoc *xml_doc = NULL;

	THROW_IF_FAIL (a_this != NULL);
	THROW_IF_FAIL (MLVIEW_IS_TREE_EDITOR (a_this));
	THROW_IF_FAIL (a_doc != NULL);
	THROW_IF_FAIL (MLVIEW_IS_XML_DOCUMENT (a_doc));

	xml_doc =
	    mlview_xml_document_get_native_document ((MlViewXMLDocument *) a_doc);
	THROW_IF_FAIL (xml_doc != NULL);
	PRIVATE (a_this)->mlview_xml_doc = a_doc;

	tree_view = mlview_tree_editor_build_tree_view_from_xml_doc
	            (a_this, xml_doc);
	THROW_IF_FAIL (tree_view != NULL);
	g_signal_connect (G_OBJECT (tree_view),
	                  "realize",
	                  G_CALLBACK (widget_realized_cb),
	                  a_this) ;

	if (PRIVATE (a_this)->tree_view) {
		gtk_widget_destroy
		(GTK_WIDGET (PRIVATE
		             (a_this)->tree_view));
		PRIVATE (a_this)->tree_view = NULL ;
	}
	if (PRIVATE (a_this)->scrolled_win) {
		gtk_widget_destroy (PRIVATE (a_this)->scrolled_win) ;
		PRIVATE (a_this)->scrolled_win = NULL ;
	}
	PRIVATE (a_this)->tree_view = tree_view;
	selection = gtk_tree_view_get_selection (tree_view);
	THROW_IF_FAIL (selection);
	gtk_tree_selection_set_mode
	(selection, GTK_SELECTION_SINGLE);
	g_signal_connect (G_OBJECT (selection),
	                  "changed",
	                  G_CALLBACK (nodeset_selected_cb),
	                  a_this);

	PRIVATE (a_this)->scrolled_win = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy
	(GTK_SCROLLED_WINDOW (PRIVATE (a_this)->scrolled_win),
						  GTK_POLICY_AUTOMATIC,
						  GTK_POLICY_AUTOMATIC);

	gtk_container_add (GTK_CONTAINER (PRIVATE (a_this)->scrolled_win),
	                   GTK_WIDGET (tree_view));
	gtk_box_pack_start (GTK_BOX (a_this),
						PRIVATE (a_this)->scrolled_win,
	                    TRUE, TRUE, 0);
	gtk_widget_show_all (GTK_WIDGET (a_this));
	set_our_dnd_callbacks (a_this) ;
	gtk_tree_view_enable_model_drag_source
	(tree_view, (GdkModifierType) (GDK_BUTTON1_MASK|GDK_BUTTON2_MASK),
	 row_targets, G_N_ELEMENTS (row_targets),
	 (GdkDragAction) (GDK_ACTION_MOVE | GDK_ACTION_COPY)) ;

	gtk_tree_view_enable_model_drag_dest
	(tree_view, row_targets,
	 G_N_ELEMENTS (row_targets),
	 (GdkDragAction) (GDK_ACTION_MOVE|GDK_ACTION_COPY)) ;

	return MLVIEW_OK ;
}

enum MlViewStatus
mlview_tree_editor_clear (MlViewTreeEditor *a_this)
{
	MlViewTreeEditorClass *klass = NULL ;
	THROW_IF_FAIL (a_this && MLVIEW_IS_TREE_EDITOR (a_this)) ;
	THROW_IF_FAIL (PRIVATE (a_this)) ;

	klass = MLVIEW_TREE_EDITOR_CLASS (G_OBJECT_GET_CLASS (a_this)) ;
	THROW_IF_FAIL (klass) ;
	return klass->clear (a_this) ;
}

enum MlViewStatus
mlview_tree_editor_reload_from_doc (MlViewTreeEditor *a_this)
{
	MlViewTreeEditorClass *klass = NULL ;
	THROW_IF_FAIL (a_this && MLVIEW_IS_TREE_EDITOR (a_this)) ;
	THROW_IF_FAIL (PRIVATE (a_this)) ;

	klass = MLVIEW_TREE_EDITOR_CLASS (G_OBJECT_GET_CLASS (a_this)) ;
	THROW_IF_FAIL (klass) ;

	return klass->reload_from_doc (a_this) ;
}

/**
 *Creates a new xml document inside the tree editor.
 *@param a_this the current instance of #MlViewTreeEditor.
 *@param a_doc the mlview xml document to edit.
 */
void
mlview_tree_editor_create_new_xml_doc (MlViewTreeEditor *a_this,
                                       MlViewXMLDocument *a_doc)
{
	THROW_IF_FAIL (a_this && a_doc);
	mlview_tree_editor_edit_xml_doc (a_this, a_doc);
}

/**
 *Set the root element of the visual xml doc.
 *Note that the xml doc must be empty ,that is, it must
 *not have any root element.
 *@param a_this the current instance of #MlViewTreeEditor
 *@param a_node the new document root.
 *@param a_emit_signals if is TRUE, this function emits the
 *"node-added" and the "tree-changed" signals in case of successful
 *completion.
 */
void
mlview_tree_editor_set_root_element (MlViewTreeEditor *a_this,
                                     xmlNode *a_node,
                                     gboolean a_emit_signals)
{
	xmlDoc *native_doc = NULL ;
	GtkTreeIter iter={0} ;
	xmlNode *node=NULL ;
	GtkTreeModel *model=NULL ;
	gboolean is_ok=TRUE ;
	GtkTreeRowReference *row_ref=NULL ;
	enum MlViewStatus status=MLVIEW_OK ;

	THROW_IF_FAIL (a_this
	                  && MLVIEW_IS_TREE_EDITOR (a_this)
	                  && PRIVATE (a_this)
	                  && PRIVATE (a_this)->tree_view
	                  && a_node) ;
	native_doc =  mlview_xml_document_get_native_document
	              (PRIVATE (a_this)->mlview_xml_doc) ;
	THROW_IF_FAIL (native_doc) ;
	node = xmlDocGetRootElement (native_doc) ;
	THROW_IF_FAIL (node == NULL) ;
	model = mlview_tree_editor_get_model (a_this) ;
	THROW_IF_FAIL (model) ;
	xmlDocSetRootElement (native_doc,
	                      a_node) ;
	is_ok = gtk_tree_model_get_iter_first (model, &iter) ;
	THROW_IF_FAIL (is_ok == TRUE) ;
	status = mlview_tree_editor_build_tree_model_from_xml_tree (a_this, a_node, &iter,
	         INSERT_TYPE_APPEND_CHILD_VISIT_NEXT,
	         &model) ;
	THROW_IF_FAIL (status == MLVIEW_OK) ;
	if (a_emit_signals == TRUE) {
		row_ref = (GtkTreeRowReference*)g_hash_table_lookup
		          (PRIVATE (a_this)->nodes_rows_hash, a_node) ;
		THROW_IF_FAIL (row_ref) ;
		g_signal_emit (G_OBJECT (a_this),
		               gv_signals[NODE_ADDED],0,
		               row_ref) ;
		g_signal_emit (G_OBJECT (a_this),
		               gv_signals[TREE_CHANGED], 0) ;
	}
}

/**
 *Gets a tree iterator that points to the tree row associated
 *to a given xml node.
 *An example of use of this function is:
 *GtkTreeIter iter ;
 *enum MlViewStatus status ;
 *status = mlview_tree_editor_get_iter (a_this,
 *                                       an_xml_node,
 *                                       &iter) ;
 *@param a_this the current instance of #MlViewTreeEditor.
 *@param a_node the xml node to consider.
 *@param a_iter out parameter the place where to copy the
 *iterator. Must have been allocated by the caller.
 *@return MLVIEW_OK upon successful completion, an error code
 *otherwise.
 */
enum MlViewStatus
mlview_tree_editor_get_iter (MlViewTreeEditor * a_this,
                             xmlNode * a_node,
                             GtkTreeIter * a_iter)
{
	GtkTreeModel *model = NULL;
	GtkTreeRowReference *row_ref = NULL;
	GtkTreePath *tree_path = NULL;
	gboolean is_ok = FALSE;

	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_TREE_EDITOR (a_this)
	                      && PRIVATE (a_this)
	                      && PRIVATE (a_this)->nodes_rows_hash
	                      && a_iter, MLVIEW_BAD_PARAM_ERROR);

	model = mlview_tree_editor_get_model (a_this);
	THROW_IF_FAIL (model);

	row_ref = (GtkTreeRowReference*) g_hash_table_lookup
	          (PRIVATE (a_this)->nodes_rows_hash, a_node);
	if (!row_ref)
		return MLVIEW_NODE_NOT_FOUND_ERROR;
	tree_path = gtk_tree_row_reference_get_path (row_ref);
	THROW_IF_FAIL (tree_path);
	is_ok = gtk_tree_model_get_iter (model, a_iter,
	                                 tree_path);
	if (tree_path) {
		gtk_tree_path_free (tree_path) ;
		tree_path = NULL ;
	}
	if (is_ok == TRUE)
		return MLVIEW_OK;
	return MLVIEW_ERROR;
}

/**
 *Gets a row reference pointer to the first node
 *a user selection.
 *@param a_this the current instance of #MlViewTreeEditor
 *@return the row reference pointer.
 */
GtkTreeRowReference*
mlview_tree_editor_get_cur_sel_start (MlViewTreeEditor *a_this)
{
	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_TREE_EDITOR (a_this)
	                      && PRIVATE (a_this),
	                      NULL) ;

	return PRIVATE (a_this)->cur_sel_start ;
}

/**
 *Gets an iterator on the first element of a selection.
 *@param a_this the current instance of #MlViewTreeEditor
 *@param a_iter out parameter, the iterator to fill. Must be allocated
 *by the caller.
 *@return MLVIEW_OK upon successful completion, an error code otherwise.
 */
enum MlViewStatus
mlview_tree_editor_get_cur_sel_start_iter (MlViewTreeEditor *a_this,
        GtkTreeIter *a_iter)
{
	GtkTreeModel *model=NULL ;
	GtkTreePath *tree_path=NULL ;
	gboolean is_ok=TRUE ;
	enum MlViewStatus status = MLVIEW_OK ;

	g_return_val_if_fail (a_this
	                      && MLVIEW_TREE_EDITOR (a_this)
	                      && PRIVATE (a_this)
	                      && a_iter,
	                      MLVIEW_BAD_PARAM_ERROR) ;

	if (!PRIVATE (a_this)->cur_sel_start) {
		return MLVIEW_NODE_NOT_FOUND_ERROR ;
	}
	tree_path = gtk_tree_row_reference_get_path
	            (PRIVATE (a_this)->cur_sel_start) ;
	THROW_IF_FAIL (tree_path) ;
	model = mlview_tree_editor_get_model (a_this) ;
	if (!model) {
		mlview_utils_trace_debug ("model failed") ;
		status = MLVIEW_OK ;
		goto cleanup ;
	}
	is_ok = gtk_tree_model_get_iter (model, a_iter, tree_path) ;
	if (is_ok != TRUE) {
		mlview_utils_trace_debug ("is_ok == TRUE failed") ;
		status = MLVIEW_OK ;
		goto cleanup ;
	}

cleanup:
	if (tree_path) {
		gtk_tree_path_free (tree_path) ;
		tree_path = NULL ;
	}
	return status ;
}

/**
 *Gets the GtkTreePath associated to the current selected
 *@param a_this the current instance of #MlViewTreeEditor
 *@param a_tree_path the tree path built. Must be freed by
 *gtk_tree_path_free()
 */
enum MlViewStatus
mlview_tree_editor_get_cur_sel_start_tree_path (MlViewTreeEditor *a_this,
        GtkTreePath **a_tree_path)
{
	GtkTreePath *tree_path=NULL ;
	enum MlViewStatus status = MLVIEW_OK ;

	g_return_val_if_fail (a_this && MLVIEW_IS_TREE_EDITOR (a_this)
	                      && PRIVATE (a_this)
	                      && a_tree_path,
	                      MLVIEW_BAD_PARAM_ERROR) ;

	if (!PRIVATE (a_this)->cur_sel_start) {
		return MLVIEW_NODE_NOT_FOUND_ERROR ;
	}
	tree_path = gtk_tree_row_reference_get_path
	            (PRIVATE (a_this)->cur_sel_start) ;
	THROW_IF_FAIL (tree_path) ;

	*a_tree_path = tree_path ;
	tree_path = NULL ;
	return status ;
}

/**
 *Gets the xml node associated to the current selected
 *Row.
 *@param a_this the current instance of #MlViewTreeEditor
 *@return the xml node or NULL if something bad happened.
 */
xmlNode *
mlview_tree_editor_get_cur_sel_xml_node (MlViewTreeEditor *a_this)
{
	GtkTreeIter iter = {0} ;
	enum MlViewStatus status = MLVIEW_OK ;
	xmlNode *result ;
	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_TREE_EDITOR (a_this)
	                      && PRIVATE (a_this), NULL) ;
	status = mlview_tree_editor_get_cur_sel_start_iter (a_this, &iter) ;
	THROW_IF_FAIL (status == MLVIEW_OK) ;
	result = mlview_tree_editor_get_xml_node (a_this, &iter) ;
	return result ;
}

/**
 *Gets the instance of xmlNode* associated to
 *an instance of GtkTreeIter*
 *@param a_this the current instance of #MlViewTreeEditor.
 *@param a_iter the iterator to consider.
 *@return the xmlNode associated to the iterator or NULL.
 */
xmlNode *
mlview_tree_editor_get_xml_node (MlViewTreeEditor * a_this,
                                 GtkTreeIter * a_iter)
{
	GtkTreeModel *model = NULL;
	xmlNode *result = NULL;

	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_TREE_EDITOR (a_this)
	                      && PRIVATE (a_this)
	                      && a_iter, NULL);

	model = mlview_tree_editor_get_model (a_this) ;
	THROW_IF_FAIL (model);
	gtk_tree_model_get (model, a_iter,
	                    XML_NODE_COLUMN, &result, -1);
	return result;
}

/**
 *Gets the instance of xmlNode* associated to
 *an instance of GtkTreeRowReference.
 *@param a_this the current instance of #MlViewTreeEditor.
 *@param a_iter the instance of GtkTreeRowReference to consider.
 *@return the xmlNode associated to the iterator or NULL.
 */
xmlNode *
mlview_tree_editor_get_xml_node2 (MlViewTreeEditor * a_this,
                                  GtkTreeRowReference * a_row_ref)
{
	GtkTreeModel *model=NULL;
	GtkTreePath *tree_path=NULL ;
	xmlNode *result=NULL;

	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_TREE_EDITOR (a_this)
	                      && PRIVATE (a_this)
	                      && a_row_ref, NULL);

	model = mlview_tree_editor_get_model (a_this);
	THROW_IF_FAIL (model);
	tree_path = gtk_tree_row_reference_get_path (a_row_ref) ;
	THROW_IF_FAIL (tree_path) ;
	result = mlview_tree_editor_get_xml_node3 (a_this, tree_path) ;
	if (tree_path) {
		gtk_tree_path_free (tree_path) ;
		tree_path = NULL ;
	}
	return result;
}

/**
 *Gets the instance of xmlNode associated to an instance
 *of GtkTreePath.
 *@param a_this the current instance of #MlViewTreeEditor
 *@param a_path the path to a graphical GtkTreeModel node.
 *@return the xml node associated to a_path or NULL.
 */
xmlNode *
mlview_tree_editor_get_xml_node3 (MlViewTreeEditor *a_this,
                                  GtkTreePath *a_path)
{
	xmlNode *result = NULL ;
	GtkTreeModel *model = NULL ;
	gboolean is_ok = TRUE ;
	GtkTreeIter iter = {0} ;

	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_TREE_EDITOR (a_this)
	                      && PRIVATE (a_this)
	                      && a_path,
	                      NULL) ;
	model = mlview_tree_editor_get_model (a_this) ;
	THROW_IF_FAIL (model) ;
	is_ok=gtk_tree_model_get_iter (model, &iter, a_path) ;
	if (is_ok != TRUE) {
		mlview_utils_trace_debug ("is_ok == TRUE failed") ;
		return NULL ;
	}
	gtk_tree_model_get (model, &iter,
	                    XML_NODE_COLUMN, &result, -1);
	return result ;
}

/**
 *Adds a child node to the node pointed to by a_parent_iter.
 *@param a_this the current instance of #MlViewTreeEditor
 *@param a_parent_iter an iterator to the parent of the node
 *to be added.
 *@param a_node the xml node to add.
 *@return MVLIEW_OK upon successful completion, an error code
 *otherwise.
 */
enum MlViewStatus
mlview_tree_editor_add_child_node (MlViewTreeEditor * a_this,
                                   GtkTreeIter * a_parent_iter,
                                   xmlNode * a_node)
{
	xmlNode *parent_xml_node = NULL ;
	gchar *parent_xml_node_path = NULL ;
	enum MlViewStatus status = MLVIEW_OK ;

	THROW_IF_FAIL (a_this && MLVIEW_IS_TREE_EDITOR (a_this) && PRIVATE (a_this)
				   && a_parent_iter && a_node);

	parent_xml_node = mlview_tree_editor_get_xml_node
	                  (a_this, a_parent_iter);
	THROW_IF_FAIL (parent_xml_node);
	mlview_xml_document_get_node_path (PRIVATE (a_this)->mlview_xml_doc,
	                                   parent_xml_node,
	                                   &parent_xml_node_path) ;
	if (!parent_xml_node_path) {
		mlview_utils_trace_debug ("Could not get node path") ;
		return MLVIEW_ERROR ;
	}
	mlview::AppContext *context = mlview::AppContext::get_instance () ;
	THROW_IF_FAIL (context) ;

	/*
	 *Some sanity checks
	 */
	if (a_node->type == XML_ENTITY_DECL) {
		if (parent_xml_node->type != XML_DTD_NODE) {
			context->error (_("An entity declaration node can "
							"only be a child of an internal subset node")) ;
			status = MLVIEW_BAD_NODE_PARENT_ERROR ;
			goto cleanup ;
		}
	} else if (parent_xml_node->type == XML_DTD_NODE &&
	           a_node->type != XML_ENTITY_DECL) {
		context->error (_("Nodes of the selected type cannot be a "
					    "DTD node's children."));
		status = MLVIEW_BAD_NODE_PARENT_ERROR;
		goto cleanup ;
	} else if (parent_xml_node->type == XML_DOCUMENT_NODE) {
		xmlNode *root_elem = NULL ;
		if (root_elem->doc)
			root_elem = xmlDocGetRootElement (parent_xml_node->doc) ;
		if (root_elem) {
			/*
			 *we are trying to add a second root element 
			 *to the doc.
			 *That's kinda forbiden
			 */
			context->error (_("The xml document already has a root element"));
			status =  MLVIEW_BAD_NODE_PARENT_ERROR ;
			goto cleanup ;
		}
	}
#ifndef MLVLIEW_WITH_LIBXML_EXPERIMENTAL_COMPLETION
	status = mlview_xml_document_add_child_node
	         (PRIVATE (a_this)->mlview_xml_doc,
	          parent_xml_node_path, a_node, TRUE,
	          TRUE);
#else

	status = mlview_xml_document_add_child_node
	         (PRIVATE (a_this)->mlview_xml_doc,
	          parent_xml_node_path, a_node, TRUE,
	          TRUE);
#endif

cleanup:
	if (parent_xml_node_path) {
		g_free (parent_xml_node_path)  ;
		parent_xml_node_path = NULL ;
	}
	return status ;
}

/**
 *Asks the user for the type of node he wants to add and adds it.
 *@param a_this the current instance of #MlViewTreeEditor
 */
void
mlview_tree_editor_add_child_node_interactive (MlViewTreeEditor *a_this)
{
	MlViewNodeTypePicker *picker=NULL ;
	gint button=0 ;
	xmlNode *cur_node=NULL ;

	THROW_IF_FAIL (a_this
	                  && MLVIEW_IS_TREE_EDITOR (a_this)
	                  && PRIVATE (a_this)->cur_sel_start) ;

	cur_node = mlview_tree_editor_get_xml_node2
	           (a_this, PRIVATE (a_this)->cur_sel_start) ;
	THROW_IF_FAIL (cur_node) ;
	mlview::AppContext *context = mlview::AppContext::get_instance () ;
	THROW_IF_FAIL (context) ;
	/*only an element node should have children*/
	if (cur_node->type != XML_ELEMENT_NODE
	        && cur_node->type != XML_DTD_NODE) {
		context->error (_("The currently selected node cannot have children."));

		return ;
	}
	picker = mlview_tree_editor_get_node_type_picker (a_this) ;
	THROW_IF_FAIL (picker) ;
	mlview_node_type_picker_set_title (picker,
	                                   _("add a child node")) ;
	mlview_node_type_picker_build_element_name_choice_list
	(picker, ADD_CHILD, cur_node) ;
	mlview_node_type_picker_select_node_name_or_content_entry_text
	(picker) ;
	button = gtk_dialog_run (GTK_DIALOG (picker)) ;
	switch (button) {
	case GTK_RESPONSE_ACCEPT:
		handle_nt_picker_ok_button_clicked_to_add_child
		(a_this) ;
		break ;
	default:
		break ;
	}
	gtk_widget_hide
	(GTK_WIDGET (PRIVATE (a_this)->node_type_picker)) ;
}

enum MlViewStatus
mlview_tree_editor_add_child_element_node (MlViewTreeEditor *a_this,
        const gchar *a_element_name,
        gboolean a_start_edit_element)
{
	MlViewXMLDocument *xml_doc = NULL ;
	xmlNode *node = NULL ;
	GtkTreeIter iter = {0} ;
	NodeTypeDefinition node_type_def = {NULL, XML_ELEMENT_NODE,
	                                    XML_INTERNAL_GENERAL_ENTITY} ;
	enum MlViewStatus status = MLVIEW_OK ;

	g_return_val_if_fail (a_this && MLVIEW_IS_TREE_EDITOR (a_this),
	                      MLVIEW_BAD_PARAM_ERROR) ;

	status = mlview_tree_editor_get_cur_sel_start_iter (a_this, &iter) ;
	THROW_IF_FAIL (status == MLVIEW_OK) ;

	xml_doc = mlview_tree_editor_get_mlview_xml_doc (a_this) ;
	THROW_IF_FAIL (xml_doc) ;

	node = new_xml_node (&node_type_def, xml_doc) ;
	THROW_IF_FAIL (node) ;

	xmlNodeSetName (node, (xmlChar*)a_element_name) ;

	status = mlview_tree_editor_add_child_node (a_this, &iter, node) ;
	if (status == MLVIEW_OK && a_start_edit_element == TRUE) {
		mlview_tree_editor_start_editing_node (a_this, node) ;
	}
	return MLVIEW_OK ;
}

/**
 *Adds a child element node to the current selected node.
 *@param a_this the current instance of #MlViewTreeEditor
 */
void
mlview_tree_editor_add_child_element_interactive (MlViewTreeEditor *a_this)
{
	THROW_IF_FAIL (a_this
	                  && MLVIEW_IS_TREE_EDITOR (a_this)
	                  && PRIVATE (a_this)
	                  && PRIVATE (a_this)->cur_sel_start) ;

	mlview_tree_editor_add_child_element_node (a_this, "element", TRUE) ;
}

void
mlview_tree_editor_add_child_text_node (MlViewTreeEditor *a_this,
                                        const gchar* a_text,
                                        gboolean a_start_editing)
{
	xmlNode *cur_node=NULL, *new_node = NULL ;
	MlViewXMLDocument *xml_doc = NULL ;
	NodeTypeDefinition node_type_def = {NULL, XML_TEXT_NODE,
	                                    XML_INTERNAL_GENERAL_ENTITY} ;
	GtkTreeIter iter  ;
	enum MlViewStatus status = MLVIEW_OK ;

	THROW_IF_FAIL (a_this
	                  && MLVIEW_IS_TREE_EDITOR (a_this)
	                  && PRIVATE (a_this)
	                  && PRIVATE (a_this)->cur_sel_start) ;

	memset ((char*)&iter, 0, sizeof (iter)) ;
	cur_node = mlview_tree_editor_get_xml_node2
	           (a_this, PRIVATE (a_this)->cur_sel_start) ;
	THROW_IF_FAIL (cur_node) ;
	xml_doc = mlview_tree_editor_get_mlview_xml_doc (a_this);
	THROW_IF_FAIL (xml_doc) ;
	new_node = new_xml_node (&node_type_def, xml_doc) ;
	if (!new_node) {
		mlview_utils_trace_debug ("Couldn't instanciate a new xml node") ;
		return ;
	}
	xmlNodeSetContent (new_node, (xmlChar*)a_text) ;
	status = mlview_tree_editor_get_cur_sel_start_iter
	         (a_this, &iter) ;
	THROW_IF_FAIL (status == MLVIEW_OK) ;
	status = mlview_tree_editor_add_child_node (a_this,
	         &iter,
	         new_node) ;
	if (status == MLVIEW_OK && a_start_editing) {
		mlview_tree_editor_start_editing_node (a_this, new_node) ;
	}
}

enum MlViewStatus
mlview_tree_editor_insert_prev_text_node (MlViewTreeEditor *a_this,
        const gchar *a_text,
        gboolean a_start_editing)
{

	xmlNode *cur_node=NULL, *new_node = NULL ;
	MlViewXMLDocument *xml_doc = NULL ;
	NodeTypeDefinition node_type_def = {NULL, XML_TEXT_NODE,
	                                    XML_INTERNAL_GENERAL_ENTITY} ;
	GtkTreeIter iter = {0} ;
	enum MlViewStatus status = MLVIEW_OK ;

	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_TREE_EDITOR (a_this)
	                      && PRIVATE (a_this)
	                      && PRIVATE (a_this)->cur_sel_start,
	                      MLVIEW_BAD_PARAM_ERROR) ;

	cur_node = mlview_tree_editor_get_xml_node2
	           (a_this, PRIVATE (a_this)->cur_sel_start) ;
	THROW_IF_FAIL (cur_node) ;
	xml_doc = mlview_tree_editor_get_mlview_xml_doc (a_this);
	THROW_IF_FAIL (xml_doc) ;
	new_node = new_xml_node (&node_type_def, xml_doc) ;
	if (!new_node) {
		mlview_utils_trace_debug ("Couldn't instanciate a new xml node") ;
		return MLVIEW_ERROR;
	}
	xmlNodeSetContent (new_node, (xmlChar*)a_text) ;
	status = mlview_tree_editor_get_cur_sel_start_iter
	         (a_this, &iter) ;

	g_return_val_if_fail (status == MLVIEW_OK,
	                      MLVIEW_ERROR) ;

	status = mlview_tree_editor_insert_sibling_node (a_this, &iter,
	         new_node, TRUE) ;
	if (status == MLVIEW_OK && a_start_editing) {
		mlview_tree_editor_start_editing_node (a_this, new_node) ;
	}
	return MLVIEW_OK ;
}


enum MlViewStatus
mlview_tree_editor_insert_next_text_node (MlViewTreeEditor *a_this,
        const gchar *a_text,
        gboolean a_start_editing)
{

	xmlNode *cur_node=NULL, *new_node = NULL ;
	MlViewXMLDocument *xml_doc = NULL ;
	NodeTypeDefinition node_type_def = {NULL, XML_TEXT_NODE,
	                                    XML_INTERNAL_GENERAL_ENTITY} ;
	GtkTreeIter iter = {0} ;
	enum MlViewStatus status = MLVIEW_OK ;

	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_TREE_EDITOR (a_this)
	                      && PRIVATE (a_this)
	                      && PRIVATE (a_this)->cur_sel_start,
	                      MLVIEW_BAD_PARAM_ERROR) ;

	cur_node = mlview_tree_editor_get_xml_node2
	           (a_this, PRIVATE (a_this)->cur_sel_start) ;
	THROW_IF_FAIL (cur_node) ;
	xml_doc = mlview_tree_editor_get_mlview_xml_doc (a_this);
	THROW_IF_FAIL (xml_doc) ;
	new_node = new_xml_node (&node_type_def, xml_doc) ;
	if (!new_node) {
		mlview_utils_trace_debug ("Couldn't instanciate a new xml node") ;
		return MLVIEW_ERROR;
	}
	xmlNodeSetContent (new_node, (xmlChar*)a_text) ;
	status = mlview_tree_editor_get_cur_sel_start_iter
	         (a_this, &iter) ;

	g_return_val_if_fail (status == MLVIEW_OK,
	                      MLVIEW_ERROR) ;

	status = mlview_tree_editor_insert_sibling_node (a_this, &iter,
	         new_node, FALSE) ;
	if (status == MLVIEW_OK && a_start_editing) {
		mlview_tree_editor_start_editing_node (a_this, new_node) ;
	}
	return MLVIEW_OK ;
}

/**
 *Adds a child text node node to the current selected node.
 *@param a_this the current instance of #MlViewTreeEditor
 */
void
mlview_tree_editor_add_child_text_node_interactive (MlViewTreeEditor *a_this)
{
	THROW_IF_FAIL (a_this
	                  && MLVIEW_IS_TREE_EDITOR (a_this)
	                  && PRIVATE (a_this)
	                  && PRIVATE (a_this)->cur_sel_start) ;

	mlview_tree_editor_add_child_text_node (a_this, "text content", TRUE) ;

}

/**
 *Asks the user for the type of nodes she wants
 *to insert an inserts it.
 *@param a_this the current instance of #MlViewTreeEditor
 */
void
mlview_tree_editor_insert_prev_sibling_node_interactive (MlViewTreeEditor *a_this)
{
	MlViewNodeTypePicker *picker=NULL ;
	gint button=0 ;
	xmlNode *cur_node=NULL ;

	THROW_IF_FAIL (a_this
	                  && MLVIEW_IS_TREE_EDITOR (a_this)
	                  && PRIVATE (a_this)
	                  && PRIVATE (a_this)->cur_sel_start) ;

	picker = mlview_tree_editor_get_node_type_picker (a_this) ;
	THROW_IF_FAIL (picker) ;
	mlview_node_type_picker_set_title
	(picker, _("insert a previous sibling node")) ;
	mlview_node_type_picker_select_node_name_or_content_entry_text
	(picker) ;
	/*
	 *insert a flag to indicate that the picker is used
	 *to insert node as previous node.
	 *this is used by function
	 *mlview_tree_editor_insert_prev_sibling_node_interactive().
	 */
	g_object_set_data (G_OBJECT (a_this), "prev",
	                   GINT_TO_POINTER (TRUE)) ;
	cur_node = mlview_tree_editor_get_xml_node2
	           (a_this, PRIVATE (a_this)->cur_sel_start) ;
	THROW_IF_FAIL (cur_node) ;
	mlview_node_type_picker_build_element_name_choice_list
	(picker, INSERT_BEFORE, cur_node) ;

	button = gtk_dialog_run (GTK_DIALOG (picker)) ;
	switch (button) {
	case GTK_RESPONSE_ACCEPT:/*OK button*/
		handle_nt_picker_ok_button_clicked_to_insert_sibling_node
		(a_this) ;
		break ;
	default:
		break ;
	}
	gtk_widget_hide
	(GTK_WIDGET (PRIVATE (a_this)->node_type_picker)) ;
}

void
mlview_tree_editor_insert_prev_sibling_element_node (MlViewTreeEditor *a_this,
        const gchar *a_element_name,
        gboolean a_start_editing)
{
	xmlNode *cur_node=NULL, *new_node = NULL ;
	MlViewXMLDocument *xml_doc = NULL ;
	NodeTypeDefinition node_type_def = {NULL, XML_ELEMENT_NODE,
	                                    XML_INTERNAL_GENERAL_ENTITY} ;
	GtkTreeIter iter = {0} ;
	enum MlViewStatus status = MLVIEW_OK ;

	THROW_IF_FAIL (a_this
	                  && MLVIEW_IS_TREE_EDITOR (a_this)
	                  && PRIVATE (a_this)
	                  && PRIVATE (a_this)->cur_sel_start) ;

	cur_node = mlview_tree_editor_get_xml_node2
	           (a_this, PRIVATE (a_this)->cur_sel_start) ;
	THROW_IF_FAIL (cur_node) ;
	xml_doc = mlview_tree_editor_get_mlview_xml_doc (a_this);
	THROW_IF_FAIL (xml_doc) ;
	new_node = new_xml_node (&node_type_def, xml_doc) ;
	if (!new_node) {
		mlview_utils_trace_debug ("Couldn't instanciate a new xml node") ;
		return ;
	}
	xmlNodeSetName (new_node, (xmlChar*)a_element_name) ;
	status = mlview_tree_editor_get_cur_sel_start_iter
	         (a_this, &iter) ;
	THROW_IF_FAIL (status == MLVIEW_OK) ;
	status = mlview_tree_editor_insert_sibling_node (a_this, &iter,
	         new_node,
	         TRUE) ;
	if (status == MLVIEW_OK && a_start_editing == TRUE) {
		mlview_tree_editor_start_editing_node (a_this, new_node) ;
	}
}


void
mlview_tree_editor_insert_next_sibling_element_node (MlViewTreeEditor *a_this,
        const gchar *a_element_name,
        gboolean a_start_editing)
{
	xmlNode *cur_node=NULL, *new_node = NULL ;
	MlViewXMLDocument *xml_doc = NULL ;
	NodeTypeDefinition node_type_def = {NULL, XML_ELEMENT_NODE,
	                                    XML_INTERNAL_GENERAL_ENTITY} ;
	GtkTreeIter iter = {0} ;
	enum MlViewStatus status = MLVIEW_OK ;

	THROW_IF_FAIL (a_this
	                  && MLVIEW_IS_TREE_EDITOR (a_this)
	                  && PRIVATE (a_this)
	                  && PRIVATE (a_this)->cur_sel_start) ;

	cur_node = mlview_tree_editor_get_xml_node2
	           (a_this, PRIVATE (a_this)->cur_sel_start) ;
	THROW_IF_FAIL (cur_node) ;
	xml_doc = mlview_tree_editor_get_mlview_xml_doc (a_this);
	THROW_IF_FAIL (xml_doc) ;
	new_node = new_xml_node (&node_type_def, xml_doc) ;
	if (!new_node) {
		mlview_utils_trace_debug ("Couldn't instanciate a new xml node") ;
		return ;
	}
	xmlNodeSetName (new_node, (xmlChar*)a_element_name) ;
	status = mlview_tree_editor_get_cur_sel_start_iter
	         (a_this, &iter) ;
	THROW_IF_FAIL (status == MLVIEW_OK) ;
	status = mlview_tree_editor_insert_sibling_node (a_this, &iter,
	         new_node, FALSE) ;
	if (status == MLVIEW_OK && a_start_editing == TRUE) {
		mlview_tree_editor_start_editing_node (a_this, new_node) ;
	}
}

/**
 *This should be called in the context of text like editing.
 *It creates and inserts a prev sibling element.
 *@param a_this the current instance of #MlViewTreeEditor
 */
void
mlview_tree_editor_insert_prev_sibling_element_interactive (MlViewTreeEditor *a_this)
{

	THROW_IF_FAIL (a_this
	                  && MLVIEW_IS_TREE_EDITOR (a_this)
	                  && PRIVATE (a_this)
	                  && PRIVATE (a_this)->cur_sel_start) ;

	mlview_tree_editor_insert_prev_sibling_element_node (a_this,
	        "element",
	        TRUE) ;
}

/**
 *Asks the user for the type of node she wants
 *to insert and inserts it as a next sibling of the
 *currently selected node.
 *@param a_this the current instance of #MlViewTreeEditor.
 */
void
mlview_tree_editor_insert_next_sibling_node_interactive (MlViewTreeEditor *a_this)
{
	MlViewNodeTypePicker *picker=NULL ;
	gint button=0 ;
	xmlNode *cur_node=NULL ;

	THROW_IF_FAIL (a_this
	                  && MLVIEW_IS_TREE_EDITOR (a_this)
	                  && PRIVATE (a_this)
	                  && PRIVATE (a_this)->cur_sel_start) ;

	picker = mlview_tree_editor_get_node_type_picker (a_this) ;
	THROW_IF_FAIL (picker) ;
	mlview_node_type_picker_set_title
	(picker, _("insert a next sibling node")) ;
	mlview_node_type_picker_select_node_name_or_content_entry_text
	(picker) ;
	/*
	 *insert a flag to indicate that the picker is used
	 *to insert node as next sibling node.
	 *this is used by function
	 *mlview_tree_editor_insert_next_sibling_node_interactive().
	 */
	g_object_set_data (G_OBJECT (a_this), "prev",
	                   GINT_TO_POINTER (FALSE)) ;
	cur_node = mlview_tree_editor_get_xml_node2
	           (a_this, PRIVATE (a_this)->cur_sel_start) ;
	THROW_IF_FAIL (cur_node) ;
	mlview_node_type_picker_build_element_name_choice_list
	(picker, INSERT_BEFORE, cur_node) ;

	button = gtk_dialog_run (GTK_DIALOG (picker)) ;
	switch (button) {
	case GTK_RESPONSE_ACCEPT:/*OK button*/
		handle_nt_picker_ok_button_clicked_to_insert_sibling_node
		(a_this) ;
		break ;
	default:
		break ;
	}
	gtk_widget_hide
	(GTK_WIDGET (PRIVATE (a_this)->node_type_picker)) ;
}

/**
 *This should be called in the context of text like editing.
 *It creates and inserts a next sibling elment.
 *@param a_this the current instance of #MlViewTreeEditor
 */
void
mlview_tree_editor_insert_next_sibling_element_interactive (MlViewTreeEditor *a_this)
{
	xmlNode *cur_node=NULL, *new_node = NULL ;
	MlViewXMLDocument *xml_doc = NULL ;
	NodeTypeDefinition node_type_def = {NULL, XML_ELEMENT_NODE,
	                                    XML_INTERNAL_GENERAL_ENTITY} ;
	GtkTreeIter iter = {0} ;
	enum MlViewStatus status = MLVIEW_OK ;

	THROW_IF_FAIL (a_this
	                  && MLVIEW_IS_TREE_EDITOR (a_this)
	                  && PRIVATE (a_this)
	                  && PRIVATE (a_this)->cur_sel_start) ;

	cur_node = mlview_tree_editor_get_xml_node2
	           (a_this, PRIVATE (a_this)->cur_sel_start) ;
	THROW_IF_FAIL (cur_node) ;
	xml_doc = mlview_tree_editor_get_mlview_xml_doc (a_this);
	THROW_IF_FAIL (xml_doc) ;
	new_node = new_xml_node (&node_type_def, xml_doc) ;
	if (!new_node) {
		mlview_utils_trace_debug ("Couldn't instanciate a new xml node") ;
		return ;
	}
	xmlNodeSetName (new_node, (xmlChar*)"element") ;
	status = mlview_tree_editor_get_cur_sel_start_iter
	         (a_this, &iter) ;
	THROW_IF_FAIL (status == MLVIEW_OK) ;
	status = mlview_tree_editor_insert_sibling_node (a_this, &iter,
	         new_node,
	         FALSE) ;
	if (status == MLVIEW_OK) {
		g_object_set_data (G_OBJECT (a_this),
		                   "new-node",
		                   new_node) ;
		g_idle_add ((GSourceFunc)start_editing_node_in_idle_time,
		            a_this) ;
	}
}


/**
 *Inserts a sibling node to the xml document.
 *@param a_this the current instance of #MlViewTreeEditor
 *@param a_ref_iter an iterator to the reference row of the visual
 *tree.
 *@param a_node the node to add before/previous the node
 *pointed to by a_ref_iter.
 *@param a_previous if set to TRUE, a_node is inserted before
 *the node pointed to by a_ref_iter, otherwise a_node is inserted
 *after the node pointed to by a_ref_iter.
 *@return MLVIEW_OK upon successful completion, an error code
 *otherwise.
 */
enum MlViewStatus
mlview_tree_editor_insert_sibling_node (MlViewTreeEditor *a_this,
                                        GtkTreeIter * a_ref_iter,
                                        xmlNode * a_node,
                                        gboolean a_previous)
{
	enum MlViewStatus status = MLVIEW_OK ;
	GtkTreeRowReference *row_ref = NULL;
	xmlNode *ref_node = NULL ;
	gchar *ref_node_path = NULL ;

	THROW_IF_FAIL (a_this && MLVIEW_IS_TREE_EDITOR (a_this) && PRIVATE (a_this)
				   && PRIVATE (a_this)->nodes_rows_hash && a_node && a_ref_iter);

	mlview::AppContext *context = mlview::AppContext::get_instance () ;
	THROW_IF_FAIL (context) ;
	/*make sure a_node hasn't be drawn already */
	row_ref = (GtkTreeRowReference*)g_hash_table_lookup
	          (PRIVATE (a_this)->nodes_rows_hash, a_node);
	THROW_IF_FAIL (row_ref == NULL);

	ref_node = mlview_tree_editor_get_xml_node (a_this,
	           a_ref_iter);
	THROW_IF_FAIL (ref_node);
	/*
	 *Some sanity checks.
	 */
	if (a_node->type == XML_ENTITY_DECL) {
		if (ref_node->parent->type != XML_DTD_NODE) {
			context->error (_("An entity declaration node "
							  "can only be a child of an internal subset node")) ;
			return MLVIEW_BAD_NODE_PARENT_ERROR ;
		}
	} else {
		xmlNode *root_elem = NULL ;
		root_elem = xmlDocGetRootElement (ref_node->doc) ;
		if (ref_node == root_elem) {
			/*
			 *we are trying to add a previous sibling to
			 *a document root element.
			 */
			if (a_previous == TRUE
			        && (a_node->type != XML_DTD_NODE)) {
				context->error (_("Only DTD nodes are "
								  "allowed before the document root elements")) ;
				return MLVIEW_BAD_NODE_PARENT_ERROR ;
			} else if (a_previous == FALSE
			           && (a_node->type != XML_DTD_NODE)) {
				context->error (_("A document root element "
								  "cannot have next sibling nodes")) ;
				return MLVIEW_BAD_NODE_PARENT_ERROR ;
			}
		}
	}
	/*
	 *call mlview_xml_document_insert_x_sibling_node()
	 *to actualy insert the node.
	 */
	mlview_xml_document_get_node_path (PRIVATE (a_this)->mlview_xml_doc,
	                                   ref_node,
	                                   &ref_node_path) ;
	if (!ref_node_path) {
		mlview_utils_trace_debug ("Could not get node path") ;
		return MLVIEW_ERROR ;
	}

	if (a_previous == TRUE) {
#ifndef MLVIEW_WITH_LIBXML_EXPERIMENTAL_COMPLETION
		status =
		    mlview_xml_document_insert_prev_sibling_node
		    (PRIVATE (a_this)->mlview_xml_doc,
		     ref_node_path, a_node, TRUE, TRUE);
#else

		status =
		    mlview_xml_document_insert_prev_sibling_node
		    (PRIVATE (a_this)->mlview_xml_doc,
		     ref_node_path, a_node, FALSE, TRUE);
#endif

	} else {
#ifndef MLVIEW_WITH_LIBXML_EXPERIMENTAL_COMPLETION
		status =
		    mlview_xml_document_insert_next_sibling_node
		    (PRIVATE (a_this)->mlview_xml_doc,
		     ref_node_path, a_node, TRUE, TRUE);
#else

		status =
		    mlview_xml_document_insert_next_sibling_node
		    (PRIVATE (a_this)->mlview_xml_doc,
		     ref_node_path, a_node, FALSE, TRUE);
#endif

	}
	if (ref_node_path) {
		g_free (ref_node_path) ;
		ref_node_path = NULL ;
	}
	THROW_IF_FAIL (status == MLVIEW_OK);
	return MLVIEW_OK;
}

/**
 *Cuts the xml node associated to a given tree iterator.
 *Basically, this just cut the underlying xml node.
 *@param a_this the current instance of #MlViewTreeEditor.
 *@param a_iter an iterator to the node to cut.
 *@return MLVIEW_OK upon successful completion, an error code
 *otherwise.
 */
enum MlViewStatus
mlview_tree_editor_cut_node (MlViewTreeEditor * a_this,
                             GtkTreeIter * a_iter)
{
	enum MlViewStatus status = MLVIEW_OK ;
	xmlNode *node = NULL ;
	gchar *node_path = NULL ;

	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_TREE_EDITOR (a_this)
	                      && a_iter, MLVIEW_BAD_PARAM_ERROR);

	node = mlview_tree_editor_get_xml_node (a_this, a_iter);
	THROW_IF_FAIL (node);

	mlview_xml_document_get_node_path (PRIVATE (a_this)->mlview_xml_doc,
	                                   node,
	                                   &node_path) ;
	if (!node_path) {
		mlview_utils_trace_debug ("Could not get node path") ;
		return MLVIEW_ERROR ;
	}
	status = mlview_xml_document_cut_node
	         (PRIVATE (a_this)->mlview_xml_doc,
	          node_path, TRUE);
	if (node_path) {
		g_free (node_path) ;
		node_path = NULL ;
	}
	THROW_IF_FAIL (status == MLVIEW_OK);
	return MLVIEW_OK;
}

/**
 *Cuts the xml node associated to a given tree iterator.
 *Basically, this just cut the underlying xml node.
 *@param a_this the current instance of #MlViewTreeEditor.
 *@param a_pat a path to the node to cut.
 *@return MLVIEW_OK upon successful completion, an error code
 *otherwise.
 */
enum MlViewStatus
mlview_tree_editor_cut_node2 (MlViewTreeEditor * a_this,
                              GtkTreePath * a_path)
{
	GtkTreeIter iter = {0} ;
	GtkTreeModel *model = NULL ;
	gboolean is_ok = TRUE ;

	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_TREE_EDITOR (a_this)
	                      && PRIVATE (a_this),
	                      MLVIEW_BAD_PARAM_ERROR) ;
	model = mlview_tree_editor_get_model (a_this) ;
	THROW_IF_FAIL (model) ;
	is_ok = gtk_tree_model_get_iter (model, &iter, a_path) ;
	THROW_IF_FAIL (is_ok == TRUE) ;
	return mlview_tree_editor_cut_node (a_this, &iter) ;
}


/**
 *Cuts the xml node associated to a given tree iterator.
 *Basically, this just cut the underlying xml node.
 *@param a_this the current instance of #MlViewTreeEditor.
 *@param a_node the xml node to cut
 *@return MLVIEW_OK upon successful completion, an error code
 *otherwise.
 */
enum MlViewStatus
mlview_tree_editor_cut_node3 (MlViewTreeEditor * a_this,
                              xmlNode *a_node)
{
	xmlDoc *native_doc = NULL ;
	xmlNode *root_element_node = NULL ;
	enum MlViewStatus status = MLVIEW_OK ;
	GtkTreeIter iter = {0} ;
	GtkTreeModel *model = NULL ;

	THROW_IF_FAIL (a_this && MLVIEW_IS_TREE_EDITOR (a_this) && PRIVATE (a_this)) ;

	mlview::AppContext *context = mlview::AppContext::get_instance () ;
	THROW_IF_FAIL (context) ;

	native_doc = mlview_xml_document_get_native_document
	             (PRIVATE (a_this)->mlview_xml_doc) ;
	THROW_IF_FAIL (native_doc) ;
	mlview_xml_document_get_root_element (PRIVATE (a_this)->mlview_xml_doc,
	                                      &root_element_node) ;
	if (root_element_node == a_node) {
		/*
		 *You can't cut the root element node because
		 *it makes no sense.
		 */
		context->warning (_("You can not cut or "
							"suppress the root element node of the document.")) ;
		return MLVIEW_OK ;
	}
	if (a_node == (xmlNode*)native_doc) {
		context->warning (_("You can not cut or suppress "
						    "the XML Document Root node")) ;
		return MLVIEW_OK ;
	}

	model = mlview_tree_editor_get_model (a_this) ;
	THROW_IF_FAIL (model) ;
	status = mlview_tree_editor_get_iter (a_this, a_node, &iter) ;
	if (status != MLVIEW_OK) {
		LOG_TO_ERROR_STREAM ("mlview_tree_editor_get_iter() failed") ;
		return status ;
	}

	return mlview_tree_editor_cut_node (a_this, &iter) ;
}


/**
 *Cuts the current selected node
 *@param a_this the current instance of #MlViewTreeEditor
 *@return MLVIEW_OK upon successful completion, an error code otherwise.
 */
void
mlview_tree_editor_cut_cur_node (MlViewTreeEditor *a_this)
{
	xmlNode *node = NULL, *node_to_select = NULL ;
	enum MlViewStatus status = MLVIEW_OK ;

	THROW_IF_FAIL (a_this && MLVIEW_IS_TREE_EDITOR (a_this)) ;


	node = mlview_tree_editor_get_cur_sel_xml_node (a_this) ;
	if (!node) {
		mlview_utils_trace_debug ("current selected node is NULL !") ;
		return ;
	}
	if (node->prev)
		node_to_select = node->prev ;
	else
		node_to_select = node->parent ;

	status = mlview_tree_editor_cut_node3 (a_this, node) ;
	if (status != MLVIEW_OK)
		return ;
}


enum MlViewStatus
mlview_tree_editor_comment_current_node (MlViewTreeEditor *a_this)
{
	enum MlViewStatus status = MLVIEW_OK ;
	xmlNode *cur_node = NULL ;
	gchar *node_path = NULL ;

	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_TREE_EDITOR (a_this)
	                      && PRIVATE (a_this)
	                      && PRIVATE (a_this)->mlview_xml_doc,
	                      MLVIEW_BAD_PARAM_ERROR) ;

	cur_node = mlview_tree_editor_get_cur_sel_xml_node (a_this) ;
	if (!cur_node) {
		mlview_utils_trace_debug ("No cur node has been selected") ;
		return MLVIEW_ERROR ;
	}
	mlview_xml_document_get_node_path (PRIVATE (a_this)->mlview_xml_doc,
	                                   cur_node, &node_path) ;
	if (!node_path) {
		mlview_utils_trace_debug ("Could not get node path") ;
		return MLVIEW_ERROR ;
	}
	status = mlview_xml_document_comment_node (PRIVATE (a_this)->mlview_xml_doc,
	         node_path, TRUE) ;
	if (node_path) {
		g_free (node_path);
		node_path= NULL ;
	}
	return status ;
}

enum MlViewStatus
mlview_tree_editor_uncomment_current_node (MlViewTreeEditor *a_this)
{
	enum MlViewStatus status = MLVIEW_OK ;
	xmlNode *cur_node = NULL ;
	gchar *node_path = NULL ;

	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_TREE_EDITOR (a_this)
	                      && PRIVATE (a_this)
	                      && PRIVATE (a_this)->mlview_xml_doc,
	                      MLVIEW_BAD_PARAM_ERROR) ;

	cur_node = mlview_tree_editor_get_cur_sel_xml_node (a_this) ;
	if (!cur_node) {
		mlview_utils_trace_debug ("No cur node has been selected") ;
		return MLVIEW_ERROR ;
	}
	mlview_xml_document_get_node_path (PRIVATE (a_this)->mlview_xml_doc,
	                                   cur_node, &node_path) ;
	if (!node_path) {
		mlview_utils_trace_debug ("Could not get node path") ;
		return MLVIEW_ERROR ;
	}

	status = mlview_xml_document_uncomment_node
	         (PRIVATE (a_this)->mlview_xml_doc,
	          node_path, TRUE) ;

	if (node_path) {
		g_free (node_path) ;
		node_path = NULL ;
	}
	return status ;
}

/**
 *Copy the node pointed by an iterator to the clipboard.
 *@param a_this the current instance of #MlViewTreeEditor.
 *@param a_iter an iterator to the node to copy.
 *@return MLVIEW_OK upon successful completion, an error code
 *otherwise.
 */
enum MlViewStatus
mlview_tree_editor_copy_node (MlViewTreeEditor * a_this,
                              GtkTreeIter * a_iter)
{
	xmlDoc *native_doc = NULL ;
	xmlNode *xml_node = NULL;

	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_TREE_EDITOR (a_this)
	                      && PRIVATE (a_this),
	                      MLVIEW_BAD_PARAM_ERROR);

	native_doc = mlview_xml_document_get_native_document
	             (PRIVATE (a_this)->mlview_xml_doc) ;
	THROW_IF_FAIL (native_doc) ;

	xml_node = mlview_tree_editor_get_xml_node (a_this,
	           a_iter);
	THROW_IF_FAIL (xml_node);
	put_node_to_clipboard (xml_node) ;
	return MLVIEW_OK;
}

enum MlViewStatus
mlview_tree_editor_copy_current_node (MlViewTreeEditor *a_this)
{
	GtkTreeIter iter = {0} ;
	enum MlViewStatus status = MLVIEW_OK ;

	g_return_val_if_fail (a_this && MLVIEW_IS_TREE_EDITOR (a_this),
	                      MLVIEW_BAD_PARAM_ERROR) ;

	status = mlview_tree_editor_get_cur_sel_start_iter (a_this, &iter) ;

	if (status != MLVIEW_OK)
		return status ;

	status = mlview_tree_editor_copy_node (a_this, &iter) ;
	return status ;
}


/**
 *Copy the node pointed by a_path to the clipboard.
 *@param a_this the current instance of #MlViewTreeEditor.
 *@param a_path the path to the xml node to copy to the clipboard.
 *@return MLVIEW_OK upon successful completion, an error code otherwise.
 */
enum MlViewStatus
mlview_tree_editor_copy_node2 (MlViewTreeEditor *a_this,
                               GtkTreePath *a_path)
{
	GtkTreeModel *model = NULL ;
	GtkTreeIter iter = {0} ;
	gboolean is_ok = TRUE ;

	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_TREE_EDITOR (a_this)
	                      && a_path,
	                      MLVIEW_BAD_PARAM_ERROR) ;

	model = mlview_tree_editor_get_model (a_this) ;
	THROW_IF_FAIL (model) ;
	is_ok = gtk_tree_model_get_iter (model, &iter, a_path) ;
	THROW_IF_FAIL (is_ok == TRUE) ;
	return mlview_tree_editor_copy_node (a_this, &iter) ;
}


/**
 *Gets the last node put into the clipboard and inserts it into
 *the visual tree.
 *@param a_this the current instance of #MlViewTreeEditor.
 *@param a_parent_iter an iterator to the parent node.
 *@return MLVIEW_OK upon successful completion, an error code otherwise.
 */
enum MlViewStatus
mlview_tree_editor_paste_node_as_child (MlViewTreeEditor *a_this,
                                        GtkTreeIter *a_parent_iter)
{
	xmlNode *parent_node=NULL ;
	gchar *parent_node_path = NULL ;
	enum MlViewStatus status = MLVIEW_OK ;

	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_TREE_EDITOR (a_this)
	                      && PRIVATE (a_this)
	                      && PRIVATE (a_this)->mlview_xml_doc
	                      && a_parent_iter,
	                      MLVIEW_BAD_PARAM_ERROR) ;

	parent_node = mlview_tree_editor_get_xml_node (a_this,
	              a_parent_iter) ;
	THROW_IF_FAIL (parent_node) ;
	mlview_xml_document_get_node_path (PRIVATE (a_this)->mlview_xml_doc,
	                                   parent_node,
	                                   &parent_node_path) ;
	if (!parent_node_path) {
		mlview_utils_trace_debug ("Could not get node path") ;
		return MLVIEW_ERROR ;
	}
	xmlNode *node = get_node_from_clipboard (PRIVATE (a_this)->mlview_xml_doc) ;
	if (!node) {
		return MLVIEW_OK ;
	}
	status = mlview_tree_editor_add_child_node (a_this, a_parent_iter, node) ;
	if (parent_node_path) {
		g_free (parent_node_path) ;
		parent_node_path = NULL ;
	}
	return status ;
}

enum MlViewStatus
mlview_tree_editor_paste_node_as_child2 (MlViewTreeEditor *a_this)
{
	enum MlViewStatus status = MLVIEW_OK ;
	GtkTreeIter iter = {0} ;

	THROW_IF_FAIL (a_this && MLVIEW_IS_TREE_EDITOR (a_this)) ;

	status = mlview_tree_editor_get_cur_sel_start_iter (a_this, &iter) ;
	if (status != MLVIEW_OK)
		return status ;
	status = mlview_tree_editor_paste_node_as_child (a_this, &iter) ;
	return status ;
}


/**
 *Gets the last node put in the clipboard and pastes
 *it as a sibling of the node pointed to by a_ref_iter.
 *@param a_this the current instance of #MlViewTreeEditor
 *@param a_ref_iter an iterator that points to the
 *reference node.
 *@param a_previous if set to TRUE, the node must be pasted before
 *the reference node otherwise it is to be pasted after the reference 
 *node.
 *@return MLVIEW_OK upon sucessful completion, an error code otherwise.
 */
enum MlViewStatus
mlview_tree_editor_paste_node_as_sibling (MlViewTreeEditor *a_this,
										  GtkTreeIter *a_ref_iter,
										  gboolean a_previous)
{
	xmlNode *sibling_node=NULL ;
	gchar *sibling_node_path=NULL, *parent_node_path = NULL ;

	THROW_IF_FAIL (a_this && MLVIEW_IS_TREE_EDITOR (a_this)
				   && PRIVATE (a_this) && PRIVATE (a_this)->mlview_xml_doc
				   && a_ref_iter) ;

	sibling_node = mlview_tree_editor_get_xml_node (a_this, a_ref_iter) ;
	THROW_IF_FAIL (sibling_node && sibling_node->parent) ;
	mlview_xml_document_get_node_path (PRIVATE (a_this)->mlview_xml_doc,
	                                   sibling_node,
	                                   &sibling_node_path) ;
	if (!sibling_node_path) {
		LOG_TO_ERROR_STREAM ("Could not get node path") ;
		return MLVIEW_ERROR ;

	}
	mlview_xml_document_get_node_path (PRIVATE (a_this)->mlview_xml_doc,
	                                   sibling_node->parent,
	                                   &parent_node_path) ;
	if (!parent_node_path) {
		mlview_utils_trace_debug ("Could not get node path") ;
		return MLVIEW_ERROR ;

	}
	xmlNode* node = get_node_from_clipboard (PRIVATE (a_this)->mlview_xml_doc) ;
	if (node) {
		if (a_previous)
			mlview_xml_document_insert_prev_sibling_node
				(PRIVATE (a_this)->mlview_xml_doc, sibling_node_path, node,
				 FALSE, TRUE) ;
		else
			mlview_xml_document_insert_next_sibling_node
				(PRIVATE (a_this)->mlview_xml_doc, sibling_node_path, node,
				 FALSE, TRUE) ;
	}
	if (sibling_node_path) {
		g_free (sibling_node_path) ;
		sibling_node_path = NULL ;
	}
	if (parent_node_path) {
		g_free (parent_node_path) ;
		parent_node_path = NULL ;
	}
	return MLVIEW_OK ;
}

/**
 *Gets the last node put in the clipboard and pastes
 *it as a sibling of the node pointed to by a_ref_iter.
 *@param a_this the current instance of #MlViewTreeEditor
 *@param a_ref_path the path that points to the reference node.
 *@param a_previous if set to TRUE, the node must be pasted before
 *the reference node otherwise it is to be pasted after the reference 
 *node.
 *@return MLVIEW_OK upon sucessful completion, an error code otherwise.
 */
enum MlViewStatus
mlview_tree_editor_paste_node_as_sibling2 (MlViewTreeEditor *a_this,
										   GtkTreePath *a_ref_path,
										   gboolean a_previous)
{
	GtkTreeModel *model = NULL ;
	GtkTreeIter iter = {0} ;
	gboolean is_ok = TRUE ;

	THROW_IF_FAIL (a_this && MLVIEW_IS_TREE_EDITOR (a_this) && PRIVATE (a_this)
				   && a_ref_path) ;

	model = mlview_tree_editor_get_model (a_this) ;
	THROW_IF_FAIL (model) ;
	is_ok = gtk_tree_model_get_iter (model, &iter, a_ref_path) ;
	THROW_IF_FAIL (is_ok == TRUE) ;
	return mlview_tree_editor_paste_node_as_sibling
	       (a_this, &iter, a_previous) ;
}

enum MlViewStatus
mlview_tree_editor_paste_node_as_next_sibling (MlViewTreeEditor *a_this)
{
	GtkTreeIter iter = {0} ;
	enum MlViewStatus status = MLVIEW_OK ;

	g_return_val_if_fail (a_this && MLVIEW_TREE_EDITOR (a_this),
	                      MLVIEW_BAD_PARAM_ERROR) ;

	status = mlview_tree_editor_get_cur_sel_start_iter (a_this, &iter) ;
	if (status != MLVIEW_OK)
		return status ;
	status = mlview_tree_editor_paste_node_as_sibling (a_this, &iter, FALSE) ;

	return status ;
}


enum MlViewStatus
mlview_tree_editor_paste_node_as_prev_sibling (MlViewTreeEditor *a_this)
{
	GtkTreeIter iter = {0} ;
	enum MlViewStatus status = MLVIEW_OK ;

	g_return_val_if_fail (a_this && MLVIEW_TREE_EDITOR (a_this),
	                      MLVIEW_BAD_PARAM_ERROR) ;

	status = mlview_tree_editor_get_cur_sel_start_iter (a_this, &iter) ;

	if (status != MLVIEW_OK)
		return status ;
	status = mlview_tree_editor_paste_node_as_sibling (a_this, &iter, TRUE) ;

	return status ;
}


/**
 *Visualy Updates the tree editor
 *to make the addition of child node visible.
 *If the node addition has been already "updated",
 *this method does nothing.
 *@param a_this the current instance of #MlViewEditor.
 *@param a_parent the parent node of the newly added
 *instance of xmlNode.
 *@param a_node the newly added instance of xmlNode.
 *@return MLVIEW_OK upon successful completion, an error
 *code otherwise.
 */
enum MlViewStatus
mlview_tree_editor_update_child_node_added (MlViewTreeEditor * a_this,
        xmlNode * a_parent,
        xmlNode * a_node,
        gboolean a_emit_signals)
{
	GtkTreeView *tree_view = NULL ;
	GtkTreeRowReference *parent_row_ref = NULL,
	                                      *row_ref = NULL;
	GtkTreeIter iter = { 0 };
	GtkTreeModel *model = NULL;
	GtkTreePath *tree_path = NULL;
	gboolean is_ok = FALSE;
	enum MlViewStatus status = MLVIEW_OK;

	g_return_val_if_fail
	        (a_this && MLVIEW_IS_TREE_EDITOR (a_this)
	         && PRIVATE (a_this), MLVIEW_BAD_PARAM_ERROR);
	/*
	 *make sure the a_node hasn't been added to
	 *the visual tree yet. row_ref must be NULL.
	 */
	row_ref = (GtkTreeRowReference*)g_hash_table_lookup
	          (PRIVATE (a_this)->nodes_rows_hash, a_node);
	if (row_ref) {
		mlview_tree_editor_select_node (a_this, a_node,
		                                TRUE, TRUE) ;
		return MLVIEW_OK ;
	}
	/*
	 *only some node types can have
	 *children nodes.
	 */
	g_return_val_if_fail
	        (a_parent->type == XML_ELEMENT_NODE
	         || (a_parent->type == XML_DTD_NODE
	             && a_node->type == XML_ENTITY_DECL),
	         MLVIEW_BAD_PARAM_ERROR);
	tree_view = mlview_tree_editor_get_tree_view (a_this);
	THROW_IF_FAIL (tree_view != NULL);
	model = gtk_tree_view_get_model (tree_view);
	THROW_IF_FAIL (model);
	/*
	 *get the visual node that matches the a_parent_node
	 *It must have been drawn already.
	 */
	parent_row_ref = (GtkTreeRowReference*) g_hash_table_lookup
	                 (PRIVATE (a_this)->nodes_rows_hash, a_parent);
	g_return_val_if_fail (parent_row_ref,
	                      MLVIEW_NODE_NOT_FOUND_ERROR);
	/*get an iterator on the parent node */
	tree_path = gtk_tree_row_reference_get_path
	            (parent_row_ref);
	THROW_IF_FAIL (tree_path);
	is_ok = gtk_tree_model_get_iter (model, &iter,
	                                 tree_path);
	if (is_ok != TRUE) {
		mlview_utils_trace_debug ("is_ok == TRUE failed") ;
		status = MLVIEW_OK ;
		goto cleanup ;
	}
	/*
	 *build a GtkTreeModel subtree that matches the a_node
	 *subtree.
	 */
	status = mlview_tree_editor_build_tree_model_from_xml_tree
	         (a_this, a_node,
	          &iter, INSERT_TYPE_APPEND_CHILD_VISIT_NEXT, &model);
	/*
	 *Build the visual representation of the newly added
	 *row.
	 */
	status = mlview_tree_editor_update_visual_node
	         (a_this, &iter, FALSE);
	/*
	 *expand the current selected node to a depth 
	*of one level and select the newly added node.
	 */
	mlview_utils_gtk_tree_view_expand_row_to_depth
	(tree_view, tree_path, 1) ;

	mlview_tree_editor_select_node (a_this, a_node, FALSE,
	                                TRUE) ;

	if (status != MLVIEW_OK) {
		mlview_utils_trace_debug
		("status ==  MVIEW_OK failed") ;
		goto cleanup ;
	}

	/*emit the appropriate signals */
	if (a_emit_signals == TRUE) {
		row_ref = (GtkTreeRowReference*)g_hash_table_lookup
		          (PRIVATE (a_this)->nodes_rows_hash,
		           a_node);
		if (!row_ref) {
			mlview_utils_trace_debug ("row_ref failed") ;
			status = MLVIEW_ERROR ;
			goto cleanup ;
		}
		g_signal_emit (G_OBJECT (a_this),
		               gv_signals[NODE_ADDED], 0,
		               row_ref);
		g_signal_emit (G_OBJECT (a_this),
		               gv_signals[TREE_CHANGED], 0);
	}
cleanup:
	if (tree_path) {
		gtk_tree_path_free (tree_path) ;
		tree_path = NULL ;
	}
	return status;
}

/**
 *Updates the graphical tree to reflect a
 *"node pasted" event.
 *@param a_this the current instance of #MlViewTreeEditor
 *@param a_parent_node the parent node of the pasted node.
 *@param a_node the pasted node.
 *@return MLVIEW_OK upon successful completion, an error code
 *otherwise.
 */
enum MlViewStatus
mlview_tree_editor_update_node_pasted (MlViewTreeEditor *a_this,
                                       xmlNode * a_parent_node,
                                       xmlNode * a_node,
                                       gboolean a_emit_signals)
{
	GtkTreeRowReference *row_ref = NULL;
	enum MlViewStatus status = MLVIEW_OK;

	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_TREE_EDITOR (a_this)
	                      && PRIVATE (a_this)
	                      && a_parent_node
	                      && a_node, MLVIEW_BAD_PARAM_ERROR);
	/*make sure a_parent_node is drawn */
	row_ref = (GtkTreeRowReference*)g_hash_table_lookup
	          (PRIVATE (a_this)->nodes_rows_hash,
	           a_parent_node);
	THROW_IF_FAIL (row_ref);
	/*make sure a_node is not drawn */
	row_ref = (GtkTreeRowReference*)g_hash_table_lookup
	          (PRIVATE (a_this)->nodes_rows_hash, a_node);
	if (row_ref) {
		mlview_tree_editor_select_node (a_this, a_node,
		                                TRUE, TRUE) ;
		return MLVIEW_OK ;
	}
	g_return_val_if_fail (row_ref == NULL,
	                      MLVIEW_BAD_PARAM_ERROR);
	status = mlview_tree_editor_update_child_node_added
	         (a_this, a_parent_node, a_node, FALSE);
	THROW_IF_FAIL (status == MLVIEW_OK);
	if (a_emit_signals == TRUE) {
		row_ref = (GtkTreeRowReference*)g_hash_table_lookup
		          (PRIVATE (a_this)->nodes_rows_hash,
		           a_node);
		THROW_IF_FAIL (row_ref);
		g_signal_emit (G_OBJECT (a_this),
		               gv_signals[NODE_PASTED], 0,
		               row_ref);
	}
	return MLVIEW_OK;
}

enum MlViewStatus
mlview_tree_editor_update_node_commented (MlViewTreeEditor *a_this,
        xmlNode *a_old_node,
        xmlNode *a_new_node)
{
	enum MlViewStatus status = MLVIEW_OK ;
	GtkTreeIter iter = {0} ;
	GtkTreeRowReference *row_ref = NULL ;
	GtkTreeModel *model = NULL ;

	g_return_val_if_fail (a_this
	                      && MLVIEW_TREE_EDITOR (a_this)
	                      && PRIVATE (a_this),
	                      MLVIEW_BAD_PARAM_ERROR) ;

	status = mlview_tree_editor_get_iter (a_this,
	                                      a_old_node,
	                                      &iter) ;
	if (status != MLVIEW_OK) {
		mlview_utils_trace_debug ("mlview_tree_editor_get_iter() failed") ;
		return status ;
	}
	row_ref = (GtkTreeRowReference*)g_hash_table_lookup
	          (PRIVATE (a_this)->nodes_rows_hash, a_old_node);
	if (!row_ref) {
		mlview_utils_trace_debug ("could not get row reference from old node") ;
		return MLVIEW_ERROR ;
	}
	model = mlview_tree_editor_get_model (a_this) ;
	if (!model) {
		mlview_utils_trace_debug ("could not get the model") ;
		return MLVIEW_ERROR ;
	}
	g_hash_table_insert (PRIVATE (a_this)->nodes_rows_hash,
	                     a_new_node, row_ref) ;
	g_hash_table_remove (PRIVATE (a_this)->nodes_rows_hash, a_old_node) ;

	gtk_tree_store_set (GTK_TREE_STORE (model),
	                    &iter,
	                    XML_NODE_COLUMN, a_new_node,
	                    -1) ;
	mlview_tree_editor_update_visual_node
	(a_this, &iter, FALSE);

	return MLVIEW_OK ;
}

enum MlViewStatus
mlview_tree_editor_update_internal_subset_added (MlViewTreeEditor *a_this,
        xmlDtd *a_subset_node)
{
	xmlDoc *native_doc = NULL ;
	GtkTreeRowReference *row_ref = NULL;
	GtkTreeModel *model = NULL;
	GtkTreeIter iter = { 0 };
	enum MlViewStatus status = MLVIEW_OK;

	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_TREE_EDITOR (a_this)
	                      && PRIVATE (a_this)
	                      && PRIVATE (a_this)->mlview_xml_doc
	                      && a_subset_node,
	                      MLVIEW_BAD_PARAM_ERROR) ;

	native_doc = mlview_xml_document_get_native_document
	             (PRIVATE (a_this)->mlview_xml_doc) ;
	THROW_IF_FAIL (native_doc) ;
	g_return_val_if_fail
	        (a_subset_node->parent == native_doc,
	         MLVIEW_BAD_PARAM_ERROR) ;

	/*make sure a_subset_node hasn't been drawn already */
	row_ref = (GtkTreeRowReference*)g_hash_table_lookup
	          (PRIVATE (a_this)->nodes_rows_hash,
	           a_subset_node);
	if (row_ref) {
		mlview_tree_editor_select_node
		(a_this,
		 (xmlNode*)a_subset_node,
		 TRUE, TRUE) ;
		return MLVIEW_OK ;
	}
	model = mlview_tree_editor_get_model (a_this) ;
	THROW_IF_FAIL (model) ;

	status = mlview_tree_editor_get_iter
	         (a_this, (xmlNode*)native_doc,
	          &iter) ;
	THROW_IF_FAIL (status == MLVIEW_OK) ;

	status = mlview_tree_editor_build_tree_model_from_xml_tree
	         (a_this, (xmlNode*)a_subset_node,
	          &iter, INSERT_TYPE_PREPEND_CHILD, &model);
	THROW_IF_FAIL (status == MLVIEW_OK) ;

	mlview_tree_editor_select_node (a_this,
	                                (xmlNode*)a_subset_node,
	                                FALSE, TRUE) ;
	return MLVIEW_OK ;
}

/**
 *updates the visual tree node to reflect the
 *"sibling node inserted event."
 *@param a_this the current instance of #MlViewTreeEditor
 *@param a_ref_node the reference node.
 *@param a_inserted the node that has been inserted after
 *or before ref_node.
 *@param a_previous if set to TRUE, a_inserted_node
 *is to be inserted before a_ref_node, otherwise a_inserted_node
 *is to be inserted after a_ref_node.
 *@return MLVIEW_OK upon sucessfull completion, an error
 *code otherwise.
 */
enum MlViewStatus
mlview_tree_editor_update_sibling_node_inserted (MlViewTreeEditor * a_this,
        xmlNode * a_ref_node,
        xmlNode * a_inserted_node,
        gboolean a_previous,
        gboolean a_emit_signals)
{
	GtkTreeRowReference *row_ref = NULL;
	GtkTreeModel *model = NULL;
	GtkTreeIter iter = { 0 };
	GtkTreeView *tree_view = NULL ;
	enum MlViewStatus status = MLVIEW_OK;

	THROW_IF_FAIL (a_this && MLVIEW_IS_TREE_EDITOR (a_this) && PRIVATE (a_this)
				   && PRIVATE (a_this)->nodes_rows_hash && a_ref_node
				   && a_inserted_node);

	/*make sure ref_node has been drawn already */
	row_ref = (GtkTreeRowReference*)g_hash_table_lookup
	          (PRIVATE (a_this)->nodes_rows_hash, a_ref_node);
	THROW_IF_FAIL (row_ref);
	/*make sure a_inserted_node hasn't been drawn already */
	row_ref = (GtkTreeRowReference*)g_hash_table_lookup
	          (PRIVATE (a_this)->nodes_rows_hash,
	           a_inserted_node);
	if (row_ref) {
		mlview_tree_editor_select_node (a_this, a_inserted_node,
		                                TRUE, TRUE) ;
		return MLVIEW_OK ;
	}
	status = mlview_tree_editor_get_iter
	         (a_this, a_ref_node, &iter);
	THROW_IF_FAIL (status == MLVIEW_OK);
	model = mlview_tree_editor_get_model (a_this);
	THROW_IF_FAIL (model);
	if (a_previous == TRUE) {
		status = mlview_tree_editor_build_tree_model_from_xml_tree
		         (a_this, a_inserted_node,
		          &iter, INSERT_TYPE_INSERT_BEFORE,
		          &model);
	} else {
		status = mlview_tree_editor_build_tree_model_from_xml_tree
		         (a_this, a_inserted_node,
		          &iter, INSERT_TYPE_INSERT_AFTER,
		          &model);
	}
	THROW_IF_FAIL (status == MLVIEW_OK);
	/*
	 *expand the current node and select a_inserted_node
	 */
	tree_view = mlview_tree_editor_get_tree_view (a_this) ;
	mlview_tree_editor_get_iter (a_this, a_inserted_node,
	                             &iter) ;

	mlview::PrefsCategoryTreeview *m_prefs =
	    dynamic_cast<mlview::PrefsCategoryTreeview*> (
		mlview::Preferences::get_instance ()
		->get_category_by_id ("treeview"));

	mlview_utils_gtk_tree_view_expand_row_to_depth2
	    (tree_view, &iter,
	     m_prefs->get_default_tree_expansion_depth ());

	mlview_tree_editor_select_node (a_this, a_inserted_node,
	                                FALSE, TRUE) ;
	status = mlview_tree_editor_update_visual_node
	         (a_this, &iter, FALSE);
	if (status == MLVIEW_OK && a_emit_signals == TRUE) {
		g_signal_emit (G_OBJECT (a_this),
		               gv_signals[NODE_ADDED], 0,
		               row_ref);
	}
	return status;
}

/**
 *Updates the visual tree so that it reflects
 *a node cut event.
 *@param a_this the current instance of #MlViewTreeEditor.
 *@param a_parent_node the parent node of the xml node that
 *has been cut.
 *@param a_node_cut the xml node that has been cut.
 *@return MLVIEW_OK upon successful completion, an error
 *code otherwise.
 */
enum MlViewStatus
mlview_tree_editor_update_node_cut (MlViewTreeEditor * a_this,
                                    xmlNode * a_parent_node,
                                    xmlNode * a_node_cut)
{
	GtkTreeIter iter = { 0 };
	GtkTreeModel *model = NULL;
	GtkTreeRowReference *row_ref=NULL ;
	gboolean is_ok = TRUE;
	GtkTreeView *tree_view = NULL ;
	xmlNode *node_to_select = NULL ;
	enum MlViewStatus status = MLVIEW_OK;
	GtkTreePath *tree_path = NULL ;

	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_TREE_EDITOR (a_this)
	                      && PRIVATE (a_this)
	                      && a_node_cut
	                      && a_parent_node,
	                      MLVIEW_BAD_PARAM_ERROR);
	g_return_val_if_fail (a_node_cut->parent == NULL
	                      && a_parent_node,
	                      MLVIEW_BAD_PARAM_ERROR);

	THROW_IF_FAIL (a_node_cut);

	model = mlview_tree_editor_get_model (a_this);
	THROW_IF_FAIL (model);

	/*
	 *make sure the a_parent_node and a_cut_node are still
	 *referenced.
	 */
	status = mlview_tree_editor_get_iter
	         (a_this, a_parent_node, &iter);
	if (status != MLVIEW_OK)
		return status;
	row_ref = (GtkTreeRowReference*)g_hash_table_lookup
	          (PRIVATE (a_this)->nodes_rows_hash, a_node_cut);
	THROW_IF_FAIL (row_ref) ;
	status = mlview_tree_editor_get_iter
	         (a_this, a_node_cut, &iter);
	if (status != MLVIEW_OK)
		return status;

	/*
	 *Find the node to select after the cut.
	 */
	tree_path = gtk_tree_model_get_path (model, &iter) ;
	if (!gtk_tree_path_prev (tree_path))
		gtk_tree_path_up (tree_path) ;
	node_to_select = mlview_tree_editor_get_xml_node3 (a_this, tree_path) ;
	if (!node_to_select)
		node_to_select = a_parent_node ;
	if (tree_path) {
		gtk_tree_path_free (tree_path) ;
		tree_path = NULL ;
	}

	/*
	 *Visually cut the node now.
	 */
	tree_view = mlview_tree_editor_get_tree_view (a_this) ;
	g_hash_table_remove
	(PRIVATE (a_this)->nodes_rows_hash, a_node_cut);
	gtk_tree_row_reference_free (row_ref) ;
	row_ref = NULL ;
	is_ok = gtk_tree_store_remove
	        (GTK_TREE_STORE (model), &iter);

	/*
	 *Select the node to be selected after the cut.
	 */
	mlview_tree_editor_select_node (a_this,
	                                node_to_select,
	                                TRUE, FALSE) ;

	g_signal_emit (G_OBJECT (a_this),
	               gv_signals[NODE_CUT], 0, a_node_cut);
	g_signal_emit (G_OBJECT (a_this),
	               gv_signals[TREE_CHANGED], 0);
	return MLVIEW_OK;
}

/**
 *Update the tag string of a given visual node.
 *@param a_this the current instance of #MlViewTreeEditor.
 *@param a_iter an iterator that points to the node to be updated.
 *@param selected whether the node is selected
 *@return MLVIEW_OK upon successful completion, an error code
 *otherwise.
 */
static enum MlViewStatus
update_visual_node (MlViewTreeEditor *a_this, GtkTreeIter *a_iter,
					gboolean a_selected)
{
    gchar *start_tag_str = NULL;
    GtkTreeModel *model = NULL;
    xmlNode *xml_node = NULL;

    g_return_val_if_fail (a_this
			  && MLVIEW_IS_TREE_EDITOR (a_this)
			  && a_iter, MLVIEW_BAD_PARAM_ERROR);

    model = mlview_tree_editor_get_model (a_this);
    THROW_IF_FAIL (model);
    gtk_tree_model_get (model, a_iter, XML_NODE_COLUMN,
			&xml_node, -1);
    start_tag_str = node_to_string_tag (a_this, xml_node,
					a_selected) ;
    if (!start_tag_str) {
	return MLVIEW_OK;
    }
    gtk_tree_store_set (GTK_TREE_STORE (model),
			a_iter, START_TAG_COLUMN,
			start_tag_str, -1);
    if (start_tag_str) {
	g_free (start_tag_str) ;
	start_tag_str = NULL ;
    }

    return MLVIEW_OK;
}

/**
 *Updates the representation of a visual node denoted
 *by an instance of GtkTreeIter (a visual node iterator).
 *@param a_this the current instance of #MlViewTreeEditor
 *@param a_iter the iterator to consider.
 *@return MLVIEW_OK upon successful completion, an error code
 *otherwise.
 */
enum MlViewStatus
mlview_tree_editor_update_visual_node (MlViewTreeEditor *a_this,
				       GtkTreeIter * a_iter,
				       gboolean a_selected)
{
    return MLVIEW_TREE_EDITOR_CLASS
	(G_OBJECT_GET_CLASS
	 (a_this))->update_visual_node (a_this, a_iter,
					a_selected);
}

/**
 *Update the tag string of a given visual node.
 *@param a_this the current instance of #MlViewTreeEditor.
 *@param a_iter an iterator that points to the node to be updated.
 *@return MLVIEW_OK upon successful completion, an error code
 *otherwise.
 */
enum MlViewStatus
mlview_tree_editor_update_visual_node2 (MlViewTreeEditor *a_this,
					xmlNode * a_node,
					gboolean a_selected)
{
    GtkTreeRowReference *row_ref = NULL;
    GtkTreePath *tree_path = NULL;
    GtkTreeIter iter = {0};
    GtkTreeModel *model = NULL;
    gboolean is_ok = FALSE;
    enum MlViewStatus status = MLVIEW_ERROR;

    g_return_val_if_fail (a_this
			  && MLVIEW_IS_TREE_EDITOR (a_this)
			  && PRIVATE (a_this)
			  && a_node, MLVIEW_BAD_PARAM_ERROR);

    row_ref = (GtkTreeRowReference*)g_hash_table_lookup
	(PRIVATE (a_this)->nodes_rows_hash, a_node);
    if (!row_ref) {
	return MLVIEW_NODE_NOT_FOUND_ERROR;
    }
    tree_path = gtk_tree_row_reference_get_path (row_ref);
    THROW_IF_FAIL (tree_path);
    model = mlview_tree_editor_get_model (a_this);
    if (!model) {
	mlview_utils_trace_debug ("model failed") ;
	status = MLVIEW_ERROR ;
	goto cleanup ;
    }
    is_ok = gtk_tree_model_get_iter (model, &iter,
				     tree_path);
    if (is_ok != TRUE) {
	mlview_utils_trace_debug ("is_ok == TRUE failed") ;
	status = MLVIEW_ERROR ;
	goto cleanup ;
    }
    status = mlview_tree_editor_update_visual_node
	(a_this, &iter, a_selected);
 cleanup:
    if (tree_path) {
	gtk_tree_path_free (tree_path) ;
	tree_path = NULL ;
    }
    return status;
}

/**
 *Displays a dialog box to let the user
 *type in search info and triggers the
 *search.
 *@param a_this the current instance of #MlViewTreeEditor
 *@return MLVIEW_OK upon successful completion, an error code
 *otherwise.
 */
enum MlViewStatus
mlview_tree_editor_search_interactive (MlViewTreeEditor *a_this)
{
	GtkWidget *find_dialog = NULL ;

	THROW_IF_FAIL (a_this) ;

	find_dialog = get_search_dialog (a_this) ;
	g_return_val_if_fail (find_dialog,
	                      MLVIEW_ERROR) ;

	gtk_widget_show (find_dialog) ;
	return MLVIEW_OK ;
}

/**
 *Calls the search backend on the current instance
 *of #MlViewXMLDocument.
 *@param a_this the current instance of #MlViewTreeEditor
 *@param a_from the row to start the search from.
 *@param a_config the search configuration .
 *@return MLVIEW_OK upon successful completion, an error code
 *otherwise.
 */
enum MlViewStatus
mlview_tree_editor_search (MlViewTreeEditor *a_this,
                           GtkTreeRowReference *a_from,
                           struct SearchConfig *a_config,
                           xmlNode **a_node_found)
{
	xmlNode *xml_node = NULL ;
	enum MlViewStatus status = MLVIEW_OK ;

	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_TREE_EDITOR (a_this)
	                      && PRIVATE (a_this)
	                      && PRIVATE (a_this)->mlview_xml_doc,
	                      MLVIEW_OK) ;

	if (PRIVATE (a_this)->cur_sel_start)
	{
		xml_node = mlview_tree_editor_get_xml_node2
		           (a_this, a_from) ;
		THROW_IF_FAIL (xml_node) ;
	}
	status = mlview_xml_document_search
	         (PRIVATE (a_this)->mlview_xml_doc, a_config,
	          xml_node, a_node_found, TRUE) ;
	return status ;
}


/**
 *Selects the visual node that matches the xml node.
 *@param a_this the current instance of #MlViewTreeEditor.
 *@param a_node the node to select.
 */
void
mlview_tree_editor_select_node (MlViewTreeEditor *a_this,
                                xmlNode *a_node,
                                gboolean a_issued_by_model,
                                gboolean a_signal_model)
{
	enum MlViewStatus status = MLVIEW_OK ;
	GtkTreePath *tree_path = NULL, *parent_path = NULL ;
	GtkTreeIter iter = {0} ;
	GtkTreeModel * model = NULL ;
	GtkTreeView *tree_view = NULL ;
	GtkTreeSelection *tree_sel = NULL ;

	THROW_IF_FAIL (a_this && MLVIEW_IS_TREE_EDITOR (a_this)
	                  && PRIVATE (a_this)) ;

	status = mlview_tree_editor_get_iter (a_this, a_node,
	                                      &iter) ;
	if (status != MLVIEW_OK) {
		return ;
	}
	model = mlview_tree_editor_get_model (a_this) ;
	if (!model) {
		mlview_utils_trace_debug ("model failed") ;
		goto cleanup ;
	}
	tree_view = mlview_tree_editor_get_tree_view (a_this) ;
	if (!tree_view) {
		mlview_utils_trace_debug ("tree_view failed") ;
		goto cleanup ;
	}
	/*expand the tree up to the current node level*/
	tree_path = gtk_tree_model_get_path (model, &iter) ;
	parent_path = gtk_tree_path_copy (tree_path);
	if (!tree_path) {
		mlview_utils_trace_debug ("tree_path failed") ;
		goto cleanup ;
	}
	gtk_tree_path_up (parent_path);
	tree_sel = gtk_tree_view_get_selection (tree_view) ;
	if (!tree_sel) {
		mlview_utils_trace_debug ("tree_sel failed") ;
		goto cleanup ;
	}
	gtk_tree_view_expand_to_path
	(tree_view, parent_path) ;

	/*now, select the current node*/
	if (a_signal_model == TRUE
	        && a_issued_by_model == FALSE) {
		/*
		 *The signal has been generated
		 *by the current view ->
		 *let everyone know about that
		 */
		mlview_xml_document_select_node
		(PRIVATE (a_this)->mlview_xml_doc, a_node) ;
	} else if (a_issued_by_model == TRUE) {
		/*
		 *the signal has been generated by
		 *another view and we get notified
		 *via the model.
		 */
		if (PRIVATE
		        (a_this)->select_issued_by_model == TRUE) {
			/*we are being called twice in
			 *the same selection process.
			 *stop the process here.
			 */
			PRIVATE (a_this)->select_issued_by_model
			= FALSE ;
		} else {
			/*
			 *first time we are being called
			 *as the result of the model issuing
			 *the "node-selected" signal.
			 */
			PRIVATE (a_this)->select_issued_by_model
			= TRUE ;
			gtk_tree_view_set_cursor (tree_view, tree_path,
			                          NULL, FALSE) ;
			PRIVATE (a_this)->select_issued_by_model = FALSE ;
			mlview_tree_editor_scroll_to_cell (a_this, tree_path) ;
		}
	}

cleanup:
	if (tree_path) {
		gtk_tree_path_free (tree_path) ;
		tree_path = NULL ;
	}
	if (parent_path) {
		gtk_tree_path_free (parent_path) ;
		parent_path = NULL ;
	}
}

/**
 *Selects a visual node
 *@param a_this the current instance of #MlViewTreeEditor.
 *@param a_tree_path a path to the node to select.
 *
 */
void
mlview_tree_editor_select_node2 (MlViewTreeEditor *a_this,
                                 GtkTreePath *a_tree_path,
                                 gboolean a_issued_by_model,
                                 gboolean a_emit_signal)
{
	xmlNode *xml_node = NULL ;
	THROW_IF_FAIL (a_this
	                  && MLVIEW_IS_TREE_EDITOR (a_this)
	                  && PRIVATE (a_this)
	                  && a_tree_path) ;

	xml_node = mlview_tree_editor_get_xml_node3 (a_this, a_tree_path) ;
	if (!xml_node) {
		mlview_utils_trace_debug
		("No xml node associated to the current visual tree node") ;
		return ;
	}
	mlview_tree_editor_select_node (a_this, xml_node,
	                                a_issued_by_model,
	                                a_emit_signal) ;
}

/**
 *Start editing a row of the tree view
 *@param a_this the current instance of #MlViewTreeEditor
 *@param a_node the xml node that matches the row to edit
 */
void
mlview_tree_editor_start_editing_node (MlViewTreeEditor *a_this,
                                       xmlNode *a_node)
{
	enum MlViewStatus status = MLVIEW_OK ;
	GtkTreeIter iter = {0} ;
	GtkTreeModel *model = NULL ;
	GtkTreePath *path = NULL ;
	GtkTreeView *tree_view = NULL ;
	GtkTreeViewColumn *column = NULL ;

	status = mlview_tree_editor_get_iter (a_this, a_node, &iter) ;
	if (status != MLVIEW_OK) {
		mlview_utils_trace_debug ("mlview_tree_editor_get_iter() failed") ;
		return ;
	}
	tree_view = mlview_tree_editor_get_tree_view (a_this) ;
	if (!tree_view) {
		mlview_utils_trace_debug ("mlview_tree_editor_get_tree_view() failed") ;
		goto cleanup ;
	}
	column = gtk_tree_view_get_column (tree_view, XML_NODE_COLUMN) ;
	if (!column) {
		mlview_utils_trace_debug ("gtk_tree_view_get_column() failed") ;
		goto cleanup ;
	}
	model = mlview_tree_editor_get_model (a_this) ;
	path = gtk_tree_model_get_path (model, &iter) ;
	gtk_tree_view_set_cursor (tree_view, path, column, TRUE) ;

cleanup:
	if (path) {
		gtk_tree_path_free (path) ;
		path = NULL ;
	}
}

/**
 *Expands the current selected visual row to the depth
 *a_depth. If a_depth is set to -1 the tree is expanded to the
 *leaves.
 *@param a_this the current instance of #MlViewTreeEditor.
 *@param a_depth the expansion depth.
 */
void
mlview_tree_editor_expand_tree_to_depth (MlViewTreeEditor * a_this,
        gint a_depth)
{
	GtkTreeRowReference *cur_row_ref=NULL ;
	GtkTreePath *cur_path=NULL ;
	GtkTreeView *tree_view=NULL ;
	enum MlViewStatus status = MLVIEW_OK ;

	THROW_IF_FAIL (a_this
	                  && MLVIEW_IS_TREE_EDITOR (a_this)) ;
	cur_row_ref = PRIVATE (a_this)->cur_sel_start ;
	THROW_IF_FAIL (cur_row_ref) ;
	cur_path = gtk_tree_row_reference_get_path (cur_row_ref) ;
	THROW_IF_FAIL (cur_path) ;
	tree_view = mlview_tree_editor_get_tree_view (a_this) ;
	if (!tree_view) {
		mlview_utils_trace_debug ("tree_view failed") ;
		goto cleanup ;
	}
	status = mlview_utils_gtk_tree_view_expand_row_to_depth
	         (tree_view, cur_path, a_depth) ;
	if (status != MLVIEW_OK) {
		mlview_utils_trace_debug
		("status == MLVIEW_OK failed.") ;
	}

cleanup:
	if (cur_path) {
		gtk_tree_path_free (cur_path) ;
		cur_path = NULL ;
	}
}


/**
 *Folds the current node if it's unfolded, unfolds it otherwise.
 *@param a_this the current instance of #MlViewTreeEditor
 */
void
mlview_tree_editor_toggle_node_folding (MlViewTreeEditor *a_this)
{
	enum MlViewStatus status = MLVIEW_OK ;
	GtkTreePath *tree_path = NULL ;
	GtkTreeView *tree_view = NULL ;

	THROW_IF_FAIL (a_this && MLVIEW_IS_TREE_EDITOR (a_this)
	                  && PRIVATE (a_this)) ;

	status = mlview_tree_editor_get_cur_sel_start_tree_path (a_this,
	         &tree_path) ;
	if (status != MLVIEW_OK || !tree_path)
		return ;


	tree_view = mlview_tree_editor_get_tree_view (a_this) ;
	if (!tree_view) {
		mlview_utils_trace_debug ("mlview_tree_editor_get_tree_view() failed") ;
		goto cleanup ;
	}
	if (gtk_tree_view_row_expanded (tree_view, tree_path) == TRUE) {
		gtk_tree_view_collapse_row (tree_view, tree_path) ;
	} else {
		gtk_tree_view_expand_row (tree_view, tree_path, FALSE) ;
	}

cleanup:
	if (tree_path) {
		gtk_tree_path_free (tree_path) ;
		tree_path = NULL ;
	}
}

/**
 *Emits the "ungrab-focus-requested" signal.
 *@param a_this the current instance of #MlViewTreeEditor
 *@return MLVIEW_OK upon sucessful completion, an error code otherwise.
 */
enum MlViewStatus
mlview_tree_editor_request_ungrab_focus (MlViewTreeEditor *a_this)
{
	g_return_val_if_fail (a_this && MLVIEW_IS_TREE_EDITOR (a_this),
	                      MLVIEW_BAD_PARAM_ERROR) ;

	g_signal_emit (G_OBJECT (a_this),
	               gv_signals[UNGRAB_FOCUS_REQUESTED], 0) ;

	return MLVIEW_OK ;
}




enum MlViewStatus
mlview_tree_editor_grab_focus (MlViewTreeEditor *a_this)
{
	g_return_val_if_fail (a_this && MLVIEW_IS_TREE_EDITOR (a_this),
	                      MLVIEW_BAD_PARAM_ERROR) ;

	g_idle_add ((GSourceFunc)idle_add_grab_focus_on_tree_view,
	            a_this) ;

	return MLVIEW_OK ;
}




enum MlViewStatus
mlview_tree_editor_scroll_to_cell (MlViewTreeEditor *a_this,
                                   GtkTreePath *a_tree_path)
{
	GtkTreePath *tree_path = NULL ;

	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_TREE_EDITOR (a_this)
	                      && a_tree_path,
	                      MLVIEW_BAD_PARAM_ERROR) ;

	tree_path = gtk_tree_path_copy (a_tree_path) ;


	g_object_set_data (G_OBJECT (a_this), "tree-path-to-scroll-to",
	                   tree_path) ;

	g_idle_add ((GSourceFunc)idle_add_scroll_to_cell,
	            a_this) ;

	return MLVIEW_OK ;
}


/**
 *Connects to the relevant signals emited by the document object
 *model and register the relevant callbacks
 *@param a_this the current instance of #MlViewTreeEditor
 *@param a_doc the document object model to connect to.
 *@return MLVIEW_OK upon successful completion, an error code otherwise.
 *FIXME: connect to the  "dtd-node-*" signals
 */
enum MlViewStatus
mlview_tree_editor_connect_to_doc (MlViewTreeEditor *a_this,
                                   MlViewXMLDocument *a_doc)
{
	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_TREE_EDITOR (a_this)
	                      && MLVIEW_IS_XML_DOCUMENT (a_doc),
	                      MLVIEW_BAD_PARAM_ERROR) ;


	g_signal_connect (G_OBJECT (a_doc),
	                  "node-selected",
	                  G_CALLBACK (xml_doc_selected_node_cb),
	                  a_this) ;

	g_signal_connect (G_OBJECT (a_doc),
	                  "node-cut",
	                  G_CALLBACK (xml_doc_node_cut_cb),
	                  a_this) ;

	g_signal_connect (G_OBJECT (a_doc),
	                  "prev-sibling-node-inserted",
	                  G_CALLBACK (xml_doc_prev_sibling_node_inserted_cb),
	                  a_this) ;

	g_signal_connect (G_OBJECT (a_doc),
	                  "next-sibling-node-inserted",
	                  G_CALLBACK (xml_doc_next_sibling_node_inserted_cb),
	                  a_this) ;

	g_signal_connect (G_OBJECT (a_doc),
	                  "child-node-added",
	                  G_CALLBACK (xml_doc_child_node_added_cb),
	                  a_this) ;

	g_signal_connect (G_OBJECT (a_doc),
	                  "content-changed",
	                  G_CALLBACK (xml_doc_content_changed_cb),
	                  a_this) ;

	g_signal_connect (G_OBJECT (a_doc),
	                  "name-changed",
	                  G_CALLBACK (xml_doc_name_changed_cb),
	                  a_this) ;

	g_signal_connect (G_OBJECT (a_doc),
	                  "node-commented",
	                  G_CALLBACK (xml_doc_node_commented_cb),
	                  a_this) ;

	g_signal_connect (G_OBJECT (a_doc),
	                  "node-uncommented",
	                  G_CALLBACK (xml_doc_node_commented_cb),
	                  a_this) ;

	g_signal_connect (G_OBJECT (a_doc),
	                  "entity-node-public-id-changed",
	                  G_CALLBACK (entity_node_public_id_changed_cb),
	                  a_this) ;

	g_signal_connect (G_OBJECT (a_doc),
	                  "entity-node-system-id-changed",
	                  G_CALLBACK (entity_node_system_id_changed_cb),
	                  a_this) ;

	g_signal_connect (G_OBJECT (a_doc),
	                  "entity-node-content-changed",
	                  G_CALLBACK (entity_node_content_changed_cb),
	                  a_this) ;

	g_signal_connect (G_OBJECT (a_doc),
	                  "dtd-node-created",
	                  G_CALLBACK (xml_doc_internal_subset_node_added_cb),
	                  a_this) ;

	g_signal_connect (G_OBJECT (a_doc),
	                  "dtd-node-public-id-changed",
	                  G_CALLBACK (xml_doc_dtd_node_changed_cb),
	                  a_this) ;

	g_signal_connect (G_OBJECT (a_doc),
	                  "dtd-node-system-id-changed",
	                  G_CALLBACK (xml_doc_dtd_node_changed_cb),
	                  a_this) ;

	g_signal_connect (G_OBJECT (a_doc),
	                  "node-attribute-name-changed",
	                  G_CALLBACK (xml_doc_node_attribute_name_changed_cb),
	                  a_this) ;

	g_signal_connect (G_OBJECT (a_doc),
	                  "node-attribute-value-changed",
	                  G_CALLBACK (xml_doc_node_attribute_value_changed_cb),
	                  a_this) ;

	g_signal_connect (G_OBJECT (a_doc),
	                  "node-attribute-removed",
	                  G_CALLBACK (xml_doc_node_attribute_removed_cb),
	                  a_this) ;

	g_signal_connect (G_OBJECT (a_doc),
	                  "node-namespace-added",
	                  G_CALLBACK (xml_doc_node_namespace_added_cb),
	                  a_this) ;

	g_signal_connect (G_OBJECT (a_doc),
	                  "node-namespace-changed",
	                  G_CALLBACK (xml_doc_node_namespace_changed_cb),
	                  a_this) ;

	g_signal_connect (G_OBJECT (a_doc),
	                  "node-namespace-removed",
	                  G_CALLBACK (xml_doc_node_namespace_removed_cb),
	                  a_this) ;

	g_signal_connect (G_OBJECT (a_doc),
	                  "searched-node-found",
	                  G_CALLBACK (xml_doc_searched_node_found_cb),
	                  a_this) ;

	g_signal_connect (G_OBJECT (a_doc),
	                  "file-path-changed",
	                  G_CALLBACK (xml_doc_file_path_changed_cb),
	                  a_this) ;

	g_signal_connect (G_OBJECT (a_doc),
					  "document-reloaded",
					  G_CALLBACK (xml_doc_document_reloaded_cb),
					  a_this) ;

	g_signal_connect (G_OBJECT (a_doc),
	                  "document-changed",
	                  G_CALLBACK (xml_doc_document_changed_cb),
	                  a_this) ;

	g_signal_connect (G_OBJECT (a_doc),
	                  "document-undo-state-changed",
	                  G_CALLBACK (xml_doc_document_undo_state_changed_cb),
	                  a_this) ;

	return MLVIEW_OK ;
}

/**
 *Disconnects from the signal emited by the document object model
 *that is,  unregister all the callback registered by
 *mlview_tree_editor_connect_to_doc() .
 *@param a_this the current instance of #MlViewTreeEditor
 *@param a_doc the document object model to disconnect from.
 *@return MLVIEW_OK upon successful completion, an error code
 *otherwise.
 *FIXME: make sure we disconnect from all the signal we connected
 *to.
 */
enum MlViewStatus
mlview_tree_editor_disconnect_from_doc (MlViewTreeEditor *a_this,
                                        MlViewXMLDocument *a_doc)
{
	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_TREE_EDITOR (a_this)
	                      && PRIVATE (a_this)
	                      && MLVIEW_IS_XML_DOCUMENT (a_doc),
	                      MLVIEW_BAD_PARAM_ERROR) ;

	g_signal_handlers_disconnect_by_func
	(G_OBJECT (a_doc),
	 (void*)xml_doc_node_cut_cb,
	 a_this) ;
	g_signal_handlers_disconnect_by_func
	(G_OBJECT (a_doc),
	 (void*)xml_doc_prev_sibling_node_inserted_cb,
	 a_this) ;
	g_signal_handlers_disconnect_by_func
	(G_OBJECT (a_doc),
	 (void*)xml_doc_next_sibling_node_inserted_cb,
	 a_this) ;
	g_signal_handlers_disconnect_by_func
	(G_OBJECT (a_doc),
	 (void*)xml_doc_child_node_added_cb,
	 a_this) ;
	g_signal_handlers_disconnect_by_func
	(G_OBJECT (a_doc),
	 (void*)xml_doc_name_changed_cb,
	 a_this) ;
	g_signal_handlers_disconnect_by_func
	(G_OBJECT (a_doc),
	 (void*)xml_doc_content_changed_cb,
	 a_this) ;
	g_signal_handlers_disconnect_by_func
	(G_OBJECT (a_doc),
	 (void*)xml_doc_node_commented_cb,
	 a_this) ;
	g_signal_handlers_disconnect_by_func
	(G_OBJECT (a_doc),
	 (void*)xml_doc_node_attribute_name_changed_cb,
	 a_this) ;
	g_signal_handlers_disconnect_by_func
	(G_OBJECT (a_doc),
	 (void*)xml_doc_node_attribute_value_changed_cb,
	 a_this) ;
	g_signal_handlers_disconnect_by_func
	(G_OBJECT (a_doc),
	 (void*)xml_doc_node_attribute_removed_cb,
	 a_this) ;
	g_signal_handlers_disconnect_by_func
	(G_OBJECT (a_doc),
	 (void*)xml_doc_node_namespace_changed_cb,
	 a_this) ;
	g_signal_handlers_disconnect_by_func
	(G_OBJECT (a_doc),
	 (void*)xml_doc_node_namespace_removed_cb,
	 a_this) ;
	g_signal_handlers_disconnect_by_func
	(G_OBJECT (a_doc),
	 (void*)xml_doc_searched_node_found_cb,
	 a_this) ;
	g_signal_handlers_disconnect_by_func
	(G_OBJECT (a_doc),
	 (void*)xml_doc_selected_node_cb,
	 a_this) ;
	g_signal_handlers_disconnect_by_func
	(G_OBJECT (a_doc),
	 (void*)entity_node_public_id_changed_cb,
	 a_this) ;
	g_signal_handlers_disconnect_by_func
	(G_OBJECT (a_doc),
	 (void*)entity_node_system_id_changed_cb,
	 a_this) ;
	g_signal_handlers_disconnect_by_func
	(G_OBJECT (a_doc),
	 (void*)xml_doc_internal_subset_node_added_cb,
	 a_this) ;
	g_signal_handlers_disconnect_by_func
	(G_OBJECT (a_doc),
	 (void*)xml_doc_internal_subset_node_added_cb,
	 a_this) ;
	g_signal_handlers_disconnect_by_func
	(G_OBJECT (a_doc),
	 (void*)xml_doc_dtd_node_changed_cb,
	 a_this) ;
	g_signal_handlers_disconnect_by_func
	(G_OBJECT (a_doc),
	 (void*)xml_doc_file_path_changed_cb,
	 a_this) ;
	g_signal_handlers_disconnect_by_func
	(G_OBJECT (a_doc),
	 (void*)xml_doc_document_reloaded_cb,
	 a_this) ;
	g_signal_handlers_disconnect_by_func
	(G_OBJECT (a_doc),
	 (void*)xml_doc_document_changed_cb,
	 a_this) ;
	g_signal_handlers_disconnect_by_func
	(G_OBJECT (a_doc),
	 (void*)xml_doc_document_undo_state_changed_cb,
	 a_this) ;
	return MLVIEW_OK ;
}


/**
 *Selects the next sibling node of the currently selected node.
 *@param a_this the current instance of #MlViewTreeEditor
 *@return MLVIEW_OK upon successful completion, an error code otherwise.
 */
enum MlViewStatus
mlview_tree_editor_select_next_sibling_node (MlViewTreeEditor *a_this)
{

	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_TREE_EDITOR (a_this)
	                      && PRIVATE (a_this),
	                      MLVIEW_BAD_PARAM_ERROR) ;

	if (!PRIVATE (a_this)->cur_sel_start) {
		return MLVIEW_OK ;
	}

	return mlview_tree_editor_select_next_sibling_node2
	       (a_this, PRIVATE (a_this)->cur_sel_start) ;
}

/**
 *selects the next sibling node of a given node.
 *@param a_this the current instance of #MlViewTreeEditor
 *@param a_cur_sel_node row reference of the reference node.
 *@return MLVIEW_OK upon successful completion, an error code otherwise.
 */
enum MlViewStatus
mlview_tree_editor_select_next_sibling_node2 (MlViewTreeEditor *a_this,
        GtkTreeRowReference *a_cur_sel_node)
{
	GtkTreePath *tree_path = NULL ;

	g_return_val_if_fail (a_this && MLVIEW_IS_TREE_EDITOR (a_this)
	                      && PRIVATE (a_this),
	                      MLVIEW_BAD_PARAM_ERROR) ;

	tree_path = gtk_tree_row_reference_get_path
	            (a_cur_sel_node) ;
	gtk_tree_path_next (tree_path) ;
	mlview_tree_editor_select_node2 (a_this, tree_path, TRUE, TRUE) ;
	if (tree_path) {
		g_free (tree_path) ;
		tree_path = NULL ;
	}
	return MLVIEW_OK ;

}

/**
 *Selects the previous sibling node of the currently selected node.
 *@param a_this the current instance of #MlViewTreeEditor
 *@return MLVIEW_OK upon succesful completion, an error code otherwise.
 */
enum MlViewStatus
mlview_tree_editor_select_prev_sibling_node (MlViewTreeEditor *a_this)
{

	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_TREE_EDITOR (a_this)
	                      && PRIVATE (a_this),
	                      MLVIEW_BAD_PARAM_ERROR) ;

	if (!PRIVATE (a_this)->cur_sel_start) {
		return MLVIEW_OK ;
	}

	return mlview_tree_editor_select_prev_sibling_node2
	       (a_this, PRIVATE (a_this)->cur_sel_start) ;
}

/**
 *Selects the previous sibling node of a given reference node.
 *@param a_this the current instance of #MlViewTreeEditor.
 *@param a_cur_sel_node the tree row reference of the reference node.
 */
enum MlViewStatus
mlview_tree_editor_select_prev_sibling_node2 (MlViewTreeEditor *a_this,
        GtkTreeRowReference *a_cur_sel_node)
{
	GtkTreePath *tree_path = NULL ;

	g_return_val_if_fail (a_this && MLVIEW_IS_TREE_EDITOR (a_this)
	                      && PRIVATE (a_this),
	                      MLVIEW_BAD_PARAM_ERROR) ;

	tree_path = gtk_tree_row_reference_get_path
	            (a_cur_sel_node) ;
	gtk_tree_path_prev (tree_path) ;
	mlview_tree_editor_select_node2 (a_this, tree_path, TRUE, TRUE) ;
	if (tree_path) {
		g_free (tree_path) ;
		tree_path = NULL ;
	}
	return MLVIEW_OK ;
}

/**
 *Selects the parent node of the currently selected node.
 *@param a_this the current instance of #MlViewTreeEditor
 *@return MLVIEW_OK upon successful completion, an error code
 *otherwise.
 */
enum MlViewStatus
mlview_tree_editor_select_parent_node (MlViewTreeEditor *a_this)
{
	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_TREE_EDITOR (a_this)
	                      && PRIVATE (a_this),
	                      MLVIEW_BAD_PARAM_ERROR) ;

	if (!PRIVATE (a_this)->cur_sel_start) {
		return MLVIEW_OK ;
	}

	return mlview_tree_editor_select_parent_node2
	       (a_this, PRIVATE (a_this)->cur_sel_start) ;
}

/**
 *Selects the parent node of a give reference node.
 *@param a_this the current instance of #MlViewTreeEditor
 *@param a_cur_sel_node the tree row reference of the reference node.
 *@return MLVIEW_OK upon successful completion, an error code otherwise.
 */
enum MlViewStatus
mlview_tree_editor_select_parent_node2 (MlViewTreeEditor *a_this,
                                        GtkTreeRowReference *a_cur_sel_node)
{
	GtkTreePath *tree_path = NULL ;

	g_return_val_if_fail (a_this && MLVIEW_IS_TREE_EDITOR (a_this)
	                      && PRIVATE (a_this),
	                      MLVIEW_BAD_PARAM_ERROR) ;

	tree_path = gtk_tree_row_reference_get_path
	            (a_cur_sel_node) ;
	gtk_tree_path_up (tree_path) ;
	mlview_tree_editor_select_node2 (a_this, tree_path,
	                                 TRUE, TRUE) ;
	if (tree_path) {
		g_free (tree_path) ;
		tree_path = NULL ;
	}
	return MLVIEW_OK ;
}

GtkStyle*
mlview_tree_editor_get_style (MlViewTreeEditor *a_this)
{
	THROW_IF_FAIL (a_this && MLVIEW_IS_TREE_EDITOR (a_this)
				   && PRIVATE (a_this));

	return (PRIVATE (a_this)->style);
}

void
mlview_tree_editor_set_style (MlViewTreeEditor *a_this,
							  GtkStyle *style)
{
	THROW_IF_FAIL (a_this && MLVIEW_IS_TREE_EDITOR (a_this)
				   && PRIVATE (a_this));

	PRIVATE (a_this)->style = style;
}

