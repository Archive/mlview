/* -*- Mode: C; indent-tabs-mode:true ; c-basic-offset: 4-*- */
/*
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute 
 *it and/or modify it under the terms of 
 *the GNU General Public License as published by the 
 *Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */

#ifndef __MLVIEW_SCHEMA_LIST_H__
#define __MLVIEW_SCHEMA_LIST_H__

#include <glib-object.h>

#include "mlview-schema.h"

G_BEGIN_DECLS

#define MLVIEW_TYPE_SCHEMA_LIST (mlview_schema_list_get_type ())
#define MLVIEW_SCHEMA_LIST(object) (G_TYPE_CHECK_INSTANCE_CAST((object),MLVIEW_TYPE_SCHEMA_LIST,MlViewSchemaList))
#define MLVIEW_SCHEMA_LIST_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST((klass),MLVIEW_TYPE_SCHEMA_LIST,MlViewSchemaListClass))
#define MLVIEW_IS_SCHEMA_LIST(object) (G_TYPE_CHECK_INSTANCE_TYPE((object),MLVIEW_TYPE_SCHEMA_LIST))
#define MLVIEW_IS_SCHEMA_LIST_CLASS(object) (G_TYPE_CHECK_CLASS_TYPE((klass),MLVIEW_TYPE_SCHEMA_LIST))

typedef void (*MlViewSchemaListFunc) (MlViewSchema *a_schema,
                                      gpointer a_user_data);

typedef struct _MlViewSchemaList MlViewSchemaList;
typedef struct _MlViewSchemaListClass MlViewSchemaListClass;
typedef struct _MlViewSchemaListPrivate MlViewSchemaListPrivate;

/**
 * This #MLViewSchemaList abstracts a list of #MlViewSchema that is to
 * be associated with an instance of #MlViewXMLDocument.
 * Client code can query an instance of #MlViewSchemaList to get a schema
 * that is denoted by a given URL, for example
 */
struct _MlViewSchemaList
{
	GObject object;
	MlViewSchemaListPrivate *priv;
};

struct _MlViewSchemaListClass
{
	GObjectClass object_class;

	void (*schema_associated) (MlViewSchemaList *a_this,
	                           MlViewSchema *a_schema,
	                           gpointer a_user_data);

	void (*schema_unassociated) (MlViewSchemaList *a_this,
	                             MlViewSchema *a_schema,
	                             gpointer a_user_data);
};

GType mlview_schema_list_get_type (void);

MlViewSchemaList *mlview_schema_list_new (void);

gboolean mlview_schema_list_add_schema (MlViewSchemaList *a_this,
                                        MlViewSchema *a_schema);

gboolean mlview_schema_list_remove_schema_by_url (MlViewSchemaList *a_this,
        const gchar *a_url);

void mlview_schema_list_foreach (MlViewSchemaList *a_schemas,
                                 MlViewSchemaListFunc a_func,
                                 gpointer a_user_data);

MlViewSchema *mlview_schema_list_lookup_by_url (MlViewSchemaList *a_this,
        const gchar *a_url);

enum MlViewStatus mlview_schema_list_get_size (MlViewSchemaList *a_this,
        guint *a_size) ;

G_END_DECLS

#endif
