/* -*- Mode: C++; indent-tabs-mode:nil; c-basic-offset: 4-*- */
/*
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute 
 *it and/or modify it under the terms of 
 *the GNU General Public License as published by the 
 *Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYING file for copyright information.
 */

#include <glade/glade.h>
#include <gnome.h>

#include "mlview-schemas-window.h"
#include "mlview-file-selection.h"
#include "mlview-schema-list.h"

static const gchar
* schemas_type_labels [] =
    {
        "Unknown Schema",
        "Document Type Definition",
        "Relax-NG Schema",
        "XML Schema Definition"
    };

static const gchar*
schemas_type_iconpath [] =
    {
        NULL,
        "mlview/mlview-dtd-schema.png",
        "mlview/mlview-rng-schema.png",
        "mlview/mlview-xsd-schema.png"
    };

typedef struct _MlViewSchemasWindow MlViewSchemasWindow;
typedef struct _MlViewSchemasWindowAddDialog MlViewSchemasWindowAddDialog;

struct _MlViewSchemasWindowAddDialog
{
	GtkDialog *dialog;
	GnomeFileEntry *fileentry;
	GtkComboBox *combo;
};

struct _MlViewSchemasWindow
{
	GtkTreeView *view;
	GtkWindow *win;
	MlViewSchemaList *schemas;
	GHashTable *map;

	MlViewSchemasWindowAddDialog *add_dialog;
};

struct BuildModelData
{
	GtkListStore *store;
	GHashTable *table;
};

static void
add_schema_to_list_store (MlViewSchema *a_schema,
                          GtkListStore *a_store,
                          GHashTable *a_table)
{
	enum MlViewSchemaType schema_type = SCHEMA_TYPE_UNDEF ;
	enum MlViewStatus status = MLVIEW_OK ;
	GtkTreeIter iter = { 0 };
	GtkTreePath *path = NULL;
	GtkTreeRowReference *ref = NULL;
	GdkPixbuf *pixbuf = NULL;
	gchar *label = NULL,
	               *url = NULL,
	                      *icon_path = NULL;

	THROW_IF_FAIL (a_schema);
	THROW_IF_FAIL (a_store && GTK_IS_LIST_STORE (a_store));
	THROW_IF_FAIL (a_table);

	gtk_list_store_append (a_store, &iter);

	status = mlview_schema_get_type (a_schema, &schema_type) ;
	THROW_IF_FAIL (
	    status == MLVIEW_OK && schema_type != SCHEMA_TYPE_UNDEF) ;

	url = mlview_schema_get_url (a_schema);
	label = g_strdup_printf ("%s\n<span color=\"gray\">%s</span>",
	                         url, schemas_type_labels[schema_type]);

	icon_path = gnome_program_locate_file (
	                NULL,
	                GNOME_FILE_DOMAIN_APP_DATADIR,
	                schemas_type_iconpath[schema_type],
	                TRUE,
	                NULL);

	pixbuf = gdk_pixbuf_new_from_file (icon_path, NULL);

	g_free (icon_path);
	icon_path = NULL;

	gtk_list_store_set (a_store, &iter,
	                    0, a_schema,
	                    1, GDK_PIXBUF (pixbuf),
	                    2, label,
	                    -1);

	path = gtk_tree_model_get_path (GTK_TREE_MODEL (a_store),
	                                &iter);

	if (!path) {
		gtk_list_store_remove (a_store, &iter);
		return;
	}

	ref = gtk_tree_row_reference_new (
	          GTK_TREE_MODEL (a_store),
	          path);

	if (ref)
		g_hash_table_insert (a_table, a_schema, ref);
	else
		gtk_list_store_remove (a_store, &iter);

	gtk_tree_path_free (path);
	path = NULL;
}

static void
add_schema_to_list_store_foreach (MlViewSchema *a_schema,
                                  struct BuildModelData *a_data)
{
	THROW_IF_FAIL (a_schema);
	THROW_IF_FAIL (a_data);
	THROW_IF_FAIL (a_data->store &&
	               GTK_IS_LIST_STORE (a_data->store));
	THROW_IF_FAIL (a_data->table);

	add_schema_to_list_store (a_schema, a_data->store,
	                          a_data->table);
}

static void
schemas_window_build_model_with_schemas (MlViewSchemaList *a_schemas,
        GtkTreeModel **a_model,
        GHashTable **a_table)
{
	GtkListStore *store = NULL;
	struct BuildModelData *data = NULL;
	GHashTable *table = NULL;

	THROW_IF_FAIL (a_model && a_table);
	THROW_IF_FAIL (a_schemas && MLVIEW_IS_SCHEMA_LIST (a_schemas));

	store = gtk_list_store_new (3, G_TYPE_POINTER,
	                            GDK_TYPE_PIXBUF, G_TYPE_STRING);

	if (!store)
		goto cleanup;

	table = g_hash_table_new_full (g_direct_hash, g_direct_equal,
	                               NULL, (GDestroyNotify) gtk_tree_row_reference_free);

	if (!table)
		goto cleanup;

	data = (BuildModelData *) g_try_malloc (sizeof (struct BuildModelData));

	if (!data)
		goto cleanup;

	data->store = store;
	data->table = table;

	mlview_schema_list_foreach (a_schemas,
	                            (MlViewSchemaListFunc) add_schema_to_list_store_foreach,
	                            data);

	g_free (data);
	data = NULL;

	*a_model = GTK_TREE_MODEL (store);
	*a_table = table;

	return;

cleanup:
	if (store) {
		g_object_unref (store);
		store = NULL;
	}

	if (data) {
		g_free (data);
		data = NULL;
	}

	if (table) {
		g_hash_table_destroy (table);
		table = NULL;
	}

	*a_model = NULL;
	*a_table = NULL;
}

static void
schemas_window_add_clicked_cb (GtkButton *a_button, MlViewSchemasWindow *a_schemas)
{
	gint result = -1;
	gint index = -1;
	gchar *schema_uri = NULL;
	MlViewSchema *schema = NULL;
	enum MlViewSchemaType schema_type = (MlViewSchemaType) -1;

	THROW_IF_FAIL (a_schemas);
	THROW_IF_FAIL (a_schemas->add_dialog);
	THROW_IF_FAIL (a_schemas->add_dialog->dialog);
	THROW_IF_FAIL (a_schemas->add_dialog->combo);
	THROW_IF_FAIL (a_schemas->add_dialog->fileentry);
	THROW_IF_FAIL (a_schemas->schemas);

	result = gtk_dialog_run (GTK_DIALOG (a_schemas->add_dialog->dialog));
	switch (result) {
	case GTK_RESPONSE_ACCEPT:
		index = gtk_combo_box_get_active (a_schemas->add_dialog->combo);
		schema_uri = (gchar*) gtk_entry_get_text (
		                 (GtkEntry*)gnome_file_entry_gtk_entry (
		                     GNOME_FILE_ENTRY (
		                         a_schemas->add_dialog->fileentry)));
		break;
	default:
		break;
	}
	gtk_widget_hide (GTK_WIDGET (a_schemas->add_dialog->dialog));

	if (index == -1)
		return;

	switch (index) {
	case 0:
		schema_type = SCHEMA_TYPE_DTD;
		break;
	case 1:
		schema_type = SCHEMA_TYPE_RNG;
		break;
	case 2:
		schema_type = SCHEMA_TYPE_XSD;
		break;
	}

	schema = mlview_schema_load_from_file (
	             schema_uri,
	             schema_type);
	if (schema)
		mlview_schema_list_add_schema (a_schemas->schemas, schema);

	/* add schema to combo history */
	gnome_entry_prepend_history (
	    GNOME_ENTRY (gnome_file_entry_gnome_entry (a_schemas->add_dialog->fileentry)),
	    TRUE,
	    schema_uri);
}

static void
unassociate_schema_foreach (GtkTreePath *a_path,
                            MlViewSchemasWindow *a_schemas)
{
	GtkTreeIter iter = { 0 };
	GtkTreeModel *model = NULL;
	gboolean res = FALSE;
	MlViewSchema *schema = NULL;
	gchar *url = NULL;

	THROW_IF_FAIL (a_path);

	if (!(a_schemas && a_schemas->schemas &&
	        MLVIEW_IS_SCHEMA_LIST (a_schemas->schemas) &&
	        a_schemas->view && GTK_IS_TREE_VIEW (a_schemas->view)))
		goto cleanup;

	model = gtk_tree_view_get_model (a_schemas->view);

	if (!(model && GTK_IS_TREE_MODEL (model)))
		goto cleanup;

	res = gtk_tree_model_get_iter (model, &iter, a_path);

	if (!res)
		goto cleanup;

	gtk_tree_model_get (model, &iter, 0, &schema, -1);

	if (!schema)
		goto cleanup;

	url = mlview_schema_get_url (schema);

	if (!url)
		goto cleanup;

	res = mlview_schema_list_remove_schema_by_url
	      (a_schemas->schemas, url);

	if (!res)
		goto cleanup;

cleanup:
	if (a_path) {
		gtk_tree_path_free (a_path);
		a_path = NULL;
	}
}

static void
schemas_window_del_clicked_cb (GtkButton *a_button, MlViewSchemasWindow *a_schemas)
{
	GtkTreeSelection *selection = NULL;
	GList *rows = NULL;

	THROW_IF_FAIL (a_schemas);
	THROW_IF_FAIL (a_schemas->view && GTK_IS_TREE_VIEW (a_schemas->view));

	selection = gtk_tree_view_get_selection (a_schemas->view);

	THROW_IF_FAIL (selection && GTK_IS_TREE_SELECTION (selection));

	rows = gtk_tree_selection_get_selected_rows (selection, NULL);

	g_list_foreach (rows, (GFunc) unassociate_schema_foreach,
	                a_schemas);

	g_list_free (rows);
	rows = NULL;
}

static void
schemas_window_close_clicked_cb (GtkButton *a_button, MlViewSchemasWindow *a_schemas)
{
	THROW_IF_FAIL (a_schemas);
	THROW_IF_FAIL (a_schemas->win && GTK_IS_WIDGET (a_schemas->win));

	gtk_widget_destroy (GTK_WIDGET (a_schemas->win));
}

static void
schema_associated_cb (MlViewSchemaList *a_list,
                      MlViewSchema *a_schema,
                      MlViewSchemasWindow *a_data)
{
	GtkTreeModel *model = NULL;
	GtkListStore *store = NULL;

	THROW_IF_FAIL (a_data && a_data->map);
	THROW_IF_FAIL (a_data->view && GTK_IS_TREE_VIEW (a_data->view));
	THROW_IF_FAIL (a_schema);

	model = gtk_tree_view_get_model (a_data->view);

	THROW_IF_FAIL (model && GTK_IS_LIST_STORE (model));

	store = GTK_LIST_STORE (model);

	THROW_IF_FAIL (store && GTK_IS_LIST_STORE (store));

	add_schema_to_list_store (a_schema, store, a_data->map);
}

static void
schema_unassociated_cb (MlViewSchemaList *a_list,
                        MlViewSchema *a_schema,
                        MlViewSchemasWindow *a_data)
{
	GtkTreeModel *model = NULL;
	GtkListStore *store = NULL;
	GtkTreeRowReference *ref = NULL;
	GtkTreePath *path = NULL;
	GtkTreeIter iter = { 0 };
	gboolean res = FALSE;

	THROW_IF_FAIL (a_data && a_data->map);
	THROW_IF_FAIL (a_data->view && GTK_IS_TREE_VIEW (a_data->view));
	THROW_IF_FAIL (a_schema);

	model = gtk_tree_view_get_model (a_data->view);

	THROW_IF_FAIL (model && GTK_IS_LIST_STORE (model));

	store = GTK_LIST_STORE (model);

	THROW_IF_FAIL (store && GTK_IS_LIST_STORE (store));

	ref = (GtkTreeRowReference *) g_hash_table_lookup (a_data->map, a_schema);

	THROW_IF_FAIL (ref);

	path = gtk_tree_row_reference_get_path (ref);

	THROW_IF_FAIL (path);

	res = gtk_tree_model_get_iter (model, &iter, path);

	gtk_tree_path_free (path);
	path = NULL;

	THROW_IF_FAIL (res);

	gtk_list_store_remove (store, &iter);

	res = g_hash_table_remove (a_data->map, a_schema);

	THROW_IF_FAIL (res);
}

static void
schemas_window_destroy_cb (GtkWidget *a_win,
                           MlViewSchemasWindow *a_schemas)
{
	THROW_IF_FAIL (a_schemas);

	if (a_schemas->schemas &&
	        MLVIEW_IS_SCHEMA_LIST (a_schemas->schemas)) {
		g_signal_handlers_disconnect_by_func
		(G_OBJECT (a_schemas->schemas),
		 (void *) (schema_unassociated_cb),
		 a_schemas);

		g_signal_handlers_disconnect_by_func
		(G_OBJECT (a_schemas->schemas),
		 (void *) (schema_associated_cb),
		 a_schemas);
	}

	g_free (a_schemas);
	a_schemas = NULL;
}


GtkWidget *
mlview_schemas_window_new_with_document (MlViewXMLDocument *a_doc)
{
	GladeXML *gxml = NULL;
	gchar *gfile = NULL,
	               *tmp = NULL,
	                      *url = NULL;
	GtkWidget *win = NULL,
	                 *view = NULL,
	                         *dialog = NULL,
	                                   *combo = NULL,
	                                            *fileentry = NULL;
	MlViewSchemaList *schemas = NULL;
	GtkTreeModel *model = NULL;
	GtkListStore *text = NULL;
	MlViewSchemasWindow *data = NULL;
	GHashTable *table = NULL;
	GtkTreeIter iter = { 0 };
	GtkTreeSelection *selection = NULL;
	GtkCellRenderer *renderer = NULL;

	THROW_IF_FAIL (a_doc && MLVIEW_IS_XML_DOCUMENT (a_doc));

	schemas = mlview_xml_document_get_schema_list (a_doc);

	g_return_val_if_fail (schemas && MLVIEW_IS_SCHEMA_LIST (schemas),
	                      NULL);

	gfile = gnome_program_locate_file
	        (NULL,
	         GNOME_FILE_DOMAIN_APP_DATADIR,
	         PACKAGE "/mlview-schemas-window.glade",
	         TRUE, NULL);

	if (!gfile)
		goto cleanup;

	gxml = glade_xml_new (gfile, NULL, NULL);

	g_free (gfile);
	gfile = NULL;

	if (!gxml)
		goto cleanup;

	win = glade_xml_get_widget (gxml, "schemas_window");

	if (!(win && GTK_IS_WINDOW (win)))
		goto cleanup;

	url = mlview_xml_document_get_file_path (a_doc);

	if (url) {
		tmp = g_strconcat ("Schemas", " - ", url, NULL);

		if (tmp) {
			gtk_window_set_title (GTK_WINDOW (win), tmp);

			g_free (tmp);
			tmp = NULL;
		}
	}

	schemas_window_build_model_with_schemas (schemas, &model,
	        &table);

	if (!(model && GTK_IS_TREE_MODEL (model) && table))
		goto cleanup;

	view = glade_xml_get_widget (gxml, "schemas_treeview");

	if (!(view && GTK_IS_TREE_VIEW (view)))
		goto cleanup;

	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (view));

	if (!selection)
		goto cleanup;

	gtk_tree_selection_set_mode (selection, GTK_SELECTION_MULTIPLE);

	renderer = gtk_cell_renderer_pixbuf_new ();
	gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (view),
	        1, _("Type"),
	        renderer, "pixbuf", 1, NULL);

	renderer = gtk_cell_renderer_text_new ();
	gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (view),
	        2, _("URL"),
	        renderer, "markup", 2, NULL);

	gtk_tree_view_set_model (GTK_TREE_VIEW (view), model);

	g_object_unref (G_OBJECT (model));
	model = NULL;

	dialog = glade_xml_get_widget (gxml, "add_dialog");

	if (!(dialog && GTK_IS_DIALOG (dialog)))
		goto cleanup;

	fileentry = glade_xml_get_widget (gxml, "fileentry");

	if (!fileentry)
		goto cleanup;

	combo = glade_xml_get_widget (gxml, "combo");

	if (!(combo && GTK_IS_COMBO_BOX (combo)))
		goto cleanup;

	text = gtk_list_store_new (1, G_TYPE_STRING);

	if (!text)
		goto cleanup;

	gtk_list_store_append (text, &iter);
	gtk_list_store_set (text, &iter, 0, "Document Type Definition (DTD)", -1);
	gtk_list_store_append (text, &iter);
	gtk_list_store_set (text, &iter, 0, "Relax-NG Schema (RNG)", -1);
	gtk_list_store_append (text, &iter);
	gtk_list_store_set (text, &iter, 0, "XML Schema Definition (XSD)", -1);

	renderer = gtk_cell_renderer_text_new ();

	gtk_cell_layout_pack_start (GTK_CELL_LAYOUT (combo),
	                            renderer, TRUE);
	gtk_cell_layout_set_attributes (GTK_CELL_LAYOUT (combo),
	                                renderer, "text", 0, NULL);

	gtk_combo_box_set_model (GTK_COMBO_BOX (combo),
	                         GTK_TREE_MODEL (text));

	gtk_combo_box_set_active (GTK_COMBO_BOX (combo),
	                          0);

	g_object_unref (G_OBJECT (text));
	text = NULL;


	data = (MlViewSchemasWindow *) g_try_malloc (sizeof (MlViewSchemasWindow));

	if (!data)
		goto cleanup;

	data->add_dialog = (MlViewSchemasWindowAddDialog *) g_try_malloc 
				(sizeof (MlViewSchemasWindowAddDialog));

	data->win = GTK_WINDOW (win);
	data->view = GTK_TREE_VIEW (view);
	data->schemas = schemas;
	data->map = table;
	data->add_dialog->dialog = GTK_DIALOG (dialog);
	data->add_dialog->combo = GTK_COMBO_BOX (combo);
	data->add_dialog->fileentry = GNOME_FILE_ENTRY (fileentry);

	glade_xml_signal_connect_data (gxml, "on_add_clicked",
	                               G_CALLBACK (schemas_window_add_clicked_cb),
	                               data);
	glade_xml_signal_connect_data (gxml, "on_del_clicked",
	                               G_CALLBACK (schemas_window_del_clicked_cb),
	                               data);
	glade_xml_signal_connect_data (gxml, "on_close_clicked",
	                               G_CALLBACK (schemas_window_close_clicked_cb),
	                               data);

	g_signal_connect (G_OBJECT (win), "destroy",
	                  G_CALLBACK (schemas_window_destroy_cb), data);

	g_signal_connect (G_OBJECT (schemas), "schema-unassociated",
	                  G_CALLBACK (schema_unassociated_cb), data);

	g_signal_connect (G_OBJECT (schemas), "schema-associated",
	                  G_CALLBACK (schema_associated_cb), data);

	return win;

cleanup:
	if (gfile) {
		g_free (gfile);
		gfile = NULL;
	}

	if (gxml) {
		g_object_unref (gxml);
		gxml = NULL;
	}

	if (win) {
		gtk_widget_destroy (win);
		win = NULL;
	}

	if (url) {
		g_free (url);
		url = NULL;
	}

	if (dialog) {
		gtk_widget_destroy (dialog);
		dialog = NULL;
	}

	if (tmp) {
		g_free (tmp);
		tmp = NULL;
	}

	if (model) {
		g_object_unref (G_OBJECT (model));
		model = NULL;
	}

	if (text) {
		g_object_unref (G_OBJECT (text));
		text = NULL;
	}

	if (data->add_dialog) {
		g_free (data->add_dialog);
		data->add_dialog = NULL;
	}

	if (data) {
		g_free (data);
		data = NULL;
	}

	if (table) {
		g_hash_table_destroy (table);
		table = NULL;
	}

	return NULL;
}
