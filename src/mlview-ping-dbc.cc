/* -*- Mode: C; indent-tabs-mode:nil; c-basic-offset: 8-*- */

/*
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute 
 *it and/or modify it under the terms of 
 *the GNU General Public License as published by the 
 *Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */

#include "string.h"
#include "mlview-ping-dbc.h"
#include "mlview-idbc.h"

#ifdef MLVIEW_WITH_DBUS

#include <dbus/dbus.h>
#define PRIVATE(obj) (obj)->priv


struct _MlViewPingDBCPriv
{
	DBusConnection *dbus_connection ;
	gboolean dispose_has_run ;
} ;

static void mlview_ping_dbc_class_init (MlViewPingDBCClass *a_klass) ;

static void mlview_ping_dbc_init (MlViewPingDBC *a_this) ;

static void mlview_ping_dbc_idbc_init (MlViewIDBC *a_this) ;

static void mlview_ping_dbc_dispose (GObject *a_this) ;

static void mlview_ping_dbc_finalize (GObject *a_this) ;

static enum MlViewStatus get_bus (MlViewPingDBC *a_this,
                                  DBusConnection **a_con,
                                  GError **a_error) ;

static GObjectClass *gv_parent_class = NULL ;

/*****************
 * private methods
 *****************/

static void
mlview_ping_dbc_class_init (MlViewPingDBCClass *a_klass)
{
	GObjectClass *gobject_class = NULL ;

	g_return_if_fail (a_klass != NULL) ;
	gv_parent_class = g_type_class_peek_parent (a_klass) ;
	g_return_if_fail (gv_parent_class) ;
	gobject_class = G_OBJECT_CLASS (a_klass) ;

	gobject_class->dispose = mlview_ping_dbc_dispose ;
	gobject_class->finalize = mlview_ping_dbc_finalize ;
}

static void
mlview_ping_dbc_init (MlViewPingDBC *a_this)
{
	g_return_if_fail (a_this && MLVIEW_IS_PING_DBC (a_this)) ;

	PRIVATE (a_this) = g_try_malloc (sizeof (MlViewPingDBCPriv));
	if (!PRIVATE (a_this)) {
		mlview_utils_trace_debug ("malloc failed") ;
		return ;
	}
	memset (PRIVATE (a_this), 0, sizeof (MlViewPingDBCPriv)) ;
}

static void
mlview_ping_dbc_idbc_init (MlViewIDBC *a_this)
{}

static void
mlview_ping_dbc_dispose (GObject *a_this)
{
	MlViewPingDBC *thiz = NULL;

	g_return_if_fail (a_this && MLVIEW_IS_PING_DBC (a_this)) ;
	thiz = MLVIEW_PING_DBC (a_this) ;
	g_return_if_fail (thiz) ;

	if (PRIVATE (thiz)->dispose_has_run) {
		return ;
	}
	if (gv_parent_class->dispose) {
		gv_parent_class->dispose (a_this) ;
	}
	PRIVATE (thiz)->dispose_has_run = TRUE ;
}

static void
mlview_ping_dbc_finalize (GObject *a_this)
{
	MlViewPingDBC *thiz = NULL ;

	g_return_if_fail (a_this && MLVIEW_IS_PING_DBC (a_this)) ;
	thiz = MLVIEW_PING_DBC (a_this) ;
	g_return_if_fail (thiz) ;

	if (!PRIVATE (thiz)) {
		return ;
	}
	g_free (PRIVATE (thiz)) ;
	PRIVATE (thiz)  = NULL ;
}


static enum MlViewStatus
get_bus (MlViewPingDBC *a_this,
         DBusConnection **a_con,
         GError **a_error)
{
	g_return_val_if_fail (MLVIEW_IS_PING_DBC (a_this)
	                      && a_con,
	                      MLVIEW_BAD_PARAM_ERROR) ;

	if (!PRIVATE (a_this)->dbus_connection) {
		if (mlview_idbc_get_session_bus
		        (MLVIEW_IDBC (a_this),
		         &PRIVATE (a_this)->dbus_connection,
		         a_error))
			return  MLVIEW_BUS_ERROR ;
	}
	*a_con = PRIVATE (a_this)->dbus_connection ;
	return MLVIEW_OK ;
}


/*****************
 * public methods
 *****************/

GType
mlview_ping_dbc_get_type (void)
{
	static GType type = 0 ;

	if (!type) {
		static const GTypeInfo type_info = {
		                                       sizeof (MlViewPingDBCClass),
		                                       NULL, NULL,
		                                       (GClassInitFunc) mlview_ping_dbc_class_init,
		                                       NULL, NULL,
		                                       sizeof (MlViewPingDBC),
		                                       0,
		                                       (GInstanceInitFunc) mlview_ping_dbc_init
		                                   } ;
		type = g_type_register_static (G_TYPE_OBJECT,
		                               "MlViewPingDBC",
		                               &type_info, 0) ;

		static const GInterfaceInfo idbc_info = {
		                                            (GInterfaceInitFunc) mlview_ping_dbc_idbc_init,
		                                            NULL, NULL
		                                        } ;
		g_type_add_interface_static (type, MLVIEW_TYPE_IDBC,
		                             &idbc_info) ;
	}
	return type ;
}

MlViewPingDBC*
mlview_ping_dbc_new (void)
{
	MlViewPingDBC *result = NULL ;

	result = g_object_new (MLVIEW_TYPE_PING_DBC, NULL) ;
	return result ;
}

enum MlViewStatus
mlview_ping_dbc_ping (MlViewPingDBC *a_this,
                      const gchar *a_service_name)
{
	DBusError dbus_error = {0} ;
	DBusConnection *dbus_connection=NULL ;
	DBusMessage *message=NULL, *reply=NULL ;
	enum MlViewStatus status = MLVIEW_OK ;

	g_return_val_if_fail (a_this && MLVIEW_IS_PING_DBC (a_this)
	                      && a_service_name,
	                      MLVIEW_BAD_PARAM_ERROR) ;

	dbus_error_init (&dbus_error) ;
	get_bus (a_this, &dbus_connection, NULL) ;
	g_return_val_if_fail (dbus_connection, MLVIEW_BUS_ERROR) ;

	message = dbus_message_new_method_call
	          (a_service_name,
	           PATH_ORG_MLVIEW_PING_OBJECT,
	           INTERFACE_ORG_MLVIEW_PING_OBJECT_IFACE,
	           "ping") ;
	if (!message) {
		mlview_utils_trace_debug ("Could not create message") ;
		status = MLVIEW_OUT_OF_MEMORY_ERROR ;
		goto cleanup ;
	}

	reply = dbus_connection_send_with_reply_and_block (dbus_connection,
	        message,
	        -1/*not timeout*/,
	        &dbus_error) ;
	if (dbus_error_is_set (&dbus_error) || !reply) {
		status = MLVIEW_PING_FAILED_ERROR ;
		goto cleanup ;
	}
	status = MLVIEW_OK ;

cleanup:
	if (message) {
		dbus_message_unref (message) ;
		message = NULL ;
	}
	if (reply) {
		dbus_message_unref (reply) ;
		reply = NULL ;
	}
	return status ;
}

enum MlViewStatus
mlview_ping_dbc_list_active_services (MlViewPingDBC *a_this,
                                      gchar ***a_active_services,
                                      guint *a_nb_of_services)
{
	enum MlViewStatus status = MLVIEW_OK ;
#define ORG_MLVIEW_SERVICE "org.mlview.Service"
#define MAX_NUM_SERVICES 20

	const gchar *base_service_name = ORG_MLVIEW_SERVICE ;
	gchar *service_name = NULL ;
	gint i = 0, j = 0 ;
	gchar tab[2] = {0, 0} ;
	gchar **services = NULL ;

	for (i = 0; i < MAX_NUM_SERVICES ; i++) {
		tab[0] = 'a' + i ;
		service_name = g_strdup_printf ("%s.%s", base_service_name,
		                                tab) ;
		if (!service_name) {
			status = MLVIEW_ERROR ;
			goto cleanup ;
		}
		if ( (status = mlview_ping_dbc_ping (a_this, service_name)) )
			continue ;

		if (!services) {
			services = g_try_malloc
			           (MAX_NUM_SERVICES * sizeof (gchar*)) ;
			if (!services) {
				mlview_utils_trace_debug ("malloc failed") ;
				status = MLVIEW_OUT_OF_MEMORY_ERROR ;
				goto cleanup ;
			}
			memset (services, 0, MAX_NUM_SERVICES) ;
		}
		services[j++] = service_name ;
	}
	if (j) {
		status = MLVIEW_OK ;
	}
	if (status == MLVIEW_OK) {
		*a_nb_of_services = j ;
		*a_active_services = services ;
		services = NULL ;
	}

cleanup:
	if (services) {
		mlview_ping_dbc_free_list_of_service_names (services, j) ;
		services = NULL ;
	}
	return status ;
}

enum MlViewStatus
mlview_ping_dbc_free_list_of_service_names (gchar **a_list,
        guint a_list_len)
{
	guint i = 0 ;
	for (i = 0; i < a_list_len ; i++) {
		if (a_list[i]) {
			g_free (a_list[i]) ;
			a_list[i] = NULL ;
		}
	}
	g_free (a_list) ;
	return MLVIEW_OK ;
}

enum MlViewStatus
mlview_ping_dbc_close_application (MlViewPingDBC *a_this,
                                   const gchar *a_service_name)
{
	DBusMessage *message=NULL, *reply = NULL ;
	DBusError dbus_error = {0} ;
	DBusConnection *dbus_connection = NULL ;
	enum MlViewStatus status = MLVIEW_OK ;

	g_return_val_if_fail (a_this && MLVIEW_IS_PING_DBC (a_this)
	                      && PRIVATE (a_this) && a_service_name,
	                      MLVIEW_BAD_PARAM_ERROR) ;

	dbus_error_init (&dbus_error) ;

	message = dbus_message_new_method_call
	          (a_service_name,
	           PATH_ORG_MLVIEW_PING_OBJECT,
	           INTERFACE_ORG_MLVIEW_PING_OBJECT_IFACE,
	           "close_application") ;
	if (!message) {
		mlview_utils_trace_debug ("could not instanciate a dbus message") ;
		status = MLVIEW_OUT_OF_MEMORY_ERROR ;
		goto cleanup ;
	}
	get_bus (a_this, &dbus_connection, NULL) ;
	if (!dbus_connection) {
		mlview_utils_trace_debug
		("could not connect to the session dbus") ;
		status = MLVIEW_ERROR ;
		goto cleanup ;
	}

	reply = dbus_connection_send_with_reply_and_block (dbus_connection,
	        message,
	        -1,
	        &dbus_error) ;
	if (!reply || dbus_error_is_set (&dbus_error)) {
		if (dbus_error_is_set (&dbus_error)) {
			mlview_utils_trace_debug
			("error while sending dbus message: %s\n",
			 dbus_error.message) ;
		} else {
			mlview_utils_trace_debug ("unknown error while "
			                          "sending dbus message") ;
		}
		status = MLVIEW_ERROR ;
	}
	status = MLVIEW_OK ;

cleanup:
	if (message) {
		dbus_message_unref (message) ;
		message = NULL ;
	}
	if (reply) {
		dbus_message_unref (reply) ;
		reply = NULL ;
	}
	return status ;
}
#endif /*MLVIEW_WITH_DBUS*/

