/* -*- Mode: C++; indent-tabs-mode: nil; c-basic-offset: 4-*- */

/*This file is part of GNU MlView
 *
 *GNU MlView is free software; 
 *you can redistribute it and/or modify it under the terms of 
 *the GNU General Public License as 
 *published by the Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope 
 *that it will be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */

#include "mlview-view-manager.h"
#include "mlview-exception.h"
#include "mlview-view-factory.h"
#include "mlview-gvc-factory.h"
#include "mlview-safe-ptr-utils.h"
#include "mlview-file-selection.h"

namespace mlview
{

typedef map<IView*, MlViewXMLDocument*> ViewToDocMap ;
typedef map<UString, IView*> StringToViewMap ;
typedef map<UString, gint> StringToIntMap ;
typedef map<MlViewXMLDocument*, struct DocumentWindowData*> DocToWindowDataMap ;
typedef list<IView*> ListOfViews ;
typedef map<MlViewXMLDocument*, ListOfViews*> DocToViewsMap ;

struct ViewManagerPriv
{
	/**
	*The set of instances of #MlViewXMLDocument
	*opened in the editor. The key is the pointer to 
	*the instance of #MlViewXMLDocument opened in the editor
	*The value is a an hash table that contains the 
	*instances of #IView that hold 
	*the key #MlViewXMLDocument.
	*/
	DocToViewsMap doc_to_views_map ;

	/**
	*A hash table where the keys are the instances of
	*IViews and the values are their associated document.
	*/
	ViewToDocMap view_to_doc_map ;

	/**
	 * A hash table where the keys are the instances
	 * of MlViewXMLDocument and the values are their
	 * matching DocumentWindowData, associated to the schema
	 * management window for each document.
	 */
	DocToWindowDataMap doc_to_schema_window_data_map ;

	/**
	 * A hash table where the keys are the instances of
	 * MlViewXMLDocument and the values are their matching
	 * DocumentWindowData, associated with the validation
	 * management window for each document.
	 */
	DocToWindowDataMap doc_to_validation_window_data_map ;

	/**
	*The current document being viewed by the user
	*/
	SafePtr<IView, ObjectRef, ObjectUnref> cur_view_ptr ;
	//IView* cur_view_ptr ;

	/**
	*a hash table which keys are the base name of 
	*the files already opened.
	*The associated data is the number 
	*of times the file name
	*has been opened.
	*/
	StringToIntMap base_name_to_occurence_map ;

	/**
	*a hash table wich keys are the file paths of 
	*the files already opened.
	*When destroying this hashtable do not 
	*destroy the referenced data (file path)
	*because file paths are hold by instances 
	*of MlViewXMLDocument and destroyed by them.
	*/
	StringToViewMap uri_to_view_map ;

	/**
	*An hash table that associates the opened document label names to
	*the matching opened view.
	*/
	StringToViewMap view_label_to_view_map ;

	/**
	 *Number of untitled document opened.
	 */
	guint untitled_docs_num;

	/**
	 *total number of docs opened
	 */
	guint opened_docs_num;

	//the graphical view container that
	//grapically manages the views
	SafePtr<GVCIface,ObjectRef, ObjectUnref> gvc_ptr ;

	sigc::signal0<void> signal_last_view_removed  ;
	sigc::signal1<void, IView*> signal_first_view_added ;

	ViewManagerPriv ():
		untitled_docs_num (0),
		opened_docs_num (0)
	{}
}
;//end ViewManagerPriv


ViewManager::ViewManager (const UString &a_gvc_type_name)
{
	m_priv = new ViewManagerPriv () ;
	THROW_IF_FAIL (m_priv) ;
	set_graphical_view_container (GVCFactory::create_gvc (a_gvc_type_name)) ;
}

ViewManager::ViewManager (const ViewManager &a_view_manager)
{
	//forbiden
}

Gtk::Widget*
ViewManager::get_embeddable_container_widget ()
{
	return m_priv->gvc_ptr->get_embeddable_container_widget () ;
}

ViewManager&
ViewManager::operator= (const ViewManager &a_view_manager)
{
	//forbiden
	return *this ;
}

IView*
ViewManager::get_cur_view ()
{
	return m_priv->cur_view_ptr ;
}

void
ViewManager::set_cur_view (IView *a_view)
{
	//setting cur view in gvc will
	//make it emit a signal_views_swapped
	//and that'll make us update m_priv->cur_view_ptr
	m_priv->cur_view_ptr = a_view ;
	return m_priv->gvc_ptr->set_cur_view (a_view, false) ;
}

ViewManager::~ViewManager ()
{
	if (m_priv) {
		delete m_priv ;
		m_priv = NULL ;
	}
}

IView*
ViewManager::create_new_view_for_document (MlViewXMLDocument *a_doc,
										   const UString &a_view_type)
{
	THROW_IF_FAIL (m_priv) ;
	THROW_IF_FAIL (MLVIEW_IS_XML_DOCUMENT (a_doc)) ;

	ViewDescriptor *view_desc = ViewFactory::peek_editing_view_descriptor
	    (a_view_type) ;
	if (!view_desc) {
	    view_desc = select_view_to_open () ;
	}
	THROW_IF_FAIL (view_desc) ;

	IView *result = ViewFactory::create_view
	                (a_doc, view_desc->view_type_name, NULL) ;
	THROW_IF_FAIL (result) ;

	result->set_desc_type_name (view_desc->view_type_name) ;
	return NULL ;
}

enum MlViewStatus
ViewManager::insert_view (IView *a_view,
                          long a_index)
{
	UString file_uri, file_path, base_name ;
	MlViewFileDescriptor *file_desc = NULL;
	gchar  *cstr = NULL ;
	MlViewXMLDocument *mlview_xml_document = NULL;
	DocToViewsMap it ;
	bool is_new_doc_tree = true;
	gboolean is_local = false;
	Gtk::Widget *view_impl = NULL ;
	UString label_str ;


	THROW_IF_FAIL  (m_priv != NULL);
	THROW_IF_FAIL (a_view != NULL);
	THROW_IF_FAIL (m_priv->gvc_ptr) ;

	view_impl = (Gtk::Widget*) a_view ;
	THROW_IF_FAIL (view_impl) ;

	mlview_xml_document = a_view->get_document () ;
	THROW_IF_FAIL (mlview_xml_document) ;

	file_desc = mlview_xml_document_get_file_descriptor
	            (mlview_xml_document) ;
	if (file_desc) {
		cstr = (gchar*)
		       mlview_file_descriptor_get_uri
		       (file_desc); //used for storing name of opened documents
		file_uri = cstr ;
		g_free (cstr);
		cstr = NULL ;

		cstr = (gchar*) mlview_file_descriptor_get_file_path (file_desc) ;
		file_path = cstr ;
		g_free (cstr);
		cstr = NULL ;

		//use filename for local files
		if (!mlview_file_descriptor_is_local (file_desc, &is_local) ) {
			if (is_local)
				file_path = g_path_get_basename
				            (file_path.c_str ());
		}
	}
	/*
	 *check if a view on the same document has been added to
	 *the editor already.
	 */
	DocToViewsMap::iterator it2 = m_priv->doc_to_views_map.find
	                              (mlview_xml_document) ;
	if (it2 != m_priv->doc_to_views_map.end ()) {
		is_new_doc_tree = FALSE ;
	}

	if (file_path == "") {
		gchar *tmp_str = NULL;
		UString label_str ;

		if (is_new_doc_tree == TRUE)
			m_priv->untitled_docs_num++;
		tmp_str =
		    g_strdup_printf
		    ("%d",
		     m_priv->untitled_docs_num);

		label_str += "Untitled Document" ;
		label_str += tmp_str ;
		g_free (tmp_str);
		tmp_str = NULL ;
		a_view->set_view_name (label_str) ;
	} else {

		gint base_name_nb = 0;

		gboolean file_is_already_opened = FALSE;

		/* crop name for long uris */
		base_name = file_path ;
		if (base_name.length () > 23) {
			base_name.erase (20) ;
			base_name += "..." ;
		}

		if (is_new_doc_tree
		        && (m_priv->uri_to_view_map.find (file_uri)
		            != m_priv->uri_to_view_map.end ())
		   ) {
			/*
			 *There is an xml document coming from the
			 *same url that is opened already. 
			 *So, reopen it.
			 *That is, remove it previous 
			 *instance and add
			 *the new one.
			 */
			typedef Glib::ListHandle<Gtk::Widget*> Widgets ;

			/*
			 *get the old label string because 
			 *this document
			 *must have the same label as 
			 *the one alreay opened
			 */
			UString old_label_text = a_view->get_view_name ();

			/*old_label_str_tmp belongs to label,
			 *and will be freed by him
			 */
			remove_view (a_view);

			m_priv->view_label_to_view_map.insert
			(StringToViewMap::value_type (old_label_text,
			                              a_view)) ;
			file_is_already_opened = TRUE;
		} else if (m_priv->base_name_to_occurence_map.find (base_name)
		           == m_priv->base_name_to_occurence_map.end ()) {
			/*
			 *It is the first time a 
			 *document with this basename is opened
			 */
			base_name_nb = 1;

		} else if (m_priv->base_name_to_occurence_map.find (base_name)
		           != m_priv->base_name_to_occurence_map.end ()) {
			/*
			 *some documents with the this basename 
			 *are already opened
			 */
			base_name_nb = m_priv->base_name_to_occurence_map.find
			               (base_name)->second ;
			if (!is_new_doc_tree)
				base_name_nb++;
		}

		m_priv->base_name_to_occurence_map.insert
		(StringToIntMap::value_type (base_name, base_name_nb)) ;

		m_priv->uri_to_view_map.insert
		(StringToViewMap::value_type (file_uri, a_view)) ;
		if (base_name_nb > 1) {
			gchar *tmp_str = NULL ;
			UString str  ;
			while (1) {
				tmp_str =
				    g_strdup_printf
				    ("%d", base_name_nb);
				label_str = base_name ;
				label_str += "<" ;
				label_str += tmp_str ;
				label_str += ">" ;

				if (tmp_str) {
					g_free (tmp_str) ;
					tmp_str = NULL ;
				}

				if (m_priv->view_label_to_view_map.find (label_str)
				        != m_priv->view_label_to_view_map.end ()) {
					base_name_nb++ ;
					continue ;
				}
				break;
			}
			a_view->set_view_name (label_str) ;
			m_priv->view_label_to_view_map.insert
			(StringToViewMap::value_type (label_str, a_view)) ;
		} else if (file_is_already_opened == FALSE) {
			a_view->set_view_name (base_name) ;
			m_priv->view_label_to_view_map.insert
			(StringToViewMap::value_type (base_name, a_view)) ;
		}
	}

	/*update the view->document map*/
	m_priv->view_to_doc_map.insert
	(ViewToDocMap::value_type (a_view, mlview_xml_document)) ;

	/*update the document->views map*/
	it2 = m_priv->doc_to_views_map.find (mlview_xml_document) ;
	ListOfViews *views = NULL;
	if (it2 == m_priv->doc_to_views_map.end ()) {
		views = new ListOfViews () ;
		m_priv->doc_to_views_map.insert
		(DocToViewsMap::value_type (mlview_xml_document,
		                            views)) ;
	} else {
		views = it2->second ;
	}
	views->push_back (a_view) ;

	if (is_new_doc_tree == TRUE)
		m_priv->opened_docs_num++;

	//now, visually add the view
	m_priv->gvc_ptr->insert_view (a_view, a_index) ;

	if ( m_priv->view_to_doc_map.size () == 1) {
		signal_first_view_added ().emit (a_view) ;
		//m_priv->gvc_ptr->signal_views_swapped ().emit (a_view, NULL) ;
	}
	return MLVIEW_OK ;
}

enum MlViewStatus
ViewManager::remove_view (IView *a_view)
{
	MlViewFileDescriptor *file_desc = NULL;
	MlViewXMLDocument *mlview_xml_doc = NULL;
	gboolean doc_to_be_closed = FALSE;
	gchar *file_path = NULL, *file_uri = NULL, *base_name = NULL ;
	SafePtr<IView, ObjectRef, ObjectUnref> view_ptr ;

	THROW_IF_FAIL (m_priv != NULL);
	THROW_IF_FAIL (m_priv->gvc_ptr != NULL) ;
	THROW_IF_FAIL (a_view != NULL);
	view_ptr = a_view ;

	mlview_xml_doc = view_ptr->get_document () ;
	THROW_IF_FAIL (mlview_xml_doc);

	file_desc = mlview_xml_document_get_file_descriptor
	            (mlview_xml_doc) ;
	if (file_desc) {
		file_uri = (gchar*)
		           mlview_file_descriptor_get_uri (file_desc);
		file_path = (gchar*) mlview_file_descriptor_get_file_path
		            (file_desc);
	}
	if (file_path != NULL)
		base_name = (gchar *) g_basename ((const gchar*)file_path);

	UString label_str = view_ptr->get_view_name () ;

	//*****************************
	//removes a_view from the map of
	//the opened document views.
	//and from the opened_file_paths map.
	//**********************************
	m_priv->view_to_doc_map.erase (view_ptr) ;
	DocToViewsMap::iterator doc_to_views_it ;
	ListOfViews *views_of_doc = NULL;
	doc_to_views_it =
	    m_priv->doc_to_views_map.find (mlview_xml_doc) ;
	if (doc_to_views_it
	        != m_priv->doc_to_views_map.end ()) {
		views_of_doc = doc_to_views_it->second ;
		views_of_doc->remove
		(view_ptr) ;
	} else {
		THROW ("A doc doesn't have any view ? hugh weird") ;
	}

	//now remove the view from the graphical view container
	m_priv->gvc_ptr->remove_view (view_ptr) ;

	if (views_of_doc->size () == 0) {
		/*
		 *no views are opened on the current 
		 *doc anymore=>close the doc
		 */
		m_priv->doc_to_views_map.erase (mlview_xml_doc) ;

		if (file_uri != NULL) {
			m_priv->uri_to_view_map.erase (file_uri) ;
		}

		doc_to_be_closed = TRUE;

		m_priv->opened_docs_num--;
		delete views_of_doc ;
		views_of_doc = NULL ;
	}

	if (doc_to_be_closed) {

		//***************************************
		// remove the entry in the
		// opened_document_label_names hash table
		//****************************************
		m_priv->view_label_to_view_map.erase (label_str) ;
	}

	//******************************************************************
	//if there are several docs that have the save base name as this one,
	//decrement their number, and if the number reaches 0,
	//remove the entry matching this base name
	//from the hash table.
	//******************************************************************
	if (doc_to_be_closed == TRUE && file_path != NULL) {
		gint occurence = 0 ;
		StringToIntMap::iterator it ;

		it = m_priv->base_name_to_occurence_map.find
		     (base_name) ;
		if (it != m_priv->base_name_to_occurence_map.end ())
			occurence= it->second ;
		occurence--;

		m_priv->base_name_to_occurence_map.erase (base_name) ;
		if (occurence> 0) {
			m_priv->base_name_to_occurence_map.insert
			(StringToIntMap::value_type (base_name,
			                             occurence)) ;
		}
	} else if (doc_to_be_closed == TRUE && !file_path) {
		m_priv->untitled_docs_num--;
	}

	if (m_priv->view_to_doc_map.size () == 0) {
		signal_last_view_removed ().emit () ;
	}
	view_ptr->unref () ;
	return MLVIEW_OK ;
}

list<IView*>
ViewManager::get_views_of_document (const MlViewXMLDocument *a_doc) const
{
	THROW_IF_FAIL (a_doc) ;
	list<IView*> *result = 0;

	DocToViewsMap::iterator it = m_priv->doc_to_views_map.find
	                             (const_cast<MlViewXMLDocument*>(a_doc))  ;

	if (it == m_priv->doc_to_views_map.end ()) {
		return list<IView*> ();
	}
	result = it->second ;
	return *result ;
}

list<IView*>
ViewManager::get_all_views (void) const
{
	list<IView*> result ;

	ViewToDocMap::iterator it ;
	for (it = m_priv->view_to_doc_map.begin ();
	        it != m_priv->view_to_doc_map.end ();
	        ++it) {
		result.push_back (it->first) ;
	}
	return result ;
}

gint
ViewManager::get_number_of_views_opened_with_doc (MlViewXMLDocument *a_doc) const
{
	THROW_IF_FAIL (m_priv) ;
	THROW_IF_FAIL (a_doc) ;

	DocToViewsMap::iterator it ;
	it = m_priv->doc_to_views_map.find (a_doc) ;
	if (it == m_priv->doc_to_views_map.end ())
		return 0 ;
	list<IView*> *views = it->second ;
	return views->size () ;
}

gint
ViewManager::get_number_of_open_documents () const
{
	THROW_IF_FAIL (m_priv) ;

	return m_priv->doc_to_views_map.size () ;
}

std::list<MlViewXMLDocument*>
ViewManager::get_list_of_open_documents () const
{
	THROW_IF_FAIL (m_priv) ;

	THROW_IF_FAIL (m_priv) ;

	DocToViewsMap::iterator it ;
	list<MlViewXMLDocument*>docs ;

	for (it = m_priv->doc_to_views_map.begin ();
	        it != m_priv->doc_to_views_map.end ();
	        ++it) {
		docs.push_back (it->first) ;
	}
	return docs;
}

bool
ViewManager::view_exists (IView *a_view) const
{
	THROW_IF_FAIL (m_priv) ;

	if (m_priv->view_to_doc_map.find (a_view)
	        == m_priv->view_to_doc_map.end ())
		return false ;
	else
		return true ;
}

//**************
//signals
//***************

sigc::signal0<void>&
ViewManager::signal_last_view_removed ()
{
	THROW_IF_FAIL (m_priv) ;

	return m_priv->signal_last_view_removed ;
}

sigc::signal1<void, IView*>&
ViewManager::signal_first_view_added ()
{
	THROW_IF_FAIL (m_priv) ;

	return m_priv->signal_first_view_added ;
}

//****************
//protected methods
//****************

ViewDescriptor*
ViewManager::select_view_to_open (void)
{

	GtkWidget *dialog = NULL ;
	GtkWidget *dialog_vbox = NULL ;
	GtkWidget *hbox1 = NULL ;
	GtkWidget *label1 = NULL ;
	GtkWidget *option_menu = NULL ;
	GtkWidget *menu = NULL ;
	GtkWidget *menu_item = NULL ;
	GtkWidget *dialog_action_area1 = NULL ;
	GtkWidget *button5 = NULL ;
	GtkWidget *button6 = NULL ;
	GtkWidget *sel_menu_item = NULL ;

	int button;
	struct ViewDescriptor *result = NULL;
	ViewDescriptor const *view_desc_ptr = NULL ;
	gchar *base_name = NULL;
	guint nr_view_desc = 0 ;

	nr_view_desc = ViewFactory::get_number_of_view_desc () ;
	THROW_IF_FAIL (nr_view_desc) ;

	/*
	 *If there is only one type of view registered
	 *in the system, get its view descriptor and use that
	 *one.
	 */
	if (nr_view_desc == 1) {
		result = ViewFactory::get_view_descriptor_at (0) ;
		THROW_IF_FAIL (result) ;
		return result ;
	}

	dialog = gtk_dialog_new ();
	gtk_window_set_title (GTK_WINDOW (dialog), _("Select View"));

	dialog_vbox = GTK_DIALOG (dialog)->vbox;
	gtk_widget_show (dialog_vbox);

	hbox1 = gtk_hbox_new (FALSE, 0);
	gtk_widget_show (hbox1);
	gtk_box_pack_start (GTK_BOX (dialog_vbox), hbox1, TRUE, TRUE, 0);
	label1 = gtk_label_new (_("Select view to open"));
	gtk_widget_show (label1);
	gtk_box_pack_start (GTK_BOX (hbox1), label1, FALSE, FALSE, 10);

	/* build select view menu */
	option_menu = gtk_option_menu_new();
	menu = gtk_menu_new();
	gtk_option_menu_set_menu (GTK_OPTION_MENU(option_menu), menu);

	gtk_widget_show (menu);
	gtk_widget_show (option_menu);

	gtk_box_pack_start (GTK_BOX (hbox1), option_menu, TRUE, TRUE, 0);
	for (view_desc_ptr = ViewFactory::get_view_descriptors ();
	        view_desc_ptr && view_desc_ptr->view_type_name;
	        view_desc_ptr ++) {
		base_name = view_desc_ptr->view_type_name;
		menu_item = gtk_menu_item_new_with_label(base_name);
		gtk_menu_shell_append (GTK_MENU_SHELL(menu),  menu_item);
		gtk_widget_show(menu_item);
		g_object_set_data (G_OBJECT(menu_item),
		                   "mlview_view_desc", (gpointer)view_desc_ptr);
	}
	gtk_option_menu_set_history(GTK_OPTION_MENU(option_menu), 0);

	/* dialog box buttons */
	dialog_action_area1 = GTK_DIALOG (dialog)->action_area;
	gtk_widget_show (dialog_action_area1);
	gtk_button_box_set_layout (GTK_BUTTON_BOX (dialog_action_area1),
	                           GTK_BUTTONBOX_END);

	button5 = gtk_button_new_from_stock ("gtk-cancel");
	gtk_widget_show (button5);
	gtk_dialog_add_action_widget (GTK_DIALOG (dialog),
	                              button5, GTK_RESPONSE_CANCEL);
	GTK_WIDGET_SET_FLAGS (button5, GTK_CAN_DEFAULT);

	button6 = gtk_button_new_from_stock ("gtk-ok");
	gtk_widget_show (button6);
	gtk_dialog_add_action_widget (GTK_DIALOG (dialog),
	                              button6, GTK_RESPONSE_OK);
	GTK_WIDGET_SET_FLAGS (button6, GTK_CAN_DEFAULT);

	/* show dialog */
	button = gtk_dialog_run (GTK_DIALOG (dialog));


	switch (button) {
	case GTK_RESPONSE_OK:
		sel_menu_item = gtk_menu_get_active(GTK_MENU(menu));

		result = (ViewDescriptor *)g_object_get_data
		         (G_OBJECT(sel_menu_item), "mlview_view_desc");
		break;

	default:
		result = NULL ;
		break;
	}
	gtk_widget_destroy (dialog);
	return result;
}

enum MlViewStatus
ViewManager::set_graphical_view_container (GVCIface *a_gvc)
{
	m_priv->gvc_ptr = a_gvc ;

	m_priv->gvc_ptr->signal_views_swapped ().connect
	(sigc::mem_fun (*this, &ViewManager::on_views_swapped)) ;

	return MLVIEW_OK ;
}

void
ViewManager::on_views_swapped (IView* a_swapped_in, IView* a_swapped_out)
{
	SafePtr<IView, ObjectRef, ObjectUnref> doc_view_ptr, prev_view_ptr ;

	AppContext *ctxt = AppContext::get_instance () ;
	THROW_IF_FAIL (ctxt) ;

	prev_view_ptr = a_swapped_out ;
	doc_view_ptr = a_swapped_in ;

	/*
	 *emit the "is-swapped-out" signal.
	 *on the previous view and the
	 *"is-swapped-in" signal on the current view.
	 */
	if (prev_view_ptr && doc_view_ptr)
		prev_view_ptr->notify_swapped_out () ;

	if (doc_view_ptr && doc_view_ptr != prev_view_ptr)
		doc_view_ptr->notify_swapped_in () ;

	/*
	 *notify the application that a new view became the
	 *current view.
	 */
	ctxt->notify_view_swapped (prev_view_ptr, doc_view_ptr) ;
	set_cur_view (a_swapped_in) ;

	/*
	 * notify  the app that the undo state changed
	 * this way, the app can update the can undo/can redo arrows
	 * in the menu bar, for example
	 */
	ctxt->notify_view_undo_state_changed () ;
}
}//namespace mlview

