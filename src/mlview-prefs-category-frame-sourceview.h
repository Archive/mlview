/* -*- Mode: C++; indent-tabs-mode: true; c-basic-offset: 4 -*- */
/*
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute 
 *it and/or modify it under the terms of 
 *the GNU General Public License as published by the 
 *Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYING file for copyright information.
 */
#ifndef MLVIEW_PREFS_CATEGORY_FRAME_SOURCEVIEW_H
#define MLVIEW_PREFS_CATEGORY_FRAME_SOURCEVIEW_H

#include "mlview-prefs-category-frame.h"

namespace mlview
{

struct PrefsCategoryFrameSourceViewPriv;


class PrefsCategoryFrameSourceView : public PrefsCategoryFrame
{
private:
    friend struct PrefsCategoryFrameSourceViewPriv;
    PrefsCategoryFrameSourceViewPriv *m_priv;

	//forbid assignation/copy
	PrefsCategoryFrameSourceView (PrefsCategoryFrameSourceView const&) ;
	PrefsCategoryFrameSourceView& operator= (PrefsCategoryFrameSourceView const&);

public:
///
/// Default constructor
///
    PrefsCategoryFrameSourceView ();
///
/// Default destructor
///
    virtual ~PrefsCategoryFrameSourceView ();
};

} // namespace mlview


#endif // MLVIEW_PREFS_CATEGORY_FRAME_SOURCEVIEW_H
