/* -*- Mode: C; indent-tabs-mode: true; c-basic-offset: 4-*- */

/*
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute 
 *it and/or modify it under the terms of 
 *the GNU General Public License as published by the 
 *Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file copyright information.
 */

/**
 *@file
 *The place where MlView application is launched.
 *Here the command line is parsed, and mlview high level classes
 *are instanciated.
 *
 */

#include <locale.h>
#include <iostream>
#include <gtkmm.h>
#include <gnome.h>
#include <libxml/parser.h>
#include <libxml/catalog.h>

#include "mlview-app.h"
#include "mlview-exception.h"
#include "mlview-service.h"
#include "mlview-safe-ptr-utils.h"

using namespace std ;
using namespace mlview ;

/**
 *MlView main entry point.
 */
int
main (int argc, char **argv)
{
	GnomeProgram *mlview_program = NULL;
	gboolean show_version = FALSE, show_info = FALSE;
	mlview::UString associated_dtd ;
	gchar *bare_str = NULL;
	poptContext popt_context = NULL;
	gchar **args = NULL;
#ifndef MLVIEW_DEBUG
	try {
#endif
		/*init gnome */
		Gtk::Main kit (argc, argv) ;

		struct poptOption cmd_options_table[] = {
			                                        {
				                                        "version",
				                                        'v',
				                                        POPT_ARG_NONE,
				                                        &show_version,
				                                        0,
				                                        N_
				                                        ("Prints The version of gnome-mlview you are using"),
				                                        NULL
			                                        },
			                                        {
			                                            "about",
			                                            'a',
			                                            POPT_ARG_NONE,
			                                            &show_info,
			                                            0,
			                                            N_
			                                            ("Prints some information about gnome-mlview"),
			                                            NULL},
			                                        {
			                                            "dtd",
			                                            'd',
			                                            POPT_ARG_STRING,
			                                            &bare_str,
			                                            0,
			                                            N_
			                                            ("Associates DTD with the XML documents on the commandline"),
			                                            NULL},
			                                        {0}
		                                        };

		/*init gnome */
		mlview_program = gnome_program_init
				(PACKAGE, VERSION, LIBGNOMEUI_MODULE, argc, argv,
				 GNOME_PARAM_POPT_TABLE, cmd_options_table,
				 GNOME_PARAM_APP_DATADIR, DATADIR,
				 GNOME_PARAM_APP_LIBDIR, LIBDIR,
				 GNOME_PARAM_NONE);

		/*init underlaying facilities */
		mlview_utils_init ();

		g_object_get (G_OBJECT (mlview_program),
		              GNOME_PARAM_POPT_CONTEXT,
		              &popt_context,
		              NULL);
		THROW_IF_FAIL (popt_context) ;

		/*Then, parse the non option command arguments */
		args = (gchar **) poptGetArgs (popt_context);

		if (show_version == TRUE) {
			cout <<  VERSION << endl ;
			poptFreeContext (popt_context);
			return 0;
		}

		if (show_info == TRUE) {
			cout <<   _("No useful info yet") << endl ;
			poptFreeContext (popt_context);
			return 0;
		}

		if (bare_str)
			associated_dtd = bare_str ;

		if (associated_dtd != "") {
			cerr << "DTD: " << associated_dtd << endl ;
		}

		/*instanciate an mlview application */
		SafePtr<App, ObjectRef, ObjectUnref> mlview_application =
		    new App (PACKAGE) ;

		/* Bootstrap facilities */
		mlview_service_start (mlview_application, NULL) ;

		/* Make window visible now */
		mlview_application->set_visible ();
		if (args) {
			/*
			 *they are some argument on the comment line. Treat them 
			 *like files to open
			 */
			mlview::Editor *mlview_editor =
			    mlview_application->get_editor ();

			if (mlview_editor) {
				int i;

				for (i = 0; args[i] != NULL; i++) {
					mlview_editor->load_xml_file_with_dtd
					((gchar *) args[i],
					 associated_dtd,
					 TRUE);
				}
			}
		}
		poptFreeContext (popt_context);
		kit.run ();
		mlview_utils_cleanup ();

		return 0;
#ifndef MLVIEW_DEBUG
	} catch (exception e) {
		TRACE_EXCEPTION (e) ;
		return -1 ;
	} catch (...) {
		LOG_TO_ERROR_STREAM ("caught unknown exception") ;
		return -1  ;
	}
#endif
}
