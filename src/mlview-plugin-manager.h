/* -*- Mode: C++; indent-tabs-mode: true; c-basic-offset: 4-*- */

/*
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute it 
 *and/or modify it under the terms of 
 *the GNU General Public License as published 
 *by the Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *MlView is distributed in the hope that it will be useful, 
 *but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. If not, write to the Free Software Foundation, Inc., 
 *59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */

#ifndef __MLVIEW_PLUGIN_MANAGER_H__
#define __MLVIEW_PLUGIN_MANAGER_H__

#include "mlview-plugin.h"
#include "mlview-safe-ptr-utils.h"

/**
 *@file
 *The declaration of #PluginManager class.
 */

namespace mlview
{

struct PluginManagerPriv ;

class PluginManager : public mlview::Object
{
	friend struct PluginManagerPriv ;
	PluginManagerPriv *m_priv ;

protected:

public:
	PluginManager () ;

	virtual ~PluginManager () ;
	const Plugin *load_plugin (const PluginDescriptor &a_descr) ;
	const Plugin *load_plugin (const UString &a_url) ;
	const map<UString, Plugin*>& get_map_of_plugins () const;
	void load_all_plugins_from_default_plugins_dir () ;

	//**************************
	// static members
	//**************************
	static void get_available_plugins (list<UString> &a_dir_names,
	                                   list<PluginDescriptor> &a_list) ;
	static void get_available_plugins (const UString &a_dir_name,
	                                   list<PluginDescriptor>&a_list) ;
	//**************************
	// signals
	//**************************
	sigc::signal1<void, Plugin*> signal_plugin_loaded ;
	sigc::signal1<void, Plugin*> signal_plugin_unloaded ;
}
; // end class mlview::PluginManager
} // end namespace mlview

#endif /* __MLVIEW_PLUGIN_MANAGER_H__ */
