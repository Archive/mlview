/* -*- Mode: C++; indent-tabs-mode: true; c-basic-offset: 4 -*- */
/*
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute 
 *it and/or modify it under the terms of 
 *the GNU General Public License as published by the 
 *Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYING file for copyright information.
 */
#ifndef MLVIEW_PREFS_CATEGORY_SOURCEVIEW_H
#define MLVIEW_PREFS_CATEGORY_SOURCEVIEW_H

#include <gtkmm.h>

#include "mlview-prefs-storage-manager.h"
#include "mlview-prefs-category.h"


namespace mlview
{

struct PrefsCategorySourceViewPriv;


///
/// This class is a view on SourceView-specific preferences
///
class PrefsCategorySourceView : public PrefsCategory
{
  private:
    friend struct PrefsCategorySourceViewPriv;
    PrefsCategorySourceViewPriv *m_priv;

	//forbid copy/assignation
	PrefsCategorySourceView (PrefsCategorySourceView const&) ;
	PrefsCategorySourceView& operator= (PrefsCategorySourceView const&) ;

  public:
///
/// Constructor for type PrefsCategorySourceView
///
/// \param storage_manager a pointer to the PrefsStorageManager to use for
/// accessing preferences
///
    PrefsCategorySourceView (PrefsStorageManager *storage_manager);
///
/// Default destructor
///
    virtual ~PrefsCategorySourceView ();

///
/// Return the default value of the 'show-line-numbers' property
///
/// \return TRUE if line numbers are shown by default, FALSE otherwise
///
    bool show_line_numbers_default ();
///
/// Return the value for the 'show-line-numbers' property
///
/// \return TRUE if line numbers are shown, FALSE otherwise
///
    bool show_line_numbers ();
///
/// Set the value of the 'show-line-numbers' property
/// When set to true, SourceView will show line numbers in its left margin
///
/// \param show Whether to show line number or not
///
    void set_show_line_numbers (bool show);


///
/// Return the default value for the 'tabs-width' property
///
/// \return the default number of characters in a tabulation
///
    int get_tabs_width_default ();
///
/// Return the value for the 'tabs-width' property
///
/// \return the number of characters in a tabulation
///
    int get_tabs_width ();
///
/// Set the value for the 'tabs-width' property
///
/// \param nchars the number of characters to use for a tabulation
///
    void set_tabs_width (int nchars);


///
/// Return the default value for the 'replace-tabs' property
///
/// \return TRUE if, by default, tabulations should be replaced by spaces
///
    bool replace_tabs_with_spaces_default ();
///
/// Return the value for the 'replace-tabs' property
///
/// \return TRUE if tabulations should be replaced by spaces
///
    bool replace_tabs_with_spaces ();
///
/// Set the value for the 'replace-tabs' property
///
/// \param replace Whether to replace tabulations with spaces or not
///
    void set_replace_tabs_with_spaces (bool replace);


///
/// Return the default value for the 'auto-indent' property
///
/// \return TRUE if, by default, text should be automatically indented in
/// source view
///
    bool auto_indent_default ();
///
/// Return the value for the 'auto-indent' property
///
/// \return TRUE if text should be automatically indented in source view
///
    bool auto_indent ();
///
/// Set the value for the 'auto-indent' property
///
/// \param autoindent Whether text should be automatically indented or not
///
    void set_auto_indent (bool autoindent);


///
/// Return the default value for the 'show-margin' property
///
/// \return TRUE if the margin should be displayed by default
///
    bool show_margin_default ();
///
/// Return the value for the 'show-margin' property
///
/// \return TRUE if the margin should be displayed
///
    bool show_margin ();
///
/// Set the value for the 'show-margin' property
///
/// \param show Whether to show the margin or not
///
    void set_show_margin (bool show);


///
/// Return the default value for the 'margin-position' property
///
/// \return the default position (in number of characters) of the margin in
/// the source view
///
    int get_margin_position_default ();
///
/// Return the value for the 'margin-position' property
///
/// \return the position (in number of characters) of the margin in
/// the source view
///
    int get_margin_position ();
///
/// Set the value for the 'margin-position' property
///
/// \param position where to place margin in the source view
///
    void set_margin_position (int position);


///
/// Return the default value for the 'fontname' property
///
/// \return the name of the default font to use
///
    UString get_default_font_name ();
///
/// Return the value for the 'fontname' property
///
/// \return the name of the font to use
///
    UString get_font_name ();
///
/// Set the value for the 'fontname' property
///
/// \param fontname the name of the font to use
///
    void set_font_name (const UString& fontname);


    sigc::signal0<void>& signal_show_line_number_changed ();
    sigc::signal0<void>& signal_tabs_width_changed ();
    sigc::signal0<void>& signal_replace_tabs_changed ();
    sigc::signal0<void>& signal_auto_indent_changed ();
    sigc::signal0<void>& signal_show_margin_changed ();
    sigc::signal0<void>& signal_margin_position_changed ();
    sigc::signal0<void>& signal_font_name_changed ();
};

}


#endif // MLVIEW_PREFS_CATEGORY_SOURCEVIEW_H
