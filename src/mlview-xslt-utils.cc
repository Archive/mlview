/* -*- Mode: C++; indent-tabs-mode: nil; c-basic-offset: 4-*- */

/*
 *This file is part of MlView
 *
 *MlView is free software; you can redistribute it and/or 
 *modify it under the terms of 
 *the GNU General Public License as published 
 *by the Free Software Foundation; either version 2
 *or (at your option) any later version.
 *
 *MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView.
 *see the file COPYING. 
 *If not, write to the 
 *Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */

#include <stdio.h>
#include <string.h>
#include <libxslt/xslt.h>
#include <libxslt/xsltInternals.h>
#include <libxslt/transform.h>
#include "mlview-editor.h"
#include "mlview-utils.h"
#include "mlview-xml-document.h"
#include "mlview-xslt-utils.h"

/**
 *@file 
 *The mlview xslt transformer
 * 
 *This is a wrapper of the libxslt from Daniel Veillard.
 *It provides an interface to apply a xslt stylesheet to 
 *xml document opened in mlview. 
 *
 */


namespace mlview
{

/**
 * Checks whether a mlview document is an xslt stylesheet
 * The test is performed on the namespaces declared on
 * the root node of the document 
 *@param mlv_xml_doc : the document to test
 *@return TRUE if the document is a xslt stylesheet,
 *FALSE otherwise.
 */
gboolean
xslt_utils_is_xslt_doc (MlViewXMLDocument* mlv_xml_doc)
{
	xmlDocPtr xml_doc;
	xmlNodePtr root_node;

	xmlNs *cur_ns = NULL ;
	gint found=FALSE;

	g_return_val_if_fail (mlv_xml_doc, FALSE) ;

	xml_doc = mlview_xml_document_get_native_document(mlv_xml_doc);
	root_node = xmlDocGetRootElement (xml_doc);
	for (cur_ns = root_node->nsDef ;
	        !found && cur_ns;
	        cur_ns = cur_ns->next) {
		found = (gboolean)(!xmlStrcmp(cur_ns->href,
		                              (xmlChar*)XSLT_NAMESPACE_URI));
	}
	return found;
}


/**
 *Applies a xslt stylesheet to a xml document
 *at libxml level
 *@param src_doc : the libxml2 xmlDocPtr to the source doc
 *@param xsl_doc : the libxml2 xmlDocPtr to the xslt stylesheet
 *@return the xmlDocPtr to the result of the transformation
 *
 */
static xmlDocPtr
xslt_utils_do_transform (xmlDocPtr src_doc,
                         xmlDocPtr xsl_doc)
{
	xmlDocPtr res_doc;
	xmlDocPtr xsl_copy_doc;
	xsltStylesheetPtr xsl_stylesheet=NULL;
	const char *params[16+1];
	params[0] = NULL;

	/* makes a copy of the stylesheet */
	xsl_copy_doc = xmlCopyDoc (xsl_doc, 1);
	xsl_stylesheet = xsltParseStylesheetDoc (xsl_copy_doc);
	res_doc = xsltApplyStylesheet(xsl_stylesheet, src_doc, params);
	xsltFreeStylesheet(xsl_stylesheet);
	return res_doc;
}


/**
 *Applies a xslt stylesheet to a xml document
 *at mlview level
 *@param src_doc : the libxml2 xmlDocPtr to the source doc
 *@param xsl_doc : the libxml2 xmlDocPtr to the xslt stylesheet
 *@return the xmlDocPtr to the result of the transformation
 *
 */
static MlViewXMLDocument*
xslt_utils_do_mlview_transform (MlViewXMLDocument* mlv_src_doc,
                                MlViewXMLDocument* mlv_xsl_doc)
{
	MlViewXMLDocument* mlv_res_doc = NULL;
	xmlDocPtr src_doc, xsl_doc, res_doc;

	mlview::AppContext *context = mlview::AppContext::get_instance () ;
	THROW_IF_FAIL (context) ;

	src_doc = mlview_xml_document_get_native_document (mlv_src_doc);
	xsl_doc = mlview_xml_document_get_native_document (mlv_xsl_doc);
	res_doc = xslt_utils_do_transform (src_doc, xsl_doc);
	if (res_doc != NULL) {
		mlv_res_doc = mlview_xml_document_new (res_doc);
	} else {
		context->error ("%s", _("XSLT transformation failed"));
	}

	return mlv_res_doc;
}




/*=================================================
 *public methods
 *=================================================*/


/**
 *Applies a XSLT stylesheet to an mlview document
 *
 *@param src_doc the source mlview xml document
 *@param xsl_doc the xslt mlview xml document
 *@result the document resuting of the transformation
 * or NULL if it fails
 *
 */
MlViewXMLDocument *
xslt_utils_transform_document (MlViewXMLDocument *src_doc,
                               MlViewXMLDocument *xsl_doc)
{
	MlViewXMLDocument *res_doc = NULL;
	g_return_val_if_fail (src_doc != NULL, NULL);
	g_return_val_if_fail (MLVIEW_IS_XML_DOCUMENT (src_doc),
	                      NULL);
	g_return_val_if_fail (xsl_doc != NULL, NULL);
	g_return_val_if_fail (MLVIEW_IS_XML_DOCUMENT (xsl_doc),
	                      NULL);
	res_doc = xslt_utils_do_mlview_transform(src_doc, xsl_doc);
	return res_doc;
}

}// namespace mlview
