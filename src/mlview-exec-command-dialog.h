/* -*- Mode: C++; indent-tabs-mode:nil; c-basic-offset: 4-*- */

/*
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute 
 *it and/or modify it under the terms of 
 *the GNU General Public License as published by the 
 *Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */
#ifndef __MLVIEW_EXEC_COMMAND_DIALOG_H__
#define __MLVIEW_EXEC_COMMAND_DIALOG_H__

#include "mlview-object.h"

namespace mlview
{

struct ExecCommandDialogPriv;

class ExecCommandDialog : public mlview::Object
{
    friend struct ExecCommandDialogPriv;
    ExecCommandDialogPriv *m_priv;
	//forbid copy/assignation
	ExecCommandDialog (ExecCommandDialog const&) ;
	ExecCommandDialog& operator= (ExecCommandDialog const&) ;

public:
    ExecCommandDialog (const gchar* filename);
    virtual ~ExecCommandDialog ();

};

} // namespace mlview

#endif // __MLVIEW_EXEC_COMMAND_DIALOG_H__
