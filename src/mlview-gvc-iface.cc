/* -*- Mode: C++; indent-tabs-mode: nil; c-basic-offset: 4-*- */

/**
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute it 
 *and/or modify it under the terms of 
 *the GNU General Public License as published 
 *by the Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *MlView is distributed in the hope that it will be useful, 
 *but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. If not, write to the Free Software Foundation, Inc., 
 *59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */
#include "mlview-gvc-iface.h"
#include "mlview-safe-ptr-utils.h"
#include "mlview-exception.h"

namespace mlview
{

struct GVCIfacePriv
{
	SafePtr<IView, ObjectRef, ObjectUnref> cur_view_ptr ;
	//IView *cur_view_ptr ;

	sigc::signal2<void,
	IView* /*swapped in*/,
	IView* /*swapped out*/> signal_views_swapped ;

	GVCIfacePriv () :
		cur_view_ptr (NULL)
	{}
}
;//end struct GVCIfacePriv

GVCIface::GVCIface ()
{
	m_priv = new GVCIfacePriv () ;
	THROW_IF_FAIL (m_priv) ;
}

GVCIface::~GVCIface ()
{
        if (!m_priv)
                return ;

        delete m_priv ;
        m_priv = NULL ;
}

IView*
GVCIface::get_cur_view ()
{
        return m_priv->cur_view_ptr ;
}

void
GVCIface::set_cur_view (IView *a_view, bool a_emit_signal)
{
	SafePtr<IView, ObjectRef, ObjectUnref> old_view_ptr =
	    m_priv->cur_view_ptr ;
	m_priv->cur_view_ptr = a_view ;

	if (a_emit_signal)
		signal_views_swapped ().emit (m_priv->cur_view_ptr,
									  old_view_ptr) ;
}

sigc::signal2<void, IView* /*swapped in*/, IView* /*swapped out*/>&
GVCIface::signal_views_swapped ()
{
        return m_priv->signal_views_swapped ;
}

}//namespace mlview
