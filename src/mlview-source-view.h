/* -*- Mode: C++; indent-tabs-mode:nil; c-basic-offset: 4-*- */
/*
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute 
 *it and/or modify it under the terms of 
 *the GNU General Public License as published by the 
 *Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */
#ifndef __MLVIEW_SOURCE_VIEW_H
#define __MLVIEW_SOURCE_VIEW_H

#include <gtksourceview/gtksourceview.h>
#include <gtksourceview/gtksourcelanguage.h>
#include <gtksourceview/gtksourcelanguagesmanager.h>
#include <gtksourceview/gtksourcebuffer.h>
#include "mlview-view-adapter.h"
#include "mlview-xml-document.h"

namespace mlview
{
struct SourceViewPriv ;

IView *create_source_view_instance (MlViewXMLDocument *a_doc,
                                    const gchar *a_name) ;

class SourceView : public ViewAdapter
{
	friend struct SourceViewPriv ;

	SourceViewPriv *m_priv ;

	//forbid copy/assignation
	SourceView (SourceView const &) ;
	SourceView& operator= (SourceView const &) ;

public:
	SourceView (MlViewXMLDocument *a_doc,
	            const UString &a_name) ;

	virtual ~SourceView () ;

	enum MlViewStatus set_default_options () ;

	MlViewXMLDocument* get_document () ;

	GtkUIManager * get_ui_manager () ;

	bool is_there_an_opened_tag ()  ;

	bool close_currently_opened_tag () ;

	enum MlViewStatus connect_to_doc (MlViewXMLDocument *a_doc) ;

	enum MlViewStatus disconnect_from_doc (MlViewXMLDocument *a_doc) ;

	bool get_must_rebuild_upon_document_reload () ;

	GtkWidget * get_contextual_menu () ;

	bool has_changed () ;

	enum MlViewStatus undo () ;

	enum MlViewStatus redo () ;

	bool can_undo () ;

	bool can_redo () ;

	enum MlViewStatus cut_selected_text () ;

	enum MlViewStatus copy_selected_text () ;

	enum MlViewStatus paste_text () ;


protected:
	GtkTextBuffer * get_text_buffer ()  ;

	enum MlViewStatus get_last_dangling_opened_tag (GtkTextIter *a_iter,
													gchar **a_tag_name) ;


	GtkSourceBuffer * get_source_buffer () ;

	enum MlViewStatus serialize_and_load_doc () ;

	enum MlViewStatus save_text_buffer_into_xml_doc () ;

	enum MlViewStatus handle_contextual_menu_request
		(GtkWidget *a_source_widget, GdkEvent *a_event) ;

	enum MlViewStatus build_contextual_menu () ;

	UString build_edit_menu_root_path (bool a_popup) ;

	enum MlViewStatus build_edit_menu_body (const UString &a_menu_root_path) ;

	GtkSourceLanguagesManager * get_languages_manager () ;

	enum MlViewStatus set_language (GtkSourceLanguage *a_language) ;

	enum MlViewStatus set_language_from_mime_type(const UString &a_mime_type) ;

	enum MlViewStatus set_default_language () ;

	enum MlViewStatus get_selected_text (UString &a_selected_text) ;

	enum MlViewStatus delete_selected_text () ;

	Clipboard& get_clipboard () ;

	//************
	//legacy callback
	//methods implemented
	//as static methods
	//***************
	static void document_changed_cb (MlViewXMLDocument *a_doc,
	                                 SourceView *a_view) ;

	static bool contextual_menu_requested_cb (GtkWidget *a_source_widget,
	        GdkEvent *a_event,
	        gpointer a_user_data) ;

	static void close_all_tag_action_cb (GtkAction *a_action,
	                                     gpointer user_data) ;

	static void close_tag_action_cb (GtkAction *a_action,
	                                 gpointer user_data) ;

	static void going_to_save_cb (MlViewXMLDocument *a_doc,
	                              SourceView *a_view) ;

	static void changed_cb (GtkTextBuffer *a_text_buffer,
	                        gpointer a_data) ;

	static void undo_state_changed_cb (GtkSourceBuffer *source_buffer,
	                                   gboolean an_arg,
	                                   gpointer a_source_view) ;

	static bool native_sv_button_press_cb (GtkSourceBuffer *a_native_sv,
	                                       GdkEventButton *a_event) ;
	//*************
	//signal handlers
	//*************
	void on_is_swapped_in () ;
	void on_is_swapped_out () ;
	void on_native_source_view_realize () ;
	bool on_native_sv_button_press_event (GdkEventButton *a_event) ;
	void on_contextual_menu_requested (GtkWidget *a_request_source,
									   GdkEvent *a_event) ;
	static void on_cut_menu_action (GtkAction *a_action, gpointer a_user_data) ;
	static void on_copy_menu_action (GtkAction *a_action, gpointer a_user_data) ;
	static void on_paste_menu_action (GtkAction *a_action, gpointer a_user_data) ;
	static void on_delete_menu_action (GtkAction *a_action, gpointer a_user_data);
}
;//end class SourceView
}//end namespace mlview

#endif /*__MLVIEW_SOURCE_VIEW_H*/
