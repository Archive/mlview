/* -*- Mode: C; indent-tabs-mode: true; c-basic-offset: 4-*- */

/*This file is part of GNU MlView
 *
 *GNU MlView is free software; 
 *you can redistribute it and/or modify it under the terms of 
 *the GNU General Public License as 
 *published by the Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope 
 *that it will be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */

#ifndef __MLVIEW_VALIDATION_OUTPUT_H__
#define __MLVIEW_VALIDATION_OUTPUT_H__

#include <vector>
#include <libxml/tree.h>
#include "mlview-xml-document.h"
#include "mlview-object.h"
#include "mlview-ustring.h"

namespace mlview
{

struct ValidationOutputPriv ;
class ValidationOutput
{
	friend struct ValidationOutputPriv ;
protected:
	ValidationOutputPriv *m_priv ;

	//forbid copy/assignation
	ValidationOutput (ValidationOutput const &) ;
	ValidationOutput& operator= (ValidationOutput const &) ;

public:
	class Message
	{

		xmlNode *m_node;
		UString m_text;
		xmlErrorLevel m_priority;
		xmlElementType m_type;

	public:
		Message ():
			m_node (NULL),
			m_priority (XML_ERR_NONE),
			m_type (XML_ELEMENT_NODE)
		{}

		Message (const UString &message,
				xmlNode *a_node,
				xmlErrorLevel a_priority,
				xmlElementType a_element_type) :
			m_node (a_node),
			m_priority (a_priority),
			m_type (a_element_type)
		{}

		Message (const Message &a_message) ;
		virtual ~Message ();
		void set
		(const Message &a_message) ;
		Message& operator= (Message& a_message) ;

		xmlNode* get_node () const ;
		void set_node (xmlNode *a_node) ;
		const UString& get_text () const ;
		xmlErrorLevel get_priority () const ;
		xmlElementType get_node_type () const ;
	}
	;//end class Message


	ValidationOutput (MlViewXMLDocument *a_doc) ;
	virtual ~ValidationOutput () ;

	MlViewXMLDocument * get_document () ;

	vector<mlview::ValidationOutput::Message*>& get_messages () ;

	void append_message (ValidationOutput::Message *a_message) ;

	void append_message (const UString &a_text,
			xmlNode *a_node,
			xmlErrorLevel a_priority) ;

protected:

	void connect_to_doc () ;

	void disconnect_from_doc () ;

	//*************************************
	//legacy gobject level signal callbacks
	//*************************************
	static void xml_document_closed_cb (MlViewXMLDocument *a_this,
			ValidationOutput *a_output) ;

	static void xml_node_name_changed_cb (MlViewXMLDocument *a_this,
			xmlNode *a_node,
			ValidationOutput *a_output) ;

	static void xml_node_cut_cb (MlViewXMLDocument *a_xml_doc,
			xmlNode *a_parent_node,
			xmlNode *a_cut_node,
			ValidationOutput *a_output) ;

private:
	ValidationOutput () ;

};//end class ValidationOutput
}//end namespace mlview
#endif
