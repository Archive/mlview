/* -*- Mode: C; indent-tabs-mode:nil; c-basic-offset: 8-*- */

/*
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute 
 *it and/or modify it under the terms of 
 *the GNU General Public License as published by the 
 *Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */
#include "mlview-idbo.h"
#ifdef MLVIEW_WITH_DBUS

static void mlview_idbo_base_init (gpointer a_iface) ;

static void
mlview_idbo_base_init (gpointer a_iface)
{
	static gboolean initialized = FALSE ;

	if (initialized)
		return ;

	initialized = TRUE ;
}

/******************
 *  public methods
 ******************/

GType
mlview_idbo_get_type (void)
{
	static GType type = 0 ;

	if (!type) {
		static const GTypeInfo info = {
		                                  sizeof (MlViewIDBO),/*class size*/
		                                  mlview_idbo_base_init, /*base_init*/
		                                  NULL, /*base_finalize*/
		                                  NULL,/*class init*/
		                                  NULL,/*class finalize*/
		                                  NULL,/*class data*/
		                                  0,/*instance size*/
		                                  0,/*n_prealloc*/
		                                  NULL,/*instance_init*/
		                              } ;
		type = g_type_register_static (G_TYPE_INTERFACE,
		                               "MlViewIDBO",
		                               &info, 0) ;
		g_type_interface_add_prerequisite (type, G_TYPE_OBJECT) ;
	}
	return type ;
}

DBusObjectPathMessageFunction
mlview_idbo_get_message_handler (MlViewIDBO *a_this)
{
	g_return_val_if_fail (a_this && MLVIEW_IS_IDBO (a_this),
	                      NULL) ;

	return MLVIEW_IDBO_GET_IFACE (a_this)->message_handler ;

}

DBusObjectPathUnregisterFunction
mlview_idbo_get_message_unregister_handler (MlViewIDBO *a_this)
{
	g_return_val_if_fail (a_this && MLVIEW_IS_IDBO (a_this),
	                      NULL) ;
	return MLVIEW_IDBO_GET_IFACE (a_this)->message_unregister_handler ;
}

#endif /*MLVIEW_WITH_DBUS*/
