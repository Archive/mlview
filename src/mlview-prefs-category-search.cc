/* -*- Mode: C++; indent-tabs-mode:true ; c-basic-offset: 4-*- */
/*
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute 
 *it and/or modify it under the terms of 
 *the GNU General Public License as published by the 
 *Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */
#include "mlview-exception.h"
#include "mlview-prefs-category-search.h"


namespace mlview
{

struct PrefsCategorySearchPriv
{
    static const char* CASE_SENSITIVE_KEY;
    static const char* SEARCH_AMONG_NODE_NAMES_KEY;
    static const char* SEARCH_AMONG_NODE_VALUES_KEY;
    static const char* SEARCH_AMONG_ATTR_NAMES_KEY;
    static const char* SEARCH_AMONG_ATTR_VALUES_KEY;
};

const char *PrefsCategorySearchPriv::CASE_SENSITIVE_KEY =
    "/apps/mlview/search/case-sensitive";
const char *PrefsCategorySearchPriv::SEARCH_AMONG_NODE_NAMES_KEY =
    "/apps/mlview/search/node-names";
const char *PrefsCategorySearchPriv::SEARCH_AMONG_NODE_VALUES_KEY =
    "/apps/mlview/search/node-values";
const char *PrefsCategorySearchPriv::SEARCH_AMONG_ATTR_NAMES_KEY =
    "/apps/mlview/search/attribute-names";
const char *PrefsCategorySearchPriv::SEARCH_AMONG_ATTR_VALUES_KEY =
    "/apps/mlview/search/attribute-values";


//////////////////////////
// Class Implementation //
//////////////////////////
PrefsCategorySearch::PrefsCategorySearch
(PrefsStorageManager *storage_manager_)
    : PrefsCategory::PrefsCategory ("search", storage_manager_)
{
    m_priv = new PrefsCategorySearchPriv ();
}

PrefsCategorySearch::~PrefsCategorySearch ()
{
    if (m_priv != NULL) {
	delete m_priv;
	m_priv = NULL;
    }
}

bool
PrefsCategorySearch::search_among_node_names_default ()
{
    try {

	return get_storage_manager ().get_default_bool_value (
		PrefsCategorySearchPriv::SEARCH_AMONG_NODE_NAMES_KEY);

    } catch (mlview::Exception& e) {
	TRACE_EXCEPTION (e);
    }
// Fallback to TRUE when everything else fails
    return TRUE;
}

bool
PrefsCategorySearch::search_among_node_values_default ()
{
    try {

	return get_storage_manager ().get_default_bool_value (
		PrefsCategorySearchPriv::SEARCH_AMONG_NODE_VALUES_KEY);

    } catch (mlview::Exception& e) {
	TRACE_EXCEPTION (e);
    }
    return TRUE;
}

bool
PrefsCategorySearch::search_among_attr_names_default ()
{
    try {

	return get_storage_manager ().get_default_bool_value (
		PrefsCategorySearchPriv::SEARCH_AMONG_ATTR_NAMES_KEY);

    } catch (mlview::Exception& e) {
	TRACE_EXCEPTION (e);
    }
    return FALSE;
}

bool
PrefsCategorySearch::search_among_attr_values_default ()
{
    try {

	return get_storage_manager ().get_default_bool_value (
		PrefsCategorySearchPriv::SEARCH_AMONG_ATTR_VALUES_KEY);

    } catch (mlview::Exception& e) {
	TRACE_EXCEPTION (e);
    }
    return TRUE;
}


bool
PrefsCategorySearch::search_among_node_names ()
{
    try {

	return get_storage_manager ().get_bool_value (
		PrefsCategorySearchPriv::SEARCH_AMONG_NODE_NAMES_KEY);

    } catch (mlview::Exception& e) {
		TRACE_EXCEPTION (e);
    }

    return search_among_node_names_default ();
}

bool
PrefsCategorySearch::search_among_node_values ()
{
    try {

	return get_storage_manager ().get_bool_value (
		PrefsCategorySearchPriv::SEARCH_AMONG_NODE_VALUES_KEY);

    } catch (mlview::Exception& e) {
	TRACE_EXCEPTION (e);
    }

    return search_among_node_values_default ();
}

bool
PrefsCategorySearch::search_among_attr_names ()
{
    try {

	return get_storage_manager ().get_bool_value (
		PrefsCategorySearchPriv::SEARCH_AMONG_ATTR_NAMES_KEY);

    } catch (mlview::Exception& e) {
	TRACE_EXCEPTION (e);
    }

    return search_among_attr_names_default ();
}

bool
PrefsCategorySearch::search_among_attr_values ()
{
    try {

	return get_storage_manager ().get_bool_value (
		PrefsCategorySearchPriv::SEARCH_AMONG_ATTR_VALUES_KEY);

    } catch (mlview::Exception& e) {
	TRACE_EXCEPTION (e);
    }

    return search_among_attr_values_default ();
}

bool
PrefsCategorySearch::search_case_sensitive ()
{
    return get_storage_manager ().get_bool_value (
	    PrefsCategorySearchPriv::CASE_SENSITIVE_KEY);
}

void
PrefsCategorySearch::set_search_among_node_names (bool value)
{
    get_storage_manager ().set_bool_value (
	    PrefsCategorySearchPriv::SEARCH_AMONG_NODE_NAMES_KEY,
	    value);
}

void
PrefsCategorySearch::set_search_among_node_values (bool value)
{
    get_storage_manager ().set_bool_value (
	    PrefsCategorySearchPriv::SEARCH_AMONG_NODE_VALUES_KEY,
	    value);
}

void
PrefsCategorySearch::set_search_among_attr_names (bool value)
{
    get_storage_manager ().set_bool_value (
	    PrefsCategorySearchPriv::SEARCH_AMONG_ATTR_NAMES_KEY,
	    value);
}

void
PrefsCategorySearch::set_search_among_attr_values (bool value)
{
    get_storage_manager ().set_bool_value (
	    PrefsCategorySearchPriv::SEARCH_AMONG_ATTR_VALUES_KEY,
	    value);
}

void
PrefsCategorySearch::set_search_case_sensitive (bool value)
{
    get_storage_manager ().set_bool_value (
	    PrefsCategorySearchPriv::CASE_SENSITIVE_KEY,
	    value);
}

} // namespace mlview
