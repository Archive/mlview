/* -*- Mode: C++; indent-tabs-mode:true ; c-basic-offset: 4-*- */

/*
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute 
 *it and/or modify it under the terms of 
 *the GNU General Public License as published by the 
 *Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */

#include <string.h>
#include "mlview-utils.h"
#include "libxml/hash.h"
#include "libxml/uri.h"
#include "libxml/xpath.h"
#include "libxml/catalog.h"

/**
 *@file
 *Misc usefull functions.
 *There are also some functions that could be small patches
 *gtk and libxml2.
 */

typedef struct _ErrorMessage ErrorMessage;

struct _ErrorMessage
{
	const gchar *error_type;
	const gchar *error_string;
};


/*The dynamic list of available encodings*/
static GList *gv_available_encodings = NULL;
static gint gv_available_encodings_ref_count = 0;


/*
 *The list of the default supported encodings
 */
const gchar *gv_default_encodings[] =
    {
        "UTF-8",
        "ISO_8859-1",
        "UTF-16",
        NULL                    /*must be the last item of the list!! */
    };

/*
 *This has been copied from libxml2 ;)
 *Free software also means "seat on the shoulders of the giants.".
 */
static gint gv_base_array[] = {
                                  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, /* 0x0000 - 0x000F */
                                  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, /* 0x0010 - 0x001F */
                                  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, /* 0x0020 - 0x002F */
                                  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, /* 0x0030 - 0x003F */
                                  0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, /* 0x0040 - 0x004F */
                                  1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, /* 0x0050 - 0x005F */
                                  0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, /* 0x0060 - 0x006F */
                                  1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, /* 0x0070 - 0x007F */
                                  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, /* 0x0080 - 0x008F */
                                  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, /* 0x0090 - 0x009F */
                                  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, /* 0x00A0 - 0x00AF */
                                  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, /* 0x00B0 - 0x00BF */
                                  1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, /* 0x00C0 - 0x00CF */
                                  1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, /* 0x00D0 - 0x00DF */
                                  1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, /* 0x00E0 - 0x00EF */
                                  1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, /* 0x00F0 - 0x00FF */
                              };

static guint gv_last_uniq_number = 0 ;

/**
 *Initialises the gettext i18n functions.
 */
static void
mlview_utils_init_i18n (void)
{
	bindtextdomain (GETTEXT_PACKAGE, MLVIEW_LOCALEDIR) ;
	bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8") ;
	textdomain (GETTEXT_PACKAGE);
}


/**
 *This has been copied from libxml2.
 *@return TRUE if a_c is a base char. See the xml rec to
 *know what base char means.
 */
gboolean
mlview_utils_is_base_char (gint a_c)
{
	return(
	          (((a_c) < 0x0100) ? gv_base_array[a_c] :
	           (	/* accelerator */
	               (((a_c) >= 0x0100) && ((a_c) <= 0x0131)) ||
	               (((a_c) >= 0x0134) && ((a_c) <= 0x013E)) ||
	               (((a_c) >= 0x0141) && ((a_c) <= 0x0148)) ||
	               (((a_c) >= 0x014A) && ((a_c) <= 0x017E)) ||
	               (((a_c) >= 0x0180) && ((a_c) <= 0x01C3)) ||
	               (((a_c) >= 0x01CD) && ((a_c) <= 0x01F0)) ||
	               (((a_c) >= 0x01F4) && ((a_c) <= 0x01F5)) ||
	               (((a_c) >= 0x01FA) && ((a_c) <= 0x0217)) ||
	               (((a_c) >= 0x0250) && ((a_c) <= 0x02A8)) ||
	               (((a_c) >= 0x02BB) && ((a_c) <= 0x02C1)) ||
	               ((a_c) == 0x0386) ||
	               (((a_c) >= 0x0388) && ((a_c) <= 0x038A)) ||
	               ((a_c) == 0x038C) ||
	               (((a_c) >= 0x038E) && ((a_c) <= 0x03A1)) ||
	               (((a_c) >= 0x03A3) && ((a_c) <= 0x03CE)) ||
	               (((a_c) >= 0x03D0) && ((a_c) <= 0x03D6)) ||
	               ((a_c) == 0x03DA) ||
	               ((a_c) == 0x03DC) ||
	               ((a_c) == 0x03DE) ||
	               ((a_c) == 0x03E0) ||
	               (((a_c) >= 0x03E2) && ((a_c) <= 0x03F3)) ||
	               (((a_c) >= 0x0401) && ((a_c) <= 0x040C)) ||
	               (((a_c) >= 0x040E) && ((a_c) <= 0x044F)) ||
	               (((a_c) >= 0x0451) && ((a_c) <= 0x045C)) ||
	               (((a_c) >= 0x045E) && ((a_c) <= 0x0481)) ||
	               (((a_c) >= 0x0490) && ((a_c) <= 0x04C4)) ||
	               (((a_c) >= 0x04C7) && ((a_c) <= 0x04C8)) ||
	               (((a_c) >= 0x04CB) && ((a_c) <= 0x04CC)) ||
	               (((a_c) >= 0x04D0) && ((a_c) <= 0x04EB)) ||
	               (((a_c) >= 0x04EE) && ((a_c) <= 0x04F5)) ||
	               (((a_c) >= 0x04F8) && ((a_c) <= 0x04F9)) ||
	               (((a_c) >= 0x0531) && ((a_c) <= 0x0556)) ||
	               ((a_c) == 0x0559) ||
	               (((a_c) >= 0x0561) && ((a_c) <= 0x0586)) ||
	               (((a_c) >= 0x05D0) && ((a_c) <= 0x05EA)) ||
	               (((a_c) >= 0x05F0) && ((a_c) <= 0x05F2)) ||
	               (((a_c) >= 0x0621) && ((a_c) <= 0x063A)) ||
	               (((a_c) >= 0x0641) && ((a_c) <= 0x064A)) ||
	               (((a_c) >= 0x0671) && ((a_c) <= 0x06B7)) ||
	               (((a_c) >= 0x06BA) && ((a_c) <= 0x06BE)) ||
	               (((a_c) >= 0x06C0) && ((a_c) <= 0x06CE)) ||
	               (((a_c) >= 0x06D0) && ((a_c) <= 0x06D3)) ||
	               ((a_c) == 0x06D5) ||
	               (((a_c) >= 0x06E5) && ((a_c) <= 0x06E6)) ||
	               (((a_c) >= 0x905) && (	/* accelerator */
	                    (((a_c) >= 0x0905) && ((a_c) <= 0x0939)) ||
	                    ((a_c) == 0x093D) ||
	                    (((a_c) >= 0x0958) && ((a_c) <= 0x0961)) ||
	                    (((a_c) >= 0x0985) && ((a_c) <= 0x098C)) ||
	                    (((a_c) >= 0x098F) && ((a_c) <= 0x0990)) ||
	                    (((a_c) >= 0x0993) && ((a_c) <= 0x09A8)) ||
	                    (((a_c) >= 0x09AA) && ((a_c) <= 0x09B0)) ||
	                    ((a_c) == 0x09B2) ||
	                    (((a_c) >= 0x09B6) && ((a_c) <= 0x09B9)) ||
	                    (((a_c) >= 0x09DC) && ((a_c) <= 0x09DD)) ||
	                    (((a_c) >= 0x09DF) && ((a_c) <= 0x09E1)) ||
	                    (((a_c) >= 0x09F0) && ((a_c) <= 0x09F1)) ||
	                    (((a_c) >= 0x0A05) && ((a_c) <= 0x0A0A)) ||
	                    (((a_c) >= 0x0A0F) && ((a_c) <= 0x0A10)) ||
	                    (((a_c) >= 0x0A13) && ((a_c) <= 0x0A28)) ||
	                    (((a_c) >= 0x0A2A) && ((a_c) <= 0x0A30)) ||
	                    (((a_c) >= 0x0A32) && ((a_c) <= 0x0A33)) ||
	                    (((a_c) >= 0x0A35) && ((a_c) <= 0x0A36)) ||
	                    (((a_c) >= 0x0A38) && ((a_c) <= 0x0A39)) ||
	                    (((a_c) >= 0x0A59) && ((a_c) <= 0x0A5C)) ||
	                    ((a_c) == 0x0A5E) ||
	                    (((a_c) >= 0x0A72) && ((a_c) <= 0x0A74)) ||
	                    (((a_c) >= 0x0A85) && ((a_c) <= 0x0A8B)) ||
	                    ((a_c) == 0x0A8D) ||
	                    (((a_c) >= 0x0A8F) && ((a_c) <= 0x0A91)) ||
	                    (((a_c) >= 0x0A93) && ((a_c) <= 0x0AA8)) ||
	                    (((a_c) >= 0x0AAA) && ((a_c) <= 0x0AB0)) ||
	                    (((a_c) >= 0x0AB2) && ((a_c) <= 0x0AB3)) ||
	                    (((a_c) >= 0x0AB5) && ((a_c) <= 0x0AB9)) ||
	                    ((a_c) == 0x0ABD) ||
	                    ((a_c) == 0x0AE0) ||
	                    (((a_c) >= 0x0B05) && ((a_c) <= 0x0B0C)) ||
	                    (((a_c) >= 0x0B0F) && ((a_c) <= 0x0B10)) ||
	                    (((a_c) >= 0x0B13) && ((a_c) <= 0x0B28)) ||
	                    (((a_c) >= 0x0B2A) && ((a_c) <= 0x0B30)) ||
	                    (((a_c) >= 0x0B32) && ((a_c) <= 0x0B33)) ||
	                    (((a_c) >= 0x0B36) && ((a_c) <= 0x0B39)) ||
	                    ((a_c) == 0x0B3D) ||
	                    (((a_c) >= 0x0B5C) && ((a_c) <= 0x0B5D)) ||
	                    (((a_c) >= 0x0B5F) && ((a_c) <= 0x0B61)) ||
	                    (((a_c) >= 0x0B85) && ((a_c) <= 0x0B8A)) ||
	                    (((a_c) >= 0x0B8E) && ((a_c) <= 0x0B90)) ||
	                    (((a_c) >= 0x0B92) && ((a_c) <= 0x0B95)) ||
	                    (((a_c) >= 0x0B99) && ((a_c) <= 0x0B9A)) ||
	                    ((a_c) == 0x0B9C) ||
	                    (((a_c) >= 0x0B9E) && ((a_c) <= 0x0B9F)) ||
	                    (((a_c) >= 0x0BA3) && ((a_c) <= 0x0BA4)) ||
	                    (((a_c) >= 0x0BA8) && ((a_c) <= 0x0BAA)) ||
	                    (((a_c) >= 0x0BAE) && ((a_c) <= 0x0BB5)) ||
	                    (((a_c) >= 0x0BB7) && ((a_c) <= 0x0BB9)) ||
	                    (((a_c) >= 0x0C05) && ((a_c) <= 0x0C0C)) ||
	                    (((a_c) >= 0x0C0E) && ((a_c) <= 0x0C10)) ||
	                    (((a_c) >= 0x0C12) && ((a_c) <= 0x0C28)) ||
	                    (((a_c) >= 0x0C2A) && ((a_c) <= 0x0C33)) ||
	                    (((a_c) >= 0x0C35) && ((a_c) <= 0x0C39)) ||
	                    (((a_c) >= 0x0C60) && ((a_c) <= 0x0C61)) ||
	                    (((a_c) >= 0x0C85) && ((a_c) <= 0x0C8C)) ||
	                    (((a_c) >= 0x0C8E) && ((a_c) <= 0x0C90)) ||
	                    (((a_c) >= 0x0C92) && ((a_c) <= 0x0CA8)) ||
	                    (((a_c) >= 0x0CAA) && ((a_c) <= 0x0CB3)) ||
	                    (((a_c) >= 0x0CB5) && ((a_c) <= 0x0CB9)) ||
	                    ((a_c) == 0x0CDE) ||
	                    (((a_c) >= 0x0CE0) && ((a_c) <= 0x0CE1)) ||
	                    (((a_c) >= 0x0D05) && ((a_c) <= 0x0D0C)) ||
	                    (((a_c) >= 0x0D0E) && ((a_c) <= 0x0D10)) ||
	                    (((a_c) >= 0x0D12) && ((a_c) <= 0x0D28)) ||
	                    (((a_c) >= 0x0D2A) && ((a_c) <= 0x0D39)) ||
	                    (((a_c) >= 0x0D60) && ((a_c) <= 0x0D61)) ||
	                    (((a_c) >= 0x0E01) && ((a_c) <= 0x0E2E)) ||
	                    ((a_c) == 0x0E30) ||
	                    (((a_c) >= 0x0E32) && ((a_c) <= 0x0E33)) ||
	                    (((a_c) >= 0x0E40) && ((a_c) <= 0x0E45)) ||
	                    (((a_c) >= 0x0E81) && ((a_c) <= 0x0E82)) ||
	                    ((a_c) == 0x0E84) ||
	                    (((a_c) >= 0x0E87) && ((a_c) <= 0x0E88)) ||
	                    ((a_c) == 0x0E8A) ||
	                    ((a_c) == 0x0E8D) ||
	                    (((a_c) >= 0x0E94) && ((a_c) <= 0x0E97)) ||
	                    (((a_c) >= 0x0E99) && ((a_c) <= 0x0E9F)) ||
	                    (((a_c) >= 0x0EA1) && ((a_c) <= 0x0EA3)) ||
	                    ((a_c) == 0x0EA5) ||
	                    ((a_c) == 0x0EA7) ||
	                    (((a_c) >= 0x0EAA) && ((a_c) <= 0x0EAB)) ||
	                    (((a_c) >= 0x0EAD) && ((a_c) <= 0x0EAE)) ||
	                    ((a_c) == 0x0EB0) ||
	                    (((a_c) >= 0x0EB2) && ((a_c) <= 0x0EB3)) ||
	                    ((a_c) == 0x0EBD) ||
	                    (((a_c) >= 0x0EC0) && ((a_c) <= 0x0EC4)) ||
	                    (((a_c) >= 0x0F40) && ((a_c) <= 0x0F47)) ||
	                    (((a_c) >= 0x0F49) && ((a_c) <= 0x0F69)) ||
	                    (((a_c) >= 0x10A0) && (	/* accelerator */
	                         (((a_c) >= 0x10A0) && ((a_c) <= 0x10C5)) ||
	                         (((a_c) >= 0x10D0) && ((a_c) <= 0x10F6)) ||
	                         ((a_c) == 0x1100) ||
	                         (((a_c) >= 0x1102) && ((a_c) <= 0x1103)) ||
	                         (((a_c) >= 0x1105) && ((a_c) <= 0x1107)) ||
	                         ((a_c) == 0x1109) ||
	                         (((a_c) >= 0x110B) && ((a_c) <= 0x110C)) ||
	                         (((a_c) >= 0x110E) && ((a_c) <= 0x1112)) ||
	                         ((a_c) == 0x113C) ||
	                         ((a_c) == 0x113E) ||
	                         ((a_c) == 0x1140) ||
	                         ((a_c) == 0x114C) ||
	                         ((a_c) == 0x114E) ||
	                         ((a_c) == 0x1150) ||
	                         (((a_c) >= 0x1154) && ((a_c) <= 0x1155)) ||
	                         ((a_c) == 0x1159) ||
	                         (((a_c) >= 0x115F) && ((a_c) <= 0x1161)) ||
	                         ((a_c) == 0x1163) ||
	                         ((a_c) == 0x1165) ||
	                         ((a_c) == 0x1167) ||
	                         ((a_c) == 0x1169) ||
	                         (((a_c) >= 0x116D) && ((a_c) <= 0x116E)) ||
	                         (((a_c) >= 0x1172) && ((a_c) <= 0x1173)) ||
	                         ((a_c) == 0x1175) ||
	                         ((a_c) == 0x119E) ||
	                         ((a_c) == 0x11A8) ||
	                         ((a_c) == 0x11AB) ||
	                         (((a_c) >= 0x11AE) && ((a_c) <= 0x11AF)) ||
	                         (((a_c) >= 0x11B7) && ((a_c) <= 0x11B8)) ||
	                         ((a_c) == 0x11BA) ||
	                         (((a_c) >= 0x11BC) && ((a_c) <= 0x11C2)) ||
	                         ((a_c) == 0x11EB) ||
	                         ((a_c) == 0x11F0) ||
	                         ((a_c) == 0x11F9) ||
	                         (((a_c) >= 0x1E00) && ((a_c) <= 0x1E9B)) ||
	                         (((a_c) >= 0x1EA0) && ((a_c) <= 0x1EF9)) ||
	                         (((a_c) >= 0x1F00) && ((a_c) <= 0x1F15)) ||
	                         (((a_c) >= 0x1F18) && ((a_c) <= 0x1F1D)) ||
	                         (((a_c) >= 0x1F20) && ((a_c) <= 0x1F45)) ||
	                         (((a_c) >= 0x1F48) && ((a_c) <= 0x1F4D)) ||
	                         (((a_c) >= 0x1F50) && ((a_c) <= 0x1F57)) ||
	                         ((a_c) == 0x1F59) ||
	                         ((a_c) == 0x1F5B) ||
	                         ((a_c) == 0x1F5D) ||
	                         (((a_c) >= 0x1F5F) && ((a_c) <= 0x1F7D)) ||
	                         (((a_c) >= 0x1F80) && ((a_c) <= 0x1FB4)) ||
	                         (((a_c) >= 0x1FB6) && ((a_c) <= 0x1FBC)) ||
	                         ((a_c) == 0x1FBE) ||
	                         (((a_c) >= 0x1FC2) && ((a_c) <= 0x1FC4)) ||
	                         (((a_c) >= 0x1FC6) && ((a_c) <= 0x1FCC)) ||
	                         (((a_c) >= 0x1FD0) && ((a_c) <= 0x1FD3)) ||
	                         (((a_c) >= 0x1FD6) && ((a_c) <= 0x1FDB)) ||
	                         (((a_c) >= 0x1FE0) && ((a_c) <= 0x1FEC)) ||
	                         (((a_c) >= 0x1FF2) && ((a_c) <= 0x1FF4)) ||
	                         (((a_c) >= 0x1FF6) && ((a_c) <= 0x1FFC)) ||
	                         ((a_c) == 0x2126) ||
	                         (((a_c) >= 0x212A) && ((a_c) <= 0x212B)) ||
	                         ((a_c) == 0x212E) ||
	                         (((a_c) >= 0x2180) && ((a_c) <= 0x2182)) ||
	                         (((a_c) >= 0x3041) && ((a_c) <= 0x3094)) ||
	                         (((a_c) >= 0x30A1) && ((a_c) <= 0x30FA)) ||
	                         (((a_c) >= 0x3105) && ((a_c) <= 0x312C)) ||
	                         (((a_c) >= 0xAC00) && ((a_c) <= 0xD7A3))) /* accelerators */ ))))));
}

gboolean
mlview_utils_is_letter (gint a_c)
{
	if (mlview_utils_is_base_char (a_c) == TRUE
	        || mlview_utils_is_ideographic (a_c) == TRUE) {
		return TRUE ;
	}
	return FALSE ;
}

/**
 *Copied from libxml2's code.
 *@return TRUE if a_c is a digit.
 *See the xml rec to know what a digit is.
 */
gboolean
mlview_utils_is_digit (gint a_c)
{
	return (
	           (((a_c) >= 0x0030) && ((a_c) <= 0x0039)) ||
	           (((a_c) >= 0x660) && (	/* accelerator */
	                (((a_c) >= 0x0660) && ((a_c) <= 0x0669)) ||
	                (((a_c) >= 0x06F0) && ((a_c) <= 0x06F9)) ||
	                (((a_c) >= 0x0966) && ((a_c) <= 0x096F)) ||
	                (((a_c) >= 0x09E6) && ((a_c) <= 0x09EF)) ||
	                (((a_c) >= 0x0A66) && ((a_c) <= 0x0A6F)) ||
	                (((a_c) >= 0x0AE6) && ((a_c) <= 0x0AEF)) ||
	                (((a_c) >= 0x0B66) && ((a_c) <= 0x0B6F)) ||
	                (((a_c) >= 0x0BE7) && ((a_c) <= 0x0BEF)) ||
	                (((a_c) >= 0x0C66) && ((a_c) <= 0x0C6F)) ||
	                (((a_c) >= 0x0CE6) && ((a_c) <= 0x0CEF)) ||
	                (((a_c) >= 0x0D66) && ((a_c) <= 0x0D6F)) ||
	                (((a_c) >= 0x0E50) && ((a_c) <= 0x0E59)) ||
	                (((a_c) >= 0x0ED0) && ((a_c) <= 0x0ED9)) ||
	                (((a_c) >= 0x0F20) && ((a_c) <= 0x0F29))) /* accelerator */ ));
}

gboolean
mlview_utils_is_combining (gint a_c)
{
	return(
	          (((a_c) >= 0x300) && (	/* accelerator */
	               (((a_c) >= 0x0300) && ((a_c) <= 0x0345)) ||
	               (((a_c) >= 0x0360) && ((a_c) <= 0x0361)) ||
	               (((a_c) >= 0x0483) && ((a_c) <= 0x0486)) ||
	               (((a_c) >= 0x0591) && ((a_c) <= 0x05A1)) ||
	               (((a_c) >= 0x05A3) && ((a_c) <= 0x05B9)) ||
	               (((a_c) >= 0x05BB) && ((a_c) <= 0x05BD)) ||
	               ((a_c) == 0x05BF) ||
	               (((a_c) >= 0x05C1) && ((a_c) <= 0x05C2)) ||
	               ((a_c) == 0x05C4) ||
	               (((a_c) >= 0x064B) && ((a_c) <= 0x0652)) ||
	               ((a_c) == 0x0670) ||
	               (((a_c) >= 0x06D6) && ((a_c) <= 0x06DC)) ||
	               (((a_c) >= 0x06DD) && ((a_c) <= 0x06DF)) ||
	               (((a_c) >= 0x06E0) && ((a_c) <= 0x06E4)) ||
	               (((a_c) >= 0x06E7) && ((a_c) <= 0x06E8)) ||
	               (((a_c) >= 0x06EA) && ((a_c) <= 0x06ED)) ||
	               (((a_c) >= 0x0901) && (	/* accelerator */
	                    (((a_c) >= 0x0901) && ((a_c) <= 0x0903)) ||
	                    ((a_c) == 0x093C) ||
	                    (((a_c) >= 0x093E) && ((a_c) <= 0x094C)) ||
	                    ((a_c) == 0x094D) ||
	                    (((a_c) >= 0x0951) && ((a_c) <= 0x0954)) ||
	                    (((a_c) >= 0x0962) && ((a_c) <= 0x0963)) ||
	                    (((a_c) >= 0x0981) && ((a_c) <= 0x0983)) ||
	                    ((a_c) == 0x09BC) ||
	                    ((a_c) == 0x09BE) ||
	                    ((a_c) == 0x09BF) ||
	                    (((a_c) >= 0x09C0) && ((a_c) <= 0x09C4)) ||
	                    (((a_c) >= 0x09C7) && ((a_c) <= 0x09C8)) ||
	                    (((a_c) >= 0x09CB) && ((a_c) <= 0x09CD)) ||
	                    ((a_c) == 0x09D7) ||
	                    (((a_c) >= 0x09E2) && ((a_c) <= 0x09E3)) ||
	                    (((a_c) >= 0x0A02) && (	/* accelerator */
	                         ((a_c) == 0x0A02) ||
	                         ((a_c) == 0x0A3C) ||
	                         ((a_c) == 0x0A3E) ||
	                         ((a_c) == 0x0A3F) ||
	                         (((a_c) >= 0x0A40) && ((a_c) <= 0x0A42)) ||
	                         (((a_c) >= 0x0A47) && ((a_c) <= 0x0A48)) ||
	                         (((a_c) >= 0x0A4B) && ((a_c) <= 0x0A4D)) ||
	                         (((a_c) >= 0x0A70) && ((a_c) <= 0x0A71)) ||
	                         (((a_c) >= 0x0A81) && ((a_c) <= 0x0A83)) ||
	                         ((a_c) == 0x0ABC) ||
	                         (((a_c) >= 0x0ABE) && ((a_c) <= 0x0AC5)) ||
	                         (((a_c) >= 0x0AC7) && ((a_c) <= 0x0AC9)) ||
	                         (((a_c) >= 0x0ACB) && ((a_c) <= 0x0ACD)) ||
	                         (((a_c) >= 0x0B01) && ((a_c) <= 0x0B03)) ||
	                         ((a_c) == 0x0B3C) ||
	                         (((a_c) >= 0x0B3E) && ((a_c) <= 0x0B43)) ||
	                         (((a_c) >= 0x0B47) && ((a_c) <= 0x0B48)) ||
	                         (((a_c) >= 0x0B4B) && ((a_c) <= 0x0B4D)) ||
	                         (((a_c) >= 0x0B56) && ((a_c) <= 0x0B57)) ||
	                         (((a_c) >= 0x0B82) && ((a_c) <= 0x0B83)) ||
	                         (((a_c) >= 0x0BBE) && ((a_c) <= 0x0BC2)) ||
	                         (((a_c) >= 0x0BC6) && ((a_c) <= 0x0BC8)) ||
	                         (((a_c) >= 0x0BCA) && ((a_c) <= 0x0BCD)) ||
	                         ((a_c) == 0x0BD7) ||
	                         (((a_c) >= 0x0C01) && ((a_c) <= 0x0C03)) ||
	                         (((a_c) >= 0x0C3E) && ((a_c) <= 0x0C44)) ||
	                         (((a_c) >= 0x0C46) && ((a_c) <= 0x0C48)) ||
	                         (((a_c) >= 0x0C4A) && ((a_c) <= 0x0C4D)) ||
	                         (((a_c) >= 0x0C55) && ((a_c) <= 0x0C56)) ||
	                         (((a_c) >= 0x0C82) && ((a_c) <= 0x0C83)) ||
	                         (((a_c) >= 0x0CBE) && ((a_c) <= 0x0CC4)) ||
	                         (((a_c) >= 0x0CC6) && ((a_c) <= 0x0CC8)) ||
	                         (((a_c) >= 0x0CCA) && ((a_c) <= 0x0CCD)) ||
	                         (((a_c) >= 0x0CD5) && ((a_c) <= 0x0CD6)) ||
	                         (((a_c) >= 0x0D02) && ((a_c) <= 0x0D03)) ||
	                         (((a_c) >= 0x0D3E) && ((a_c) <= 0x0D43)) ||
	                         (((a_c) >= 0x0D46) && ((a_c) <= 0x0D48)) ||
	                         (((a_c) >= 0x0D4A) && ((a_c) <= 0x0D4D)) ||
	                         ((a_c) == 0x0D57) ||
	                         (((a_c) >= 0x0E31) && (	/* accelerator */
	                              ((a_c) == 0x0E31) ||
	                              (((a_c) >= 0x0E34) && ((a_c) <= 0x0E3A)) ||
	                              (((a_c) >= 0x0E47) && ((a_c) <= 0x0E4E)) ||
	                              ((a_c) == 0x0EB1) ||
	                              (((a_c) >= 0x0EB4) && ((a_c) <= 0x0EB9)) ||
	                              (((a_c) >= 0x0EBB) && ((a_c) <= 0x0EBC)) ||
	                              (((a_c) >= 0x0EC8) && ((a_c) <= 0x0ECD)) ||
	                              (((a_c) >= 0x0F18) && ((a_c) <= 0x0F19)) ||
	                              ((a_c) == 0x0F35) ||
	                              ((a_c) == 0x0F37) ||
	                              ((a_c) == 0x0F39) ||
	                              ((a_c) == 0x0F3E) ||
	                              ((a_c) == 0x0F3F) ||
	                              (((a_c) >= 0x0F71) && ((a_c) <= 0x0F84)) ||
	                              (((a_c) >= 0x0F86) && ((a_c) <= 0x0F8B)) ||
	                              (((a_c) >= 0x0F90) && ((a_c) <= 0x0F95)) ||
	                              ((a_c) == 0x0F97) ||
	                              (((a_c) >= 0x0F99) && ((a_c) <= 0x0FAD)) ||
	                              (((a_c) >= 0x0FB1) && ((a_c) <= 0x0FB7)) ||
	                              ((a_c) == 0x0FB9) ||
	                              (((a_c) >= 0x20D0) && ((a_c) <= 0x20DC)) ||
	                              ((a_c) == 0x20E1) ||
	                              (((a_c) >= 0x302A) && ((a_c) <= 0x302F)) ||
	                              ((a_c) == 0x3099) ||
	                              ((a_c) == 0x309A))))))))));
}

gboolean
mlview_utils_is_extender (gint a_c)
{
	switch (a_c) {
	case 0x00B7:
	case 0x02D0:
	case 0x02D1:
	case 0x0387:
	case 0x0640:
	case 0x0E46:
	case 0x0EC6:
	case 0x3005:
	case 0x3031:
	case 0x3032:
	case 0x3033:
	case 0x3034:
	case 0x3035:
	case 0x309D:
	case 0x309E:
	case 0x30FC:
	case 0x30FD:
	case 0x30FE:
		return TRUE;
	default:
		return FALSE;
	}
}

gboolean
mlview_utils_is_ideographic (gint a_c)
{
	return (((a_c) < 0x0100) ? 0 :
	        (((a_c) >= 0x4e00) && ((a_c) <= 0x9fa5)) ||
	        (((a_c) >= 0xf900) && ((a_c) <= 0xfa2d)) ||
	        (((a_c) >= 0x3021) && ((a_c) <= 0x3029)) ||
	        ((a_c) == 0x3007));
}

gboolean
mlview_utils_is_name_char (gint a_c)
{
	if (a_c == '.' || a_c == '-' || a_c == '_' || a_c == ':'
	        || mlview_utils_is_letter (a_c) == TRUE
	        || mlview_utils_is_digit (a_c) == TRUE
	        || mlview_utils_is_combining (a_c) == TRUE
	        || mlview_utils_is_extender (a_c) == TRUE) {
		return TRUE ;
	}
	return FALSE ;
}

enum MlViewStatus
mlview_utils_parse_pi (gchar *a_raw_pi,
                       GString **a_pi_target,
                       GString **a_pi_param)
{
	gchar *cur=a_raw_pi ;
	gchar *pi_param_start=NULL,
	                      *pi_param_end=NULL,
	                                    *pi_target_end=NULL;
	GString *pi_target = NULL ;
	gint len=0 ;
	guint32 unichar = 0 ;
	enum MlViewStatus status = MLVIEW_OK ;

	g_return_val_if_fail (a_raw_pi
	                      && a_pi_target
	                      && (*a_pi_target == NULL)
	                      && a_pi_param
	                      && (*a_pi_param == NULL),
	                      MLVIEW_BAD_PARAM_ERROR) ;

	len = strlen ((const char*)a_raw_pi) ;
	if (len < 5)
		return MLVIEW_PARSING_ERROR ;
	if (*cur      == '<'
	        && cur[1] == '?') {
		cur += 2 ;
	} else {
		return MLVIEW_PARSING_ERROR ;
	}
	status = mlview_utils_parse_element_name
	         (cur, &pi_target_end) ;
	if (status != MLVIEW_OK
	        || !pi_target_end) {
		status = MLVIEW_PARSING_ERROR ;
		goto cleanup ;
	}
	pi_target = g_string_new_len
	            ((const gchar*)cur, pi_target_end - cur + 1);
	cur = pi_target_end + 1;
	if (len <= (cur - a_raw_pi)
	        || len -  (cur - a_raw_pi) < 2) {
		status = MLVIEW_PARSING_ERROR ;
		goto cleanup ;
	}
	if (mlview_utils_is_space (*cur) == TRUE) {
		status = mlview_utils_skip_spaces
		         (cur, &cur) ;
		if (status != MLVIEW_OK)
			goto cleanup ;
		pi_param_start = cur ;
	}

	if (pi_param_start)
		goto eat_chars ;

	if (*cur      == '?'
	        && cur[1] == '>') {
		cur += 2 ;
		goto end_of_pi ;
	} else {
		status = MLVIEW_PARSING_ERROR ;
		goto cleanup ;
	}

eat_chars:
	cur = (gchar*)g_utf8_find_next_char ((const gchar*)cur, NULL) ;
	unichar = g_utf8_get_char ((const gchar*)cur) ;
	if (!unichar) {
		status = MLVIEW_EOF_ERROR ;
		goto cleanup ;
	}
	if (unichar == '?') {
		cur = (gchar*) g_utf8_find_next_char ((gchar*)cur, NULL) ;
		unichar = g_utf8_get_char ((gchar*)cur) ;
		if (unichar == '>') {
			pi_param_end = cur - 2 ;
			goto end_of_pi ;
		} else if (!unichar) {
			status = MLVIEW_EOF_ERROR ;
			goto cleanup ;
		} else {
			goto eat_chars ;
		}
	} else {
		goto eat_chars ;
	}

end_of_pi:
	*a_pi_target = pi_target ;
	pi_target = NULL ;
	if (pi_param_start && pi_param_end) {
		*a_pi_param = g_string_new_len
		              ((const gchar*)pi_param_start,
		               pi_param_end - pi_param_start + 1) ;
		pi_param_start = pi_param_end = NULL ;
	}
	status = MLVIEW_OK ;

cleanup:
	if (pi_target) {
		g_string_free (pi_target, TRUE) ;
		pi_target = NULL ;
	}
	return status ;
}

enum MlViewStatus
mlview_utils_parse_element_name (gchar *a_raw_str,
                                 gchar **a_name_end)
{
	gchar *cur = NULL, *prev = NULL;
	gunichar cur_unichar = 0 ;
	gboolean is_ok = TRUE ;
	enum MlViewStatus status = MLVIEW_OK ;

	g_return_val_if_fail (a_raw_str
	                      && a_name_end,
	                      MLVIEW_BAD_PARAM_ERROR) ;
	is_ok = g_utf8_validate ((const gchar*)a_raw_str,
	                         strlen ((const char*)a_raw_str),
	                         NULL) ;
	g_return_val_if_fail (is_ok == TRUE, MLVIEW_ENCODING_ERROR) ;
	*a_name_end = NULL ;
	cur = a_raw_str ;
	cur_unichar = g_utf8_get_char ((const char*)cur) ;
	if (!cur_unichar) {
		status = MLVIEW_EOF_ERROR ;
		goto error ;
	}
	if (cur_unichar != ' '
	        && cur_unichar != ':'
	        && mlview_utils_is_letter (cur_unichar) == FALSE) {
		return MLVIEW_PARSING_ERROR ;
	}
	prev = cur ;
	cur = (gchar*)g_utf8_find_next_char ((const gchar*)prev, NULL) ;
	while (cur && *cur) {
		cur_unichar = g_utf8_get_char ((const gchar*)cur) ;
		if (!cur_unichar) {
			status = MLVIEW_EOF_ERROR ;
			goto error ;
		}
		if (mlview_utils_is_name_char (cur_unichar) == TRUE) {
			prev = cur ;
			cur = (gchar*)g_utf8_find_next_char ((const gchar*)prev, NULL) ;
		} else {
			break ;
		}
	}
	*a_name_end = prev ;
	return MLVIEW_OK ;
error:
	*a_name_end = NULL ;
	return status ;
}

enum MlViewStatus
mlview_utils_parse_element_name2 (GtkTextIter *a_from,
                                  GtkTextIter **a_name_start,
                                  GtkTextIter **a_name_end)
{
	GtkTextIter *cur = NULL, *name_start = NULL, *prev_char_iter = NULL;
	gunichar cur_unichar = 0 ;
	enum MlViewStatus status = MLVIEW_OK ;

	g_return_val_if_fail (a_from && a_name_start && a_name_end,
	                      MLVIEW_BAD_PARAM_ERROR) ;

	cur = gtk_text_iter_copy (a_from) ;
	g_return_val_if_fail (cur, MLVIEW_ERROR) ;
	cur_unichar = gtk_text_iter_get_char (cur) ;
	if (!cur_unichar) {
		status = MLVIEW_EOF_ERROR ;
		goto cleanup ;
	}
	if (cur_unichar != ' '
	        && cur_unichar != ':'
	        && mlview_utils_is_letter (cur_unichar) == FALSE) {
		status = MLVIEW_PARSING_ERROR ;
		goto cleanup ;
	}
	name_start = gtk_text_iter_copy (cur) ;
	if (!name_start) {
		status = MLVIEW_ERROR ;
		goto cleanup ;
	}
	prev_char_iter = gtk_text_iter_copy (cur) ;
	if (!gtk_text_iter_forward_char (cur)) {
		status = MLVIEW_EOF_ERROR ;
		goto cleanup ;
	}
	cur_unichar = gtk_text_iter_get_char (cur) ;
	while (cur && cur_unichar) {
		cur_unichar = gtk_text_iter_get_char (cur) ;
		if (!cur_unichar) {
			status = MLVIEW_EOF_ERROR ;
			goto cleanup ;
		}
		if (mlview_utils_is_name_char (cur_unichar) == TRUE) {
			if (prev_char_iter) {
				gtk_text_iter_free (prev_char_iter) ;
				prev_char_iter = NULL ;
			}
			prev_char_iter = gtk_text_iter_copy (cur) ;
			if (!gtk_text_iter_forward_char (cur)) {
				status = MLVIEW_EOF_ERROR ;
				goto cleanup ;
			}
		} else {
			break ;
		}
	}
	*a_name_start = name_start ;
	name_start = NULL ;
	*a_name_end = prev_char_iter ;
	prev_char_iter = NULL ;

cleanup:
	if (cur) {
		gtk_text_iter_free (cur) ;
		cur = NULL ;
	}
	if (name_start) {
		gtk_text_iter_free (name_start) ;
		name_start = NULL ;
	}
	if (prev_char_iter) {
		gtk_text_iter_free (prev_char_iter) ;
		prev_char_iter = NULL ;
	}
	return status ;
}

enum MlViewStatus
mlview_utils_parse_cdata_section (const gchar *a_raw_str,
                                  gchar **a_out_start,
                                  gchar **a_out_end)
{
	enum MlViewStatus status = MLVIEW_OK;
	gchar *cur = NULL, *start = NULL, *end = NULL ;
	gint len = 0 ;
	g_return_val_if_fail (a_raw_str && a_out_start && a_out_end,
	                      MLVIEW_BAD_PARAM_ERROR) ;

	len = strlen (a_raw_str) ;
	if (len < 12)
		return MLVIEW_PARSING_ERROR ;
	cur = (gchar*)a_raw_str ;
	if (cur[0]    == '<'
	        && cur[1] == '!'
	        && cur[2] == '['
	        && cur[3] == 'C'
	        && cur[4] == 'D'
	        && cur[5] == 'A'
	        && cur[6] == 'T'
	        && cur[7] == 'A'
	        && cur[8] == '[') {
		cur += 9 ;
		if (!*cur)
			return MLVIEW_EOF_ERROR ;
		start = cur ;
	} else {
		return MLVIEW_PARSING_ERROR ;
	}
	for (; cur && *cur && (*cur != ']') ; cur++)
		;
	if (cur == NULL || !*cur || *cur != ']')
		return MLVIEW_EOF_ERROR ;
	len = strlen (cur) ;
	if (len < 3)
		return MLVIEW_PARSING_ERROR ;/*this may be an EOF though*/
	end = cur - 1 ;
	if (cur[1] ==    ']'
	        && cur[2] == '>') {
		*a_out_start = start ;
		*a_out_end = end ;
	}
	return status ;
}

gboolean
mlview_utils_is_space (gint a_c)
{
	if (a_c == 0x20 || a_c == 0x09 || a_c == 0xA || a_c == 0x0D)
		return TRUE ;
	return FALSE ;
}

enum MlViewStatus
mlview_utils_skip_spaces (gchar *a_raw_str,
                          gchar **a_skiped_to)
{
	gchar *cur = NULL ;
	glong unichar = 0 ;

	g_return_val_if_fail (a_raw_str, MLVIEW_BAD_PARAM_ERROR) ;
	*a_skiped_to = NULL ;
	cur = a_raw_str ;
	if (!*cur) {
		return MLVIEW_EOF_ERROR ;
	}
	while (cur) {
		unichar = g_utf8_get_char_validated
		          ((const gchar*)cur,
		           strlen ((const char*)cur)) ;
		if (unichar == -1) {
			return MLVIEW_ENCODING_ERROR ;
		}
		if (mlview_utils_is_space (unichar) == TRUE) {
			cur = (gchar*)g_utf8_find_next_char ((const gchar*)cur, NULL) ;
			if (!cur) {
				return MLVIEW_EOF_ERROR ;
			}
		} else {
			break ;
		}
	}
	*a_skiped_to = cur ;
	return MLVIEW_OK ;
}

enum MlViewStatus
mlview_utils_skip_spaces2 (GtkTextIter *a_from,
                           GtkTextIter **a_skiped_to)
{
	enum MlViewStatus status = MLVIEW_OK ;
	GtkTextIter *cur = NULL ;
	glong unichar = 0 ;

	g_return_val_if_fail (a_from, MLVIEW_BAD_PARAM_ERROR) ;
	*a_skiped_to = NULL ;
	cur = gtk_text_iter_copy (a_from) ;
	if (!cur) {
		return MLVIEW_ERROR ;
	}
	while (cur) {
		unichar = gtk_text_iter_get_char (cur) ;
		if (!unichar) {
			return MLVIEW_ENCODING_ERROR ;
		}
		if (mlview_utils_is_space (unichar) == TRUE) {
			if (!gtk_text_iter_forward_char (cur)) {
				status = MLVIEW_EOF_ERROR ;
				goto error ;
			}
		} else {
			break ;
		}
	}
error:
	if (status != MLVIEW_OK && cur) {
		gtk_text_iter_free (cur) ;
		cur = NULL ;
	}
	if (cur) {
		*a_skiped_to = cur ;
	}
	return status ;
}

enum MlViewStatus
mlview_utils_parse_reference (gchar *a_raw_str,
                              gchar **a_ref_end)
{
	gchar *cur = NULL, *tmp = NULL ;
	enum MlViewStatus status = MLVIEW_OK ;

	g_return_val_if_fail (a_raw_str && a_ref_end,
	                      MLVIEW_BAD_PARAM_ERROR) ;
	cur = a_raw_str ;
	if (!*cur) {
		status = MLVIEW_EOF_ERROR ;
		goto error ;
	}
	if (*cur == '&' || *cur == '%') {
		status = mlview_utils_parse_element_name (cur, &tmp) ;
		if (status != MLVIEW_OK) {
			goto error ;
		}
	} else {
		status = MLVIEW_PARSING_ERROR ;
		goto error ;
	}
	cur = tmp + 1 ;
	if (!*cur) {
		status = MLVIEW_EOF_ERROR ;
		goto error ;
	}
	if (*cur != ';') {
		status = MLVIEW_PARSING_ERROR ;
		goto error ;
	}
	*a_ref_end = tmp ;
	return MLVIEW_OK ;

error:
	*a_ref_end = NULL;
	return status ;
}

enum MlViewStatus
mlview_utils_parse_reference2 (GtkTextIter *a_from,
                               GtkTextIter **a_ref_end)
{
	GtkTextIter *cur = NULL, *name_start = NULL,
	                                       *name_end = NULL ;
	gunichar cur_char = 0 ;
	enum MlViewStatus status = MLVIEW_OK ;

	g_return_val_if_fail (a_from && a_ref_end,
	                      MLVIEW_BAD_PARAM_ERROR) ;

	cur = gtk_text_iter_copy (a_from) ;
	if (!cur) {
		status = MLVIEW_EOF_ERROR ;
		goto cleanup ;
	}
	cur_char = gtk_text_iter_get_char (cur) ;
	if (!cur_char) {
		status = MLVIEW_ENCODING_ERROR ;
		goto cleanup ;
	}
	if (cur_char == '&' || cur_char == '%') {
		status = mlview_utils_parse_element_name2
		         (cur, &name_start, &name_end) ;
		if (status != MLVIEW_OK) {
			goto cleanup ;
		}
	} else {
		status = MLVIEW_PARSING_ERROR ;
		goto cleanup ;
	}
	if (cur) {
		gtk_text_iter_free (cur) ;
		cur= NULL ;
	}
	cur = mlview_utils_text_iter_forward_chars_dup (name_end, 1) ;
	if (!cur) {
		status = MLVIEW_EOF_ERROR ;
		goto cleanup ;
	}
	cur_char = gtk_text_iter_get_char (cur) ;
	if (cur_char != ';') {
		status = MLVIEW_PARSING_ERROR ;
		goto cleanup ;
	}
	*a_ref_end = name_end;
	status = MLVIEW_OK ;
	name_end = NULL ;

cleanup :
	if (cur) {
		gtk_text_iter_free (cur) ;
		cur = NULL ;
	}
	if (name_start) {
		gtk_text_iter_free (name_start) ;
		name_start = NULL ;
	}
	if (name_end) {
		gtk_text_iter_free (name_end) ;
		name_end = NULL ;
	}
	return status ;
}
enum MlViewStatus
mlview_utils_parse_attribute (gchar *a_raw_str,
                              gchar **a_name_end,
                              gchar **a_val_start,
                              gchar **a_val_end)
{
	gchar *cur = NULL , *tmp = NULL;
	enum MlViewStatus status = MLVIEW_OK ;
	gint delim = 0 ;
	g_return_val_if_fail (a_raw_str
	                      && a_name_end
	                      && a_val_start
	                      && a_val_end,
	                      MLVIEW_BAD_PARAM_ERROR) ;

	cur = a_raw_str ;
	if (!*cur) {
		status = MLVIEW_EOF_ERROR ;
		goto error ;
	}
	status = mlview_utils_parse_element_name (cur, a_name_end) ;
	if (status != MLVIEW_OK) {
		goto error ;
	}
	cur = *a_name_end + 1 ;
	if (!*cur) {
		status = MLVIEW_EOF_ERROR ;
		goto error ;
	}
	status = mlview_utils_skip_spaces (cur, &tmp) ;
	if (status != MLVIEW_OK) {
		goto error ;
	}
	cur = tmp;
	if (!*cur) {
		status = MLVIEW_EOF_ERROR ;
		goto error ;
	}
	if (*cur != '=') {
		status = MLVIEW_PARSING_ERROR ;
		goto error ;
	}
	cur ++ ;
	if (!*cur) {
		status = MLVIEW_EOF_ERROR ;
		goto error ;
	}
	status = mlview_utils_skip_spaces (cur, &tmp) ;
	if (status != MLVIEW_OK) {
		goto error ;
	}
	cur = tmp;
	if (!*cur) {
		status = MLVIEW_EOF_ERROR ;
		goto error ;
	}
	if (*cur != '\"' && *cur != '\'') {
		status = MLVIEW_PARSING_ERROR ;
		goto error ;
	}
	delim = *cur ;
	cur ++ ;
	if (!*cur) {
		status = MLVIEW_EOF_ERROR ;
		goto error ;
	}
	/*
	 *now, try to parse attribute value.
	 */
	*a_val_start = cur ;
	while (cur && *cur) {
		status = mlview_utils_parse_reference (cur, &tmp) ;
		if (status == MLVIEW_OK) {
			/*continue the parsing*/
			cur = tmp + 1 ;
			if (!*cur) {
				status = MLVIEW_EOF_ERROR ;
				goto error ;
			}
		} else if (status == MLVIEW_PARSING_ERROR) {
			/*end of the attribute value may be reached here.*/
			if (*cur == '<'
			        || *cur == '&'
			        || *cur == delim) {
				break ;
			}
		} else {
			/*we are sure there is an error, get out.*/
			goto error ;
		}
		cur ++ ;
	}
	if (*cur == delim) {
		*a_val_end = cur - 1 ;
	} else {
		status = MLVIEW_PARSING_ERROR ;
		goto error ;
	}
	if (a_val_start == a_val_end) {
		*a_val_start = *a_val_end = NULL ;
	}
	return MLVIEW_OK ;
error:
	*a_name_end = NULL ;
	*a_val_start = NULL ;
	*a_val_end = NULL ;
	return status ;
}

enum MlViewStatus
mlview_utils_parse_attribute2 (GtkTextIter *a_from,
                               GtkTextIter **a_name_end,
                               GtkTextIter **a_val_start,
                               GtkTextIter **a_val_end)
{
	GtkTextIter *cur = NULL , *name_end = NULL, *name_start = NULL,
	                                      *val_start = NULL , *val_end = NULL, *tmp = NULL ;
	enum MlViewStatus status = MLVIEW_OK ;
	guint delim = 0 ;
	gunichar cur_char = 0 ;

	g_return_val_if_fail (a_from
	                      && a_name_end
	                      && a_val_start
	                      && a_val_end,
	                      MLVIEW_BAD_PARAM_ERROR) ;

	cur = gtk_text_iter_copy (a_from) ;
	if (!cur) {
		status = MLVIEW_ERROR ;
		goto cleanup ;
	}
	status = mlview_utils_parse_element_name2 (cur, &name_start, &name_end) ;
	if (status != MLVIEW_OK) {
		goto cleanup ;
	}
	if (cur) {
		gtk_text_iter_free (cur) ;
		cur = NULL ;
	}
	cur = mlview_utils_text_iter_forward_chars_dup (name_end, 1) ;
	if (!cur) {
		status = MLVIEW_ERROR ;
		goto cleanup ;
	}
	status = mlview_utils_skip_spaces2 (cur, &tmp) ;
	if (status != MLVIEW_OK) {
		goto cleanup ;
	}
	if (cur) {
		gtk_text_iter_free (cur) ;
		cur = NULL ;
	}
	cur = tmp;
	tmp = NULL ;
	cur_char = gtk_text_iter_get_char (cur) ;

	if (cur_char != '=') {
		status = MLVIEW_PARSING_ERROR ;
		goto cleanup ;
	}
	if (!gtk_text_iter_forward_char (cur)) {
		status = MLVIEW_EOF_ERROR ;
		goto cleanup ;
	}
	status = mlview_utils_skip_spaces2 (cur, &tmp) ;
	if (status != MLVIEW_OK) {
		goto cleanup ;
	}
	if (cur) {
		gtk_text_iter_free (cur) ;
		cur = NULL ;
	}
	cur = tmp;
	cur_char = gtk_text_iter_get_char (cur) ;
	if (!cur_char) {
		status = MLVIEW_EOF_ERROR ;
		goto cleanup ;
	}
	if (cur_char != '\"' && cur_char != '\'') {
		status = MLVIEW_PARSING_ERROR ;
		goto cleanup ;
	}
	delim = cur_char ;
	if (!gtk_text_iter_forward_char (cur)) {
		status = MLVIEW_ERROR ;
		goto cleanup ;
	}
	val_start = gtk_text_iter_copy (cur) ;
	if (!val_start) {
		status = MLVIEW_ERROR ;
		goto cleanup ;
	}

	/*
	 *now, try to parse attribute value.
	 */
	while (cur) {
		status = mlview_utils_parse_reference2 (cur, &tmp) ;
		if (status == MLVIEW_OK) {
			/*continue the parsing*/
			if (cur) {
				gtk_text_iter_free (cur) ;
				cur = NULL ;
			}
			cur = mlview_utils_text_iter_forward_chars_dup (tmp, 1) ;
			if (!cur) {
				status = MLVIEW_EOF_ERROR ;
				goto cleanup ;
			}
		} else if (status == MLVIEW_PARSING_ERROR) {
			/*end of the attribute value may be reached here.*/
			cur_char = gtk_text_iter_get_char (cur) ;
			if (cur_char == '<'
			        || cur_char == '&'
			        || cur_char == delim) {
				if (cur_char == delim) {
					/*save an iter to end of the att value.*/
					gtk_text_iter_backward_char (cur) ;
					val_end = gtk_text_iter_copy (cur) ;
				}
				break ;
			}
		} else {
			/*we are sure there is an error, get out.*/
			goto cleanup ;
		}
		if (!gtk_text_iter_forward_char (cur)) {
			status = MLVIEW_EOF_ERROR ;
		}
	}
	cur_char = gtk_text_iter_get_char (cur) ;
	if (!val_end) {
		status = MLVIEW_PARSING_ERROR ;
		goto cleanup ;
	}
	*a_name_end = name_end ;
	name_end = NULL ;

	if (gtk_text_iter_compare (val_start, val_end)) {
		gtk_text_iter_free (val_start) ;
		val_start = NULL ;
		gtk_text_iter_free (val_end) ;
		val_end = NULL ;
	} else {
		*a_val_start = val_start ;
		val_start = NULL ;
		*a_val_end = val_end ;
		val_end = NULL ;
	}

cleanup:
	if (val_start) {
		gtk_text_iter_free (val_start) ;
		val_start = NULL ;
	}
	if (val_end) {
		gtk_text_iter_free (val_end) ;
		val_end = NULL ;
	}
	if (cur) {
		gtk_text_iter_free (cur) ;
		cur = NULL ;
	}
	if (name_end) {
		gtk_text_iter_free (name_end) ;
		name_end = NULL ;
	}
	return status ;
}

struct NameValuePair *
			mlview_utils_name_value_pair_new (GString *a_name,
			                                  GString *a_value)
{
	struct NameValuePair *result = NULL ;

	result = (struct NameValuePair*)g_try_malloc (sizeof (struct NameValuePair)) ;
	if (!result)
	{
		mlview_utils_trace_debug ("g_try_malloc failed") ;
		return NULL ;
	}
	memset (result, 0, sizeof (struct NameValuePair)) ;
	result->name = a_name ;
	result->value = a_value ;
	return result ;
}

void
mlview_utils_name_value_pair_free (struct NameValuePair *a_this,
                                   gboolean a_free_strings)
{
	g_return_if_fail (a_this) ;

	if (a_free_strings)
	{
		if (a_this->name) {
			g_string_free (a_this->name, TRUE) ;
			a_this->name = NULL ;
		}
		if (a_this->value) {
			g_string_free (a_this->value, TRUE) ;
			a_this->value = NULL ;
		}
	}
	g_free (a_this) ;
}

void
mlview_utils_name_value_pair_list_free (GList *a_nv_pair_list,
                                        gboolean a_free_strings)
{
	GList *cur_list_item = NULL ;

	g_return_if_fail (a_nv_pair_list) ;

	for (cur_list_item = a_nv_pair_list ; cur_list_item ;
	        cur_list_item = cur_list_item->next) {
		if (cur_list_item->data) {
			mlview_utils_name_value_pair_free
			((struct NameValuePair*)cur_list_item->data, a_free_strings) ;
		}
	}
	g_list_free (a_nv_pair_list) ;
}

enum MlViewStatus
mlview_utils_parse_comment (gchar *a_raw_str,
                            GString **a_comment)
{
	gint len=0, i=0 ;
	gchar *cur = a_raw_str ;
	gchar *comment_start = NULL ;
	gchar *comment_end = NULL ;
	gboolean parsing_ok = FALSE ;

	g_return_val_if_fail (a_raw_str && a_comment
	                      && (*a_comment == NULL),
	                      MLVIEW_BAD_PARAM_ERROR) ;

	len = strlen ((const char*)a_raw_str) ;
	if (len < 7)
		return MLVIEW_PARSING_ERROR ;
	if (cur[i]      ==  '<'
	        && cur[i+1] ==  '!'
	        && cur[i+2] ==  '-'
	        && cur[i+3] ==  '-') {
		i += 4 ;
		comment_start = &cur[i] ;
	} else {
		return MLVIEW_PARSING_ERROR ;
	}
	while (len - i >= 3) {
		if (cur[i]        == '-'
		        && cur[i + 1] == '-'
		        && cur[i + 2] == '>') {
			parsing_ok = TRUE ;
			comment_end = &cur[i-1] ;
			break ;
		}
		i++ ;
	}
	*a_comment = g_string_new_len
	             ((const gchar*)comment_start,
	              comment_end - comment_start + 1) ;
	if (!*a_comment) {
		mlview_utils_trace_debug
		("!*a_comment failed") ;
		return MLVIEW_ERROR ;
	}
	return MLVIEW_OK ;
}

enum MlViewStatus
mlview_utils_parse_start_tag (gchar *a_raw_str,
                              GString **a_name,
                              GList **a_name_value_pair_list)
{
	gchar *cur = NULL, *name_start = NULL,
	                                 *name_end = NULL, *tmp = NULL,
	                                                          *attr_name_start=NULL,
	                                                                           *attr_name_end = NULL,
	                                                                                            *attr_value_start = NULL,
	                                                                                                                *attr_value_end = NULL ;
	enum MlViewStatus status = MLVIEW_OK ;
	GList *nv_pair_list = NULL ;
	GString *attr_name = NULL, *attr_value = NULL ;

	g_return_val_if_fail (a_raw_str, MLVIEW_BAD_PARAM_ERROR) ;

	cur = a_raw_str ;
	if (*cur != '<') {
		return MLVIEW_NO_LEFT_ANGLE_BRACKET_ERROR ;
	}
	if (cur[strlen ((const char*)cur) -1] != '>') {
		return MLVIEW_NO_RIGHT_ANGLE_BRACKET_ERROR ;
	}
	cur ++ ;
	if (!*cur) {
		status = MLVIEW_EOF_ERROR ;
		goto error ;
	}
	name_start = cur ;
	status = mlview_utils_parse_element_name (cur, &name_end) ;
	if (status != MLVIEW_OK) {
		goto error ;
	}
	cur = name_end + 1 ;
	if (!*cur) {
		status = MLVIEW_EOF_ERROR ;
		goto error ;
	}
	status = mlview_utils_skip_spaces (cur, &tmp) ;
	if (status != MLVIEW_OK || !tmp) {
		goto error ;
	}
	cur = tmp ;
	if (!*cur) {
		status = MLVIEW_EOF_ERROR ;
		goto error ;
	}
	status = mlview_utils_parse_attribute (cur, &attr_name_end,
	                                       &attr_value_start,
	                                       &attr_value_end) ;
	if (status == MLVIEW_OK) {
		if (attr_name_end) {
			attr_name_start = cur ;
		} else {
			status = MLVIEW_ERROR ;
			goto error ;
		}
		if (attr_value_end) {
			cur = attr_value_end + 1 ;
		} else {
			cur = attr_name_end + 1 ;
		}
	}
	while (status == MLVIEW_OK && cur && *cur) {
		struct NameValuePair *nv_pair = NULL ;
		if (!attr_name_end && *cur != '"' && *cur != '\'') {
			status = MLVIEW_ERROR ;
			goto error ;
		}
		cur ++ ;
		if (!cur || !*cur || !attr_name_start) {
			status = MLVIEW_PARSING_ERROR ;
			goto error ;
		}
		attr_name = g_string_new_len
		            ((const gchar*)attr_name_start,
		             attr_name_end - attr_name_start + 1) ;
		if (attr_value_start) {
			attr_value = g_string_new_len
			             ((const gchar*)attr_value_start,
			              attr_value_end - attr_value_start + 1) ;
		} else {
			attr_value = NULL ;
		}
		attr_value_start = attr_value_end =
		                       attr_name_start = attr_name_end = NULL ;
		nv_pair = mlview_utils_name_value_pair_new
		          (attr_name, attr_value) ;
		if (!nv_pair) {
			status = MLVIEW_OUT_OF_MEMORY_ERROR ;
			goto error ;
		}
		nv_pair_list = g_list_append (nv_pair_list, nv_pair) ;
		nv_pair = NULL ;
		status = mlview_utils_skip_spaces (cur, &tmp) ;
		if (status != MLVIEW_OK || !tmp) {
			goto error ;
		}
		cur = tmp ;
		if (!*cur) {
			status = MLVIEW_EOF_ERROR ;
			goto error ;
		}
		tmp = NULL ;
		status = mlview_utils_parse_attribute (cur, &attr_name_end,
		                                       &attr_value_start,
		                                       &attr_value_end) ;
		if (status != MLVIEW_OK && status != MLVIEW_PARSING_ERROR) {
			goto error ;
		}
		if (status == MLVIEW_OK) {
			if (attr_name_end) {
				attr_name_start = cur ;
			} else {
				status = MLVIEW_ERROR ;
				goto error ;
			}
			if (attr_value_end) {
				cur = attr_value_end + 1 ;
			} else {
				cur = attr_name_end + 1 ;
			}
		}
	}
	if (*cur != '>'
	        && (*cur != '/' || *(cur + 1) != '>')) {
		status = MLVIEW_PARSING_ERROR ;
		goto error ;
	}
	status = MLVIEW_OK ;
	*a_name = g_string_new_len
	          ((const gchar*)name_start,
	           name_end - name_start + 1) ;
	if (!*a_name) {
		status = MLVIEW_ERROR ;
		goto error ;
	}
	name_start = name_end = NULL ;
	*a_name_value_pair_list = nv_pair_list ;
	nv_pair_list = NULL ;
	return status ;

error:
	if (status == MLVIEW_OK)
		status = MLVIEW_ERROR ;
	if (nv_pair_list) {
		GList *cur_item = NULL;
		for (cur_item = nv_pair_list; cur_item ;
		        cur_item = cur_item->next) {
			if (cur_item->data) {
				mlview_utils_name_value_pair_free
				((struct NameValuePair*)cur_item->data,
				 TRUE) ;
			}
		}
	}
	return status ;
}

/**
 * parses a start tag from GtkTextBuffer.
 * @param a_from where to start the parsing from.
 * @param a_name out parameter. The name of the parsed tag, if
 * the parsing occured correctly. Must be freed by the caller using
 * g_string_free().
 * @param a_name_value_pair_list out parameter. Contains attribute list
 * of the parsed start tag, if the parsing occured correctly.
 * @param a_to out parameter. Points to the last caracter of the parsed
 * start tag. This parameter is set if and only if the parsing was successful.
 * @param a_is_empty_tag out parameter. Is set to TRUE if the parsed start tag
 * is an empty tag, FALSE otherwise. This parameter is set if and only the parsing
 * was sucessful.
 * @return MLVIEW_OK upon successful completion, an error code otherwise.
 */
enum MlViewStatus
mlview_utils_parse_start_tag2 (GtkTextIter *a_from,
                               GString **a_name,
                               GList **a_name_value_pair_list,
                               GtkTextIter **a_to,
                               gboolean *a_is_empty_tag)
{
	GtkTextIter *cur = NULL, *name_start = NULL,
	                                       *name_end = NULL, *tmp =  {0},
	                                                                 *attr_name_start = NULL ,
	                                                                                    *attr_name_end = NULL ,
	                                                                                                     *attr_value_start = NULL,
	                                                                                                                         *attr_value_end = NULL ;
	enum MlViewStatus status = MLVIEW_OK ;
	gunichar cur_char = 0, next_char = 0 ;
	GList *nv_pair_list = NULL ;
	GString *attr_name = NULL, *attr_value = NULL ;

	g_return_val_if_fail (a_from, MLVIEW_BAD_PARAM_ERROR) ;

	cur = gtk_text_iter_copy (a_from) ;
	g_return_val_if_fail (cur, MLVIEW_ERROR) ;
	cur_char = gtk_text_iter_get_char (cur) ;
	if (cur_char != '<') {
		return MLVIEW_NO_LEFT_ANGLE_BRACKET_ERROR ;
	}
	if (!gtk_text_iter_forward_char (cur)) {
		status = MLVIEW_PARSING_ERROR ;
		goto cleanup;
	}
	cur_char = gtk_text_iter_get_char (cur) ;

	name_start = gtk_text_iter_copy (cur) ;
	;
	if (!name_start) {
		status = MLVIEW_ERROR ;
		goto cleanup;
	}
	status = mlview_utils_parse_element_name2 (cur, &name_start, &name_end) ;
	if (status != MLVIEW_OK) {
		goto cleanup;
	}
	if (cur) {
		gtk_text_iter_free (cur) ;
		cur = NULL ;
	}
	cur = mlview_utils_text_iter_forward_chars_dup (name_end, 1) ;
	if (!cur) {
		status = MLVIEW_ERROR ;
		goto cleanup;
	}
	status = mlview_utils_skip_spaces2 (cur, &tmp) ;
	if (status != MLVIEW_OK || !tmp) {
		goto cleanup;
	}
	if (cur) {
		gtk_text_iter_free (cur) ;
		cur = NULL ;
	}
	cur = tmp ;
	status = mlview_utils_parse_attribute2 (cur, &attr_name_end,
	                                        &attr_value_start,
	                                        &attr_value_end) ;
	if (status == MLVIEW_OK) {
		if (attr_name_end) {
			attr_name_start = gtk_text_iter_copy (cur) ;
		} else {
			status = MLVIEW_ERROR ;
			goto cleanup;
		}
		if (attr_value_end) {
			cur = mlview_utils_text_iter_forward_chars_dup
			      (attr_value_end, 1) ;
		} else {
			cur = mlview_utils_text_iter_forward_chars_dup
			      (attr_name_end, 1) ;
		}
	}
	while (status == MLVIEW_OK && cur) {
		struct NameValuePair *nv_pair = NULL ;
		cur_char = 0 ;
		cur_char = gtk_text_iter_get_char (cur) ;
		if (!attr_name_end && cur_char != '"' && cur_char != '\'') {
			status = MLVIEW_ERROR ;
			goto cleanup;
		}
		if (!gtk_text_iter_forward_char (cur)) {
			status = MLVIEW_ERROR ;
			goto cleanup;
		}
		cur_char = gtk_text_iter_get_char (cur) ;

		if (!cur_char || !attr_name_start) {
			status = MLVIEW_PARSING_ERROR ;
			goto cleanup;
		}
		if (attr_name) {
			g_string_free (attr_name, TRUE) ;
			attr_name = NULL ;
		}
		attr_name = g_string_new (gtk_text_iter_get_text (attr_name_start,
		                          attr_name_end)) ;
		if (attr_value_start) {
			attr_value = g_string_new (gtk_text_iter_get_text
			                           (attr_value_start,
			                            attr_value_end)) ;
			if (!attr_value) {
				mlview_utils_trace_debug
				("could not allocated attr_value") ;
				status = MLVIEW_ERROR ;
				goto cleanup ;
			}
		} else {
			if (attr_value) {
				g_string_free (attr_value, TRUE) ;
				attr_value = NULL ;
			}
		}
		if (attr_value_start) {
			gtk_text_iter_free (attr_value_start) ;
			attr_value_start = NULL ;
		}
		if (attr_value_end) {
			gtk_text_iter_free (attr_value_end) ;
			attr_value_end = NULL ;
		}
		if (attr_name_start) {
			gtk_text_iter_free (attr_name_start) ;
			attr_name_start = NULL ;
		}
		if (attr_name_end) {
			gtk_text_iter_free (attr_name_end) ;
			attr_name_end = NULL ;
		}
		nv_pair = mlview_utils_name_value_pair_new
		          (attr_name, attr_value) ;
		if (!nv_pair) {
			status = MLVIEW_OUT_OF_MEMORY_ERROR ;
			goto cleanup;
		}
		attr_name = NULL ;
		attr_value = NULL ;
		nv_pair_list = g_list_append (nv_pair_list, nv_pair) ;
		nv_pair = NULL ;
		status = mlview_utils_skip_spaces2 (cur, &tmp) ;
		if (status != MLVIEW_OK || !tmp) {
			if (status == MLVIEW_OK)
				status = MLVIEW_ERROR ;
			goto cleanup;
		}
		gtk_text_iter_free (cur) ;
		cur = tmp ;
		cur_char = gtk_text_iter_get_char (cur) ;
		if (!cur_char) {
			status = MLVIEW_EOF_ERROR ;
			goto cleanup ;
		}
		tmp = NULL ;
		status = mlview_utils_parse_attribute2 (cur, &attr_name_end,
		                                        &attr_value_start,
		                                        &attr_value_end) ;
		if (status != MLVIEW_OK && status != MLVIEW_PARSING_ERROR) {
			goto cleanup ;
		}
		if (status == MLVIEW_OK) {
			if (attr_name_end) {
				attr_name_start = gtk_text_iter_copy (cur) ;
			} else {
				status = MLVIEW_ERROR ;
				goto cleanup ;
			}
			if (attr_value_end) {
				cur =  mlview_utils_text_iter_forward_chars_dup
				       (attr_value_end, 1) ;
			} else {
				cur = mlview_utils_text_iter_forward_chars_dup
				      (attr_name_end, 1) ;
			}
		}
	}
	cur_char = gtk_text_iter_get_char (cur) ;
	mlview_utils_text_iter_get_char_at (cur, 1, &next_char) ;

	if (cur_char != '>'
	        && (cur_char != '/' || next_char != '>')) {
		status = MLVIEW_PARSING_ERROR ;
		goto cleanup ;
	}
	if (!gtk_text_iter_forward_char (name_end)) {
		status = MLVIEW_ERROR ;
		goto cleanup ;
	}
	status = MLVIEW_OK ;
	*a_name = g_string_new (gtk_text_iter_get_text (name_start, name_end)) ;
	if (!*a_name) {
		status = MLVIEW_ERROR ;
		goto cleanup ;
	}
	name_start = name_end = NULL ;

	/*set to a_to ptr to the end of the ending tag.*/
	if (cur_char == '>') {
		mlview_utils_text_iter_get_iter_at (cur, 1, a_to) ;
		*a_is_empty_tag = FALSE ;
	} else {
		/*next chars are: '/>'*/
		mlview_utils_text_iter_get_iter_at (cur, 2, a_to) ;
		*a_is_empty_tag = TRUE ;
	}
	*a_name_value_pair_list = nv_pair_list ;
	nv_pair_list = NULL ;

cleanup:
	if (nv_pair_list) {
		GList *cur_item = NULL;
		for (cur_item = nv_pair_list; cur_item ;
		        cur_item = cur_item->next) {
			if (cur_item->data) {
				mlview_utils_name_value_pair_free
				((struct NameValuePair*)cur_item->data, TRUE) ;
			}
		}
	}
	if (name_start) {
		gtk_text_iter_free (name_start) ;
		name_start = NULL ;
	}
	if (name_end) {
		gtk_text_iter_free (name_end) ;
		name_end = NULL ;
	}
	if (cur) {
		gtk_text_iter_free (cur) ;
		cur = NULL ;
	}
	if (attr_name_start) {
		gtk_text_iter_free (attr_name_start) ;
		attr_name_start = NULL ;
	}
	if (attr_name_end) {
		gtk_text_iter_free (attr_name_end) ;
		attr_name_end = NULL ;
	}
	if (attr_value_start) {
		gtk_text_iter_free (attr_value_start) ;
		attr_value_start = NULL ;
	}
	if (attr_value_end) {
		gtk_text_iter_free (attr_value_end) ;
		attr_value_end = NULL ;
	}
	if (attr_name) {
		g_string_free (attr_name, TRUE) ;
		attr_name = NULL ;
	}
	if (attr_value) {
		g_string_free (attr_value, TRUE) ;
		attr_value = NULL ;
	}
	return status ;
}

enum MlViewStatus
mlview_utils_parse_closing_tag2 (GtkTextIter *a_from,
                                 GString **a_name)
{
	GtkTextIter *cur = NULL, *name_start = NULL,
	                                       *name_end = NULL, *tmp =  {0} ;
	enum MlViewStatus status = MLVIEW_OK ;
	gunichar cur_char = 0, next_char = 0 ;

	g_return_val_if_fail (a_from, MLVIEW_BAD_PARAM_ERROR) ;

	cur = gtk_text_iter_copy (a_from) ;
	g_return_val_if_fail (cur, MLVIEW_ERROR) ;
	cur_char = gtk_text_iter_get_char (cur) ;
	if (cur_char != '<') {
		return MLVIEW_NO_LEFT_ANGLE_BRACKET_ERROR ;
	}
	if (!gtk_text_iter_forward_char (cur)) {
		status = MLVIEW_PARSING_ERROR ;
		goto cleanup;
	}
	cur_char = gtk_text_iter_get_char (cur) ;
	if (cur_char != '/') {
		status = MLVIEW_PARSING_ERROR ;
		goto cleanup ;
	}
	if (!gtk_text_iter_forward_char (cur)) {
		status = MLVIEW_PARSING_ERROR ;
		goto cleanup ;
	}

	name_start = gtk_text_iter_copy (cur) ;
	;
	if (!name_start) {
		status = MLVIEW_ERROR ;
		goto cleanup;
	}
	status = mlview_utils_parse_element_name2 (cur, &name_start, &name_end) ;
	if (status != MLVIEW_OK) {
		goto cleanup;
	}
	if (cur) {
		gtk_text_iter_free (cur) ;
		cur = NULL ;
	}
	cur = mlview_utils_text_iter_forward_chars_dup (name_end, 1) ;
	if (!cur) {
		status = MLVIEW_ERROR ;
		goto cleanup;
	}
	status = mlview_utils_skip_spaces2 (cur, &tmp) ;
	if (status != MLVIEW_OK || !tmp) {
		if (status == MLVIEW_OK) {
			status = MLVIEW_ERROR ;
		}
		goto cleanup;
	}
	if (cur) {
		gtk_text_iter_free (cur) ;
		cur = NULL ;
	}
	cur = tmp ;
	cur_char = gtk_text_iter_get_char (cur) ;
	mlview_utils_text_iter_get_char_at (cur, 1, &next_char) ;

	if (cur_char != '>'
	        && (cur_char != '/' || next_char != '>')) {
		status = MLVIEW_PARSING_ERROR ;
		goto cleanup ;
	}
	if (!gtk_text_iter_forward_char (name_end)) {
		status = MLVIEW_EOF_ERROR ;
		goto cleanup ;
	}
	status = MLVIEW_OK ;
	*a_name = g_string_new (gtk_text_iter_get_text (name_start, name_end)) ;
	if (!*a_name) {
		status = MLVIEW_ERROR ;
		goto cleanup ;
	}

cleanup:

	if (name_start) {
		gtk_text_iter_free (name_start) ;
		name_start = NULL ;
	}
	if (name_end) {
		gtk_text_iter_free (name_end) ;
		name_end = NULL ;
	}
	if (cur) {
		gtk_text_iter_free (cur) ;
		cur = NULL ;
	}
	return status ;
}
struct NameValuePair *
			mlview_utils_name_value_pair_list_lookup (GList *a_nv_pair_list ,
			        const gchar *a_name)
{
	GList *cur_item = NULL ;
	struct NameValuePair *cur_nv_pair = NULL ;
	for (cur_item = a_nv_pair_list ; cur_item;
	        cur_item = cur_item->next)
	{
		cur_nv_pair = (struct NameValuePair*)cur_item->data ;
		if (cur_nv_pair->name
		        && !strcmp (cur_nv_pair->name->str,
		                    (const gchar*)a_name)) {
			return cur_nv_pair ;
		}
	}
	return NULL ;
}


/**
 *Tests if a given string is a white string.
 *@param a_str the string to consider.
 *@return TRUE if a_str is a white string, FALSE if not.
 */
gboolean
mlview_utils_is_white_string (const gchar * a_str)
{
	gchar *tmp_str = (gchar *) a_str;

	if (tmp_str == NULL)
		return FALSE;

	while (*tmp_str) {
		if (mlview_utils_is_space (*tmp_str) == FALSE)
			return FALSE;
		tmp_str++;
	}
	return TRUE;
}

/**
 *Walks a string and escapes the '<' characters to &lt;
 *and '>' into &gt; ;
 *@param a_instr the input string to escape.
 *@param a_outstr the output escaped string. *a_outstr must be
 *freed by the caller.
 *@param a_outstrlen the length of *a_outstr.
 *@param MLVIEW_OK upon successful completion, an error code otherwise.
 */
enum MlViewStatus
mlview_utils_escape_predef_entities_in_str (gchar *a_instr,
        gchar **a_outstr,
        guint *a_outstrlen)
{
	gint instrlen = 0, nb_ab = 0, outstrlen = 0, nb_amp = 0 ;
	gchar *char_ptr = NULL, *outstr = NULL, *outp ;

	g_return_val_if_fail (a_instr
	                      && a_outstr
	                      && a_outstrlen,
	                      MLVIEW_BAD_PARAM_ERROR) ;

	instrlen = strlen ((const gchar*)a_instr) ;
	/*
	 *first, walk through a_instr to count the number
	 *of angle brackets.
	 */
	for (char_ptr = a_instr ; *char_ptr ; char_ptr++) {
		if (*char_ptr == '<' || *char_ptr == '>') {
			nb_ab++ ;
		} else if (*char_ptr == '&'
		           && ! (*(char_ptr + 1)     == 'a'
		                 && *(char_ptr + 2 ) == 'm'
		                 && *(char_ptr + 3 ) == 'p'
		                 && *(char_ptr + 4 ) == ';')
		          ) {
			nb_amp ++ ;
		}
	}
	if (nb_ab || nb_amp) {
		outstrlen = instrlen + nb_ab * 4 + nb_amp * 5 + 1;
		outstr = (gchar*) g_try_malloc (outstrlen) ;
		if (!outstr) {
			mlview_utils_trace_debug
			("malloc failed. system may be out of mem\n") ;
			return MLVIEW_OUT_OF_MEMORY_ERROR ;
		}
	}

	if (outstr) {
		/*
		 *Walk a_strin to escape '<' and '>'.
		 */
		for (char_ptr = a_instr, outp = outstr ;
		        char_ptr && *char_ptr;
		        char_ptr ++) {
			if (*char_ptr == '<') {
				*outp++ = '&' ;
				*outp++ = 'l' ;
				*outp++ = 't' ;
				*outp++ = ';' ;
			} else if (*char_ptr == '>') {
				*outp++ = '&' ;
				*outp++ = 'g' ;
				*outp++ = 't' ;
				*outp++ = ';' ;
			} else if (*char_ptr == '&') {
				*outp++ = '&' ;
				*outp++ = 'a' ;
				*outp++ = 'm' ;
				*outp++ = 'p' ;
				*outp++ = ';' ;
			} else {
				*outp++ = *char_ptr ;
			}
		}
		*outp = 0 ;
	}
	*a_outstrlen = outstrlen ;
	*a_outstr = outstr ;

	return MLVIEW_OK ;
}

/**
 *Given an ISOLAT1 string, this function
 *returns the size (in bytes) this string
 *would have occupied if it was encoded in utf-8.
 *@param a_str the  NULL terminated string to consider.
 *@param a_result_len
 */
enum MlViewStatus
mlview_utils_isolat1_str_len_as_utf8 (const gchar * a_str,
                                      gint * a_result_len)
{
	gint len = 0;
	gchar *char_ptr = NULL;

	g_return_val_if_fail (a_str != NULL
	                      && a_result_len != NULL,
	                      MLVIEW_BAD_PARAM_ERROR);

	for (char_ptr = (gchar *) a_str; *char_ptr; char_ptr++) {

		if ((guchar)*char_ptr <= 0x7F) {
			/*the utf-8 char would take 1 byte */
			len += 1;
		} else {
			/*the utf-8 char would take 2 bytes */
			len += 2;
		}
	}

	*a_result_len = len;

	return MLVIEW_OK;
}

/**
 *Compute the number of ISO 8859_1 characters contained
 *in a null terminated utf8 string.
 *@param a_utf8_str the utf8 string input to consider. Must be
 *not NULL.
 *@a_len out parameter the computed length. This parameter
 *is valid if and only if this function returns MLVIEW_OK. Must be
 *not NULL.
 *@return <ul>
 *<li>MLVIEW_OK upon successufull completion</li>
 *<li>MLVIEW_BAD_PARAM_ERROR if one the parameter is erratic.</li>
 *<li>MLVIEW_BAD_PARAM_ERROR if one the parameter is erratic.</li>
 *<li>MLVIEW_ENCODING_ERROR if a_utf8_str is not a well formed utf8 string</li>
 *<li>MLVIEW_CHAR_TOO_LONG if one of the characters contained
 *in a_utf8_str is too long to be an ISO 8859_1 character. In this
 *case, a_len points to the number of characters in a_utf8_str up to the
 *character that is too long (exclusive)</li>
 *</ul>
 */
enum MlViewStatus
mlview_utils_utf8_str_len_as_isolat1 (const gchar * a_utf8_str,
                                      gint * a_len)
{
	/*
	 *Note: this function can be made shorter
	 *but it considers all the cases of the utf8 encoding
	 *to ease further extensions ...
	 */

	gchar *byte_ptr = NULL;
	gint len = 0;

	/*to store the final decoded
	 *unicode char
	 */
	guint c = 0;

	g_return_val_if_fail (a_utf8_str && a_len,
	                      MLVIEW_BAD_PARAM_ERROR);
	*a_len = 0;

	for (byte_ptr = (gchar *) a_utf8_str;
	        byte_ptr && *byte_ptr; byte_ptr++) {

		gint nb_bytes_2_decode = 0;

		if ((guchar)*byte_ptr <= 0x7F) {
			/*
			 *7 bits long char
			 *encoded over 1 byte:
			 * 0xxx xxxx
			 */
			c = *byte_ptr;
			nb_bytes_2_decode = 1;

		} else if ((*byte_ptr & 0xE0) == 0xC0) {
			/*
			 *up to 11 bits long char.
			 *encoded over 2 bytes:
			 *110x xxxx  10xx xxxx
			 */
			c = *byte_ptr & 0x1F;
			nb_bytes_2_decode = 2;

		} else if ((*byte_ptr & 0xF0) == 0xE0) {
			/*
			 *up to 16 bit long char
			 *encoded over 3 bytes:
			 *1110 xxxx  10xx xxxx  10xx xxxx
			 */
			c = *byte_ptr & 0x0F;
			nb_bytes_2_decode = 3;

		} else if ((*byte_ptr & 0xF8) == 0xF0) {
			/*
			 *up to 21 bits long char
			 *encoded over 4 bytes:
			 *1111 0xxx  10xx xxxx  10xx xxxx  10xx xxxx
			 */
			c = *byte_ptr & 0x7;
			nb_bytes_2_decode = 4;

		} else if ((*byte_ptr & 0xFC) == 0xF8) {
			/*
			 *up to 26 bits long char
			 *encoded over 5 bytes.
			 *1111 10xx  10xx xxxx  10xx xxxx  
			 *10xx xxxx  10xx xxxx
			 */
			c = *byte_ptr & 3;
			nb_bytes_2_decode = 5;

		} else if ((*byte_ptr & 0xFE) == 0xFC) {
			/*
			 *up to 31 bits long char
			 *encoded over 6 bytes:
			 *1111 110x  10xx xxxx  10xx xxxx  
			 *10xx xxxx  10xx xxxx  10xx xxxx
			 */
			c = *byte_ptr & 1;
			nb_bytes_2_decode = 6;

		} else {
			/*BAD ENCODING */
			return MLVIEW_ENCODING_ERROR;
		}

		/*
		 *Go and decode the remaining byte(s)
		 *(if any) to get the current character.
		 */
		for (;
		        nb_bytes_2_decode > 1;
		        nb_bytes_2_decode--) {
			/*decode the next byte */
			byte_ptr++;

			/*byte pattern must be: 10xx xxxx */
			if ((*byte_ptr & 0xC0) != 0x80) {
				return MLVIEW_ENCODING_ERROR;
			}

			c = (c << 6) | (*byte_ptr & 0x3F);
		}

		if (c <= 0xFF) { /*Add other conditions to support
			                                  *other char sets (ucs2, ucs3, ucs4).
			                                  */
			len++;
		} else {
			/*the char is too long to fit
			 *into the supposed charset len.
			 */
			return MLVIEW_CHAR_TOO_LONG_ERROR;
		}
	}

	*a_len = len;

	return MLVIEW_OK;
}


/**
 *Translates a_in_str in a string encoded in utf8.
 *For the time being, a_str is supposed to 
 *be encoded in an isolat1 charset.
 *@param a_in_str the string to translate.
 *@param a_out_str out parameter. a pointer
 *to an allocated buffer where to store the translated string.
 *Note that the caller deallocate *a_out_str 
 *after completion of this function.
 *
 *@return 
 *<ul>
 *<li>MLVIEW_OK if a_str has been successfully translated.
 *<li> error codes in case of error.
 *</ul>
 */
enum MlViewStatus
mlview_utils_isolat1_str_to_utf8 (const gchar * a_in_str,
                                  gchar ** a_out_str)
{
	enum MlViewStatus status = MLVIEW_OK;
	gint len1 = 0,
	            len2 = 0;

	g_return_val_if_fail (a_in_str != NULL
	                      && a_out_str != NULL,
	                      MLVIEW_BAD_PARAM_ERROR);

	*a_out_str = NULL;
	len1 = strlen ((const char*)a_in_str);

	if (len1 == 0)
		return MLVIEW_OK;

	status = mlview_utils_isolat1_str_len_as_utf8 (a_in_str,
	         &len2);
	if (status != MLVIEW_OK)
		return status;

	if (len2 == 0)
		return MLVIEW_OK;

	*a_out_str = (gchar*) g_malloc0 (len2 + 1);

	if (isolat1ToUTF8 ((xmlChar*)*a_out_str, &len2,
	                   (xmlChar*)a_in_str, &len1)) {

		g_free (*a_out_str);
		*a_out_str = NULL;
		return MLVIEW_ERROR;
	}

	return MLVIEW_OK;
}


/**
 *Converts an utf8 string to an ucs1 string.
 *@param a_in_str the NULL terminated 
 *string to convert.
 *@param a_out_str out param. 
 *A pointer to the converted string.
 *Must be free by the caller.
 *@return MLVIEW_OK upon successfull completion,
 *
 *
 */
enum MlViewStatus
mlview_utils_utf8_str_to_isolat1 (const gchar * a_in_str,
                                  gchar ** a_out_str)
{
	enum MlViewStatus status = MLVIEW_OK;
	gint len1 = 0,
	            len2 = 0;

	g_return_val_if_fail (a_in_str && a_out_str,
	                      MLVIEW_BAD_PARAM_ERROR);

	*a_out_str = NULL;

	len2 = strlen ((const char*)a_in_str);

	if (len2 == 0)
		return MLVIEW_OK;

	status = mlview_utils_utf8_str_len_as_isolat1 (a_in_str,
	         &len1);

	g_return_val_if_fail (status == MLVIEW_OK, status);

	*a_out_str = (gchar*) g_malloc0 (len1 + 1);

	if (UTF8Toisolat1 ((xmlChar*)*a_out_str, &len1,
	                   (xmlChar*)a_in_str, &len2)) {

		/*function failed */
		g_free (*a_out_str);
		*a_out_str = NULL;
		status = MLVIEW_ENCODING_ERROR;

	} else {

		/*function returned successfully */
		status = MLVIEW_OK;
	}

	return status;
}

/**
 *Initialize some facilities used by
 *mlview.
 *This must be called at the beginning
 *of the main routine.
 */
void
mlview_utils_init (void)
{
	/* Initialize gnome-vfs*/
	if (! gnome_vfs_init ())
		g_error ("Couldn't initialize VFS");

	mlview_utils_init_i18n ();
	xmlInitializeCatalog ();
}

/**
 *Cleanups some faicilties used bu mlview.
 *This must be called just before exiting
 *from mlview.
 */
void
mlview_utils_cleanup (void)
{
	xmlCatalogCleanup ();

	/* Shutdown gnome-vfs*/
	gnome_vfs_shutdown ();
}

/**
 *This method increments the reference count
 *of the "available encodings list".
 *See mlview_utils_editor_unref_available_encodings()
 *to decrement the reference count and free the
 *"available encodings list" variable.
 *
 *To avoid memory leaks, 
 *Every single object that uses the
 *"available encodings list" variable should call
 *this method once at its creation and call
 *mlview_utils_unref_available_encodings()
 *at its destruction.
 */
void
mlview_utils_ref_available_encodings (void)
{
	if (gv_available_encodings)
		gv_available_encodings_ref_count++;
}


/**
 *This method decrements the reference count of
 *the "available encoding list" global variable. 
 *If the reference count of that variable goes
 *down to zero, this method destroys the variable.
 *See mlview_utils_ref_available_encodings() for
 *more explanation about how these methods should be used.
 */
void
mlview_utils_unref_available_encodings (void)
{
	if (gv_available_encodings == NULL)
		return;

	if (gv_available_encodings_ref_count)
		gv_available_encodings_ref_count--;

	if (gv_available_encodings_ref_count == 0) {
		/*free the gv_available_encodings list */
		GList *list_elem = NULL;

		for (list_elem = gv_available_encodings;
		        list_elem; list_elem = list_elem->next) {

			if (list_elem->data) {
				g_free (list_elem->data);
				list_elem->data = NULL;
			}
		}

		g_list_free (gv_available_encodings);
		gv_available_encodings = NULL;
	}
}


/**
 *Initialyses the list of the available encodings
 *supported by mlview.
 *This method must be called at least once to make
 *the least available.
 *
 */
void
mlview_utils_init_available_encodings_list (void)
{
	if (gv_available_encodings == NULL) {
		gchar *enc = NULL;
		gint i = 0;

		/*
		 *initialise the libxml2 encoding support, 
		 *in case it hasn't been initialized before ...
		 */
		xmlInitCharEncodingHandlers ();

		/*initialise the list */
		for (i = 0, enc =
		            (gchar *) gv_default_encodings[i]; enc;
		        i++, enc =
		            (gchar *) gv_default_encodings[i]) {

			gv_available_encodings =
			    g_list_append
			    (gv_available_encodings,
			     g_strdup ((const gchar*)enc));
		}
	}
}


/**
 *Gets the list of the available
 *encodings supported by mlview.
 *
 *The returned list must *NOT* be
 *freed by the caller.
 *It should intead use the functions
 *mlview_utils_ref_available_encodings()
 *and mlview_utils_unref_available_encodings()
 *to control the lifecyle of the list.
 */
GList *
mlview_utils_get_available_encodings (void)
{
	if (gv_available_encodings == NULL) {
		mlview_utils_init_available_encodings_list ();
	}

	return gv_available_encodings;
}


/**
 *An implementation of the 
 *glib GCompareFunc.
 *Compares two strings.
 *
 *@param a_str1 the of the two strings to compare.
 *@param a_str2 the second of the two strings to compare.
 *@return TRUE if the two strings are equal, FALSE otherwise.
 */
static gboolean
string_compare (const gchar * a_str1, const gchar * a_str2)
{
	if (a_str1 == NULL || a_str2 == NULL)
		return FALSE;

	if (!strcmp ((const char*)a_str1, (const char*)a_str2))
		return TRUE;

	return FALSE;
}


static void
mlview_utils_free_encoding_handler_if_needed (xmlCharEncodingHandler * a_enc_handler)
{
	if (a_enc_handler == NULL)
		return;

	if (a_enc_handler->iconv_in) {

		iconv_close (a_enc_handler->iconv_in);
		a_enc_handler->iconv_in = NULL;
	} else {
		return;
	}

	if (a_enc_handler->iconv_out) {

		iconv_close (a_enc_handler->iconv_out);
		a_enc_handler->iconv_out = NULL;
	} else {
		return;
	}

	if (a_enc_handler->name) {
		xmlFree (a_enc_handler->name);
		a_enc_handler->name = NULL;
	}

	xmlFree (a_enc_handler);
	return;
}

/**
 *Tests wether a given encoding is supported by mlview 
 *(actually, the underlying libxml2) or not.
 *
 *@param a_encoding the encoding name preferably 
 *styled as the iconv encoding naming style.
 *On GNU/Linux systems, type "iconv --list" to have 
 *an idea about the iconv encoding naming style.
 *
 *@return TRUE if the encoding is supported FALSE if otherwise.
 */
gboolean
mlview_utils_is_encoding_supported (const gchar * a_encoding)
{
	xmlCharEncodingHandler *enc_handler = NULL;


	if (a_encoding == NULL)
		return FALSE;

	enc_handler = xmlFindCharEncodingHandler ((const char*)a_encoding);

	if (!enc_handler)
		return FALSE;

	mlview_utils_free_encoding_handler_if_needed
	(enc_handler);

	return TRUE;
}


/**
 *Adds an ecoding to the "available list of encodings"
 *supported by mlview. 
 *This method actually checks if the new encoding
 *is supported by the underlying libxml parser.
 *If yes, it adds the encoding to the list of available
 *encodings if and only if the list does not contain it
 *already.
 *
 *@param a_name the name of the encoding. Must be 
 *respect the naming scheme of the GNU/Linux version of iconv.
 *
 *@return 
 *<ul>
 *<li>MLVIEW_OK upon successfull completion, 
 *MLVIEW_NO_ENCODINGS if the list of available encodings supported
 *by mlview has not been built prior to calling this method.
 *In that case, call mlview_utils_get_available_encodings()
 *to build it.</li>
 *<li>
 *MLVIEW_ENCODING_NOT_SUPPORTED if the encoding is not
 *supported by the underlying libxml2 parser.
 *</li>
 *</ul>
 */
enum MlViewStatus
mlview_utils_add_supported_encoding (const gchar * a_name)
{
	if (gv_available_encodings == NULL)
		return MLVIEW_NO_ENCODINGS_ERROR;

	/*make sure libxml2 supports this encoding */
	if (mlview_utils_is_encoding_supported (a_name) == FALSE)
		return MLVIEW_ENCODING_NOT_SUPPORTED_ERROR;

	/*
	 *make sure the encoding is not already in the list
	 *and add it.
	 */
	if (!g_list_find_custom (gv_available_encodings, a_name,
	                         (GCompareFunc) string_compare)) {

		gv_available_encodings =
		    g_list_append (gv_available_encodings,
		                   g_strdup ((const gchar*)a_name));
	}

	return MLVIEW_OK;
}


/*
 *FIXME: define a macro to free
 *a char handler handler if and only
 *if it must be freed (the caller)
 */

enum MlViewStatus
mlview_utils_del_supported_encoding (const gchar * a_name)
{
	GList *list_elem = NULL;

	if (gv_available_encodings == NULL)
		return MLVIEW_NO_ENCODINGS_ERROR;

	if ((list_elem =
	            g_list_find_custom (gv_available_encodings, a_name,
	                                (GCompareFunc) string_compare))) {
		gv_available_encodings =
		    g_list_remove_link
		    (gv_available_encodings, list_elem);

		if (list_elem && list_elem->data) {
			g_free (list_elem->data);
			g_list_free (list_elem);
		}
	}

	return MLVIEW_OK;
}

gboolean
mlview_utils_strstr (const gchar *a_haystack,
                     const gchar *a_needle,
                     gboolean a_ignore_case)
{
	gboolean result = TRUE ;
	gchar *tmp_str = NULL ;

	if (a_ignore_case == TRUE) {
		result = mlview_utils_strstr_ignore_case
		         (a_haystack, a_needle) ;
	} else {
		tmp_str = (gchar*) strstr ((const char*)a_haystack,
		                           (const char*)a_needle) ;
		if (tmp_str)
			result = TRUE ;
		else
			result = FALSE ;
	}
	return result ;
}

gboolean
mlview_utils_strstr_ignore_case (const gchar *a_haystack,
                                 const gchar *a_needle)
{
	gchar *str1 = NULL, *str2 = NULL, *str=NULL ;

	g_return_val_if_fail (a_haystack && a_needle, FALSE) ;

	str1 = (gchar*)g_utf8_casefold ((const gchar*)a_haystack,
	                                strlen ((const char*)a_haystack)) ;

	str2 = (gchar*) g_utf8_casefold ((const gchar*)a_needle,
	                                 strlen ((const char*)a_needle)) ;
	str = (gchar*)strstr ((char*)str1, (const char*)str2) ;

	if (str1) {
		g_free (str1) ;
		str1 = NULL ;
	}
	if (str2) {
		g_free (str2) ;
		str2 = NULL ;
	}
	if (str)
		return TRUE ;
	return FALSE ;
}

/**
 *UTF8 case insentive comparison function.
 *@return TRUE if the two strings are equal, FALSE otherwise.
 *@param a_str1 first utf8 NULL terminated string.
 *@param a_str2 second utf8 NULL terminated string.
 */
gboolean
mlview_utils_str_equals_ignore_case (const gchar *a_str1,
                                     const gchar *a_str2)
{
	gint res = 0 ;
	gchar *str1 = NULL, *str2 = NULL ;

	g_return_val_if_fail (a_str1 && a_str2, FALSE) ;

	str1 = (gchar*)g_utf8_casefold ((const gchar*)a_str1,
	                                strlen ((const char*)a_str1)) ;
	str2 = (gchar*) g_utf8_casefold ((const gchar*)a_str2,
	                                 strlen ((const char*)a_str2)) ;
	res = g_utf8_collate ((const gchar*)str1, (const gchar*)str2) ;
	if (str1) {
		g_free (str1) ;
		str1 = NULL ;
	}
	if (str2) {
		g_free (str2) ;
		str2 = NULL ;
	}
	if (!res)
		return TRUE ;
	return FALSE ;
}

/**
 *Compare two utf8 strings for equality.
 *@param a_str1 the first NULL terminated utf8 string.
 *@param a_str2 the second NULL terminated utf8 string.
 *@param a_ignore_case if set to TRUE, this function ingnores
 *the case during the comparison.
 *@return TRUE if a_str1 and a_str2 are equal.
 */
gboolean
mlview_utils_str_equals (const gchar *a_str1, const gchar *a_str2,
                         gboolean a_ignore_case)
{
	gboolean result = FALSE ;

	g_return_val_if_fail (a_str1 && a_str2,
	                      FALSE) ;
	if (a_ignore_case == TRUE) {
		result = mlview_utils_str_equals_ignore_case
		         (a_str1, a_str2) ;
	} else {
		if (!strcmp ((const char*)a_str1, (const char*)a_str2)) {
			result = TRUE ;
		} else {
			result = FALSE ;
		}
	}
	return result ;
}

/*=============================================
 * some usefull patches to libxml2:
 *
 * xmlUnlinkNs (xmlNode * node, xmlNs * ns)
 * xmlUnlinkNsDef (xmlNode * node, xmlNs * ns)
 *============================================*/

void
xmlDictFreeMem (xmlDict *a_dict,
                xmlChar* a_mem)
{
	int free_it_hard = 0 ;

	if (!a_dict || !xmlDictOwns (a_dict, a_mem)) {
		free_it_hard = 1 ;
	}
	if (free_it_hard) {
		xmlFree (a_mem) ;
	}
}

/**
 *Unlinks a namespace from an xmlNode.
 *@param node the node to unlink the namespace
 *from.
 *@param ns the namespace to unlink.
 *@return the unlinked namespace, or NULL
 *if the functions failed or could not unlink
 *the namespace.
 */
xmlNs *
xmlUnlinkNs (xmlNode * node, xmlNs * ns)
{
	xmlNs *cur;

	if (node == NULL || ns == NULL) {
		return NULL;
	}

	cur = node->ns;

	if (cur == NULL)
		return NULL;

	/*
	 *Check if the namespace to unlink is 
	 *the first ns in the xml node nsDef list. If yes, unlink it
	 **/
	if (ns == cur) {

		if (ns->next) {

			cur = ns->next;
			ns->next = NULL;
		}

		return ns;
	}

	/*
	 *the namespace to unlink may be 
	 *further in the ns list. 
	 *Walk to the namespace just before it...
	 */
	while (cur != NULL && cur->next != ns) {

		cur = cur->next;
	}

	if (cur == NULL)
		return NULL;

	/*
	 *at this stage, cur points to the 
	 *namespace that is just before the 
	 *namespace to unlink. Let's now unlink ns
	 */
	if (ns->next == NULL) {

		cur->next = NULL;

	} else {
		cur->next = ns->next;
		ns->next = NULL;
	}

	return ns;

}


/**
 *Unlinks a namespace definition
 *from an xmlNode .
 *@param node the xml node to unlink the namespace
 *definition from.
 *@param ns the namespace to unlink.
 */
xmlNs *
xmlUnlinkNsDef (xmlNode * node, xmlNs * ns)
{
	xmlNs *cur;

	if (node == NULL || ns == NULL) {
		return NULL;
	}

	cur = node->nsDef;
	if (cur == NULL)
		return NULL;

	/*
	 *Check if the namespace to unlink is the 
	 *first ns in the xml node 
	 *nsDef list. If yes, unlink it
	 */
	if (ns == cur) {
		node->nsDef = node->nsDef->next;
		ns->next = NULL;
		return ns;
	}

	/*
	 *the namespace to unlink may be 
	 *further in the ns list. 
	 *Walk to the namespace just before it...
	 */
	while (cur != NULL && cur->next != ns) {
		cur = cur->next;
	}
	if (cur == NULL)
		return NULL;

	/*
	 *at this stage, cur points to the 
	 *namespace that is just before the 
	 *namespace to unlink. 
	 *Let's now unlink ns
	 */
	if (ns->next == NULL)
		cur->next = NULL;
	else {
		cur->next = ns->next;
		ns->next = NULL;
	}
	return ns;
}


/**
 *Gets the valid children of the element a_node.
 *This function replaces xmlValidGetValidElement because it
 *works even if a_node is the 
 *root node and has no children... I've remarked that
 *xmlValidGetValidElements () does not work in that case :( 
 *
 *@param a_list the output element name array.
 *Must be allocated by the user. 
 *@param a_max the max size of the output list.
 *@param a_node the node to consider.
 *
 *
 */
int
xmlValidGetValidElementsChildren (xmlNode * a_node,
                                  const xmlChar ** a_list,
                                  int a_max)
{
	xmlElement *element_desc = NULL;
	gint nb_valid_elements = 0;
	xmlDict *dict = NULL ;

	/*some sanity checks... */
	if (a_node == NULL)
		return -2;
	if (a_list == NULL)
		return -2;
	if (a_list == NULL)
		return -2;
	if (a_max == 0)
		return -2;
	if (a_node->type != XML_ELEMENT_NODE)
		return -2;
	if (a_node->parent == NULL)
		return -2;

	if (a_node && a_node->doc && a_node->doc->dict) {
		dict = a_node->doc->dict ;
	}
	if (a_node->children) {
		/*the node is the root node
		 *and it has children. We can get
		 *its valid children
		 *with the normal libxml2 function
		 */
		return xmlValidGetValidElements (a_node->last,
		                                 NULL, a_list,
		                                 a_max);

	} else {
		gint nb_elements = 0,
		                   i = 0;
		const xmlChar *elements[256];
		xmlNode *test_node = NULL;
		xmlValidCtxt vctxt;

		memset (&vctxt, 0, sizeof (xmlValidCtxt));

		element_desc =
		    xmlGetDtdElementDesc (a_node->parent->
		                          doc->intSubset,
		                          a_node->name);

		if ((element_desc == NULL)
		        && (a_node->parent->doc->extSubset != NULL)) {
			element_desc =
			    xmlGetDtdElementDesc
			    (a_node->parent->doc->extSubset,
			     a_node->name);
		}

		if (element_desc == NULL)
			return (-1);

		/*
		 *Create a dummy child element
		 */
		test_node =
		    xmlNewChild (a_node, NULL,
		                 (const xmlChar*)"<!dummy?>",
		                 NULL);

		if (test_node->name) {
			xmlDictFreeMem (dict, (xmlChar *) test_node->name);
			test_node->name = NULL;
		}

		/*
		 *Insert each potential child node 
		 *and check if the current node is still valid.
		 */
		nb_elements =
		    xmlValidGetPotentialChildren
		    (element_desc->content, elements,
		     &nb_elements, 256);

		for (i = 0; i < nb_elements; i++) {
			test_node->name = elements[i];
			if (xmlStrEqual
			        (test_node->name, (const xmlChar*)"#PCDATA"))
				test_node->type = XML_TEXT_NODE;
			else
				test_node->type =
				    XML_ELEMENT_NODE;

			if (xmlValidateOneElement (&vctxt,
			                           a_node->
			                           parent->doc,
			                           a_node)) {
				int j = 0;

				for (j = 0;
				        j < nb_valid_elements; j++)
					if (xmlStrEqual
					        (elements[i],
					         a_list[j])) {
						break;
					}

				a_list[nb_valid_elements++] =
				    elements[i];

				if (nb_valid_elements >= a_max)
					break;
			}
		}

		/*
		 *Restore the tree structure
		 */
		xmlUnlinkNode (test_node);

		if (test_node) {
			test_node->name = NULL;
			xmlFreeNode (test_node);
			test_node = NULL;
		}

		return nb_valid_elements;
	}
}

/**
 *Sets the name of an entity declaration node.
 *Takes care to update the entities dictionary.
 *@param aDtd the dtd subset the entity declaration node
 *is attached to.
 *@param anEntityNode the entity declaration node to consider
 *@param aName the new name
 *@return 0 upon successful completion, -1 if a bad parameter
 *was given to the function, 1 if aName is the name of an existing
 *entity declaration.
 */
int
xmlSetEntityNodeName (xmlDtd *aDtd, xmlEntity *anEntityNode,
                      xmlChar *aName)
{
	xmlEntity *entity = NULL ;

	if (!aDtd || !aDtd->entities
	        || !anEntityNode || !aName
	        || !anEntityNode->name)
		return -1 ;
	/*
	 *If an other entity already has the name aName,
	 *get out!
	 */
	if (xmlHashLookup ((xmlHashTable*)aDtd->entities,
	                   aName)) {
		return 1 ;
	}
	/*
	 *if the current entity is present in the entities
	 *dictionary under its old name, remove it.
	 */
	if ((entity = (xmlEntity*)xmlHashLookup ((xmlHashTable*)aDtd->entities,
	              anEntityNode->name))) {

		xmlHashRemoveEntry ((xmlHashTable*)aDtd->entities,
		                    anEntityNode->name, NULL) ;
	}
	xmlNodeSetName ((xmlNode*)anEntityNode, aName) ;
	xmlHashAddEntry ((xmlHashTable*)aDtd->entities,
	                 anEntityNode->name, entity) ;
	return 0 ;
}

/**
 *Checks if a node is the child of another in the tree.
 *@param aChild the child node
 *@param aParent the parent node
 *@return 0 if aChild is not a child of aParent, 1 if aChild
 *is a child of aParent, -1 if a bad parameter was given.
 */
int
xmlNodeIsChildOf (xmlNodePtr aChild,
                  xmlNodePtr aParent)
{
	xmlNodePtr cur = NULL;

	if (!aChild || !aParent)
		return -1;

	cur = aChild;

	while (cur) {
		if (cur == aParent)
			return 1;

		cur = cur->parent;
	}

	return 0;
}


static int
xmlElectElementNamesFromElementContent (xmlElementContent *aContent,
                                        const xmlChar **aElementNames,
                                        unsigned long aMax)
{
	xmlChar **elementNames = NULL ;
	gulong index = 0, nbElementNames = 0 ;

	if (!aContent || aElementNames)
		return -2 ;

	elementNames = (xmlChar**) aElementNames ;

	switch (aContent->type) {
	case XML_ELEMENT_CONTENT_PCDATA:
		break ;
	case XML_ELEMENT_CONTENT_ELEMENT:
		if (index < aMax)
			elementNames[index++] = (xmlChar*) aContent->name ;
		return 1 ;
		break ;
	case XML_ELEMENT_CONTENT_SEQ:
	case XML_ELEMENT_CONTENT_OR:
		if (aContent->c1 && index < aMax) {
			nbElementNames = xmlElectElementNamesFromElementContent
			                 (aContent->c1, (const xmlChar**)&elementNames[index],
			                  aMax - index);

			if (nbElementNames > 0)
				index += nbElementNames ;
		}
		if (aContent->c2 && index < aMax) {
			nbElementNames = xmlElectElementNamesFromElementContent
			                 (aContent->c2, (const xmlChar**)&elementNames[index],
			                  aMax - index) ;
			if (nbElementNames > 0)
				index += nbElementNames ;
		}
		break ;
	default:
		break ;
	}

	return index ;
}

static int
xmlGetValidElementChildrenIfNoChildren (xmlNode *aNode,
                                        const xmlChar ** aList,
                                        unsigned int aMax)
{
	xmlElement *elemDecl = NULL ;
	int nbElementNames = 0 ;

	if (!aNode || !aList || !aNode->doc
	        || !aNode->doc->extSubset || aNode->type != XML_ELEMENT_NODE)
		return -2 ;
	if (aNode->children)
		return -2 ;

	elemDecl = xmlGetDtdElementDesc (aNode->doc->extSubset, aNode->name) ;
	if (!elemDecl)
		return -1 ;
	if (elemDecl->etype == XML_ELEMENT_TYPE_ELEMENT) {
		nbElementNames = xmlElectElementNamesFromElementContent (elemDecl->content,
		                 aList, aMax) ;
	}
	return nbElementNames ;
}


int
xmlGetValidElementsChildren2 (xmlNode *aNode,
                              const xmlChar **aList,
                              unsigned int aMax)
{
	int nbElements = 0 ;

	if (!aNode || !aList)
		return -2 ;
	if (!aList)
		return -2 ;

	if (!aNode->children) {
		nbElements = xmlGetValidElementChildrenIfNoChildren
		             (aNode, aList, aMax) ;
	} else {}

	return nbElements ;
}

/**
 *Gets the last children element node of a given
 *xml element node
 *@param a_ref_node the xml node to consider
 *@param the last children element node found, or NULL if
 *none has been found.
 */
xmlNode *
mlview_utils_get_last_child_element_node (xmlNode *a_ref_node)
{
	xmlNode *cur = NULL ;
	g_return_val_if_fail (a_ref_node, NULL) ;

	if (a_ref_node->children == NULL)
		return NULL ;

	for (cur = a_ref_node->last ; cur ; cur = cur->prev) {
		if (cur->type == XML_ELEMENT_NODE)
			return cur ;
	}
	return NULL ;
}

/**
 *Parses a string to get a node name and a
 *namespace out of it.
 *
 *@param a_node the xml node that is 
 *supposed to hold the fully qualified name.
 *This parameter is usefull to access the information
 *about namespace definition store in the document
 *holding this node if any.
 *
 *@param a_full_name the 
 *name to parse to extract 
 *the prefix part and the local name part.
 *
 *@param a_ns out parameter. 
 *The parsed namespace.
 *Can be null if there is no prefix or 
 *if the prefix has not been declared in 
 *the node or in one of the node's parents.
 *NOTE THAT THE USER MUST *NOT* free this pointer !!!.
 *
 *@param a_local_name out parameter. 
 *The local name part of the full name. 
 *Can be null if there is no local name part.
 *If a_local_name is not null, 
 *the caller has to free it ; 
 */
void
mlview_utils_parse_full_name (xmlNode * a_node,
                              const gchar * a_full_name,
                              xmlNs ** a_ns,
                              gchar ** a_local_name)
{
	gchar *colon_ptr = NULL,
	                   *prefix = NULL,
	                             *local_name = NULL;
	gchar **full_name_array = NULL;
	xmlNs *ns = NULL;

	g_return_if_fail (a_node != NULL);
	g_return_if_fail (a_full_name != NULL);

	*a_ns = NULL;
	*a_local_name = NULL;

	colon_ptr = (gchar*)strchr ((const char*)a_full_name, ':');

	if (colon_ptr != NULL) {
		full_name_array =
		    g_strsplit ((const gchar*)a_full_name, ":", 2);
		prefix = (gchar*)full_name_array[0];
		local_name = (gchar*)full_name_array[1];
		ns = xmlSearchNs (a_node->doc, a_node, (xmlChar*)prefix);
		if (ns != NULL)
			*a_ns = ns;
		else
			*a_ns = NULL;

		if (local_name != NULL
		        &&
		        !mlview_utils_is_white_string (local_name))
			*a_local_name = (gchar*)g_strstrip ((gchar*)local_name);

	} else {
		*a_local_name = (gchar*)g_strdup ((const gchar*)a_full_name);
	}
}

/**
 *Parses the doctype declaration as specified at
 *http://www.w3.org/TR/REC-xml#NT-doctypedecl, but ignores
 *the markup declaration parts ...
 *This is only to allow the user to edit the name, public id
 *and system id.
 *@param a_instr the input stream to parse
 *@param a_name_start out parameter. A pointer to the first byte
 *of the doctype declaration name.
 *@param a_name_end out parameter. A pointer to the last byte of the
 *doctype declaration name.
 *@param a_public_id_start out parameter. A pointer to the first byte of 
 *the parsed public_id if any, or NULL.
 *@param a_public_id_end out parameter. A pointer to the last byte of 
 *the parsed public_id if any, or NULL.
 *@param a_system_id_start out paramter. A pointer ot the first byte of
 *the parsed system_id if any, or NULL.
 *@param a_system_id_end out paramter. A pointer ot the last byte of
 *the parsed system_id if any, or NULL.
 *@return MLVIEW_OK upon successful completion, an error code otherwise.
 */
enum MlViewStatus
mlview_utils_parse_doctype_decl (gchar *a_instr,
                                 gchar **a_name_start,
                                 gchar **a_name_end,
                                 gchar **a_public_id_start,
                                 gchar **a_public_id_end,
                                 gchar **a_system_id_start,
                                 gchar **a_system_id_end)
{
	guint instr_len = 0 ;
	gchar *iptr = a_instr ;
	gchar *name_start=NULL, *name_end=NULL ;
	gchar *public_id_start=NULL,*public_id_end=NULL ;
	gchar *system_id_start=NULL, *system_id_end=NULL;
	gchar *ptr_end = NULL ;
	enum MlViewStatus status = MLVIEW_OK ;

	g_return_val_if_fail (a_instr
	                      && a_name_start
	                      && a_name_end
	                      && a_public_id_start
	                      && a_public_id_end
	                      && a_system_id_start
	                      && a_system_id_end,
	                      MLVIEW_BAD_PARAM_ERROR) ;
	instr_len = strlen ((const char*)a_instr) ;

	if (instr_len < 11)
		return MLVIEW_PARSING_ERROR ;

	if (*iptr          == '<'
	        && *(iptr + 1) == '!'
	        && *(iptr + 2) == 'D'
	        && *(iptr + 3) == 'O'
	        && *(iptr + 4) == 'C'
	        && *(iptr + 5) == 'T'
	        && *(iptr + 6) == 'Y'
	        && *(iptr + 7) == 'P'
	        && *(iptr + 8) == 'E'
	        && mlview_utils_is_space (*(iptr + 9)) == TRUE) {
		iptr += 10 ;
	} else {
		return MLVIEW_PARSING_ERROR ;
	}
	while (mlview_utils_is_space (*iptr) == TRUE) {
		iptr ++ ;
	}
	status = mlview_utils_parse_element_name (iptr, &name_end) ;
	if (status == MLVIEW_OK) {
		name_start = iptr ;
		iptr = name_end + 1 ;
	} else {
		return MLVIEW_PARSING_ERROR ;
	}
	while (mlview_utils_is_space (*iptr) == TRUE) {
		iptr ++ ;
	}

	status = mlview_utils_parse_external_id (iptr,
	         &public_id_start,
	         &public_id_end,
	         &system_id_start,
	         &system_id_end,
	         &ptr_end) ;

	/*
	 *now, ignore everything until '>'
	 */
	if (status != MLVIEW_OK || !ptr_end) {
		return MLVIEW_PARSING_ERROR ;
	}
	iptr = ptr_end ;
	status = MLVIEW_ERROR ;
	while (iptr
	        && *iptr
	        && ((*iptr != '>'))) {
		iptr ++ ;
	}
	if (iptr && *iptr == '>') {
		status = MLVIEW_OK ;
	} else {
		status = MLVIEW_PARSING_ERROR ;
	}
	if (status == MLVIEW_OK) {
		*a_name_start = name_start ;
		*a_name_end = name_end ;
		*a_public_id_start = public_id_start ;
		*a_public_id_end = public_id_end ;
		*a_system_id_start = system_id_start ;
		*a_system_id_end = system_id_end ;
	}
	return status ;
}

/**
 *Tells if a given character is a "pubidchar as defined by the
 *XML spec:
 *pubidchar: #x20 | #xD | #xA | [a-zA-Z0-9] | [-'()+,./:=?;!*#@$_%]
 *@return TRUE if a_c is a pubidchar, FALSE otherwise.
 */
gboolean
mlview_utils_is_pubidchar (gint a_c)
{
	switch (a_c) {
	case 0x20:
	case 0xD:
	case 0xA:
	case 'a'...'z':
	case 'A'...'Z':
	case '0'...'9':
	case '-':
	case '(':
	case ')':
	case '+':
	case ',':
	case '.':
	case '/':
	case ':':
	case '=':
	case '?':
	case ';':
	case '!':
	case '*':
	case '#':
	case '@':
	case '$':
	case '_':
	case '%':
		return TRUE ;
	default:
		return FALSE ;
	}
}

/**
 *Parses the externalid production
 *defined by the xml spec:
 *externalID    ::= 'SYSTEM' S SystemLiteral
 *                  | PUBLIC' S PubidLiteral S SystemLiteral
 *SystemLiteral    ::=    ('"' [^"]* '"') | ("'" [^']* "'")
 *PubidLiteral    ::=    '"' PubidChar* '"' | "'" (PubidChar - "'")* "'"
 *PubidChar    ::=    #x20 | #xD | #xA | [a-zA-Z0-9] | [-'()+,./:=?;!*#@$_%]
 *@param a_instr the input string to parse.
 *@param a_public_id_start out parameter. A pointer to the start of the parsed
 *public ID, if any. NULL otherwise.
 *@param a_public_id_end out parameter. A pointer to the end of the parsed
 *public ID, if any. NULL otherwise.
 *@param a_system_id_start out parameter. A pointer to the beginning of the
 *parsed system id, if any. NULL otherwise.
 *@param a_system_id_end out parameter. A pointer to the end of the parsed
 *system id, if any. NULL otherwise.
 *@param a_end_ptr out parameter. A pointer to the end of the parsed external
 *id.
 *@return MLVIEW_OK upon successful completion, and error code otherwise.
 */
enum MlViewStatus
mlview_utils_parse_external_id (gchar *a_instr,
                                gchar **a_public_id_start,
                                gchar **a_public_id_end,
                                gchar **a_system_id_start,
                                gchar **a_system_id_end,
                                gchar **a_end_ptr)
{
	gchar *iptr = a_instr ;
	gchar delim = 0 ;
	gchar *public_id_start = NULL, *public_id_end = NULL,
	                         *system_id_start, *system_id_end = NULL,
	                                                            *end_ptr=NULL ;

	g_return_val_if_fail (a_instr
	                      && a_public_id_start
	                      && a_public_id_end
	                      && a_system_id_start
	                      && a_system_id_end
	                      && a_end_ptr,
	                      MLVIEW_BAD_PARAM_ERROR) ;
	if (iptr[0]     == 'S'
	        && iptr[1]  == 'Y'
	        && iptr[2]  == 'S'
	        && iptr[3]  == 'T'
	        && iptr[4]  == 'E'
	        && iptr[5]  == 'M') {
		iptr+=6 ;
		if (mlview_utils_is_space (*iptr) != TRUE)
			return MLVIEW_PARSING_ERROR ;
		while (mlview_utils_is_space (*iptr) == TRUE) {
			iptr ++ ;
		}
		if (*iptr == '"'
		        || *iptr == '\'') {
			delim = *iptr ;
			iptr ++ ;
		} else {
			return MLVIEW_PARSING_ERROR ;
		}
		system_id_start = iptr ;
		while (iptr && *iptr
		        && *iptr != delim) {
			iptr ++ ;
		}
		if (*iptr == delim) {
			system_id_end = iptr - 1;
		} else {
			return MLVIEW_PARSING_ERROR ;
		}
		iptr++ ;
		end_ptr = iptr ;
	} else if (iptr[0]     == 'P'
	           && iptr[1]  == 'U'
	           && iptr[2]  == 'B'
	           && iptr[3]  == 'L'
	           && iptr[4]  == 'I'
	           && iptr[5]  == 'C') {
		iptr += 6 ;
		if (mlview_utils_is_space (*iptr) != TRUE)
			return MLVIEW_PARSING_ERROR ;
		while (mlview_utils_is_space (*iptr) == TRUE) {
			iptr ++ ;
		}
		if (*iptr == '"'
		        || *iptr == '\'') {
			delim = *iptr ;
			iptr ++ ;
		} else {
			return MLVIEW_PARSING_ERROR ;
		}
		if (mlview_utils_is_pubidchar (*iptr) == TRUE) {
			public_id_start = iptr ;
		} else {
			return MLVIEW_PARSING_ERROR ;
		}
		while (mlview_utils_is_pubidchar (*iptr) == TRUE) {
			iptr++ ;
		}
		if (iptr && *iptr
		        && *iptr == delim) {
			public_id_end = iptr - 1 ;
			iptr++ ;
		} else {
			return MLVIEW_PARSING_ERROR ;
		}
		if (mlview_utils_is_space (*iptr) == FALSE) {
			return MLVIEW_PARSING_ERROR ;
		}
		while (mlview_utils_is_space (*iptr) == TRUE) {
			iptr ++ ;
		}
		if (iptr && *iptr
		        && (*iptr == '"' || *iptr == '\'')) {
			delim = *iptr ;
			iptr ++ ;
		} else {
			return MLVIEW_PARSING_ERROR ;
		}
		system_id_start = iptr ;
		while (iptr && *iptr && *iptr != delim) {
			iptr ++ ;
		}
		if (iptr && *iptr && *iptr == delim) {
			system_id_end = iptr - 1 ;
		} else {
			return MLVIEW_PARSING_ERROR ;
		}
		end_ptr = iptr + 1 ;
	} else {
		return MLVIEW_PARSING_ERROR ;
	}

	*a_public_id_start = public_id_start ;
	*a_public_id_end = public_id_end ;
	*a_system_id_start = system_id_start ;
	*a_system_id_end =system_id_end ;
	*a_end_ptr = end_ptr ;

	return MLVIEW_OK ;
}

/**
 *Parses an entity reference as described by the XML spec:
 *Reference    ::=    EntityRef | CharRef
 *EntityRef    ::=    '&' Name ';'
 *PEReference    ::=    '%' Name ';'
 *@param a_instr the input string that contains the entity ref
 *to parse.
 *@param a_name_start out parameter. A pointer to the begining
 *of the parsed entity name.
 *@param a_name_end out parameter. A pointer to the end of the parsed
 *entity name.
 *@return MLVIEW_OK upon successful completion, an error code
 *otherwise.
 */
enum MlViewStatus
mlview_utils_parse_entity_ref (gchar *a_instr,
                               gchar **a_name_start,
                               gchar **a_name_end)
{
	enum MlViewStatus status = MLVIEW_OK ;

	gchar *iptr = a_instr, *name_end = NULL ;

	g_return_val_if_fail (a_instr && a_name_start
	                      && a_name_end, MLVIEW_BAD_PARAM_ERROR) ;

	if (*iptr != '&')
		return MLVIEW_PARSING_ERROR ;
	iptr ++ ;
	status = mlview_utils_parse_element_name (iptr, &name_end) ;
	if (status != MLVIEW_OK || !name_end)
		return MLVIEW_PARSING_ERROR ;
	*a_name_start = iptr ;
	*a_name_end = name_end ;
	return MLVIEW_OK ;
}

/**
 *Parses a char entity refernence as defined by the XML spec.
 *CharRef    ::=    '&#' [0-9]+ ';'
 * | '&#x' [0-9a-fA-F]+ ';
 *@param a_instr the input string that contains the char entity
 *reference to be parse.
 *@param a_char_code_start out parameter. A pointer to the start
 *of the parsed character code. The character code is left in its
 *string form.
 *@param a_char_code_end. out parameter. A pointer to the end
 *of the parsed character code.
 *@return MLVIEW_OK upon succesful completion, an error 
 *code otherwise.
 */
enum MlViewStatus
mlview_utils_parse_char_ref (gchar *a_instr,
                             gchar **a_char_code_start,
                             gchar **a_char_code_end,
                             gboolean *a_is_hexa)
{
	gchar *iptr = NULL, *char_code_start = NULL,
	                                       *char_code_end = NULL ;
	gboolean is_hexa = FALSE, loop = TRUE ;

	g_return_val_if_fail (a_instr && *a_instr
	                      && char_code_start
	                      && char_code_end,
	                      MLVIEW_BAD_PARAM_ERROR) ;
	iptr = a_instr ;
	if (iptr[0]    == '&'
	        && iptr[1] == '#') {
		iptr+= 2 ;
	} else {
		return  MLVIEW_PARSING_ERROR ;
	}
	if (*iptr == 'x') {
		is_hexa = TRUE ;
		iptr ++ ;
	}
	char_code_start = iptr ;
	while (loop == TRUE) {
		if (is_hexa == FALSE) {
			switch (*iptr) {
			case '0' ... '9':
				iptr ++ ;
				break ;
			default:
				loop = FALSE ;
				break ;
			}
		} else {
			switch (*iptr) {
			case '0' ... '9':
			case 'A' ... 'F':
			case 'a' ... 'f':
				iptr ++ ;
				break ;
			default:
				loop = FALSE ;
				break ;
			}
		}
	}
	if (iptr && *iptr)
		char_code_end = iptr - 1;
	else
		return MLVIEW_PARSING_ERROR ;

	*a_char_code_start = char_code_start ;
	*a_char_code_end = char_code_end ;
	*a_is_hexa = is_hexa ;
	return MLVIEW_OK ;
}

/**
 *Parses a parameter entity ref.
 *@param a_instr the input string that contains
 *the parameter entity reference to parse.
 *@param a_name_start out parameter. A pointer to
 *the start of the parsed entity parameter name.
 *@param a_name_end out parameter. A pointer to the
 *end of the parsed entity parameter name.
 *@return MLVIEW_OK upon successful completion, an
 *error code otherwise.
 */
enum MlViewStatus
mlview_utils_parse_pe_ref (gchar *a_instr,
                           gchar **a_name_start,
                           gchar **a_name_end)
{
	enum MlViewStatus status = MLVIEW_OK ;
	gchar *iptr = NULL, *name_start = NULL, *name_end = NULL;

	g_return_val_if_fail (a_instr && *a_instr
	                      && a_name_start
	                      && a_name_end,
	                      MLVIEW_BAD_PARAM_ERROR) ;

	if (iptr[0]   == '%')
		iptr ++ ;
	else
		return MLVIEW_PARSING_ERROR ;
	status = mlview_utils_parse_element_name (iptr, &name_end) ;
	if (status != MLVIEW_OK || !name_end)
		return MLVIEW_OK ;
	name_start = iptr ;
	iptr = name_end + 1 ;
	if (*iptr == ';')
		iptr ++ ;
	else
		return MLVIEW_PARSING_ERROR ;

	*a_name_start = name_start ;
	*a_name_end = name_end ;

	return MLVIEW_OK ;
}

/**
 *Parses an entity value as defined by the XML spec:
 *EntityValue    ::=    '"' ([^%&"] | PEReference | Reference)* '"'
 * |  "'" ([^%&'] | PEReference | Reference)* "'"
 *@param a_instr the input string that contains the entity value
 *to parse.
 *@param a_value_start out parameter. A pointer to the beginning
 *of the parsed entity value string.
 *@param a_value_end out parameter. A pointer to the end of the
 *parsed entity value string.
 *@return MLVIEW_OK upon succesful completion, an error code
 *otherwise.
 */
enum MlViewStatus
mlview_utils_parse_entity_value (gchar *a_instr,
                                 gchar **a_value_start,
                                 gchar **a_value_end)
{
	enum MlViewStatus status = MLVIEW_OK ;
	gchar *iptr = NULL, *value_start=NULL,*value_end=NULL  ;
	gchar delim = 0 ;

	g_return_val_if_fail (a_instr && *a_instr
	                      && a_value_start
	                      && a_value_end,
	                      MLVIEW_BAD_PARAM_ERROR) ;

	iptr = a_instr ;
	if (*iptr == '\'' || *iptr == '"') {
		delim = *iptr ;
		iptr ++ ;
	} else {
		return MLVIEW_PARSING_ERROR ;
	}
	value_start = iptr ;
eat_entity_value:
	while (iptr && *iptr
	        && *iptr != delim
	        && *iptr != '&'
	        && *iptr != '%') {
		iptr ++ ;
	}
	if (*iptr == '&') {
		if (iptr[1] == '#') {
			gchar *code_start=NULL,
			                  *code_end=NULL;
			gboolean is_hexa = FALSE ;
			status = mlview_utils_parse_char_ref
			         (iptr,&code_start, &code_end,
			          &is_hexa) ;
			if (status != MLVIEW_OK)
				return MLVIEW_PARSING_ERROR ;
			iptr = code_end + 2 ;
			goto eat_entity_value ;
		} else {
			gchar *name_start = NULL, *name_end = NULL ;
			status = mlview_utils_parse_entity_ref
			         (iptr, &name_start, &name_end) ;
			if (status != MLVIEW_OK)
				return MLVIEW_PARSING_ERROR ;
			iptr = name_end + 2 ;
			goto eat_entity_value ;
		}
	} else if (*iptr == '%') {
		gchar *name_start = NULL, *name_end = NULL ;
		status = mlview_utils_parse_pe_ref
		         (iptr, &name_start, &name_end) ;
		if (status != MLVIEW_OK)
			return MLVIEW_PARSING_ERROR ;
		iptr = name_end + 2 ;
		goto eat_entity_value ;
	}
	if (*iptr != delim)
		return MLVIEW_PARSING_ERROR ;
	value_end = iptr -1 ;
	*a_value_start = value_start ;
	*a_value_end = value_end ;
	return MLVIEW_OK ;
}

/**
 *Parses an internal general entity declaration.
 *@param a_instr the input string that contains
 *the internal general entity declaration to parse.
 *@param a_name_start out parameter. A pointer to the
 *beginning of the parsed entity name.
 *@param a_name_end out parameter. A pointer to the end
 *of the parsed entity name.
 *@param a_value_start out parameter. A pointer to the begining
 *of the parsed entity value.
 *@param a_value_end out parameter. A pointer to the end of
 *the parsed entity value.
 *@return MLVIEW_OK upon succesful completion, an error code
 *otherwise.
 */
enum MlViewStatus
mlview_utils_parse_internal_general_entity (gchar *a_instr,
        gchar **a_name_start,
        gchar **a_name_end,
        gchar **a_value_start,
        gchar **a_value_end)
{
	enum MlViewStatus status = MLVIEW_OK ;
	gchar *iptr = NULL, *name_start = NULL,
	                                  *name_end = NULL, *value_start = NULL,
	                                                                   *value_end = NULL ;

	g_return_val_if_fail (a_instr && a_name_start
	                      && a_name_end && a_value_start
	                      && a_value_end,
	                      MLVIEW_BAD_PARAM_ERROR) ;

	iptr = a_instr ;

	if (iptr[0]    == '<'
	        && iptr[1] == '!'
	        && iptr[2] == 'E'
	        && iptr[3] == 'N'
	        && iptr[4] == 'T'
	        && iptr[5] == 'I'
	        && iptr[6] == 'T'
	        && iptr[7] == 'Y') {
		iptr += 8 ;
	} else {
		return MLVIEW_PARSING_ERROR ;
	}
	if (mlview_utils_is_space (*iptr) == FALSE)
		return MLVIEW_PARSING_ERROR ;
	while (mlview_utils_is_space (*iptr) == TRUE) {
		iptr ++ ;
	}
	status = mlview_utils_parse_element_name (iptr, &name_end) ;
	if (status != MLVIEW_OK || !name_end) {
		return MLVIEW_PARSING_ERROR ;
	}
	name_start = iptr ;
	iptr = name_end + 1 ;

	if (mlview_utils_is_space (*iptr) == FALSE) {
		return MLVIEW_PARSING_ERROR ;
	}
	while (iptr
	        && *iptr
	        && mlview_utils_is_space (*iptr) == TRUE) {
		iptr ++ ;
	}
	if (!iptr || !*iptr) {
		return MLVIEW_PARSING_ERROR ;
	}
	status = mlview_utils_parse_entity_value (iptr,&value_start,
	         &value_end) ;
	if (status != MLVIEW_OK)
		return MLVIEW_PARSING_ERROR ;

	*a_name_start = name_start ;
	*a_name_end = name_end ;
	*a_value_start = value_start ;
	*a_value_end = value_end ;
	return MLVIEW_OK ;
}

/**
 *Parses an external general parsed entity
 *@param a_instr the input string that contains the external
 *parsed entity to parse.
 *@param a_name_start out parameter. A pointer to the begining
 *of the parsed entity name
 *@param a_name_end out parameter. A pointer to the end of the
 *parsed entity name
 *@param a_public_id_start out parameter. A pointer to the begining
 *of the parsed entity public id.
 *@param a_public_id_end out parameter. A pointer to the end
 *of the parsed entity public id
 *@param a_system_id_start out parameter. A pointer to the begining
 *of the parsed entity system id.
 *@param a_system_id_end out parameter. A pointer to the end of
 *the parsed entity system id.
 *@return MVLIEW_OK upon sucessful completion, an error code 
 *otherwise.
 */
enum MlViewStatus
mlview_utils_parse_external_general_parsed_entity (gchar *a_instr,
        gchar **a_name_start,
        gchar **a_name_end,
        gchar **a_public_id_start,
        gchar **a_public_id_end,
        gchar **a_system_id_start,
        gchar **a_system_id_end)
{
	enum MlViewStatus status = MLVIEW_OK ;
	gchar *iptr = NULL, *name_start = NULL, *name_end = NULL,
	                                  *public_id_start = NULL, *public_id_end = NULL,
	                                                     *system_id_start = NULL, *system_id_end = NULL,
	                                                                        *ptr_end = NULL;

	g_return_val_if_fail (a_instr && a_name_start
	                      && a_name_end && a_public_id_start
	                      && a_public_id_end
	                      && a_system_id_start,
	                      MLVIEW_BAD_PARAM_ERROR) ;

	iptr = a_instr ;
	if (iptr[0]    == '<'
	        && iptr[1] == '!'
	        && iptr[2] == 'E'
	        && iptr[3] == 'N'
	        && iptr[4] == 'T'
	        && iptr[5] == 'I'
	        && iptr[6] == 'T'
	        && iptr[7] == 'Y') {
		iptr += 8 ;
	} else {
		return MLVIEW_PARSING_ERROR ;
	}
	if (mlview_utils_is_space (*iptr) == FALSE) {
		return MLVIEW_PARSING_ERROR ;
	}
	while (mlview_utils_is_space (*iptr) == TRUE) {
		iptr ++ ;
	}
	status = mlview_utils_parse_element_name
	         (iptr, &name_end) ;
	if (status != MLVIEW_OK || !name_end)
		return MLVIEW_PARSING_ERROR ;
	name_start = iptr ;
	iptr = name_end + 1 ;
	if (mlview_utils_is_space (*iptr) == FALSE) {
		return MLVIEW_PARSING_ERROR ;
	}
	while (mlview_utils_is_space (*iptr) == TRUE) {
		iptr ++ ;
	}
	status = mlview_utils_parse_external_id (iptr,
	         &public_id_start,
	         &public_id_end,
	         &system_id_start,
	         &system_id_end,
	         &ptr_end) ;
	if (status != MLVIEW_OK) {
		return MLVIEW_PARSING_ERROR ;
	}
	*a_name_start = name_start ;
	*a_name_end = name_end ;
	*a_public_id_start = public_id_start ;
	*a_public_id_end = public_id_end ;
	*a_system_id_start = system_id_start ;
	*a_system_id_end = system_id_end ;
	return MLVIEW_OK ;
}

/**
 *Parses an external general unparsed entity declaration.
 *@param a_instr the input string that contains the external
 *general unparsed entity declaration to parse.
 *@param a_instr the input string that contains the 
 *entity decl to parse.
 *@param a_name_start out parameter. A pointer to the begining
 *of the parsed entity name.
 *@param a_name_end out parameter. A pointer to the end of the 
 *parsed entity name.
 *@param a_public_id_start out parameter. A pointer the begining
 *of the parsed public id, if any, NULL otherwise.
 *@param a_public_id_end out parameter. A pointer the end of the
 *parsed public id if any, NULL otherwise.
 *@param a_system_id_start out parameter. A pointer to the begining
 *of the parsed system id if any, NULL otherwise.
 *@param a_system_id_end out parameter. A pointer to the end of
 *the parsed system id if any, NULL otherwise.
 *@param a_ndata_start out parameter. A pointer to the beginning of
 * the ndata value if any, NULL otherwise.
 *@param a_ndata_end out parameter. A pointer to the end of the
 *ndata value if any, NULL otherwise.
 *@return MLVIEW_OK upon successful completion, an error code
 *otherwise.
 */
enum MlViewStatus
mlview_utils_parse_external_general_unparsed_entity (gchar *a_instr,
        gchar **a_name_start,
        gchar **a_name_end,
        gchar **a_public_id_start,
        gchar **a_public_id_end,
        gchar **a_system_id_start,
        gchar **a_system_id_end,
        gchar **a_ndata_start,
        gchar **a_ndata_end)
{
	enum MlViewStatus status = MLVIEW_OK ;
	gchar *iptr = NULL, *public_id_start=NULL,
	                                     *name_start=NULL, *name_end=NULL,
	                                                                 *public_id_end=NULL, *system_id_start=NULL,
	                                                                                                       *system_id_end=NULL, *ndata_start=NULL,
	                                                                                                                                         *ndata_end=NULL, *end_ptr=NULL;

	g_return_val_if_fail (a_instr && *a_instr && a_public_id_start
	                      && a_public_id_end && a_system_id_start
	                      && a_system_id_end && a_ndata_start
	                      && a_ndata_end,
	                      MLVIEW_BAD_PARAM_ERROR) ;

	iptr = a_instr ;
	if (iptr[0]    == '<'
	        && iptr[1] == '!'
	        && iptr[2] == 'E'
	        && iptr[3] == 'N'
	        && iptr[4] == 'T'
	        && iptr[5] == 'I'
	        && iptr[6] == 'T'
	        && iptr[7] == 'Y') {
		iptr += 8 ;
	} else {
		return MLVIEW_PARSING_ERROR ;
	}
	if (mlview_utils_is_space (*iptr) == FALSE)
		return MLVIEW_PARSING_ERROR ;
	while (mlview_utils_is_space (*iptr) == TRUE)
		iptr++ ;
	status = mlview_utils_parse_element_name (iptr, &name_end) ;
	if (status != MLVIEW_OK)
		return MLVIEW_PARSING_ERROR ;
	name_start = iptr ;
	iptr = name_end + 1 ;
	if (mlview_utils_is_space (*iptr) == FALSE)
		return MLVIEW_PARSING_ERROR ;
	while (mlview_utils_is_space (*iptr) == TRUE)
		iptr++ ;
	status = mlview_utils_parse_external_id
	         (iptr, &public_id_start, &public_id_end,
	          &system_id_start, &system_id_end,
	          &end_ptr) ;
	if (status != MLVIEW_OK)
		return MLVIEW_PARSING_ERROR ;
	iptr = end_ptr + 1 ;
	while (mlview_utils_is_space (*iptr) == TRUE) {
		iptr ++ ;
	}
	if (*iptr == '>')
		goto end ;

	if (iptr[0]    == 'N'
	        && iptr[1] == 'D'
	        && iptr[2] == 'A'
	        && iptr[3] == 'T'
	        && iptr[4] == 'A') {
		iptr += 5 ;
		while (mlview_utils_is_space (*iptr) == TRUE) {
			iptr ++ ;
		}
		status = mlview_utils_parse_element_name
		         (iptr, &ndata_end) ;
		if (status != MLVIEW_OK) {
			return MLVIEW_PARSING_ERROR ;
		}
		ndata_start = iptr ;
		iptr = ndata_end + 1 ;
		while (mlview_utils_is_space (*iptr) == TRUE) {
			iptr ++ ;
		}
		if (*iptr != '>')
			return MLVIEW_PARSING_ERROR ;
	} else {
		return MLVIEW_PARSING_ERROR ;
	}

end:
	*a_name_start = name_start ;
	*a_name_end = name_end ;
	*a_public_id_start = public_id_start ;
	*a_public_id_end = public_id_end ;
	*a_system_id_start = system_id_start ;
	*a_system_id_end = system_id_end ;
	*a_ndata_start = ndata_start ;
	*a_ndata_end = ndata_end ;
	return MLVIEW_OK ;
}


/**
 *Parses an internal parameter entity declaration.
 *@param a_instr the input string that contains the entity
 *declaration to parse.
 *@param a_name_start out parameter. a pointer to the beginning of
 *the parsed entity name.
 *@param a_name_end out parameter. a pointer to the end of the
 *parsed entity name.
 *@param a_value_start out parameter. a pointer to the begining
 *of the parsed entity value.
 *@param a_value_end out parameter. a pointer the the end of
 *the parsed entity value.
 *@return MLVIEW_OK upon successful completion, an error code
 *otherwise.
 */
enum MlViewStatus
mlview_utils_parse_internal_parameter_entity (gchar *a_instr,
        gchar **a_name_start,
        gchar **a_name_end,
        gchar **a_value_start,
        gchar **a_value_end)
{
	enum MlViewStatus status = MLVIEW_OK ;
	gchar *name_start = NULL, *name_end = NULL,
	                                      *value_start = NULL, *value_end = NULL,
	                                                                        *iptr = NULL ;

	iptr = a_instr ;
	if (iptr[0]    == '<'
	        && iptr[1] == '!'
	        && iptr[2] == 'E'
	        && iptr[3] == 'N'
	        && iptr[4] == 'T'
	        && iptr[5] == 'I'
	        && iptr[6] == 'T'
	        && iptr[7] == 'Y') {
		iptr += 8 ;
	} else {
		return MLVIEW_PARSING_ERROR ;
	}
	if (mlview_utils_is_space (*iptr) == FALSE)
		return MLVIEW_PARSING_ERROR ;
	while (mlview_utils_is_space (*iptr) == TRUE)
		iptr++ ;
	if (*iptr != '%')
		return MLVIEW_PARSING_ERROR ;
	iptr ++ ;
	if (mlview_utils_is_space (*iptr) == FALSE)
		return MLVIEW_PARSING_ERROR ;
	while (mlview_utils_is_space (*iptr) == TRUE)
		iptr++ ;
	status = mlview_utils_parse_element_name (iptr, &name_end) ;
	if (status != MLVIEW_OK)
		return MLVIEW_PARSING_ERROR ;
	name_start = iptr ;
	iptr = name_end + 1 ;
	if (mlview_utils_is_space (*iptr) == FALSE)
		return MLVIEW_PARSING_ERROR ;
	while (mlview_utils_is_space (*iptr) == TRUE)
		iptr++ ;
	status = mlview_utils_parse_entity_value (iptr, &value_start,
	         &value_end) ;
	if (status != MLVIEW_OK)
		return MLVIEW_PARSING_ERROR ;
	iptr = value_end + 2 ;
	while (mlview_utils_is_space (*iptr))
		iptr ++ ;
	if (*iptr != '>')
		return MLVIEW_PARSING_ERROR ;
	*a_name_start = name_start ;
	*a_name_end = name_end ;
	*a_value_start = value_start ;
	*a_value_end = value_end ;
	return MLVIEW_OK ;
}


/**
 *Parses an external parameter entity.
 *@param a_instr the input string that contains the
 *external parameter entity to parse.
 *@param a_name_start out parameter. A pointer to the
 *begining of the parsed entity name.
 *@param a_name_end out parameter. A pointer to the
 *end of the parsed entity name.
 *@param a_public_id_start out parameter. A pointer to
 *the begining of the parsed public id, if any. NULL otherwise.
 *@param a_public_id_end out parameter. A pointer to the
 *end of the parsed public id, if any. NULL otherwise.
 *@param a_system_id_start out parameter. A pointer to the
 *begining of the parsed system id if any, NULL otherwise.
 *@param a_system_id_end. A pointer to the end of the parsed system id
 *if any, NULL otherwise.
 *@return MLVIEW_OK upon succesful completion, 
 *an error code otherwise.
 */
enum MlViewStatus
mlview_utils_parse_external_parameter_entity (gchar *a_instr,
        gchar **a_name_start,
        gchar **a_name_end,
        gchar **a_public_id_start,
        gchar **a_public_id_end,
        gchar **a_system_id_start,
        gchar **a_system_id_end)
{
	enum MlViewStatus status = MLVIEW_OK ;
	gchar *name_start = NULL, *name_end = NULL,
	                                      *public_id_start = NULL, *public_id_end = NULL,
	                                                         *system_id_start = NULL, *system_id_end = NULL,
	                                                                            *iptr = NULL, *end_ptr = NULL ;

	iptr = a_instr ;
	if (iptr[0]    == '<'
	        && iptr[1] == '!'
	        && iptr[2] == 'E'
	        && iptr[3] == 'N'
	        && iptr[4] == 'T'
	        && iptr[5] == 'I'
	        && iptr[6] == 'T'
	        && iptr[7] == 'Y') {
		iptr += 8 ;
	} else {
		return MLVIEW_PARSING_ERROR ;
	}
	if (mlview_utils_is_space (*iptr) == FALSE)
		return MLVIEW_PARSING_ERROR ;
	while (mlview_utils_is_space (*iptr) == TRUE)
		iptr++ ;
	if (*iptr != '%')
		return MLVIEW_PARSING_ERROR ;
	iptr ++ ;
	if (mlview_utils_is_space (*iptr) == FALSE)
		return MLVIEW_PARSING_ERROR ;
	while (mlview_utils_is_space (*iptr) == TRUE)
		iptr++ ;
	status = mlview_utils_parse_element_name (iptr, &name_end) ;
	if (status != MLVIEW_OK)
		return MLVIEW_PARSING_ERROR ;
	name_start = iptr ;
	iptr = name_end + 1 ;
	if (mlview_utils_is_space (*iptr) == FALSE)
		return MLVIEW_PARSING_ERROR ;
	while (mlview_utils_is_space (*iptr) == TRUE)
		iptr++ ;
	status = mlview_utils_parse_external_id
	         (iptr, &public_id_start, &public_id_end,
	          &system_id_start, &system_id_end, &end_ptr) ;
	if (status != MLVIEW_OK)
		return MLVIEW_PARSING_ERROR ;
	iptr = end_ptr ;
	while (mlview_utils_is_space (*iptr) == TRUE)
		iptr++ ;
	if (*iptr != '>')
		return MLVIEW_PARSING_ERROR ;
	*a_name_start = name_start ;
	*a_name_end = name_end ;
	*a_public_id_start = public_id_start ;
	*a_public_id_end = public_id_end ;
	*a_system_id_start = system_id_start ;
	*a_system_id_end = system_id_end ;
	return MLVIEW_OK ;
}

/**
 *Gets the bounds of the word pointed to by an index
 *@param a_phrase the phrase (words separated by spaces) to consider
 *@param a_phrase_len the len of a_phrase
 *@param a_cur_index the index inside the phrase to consider
 *@param a_word_start out param. A pointer to the start of the word
 *@param a_word_end out param. A pointer to the end of the word
 *@return MLVIEW_OK upon successful completion, an error code otherwise
 */
enum MlViewStatus
mlview_utils_get_current_word_bounds (gchar *a_phrase,
                                      gint a_phrase_len,
                                      gint a_cur_index,
                                      gchar **a_word_start,
                                      gchar **a_word_end)
{
	gchar *ptr = NULL, *word_start = NULL, *word_end = NULL, *end_ptr ;

	g_return_val_if_fail (a_phrase && a_word_start && a_word_end,
	                      MLVIEW_BAD_PARAM_ERROR) ;

	g_return_val_if_fail (a_phrase && a_word_start && a_word_end,
	                      MLVIEW_BAD_PARAM_ERROR) ;
	g_return_val_if_fail (a_phrase_len, MLVIEW_BAD_PARAM_ERROR) ;

	end_ptr = &a_phrase[a_phrase_len - 1] ;
	ptr = &a_phrase[a_cur_index] ;

	/*go find the begining of the word*/
	while (1) {
		if (ptr && (g_ascii_isspace (*ptr) == TRUE
		            || *ptr == '>'
		            || *ptr == '<') ) {
			if ((ptr + 1) <= end_ptr) {
				word_start = ptr + 1 ;
				break ;
			} else {
				word_start = ptr ;
				break ;
			}
		} else if (ptr == a_phrase) {
			word_start = ptr ;
			break ;
		}
		ptr -- ;
	}
	if (!word_start)
		return MLVIEW_ERROR ;
	/*go find the  end of the word*/
	ptr = &a_phrase[a_cur_index] ;
	while (1) {
		if (ptr && (g_ascii_isspace (*ptr) == TRUE
		            || *ptr == '>'
		            || *ptr == '<')) {
			if ((ptr + 1) <= end_ptr) {
				word_end = ptr + 1 ;
				break ;
			} else {
				word_end = ptr ;
				break ;
			}
		} else if (ptr == end_ptr) {
			word_end = ptr ;
			break ;
		}
		ptr ++ ;
	}
	if (!word_end)
		return MLVIEW_ERROR ;
	*a_word_start = word_start ;
	*a_word_end = word_end ;

	return MLVIEW_OK ;
}

enum MlViewStatus
mlview_utils_mark_menu_object (GtkWidget *a_menu_object,
                               gchar *a_mark_string)
{
	g_return_val_if_fail (a_menu_object
	                      && GTK_IS_WIDGET (a_menu_object)
	                      && a_mark_string,
	                      MLVIEW_BAD_PARAM_ERROR) ;

	g_object_set_data (G_OBJECT (a_menu_object),
	                   (const gchar*)a_mark_string,
	                   a_menu_object) ;

	return MLVIEW_OK ;
}


enum MlViewStatus
mlview_utils_get_menu_object (GtkWidget *a_menu_root,
                              gchar *a_path,
                              GtkWidget **a_menu_object)
{
	gboolean matches = FALSE ;
	GtkWidget *menu_shell = a_menu_root ;
	GList *menu_children = NULL, *cur_child = NULL ;
	GtkWidget *menu_object = NULL ;
	gchar ** path = NULL ;
	guint i = 0 ;

	g_return_val_if_fail (GTK_IS_MENU_SHELL (a_menu_root),
	                      MLVIEW_BAD_PARAM_ERROR) ;

	path = g_strsplit ((const gchar*)a_path, "/", 0);
	g_return_val_if_fail (path, MLVIEW_ERROR) ;

	for (i = 0 ; path[i] ; i++) {
		if (!GTK_IS_MENU_SHELL (menu_shell)) {
			return MLVIEW_PATH_TOO_DEEP_ERRROR ;
		}
		menu_children = GTK_MENU_SHELL
		                (menu_shell)->children ;
		for (cur_child = menu_children ;
		        cur_child ;
		        cur_child = g_list_next (cur_child)) {
			menu_object = (GtkWidget*)cur_child->data ;

			if (menu_object
			        && g_object_get_data
			        (G_OBJECT (menu_object),
			         path[i])) {
				matches = TRUE ;
				break ;
			}
		}
		if (matches == TRUE) {
			menu_shell = menu_object ;
			matches = FALSE ;
		} else {
			return MLVIEW_OBJECT_NOT_FOUND_ERROR ;
		}
	}
	return MLVIEW_OK ;
}


static void
display_message_dialog (GtkMessageType a_msg_type,
                        const gchar * a_msg_format,
                        va_list a_params)
{
	GtkWidget *err_dialog = NULL ;
	gchar *err_msg = NULL;

	g_return_if_fail (a_msg_format);

	err_msg = g_strdup_vprintf ((const gchar*)a_msg_format, a_params);
	g_return_if_fail (err_msg != NULL);

	err_dialog = gtk_message_dialog_new (NULL, GTK_DIALOG_MODAL,
	                                     a_msg_type,
	                                     GTK_BUTTONS_CLOSE,
	                                     err_msg) ;

	g_return_if_fail (err_dialog) ;
	gtk_dialog_set_default_response
	(GTK_DIALOG (err_dialog), GTK_RESPONSE_ACCEPT) ;
	g_return_if_fail (err_dialog);
	/*let the window be user resizable */
	gtk_window_set_policy (GTK_WINDOW (err_dialog),
	                       FALSE, TRUE, FALSE);
	gtk_dialog_run (GTK_DIALOG (err_dialog));
	gtk_widget_destroy (err_dialog) ;
	if (err_msg) {
		g_free (err_msg) ;
		err_msg = NULL ;
	}
}


/**
 *Displays an error message in a error dialog graphically.
 *The error dialog box is modal.
 *
 *@param a_msg_format the format string of the
 *message, just the same as in the C standard lib
 *printf first parameter.
 *@param ... a list of argument just like the list of
 *argument of the C standard lib printf list of
 *arguments.
 */
void
mlview_utils_display_error_dialog (const gchar * a_msg_format,
                                   ...)
{
	va_list params ;

	va_start (params, a_msg_format) ;
	display_message_dialog (GTK_MESSAGE_ERROR,
	                        a_msg_format,
	                        params) ;
	va_end (params) ;
}

/**
 *Displays a warning message in a error dialog graphically.
 *The error dialog box is modal.
 *
 *@param a_msg_format the format string of the
 *message, just the same as in the C standard lib
 *printf first parameter.
 *@param ... a list of argument just like the list of
 *argument of the C standard lib printf list of
 *arguments.
 */
void
mlview_utils_display_warning_dialog (const gchar * a_msg_format, ...)
{
	va_list params ;

	va_start (params, a_msg_format) ;
	display_message_dialog (GTK_MESSAGE_WARNING,
	                        a_msg_format,
	                        params) ;
	va_end (params) ;
}

/**
 *Displays an informative message in a error dialog graphically.
 *The error dialog box is modal.
 *
 *@param a_msg_format the format string of the
 *message, just the same as in the C standard lib
 *printf first parameter.
 *@param ... a list of argument just like the list of
 *argument of the C standard lib printf list of
 *arguments.
 */
void
mlview_utils_display_message_dialog (const gchar * a_msg_format, ...)
{
	va_list params ;

	va_start (params, a_msg_format) ;
	display_message_dialog (GTK_MESSAGE_INFO,
	                        a_msg_format,
	                        params) ;
	va_end (params) ;
}

/**
 *Tests if an given uri is relative or not.
 *@param a_uri the uri to test.
 *@param a_is_relative the resulting boolean
 *@return MLVIEW_OK upon successful completion, 
 *an error code otherwise.
 */
enum MlViewStatus
mlview_utils_uri_is_relative (const gchar *a_uri,
                              gboolean *a_is_relative)
{
	gchar * escaped_uri = NULL ;
	enum MlViewStatus status = MLVIEW_OK ;
	xmlURI *uri = NULL ;

	g_return_val_if_fail (a_uri && a_is_relative,
	                      MLVIEW_BAD_PARAM_ERROR) ;
	/*
	        *escape all characters in the pathname
	        */
	escaped_uri = (gchar*)xmlURIEscapeStr ((const xmlChar*)a_uri,
	                                       (const xmlChar*)"");
	g_return_val_if_fail (escaped_uri,
	                      MLVIEW_BAD_URI_ERROR) ;
	uri = xmlParseURI (escaped_uri) ;
	g_free(escaped_uri);
	escaped_uri = NULL ;
	if (!uri) {
		return MLVIEW_BAD_URI_ERROR ;
	}
	if (uri->scheme) {
		*a_is_relative = FALSE ;
	} else {
		if (uri->path && strstr(uri->path, "://")) {
			*a_is_relative = FALSE ;
		} else if (uri->path && uri->path[0] != '/') {
			*a_is_relative = TRUE ;
		} else {
			*a_is_relative = FALSE ;
		}
	}

	if (uri) {
		xmlFreeURI (uri) ;
		uri = NULL ;
	}
	return status ;
}

/**
 *Convert a relative URI into an absolute URI, using a given base absolute 
 *URI, which MUST be a directory and NOT a file.
 *@param a_relative_uri the relative uri to convert.
 *@param a_base_uri the base uri to consider
 *@a_absolute_uri out parameter. The result absoclute_uri.
 *@return MLVIEW_OK upon successful completion, an error code otherwise.
 */
enum MlViewStatus
mlview_utils_relative_uri_to_absolute_uri (const gchar *a_relative_uri,
        const gchar *a_base_uri,
        gchar **a_absolute_uri)
{
	enum MlViewStatus status = MLVIEW_OK;
	gboolean is_relative = FALSE;
	gchar *result = NULL;

	g_return_val_if_fail (a_base_uri, MLVIEW_BAD_PARAM_ERROR) ;

	status = mlview_utils_uri_is_relative (a_base_uri, &is_relative);

	if (status != MLVIEW_OK || is_relative) {
		mlview_utils_trace_debug ("The base URI is malformed or is not absolute.") ;
		return MLVIEW_BAD_PARAM_ERROR ;
	}

	status = mlview_utils_uri_is_relative (a_relative_uri, &is_relative);

	if (status != MLVIEW_OK || !is_relative) {
		mlview_utils_trace_debug ("The relative URI is malformed or is not relative.") ;
		return MLVIEW_BAD_PARAM_ERROR ;
	}

	result = g_build_path ("/", a_base_uri, a_relative_uri, NULL) ;

	*a_absolute_uri = result ;
	result = NULL ;
	return MLVIEW_OK ;
}

/**
 *gets the dir name of an URI.
 *@param a_uri the uri ton consider.
 *@return the dirname, must be freed using g_free().
 *NULL is returned if a_uri is badly formed.
 */
gchar *
mlview_utils_get_dir_name_from_uri (const gchar *a_uri)
{
	xmlURI *uri = NULL ;
	gchar *result = NULL ;

	if (!a_uri) {
		result = g_strdup (".") ;
		goto out ;
	}
	uri = xmlParseURI (a_uri) ;
	if (!uri) {
		goto out ;
	}
	result = g_path_get_dirname (uri->path) ;

out:
	if (uri) {
		xmlFreeURI (uri) ;
		uri = NULL ;
	}
	return result  ;
}

/******************************************************
  some usefull patches i wrote for the files gtkclist.c and gtkctree.c:
 
  gtk_clist_absolute_row_top_ypixel(GtkCList *a_clist, gint a_row)
  gtk_ctree_node_absolute_top_ypixel(GtkCTree * a_tree, GtkCTreeNode *a_node)
*********************************************************/

#define CELL_SPACING 1

gint
gtk_clist_absolute_row_top_ypixel (GtkCList * a_clist,
                                   gint a_row)
{
	g_return_val_if_fail (a_clist != NULL, -1);
	g_return_val_if_fail (GTK_IS_CLIST (a_clist), -1);

	return (a_clist->row_height * a_row +
	        (a_row + 1) * CELL_SPACING);
}

gint
gtk_ctree_node_absolute_top_ypixel (GtkCTree * a_tree,
                                    GtkCTreeNode * a_node)
{
	gint pos;

	g_return_val_if_fail (a_tree != NULL, -1);
	g_return_val_if_fail (GTK_IS_CTREE (a_tree), -1);
	g_return_val_if_fail (a_node != NULL, -1);

	pos = g_list_position (a_tree->clist.row_list,
	                       &a_node->list);
	return gtk_clist_absolute_row_top_ypixel (&a_tree->clist,
	        pos);
}

/***********************************
 *Helper function for gtktreeview stuffs
 **********************************/

/**
 *A helper function to build an instance
 *of GtkTreeIter* out of an instance of 
 *GtkTreeRowReference *
 *@param a_model the instance of GtkTreeModel to consider.
 *@param a_ref the row reference 
 *to consider when filling the tree iterator.
 *@param a_iter the allocated instance of GtkTreeIter
 *to be filled by this function.
 *@return TRUE if the iter has been filed, FALSE otherwise.
 */
gboolean
mlview_utils_gtk_row_ref_2_iter (GtkTreeModel * a_model,
                                 GtkTreeRowReference * a_ref,
                                 GtkTreeIter * a_iter)
{
	GtkTreePath *tree_path = NULL;
	gboolean status = FALSE;

	g_return_val_if_fail (a_model && a_ref && a_iter, FALSE);
	tree_path = gtk_tree_row_reference_get_path (a_ref);
	g_return_val_if_fail (tree_path, FALSE);
	status = gtk_tree_model_get_iter
	         (a_model, a_iter, tree_path);
	return status;
}

/**
 *This is a gtk+ add on function.
 *It expands a given row to a given depth.
 *@param a_view the GtkTreeView to consider.
 *@param a_path the path of the row to expand.
 *@param a_depth the expansion depth. If <0, expands
 *the row untill the leaves.
 *@return MLVIEW_OK upon successfull completion, an error code
 *otherwise.
 */
enum MlViewStatus
mlview_utils_gtk_tree_view_expand_row_to_depth (GtkTreeView *a_view,
        GtkTreePath *a_path,
        gint a_depth)
{
	GtkTreeModel *model=NULL ;
	GtkTreeIter iter={0} ;
	gboolean is_ok=FALSE ;
	enum MlViewStatus status = MLVIEW_OK ;

	g_return_val_if_fail (a_view
	                      && GTK_IS_TREE_VIEW (a_view)
	                      && a_path,
	                      MLVIEW_BAD_PARAM_ERROR) ;
	model = gtk_tree_view_get_model (a_view) ;
	g_return_val_if_fail (model, MLVIEW_BAD_PARAM_ERROR) ;
	if (a_depth < 0) {
		gtk_tree_view_expand_row (a_view, a_path,
		                          TRUE) ;
	} else if (a_depth > 0) {
		is_ok = gtk_tree_model_get_iter (model, &iter,
		                                 a_path) ;
		g_return_val_if_fail (is_ok == TRUE, MLVIEW_ERROR) ;
		gtk_tree_view_collapse_row (a_view, a_path) ;
		for (;is_ok == TRUE;
		        is_ok = gtk_tree_model_iter_next
		                (model, &iter)) {
			GtkTreePath *tmp_path=NULL ;
			GtkTreeIter children_iter = {0} ;
			tmp_path = gtk_tree_model_get_path
			           (model, &iter) ;
			if (!tmp_path)
				continue ;
			gtk_tree_view_expand_row (a_view, tmp_path,
			                          FALSE) ;
			gtk_tree_path_free (tmp_path) ;
			tmp_path = NULL ;
			is_ok = gtk_tree_model_iter_children
			        (model,  &children_iter, &iter) ;
			if (is_ok == TRUE) {
				tmp_path = gtk_tree_model_get_path
				           (model, &children_iter) ;
				status = mlview_utils_gtk_tree_view_expand_row_to_depth
				         (a_view, tmp_path, a_depth-1) ;
				if (status != MLVIEW_OK) {
					g_warning ("argh, status == MLVIEW_OK failed.") ;
				}
				gtk_tree_path_free (tmp_path) ;
				tmp_path = NULL ;
			}
		}
	}
	return status ;
}

enum MlViewStatus
mlview_utils_tree_path_string_to_iter (GtkTreeModel *a_model,
                                       gchar *a_tree_path_str,
                                       GtkTreeIter *a_iter)
{
	GtkTreePath *tree_path = NULL ;

	g_return_val_if_fail (a_tree_path_str
	                      && a_iter
	                      && a_model,
	                      MLVIEW_BAD_PARAM_ERROR) ;

	tree_path = gtk_tree_path_new_from_string ((const gchar*)a_tree_path_str) ;
	g_return_val_if_fail (tree_path, MLVIEW_ERROR) ;
	gtk_tree_model_get_iter (a_model, a_iter, tree_path) ;
	if (tree_path) {
		gtk_tree_path_free (tree_path) ;
		tree_path = NULL ;
	}
	return MLVIEW_OK ;
}

enum MlViewStatus
mlview_utils_gtk_tree_view_expand_row_to_depth2 (GtkTreeView *a_view,
        GtkTreeIter *a_iter,
        gint a_depth)
{
	GtkTreePath *tree_path = NULL ;
	GtkTreeModel * model = NULL ;
	enum MlViewStatus status = MLVIEW_OK ;

	g_return_val_if_fail (a_view
	                      && GTK_IS_TREE_VIEW (a_view)
	                      && a_iter,
	                      MLVIEW_BAD_PARAM_ERROR) ;

	model = gtk_tree_view_get_model (a_view) ;
	g_return_val_if_fail (model, MLVIEW_ERROR) ;
	tree_path = gtk_tree_model_get_path (model, a_iter) ;
	g_return_val_if_fail (tree_path, MLVIEW_ERROR) ;
	status = mlview_utils_gtk_tree_view_expand_row_to_depth
	         (a_view, tree_path, a_depth) ;
	if (tree_path) {
		gtk_tree_path_free (tree_path) ;
		tree_path = NULL ;
	}
	return status ;
}

/**
 *Normalizes some text (line length and line ends,
 *spaces).
 *@param a_original original text.
 *@param a_ex_endline string considered as line end in original text.
 *@param a_endline string considered as line end in the output.
 *@param a_space string considered as space in the output.
 *@param a_line_length maximum line length in the output.
 *@return the normalized text.
 */
gchar *
mlview_utils_normalize_text (const gchar *a_original,
                             const gchar *a_ex_endline,
                             const gchar *a_endline,
                             const gchar *a_space,
                             const guint a_line_length)
{
	GString *result = NULL;
	gint endline_chr = 0, endline_b = 0;
	guint max_offset = 0;
	guint offset = 0, index = 0;
	guint line_length = 0;
	guint curs_offset = 0, curs_index = 0;
	gint ex_endl_b = 0, ex_endl_chr = 0;
	gint space_b = 0, space_chr = 0;

	g_return_val_if_fail (a_original && a_endline,
	                      NULL);

	result = g_string_new (a_original);

	g_return_val_if_fail (result, NULL);

	endline_chr = g_utf8_strlen (a_endline, -1);
	endline_b = strlen (a_endline);
	ex_endl_b = strlen (a_ex_endline);
	ex_endl_chr = g_utf8_strlen (a_ex_endline, -1);
	space_b = strlen (a_space);
	space_chr = g_utf8_strlen (a_space, -1);

	max_offset = g_utf8_pointer_to_offset (result->str,
	                                       result->str + result->len);

	for (offset = 0 ; offset < max_offset ; offset++) {
		line_length++;
		if (!strncmp (result->str + index, a_ex_endline, ex_endl_b)) {
			g_string_erase (result, index, ex_endl_b);
			max_offset -= ex_endl_chr;
			g_string_insert (result, index, a_space);
			max_offset += space_chr;
		}
		curs_offset = offset;
		curs_index = index;
		while (curs_offset < max_offset &&
		        !g_unichar_isspace
		        (g_utf8_get_char (result->str + curs_index))) {
			curs_offset++;
			curs_index = g_utf8_find_next_char (result->str + curs_index,
			                                    result->str + result->len)
			             - result->str;
		}
		if (line_length + curs_offset - offset > a_line_length) {
			g_string_insert (result, index, a_endline);
			offset += endline_chr;
			max_offset += endline_chr;
			line_length = 0;
			index += endline_b;
		}
		index = g_utf8_find_next_char (result->str + index,
		                               result->str + result->len) - result->str;
	}

	return g_string_free (result, FALSE);
}


/**
 *Build the absolute path of a data file shipped
 *in the mlview distribution. (basically in $prefix/mlview)
 *@param the file name to lookup
 *@return the computed absolute path. Must be g_free()'ed by
 *the caller.
 */
gchar *
mlview_utils_locate_file (const gchar *a_file_name)
{
	gchar *file_path = NULL, *result = NULL ;

	g_return_val_if_fail (a_file_name, NULL) ;

	if (!strlen (a_file_name))
		return NULL ;

	file_path = g_strconcat ("mlview/",
	                         a_file_name,
	                         NULL) ;

	result =  gnome_program_locate_file (NULL,
	                                     GNOME_FILE_DOMAIN_APP_DATADIR,
	                                     file_path,
	                                     TRUE, NULL) ;
	if (file_path) {
		g_free (file_path) ;
		file_path = NULL ;
	}

	return result ;
}


gchar *
mlview_utils_get_unique_string (const gchar *a_prefix)
{
	gchar *result = NULL ;

	result = g_strdup_printf ("%s-%d", a_prefix,
	                          gv_last_uniq_number) ;
	gv_last_uniq_number ++ ;
	return result ;
}

/**
 *Make sure underscode are doubled so that
 *they don't get interpreted as underline by
 *widgets like menus or treeview titles
 *@param a_int_string the string to escape
 *@return the escaped string. Must be freed
 *using g_free().
 */
gchar *
mlview_utils_escape_underscore_for_gtk_widgets (const gchar *a_in_string)
{
	gchar *ptr = NULL ;
	GString *stringue = NULL ;

	g_return_val_if_fail (a_in_string, NULL) ;

	stringue = g_string_new (NULL) ;

	for (ptr = (gchar*)a_in_string ; ptr && *ptr; ptr++) {
		if (*ptr != '_') {
			g_string_append_c (stringue, *ptr) ;
		} else {
			g_string_append (stringue, "__") ;
		}
	}

	ptr = stringue->str ;
	g_string_free (stringue, FALSE) ;
	return ptr ;
}

/**
 * Replaces all the occurences of word a_lookup_word 
 * by a word a_replacement_word in a given a_input_string.
 * @param a_input_string the input string to consider for the replacement.
 * @param a_lookup_word the word to replace in the input string.
 * @param a_replacement_word the word by which a_lookup_word is to be replaced.
 * *@return a string in which the replacement has been made.
 */
gchar *
mlview_utils_replace_word (const gchar *a_input_string,
                           const gchar *a_lookup_word,
                           const gchar *a_replacement_word)
{
	gchar *found_str = NULL, *result = NULL ;
	gint lookup_string_len = 0, found_str_index =  0;
	GString *input_string = NULL, *stringue = NULL ;

	g_return_val_if_fail (a_input_string
	                      && a_lookup_word
	                      && a_replacement_word,
	                      NULL) ;

	lookup_string_len = strlen (a_lookup_word) ;
	if (!lookup_string_len)
		return NULL ;

	found_str = g_strstr_len (a_input_string,
	                          lookup_string_len,
	                          a_lookup_word) ;

	if (!found_str || found_str < a_input_string)
		return NULL ;

	found_str_index = found_str - a_input_string ;
	input_string = g_string_new (a_input_string) ;
	if (!input_string) {
		g_warning ("g_string_new() failed") ;
		return NULL ;
	}
	stringue = g_string_erase (input_string,
	                           found_str_index,
	                           lookup_string_len) ;
	if (!stringue) {
		g_warning ("g_string_erase() failed") ;
		goto error ;
	}

	stringue = g_string_insert (input_string, found_str_index,
	                            a_replacement_word) ;
	if (!stringue) {
		g_warning ("g_string_insert() failed") ;
		goto error ;
	}

	result = input_string->str ;
	if (input_string) {
		g_string_free (input_string, FALSE) ;
		input_string = NULL ;
	}
	return result ;

error:
	if (input_string) {
		g_string_free (input_string, TRUE) ;
		input_string = NULL ;
	}
	return NULL ;
}

/**
 * Get the text of the selected combo item.
 * This is a helper method for gtk+ < 2.6. In gtk+ 2.6
 * the method gtk_combo_box_get_active_text does the same thing.
 * @param a_combo_box the current instance of GtkComboBox
 * *@return a pointer to the internal char string stored in the
 * active combo item. This pointer must not be freed by the caller !
 */
const gchar *
mlview_utils_combo_box_get_active_text (GtkComboBox *a_combo_box)
{
	GtkTreeIter iter = {0};
	gchar *text = NULL;
	GtkTreeModel * model = NULL ;

	g_return_val_if_fail (GTK_IS_COMBO_BOX (a_combo_box), NULL);

	model = gtk_combo_box_get_model (a_combo_box) ;
	g_return_val_if_fail (GTK_IS_LIST_STORE (model), NULL);

	if (gtk_combo_box_get_active_iter (a_combo_box, &iter))
		gtk_tree_model_get (model, &iter,
		                    0, &text, -1);
	return text;
}


GList *
mlview_utils_push_on_stack (GList *a_stack, gpointer a_element)
{
	GList *result = NULL ;

	g_return_val_if_fail (a_element, NULL) ;

	result = g_list_prepend (a_stack, a_element) ;
	return result ;
}

GList *
mlview_utils_pop_from_stack (GList *a_stack, gpointer *a_data)
{
	GList *result = NULL ;

	g_return_val_if_fail (a_stack && a_data, NULL) ;

	*a_data = a_stack->data ;
	result = g_list_delete_link (a_stack, a_stack) ;
	return result ;
}

GList *
mlview_utils_peek_from_stack (GList *a_stack, gpointer *a_data)
{
	g_return_val_if_fail (a_stack && a_data, NULL);

	*a_data = a_stack->data ;
	return a_stack;
}

GtkTextIter *
mlview_utils_text_iter_forward_chars_dup (GtkTextIter *a_iter,
        guint a_count)
{
	GtkTextIter *result = NULL ;

	g_return_val_if_fail (a_iter, NULL) ;

	result = gtk_text_iter_copy (a_iter) ;
	if (!result) {
		return NULL ;
	}
	if (!gtk_text_iter_forward_chars (result, a_count)) {
		if (result) {
			gtk_text_iter_free (result) ;
			result = NULL ;
		}
	}
	return result ;
}

enum MlViewStatus
mlview_utils_text_iter_get_char_at (GtkTextIter *a_iter,
                                    guint a_count,
                                    gunichar *a_char)
{
	GtkTextIter *cur = NULL ;
	gunichar unichar = 0 ;

	g_return_val_if_fail (a_iter && a_char, MLVIEW_BAD_PARAM_ERROR) ;

	cur = gtk_text_iter_copy (a_iter) ;
	if (!cur) {
		return MLVIEW_BAD_PARAM_ERROR ;
	}
	gtk_text_iter_forward_chars (cur, a_count) ;
	unichar = gtk_text_iter_get_char (cur) ;
	if (cur) {
		gtk_text_iter_free (cur) ;
		cur = NULL ;
	}
	if (!unichar) {
		return MLVIEW_ERROR ;
	}
	*a_char = unichar ;
	return MLVIEW_OK ;
}

enum MlViewStatus
mlview_utils_text_iter_get_iter_at (GtkTextIter *a_cur_iter,
                                    guint a_count,
                                    GtkTextIter **a_iter)
{
	enum MlViewStatus status = MLVIEW_OK ;
	GtkTextIter *cur = NULL ;

	g_return_val_if_fail (a_cur_iter && a_iter, MLVIEW_BAD_PARAM_ERROR) ;

	cur= gtk_text_iter_copy (a_cur_iter) ;
	if (!cur) {
		status = MLVIEW_ERROR ;
		goto cleanup ;
	}
	if (!gtk_text_iter_forward_chars (cur, a_count)) {
		status = MLVIEW_EOF_ERROR ;
		goto cleanup ;
	}
	*a_iter = cur ;
	cur = NULL ;

cleanup:
	if (cur) {
		gtk_text_iter_free (cur) ;
		cur = NULL ;
	}
	return status ;
}


enum MlViewStatus
mlview_utils_lookup_action_group (GtkUIManager *a_manager,
                                  const gchar * a_name,
                                  GtkActionGroup **a_action_group)
{
	GList *action_groups = NULL, *iter = NULL;
	gchar *action_group_name = NULL ;
	GtkActionGroup *action_group = NULL ;

	g_return_val_if_fail (a_manager && GTK_IS_UI_MANAGER (a_manager),
	                      MLVIEW_BAD_PARAM_ERROR) ;

	action_groups = gtk_ui_manager_get_action_groups (a_manager) ;

	/*
	 * walk through the list of action group and look for the one that
	 * has the name "a_name".
	 */
	for (iter = action_groups ;iter ; iter = iter->next) {
		if (iter->data) {
			action_group = (GtkActionGroup*) iter->data ;
			action_group_name = (gchar*)
			                    gtk_action_group_get_name (action_group) ;
			if (action_group_name
			        && !strcmp (action_group_name, a_name)) {
				*a_action_group = action_group ;
				return MLVIEW_OK ;
			} else {
				continue ;
			}
		}
	}
	return MLVIEW_OK ;
}
