/* -*- Mode: C; indent-tabs-mode:nil; c-basic-offset: 8-*- */
/*
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute 
 *it and/or modify it under the terms of 
 *the GNU General Public License as published by the 
 *Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */

#ifndef __MLVIEW_KB_ENG_H__
#define __MLVIEW_KB_ENG_H__

#include "mlview-utils.h"

G_BEGIN_DECLS

typedef struct _MlViewKBEng MlViewKBEng ;
typedef struct _MlViewKBEngPriv MlViewKBEngPriv ;

/**
 *This strcutures holds keyinput (used in the keybinding parsing)
 *We basically want to be able to parse key sequences like
 *=> 'Ctrl-x-a' (keep the ctrl key pressed and press x, then a)
 *=> or 'Alt-x a-command' (keep the alt key pressed, then press x,
 *release the alt key and then type a-command)
 */
struct MlViewKeyInput
{
	/*
	        *the key pressed, one of the values defined
	        *in gdk/gdkkeysyms.h
	        */
	guint key ;
	/*
	 *the modifier pressed during the key press
	 *is one of GdkModifierType
	 */
	GdkModifierType modifier_mask ;
	/*the date of the key press*/
	guint32 date ;

} ;


typedef void (*MlViewKBEngAction) (gpointer a_context) ;

struct MlViewKBDef
{
	struct MlViewKeyInput key_inputs[10] ;
	gint key_inputs_len ;
	MlViewKBEngAction action ;
	const gchar *name ;
} ;

struct _MlViewKBEng
{
	MlViewKBEngPriv *priv ;
} ;

MlViewKBEng * mlview_kb_eng_new (void) ;

enum MlViewStatus mlview_kb_eng_register_a_key_binding (MlViewKBEng *a_this,
        const struct MlViewKBDef *a_kb_def) ;

enum MlViewStatus mlview_kb_eng_register_key_bindings (MlViewKBEng *a_this,
        const struct MlViewKBDef *a_kb_defs_tab,
        gint a_kb_defs_tab_len) ;

enum MlViewStatus mlview_kb_eng_lookup_a_key_binding (MlViewKBEng *a_this,
        const struct MlViewKeyInput *a_key_input_tab,
        gint a_key_input_tab_len,
        struct MlViewKBDef **a_key_binding_found) ;

enum MlViewStatus mlview_kb_lookup_key_binding_from_key_press (MlViewKBEng *a_this,
        GdkEventKey *a_event,
        struct MlViewKBDef **a_key_binding_found) ;

enum MlViewStatus  mlview_kb_eng_clear_key_inputs_queue (MlViewKBEng *a_this) ;

void mlview_kb_eng_destroy (MlViewKBEng *a_this) ;

G_END_DECLS
#endif /*__MLVIEW_KB_ENG_H__*/
