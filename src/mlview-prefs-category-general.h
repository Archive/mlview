/* -*- Mode: C++; indent-tabs-mode: true; c-basic-offset: 4 -*- */
/*
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute 
 *it and/or modify it under the terms of 
 *the GNU General Public License as published by the 
 *Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYING file for copyright information.
 */
#ifndef MLVIEW_PREFS_CATEGORY_GENERAL_H
#define MLVIEW_PREFS_CATEGORY_GENERAL_H

#include "mlview-prefs-storage-manager.h"
#include "mlview-prefs-category.h"

namespace mlview
{
///
/// This structure holds PrefsCategoryGeneral
/// private members
///
struct PrefsCategoryGeneralPriv;

///
/// This class is the category handling general preferences.
///
class PrefsCategoryGeneral : public PrefsCategory
{
	friend struct PrefsCategoryGeneralPriv;
	PrefsCategoryGeneralPriv *m_priv;

	//forbid copy/assignation
	PrefsCategoryGeneral (PrefsCategoryGeneral const&) ;
	PrefsCategoryGeneral& operator= (PrefsCategoryGeneral const&) ;

	//forbid assignation/copy
	PrefsCategoryGeneral (PrefsCategory const&) ;
	PrefsCategoryGeneral& operator= (PrefsCategory const&) ;

public:
	///
	/// Create a PrefsCategoryGeneral object.
	///
	/// \param manager the storage manager used to access preferences
	PrefsCategoryGeneral (PrefsStorageManager *manager);
	///
	/// Default destructor.
	///
	virtual ~PrefsCategoryGeneral ();

	///
	/// Retrieve the 'default edition view' property default value.
	/// This property represents the view which is used by default when documents
	/// are created or opened.
	///
	/// \return a string containing the value or null.
	///
	const UString get_default_edition_view_default ();

	///
	/// Retrieve the 'default edition view' property value.
	/// This property represents the view which is used by default when documents
	/// are created or opened.
	///
	/// \return a string containing the value or null.
	///
	const UString get_default_edition_view ();
	///
	/// Store the 'default edition view' property value.
	/// This property represents the view which is used by default when documents
	/// are created or opened.
	///
	/// \param name the new value
	///
	void set_default_edition_view (const UString& name);

	///
	/// Retrieve the 'use validation' property default value.
	/// This property when set turns validation of documents on.
	///
	/// \return TRUE if set, FALSE otherwise
	///
	bool use_validation_default ();
	///
	/// Retrieve the 'use validation' property value.
	/// This property when set turns validation of documents on.
	///
	/// \return TRUE if set, FALSE otherwise
	///
	bool use_validation ();
	///
	/// Store a new 'use validation' property value
	/// This property when set turns validation of documents on.
	///
	/// \param flag whether to turn validation on or not.
	///
	void set_use_validation (bool flag);

       /// The category id
	static const char* CATEGORY_ID;
};

} // namespace mlview


#endif
