/* -*- Mode: C; indent-tabs-mode:nil; c-basic-offset: 8-*- */

/*
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute 
 *it and/or modify it under the terms of 
 *the GNU General Public License as published by the 
 *Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */

#include <string.h>
#include "mlview-editor-dbc.h"
#include "mlview-idbc.h"
#include "mlview-utils.h"

#ifdef MLVIEW_WITH_DBUS
#include <dbus/dbus.h>
#include <dbus/dbus-glib-lowlevel.h>

#define PRIVATE(obj) (obj)->priv

struct _MlViewEditorDBCPriv
{
	DBusConnection *dbus_connection ;
	gboolean dispose_has_run ;
} ;

#define ORG_MLVIEW_SERVICE "org.mlview.Service"
#define PATH_ORG_MLVIEW_EDITOR_OBJECT "/org/mlview/MlViewEditorObject"
#define INTERFACE_ORG_MLVIEW_EDITOR_IFACE "org.mlview.EditorIface"

static void mlview_editor_dbc_class_init (MlViewEditorDBCClass *a_klass) ;

static void mlview_editor_dbc_init (MlViewEditorDBC *a_this) ;

static void mlview_editor_dbc_idbc_init (MlViewIDBC *a_this) ;

static void mlview_editor_dbc_dispose (GObject *a_this) ;

static void mlview_editor_dbc_finalize (GObject *a_this) ;

static enum MlViewStatus get_bus (MlViewEditorDBC *a_this,
                                  DBusConnection **a_dbus_connection,
                                  GError **a_error)  ;

static GObjectClass *gv_parent_class = NULL ;

static void
mlview_editor_dbc_class_init (MlViewEditorDBCClass *a_klass)
{
	GObjectClass *gobject_class = NULL ;

	g_return_if_fail (a_klass != NULL) ;
	gv_parent_class = g_type_class_peek_parent (a_klass) ;
	g_return_if_fail (gv_parent_class) ;
	gobject_class = G_OBJECT_CLASS (a_klass) ;

	gobject_class->dispose = mlview_editor_dbc_dispose ;
	gobject_class->finalize = mlview_editor_dbc_finalize ;
}

static void
mlview_editor_dbc_init (MlViewEditorDBC *a_this)
{
	g_return_if_fail (a_this && MLVIEW_IS_EDITOR_DBC (a_this)) ;

	PRIVATE (a_this) = g_try_malloc (sizeof (MlViewEditorDBCPriv)) ;
	if (!PRIVATE (a_this)) {
		mlview_utils_trace_debug ("malloc failed") ;
		return ;
	}
	memset (PRIVATE (a_this), 0, sizeof (MlViewEditorDBCPriv)) ;
}

static void
mlview_editor_dbc_idbc_init (MlViewIDBC *a_this)
{}

static void
mlview_editor_dbc_dispose (GObject *a_this)
{
	MlViewEditorDBC *thiz = NULL ;

	thiz = MLVIEW_EDITOR_DBC (a_this) ;
	g_return_if_fail (thiz) ;

	if (PRIVATE (thiz) && PRIVATE (thiz)->dispose_has_run) {
		return ;
	}

	if (((GObjectClass*)gv_parent_class)->dispose) {
		gv_parent_class->dispose (a_this) ;
	}
	PRIVATE (thiz)->dispose_has_run = TRUE  ;
}

static void
mlview_editor_dbc_finalize (GObject *a_this)
{
	MlViewEditorDBC *thiz = NULL ;

	thiz = MLVIEW_EDITOR_DBC (a_this) ;
	g_return_if_fail (thiz) ;

	if (PRIVATE (thiz)) {
		g_free (PRIVATE (thiz)) ;
		PRIVATE (thiz) = NULL;
	}
	if (gv_parent_class->finalize) {
		gv_parent_class->finalize (a_this) ;
	}
}

static enum MlViewStatus
get_bus (MlViewEditorDBC *a_this,
         DBusConnection **a_dbus_connection,
         GError **a_error)
{
	g_return_val_if_fail (a_this && MLVIEW_IS_EDITOR_DBC (a_this)
	                      && PRIVATE (a_this),
	                      MLVIEW_BAD_PARAM_ERROR) ;

	if (!PRIVATE (a_this)->dbus_connection) {
		if (mlview_idbc_get_session_bus
		        (MLVIEW_IDBC (a_this),
		         &PRIVATE (a_this)->dbus_connection,
		         a_error)) {
			return MLVIEW_BUS_ERROR ;
		}
	}
	*a_dbus_connection = PRIVATE (a_this)->dbus_connection ;
	return MLVIEW_OK ;
}


/*****************
 * public methods
 *****************/

GType
mlview_editor_dbc_get_type (void)
{
	static GType type = 0 ;

	if (!type) {
		static const GTypeInfo type_info = {
		                                       sizeof (MlViewEditorDBCClass),
		                                       NULL, NULL,
		                                       (GClassInitFunc) mlview_editor_dbc_class_init,
		                                       NULL, NULL,
		                                       sizeof (MlViewEditorDBC),
		                                       0,
		                                       (GInstanceInitFunc)mlview_editor_dbc_init
		                                   } ;
		type = g_type_register_static (G_TYPE_OBJECT,
		                               "MlViewEditorDBC",
		                               &type_info, 0) ;
		static const GInterfaceInfo idbc_info = {
		                                            (GInterfaceInitFunc) mlview_editor_dbc_idbc_init,
		                                            NULL, NULL
		                                        } ;
		g_type_add_interface_static (type, MLVIEW_TYPE_IDBC,
		                             &idbc_info) ;
	}
	return type ;
}

MlViewEditorDBC *
mlview_editor_dbc_new (void)
{
	MlViewEditorDBC *editor_proxy = NULL ;

	editor_proxy = g_object_new (MLVIEW_TYPE_EDITOR_DBC, NULL) ;
	return editor_proxy ;
}

enum MlViewStatus
mlview_editor_dbc_load_xml_file_with_dtd (MlViewEditorDBC *a_this,
        const gchar *a_service_name,
        const gchar *a_doc_uri,
        const gchar *a_dtd_uri)
{
	enum MlViewStatus status = MLVIEW_OK ;
	DBusMessage *message = NULL, *reply = NULL ;
	DBusConnection *dbus_connection = NULL ;
	DBusError dbus_error = {0} ;
	gchar *dtd_uri = NULL ;

	g_return_val_if_fail (a_this && MLVIEW_IS_EDITOR_DBC (a_this),
	                      MLVIEW_BAD_PARAM_ERROR) ;

	g_return_val_if_fail (a_doc_uri, MLVIEW_BAD_PARAM_ERROR) ;

	dbus_error_init (&dbus_error) ;

	status = get_bus (a_this, &dbus_connection, NULL) ;
	if (status != MLVIEW_OK || !dbus_connection) {
		return MLVIEW_ERROR ;
	}

	message = dbus_message_new_method_call
	          (a_service_name,
	           PATH_ORG_MLVIEW_EDITOR_OBJECT,
	           INTERFACE_ORG_MLVIEW_EDITOR_IFACE,
	           "load_xml_file_with_dtd") ;
	g_return_val_if_fail (message, MLVIEW_OUT_OF_MEMORY_ERROR) ;

	if (!a_dtd_uri) {
		dtd_uri = (gchar*)"" ;
	} else {
		dtd_uri = (gchar*)a_dtd_uri ;
	}
	if (!dbus_message_append_args (message,
	                               DBUS_TYPE_STRING, a_doc_uri,
	                               DBUS_TYPE_STRING, dtd_uri,
	                               DBUS_TYPE_INVALID)) {

		mlview_utils_trace_debug ("append args to message failed") ;
		status = MLVIEW_OK ;
	}

	reply = dbus_connection_send_with_reply_and_block
	        (dbus_connection,
	         message,
	         -1/*no timeout*/,
	         &dbus_error) ;

	if (dbus_error_is_set (&dbus_error)) {
		mlview_utils_trace_debug ("Got an error when "
		                          "calling remote mehod") ;
		mlview_utils_trace_debug (dbus_error.message) ;
		status = MLVIEW_ERROR ;
		goto cleanup ;
	}

	if (!reply) {
		mlview_utils_trace_debug ("Could not get reply from server") ;
		status = MLVIEW_ERROR ;
		goto cleanup ;
	}

	if (!dbus_message_get_args (reply, &dbus_error,
	                            DBUS_TYPE_INT32, &status,
	                            DBUS_TYPE_INVALID)) {
		mlview_utils_trace_debug ("getting args from reply failed") ;
		status = MLVIEW_ERROR ;
		goto cleanup ;
	}

cleanup:
	if (message) {
		dbus_message_unref (message) ;
		message = NULL ;
	}
	if (reply) {
		dbus_message_unref (reply) ;
		reply = NULL ;
	}
	return status ;
}

#endif /*MLVIEW_WITH_DBUS*/
