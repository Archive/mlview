/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 8-*- */

/*
 *This file is part of MlView
 *
 *MlView is free software; you can redistribute 
 *it and/or modify it under the terms of 
 *the GNU General Public License as published 
 *by the Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. If not, write to the 
 *Free Software Foundation, Inc., 59 
 *Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/unistd.h>
#include <fcntl.h>
#include <time.h>
#include <string.h>
#include <libgnomevfs/gnome-vfs.h>
#include <libgnomevfs/gnome-vfs-mime-utils.h>
#include <libxml/uri.h>
#include <libxml/nanohttp.h>
#include <libxml/nanoftp.h>
#include "mlview-file-descriptor.h"

/**
 *@file
 *The class definition of #MlViewFileDescriptor.
 */

/**
 *The private members of #MlViewFileDescriptor class.
 */
struct _MlViewFileDescriptorPrivate
{
	/** the gnome-vfs URI */
	GnomeVFSURI *uri;

	/** the file info. assiocated with the uri */
	GnomeVFSFileInfo file_info;

	/** the string representation of the uri */
	gchar * uri_string;

	/** the mime type of the file */
	gchar * mime_type;

	/**
	 *The last modification date of the current file.
	 *is valid only in the case of a local file
	 */
	time_t last_modif_date;
};

enum MLVIEW_FILE_DESCRIPTOR_STATUS {
    MLVIEW_FILE_DESCRIPTOR_BAD_PARAM = -2,
    MLVIEW_FILE_DESCRIPTOR_FETCH_FAILED = -1,
    MLVIEW_FILE_DESCRIPTOR_OK = 0,
    MLVIEW_FILE_DESCRIPTOR_IS_LOCAL = 1
};

#define PRIVATE(file_desc) (file_desc->priv)

/**
 *The constructor of #MlViewFileDescriptor class.
 *
 *@param a_file_uri The uri to associate to the file descriptor.
 *This null terminated char is duplicated. Then caller still owns
 *the pointer.
 *Note that this argument _must_ be a uri.
 *
 *@return the newly created file descriptor or NULL if could not
 *Have been created.
 */
MlViewFileDescriptor *
mlview_file_descriptor_new (const gchar * a_file_uri)
{
	MlViewFileDescriptor *fd = NULL;
	GnomeVFSResult result;

	g_return_val_if_fail (a_file_uri != NULL, NULL);

	fd = (MlViewFileDescriptor*) g_malloc0 (sizeof (MlViewFileDescriptor));
	PRIVATE (fd) = (MlViewFileDescriptorPrivate*)g_malloc0
	               (sizeof (MlViewFileDescriptorPrivate));

	PRIVATE (fd)->uri = gnome_vfs_uri_new (a_file_uri);
	g_return_val_if_fail (PRIVATE (fd)->uri != NULL, NULL);

	result = gnome_vfs_get_file_info_uri (PRIVATE (fd)->uri,
	                                      &PRIVATE (fd)->file_info,
	                                      GNOME_VFS_FILE_INFO_DEFAULT);
	if ((result != GNOME_VFS_OK) && (result != GNOME_VFS_ERROR_NOT_FOUND)) {
		return NULL;
	}

	PRIVATE (fd)->uri_string = g_strdup (a_file_uri);

	PRIVATE (fd)->mime_type = gnome_vfs_get_mime_type (a_file_uri);

	if (gnome_vfs_uri_is_local (PRIVATE (fd)->uri)) {
		PRIVATE (fd)->last_modif_date = PRIVATE (fd)->file_info.mtime;
	}

	return fd;
}


/**
 *The destructor of the #MlViewFileDescriptor class.
 *@param a_this the instance of #MlViewFileDescriptor to destroy.
 */
void
mlview_file_descriptor_destroy (MlViewFileDescriptor *a_this)
{
	g_return_if_fail (a_this != NULL);

	if (!PRIVATE (a_this))
		return;

	if (PRIVATE (a_this)->uri) {
		gnome_vfs_uri_unref (PRIVATE (a_this)->uri);
		PRIVATE (a_this)->uri = NULL;
	}

	if (PRIVATE (a_this)->file_info.refcount > 0) {
		gnome_vfs_file_info_unref (&PRIVATE (a_this)->file_info);
	}

	if (PRIVATE (a_this)->uri_string != NULL) {
		g_free (PRIVATE (a_this)->uri_string);
		PRIVATE (a_this)->uri_string = NULL ;
	}

	if (PRIVATE (a_this)->mime_type != NULL) {
		g_free (PRIVATE (a_this)->mime_type);
		PRIVATE (a_this)->mime_type = NULL ;
	}

	g_free (PRIVATE (a_this));
	PRIVATE (a_this) = NULL ;
	g_free (a_this);
}


/**
 *Sets the last modified time of the current instance 
 *of file descriptor to the current time. 
 *
 *@param a_this the current instance of MlViewFileDescriptor.
 */
void
mlview_file_descriptor_update_modified_time (MlViewFileDescriptor *a_this)
{
	g_return_if_fail (a_this != NULL);
	g_return_if_fail (PRIVATE (a_this) != NULL);

	if (gnome_vfs_uri_is_local (PRIVATE (a_this)->uri)) {
		PRIVATE (a_this)->last_modif_date = time (NULL) ;
	}
}


/**
 *Check wether the in memory file has been modified.
 *
 *@param a_this the current instance of #MlViewFileDescriptor. 
 *(in memory file)
 *@param a_is_modified out parameter. Is set to TRUE if the lastp modif date of
 *the in memory file is newer than the last modif date of the on disk file. 
 *This parameter is valid if and only if the return value of this method is 0.
 *
 *@return 0 upon successfull completion,  -1 otherwise.
 */
gint
mlview_file_descriptor_is_modified (const MlViewFileDescriptor *a_this,
                                    gboolean * a_is_modified)
{
	*a_is_modified = FALSE;
	g_return_val_if_fail (a_this != NULL, -1);
	g_return_val_if_fail (PRIVATE (a_this) != NULL, -1);

	if (gnome_vfs_uri_is_local (PRIVATE (a_this)->uri)) {
		GnomeVFSResult result;

		result = gnome_vfs_get_file_info_uri (PRIVATE (a_this)->uri,
		                                      &PRIVATE (a_this)->file_info,
		                                      GNOME_VFS_FILE_INFO_DEFAULT);
		if (result != GNOME_VFS_OK)
			return -1;

		*a_is_modified =
		    (PRIVATE (a_this)->file_info.mtime
		     <
		     PRIVATE (a_this)->last_modif_date) ;

		PRIVATE (a_this)->last_modif_date =
		    PRIVATE (a_this)->file_info.mtime;
	}

	return 0;
}

/**
 * Gets the uri of the file descriptor
 *
 * @param a_this the 'this' pointer
 * @return the uri of the file. The user is responsible of freeing
 * the returned pointer using g_free()
 */
gchar *
mlview_file_descriptor_get_uri (const MlViewFileDescriptor *a_this)
{
	gchar *uri_string = NULL;

	g_return_val_if_fail (a_this != NULL, NULL);
	g_return_val_if_fail (PRIVATE (a_this) != NULL, NULL);

	uri_string = gnome_vfs_uri_to_string (
	                 PRIVATE (a_this)->uri,
	                 (GnomeVFSURIHideOptions)
	                 (GNOME_VFS_URI_HIDE_PASSWORD | GNOME_VFS_URI_HIDE_HOST_PORT));

	return uri_string;
}
/**
 *Gets the file path of the current file.
 *
 *@param a_this the this pointer.
 *@return the file path. Note that if no file path is associated,
 *this method returns NULL. Note also that the caller MUST free the
 *returned string using g_free().
 */
gchar *
mlview_file_descriptor_get_file_path (const MlViewFileDescriptor*a_this)
{
	gchar *file_path = NULL;

	g_return_val_if_fail (a_this != NULL, NULL);
	g_return_val_if_fail (PRIVATE (a_this) != NULL, NULL);

	if (gnome_vfs_uri_is_local (PRIVATE (a_this)->uri))
		file_path = g_strdup (gnome_vfs_uri_get_path
		                      (PRIVATE (a_this)->uri)) ;
	else
		file_path = gnome_vfs_uri_to_string
		            (PRIVATE (a_this)->uri,
		             (GnomeVFSURIHideOptions)
		             (GNOME_VFS_URI_HIDE_PASSWORD | GNOME_VFS_URI_HIDE_HOST_PORT));

	return file_path;
}

/**
 * NB: NOT USED ATM - tns - jeu sep 23 20:40:11 CEST 2004
 *
 *Sets the file path associated to the current instance of 
 *#MlViewFileDescriptor.
 *@param a_this the "this pointer".
 *@param a_file_path the file path to set.
 */
void
mlview_file_descriptor_set_file_path (MlViewFileDescriptor *a_this,
                                      const gchar * a_file_path)
{
	g_return_if_fail (a_this != NULL);
	g_return_if_fail (PRIVATE (a_this) != NULL);

	/* FIXME: rework that. */
	PRIVATE (a_this)->uri = gnome_vfs_uri_new (gnome_vfs_get_uri_from_local_path (a_file_path));
	gnome_vfs_get_file_info_uri (PRIVATE (a_this)->uri, &(PRIVATE (a_this)->file_info), GNOME_VFS_FILE_INFO_DEFAULT);
}


/**
 *Tests if the file is a regular file.
 *
 *@param a_this the "this pointer".
 *@param a_is_reg out parameter. Is set to TRUE if the current file is a regular
 *file, is set to FALSE otherwise.
 *@return 0 upon successfull completion, -1 otherwise.
 */
gint
mlview_file_descriptor_is_regular_file (const MlViewFileDescriptor *a_this,
                                        gboolean * a_is_reg)
{
	*a_is_reg = FALSE;
	g_return_val_if_fail (a_this != NULL, -1);
	g_return_val_if_fail (PRIVATE (a_this) != NULL, -1);

	*a_is_reg = (PRIVATE (a_this)->file_info.type == GNOME_VFS_FILE_TYPE_REGULAR) ? TRUE : FALSE;

	return 0;
}


/**
 *Tests if the current file is on the local file system.
 *
 *@param a_this the "this pointer".
 *@param a_is_local out parameter. Is set to TRUE if the file is local,
 *is set to FALSE otherwise.
 *@return 0 upon successfull completion, an error code otherwise.
 */
gint
mlview_file_descriptor_is_local (MlViewFileDescriptor *a_this,
                                 gboolean * a_is_local)
{
	g_return_val_if_fail (a_this != NULL, -1);
	g_return_val_if_fail (PRIVATE (a_this) != NULL, -1);

	*a_is_local = gnome_vfs_uri_is_local (PRIVATE (a_this)->uri);

	return 0;
}

/**
 * Return the URI mime-type
 *
 * @param a_this the "this pointer"
 *
 * @return the mime-type. Must be freed by the caller using g_free () .
 */
gchar *
mlview_file_descriptor_get_mime_type (MlViewFileDescriptor *a_this)
{
	g_return_val_if_fail (a_this != NULL, NULL);
	g_return_val_if_fail (PRIVATE (a_this) != NULL, NULL);

	return g_strdup (PRIVATE (a_this)->mime_type);
}
