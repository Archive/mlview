/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 8-*- */

/*
 *This file is part of MlView
 *
 *MlView is free software; you can redistribute it and/or 
 *modify it under the terms of 
 *the GNU General Public License as published 
 *by the Free Software Foundation; either version 2
 *or (at your option) any later version.
 *
 *MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView.
 *see the file COPYING. 
 *If not, write to the 
 *Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */

#include <stdio.h>
#include <glib.h>

#define DBUS_API_SUBJECT_TO_CHANGE
#include <dbus/dbus.h>
#include <dbus/dbus-glib-lowlevel.h>

#include "config.h"
#include "mlview-editor.h"
#include "mlview-app.h"
#include "mlview-app-context.h"
#include "mlview-dbus-manager.h"

/***************
 * DEFINITIONS *
 ***************/
static gboolean mlview_dbus_manager_is_mlview_instance_running (DBusConnection *connection);
static void
mlview_dbus_manager_delegate_init_process (DBusConnection *connection, MlViewApp *app, gchar **args);

/* MlViewInstance DBus Object methods */
static DBusHandlerResult mlview_dbus_manager_instance_message_handler (DBusConnection *connection, DBusMessage *message, void *data);
static void mlview_dbus_manager_instance_unregistered_cb (DBusConnection *connection, void *data);

/* DBus ClientSide method invocations */
static gboolean
invoke_org_mlview_instance_opendocument (DBusConnection *connection, const gchar *uri);

/* DBus ServerSide method implementations */
static DBusHandlerResult
process_org_mlview_instance_opendocument (DBusConnection *connection, DBusMessage *message, void *data);
static DBusHandlerResult
process_org_mlview_instance_getdocument (DBusConnection *connection, DBusMessage *message, void *data);


/*******************
 * PRIVATE METHODS *
 *******************/
static gboolean
mlview_dbus_manager_is_mlview_instance_running (DBusConnection *connection)
{
	DBusError		dbus_error;
	dbus_uint32_t		result;


	dbus_error_init (&dbus_error);

	result = dbus_bus_acquire_service (connection,
	                                   MLVIEW_DBUS_INSTANCE_SERVICE_NAME,
	                                   0, /* flags? */
	                                   &dbus_error);

	switch (result) {
		/* Service isn't running */
	case DBUS_SERVICE_REPLY_PRIMARY_OWNER:
	case DBUS_SERVICE_REPLY_ALREADY_OWNER: /* should not happen */
	case DBUS_SERVICE_REPLY_IN_QUEUE:      /* investigate!? */
		return FALSE;
		break;
		/* Service is running */
	case DBUS_SERVICE_REPLY_SERVICE_EXISTS:
		return TRUE;
		break;
	default:
		g_error ("Should never happen");
	}

	return FALSE;
}

/**
 * Delegate the init process to the running process
 * Basically, loads document specified on the command line
 */
static void
mlview_dbus_manager_delegate_init_process (DBusConnection *connection, MlViewApp *app, gchar **args)
{
	if (args) {
		int i;
		for (i = 0; args[i] != NULL; i++) {
			invoke_org_mlview_instance_opendocument (connection, (gchar*)args[i]);
		}
	}
}

/********
 * DBUS *
 ********/
/* DBus Message Handler
   Invoked on each message for the org.mlview.MlViewInstance Object */
static DBusHandlerResult
mlview_dbus_manager_instance_message_handler (DBusConnection *connection,
        DBusMessage *message,
        void *data)
{
	DBusHandlerResult result = DBUS_HANDLER_RESULT_NOT_YET_HANDLED;

	/* org.mlview.Instance.OpenDocument () */
	if (dbus_message_is_method_call (message,
	                                 MLVIEW_DBUS_INSTANCE_OBJECT_NAME,
	                                 "OpenDocument")) {
		result = process_org_mlview_instance_opendocument (connection, message, data);
	} else if (dbus_message_is_method_call (message,
	                                        MLVIEW_DBUS_INSTANCE_OBJECT_NAME,
	                                        "GetDocument")) {
		result = process_org_mlview_instance_getdocument (connection, message, data);
	}

	return result;

}
static void mlview_dbus_manager_instance_unregistered_cb (DBusConnection *connection, void *data)
{
	/* Nothing to do yet */
	g_warning ("[DBUS] Instance unregistered");
}

/* ClientSide
   DBus objects methods invocations */
static gboolean
invoke_org_mlview_instance_opendocument (DBusConnection *connection,
        const gchar *uri)
{
	DBusError dbus_error;
	DBusMessage *message = NULL;
	DBusMessage *reply = NULL;

	message = dbus_message_new_method_call (MLVIEW_DBUS_INSTANCE_SERVICE_NAME,
	                                        MLVIEW_DBUS_INSTANCE_OBJECT_PATH,
	                                        MLVIEW_DBUS_INSTANCE_OBJECT_NAME,
	                                        "OpenDocument");

	if (!dbus_message_append_args (message,
	                               DBUS_TYPE_STRING, uri,
	                               DBUS_TYPE_INVALID)) {
		g_warning ("[DBUS] Couldn't append uri to message");

		dbus_message_unref (message);

		return FALSE;
	}

	dbus_error_init (&dbus_error);

	reply = dbus_connection_send_with_reply_and_block (connection,
	        message,
	        -1,
	        &dbus_error);

	dbus_message_unref (message);

	if (reply == NULL) {
		g_warning ("[DBUS] Got NULL Reply: %s", dbus_error.message);
		return FALSE;
	}

	if (dbus_set_error_from_message (&dbus_error, message)) {
		g_warning ("[DBUS] Error on reply: %s", dbus_error.message);
		dbus_message_unref (reply);
		return FALSE;
	}

	/*TODO Use the ACK from the reply maybe ? */

	dbus_message_unref (reply);

	return TRUE;
}

/* ServerSide
   DBus objects methods implementations */
/*
Object: org.mlview.Instance
Method: OpenDocument
Params: string uri
Return: OK on success
*/
static DBusHandlerResult
process_org_mlview_instance_opendocument (DBusConnection *connection,
        DBusMessage *message,
        void *data)
{
	DBusError	error;
	DBusMessage	*reply		= NULL;
	char		*uri		= NULL;
	MlViewApp	*app		= (MlViewApp*)data;
	MlViewEditor	*mlview_editor	= mlview_app_get_editor (app);


	dbus_error_init (&error);

	if (!dbus_message_get_args (message,
	                            &error,
	                            DBUS_TYPE_STRING, &uri,
	                            DBUS_TYPE_INVALID)) {

		reply = dbus_message_new_error (message,
		                                error.name,
		                                error.message);

		if (reply == NULL)
			g_error ("OUT OF MEMORY");

		if (!dbus_connection_send (connection, reply, NULL))
			g_error ("OUT OF MEMORY");

		dbus_message_unref (reply);

		return DBUS_HANDLER_RESULT_NOT_YET_HANDLED;
	}

	reply = dbus_message_new_method_return (message);
	if (reply == NULL)
		g_error ("OUT OF MEMORY");

	if (!dbus_message_append_args (reply,
	                               DBUS_TYPE_STRING, "OK",
	                               DBUS_TYPE_INVALID))
		g_error ("OUT OF MEMORY");

	if (!dbus_connection_send (connection, reply, NULL))
		g_error ("OUT OF MEMORY");

	/* Everything went fine open the document */
	g_warning ("[DBUS] Opening URI '%s'", uri);
	if (mlview_editor) {
		mlview_editor_load_xml_file_with_dtd (mlview_editor,
		                                      (gchar*)uri,
		                                      NULL);
	}

	dbus_free (uri);
	dbus_message_unref (reply);

	return DBUS_HANDLER_RESULT_HANDLED;
}

/*
Object: org.mlview.Instance
Method: GetDocument
Params: void
Return: current document (uri;mime-type;content) 
*/
static DBusHandlerResult
process_org_mlview_instance_getdocument (DBusConnection *connection,
        DBusMessage *message,
        void *data)
{
	DBusError	        error;
	DBusMessage	        *reply	= NULL;
	MlViewApp	        *app	= (MlViewApp*)data;
	MlViewEditor	        *editor	= mlview_app_get_editor (app);
	MlViewXMLDocument       *doc    = mlview_editor_get_current_document (editor);
	MlViewFileDescriptor    *fd     = mlview_xml_document_get_file_descriptor (doc);
	char                    *uri    = NULL;
	char                    *mime   = NULL;
	char                    *buffer = NULL;

	dbus_error_init (&error);

	reply = dbus_message_new_method_return (message);
	if (reply == NULL)
		g_error ("OUT OF MEMORY");

	/* Get data for reply */
	uri    = (char*)mlview_file_descriptor_get_uri (fd);
	mime   = (char*)mlview_file_descriptor_get_mime_type (fd);
	buffer = (char*)"<xml/>";
	/*(char*)mlview_xml_document_get_xml_document_as_string (doc);*/
	if (!dbus_message_append_args (reply,
	                               DBUS_TYPE_STRING, uri,
	                               DBUS_TYPE_STRING, mime,
	                               DBUS_TYPE_STRING, buffer,
	                               DBUS_TYPE_INVALID))
		g_error ("OUT OF MEMORY");

	if (!dbus_connection_send (connection, reply, NULL))
		g_error ("OUT OF MEMORY");

	dbus_message_unref (reply);
	free (uri);
	free (mime);
	free (buffer);

	g_warning ("[DBUS] Sent document content.");

	return DBUS_HANDLER_RESULT_HANDLED;
}

/******************
 * PUBLIC METHODS *
 ******************/

void mlview_dbus_manager_init (MlViewApp *app, gchar **args)
{
	DBusConnection  *connection	= NULL;
	DBusError	error;

	DBusObjectPathVTable mlview_instance_handler_vtable = {
	            mlview_dbus_manager_instance_unregistered_cb,
	            mlview_dbus_manager_instance_message_handler,
	            NULL,
	        };


	dbus_error_init (&error);
	connection = dbus_bus_get (DBUS_BUS_SESSION,
	                           NULL);
	if (connection == NULL) {
		g_warning ("[DBUS] Couldn't get connection to session bus: %s", error.message);
		dbus_error_free (&error);
		return;
	}

	dbus_connection_setup_with_g_main (connection, NULL);

	/* check for instance */
	if (mlview_dbus_manager_is_mlview_instance_running (connection)) {
		mlview_dbus_manager_delegate_init_process (connection, app, args);
		exit (0);
	}

	/* register object path */
	if (!dbus_connection_register_object_path (connection,
	        MLVIEW_DBUS_INSTANCE_OBJECT_PATH,
	        &mlview_instance_handler_vtable,
	        (void*)app)) {
		g_error ("OUT OF MEMORY");
	}

	return;
}

