/* -*- Mode: C++; indent-tabs-mode: nil; c-basic-offset: 4-*- */

/*
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute 
 *it and/or modify it under the terms of 
 *the GNU General Public License as published by 
 *the Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *MlView is distributed in the hope that 
 *it will be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. If not, write to the 
 *Free Software Foundation, Inc., 
 *59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */

#include <string.h>
#include <libxml/hash.h>
#include <libxml/parser.h>
#include "mlview-attrs-editor.h"
#include "mlview-attribute-picker.h"
#include "mlview-marshal.h"
#include <gdk/gdkkeysyms.h>

/**
 *@file
 *The definition of the class #MlViewAttrsEditor.
 */
enum {
    ATTRIBUTE_CHANGED = 0,
    NUMBER_OF_SIGNALS
};

enum MlViewAttrsEditorColumns {
    HIDDEN_XML_ATTR_COLUMN = 0,
    IS_ADD_NEW_ATTR_COLUMN,
    IS_EDITABLE_COLUMN,
    ATTRIBUTE_NAMES_COLUMN,
    ATTRIBUTE_VALUES_COLUMN,
    NB_COLUMNS,
} ;

struct _MlViewAttrsEditorPrivate
{
	GtkTreeModel *model ;
	/**
	 *visual list of attributes
	 */
	GtkTreeView * attrs_view ;
	/**
	 *The title of the attributes names column
	 */
	gchar *names_title;
	/**
	 *The title of the attributes values column
	 */
	gchar *values_title;

	/**
	 *the row that is currently selected bu the user.
	 */
	GtkTreeRowReference *cur_selected_row ;
	GHashTable *attr_row_hash ;

	/**
	*says if the current instance is editable or not. 
	*ONLY use the setter to change it !!!
	*/
	gboolean editable;

	/**
	*The current xmlNode that holds the list 
	*of attributes being edited.
	*/
	xmlNode *current_xml_node;

	/**
	*The attribute picker used to 
	*populate the list.
	*/
	MlViewAttributePicker *attribute_picker;
	MlViewXMLDocument *mlview_xml_doc ;
	gboolean dispose_has_run ;
};


#define PRIVATE(list) ((list)->priv)

/*compulsory functions to comply with the Gtk type system framework*/
static void
mlview_attrs_editor_class_init (MlViewAttrsEditorClass * a_klass);

static void
mlview_attrs_editor_init (MlViewAttrsEditor * a_this);

static void
mlview_attrs_editor_finalize (GObject *a_this) ;

static void mlview_attrs_editor_dispose (GObject * a_this) ;

static GtkVBoxClass *gv_parent_class = NULL;

/*signals array*/
static guint gv_signals[NUMBER_OF_SIGNALS] = {0};

/*private helper functions*/
static enum MlViewStatus mlview_attrs_editor_add_attribute_to_node_interactive (MlViewAttrsEditor * a_list,
        xmlNode * a_node,
        xmlAttr **a_added_attribute);

/*signal callbacks*/

static void row_selected_cb (GtkTreeSelection *a_tree_sel,
                             MlViewAttrsEditor * a_this);

/*default signal handlers*/

static void mlview_attrs_editor_attribute_changed_default_handler (MlViewAttrsEditor * a_this,
        gpointer data);

static void free_attr_row_hash (MlViewAttrsEditor *a_this) ;


static void
attr_value_cell_edited_cb (GtkCellRendererText *a_renderer,
                           gchar *a_cell_path,
                           gchar *a_attr_value,
                           MlViewAttrsEditor *a_editor) ;

static void
attr_name_cell_edited_cb (GtkCellRendererText *a_renderer,
                          gchar *a_cell_path,
                          gchar *a_attr_name,
                          MlViewAttrsEditor *a_editor) ;
static void
xml_doc_node_attribute_added_cb (MlViewXMLDocument *a_this,
                                 xmlAttr *a_attr,
                                 MlViewAttrsEditor *a_editor) ;

static gboolean
tree_key_press_cb (GtkTreeView *a_tree,
                   GdkEventKey *a_event,
                   MlViewAttrsEditor *a_this);

/*
 *Mandatory private functions to comply with the gtk object
 *framework. go to http://www.gtk.org/tutorial/sec-theanatomyofawidget.html
 *to learn about what the object oriented widget framework looks like.
 */

/**
 *class initialyzer. 
 *@param a_klass the vtable.
 */
static void
mlview_attrs_editor_class_init (MlViewAttrsEditorClass *a_klass)
{
	GObjectClass *gobject_class;

	g_return_if_fail (a_klass != NULL);

	gv_parent_class = (GtkVBoxClass*) g_type_class_peek_parent (a_klass);
	g_return_if_fail (gv_parent_class) ;

	gobject_class = G_OBJECT_CLASS (a_klass) ;

	/*overload the destroy method of GtkObjectClass */
	gobject_class->dispose = mlview_attrs_editor_dispose ;
	gobject_class->finalize = mlview_attrs_editor_finalize ;

	/*signal definitions */

	gv_signals[ATTRIBUTE_CHANGED] =
	    g_signal_new ("attribute-changed",
	                  G_TYPE_FROM_CLASS (gobject_class),
	                  G_SIGNAL_RUN_FIRST,
	                  G_STRUCT_OFFSET
	                  (MlViewAttrsEditorClass,
	                   attribute_changed), NULL, NULL,
	                  mlview_marshal_VOID__VOID,
	                  G_TYPE_NONE, 0, NULL);

	a_klass->attribute_changed =
	    mlview_attrs_editor_attribute_changed_default_handler;
}

/**
 *The allocator of the instance MlViewAttrsEditor.
 *@param a_this the current instance of MlViewAttrsEditor to
 *initialyzes
 */
static void
mlview_attrs_editor_init (MlViewAttrsEditor * a_this)
{

	g_return_if_fail (a_this != NULL);

	PRIVATE (a_this) = (MlViewAttrsEditorPrivate*)
	                   g_try_malloc (sizeof (MlViewAttrsEditorPrivate));
	if (!PRIVATE (a_this)) {
		mlview_utils_trace_debug ("g_try_malloc failed") ;
		return ;
	}
	memset (PRIVATE (a_this), 0,
	        sizeof (MlViewAttrsEditorPrivate)) ;

}

/**
 *The real place where the attrs editor is graphically built.
 *@param a_this the current instance of #MlViewAttrsEditor.
 *@param a_names_title the title of the attributes name column
 *@param a_values_title the title of the attritbutes value column
 */
static void
mlview_attrs_editor_construct (MlViewAttrsEditor *a_this,
                               gchar *a_names_title,
                               gchar *a_values_title)
{
	GtkCellRendererText *cell_renderer = NULL;
	GtkWidget *scrolled_window = NULL;
	GtkTreeSelection *selection = NULL ;
	GtkTreeIter iter = {0} ;

	gtk_box_set_spacing (GTK_BOX (a_this), 0);

	PRIVATE (a_this)->model = GTK_TREE_MODEL
	                          (gtk_list_store_new
	                           (NB_COLUMNS, G_TYPE_POINTER, G_TYPE_BOOLEAN,
	                            G_TYPE_BOOLEAN, G_TYPE_STRING,  G_TYPE_STRING)) ;
	g_return_if_fail (PRIVATE (a_this)->model) ;
	/*
	 *add the "almost always empty" row that
	 *serves to new attribute addition.
	 */
	gtk_list_store_append (GTK_LIST_STORE
	                       (PRIVATE (a_this)->model),
	                       &iter) ;
	gtk_list_store_set (GTK_LIST_STORE (PRIVATE (a_this)->model),
	                    &iter,
	                    HIDDEN_XML_ATTR_COLUMN, NULL,
	                    IS_ADD_NEW_ATTR_COLUMN, TRUE,
	                    IS_EDITABLE_COLUMN, TRUE,
	                    ATTRIBUTE_NAMES_COLUMN, "",
	                    ATTRIBUTE_VALUES_COLUMN, "",
	                    -1) ;
	/*initialyze the attributes list view*/
	PRIVATE (a_this)->attrs_view =
	    GTK_TREE_VIEW (gtk_tree_view_new_with_model
	                   (PRIVATE (a_this)->model)) ;
	g_return_if_fail (PRIVATE (a_this)->attrs_view) ;

	g_signal_connect (G_OBJECT (PRIVATE (a_this)->attrs_view),
	                  "key_press_event",
	                  G_CALLBACK (tree_key_press_cb),
	                  a_this) ;

	selection = gtk_tree_view_get_selection
	            (PRIVATE (a_this)->attrs_view) ;
	g_return_if_fail (selection) ;
	g_signal_connect (G_OBJECT (selection), "changed",
	                  G_CALLBACK (row_selected_cb),
	                  a_this);
	scrolled_window = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (
	    GTK_SCROLLED_WINDOW (scrolled_window),
	    GTK_POLICY_AUTOMATIC,
	    GTK_POLICY_AUTOMATIC);

	gtk_container_add
	(GTK_CONTAINER (scrolled_window),
	 GTK_WIDGET (PRIVATE (a_this)->attrs_view));
	gtk_box_pack_start (GTK_BOX (a_this),
	                    scrolled_window, TRUE, TRUE, 0);
	cell_renderer = GTK_CELL_RENDERER_TEXT (gtk_cell_renderer_text_new ()) ;
	g_return_if_fail (cell_renderer) ;
	if (!a_names_title) {
		a_names_title = _("Attribute names") ;
	}
	gtk_tree_view_insert_column_with_attributes
	(PRIVATE (a_this)->attrs_view,
	 ATTRIBUTE_NAMES_COLUMN, a_names_title,
	 GTK_CELL_RENDERER (cell_renderer),
	 "text",  ATTRIBUTE_NAMES_COLUMN,
	 "editable", IS_EDITABLE_COLUMN,
	 NULL);
	g_signal_connect (G_OBJECT (cell_renderer),
	                  "edited",
	                  G_CALLBACK (attr_name_cell_edited_cb),
	                  a_this) ;
	cell_renderer = GTK_CELL_RENDERER_TEXT (gtk_cell_renderer_text_new ()) ;
	g_return_if_fail (cell_renderer) ;
	if (!a_values_title) {
		a_values_title = _("Attribute values") ;
	}
	gtk_tree_view_insert_column_with_attributes
	(PRIVATE (a_this)->attrs_view,
	 ATTRIBUTE_VALUES_COLUMN, a_values_title,
	 GTK_CELL_RENDERER (cell_renderer),
	 "text", ATTRIBUTE_VALUES_COLUMN,
	 "editable", IS_EDITABLE_COLUMN,
	 NULL) ;
	g_signal_connect (G_OBJECT (cell_renderer),
	                  "edited",
	                  G_CALLBACK (attr_value_cell_edited_cb),
	                  a_this) ;
}


/**
 *The gobject dispose method.
 *Frees the member objects "own" by #MlViewAttrsEditor.
 *@param a_object the instance of #MlViewAttrsEditor to destroy.
 */
static void
mlview_attrs_editor_dispose (GObject * a_this)
{
	MlViewAttrsEditor *attrs_editor = NULL;

	g_return_if_fail (a_this != NULL
	                  && MLVIEW_IS_ATTRS_EDITOR (a_this));
	attrs_editor = MLVIEW_ATTRS_EDITOR (a_this);
	g_return_if_fail (PRIVATE (attrs_editor)) ;
	if (PRIVATE (attrs_editor)->dispose_has_run == TRUE)
		return;

	/*destroy non ref'd allocated widgets and other objects */
	if (PRIVATE (attrs_editor)->names_title) {
		g_free (PRIVATE (attrs_editor)->names_title);
		PRIVATE (attrs_editor)->names_title = NULL;
	}

	if (PRIVATE (attrs_editor)->values_title) {
		g_free (PRIVATE (attrs_editor)->values_title);
		PRIVATE (attrs_editor)->values_title = NULL;
	}
	if (PRIVATE (attrs_editor)->attribute_picker) {
		gtk_widget_destroy
		(GTK_WIDGET
		 (PRIVATE (attrs_editor)->
		  attribute_picker));
		PRIVATE (attrs_editor)->attribute_picker = NULL;
	}
	if (PRIVATE (attrs_editor)->attr_row_hash) {
		free_attr_row_hash (attrs_editor) ;
	}
	/*call the destroy method of the object class */
	if (gv_parent_class) {
		G_OBJECT_CLASS (gv_parent_class)->dispose (a_this) ;
	}
}

static void
mlview_attrs_editor_finalize (GObject *a_this)
{
	MlViewAttrsEditor *editor = NULL ;

	g_return_if_fail (a_this && MLVIEW_IS_ATTRS_EDITOR (a_this)) ;
	editor = MLVIEW_ATTRS_EDITOR (a_this) ;
	g_return_if_fail (PRIVATE (editor)) ;
	g_return_if_fail (PRIVATE (editor)) ;
	PRIVATE (editor) = NULL ;
}

/*===================================
 *private helper functions
 *==================================*/

/**
 *Interactively adds an attribute to the list.
 *@param a_this the current instance of MlViewAttrsEditor.
 *@param a_node the xmlNode to add the attributes to.
 */
enum MlViewStatus
mlview_attrs_editor_add_attribute_to_node_interactive (MlViewAttrsEditor * a_this,
        xmlNode * a_node,
        xmlAttr **a_added_attribute)
{
	gint button = 0;
	gchar *name_str = NULL, *value_str = NULL, *node_path = NULL ;
	gboolean loop = TRUE;
	enum MlViewStatus result = MLVIEW_OK ;
	xmlAttr *attr = NULL ;

	g_return_val_if_fail (a_this, MLVIEW_BAD_PARAM_ERROR);
	g_return_val_if_fail (MLVIEW_IS_ATTRS_EDITOR (a_this),
	                      MLVIEW_BAD_PARAM_ERROR);

	if (a_node) {
		mlview_xml_document_get_node_path  (PRIVATE (a_this)->mlview_xml_doc,
		                                    a_node, &node_path) ;
		if (!node_path) {
			mlview_utils_trace_debug ("Could not get node xpath expr") ;
			return MLVIEW_ERROR ;
		}
	}
	if (PRIVATE (a_this)->attribute_picker == NULL) {
		PRIVATE (a_this)->attribute_picker =
		    MLVIEW_ATTRIBUTE_PICKER (mlview_attribute_picker_new
					((gchar*) _("Enter attribute name and value"))) ;
	}
	mlview_attribute_picker_grab_focus_to_name_entry
	(PRIVATE (a_this)->attribute_picker);
	gtk_window_set_modal (GTK_WINDOW
	                      (PRIVATE (a_this)->attribute_picker),
	                      TRUE);
	name_str = mlview_attribute_picker_get_attribute_name
	           (PRIVATE (a_this)->attribute_picker);
	if (!mlview_utils_is_white_string (name_str)) {
		mlview_attribute_picker_select_attribute_name
		(PRIVATE (a_this)->attribute_picker);
	}
	mlview_attribute_picker_set_current_xml_node
	(PRIVATE (a_this)->attribute_picker, a_node);
	mlview_attribute_picker_build_attribute_name_choice_list
	(PRIVATE (a_this)->attribute_picker, a_node);
	while (loop) {
		xmlAttributeType attr_type;
		button = gtk_dialog_run
		         (GTK_DIALOG
		          (PRIVATE (a_this)->attribute_picker));
		switch (button) {
		case GTK_RESPONSE_ACCEPT:
			name_str =
			    mlview_attribute_picker_get_attribute_name
			    (PRIVATE (a_this)->attribute_picker);
			value_str =
			    mlview_attribute_picker_get_attribute_value
			    (PRIVATE (a_this)->attribute_picker);
			attr_type =
			    mlview_attribute_picker_get_attribute_type
			    (PRIVATE (a_this)->attribute_picker);
			if (!mlview_utils_is_white_string (value_str)
			        && !mlview_utils_is_white_string
			        (name_str)) {
				result = mlview_xml_document_set_attribute
				         (PRIVATE (a_this)->mlview_xml_doc,
				          node_path, (xmlChar*)name_str,
				          (xmlChar*)value_str, TRUE) ;

				if (result != MLVIEW_OK)
					return MLVIEW_ERROR ;
				attr = xmlHasProp (a_node, (xmlChar*)name_str) ;
				if (!attr)
					return MLVIEW_ERROR ;

				/*
				 *FIXME: do this only when 
				 *validation is on 
				 */
				if (attr_type == XML_ATTRIBUTE_ID
				        && a_node->doc
				        && a_node->doc->ids) {
					xmlID *id = NULL;
					attr->atype =
					    XML_ATTRIBUTE_ID;
					id = (xmlID*)xmlMalloc (sizeof (xmlID));
					g_assert (id != NULL);
					id->value = (xmlChar*)g_strdup
					            (value_str);
					id->attr = attr;
					xmlHashAddEntry
					((xmlHashTable*)attr->doc->ids,
					 (const xmlChar*)
					 value_str, id);
				}
				loop = FALSE;
			}
			break;
		case GTK_RESPONSE_REJECT:
			loop = FALSE;
			break;
		case GTK_RESPONSE_CLOSE:
			loop = FALSE;
			break;
		default:
			break;
		}
	}
	gtk_widget_hide (GTK_WIDGET
	                 (PRIVATE (a_this)->attribute_picker));
	if (node_path) {
		g_free (node_path) ;
		node_path = NULL ;
	}
	if (attr) {
		*a_added_attribute = attr ;
	}
	return result;
}

/*signal handlers and callbacks*/


static void
mlview_attrs_editor_attribute_changed_default_handler (MlViewAttrsEditor * a_this,
        gpointer data)
{
}

static void
mlview_attrs_editor_set_current_selected_row (MlViewAttrsEditor *a_this,
        GtkTreeIter *a_iter)
{
	GtkTreeRowReference *row_ref = NULL ;

	g_return_if_fail (a_this && MLVIEW_ATTRS_EDITOR (a_this)
	                  && PRIVATE (a_this)) ;
	row_ref = mlview_attrs_editor_get_row_ref (a_this, a_iter) ;
	if (!row_ref) {
		row_ref = mlview_attrs_editor_get_new_row_ref
		          (a_this, a_iter) ;
	}
	g_return_if_fail (row_ref) ;
	PRIVATE (a_this)->cur_selected_row = row_ref ;
}


/**
 *Associates a row reference to a given instance of xmlAttr.
 *@param a_this the current instance of #MlViewAttrsEditor.
 *@param a_ref the row reference to consider.
 *@param a_xml_attr the instance of xmlAttr to consider.
 *@return MLVIEW_OK upon successful completion, 
 *an error code otherwise.
 */
static enum MlViewStatus
associate_row_ref_to_xml_attr (MlViewAttrsEditor *a_this,
                               GtkTreeRowReference *a_ref,
                               xmlAttr *a_xml_attr)
{
	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_ATTRS_EDITOR (a_this)
	                      && PRIVATE (a_this)
	                      && a_ref,
	                      MLVIEW_BAD_PARAM_ERROR) ;

	if (!PRIVATE (a_this)->attr_row_hash) {
		PRIVATE (a_this)->attr_row_hash =
		    g_hash_table_new
		    (g_direct_hash, g_direct_equal) ;
		if (!PRIVATE (a_this)->attr_row_hash) {
			mlview_utils_trace_debug ("g_hash_table_new() "
			                          "failed.") ;
			return MLVIEW_OUT_OF_MEMORY_ERROR ;
		}
	}
	g_hash_table_insert
	(PRIVATE (a_this)->attr_row_hash,
	 a_xml_attr, a_ref) ;
	return MLVIEW_OK ;
}

/**
 *Frees the association build by calling 
 *associate_row_ref_to_xml_attr().
 *That is, frees the GtkTreeRowReference associated
 *to the xml attribute and remove the matching entry
 *from the hash table that holds the association.
 *@param a_this the current instance of #MlViewAttrsEditor.
 *@param a_xml_attr the xml attribute that identifies the
 *association.
 */
static enum MlViewStatus
remove_xml_attr_row_ref_association (MlViewAttrsEditor *a_this,
                                     xmlAttr *a_xml_attr)
{
	GtkTreeRowReference *row_ref = NULL ;
	enum MlViewStatus status = MLVIEW_OK ;

	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_ATTRS_EDITOR (a_this)
	                      && PRIVATE (a_this),
	                      MLVIEW_BAD_PARAM_ERROR) ;
	if (!PRIVATE (a_this)->attr_row_hash) {
		return status ;
	}
	row_ref = (GtkTreeRowReference*)g_hash_table_lookup
	          (PRIVATE (a_this)->attr_row_hash, a_xml_attr) ;
	if (row_ref) {
		gtk_tree_row_reference_free (row_ref) ;
		row_ref = NULL ;
	}
	g_hash_table_remove (PRIVATE (a_this)->attr_row_hash,
	                     a_xml_attr) ;
	return status ;
}

static void
hash_table_free_row_ref (xmlAttr *a_xml_attr,
                         GtkTreeRowReference *a_row_ref,
                         MlViewAttrsEditor *a_editor)
{
	if (a_row_ref) {
		gtk_tree_row_reference_free (a_row_ref) ;
		a_row_ref = NULL ;
	}
}

/**
 *Private method of #MlViewAttrsEditor.
 *Frees the resources used by the "attr_row_hash" private field
 *of #MlViewAttrsEditor. It also frees each instance of GtkTreeRowReference
 *stored in it.
 *@param a_this the current instance of #MlViewAttrsEditor.
 */
static void
free_attr_row_hash (MlViewAttrsEditor *a_this)
{
	g_return_if_fail (a_this
	                  && MLVIEW_IS_ATTRS_EDITOR (a_this)
	                  && PRIVATE (a_this)) ;
	if (!PRIVATE (a_this)->attr_row_hash) {
		return ;
	}
	g_hash_table_foreach
	(PRIVATE (a_this)->attr_row_hash,
	 (GHFunc)hash_table_free_row_ref, a_this) ;

	g_hash_table_destroy (PRIVATE (a_this)->attr_row_hash) ;
	PRIVATE (a_this)->attr_row_hash = NULL ;
}

/**
 *callback of the "changed" selection signal emited
 *by instances of GtkTreeSelection.
 */
static void
row_selected_cb (GtkTreeSelection *a_tree_sel,
                 MlViewAttrsEditor * a_attrs_editor)
{
	GtkTreeIter iter = {0} ;
	gboolean is_ok = TRUE ;
	xmlNode *cur_xml_node = NULL ;

	g_return_if_fail (a_tree_sel
	                  && a_attrs_editor
	                  && MLVIEW_IS_ATTRS_EDITOR (a_attrs_editor)
	                  && PRIVATE (a_attrs_editor)
	                  && PRIVATE (a_attrs_editor)->model) ;

	is_ok = gtk_tree_selection_get_selected
	        (a_tree_sel,
	         &PRIVATE (a_attrs_editor)->model, &iter) ;
	if (is_ok != TRUE) {
		return ;
	}
	cur_xml_node = mlview_attrs_editor_get_cur_xml_node
	               (a_attrs_editor) ;
	g_return_if_fail (cur_xml_node) ;

	mlview_attrs_editor_set_current_selected_row
	(a_attrs_editor, &iter) ;
}


/*
   NOT USED ATM
 *
static enum MlViewStatus
get_cur_selected_iter (MlViewAttrsEditor *a_this,
                       GtkTreeIter *a_iter)
{
        GtkTreePath *tree_path = NULL ;
        GtkTreeModel *model = NULL ;
 
        g_return_val_if_fail (a_this 
                              && MLVIEW_IS_ATTRS_EDITOR (a_this)
                              && PRIVATE (a_this)
                              && PRIVATE (a_this)->cur_selected_row,
                              MLVIEW_BAD_PARAM_ERROR) ;
        tree_path = gtk_tree_row_reference_get_path
                (PRIVATE (a_this)->cur_selected_row) ;
        g_return_val_if_fail (tree_path, MLVIEW_ERROR) ;
        model = mlview_attrs_editor_get_model (a_this) ;
        if (!model) {
                mlview_utils_trace_debug ("model failed") ;
                goto cleanup ;
        }
        gtk_tree_model_get_iter (model, a_iter, tree_path) ;
 cleanup:
        if (tree_path) {
                gtk_tree_path_free (tree_path) ;
                tree_path = NULL ;
        }
        return MLVIEW_OK ;
}
*/

static void
xml_doc_node_attribute_name_changed_cb (MlViewXMLDocument *a_this,
                                        xmlAttr *a_attr,
                                        MlViewAttrsEditor *a_editor)
{
	g_return_if_fail (a_this
	                  && MLVIEW_IS_XML_DOCUMENT (a_this)
	                  && a_editor
	                  && MLVIEW_IS_ATTRS_EDITOR (a_editor)) ;

	mlview_attrs_editor_update_attribute (a_editor, a_attr) ;
}

static void
xml_doc_node_attribute_value_changed_cb (MlViewXMLDocument *a_this,
        xmlAttr *a_attr,
        MlViewAttrsEditor *a_editor)
{
	g_return_if_fail (a_this
	                  && MLVIEW_IS_XML_DOCUMENT (a_this)
	                  && a_editor
	                  && MLVIEW_IS_ATTRS_EDITOR (a_editor)) ;

	mlview_attrs_editor_update_attribute (a_editor, a_attr) ;
}

static void
xml_doc_node_attribute_added_cb (MlViewXMLDocument *a_this,
                                 xmlAttr *a_attr,
                                 MlViewAttrsEditor *a_editor)
{
	g_return_if_fail (a_this
	                  && MLVIEW_IS_XML_DOCUMENT (a_this)
	                  && a_attr
	                  && a_editor
	                  && MLVIEW_IS_ATTRS_EDITOR (a_editor)) ;

	mlview_attrs_editor_update_attribute
	(a_editor, a_attr) ;
}

static void
xml_doc_node_attribute_removed_cb (MlViewXMLDocument *a_this,
                                   xmlNode *a_node,
                                   xmlChar *a_name,
                                   MlViewAttrsEditor *a_editor)
{
	g_return_if_fail (a_this
	                  && MLVIEW_IS_XML_DOCUMENT (a_this)
	                  && a_editor
	                  && PRIVATE (a_editor)
	                  && MLVIEW_IS_ATTRS_EDITOR (a_editor)) ;

	mlview_attrs_editor_update_attribute_removed2
	(a_editor, a_node, a_name) ;
}

static void
attr_name_cell_edited_cb (GtkCellRendererText *a_renderer,
                          gchar *a_cell_path,
                          gchar *a_attr_name,
                          MlViewAttrsEditor *a_editor)
{
	enum MlViewStatus status = MLVIEW_OK ;
	GtkTreeIter iter = {0} ;
	gchar *attr_value = NULL, *attr_name = NULL ;
	GtkTreeModel *model = NULL ;
	xmlAttr *attr = NULL, *attr2 = NULL ;
	const gchar * err_msg = "This node already has an "
							"attribute with the same name !" ;
	gchar *node_path = NULL ;

	THROW_IF_FAIL (a_cell_path && a_attr_name && a_editor
				   && MLVIEW_IS_ATTRS_EDITOR (a_editor)
				   && PRIVATE (a_editor)->current_xml_node
				   && PRIVATE (a_editor)->mlview_xml_doc) ;

	mlview::AppContext *context = mlview::AppContext::get_instance () ;
	THROW_IF_FAIL (context) ;

	mlview_xml_document_get_node_path (PRIVATE (a_editor)->mlview_xml_doc,
	                                   PRIVATE (a_editor)->current_xml_node,
	                                   &node_path) ;
	if (!node_path) {
		mlview_utils_trace_debug ("Could not get XPATH expr from XML node") ;
		return ;
	}

	model = mlview_attrs_editor_get_model (a_editor) ;
	g_return_if_fail (model) ;
	/*get an iter on the currently selected row*/
	status = mlview_utils_tree_path_string_to_iter
	         (model, a_cell_path, &iter) ;
	g_return_if_fail (status == MLVIEW_OK) ;
	gtk_tree_model_get (model, &iter,
	                    ATTRIBUTE_VALUES_COLUMN, &attr_value,
	                    ATTRIBUTE_NAMES_COLUMN, &attr_name,
	                    -1) ;
	if (mlview_attrs_editor_is_row_the_add_new_attr_row
	        (a_editor, &iter) == TRUE) {
		/*
		 *the user wants to add a new attribute
		 *to the current xml node.
		 */
		if (a_attr_name
		        && strcmp (a_attr_name, "")) {
			if (xmlHasProp (PRIVATE (a_editor)->current_xml_node,
			                (xmlChar*)a_attr_name)) {
				/*
				 *this node already has an attibute with
				 *the same name.
				 */
				context->error (_(err_msg)) ;
				return ;
			}
			mlview_xml_document_set_attribute
			(PRIVATE (a_editor)->mlview_xml_doc,
			 node_path,
			 (xmlChar*)a_attr_name,
			 (xmlChar*)attr_value, TRUE) ;
		}
	} else {
		gtk_tree_model_get (model, &iter,
		                    HIDDEN_XML_ATTR_COLUMN, &attr,
		                    -1) ;
		g_return_if_fail (attr) ;
		attr2 = xmlHasProp (attr->parent, (xmlChar*)a_attr_name) ;
		if (attr2 != attr) {
			/*an attribute with the same name already exist !*/
			context->error (_(err_msg)) ;
			return ;
		}
		if (a_attr_name
		        && strcmp (a_attr_name, "")
		        && strcmp (a_attr_name, attr_name)) {
			mlview_xml_document_set_attribute_name
			(PRIVATE (a_editor)->mlview_xml_doc,
			 attr, (xmlChar*)a_attr_name, TRUE) ;
		} else if (a_attr_name
		           && !strcmp (a_attr_name, "")) {
			mlview_xml_document_remove_attribute
			(PRIVATE (a_editor)->mlview_xml_doc,
			 PRIVATE (a_editor)->current_xml_node,
			 attr->name, TRUE) ;
		}
	}
	if (node_path) {
		g_free (node_path) ;
		node_path = NULL ;
	}
}



static void
attr_value_cell_edited_cb (GtkCellRendererText *a_renderer,
                           gchar *a_cell_path,
                           gchar *a_attr_value,
                           MlViewAttrsEditor *a_editor)
{
	enum MlViewStatus status = MLVIEW_OK ;
	GtkTreeIter iter = {0} ;
	gchar *attr_value = NULL, *attr_name = NULL ;
	GtkTreeModel *model = NULL ;
	gchar *node_path = NULL ;

	g_return_if_fail (a_cell_path && a_attr_value
	                  && a_editor && MLVIEW_IS_ATTRS_EDITOR (a_editor)
	                  && PRIVATE (a_editor)->current_xml_node
	                  && PRIVATE (a_editor)->mlview_xml_doc) ;

	mlview_xml_document_get_node_path (PRIVATE (a_editor)->mlview_xml_doc,
	                                   PRIVATE (a_editor)->current_xml_node,
	                                   &node_path) ;
	if (!node_path) {
		mlview_utils_trace_debug ("Could not get XPATH expr from node") ;
		return ;
	}
	model = mlview_attrs_editor_get_model (a_editor) ;
	g_return_if_fail (model) ;
	/*get an iter on the currently selected row*/
	status = mlview_utils_tree_path_string_to_iter
	         (model, a_cell_path, &iter) ;
	g_return_if_fail (status == MLVIEW_OK) ;
	gtk_tree_model_get (model, &iter,
	                    ATTRIBUTE_VALUES_COLUMN, &attr_value,
	                    ATTRIBUTE_NAMES_COLUMN, &attr_name,
	                    -1) ;
	if (mlview_attrs_editor_is_row_the_add_new_attr_row
	        (a_editor, &iter) == FALSE) {
		if (a_attr_value
		        && strcmp (a_attr_value, attr_value)) {
			mlview_xml_document_set_attribute
			(PRIVATE (a_editor)->mlview_xml_doc,
			 node_path,
			 (xmlChar*)attr_name,
			 (xmlChar*)a_attr_value, TRUE) ;
		}
	}
	if (node_path) {
		g_free (node_path) ;
		node_path = NULL ;
	}
}


/**
 *callback for key_press_event in the attributes treeview
 *remove the selected attribute if DEL key is pressed
 */

static gboolean
tree_key_press_cb (GtkTreeView *a_tree,
                   GdkEventKey *a_event,
                   MlViewAttrsEditor *a_this)
{
	enum MlViewStatus status = MLVIEW_OK ;
	GtkTreeIter iter = {0} ;

	g_return_val_if_fail (a_tree && a_event && a_this, FALSE) ;

	status = mlview_attrs_editor_get_cur_sel_iter(a_this, &iter);
	if(status != MLVIEW_OK)
		return FALSE ;

	if (a_event->keyval == GDK_Delete) {
		mlview_attrs_editor_remove_attribute (a_this, &iter) ;
		return TRUE ;
	}

	return FALSE ;
}

/************************
 *public methods        *
 ************************/

/**
 *type builder. 
 *@return the id of the #MlViewAttrsEditor type.
 */
guint
mlview_attrs_editor_get_type (void)
{
	static guint type = 0;

	if (!type) {
		static const GTypeInfo type_info = {
		                                       sizeof (MlViewAttrsEditorClass),
		                                       NULL,   /* base_init */
		                                       NULL,   /* base_finalize */
		                                       (GClassInitFunc)
		                                       mlview_attrs_editor_class_init,
		                                       NULL,   /* class_finalize */
		                                       NULL,   /* class_data */
		                                       sizeof (MlViewAttrsEditor),
		                                       0,
		                                       (GInstanceInitFunc)
		                                       mlview_attrs_editor_init
		                                   };
		type = g_type_register_static (GTK_TYPE_VBOX,
		                               "MlViewAttrsEditor",
		                               &type_info, (GTypeFlags)0);
	}
	return type;
}


/**
 *Creates a new #MlViewAttrsEditor.
 *
 *@param a_names_titles the title of the names column
 *@param a_value_title the title of the values column
 *@return the newly created instance of #MlViewAttrsEditor.
 */
GtkWidget *
mlview_attrs_editor_new (gchar * a_names_title,
                         gchar * a_values_title)
{
	MlViewAttrsEditor *editor = NULL ;


	editor = (MlViewAttrsEditor*)gtk_type_new (MLVIEW_TYPE_ATTRS_EDITOR);
	mlview_attrs_editor_construct (editor, a_names_title, a_values_title) ;
	return GTK_WIDGET (editor);
}

/**
 *Returns the tree model stored in the current widget.
 *@param a_this the current instance of #MlViewAttrsEditor
 *@return the model, or NULL if something bad happened.
 */
GtkTreeModel *
mlview_attrs_editor_get_model (MlViewAttrsEditor *a_this)
{
	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_ATTRS_EDITOR (a_this)
	                      && PRIVATE (a_this),
	                      NULL) ;

	return PRIVATE (a_this)->model ;
}

/**
 *Gets the instance of GtkTreeView used by the current instance of 
 *#MlViewAttrsEditor to render the xml attributes.
 *@param a_this the current instance of #MlViewAttrsEditor.
 *@return the GtkTreeView to return or NULL . The caller _MUST NOT_
 *free or ref the returned GtkTreeView.
 */
GtkTreeView *
mlview_attrs_editor_get_tree_view (MlViewAttrsEditor *a_this)
{
	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_ATTRS_EDITOR (a_this)
	                      && PRIVATE (a_this),
	                      NULL) ;
	return PRIVATE (a_this)->attrs_view ;
}

/**
 *Builds and returns the GtkTreeRowRefernce associated to
 *the a given iterator. This function also associates
 *the new row ref to the xml attribute present at that row.
 *this way, subsequent invocations of 
 *mlview_attrs_editor_get_row_ref ()
 *on the same iterator will return the same instance of 
 *GtkTreeRowReference.
 *NOTE: if the row denoted by a_iter already has a 
 *GtkTreeRowReference associated to it, this function returns
 *that same GtkTreeRowReference, which means it does not
 *allocated a new one.
 *@param a_this the current instance of #MlViewAttrsEditor to 
 *consider.
 *@param a_iter the iterator to consider.
 *@return the newly built instance of GtkTreeRowReference or NULL
 *if something bad happened. The returned pointer MUST be freed
 *by calling gtk_tree_row_reference_free().
 */
GtkTreeRowReference *
mlview_attrs_editor_get_new_row_ref (MlViewAttrsEditor *a_this,
                                     GtkTreeIter *a_iter)
{
	GtkTreeModel *model = NULL ;
	GtkTreePath *tree_path = NULL ;
	GtkTreeRowReference *ref = NULL, *result = NULL ;
	xmlAttr *xml_attr = NULL ;
	enum MlViewStatus status = MLVIEW_OK ;

	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_ATTRS_EDITOR (a_this)
	                      && a_iter, NULL) ;

	model = mlview_attrs_editor_get_model (a_this) ;
	g_return_val_if_fail (model, NULL) ;
	/*
	 *first, check if this row doesn't have 
	 *a row reference created already.
	 */
	ref = mlview_attrs_editor_get_row_ref (a_this, a_iter) ;
	if (ref) {
		result = ref;
		ref = NULL ;
		goto cleanup ;
	}
	tree_path = gtk_tree_model_get_path (model, a_iter) ;
	g_return_val_if_fail (tree_path, NULL) ;
	ref = gtk_tree_row_reference_new (model, tree_path) ;
	if (!ref) {
		mlview_utils_trace_debug ("result failed") ;
		goto cleanup ;
	}
	xml_attr = mlview_attrs_editor_get_xml_attr (a_this, a_iter) ;
	if (!xml_attr) {
		gboolean is_add_new_attr_row = FALSE ;
		gtk_tree_model_get (model, a_iter,
		                    IS_ADD_NEW_ATTR_COLUMN,
		                    &is_add_new_attr_row,
		                    -1) ;
		if (is_add_new_attr_row == FALSE) {
			mlview_utils_trace_debug ("xml_attr failed") ;
			goto cleanup ;
		}
	}
	status = associate_row_ref_to_xml_attr (a_this, ref, xml_attr) ;
	if (status != MLVIEW_OK) {
		mlview_utils_trace_debug ("status == MLVIEW_OK failed") ;
		goto cleanup ;
	}
	result = ref ;
	ref = NULL ;
cleanup:
	if (ref) {
		gtk_tree_row_reference_free (ref) ;
		ref = NULL ;
	}
	if (tree_path) {
		gtk_tree_path_free (tree_path) ;
		tree_path = NULL ;
	}
	return result ;
}

/**
 *Gets the instance of #GtkTreeRowReference associated to
 *a given instance of xmlAttr.
 *@param a_this the current instance of #MlViewAttrsEditor
 *@param a_xml_attr the xml attribute to consider.
 *@param a_result out parameter the returned row reference, or NULL if
 *not found.
 *@param MLVIEW_OK upon successfull completion, 
 *an error code otherwise.
 */
enum MlViewStatus
mlview_attrs_editor_get_row_ref_from_xml_attr (MlViewAttrsEditor *a_this,
        xmlAttr *a_xml_attr,
        GtkTreeRowReference **a_result)
{
	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_ATTRS_EDITOR (a_this)
	                      && PRIVATE (a_this)
	                      && a_xml_attr
	                      && a_result,
	                      MLVIEW_BAD_PARAM_ERROR) ;

	if (!PRIVATE (a_this)->attr_row_hash) {
		*a_result = NULL ;
	} else {
		*a_result = (GtkTreeRowReference*)g_hash_table_lookup
		            (PRIVATE (a_this)->attr_row_hash,
		             a_xml_attr) ;
	}
	return MLVIEW_OK ;
}


/**
 *Gets the row reference associated to a given
 *iterator. It actually gets the row reference
 *associated to the xmlAttr * found a the position
 *denoted by a_iter.
 *@param a_this the current instance of #MlViewAttrsEditor.
 *@param a_iter the iterator to consider.
 *@return the row reference associated to the row.
 */
GtkTreeRowReference *
mlview_attrs_editor_get_row_ref (MlViewAttrsEditor *a_this,
                                 GtkTreeIter *a_iter)
{
	GtkTreeRowReference *result = NULL ;
	xmlAttr *xml_attr = NULL ;
	enum MlViewStatus status = MLVIEW_OK ;

	g_return_val_if_fail (a_this
	                      && MLVIEW_ATTRS_EDITOR (a_this)
	                      && a_iter, NULL) ;

	xml_attr = mlview_attrs_editor_get_xml_attr (a_this, a_iter) ;
	if (!xml_attr)
		return NULL ;
	status =  mlview_attrs_editor_get_row_ref_from_xml_attr
	          (a_this, xml_attr, &result) ;
	g_return_val_if_fail (status == MLVIEW_OK, NULL) ;
	return result ;
}

/**
 *Gets the xml attribute stored at the position denoted by
 *a given tree iterator.
 *@param a_this the current instance of #MlViewAttrsEditor.
 *@param a_iter the iterator to consider.
 *@return the xml attribute or NULL if something bad happened.
 */
xmlAttr *
mlview_attrs_editor_get_xml_attr (MlViewAttrsEditor *a_this,
                                  GtkTreeIter *a_iter)
{
	xmlAttr * result = NULL ;
	GtkTreeModel *model = NULL ;

	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_ATTRS_EDITOR (a_this)
	                      && PRIVATE (a_this)
	                      && a_iter,
	                      NULL) ;
	model = mlview_attrs_editor_get_model (a_this) ;
	g_return_val_if_fail (model, NULL) ;
	gtk_tree_model_get (model, a_iter,
	                    HIDDEN_XML_ATTR_COLUMN,
	                    &result, -1) ;
	return result ;
}

/**
 *Gets an iterator on the currently selected row of the attributes
 *list.
 *@param a_this the current instance of #MlViewAttrsEditor.
 *@param GtkTreeIter *a_iter the place where to store the iterator.
 *must be allocated by the caller.
 *@param MLVIEW_OK upon successfull completion, 
 *an error code otherwise.
 */
enum MlViewStatus
mlview_attrs_editor_get_cur_sel_iter (MlViewAttrsEditor *a_this,
                                      GtkTreeIter *a_iter)
{
	GtkTreeModel *model = NULL ;
	enum MlViewStatus status = MLVIEW_OK ;
	gboolean is_ok = TRUE ;
	GtkTreePath *tree_path = NULL ;

	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_ATTRS_EDITOR (a_this)
	                      && PRIVATE (a_this),
	                      MLVIEW_BAD_PARAM_ERROR) ;

	if (!PRIVATE (a_this)->cur_selected_row) {
		return MLVIEW_NO_ROW_SELECTED_ERROR ;
	}
	model = mlview_attrs_editor_get_model (a_this) ;
	g_return_val_if_fail (model && GTK_IS_LIST_STORE (model),
	                      MLVIEW_ERROR) ;
	tree_path = gtk_tree_row_reference_get_path
	            (PRIVATE (a_this)->cur_selected_row) ;
	g_return_val_if_fail (tree_path, MLVIEW_ERROR) ;
	is_ok = gtk_tree_model_get_iter (model, a_iter, tree_path) ;
	if (is_ok != TRUE) {
		mlview_utils_trace_debug ("is_ok == TRUE failed") ;
		status = MLVIEW_ERROR ;
		goto cleanup ;
	}
cleanup:
	if (tree_path) {
		gtk_tree_path_free (tree_path) ;
		tree_path = NULL ;
	}
	return status ;
}


/**
 *Gets the xml node which attributes are being currently edited.
 *@param a_this the current instance of #MlViewAttrsEditor.
 *@return the xml node which attributes are currently being edited,
 *or NULL if something bad happened.
 */
xmlNode *
mlview_attrs_editor_get_cur_xml_node (MlViewAttrsEditor *a_this)
{
	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_ATTRS_EDITOR (a_this)
	                      && PRIVATE (a_this),
	                      NULL) ;
	return PRIVATE (a_this)->current_xml_node ;
}


/**
 *Sets the titles of the names and/or values of the attributes list. 
 *
 *@param a_this the current instance of #MlViewAttrsEditor
 *@param a_names_title the new names 
 *column title. If NULL or equal to "", 
 *the names column title is not changed.
 *@param a_values_titles the new values title. 
 *If NULL or equal to "", the values column title is not changed.
 */
void
mlview_attrs_editor_set_titles (MlViewAttrsEditor *a_this,
                                gchar * a_names_title,
                                gchar * a_values_title)
{

	g_return_if_fail (a_this != NULL && MLVIEW_IS_ATTRS_EDITOR (a_this)
	                  && a_names_title && a_values_title) ;

	if (strcmp (a_names_title, "")) {
		if (PRIVATE (a_this)->names_title)
			g_free (PRIVATE (a_this)->names_title);
		PRIVATE (a_this)->names_title =
		    g_strdup (a_names_title);
	}
	if (strcmp (a_values_title, "")) {
		if (PRIVATE (a_this)->values_title)
			g_free (PRIVATE (a_this)->values_title);
		PRIVATE (a_this)->values_title =
		    g_strdup (a_values_title);
	}
}

/**
 *Insert a new xml attribute.
 *
 *@param a_iter an unset tree iterator that will be set
 *to the row of where the new attribute has been inserted.
 *@param a_offset the offset of the row where to insert the xml attribute.
 *0 is the topmost row. If offset < 0, the attribute is appended to
 *the attribute list.
 *@param a_this  the current instance of #MlViewAttrsEditor.
 *@param a_xml_attr the new attribute to append to insert to the
 *editor.
 *@return MLVIEW_OK upon successful completion, an error code
 *otherwise.
 */
enum MlViewStatus
mlview_attrs_editor_insert_attribute (MlViewAttrsEditor *a_this,
                                      GtkTreeIter *a_iter,
                                      gint a_offset,
                                      const xmlAttrPtr a_xml_attr)
{
	GtkTreeModel *model = NULL ;
	xmlNode *node = NULL ;
	xmlChar *value = NULL ;
	GtkTreeIter iter = {0} ;
	GtkTreeRowReference *row_ref = NULL ;
	enum MlViewStatus status = MLVIEW_OK ;
	gboolean is_ok = FALSE, is_add_new_attr_row = FALSE ;

	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_ATTRS_EDITOR (a_this)
	                      && a_xml_attr
	                      && a_xml_attr->parent
	                      && PRIVATE (a_this)->attrs_view,
	                      MLVIEW_BAD_PARAM_ERROR) ;

	model = mlview_attrs_editor_get_model (a_this) ;
	g_return_val_if_fail (model && GTK_IS_LIST_STORE (model),
	                      MLVIEW_ERROR) ;
	node = mlview_attrs_editor_get_cur_xml_node (a_this) ;
	g_return_val_if_fail (node, MLVIEW_ERROR) ;
	is_ok = gtk_tree_model_get_iter_first (model, &iter) ;
	g_return_val_if_fail (is_ok == TRUE, MLVIEW_ERROR) ;
	value = xmlGetProp (node, a_xml_attr->name) ;
	if (a_offset < 0) {
		while (is_ok == TRUE) {
			is_add_new_attr_row =
			    mlview_attrs_editor_is_row_the_add_new_attr_row
			    (a_this, &iter) ;
			if (is_add_new_attr_row == TRUE)
				break ;
			gtk_tree_model_iter_next (model, &iter) ;
		}
		gtk_list_store_insert_before (GTK_LIST_STORE (model),
		                              a_iter, &iter) ;
	} else {
		gtk_list_store_insert (GTK_LIST_STORE (model),
		                       a_iter, a_offset) ;
	}
	gtk_list_store_set (GTK_LIST_STORE (model),
	                    a_iter,
	                    HIDDEN_XML_ATTR_COLUMN, a_xml_attr,
	                    ATTRIBUTE_NAMES_COLUMN, a_xml_attr->name,
	                    ATTRIBUTE_VALUES_COLUMN, value,
	                    IS_EDITABLE_COLUMN, TRUE,
	                    -1) ;
	/*
	 *create an association between
	 *the attribute and the its row
	 *reference.
	 */
	row_ref = mlview_attrs_editor_get_new_row_ref
	          (a_this, a_iter) ;
	if (!row_ref) {
		mlview_utils_trace_debug
		("mlview_attrs_editor_get_new_row_ref () failed") ;
		status = MLVIEW_ERROR ;
		goto cleanup ;
	}
cleanup:

	if (value) {
		xmlFree (value) ;
		value = NULL ;
	}
	return status ;
}

/**
 *Edits the xml attributes hold by a given xml node.
 *@param a_this the current instance of #MlViewAttrsEditor
 *@param a_mlview_xml the current instance of 
 *#MlViewXMLDocument that
 *a_xml_node belongs to.
 *@param a_xml_node the xml node that holds the 
 *attributes list to edit.
 */
enum MlViewStatus
mlview_attrs_editor_edit_xml_attributes (MlViewAttrsEditor* a_this,
        MlViewXMLDocument *a_mlview_xml_doc,
        const xmlNodePtr a_xml_node)
{
	xmlAttrPtr xml_attr = NULL;
	GtkTreeIter iter = {0} ;
	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_ATTRS_EDITOR (a_this)
	                      && PRIVATE (a_this)
	                      && a_mlview_xml_doc
	                      && MLVIEW_IS_XML_DOCUMENT
	                      (a_mlview_xml_doc)
	                      && a_xml_node,
	                      MLVIEW_BAD_PARAM_ERROR) ;

	/*
	 *even if there are no attributes, do not
	 *forget to store the pointer to the xml_node so that
	 *it will be possible to add new attributes to that xml_node.
	 */
	PRIVATE (a_this)->current_xml_node =
	    a_xml_node;
	PRIVATE (a_this)->mlview_xml_doc = a_mlview_xml_doc ;
	if (a_xml_node->properties == NULL)
		return MLVIEW_OK ;
	for (xml_attr = a_xml_node->properties;
	        xml_attr != NULL; xml_attr = xml_attr->next) {
		if (xml_attr->name) {
			/*append each attribute to the editor.*/
			mlview_attrs_editor_insert_attribute
			(a_this, &iter, -1, xml_attr);
		}
	}
	gtk_widget_show_all (GTK_WIDGET (a_this));

	return MLVIEW_OK ;
}

/**
 *Gets the attribute that is at a given offset 
 *in the current instance of #MlViewAttrsEditor.
 *
 *@param a_this the current instance of 
 *#MlViewAttrsEditor.
 *@param a_iter the iterator to the row to consider.
 *@param a_xml_attr_ptr out parameter. A pointer 
 *to the xmlAttrPtr (the libxml2 data structure that holds an attribute).
 *after return, *a_xlm_attr_ptr points to the xmlAttr store in #MlViewAttrEditor.
 *Caller must take great care of this pointer, and must not free it.
 */
enum MlViewStatus
mlview_attrs_editor_get_attribute (MlViewAttrsEditor * a_this,
                                   GtkTreeIter *a_iter,
                                   xmlAttr **a_xml_attr_ptr)
{
	GtkTreeModel *model = NULL ;

	g_return_val_if_fail (a_this != NULL
	                      && MLVIEW_IS_ATTRS_EDITOR (a_this)
	                      && a_iter
	                      && a_xml_attr_ptr,
	                      MLVIEW_BAD_PARAM_ERROR);

	model = mlview_attrs_editor_get_model (a_this) ;
	g_return_val_if_fail (model, MLVIEW_ERROR) ;

	gtk_tree_model_get (model, a_iter,
	                    HIDDEN_XML_ATTR_COLUMN,
	                    a_xml_attr_ptr, -1) ;
	return MLVIEW_OK ;
}

/**
 *Gets the attribute that is at a given offset 
 *in the current instance of #MlViewAttrsEditor.
 *
 *@param a_this the current instance of 
 *#MlViewAttrsEditor.
 *@param a_row_ref the row reference of the attr to get.
 *@param a_xml_attr_ptr out parameter. A pointer 
 *to the xmlAttrPtr (the libxml2 data structure that holds an attribute).
 *after return, *a_xlm_attr_ptr points to the xmlAttr store in #MlViewAttrEditor.
 *Caller must take great care of this pointer, and must not free it.
 */
enum MlViewStatus
mlview_attrs_editor_get_attribute2 (MlViewAttrsEditor * a_this,
                                    GtkTreeRowReference *a_row_ref,
                                    xmlAttr **a_xml_attr_ptr)
{
	GtkTreePath *tree_path = NULL ;
	GtkTreeIter iter = {0} ;
	GtkTreeModel *model = NULL ;
	enum MlViewStatus status = MLVIEW_OK ;

	g_return_val_if_fail (a_this, MLVIEW_BAD_PARAM_ERROR) ;

	model = mlview_attrs_editor_get_model (a_this) ;
	g_return_val_if_fail (model, MLVIEW_BAD_PARAM_ERROR) ;
	tree_path = gtk_tree_row_reference_get_path
	            (a_row_ref) ;
	g_return_val_if_fail (tree_path, MLVIEW_ERROR) ;
	gtk_tree_model_get_iter (model, &iter, tree_path) ;
	if (tree_path) {
		gtk_tree_path_free (tree_path) ;
		tree_path = NULL ;
	}
	status = mlview_attrs_editor_get_attribute
	         (a_this, &iter, a_xml_attr_ptr) ;
	return status ;
}

/**
 *Checks if the row denoted by a given instance of GtkTreeIter *
 *is the one that serves to new attribute addition or not.
 *@param a_this the current instance of #MlViewEditor.
 *@param a_iter the iterator to consider.
 *@return TRUE if the row denoted by a_iter is the row that serves
 *to new attributes addition.
 */
gboolean
mlview_attrs_editor_is_row_the_add_new_attr_row (MlViewAttrsEditor *a_this,
        GtkTreeIter *a_iter)
{
	GtkTreeModel *model = NULL ;
	gboolean result = FALSE ;

	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_ATTRS_EDITOR (a_this)
	                      && PRIVATE (a_this),
	                      FALSE) ;

	model = mlview_attrs_editor_get_model (a_this) ;
	g_return_val_if_fail (model, FALSE) ;
	gtk_tree_model_get (model, a_iter,
	                    IS_ADD_NEW_ATTR_COLUMN, &result,
	                    -1) ;
	return result ;
}


/**
 *clears all the attributes edited by the current 
 *instance of #MlViewAttrsEditor.
 *
 *@param a_this the current instance of MlViewAttrsEditor.
 */
enum MlViewStatus
mlview_attrs_editor_clear (MlViewAttrsEditor *a_this)
{
	GtkTreeModel *model = NULL ;
	GtkTreeIter iter = {0} ;
	xmlAttr *attr = NULL ;
	gboolean is_ok = TRUE ;

	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_ATTRS_EDITOR (a_this)
	                      && PRIVATE (a_this)
	                      && PRIVATE (a_this)->attrs_view,
	                      MLVIEW_BAD_PARAM_ERROR ) ;

	model = mlview_attrs_editor_get_model (a_this) ;
	g_return_val_if_fail (model && GTK_IS_LIST_STORE (model),
	                      MLVIEW_ERROR) ;
	if (gtk_tree_model_get_iter_first (model, &iter) == FALSE) {
		/*the tree is empty*/
		return MLVIEW_OK ;
	}
	g_return_val_if_fail (PRIVATE (a_this)->attrs_view,
	                      MLVIEW_BAD_PARAM_ERROR) ;

	while (1) {
		is_ok = gtk_tree_model_get_iter_first
		        (PRIVATE (a_this)->model, &iter) ;
		g_return_val_if_fail (is_ok == TRUE,
		                      MLVIEW_ERROR) ;
		if (mlview_attrs_editor_is_row_the_add_new_attr_row
		        (a_this, &iter) == TRUE) {
			break ;
		}
		gtk_tree_model_get (model, &iter,
		                    HIDDEN_XML_ATTR_COLUMN, &attr,
		                    -1) ;
		if (attr) {
			g_return_val_if_fail
			        (PRIVATE (a_this)->attr_row_hash,
			         MLVIEW_ERROR) ;
			g_hash_table_remove
			(PRIVATE (a_this)->attr_row_hash,
			 attr) ;
		}
		is_ok = gtk_list_store_remove
		        (GTK_LIST_STORE (model),
		         &iter) ;
		g_return_val_if_fail (is_ok == TRUE, MLVIEW_ERROR) ;
	}
	PRIVATE (a_this)->cur_selected_row = NULL ;

	return MLVIEW_OK ;
}

/**
 *Interactively creates a new attribute and inserts it at the end of the attribute list.
 *The name and value of the attribute are the one entered by the user.
 *
 *@param a_this the current instance of #MlViewAttrsEditor
 */
enum MlViewStatus
mlview_attrs_editor_create_attribute (MlViewAttrsEditor *a_this)
{
	enum MlViewStatus result = MLVIEW_OK ;
	GtkTreeIter iter = {0} ;
	xmlAttr *attr = NULL ;

	g_return_val_if_fail (a_this != NULL
	                      && PRIVATE (a_this)->current_xml_node,
	                      MLVIEW_BAD_PARAM_ERROR) ;
	result =
	    mlview_attrs_editor_add_attribute_to_node_interactive
	    (a_this, PRIVATE (a_this)->current_xml_node,
	     &attr);

	if (attr) {
		result = mlview_attrs_editor_insert_attribute
		         (a_this, &iter, -1, attr) ;

		return result;
	}
	return MLVIEW_ERROR ;
}


/**
 *Removes the attribute located at 
 *offset a_attr_offset in the attribute list.
 *If the removal succeeds, emits a 
 *"attribute-changed" signal on #MlViewAttrsEditor. 
 *
 *@param a_this the current instance of #MlViewAttrsEditor.
 *@param a_attr_offset the offset to consider.
 */
enum MlViewStatus
mlview_attrs_editor_remove_attribute (MlViewAttrsEditor *a_this,
                                      GtkTreeIter *a_iter)
{
	enum MlViewStatus status = MLVIEW_OK ;
	xmlAttrPtr xml_attr = NULL;
	GtkTreeModel *model = NULL ;

	g_return_val_if_fail (a_this
	                      && PRIVATE (a_this)->current_xml_node
	                      && PRIVATE (a_this)->attrs_view
	                      && PRIVATE (a_this)->mlview_xml_doc,
	                      MLVIEW_BAD_PARAM_ERROR);

	status = mlview_attrs_editor_get_attribute
	         (a_this, a_iter, &xml_attr) ;
	g_return_val_if_fail (status == MLVIEW_OK && xml_attr,
	                      MLVIEW_ERROR) ;
	model = mlview_attrs_editor_get_model (a_this) ;
	g_return_val_if_fail (model && GTK_IS_LIST_STORE (model),
	                      MLVIEW_ERROR) ;
	status = mlview_xml_document_remove_attribute
	         (PRIVATE (a_this)->mlview_xml_doc,
	          xml_attr->parent, xml_attr->name, TRUE) ;
	return status ;
}

/**
 *Uptades the row that shows a given attribute.
 *@param a_this the current instance of #MlViewAttrsEditor
 *@param a_attr the attribute to reflect.
 *@return MLVIEW_OK upon successful completion.
 */
enum MlViewStatus
mlview_attrs_editor_update_attribute (MlViewAttrsEditor *a_this,
                                      xmlAttr *a_attr)
{
	enum MlViewStatus status = MLVIEW_OK ;
	GtkTreeRowReference *row_ref = NULL ;
	GtkTreeIter iter = {0} ;
	GtkTreePath *tree_path = NULL ;
	GtkTreeModel *model = NULL ;
	xmlChar *attr_val = NULL ;

	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_ATTRS_EDITOR (a_this)
	                      && a_attr
	                      && a_attr->parent,
	                      MLVIEW_BAD_PARAM_ERROR) ;

	status = mlview_attrs_editor_get_row_ref_from_xml_attr
	         (a_this, a_attr, &row_ref) ;

	g_return_val_if_fail (status == MLVIEW_OK, status) ;


	if (!row_ref) {
		status = mlview_attrs_editor_insert_attribute
		         (a_this, &iter, -1 , a_attr) ;
		g_return_val_if_fail (status == MLVIEW_OK, status) ;
	} else {
		tree_path = gtk_tree_row_reference_get_path
		            (row_ref) ;
		if (!tree_path) {
			status = MLVIEW_ERROR ;
			mlview_utils_trace_debug ("tree_path failed") ;
			goto cleanup ;
		}
		model = mlview_attrs_editor_get_model (a_this) ;
		if (!model) {
			status = MLVIEW_ERROR ;
			mlview_utils_trace_debug
			("mlview_attrs_editor_get_model () failed") ;
			goto cleanup ;
		}
		gtk_tree_model_get_iter (model, &iter, tree_path) ;
		attr_val = xmlGetProp (a_attr->parent, a_attr->name) ;
		gtk_list_store_set (GTK_LIST_STORE (model),
		                    &iter,
		                    ATTRIBUTE_NAMES_COLUMN,
		                    a_attr->name,
		                    ATTRIBUTE_VALUES_COLUMN,
		                    attr_val,
		                    -1) ;
	}
	g_signal_emit (G_OBJECT (a_this),
	               gv_signals [ATTRIBUTE_CHANGED], 0);
cleanup:
	if (tree_path) {
		gtk_tree_path_free (tree_path) ;
		tree_path = NULL ;
	}
	if (attr_val) {
		xmlFree (attr_val) ;
		attr_val = NULL ;
	}

	return status ;
}

/**
 *Removes an attribute row from the editor.
 *@param a_this the current instance of #MlViewAttrsEditor.
 *@param a_attr the attribute to remove.
 *@return MLVIEW_OK upon successful completion, an error code
 *otherwise.
 */
enum MlViewStatus
mlview_attrs_editor_update_attribute_removed (MlViewAttrsEditor *a_this,
        xmlAttr *a_attr)
{
	enum MlViewStatus status = MLVIEW_OK ;
	GtkTreeRowReference *row_ref = NULL ;
	GtkTreeIter iter = {0} ;
	GtkTreePath *tree_path = NULL ;
	GtkTreeModel *model = NULL ;
	xmlChar *attr_val = NULL ;
	gboolean is_ok = TRUE ;

	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_ATTRS_EDITOR (a_this)
	                      && a_attr
	                      && a_attr->parent,
	                      MLVIEW_BAD_PARAM_ERROR) ;

	status = mlview_attrs_editor_get_row_ref_from_xml_attr
	         (a_this, a_attr, &row_ref) ;
	g_return_val_if_fail (status == MLVIEW_OK, status) ;
	if (!row_ref) {
		status = mlview_attrs_editor_insert_attribute
		         (a_this, &iter, -1, a_attr) ;
		g_return_val_if_fail (status == MLVIEW_OK, status) ;
	} else {
		tree_path = gtk_tree_row_reference_get_path
		            (row_ref) ;
		model = mlview_attrs_editor_get_model (a_this) ;
		if (!model) {
			mlview_utils_trace_debug
			("mlview_attrs_editor_get_model () failed") ;
			goto cleanup ;
		}
		is_ok = gtk_tree_model_get_iter
		        (model, &iter, tree_path) ;
		if (is_ok == FALSE) {
			mlview_utils_trace_debug ("gtk_tree_model_get_iter() failed") ;
			goto cleanup ;
		}
	}
	gtk_list_store_remove (GTK_LIST_STORE (model),
	                       &iter) ;
	remove_xml_attr_row_ref_association (a_this, a_attr) ;
	g_signal_emit (G_OBJECT (a_this),
	               gv_signals [ATTRIBUTE_CHANGED], 0);
cleanup:
	if (tree_path) {
		gtk_tree_path_free (tree_path) ;
		tree_path = NULL ;
	}
	if (attr_val) {
		xmlFree (attr_val) ;
		attr_val = NULL ;
	}
	return status ;
}

/**
 *Removes an attribute row from the editor.
 *@param a_this the current instance of #MlViewAttrsEditor.
 *@param a_node the xml node that holds the attribute to be removed.
 *If the current xml node being edited is different from a_node, the
 *the function just returns MLVIEW_OK and does nothing.
 *@param a_name the name of the attribute to remove.
 *@return MLVIEW_OK upon successful completion, an error code otherwise.
 */
enum MlViewStatus
mlview_attrs_editor_update_attribute_removed2 (MlViewAttrsEditor *a_this,
        xmlNode *a_node,
        xmlChar *a_name)
{
	GtkTreeIter iter = {0} ;
	GtkTreeModel *model = NULL ;
	gboolean is_ok = TRUE ;
	xmlChar *attr_name = NULL;
	xmlAttr *attr = NULL ;

	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_ATTRS_EDITOR (a_this)
	                      && PRIVATE (a_this)
	                      && a_name
	                      && a_node,
	                      MLVIEW_BAD_PARAM_ERROR) ;

	if (!PRIVATE (a_this)->current_xml_node
	        || PRIVATE (a_this)->current_xml_node != a_node)
		return MLVIEW_OK ;

	model = mlview_attrs_editor_get_model (a_this) ;
	g_return_val_if_fail (model, MLVIEW_ERROR) ;
	is_ok = gtk_tree_model_get_iter_first (model, &iter) ;
	g_return_val_if_fail (is_ok == TRUE, MLVIEW_ERROR) ;
	for (is_ok = gtk_tree_model_get_iter_first (model, &iter) ;
	        is_ok == TRUE;
	        is_ok = gtk_tree_model_iter_next (model, &iter) ) {
		gtk_tree_model_get
		(model, &iter,
		 ATTRIBUTE_NAMES_COLUMN, &attr_name,
		 HIDDEN_XML_ATTR_COLUMN, &attr,
		 -1) ;
		if (attr_name && !strcmp ((char*)attr_name, (char*)a_name))
			break ;
		attr_name = NULL ;
		attr = NULL ;
	}
	if (is_ok == FALSE) {
		/*we didn't find any attribute to visualy remove*/
		return MLVIEW_OK ;
	}
	g_return_val_if_fail (attr, MLVIEW_ERROR) ;
	gtk_list_store_remove (GTK_LIST_STORE (model), &iter) ;
	remove_xml_attr_row_ref_association (a_this, attr) ;
	g_signal_emit (G_OBJECT (a_this),
	               gv_signals [ATTRIBUTE_CHANGED], 0);
	return MLVIEW_OK ;
}

/**
 *Connects to the signals emited by #MlViewXMLDocument .
 *This defines the callbacks to be called for each relevant
 *signal emitted by the document model.
 *@param a_this the current instance of #MlViewAttrsEditor.
 *@param a_doc the document model to connect to.
 *@return MLVIEW_OK upon successful completion, an error code
 *otherwise.
 */
enum MlViewStatus
mlview_attrs_editor_connect_to_doc (MlViewAttrsEditor *a_this,
                                    MlViewXMLDocument *a_doc)
{
	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_ATTRS_EDITOR (a_this)
	                      && a_doc
	                      && MLVIEW_IS_XML_DOCUMENT (a_doc),
	                      MLVIEW_BAD_PARAM_ERROR) ;

	g_signal_connect (G_OBJECT (a_doc),
	                  "node-attribute-name-changed",
	                  G_CALLBACK
	                  (xml_doc_node_attribute_name_changed_cb),
	                  a_this) ;
	g_signal_connect (G_OBJECT (a_doc),
	                  "node-attribute-value-changed",
	                  G_CALLBACK
	                  (xml_doc_node_attribute_value_changed_cb),
	                  a_this) ;
	g_signal_connect (G_OBJECT (a_doc),
	                  "node-attribute-added",
	                  G_CALLBACK (xml_doc_node_attribute_added_cb),
	                  a_this) ;
	g_signal_connect (G_OBJECT (a_doc),
	                  "node-attribute-removed",
	                  G_CALLBACK
	                  (xml_doc_node_attribute_removed_cb),
	                  a_this) ;
	return MLVIEW_OK ;
}

/**
 *Disconnects from the document model (#MlViewXMLDocument)
 *Disconnects all the callbacks connected by 
 *mlview_attrs_editor_connect_to_doc() .
 *@param a_this the current instance of #MlViewAttrsEditor.
 *@param a_doc the document object model to disconnect from.
 *@return MLVIEW_OK upon successful completion, an error code
 *otherwise.
 */
enum MlViewStatus
mlview_attrs_editor_disconnect_from_doc (MlViewAttrsEditor *a_this,
        MlViewXMLDocument *a_doc)
{
	g_return_val_if_fail (a_this
	                      && MLVIEW_IS_ATTRS_EDITOR (a_this)
	                      && PRIVATE (a_this)
	                      && a_doc
	                      && MLVIEW_IS_XML_DOCUMENT (a_doc),
	                      MLVIEW_BAD_PARAM_ERROR) ;

	g_signal_handlers_disconnect_by_func
	(G_OBJECT (a_doc),
	 (void*)xml_doc_node_attribute_name_changed_cb,
	 a_this) ;
	g_signal_handlers_disconnect_by_func
	(G_OBJECT (a_doc),
	 (void*)xml_doc_node_attribute_value_changed_cb,
	 a_this) ;
	g_signal_handlers_disconnect_by_func
	(G_OBJECT (a_doc),
	 (void*)xml_doc_node_attribute_removed_cb,
	 a_this) ;
	return MLVIEW_OK ;
}
