/* -*- Mode: C++; indent-tabs-mode:nil; c-basic-offset: 4-*- */
/*
 *This file is part of MlView
 *
 *MlView is free software; you can redistribute
 *it and/or modify it under the terms of
 *the GNU General Public License as published
 *by the Free Software Foundation; either version 2,
 *or (at your option) any later version.
 *
 *MlView is distributed in the hope that it
 *will be useful, but WITHOUT ANY WARRANTY;
 *without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the GNU
 *General Public License along with MlView;
 *see the file COPYING. If not, write to
 *the Free Software Foundation, Inc.,
 *59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */

#include <string.h>

#include <glade/glade.h>
#include <gtk/gtk.h>
#include "mlview-editor.h"
#include "mlview-tree-view.h"
#include "mlview-tree-editor.h"
#include "mlview-icon-tree.h"
#include "mlview-prefs.h"
#include "mlview-prefs-category-sizes.h"
#include "mlview-prefs-category-general.h"
#include "gtkmm.h"

/**
 *@file
 *The definition of the #TreeView class.
 */

namespace mlview
{

struct TreeViewPriv
{
	GtkWidget *set_name_dialog ;
	Gtk::Notebook *tree_editors ;
	xmlNode *current_node ;
	MlViewTreeEditor *current_tree_editor ;
	MlViewTreeEditor *raw_tree_editor ;
	MlViewTreeEditor *icon_tree_editor ;
	MlViewCompletionTable *completion_widget ;
	MlViewNodeEditor *node_editor ;
	GtkDialog *expand_tree_dialog;
	Gtk::Paned *main_paned;
	Gtk::Paned *upper_paned1;
	guint main_paned_percentage;
	struct TreeEditorContextualMenuHandle *contextual_menu_handle ;
	GtkUIManager *ui_manager ;
	GtkActionGroup *action_group ;
	GtkActionGroup *add_child_element_action_group ;
	GtkActionGroup *insert_next_element_action_group ;
	GtkActionGroup *insert_prev_element_action_group ;
	guint add_child_element_merge_id ;
	guint insert_prev_element_merge_id ;
	guint insert_next_element_merge_id ;
	guint edit_menu_merge_id ;
	guint tree_view_popup_edit_menu_merge_id ;
	sigc::signal0<void> signal_document_changed ;

	TreeViewPriv ():
			set_name_dialog (NULL),
			tree_editors (NULL),
			current_node (NULL),
			current_tree_editor (NULL),
			raw_tree_editor (NULL),
			icon_tree_editor (NULL),
			completion_widget (NULL),
			node_editor (NULL),
			expand_tree_dialog (NULL),
			main_paned (NULL),
			upper_paned1 (NULL),
			main_paned_percentage (0),
			contextual_menu_handle (NULL),
			ui_manager (NULL),
			action_group (NULL),
			add_child_element_action_group (NULL),
			add_child_element_merge_id (0),
			insert_prev_element_merge_id (0),
			insert_next_element_merge_id (0),
			edit_menu_merge_id (0),
			tree_view_popup_edit_menu_merge_id (0)

	{}
}
;

static void comment_node_action_cb (GtkAction *a_action,
                                    TreeView *a_this) ;

static void uncomment_node_action_cb (GtkAction *a_action,
                                      TreeView *a_this) ;

    static void add_child_node_action_cb (GtkAction *a_action,
					  TreeView *a_this) ;

static void insert_element_action_cb (GtkAction *a_action,
                                      TreeView *a_this) ;

    static void insert_next_sibling_node_action_cb (GtkAction *a_action,
						    TreeView *a_this) ;

static void insert_next_sibling_node_action_cb (GtkAction *a_action,
        TreeView *a_this) ;

static void insert_prev_sibling_node_action_cb (GtkAction *a_action,
        TreeView *a_this) ;


static void copy_node_action_cb (GtkAction *a_action,
                                 TreeView *a_this) ;

static void cut_node_action_cb (GtkAction *a_action,
                                TreeView *a_this) ;

static void paste_node_as_child_action_cb (GtkAction *a_action,
        TreeView *a_this) ;

static void paste_node_as_prev_action_cb (GtkAction *a_action,
        TreeView *a_this) ;

static void paste_node_as_next_action_cb (GtkAction *a_action,
        TreeView *a_this) ;

static void select_next_sibling_node_action_cb (GtkAction *a_action,
        TreeView *a_this) ;

static void select_prev_sibling_node_action_cb (GtkAction *a_action,
        TreeView *a_this) ;


static void xml_node_changed_cb (MlViewNodeEditor * a_editor,
                                 gpointer a_data);

static void select_parent_node_action_cb (GtkAction *a_action,
        TreeView *a_this) ;

static void find_node_action_cb (GtkAction *a_action,
                                 TreeView *a_this) ;

static void document_ext_subset_changed_cb (MlViewXMLDocument *a_xml_doc,
        gpointer a_user_data) ;

static gboolean completion_widget_mapped_cb (GtkWidget *a_widget,
        gpointer a_user_data) ;

static void update_completion_widget_cb (MlViewXMLDocument *a_xml_doc,
        xmlNode *a_node_found,
        gpointer a_user_data) ;

static void doc_path_changed_cb (MlViewXMLDocument * a_xml_doc,
                                 gpointer a_xml_doc_tree_view) ;

static void tree_editor_ungrab_focus_requested_cb (MlViewTreeEditor *a_editor,
        gpointer a_user_data) ;

static void node_editor_ungrab_focus_requested_cb (MlViewNodeEditor *a_editor,
        gpointer a_user_data) ;

static void toggle_expand_to_leaves_cb (GtkToggleButton * a_toggle_button,
                                        gpointer * a_depth_entry) ;

static GtkActionEntry gv_edit_menu_actions [] = {
            {
                "CommentNodeAction", NULL, N_("Comment"),
                NULL, N_("Comment the currently selected node"),
                G_CALLBACK (comment_node_action_cb)
            },

            {
                "UncommentNodeAction", NULL, N_("Uncomment"),
                NULL, N_("Uncomment the currently selected node"),
                G_CALLBACK (uncomment_node_action_cb)
            },

            {
                "AddChildNodeMenuAction", NULL, N_("Add child node"),
                NULL, N_("Add a child node to the currently selected node"),
                NULL
            },

            {
                "AddChildNodeAction", NULL, N_("Add custom child node"),
                NULL, N_("Add a custom child node to the currently selected node"),
                G_CALLBACK (add_child_node_action_cb)
            },


            {
                "InsertNextSiblingNodeMenuAction", NULL, N_("Insert a node after"),
                NULL, N_("Insert a node after the currently selected node"),
                NULL
            },

            {
                "InsertNextSiblingNodeAction", NULL, N_("Insert a custom node after"),
                NULL, N_("Insert a node after the currently selected node"),
                G_CALLBACK (insert_next_sibling_node_action_cb)
            },

            {
                "InsertPrevSiblingNodeMenuAction", NULL, N_("Insert a  node before"),
                NULL, N_("Insert a node after the currently selected node"),
                NULL
            },

            {
                "InsertPrevSiblingNodeAction", NULL, N_("Insert a custom node before"),
                NULL, N_("Insert a node after the currently selected node"),
                G_CALLBACK (insert_prev_sibling_node_action_cb)
            },


            {
                "CopyNodeAction", NULL, N_("Copy node"),
                NULL, N_("Copy the current node to the internal clipboard"),
                G_CALLBACK (copy_node_action_cb)
            },

            {
                "CutNodeAction", NULL, N_("Cut node"),
                NULL, N_("Cuts the current node to the internal clipboard"),
                G_CALLBACK (cut_node_action_cb)
            },

            {
                "PasteNodeAsChildAction", NULL, N_("Paste node as child"),
                NULL, N_("Paste node as a child of the currently selected node"),
                G_CALLBACK (paste_node_as_child_action_cb)
            },

            {
                "PasteNodeAsPrevAction", NULL, N_("Paste node as previous sibling"),
                NULL, N_("Paste node as the previous sibling of the currently selected node"),
                G_CALLBACK (paste_node_as_prev_action_cb)
            },

            {
                "PasteNodeAsNextAction", NULL, N_("Paste node as next sibling"),
                NULL, N_("Paste node as the next sibling of the currently selected node"),
                G_CALLBACK (paste_node_as_next_action_cb)
            },

            {
                "SelectNextSiblingAction", NULL, N_("Select next sibling"),
                NULL, N_("Select the next sibling node"),
                G_CALLBACK (select_next_sibling_node_action_cb)
            },

            {
                "SelectPrevSiblingAction", NULL, N_("Select prev sibling"),
                NULL, N_("Select the previous sibling node"),
                G_CALLBACK (select_prev_sibling_node_action_cb)
            },

            {
                "SelectParentNodeAction", NULL, N_("Select parent node"),
                NULL, N_("Select the parent of the currently selected node"),
                G_CALLBACK (select_parent_node_action_cb)
            },


            {
                "FindNodeAction", GTK_STOCK_FIND, N_("Find node"),
                NULL, N_("Find a node"),
                G_CALLBACK (find_node_action_cb)
            },

        } ;

//*********************
//static functions
//*********************
//***************************
//activated action callbacks
//***************************
static void
comment_node_action_cb (GtkAction *a_action,
                        TreeView *a_view)
{
	MlViewAction action = {0} ;

	THROW_IF_FAIL (a_view) ;
	action.name = (gchar*) "comment-current-node";
	a_view->execute_action (&action) ;
}

static void
uncomment_node_action_cb (GtkAction *a_action,
                          TreeView *a_view)
{
	MlViewAction action = {0} ;

	action.name = (gchar*) "uncomment-current-node" ;
	a_view->execute_action (&action) ;
}

static void
add_child_node_action_cb (GtkAction *a_action,
                          TreeView *a_view)
{
	MlViewAction action = {0} ;

	THROW_IF_FAIL (a_view) ;

	action.name = (gchar*) "add-child-node-interactive";
	a_view->execute_action (&action) ;
}

static void
insert_element_action_cb (GtkAction *a_action,
                          TreeView *a_view)
{
	gchar *action_name = NULL, *label = NULL ;

	action_name = (gchar*) gtk_action_get_name (a_action) ;
	g_object_get (G_OBJECT (a_action),
	              "label", &label,
	              NULL) ;
	THROW_IF_FAIL (label) ;

	if (g_str_has_prefix (action_name,
	                      "AddElementAction")) {
		if (!strcmp (label, "#PCDATA")) {
			a_view->add_child_text_node ("text node") ;
		} else {
			a_view->add_child_element_node (label) ;
		}
	}
	if (g_str_has_prefix (action_name,
	                      "InsertPrevSiblingElementAction")) {
		if (!strcmp (label, "#PCDATA")) {
			a_view->insert_prev_sibling_text_node ("text node") ;
		} else {
			a_view->insert_prev_sibling_element_node (label) ;
		}
	}
	if (g_str_has_prefix (action_name,
	                      "InsertNextSiblingElementAction")) {
		if (!strcmp (label, "#PCDATA")) {
			a_view->insert_next_sibling_text_node ("text node") ;
		} else {
			a_view->insert_next_sibling_element_node (label) ;
		}
	}
}

static void
insert_next_sibling_node_action_cb (GtkAction *a_action,
                                    TreeView *a_view)
{
	MlViewAction action = {0} ;

	action.name = (gchar*) "insert-next-sibling-node-interactive";
	a_view->execute_action (&action) ;
}

static void
insert_prev_sibling_node_action_cb (GtkAction *a_action,
                                    TreeView *a_view)
{
	MlViewAction action = {0} ;
	THROW_IF_FAIL (a_view) ;

	action.name = (gchar*) "insert-prev-sibling-node-interactive";
	a_view->execute_action (&action) ;
}

static void
copy_node_action_cb (GtkAction *a_action,
                     TreeView *a_view)
{
	MlViewAction action = {0} ;

	action.name = (gchar*) "copy-node";

	a_view->execute_action (&action) ;
}

static void
cut_node_action_cb (GtkAction *a_action,
                    TreeView *a_view)
{
	MlViewAction action = {0} ;

	action.name = (gchar*) "cut-node";
	a_view->execute_action (&action) ;
}

static void
paste_node_as_child_action_cb (GtkAction *a_action,
                               TreeView *a_view)
{
	MlViewAction action = {0} ;
	THROW_IF_FAIL (a_view) ;

	action.name = (gchar*) "paste-node-as-child";
	a_view->execute_action (&action) ;
}

static void
paste_node_as_prev_action_cb (GtkAction *a_action,
                              TreeView *a_view)
{
	MlViewAction action = {0} ;

	THROW_IF_FAIL (a_view) ;

	action.name = (gchar*) "paste-node-as-prev-sibling";

	a_view->execute_action (&action) ;
}

static void
paste_node_as_next_action_cb (GtkAction *a_action,
                              TreeView *a_view)
{
	MlViewAction action = {0} ;

	THROW_IF_FAIL (a_view) ;

	action.name = (gchar*) "paste-node-as-next-sibling";

	a_view->execute_action (&action) ;
}

static void
select_next_sibling_node_action_cb (GtkAction *a_action,
                                    TreeView *a_view)
{
	MlViewAction action = {0} ;

	action.name = (gchar*) "select-next-sibling-node";
	a_view->execute_action (&action) ;
}

static void
select_prev_sibling_node_action_cb (GtkAction *a_action,
                                    TreeView *a_view)
{
	MlViewAction action = {0} ;

	action.name = (gchar*) "select-prev-sibling-node";

	a_view->execute_action (&action) ;
}

static void
select_parent_node_action_cb (GtkAction *a_action,
                              TreeView *a_view)
{
	MlViewAction action = {0} ;

	action.name = (gchar*) "select-parent-node";

	a_view->execute_action (&action) ;
}


static void
find_node_action_cb (GtkAction *a_action,
                     TreeView *a_view)
{
	MlViewAction action = {0} ;
	THROW_IF_FAIL (a_view) ;

	action.name = (gchar*) "find-node-that-contains-str-interactive";
	a_view->execute_action (&action) ;
}

//****************
//callback functions
//***************
static void
xml_node_changed_cb (MlViewNodeEditor * a_node_editor,
		     gpointer a_data)
{
	GtkTreeView *visual_tree=NULL;
	xmlNodePtr xml_node=NULL;
	TreeView *view=NULL;
	MlViewTreeEditor *tree_editor = NULL;

	THROW_IF_FAIL (a_node_editor
	               && MLVIEW_IS_NODE_EDITOR (a_node_editor)
	               && a_data) ;
	view = (TreeView*) a_data ;
	Gtk::Notebook *notebook = view->get_tree_editors_notebook () ;

	xml_node =
	    mlview_node_editor_get_current_xml_node (a_node_editor);
	THROW_IF_FAIL (xml_node != NULL);

	Gtk::Notebook_Helpers::PageList pages = notebook->pages () ;
	Gtk::Notebook_Helpers::PageList::iterator it ;
	for (it = pages.begin () ; it != pages.end () ; ++it) {
		tree_editor = MLVIEW_TREE_EDITOR (it->get_child ()->gobj ()) ;
		THROW_IF_FAIL (tree_editor) ;
		visual_tree = mlview_tree_editor_get_tree_view (tree_editor);
		THROW_IF_FAIL (visual_tree && GTK_IS_TREE_VIEW (visual_tree));
		mlview_tree_editor_update_visual_node2 (tree_editor,
												xml_node,
												FALSE);
	}
}

static void
update_completion_widget_cb (MlViewXMLDocument *a_xml_doc,
			     xmlNode *a_node_found,
			     gpointer a_user_data)
{
	THROW_IF_FAIL (a_xml_doc && MLVIEW_IS_XML_DOCUMENT (a_xml_doc));
	THROW_IF_FAIL (a_node_found);
	THROW_IF_FAIL (a_user_data && MLVIEW_IS_COMPLETION_TABLE (a_user_data));

	mlview_completion_table_select_node (MLVIEW_COMPLETION_TABLE (a_user_data),
	                                     a_node_found);
}

static void
doc_path_changed_cb (MlViewXMLDocument * a_xml_doc,
		     gpointer a_xml_doc_tree_view)
{
	MlViewFileDescriptor *file_desc = NULL;
	TreeView *tree_view = NULL;
	gchar *path = NULL;

	THROW_IF_FAIL (a_xml_doc != NULL);
	THROW_IF_FAIL (MLVIEW_IS_XML_DOCUMENT (a_xml_doc));
	THROW_IF_FAIL (a_xml_doc_tree_view != NULL);

	tree_view = (TreeView*) a_xml_doc_tree_view;
	file_desc = mlview_xml_document_get_file_descriptor (a_xml_doc);

	THROW_IF_FAIL (file_desc != NULL);

	path = mlview_file_descriptor_get_file_path (file_desc);

	THROW_IF_FAIL (path != NULL);

	tree_view->set_xml_document_path (path);
}

static void
tree_editor_ungrab_focus_requested_cb (MlViewTreeEditor *a_editor,
                                       gpointer a_user_data)
{
	TreeView *tree_view = NULL ;

	THROW_IF_FAIL (a_editor && MLVIEW_IS_TREE_EDITOR (a_editor)) ;

	tree_view = (TreeView*) (a_user_data) ;
	THROW_IF_FAIL (tree_view) ;
	MlViewNodeEditor *node_editor = tree_view->get_node_editor () ;
	mlview_node_editor_grab_focus (node_editor) ;
}

static void
document_ext_subset_changed_cb (MlViewXMLDocument *a_xml_doc,
                                gpointer a_user_data)
{
	xmlNode *current_node = NULL ;
	TreeView *tree_view = (TreeView*) (a_user_data) ;

	THROW_IF_FAIL (a_xml_doc && MLVIEW_IS_XML_DOCUMENT (a_xml_doc));
	THROW_IF_FAIL (tree_view) ;

	MlViewCompletionTable *completion_widget =
	    tree_view->get_completion_widget () ;
	MlViewTreeEditor * current_tree_editor =
	    tree_view->get_current_tree_editor () ;

	if (mlview_xml_document_is_completion_possible_global
	        (a_xml_doc)
	        && completion_widget) {
		gtk_widget_show (GTK_WIDGET (completion_widget)) ;
		current_node = mlview_tree_editor_get_cur_sel_xml_node
		               (current_tree_editor) ;
		if (current_node) {
			mlview_completion_table_select_node
			(completion_widget, current_node) ;
		}
	}
}

static gboolean
completion_widget_mapped_cb (GtkWidget *a_widget,
                             gpointer a_user_data)
{
	TreeView *tree_view = NULL ;
	MlViewXMLDocument *xml_doc = NULL ;

	tree_view = (TreeView*) (a_user_data) ;
	THROW_IF_FAIL (tree_view) ;

	xml_doc = tree_view->get_document () ;
	THROW_IF_FAIL (xml_doc) ;

	if (!mlview_xml_document_is_completion_possible_global (xml_doc))
		gtk_widget_hide (a_widget);

	return FALSE ;
}


static void
node_editor_ungrab_focus_requested_cb (MlViewNodeEditor *a_editor,
                                       gpointer a_user_data)
{
	TreeView *tree_view = NULL ;
	MlViewTreeEditor *current_tree_editor = NULL ;

	THROW_IF_FAIL (a_editor
	               && MLVIEW_IS_NODE_EDITOR (a_editor)
	               && a_user_data) ;

	tree_view = (TreeView*) a_user_data ;
	current_tree_editor = tree_view->get_current_tree_editor () ;
	THROW_IF_FAIL (current_tree_editor) ;
	mlview_tree_editor_grab_focus (current_tree_editor) ;
}

static void
toggle_expand_to_leaves_cb (GtkToggleButton * a_toggle_button,
                            gpointer * a_depth_entry)
{
	THROW_IF_FAIL (a_toggle_button != NULL);
	THROW_IF_FAIL (GTK_IS_TOGGLE_BUTTON
	               (a_toggle_button));
	THROW_IF_FAIL (a_depth_entry != NULL);
	THROW_IF_FAIL (GTK_IS_WIDGET (a_depth_entry));

	if (gtk_toggle_button_get_active (a_toggle_button) == TRUE)
		gtk_widget_set_sensitive (GTK_WIDGET
		                          (a_depth_entry),
		                          FALSE);
	else
		gtk_widget_set_sensitive (GTK_WIDGET
		                          (a_depth_entry), TRUE);
}

//*********************
//public methods
//*********************


TreeView::TreeView (MlViewXMLDocument *a_doc,
                    const UString &a_name):
		ViewAdapter::ViewAdapter (a_doc, a_name, "tree-view")
{
	THROW_IF_FAIL (a_doc) ;

	m_priv = new TreeViewPriv ();

	//******************
	//construct the view
	//******************
	AppContext *context = AppContext::get_instance () ;
	THROW_IF_FAIL (context) ;

	m_priv->main_paned = manage (new Gtk::HPaned ()) ;
	m_priv->main_paned->reference () ;
	Gtk::VBox *view_impl = dynamic_cast<Gtk::VBox*>(get_view_widget ()) ;
	THROW_IF_FAIL (view_impl) ;
	view_impl->pack_start (*m_priv->main_paned, true, true);

	//The tree editors and the tree editors' notebook
	//
	// Notebook
	m_priv->upper_paned1 = manage (new Gtk::VPaned ()) ;
	m_priv->upper_paned1->reference () ;

	m_priv->main_paned->add1 (*m_priv->upper_paned1) ;

	m_priv->tree_editors = manage (new Gtk::Notebook ()) ;
	m_priv->tree_editors->reference ();

	m_priv->tree_editors->set_tab_pos (Gtk::POS_BOTTOM) ;

	m_priv->upper_paned1->add1 (*m_priv->tree_editors) ;
	m_priv->tree_editors->signal_switch_page ().connect
	(sigc::mem_fun2 (*this, &TreeView::on_tree_editor_selected)) ;

	/* Tree editing widgets */
	MlViewTreeEditor *raw_xml = MLVIEW_TREE_EDITOR
	                            (mlview_tree_editor_new ());

	MlViewTreeEditor *elements = MLVIEW_TREE_EDITOR
	                             (mlview_icon_tree_new ());

	m_priv->raw_tree_editor = raw_xml;
	m_priv->icon_tree_editor = elements ;

	m_priv->tree_editors->append_page
	(*Glib::wrap (GTK_WIDGET (elements)),
	 *manage (new Gtk::Label (_("Elements")))) ;

	m_priv->tree_editors->append_page
	(*Glib::wrap (GTK_WIDGET (raw_xml)),
	 *manage (new Gtk::Label (_("Raw XML")))) ;

	/* The element name completion widget*/
	m_priv->completion_widget =
	    MLVIEW_COMPLETION_TABLE
	    (mlview_completion_table_new (a_doc)) ;

	g_signal_connect
	(a_doc, "node-selected",
	 (GCallback)update_completion_widget_cb,
	 m_priv->completion_widget);

	/*The node editor */
	m_priv->node_editor = MLVIEW_NODE_EDITOR (mlview_node_editor_new (a_doc));
	g_object_ref (G_OBJECT (m_priv->node_editor)) ;

	g_signal_connect (G_OBJECT
	                  (m_priv->node_editor),
	                  "element-changed",
	                  G_CALLBACK (xml_node_changed_cb),
	                  this);

	m_priv->upper_paned1->pack2 (*Glib::wrap (GTK_WIDGET (m_priv->node_editor)),
	                             FALSE, FALSE) ;

	m_priv->main_paned->pack2 (*Glib::wrap (GTK_WIDGET (m_priv->completion_widget)),
	                           FALSE, TRUE) ;

	/* Restore paned positions */
	mlview::PrefsCategorySizes *prefs =
	    dynamic_cast<mlview::PrefsCategorySizes*> (
		mlview::Preferences::get_instance ()
		->get_category_by_id (mlview::PrefsCategorySizes::CATEGORY_ID));

	m_priv->upper_paned1->set_position (prefs->get_tree_editor_size ());
	m_priv->main_paned->set_position (prefs->get_completion_box_size ());

	/* Build trees */
	int nb_pages = m_priv->tree_editors->get_n_pages () ;
	for (int i = 0; i < nb_pages; i++) {
		Gtk::Widget *widget = NULL ;
		MlViewTreeEditor *tree_editor = NULL;

		widget = m_priv->tree_editors->get_nth_page (i) ;
		tree_editor = MLVIEW_TREE_EDITOR (widget->gobj ()) ;
		THROW_IF_FAIL (MLVIEW_IS_TREE_EDITOR (tree_editor)) ;
		mlview_tree_editor_edit_xml_doc (tree_editor, a_doc);
	}

	g_signal_connect (G_OBJECT (a_doc),
	                  "file-path-changed",
	                  G_CALLBACK
	                  (doc_path_changed_cb),
	                  this) ;
	g_signal_connect (G_OBJECT (m_priv->completion_widget),
	                  "map",
	                  G_CALLBACK (completion_widget_mapped_cb),
	                  this) ;
	g_signal_connect (G_OBJECT (get_document ()),
	                  "ext-subset-changed",
	                  G_CALLBACK (document_ext_subset_changed_cb),
	                  this);

	context->signal_contextual_menu_requested ().connect
					(sigc::mem_fun (*this,
									&TreeView::on_contextual_menu_requested)) ;
	context->signal_view_swapped ().connect
					(sigc::mem_fun (*this, &TreeView::on_view_swapped)) ;

	g_signal_connect (G_OBJECT (m_priv->raw_tree_editor),
	                  "ungrab-focus-requested",
	                  G_CALLBACK (tree_editor_ungrab_focus_requested_cb),
	                  this) ;
	g_signal_connect (G_OBJECT (m_priv->icon_tree_editor),
	                  "ungrab-focus-requested",
	                  G_CALLBACK (tree_editor_ungrab_focus_requested_cb),
	                  this) ;
	g_signal_connect (G_OBJECT (m_priv->node_editor),
	                  "ungrab-focus-requested",
	                  G_CALLBACK (node_editor_ungrab_focus_requested_cb) ,
	                  this) ;
	get_view_widget ()->signal_realize ().connect
			(sigc::mem_fun (*this, &TreeView::on_realized)) ;

	signal_is_swapped_in ().connect
	(sigc::mem_fun (*this, &TreeView::on_is_swapped_in)) ;
	signal_is_swapped_out ().connect
	(sigc::mem_fun (*this, &TreeView::on_is_swapped_out)) ;

	UString path = mlview_xml_document_get_file_path (a_doc) ;
	if (path == "")
		path = "Untitled document" ;

	set_xml_document_path (path) ;

	//connect to the doc signals and select the root node.
	connect_to_doc (a_doc) ;
	xmlDoc * xml_doc = mlview_xml_document_get_native_document (a_doc) ;
	if (xml_doc)
		mlview_xml_document_select_node (a_doc, (xmlNode*)xml_doc) ;
}

TreeView::~TreeView ()
{
	THROW_IF_FAIL (m_priv) ;

	clear_completion_popup_submenus () ;

	//save paned positions state
	AppContext *context = AppContext::get_instance () ;
	THROW_IF_FAIL (context) ;
	context->save_treeview_state (m_priv->upper_paned1->get_position (),
								  m_priv->main_paned->get_position ());
	m_priv->upper_paned1->unreference () ;
	m_priv->main_paned->unreference () ;
	m_priv->main_paned = NULL ;
	m_priv->upper_paned1 = NULL ;

	if (m_priv->expand_tree_dialog) {
		delete m_priv->expand_tree_dialog ;
		m_priv->expand_tree_dialog = NULL ;
	}
	if (get_document ()) {
		disconnect_from_doc (get_document ()) ;
		g_signal_handlers_disconnect_by_func
		(G_OBJECT (get_document ()),
		 (void*)document_ext_subset_changed_cb,
		 this) ;

		g_signal_handlers_disconnect_by_func
		(G_OBJECT (get_document ()),
		 (void*)doc_path_changed_cb,
		 this) ;
		m_priv->tree_editors->unreference () ;
	}

	delete m_priv ;
	m_priv = NULL ;
}


MlViewTreeEditor *
TreeView::get_current_tree_editor ()
{
	THROW_IF_FAIL (m_priv) ;
	return m_priv->current_tree_editor ;
}

MlViewNodeEditor *
TreeView::get_node_editor ()
{
	THROW_IF_FAIL (m_priv) ;

	return m_priv->node_editor ;
}

MlViewCompletionTable*
TreeView::get_completion_widget ()
{
	THROW_IF_FAIL (m_priv) ;

	return m_priv->completion_widget ;
}

Gtk::Notebook *
TreeView::get_tree_editors_notebook ()
{
	THROW_IF_FAIL (m_priv) ;
	return m_priv->tree_editors ;
}


enum MlViewStatus
TreeView::create_internal_subset_node_interactive ()
{
	THROW_IF_FAIL (m_priv) ;

	if (!get_document ())
		return MLVIEW_OK;

	xmlDoc *native_doc = mlview_xml_document_get_native_document
	                     (get_document ()) ;
	THROW_IF_FAIL (native_doc) ;
	AppContext *context = AppContext::get_instance () ;
	THROW_IF_FAIL (context) ;
	if (native_doc->intSubset) {
		context->error (_("The document already has "
						  "an internal subset defined !")) ;
		return MLVIEW_OK ;
	}
	gchar *cstr=NULL ;
	AppContext::ask_internal_subset_node_name (&cstr) ;
	UString name (cstr) ;
	if (name != "") {
		return mlview_xml_document_create_internal_subset
		       (get_document (),
		        (const xmlChar*)name.c_str (),
		        (const xmlChar*)"default-public-id",
		        (const xmlChar*)"default-system-id",
		        TRUE) ;
	}
	return MLVIEW_OK ;
}

void
TreeView::add_child_element_node (const UString &a_element_name)
{
	MlViewTreeEditor *tree_editor = NULL ;

	tree_editor = get_current_tree_editor () ;
	THROW_IF_FAIL (tree_editor) ;
	mlview_tree_editor_add_child_element_node (tree_editor,
	        a_element_name.c_str (),
	        FALSE) ;
}

void
TreeView::add_child_text_node (const UString &a_text)
{
	MlViewTreeEditor *tree_editor = NULL ;


	tree_editor = get_current_tree_editor () ;
	THROW_IF_FAIL (tree_editor) ;

	mlview_tree_editor_add_child_text_node (tree_editor,
	                                        a_text.c_str (),
	                                        TRUE) ;
}


void
TreeView::add_child_node_interactive ()
{
	MlViewTreeEditor *tree_editor = NULL;
	tree_editor = get_current_tree_editor () ;
	THROW_IF_FAIL (tree_editor) ;

	mlview_tree_editor_add_child_node_interactive (tree_editor);
}

void
TreeView::insert_sibling_node_interactive ()
{
	MlViewTreeEditor *tree_editor = NULL;
	tree_editor = get_current_tree_editor ();

	THROW_IF_FAIL (tree_editor) ;
	mlview_tree_editor_insert_prev_sibling_node_interactive
	(tree_editor);
}

void
TreeView::insert_prev_sibling_node_interactive ()
{
	MlViewTreeEditor *tree_editor = NULL;
	tree_editor = get_current_tree_editor ();

	THROW_IF_FAIL (tree_editor) ;
	mlview_tree_editor_insert_prev_sibling_node_interactive
	(tree_editor);
}

void
TreeView::insert_prev_sibling_text_node (const UString &a_text)
{
	MlViewTreeEditor *tree_editor = NULL ;

	tree_editor = get_current_tree_editor () ;
	THROW_IF_FAIL (tree_editor) ;

	mlview_tree_editor_insert_prev_text_node (tree_editor,
	        a_text.c_str (),
	        TRUE) ;
}

void
TreeView::insert_next_sibling_text_node (const UString &a_text)
{
	MlViewTreeEditor *tree_editor = NULL ;

	tree_editor = get_current_tree_editor () ;
	THROW_IF_FAIL (tree_editor) ;

	mlview_tree_editor_insert_next_text_node (tree_editor,
	        a_text.c_str (), TRUE) ;
}

void
TreeView::insert_prev_sibling_element_node (const UString &a_element_name)
{
	MlViewTreeEditor *tree_editor = get_current_tree_editor () ;
	THROW_IF_FAIL (tree_editor) ;

	mlview_tree_editor_insert_prev_sibling_element_node
	(tree_editor, a_element_name.c_str (), FALSE) ;
}

void
TreeView::insert_next_sibling_element_node (const UString &a_element_name)
{
	MlViewTreeEditor *tree_editor = get_current_tree_editor () ;
	THROW_IF_FAIL (tree_editor) ;
	mlview_tree_editor_insert_next_sibling_element_node (tree_editor,
	        a_element_name.c_str (),
	        FALSE) ;
}

void
TreeView::insert_next_sibling_node_interactive ()
{
	MlViewTreeEditor *tree_editor = get_current_tree_editor ();
	THROW_IF_FAIL (tree_editor) ;
	mlview_tree_editor_insert_next_sibling_node_interactive (tree_editor);
}

void
TreeView::cut_node ()
{
	MlViewTreeEditor *tree_editor = NULL;
	GtkTreeIter cur_sel_start = {0};
	enum MlViewStatus status = MLVIEW_OK ;

	tree_editor = get_current_tree_editor ();
	THROW_IF_FAIL (tree_editor) ;

	status = mlview_tree_editor_get_cur_sel_start_iter
	         (tree_editor, &cur_sel_start);
	THROW_IF_FAIL (status == MLVIEW_OK) ;
	mlview_tree_editor_cut_node (tree_editor, &cur_sel_start);
}

void
TreeView::comment_current_node ()
{
	MlViewTreeEditor *tree_editor = NULL ;

	tree_editor = get_current_tree_editor () ;
	THROW_IF_FAIL (tree_editor) ;
	mlview_tree_editor_comment_current_node (tree_editor) ;
}

void
TreeView::uncomment_current_node ()
{
	MlViewTreeEditor *tree_editor = NULL ;

	tree_editor = get_current_tree_editor () ;
	THROW_IF_FAIL (tree_editor) ;
	mlview_tree_editor_uncomment_current_node (tree_editor) ;
}

void
TreeView::copy_node ()
{
	MlViewTreeEditor *tree_editor = NULL;
	GtkTreeIter cur_sel_start = {0} ;
	enum MlViewStatus status = MLVIEW_OK ;

	tree_editor = get_current_tree_editor () ;
	THROW_IF_FAIL (tree_editor) ;
	status = mlview_tree_editor_get_cur_sel_start_iter
	         (tree_editor, &cur_sel_start) ;
	THROW_IF_FAIL (status == MLVIEW_OK) ;
	mlview_tree_editor_copy_node (tree_editor, &cur_sel_start) ;
}

void
TreeView::paste_node_as_child ()
{
	GtkTreeIter cur_sel_start={0} ;
	enum MlViewStatus status = MLVIEW_OK;

	THROW_IF_FAIL (m_priv && m_priv->current_tree_editor) ;

	status = mlview_tree_editor_get_cur_sel_start_iter
	         (m_priv->current_tree_editor, &cur_sel_start) ;
	THROW_IF_FAIL (status == MLVIEW_OK) ;
	mlview_tree_editor_paste_node_as_child
	(m_priv->current_tree_editor, &cur_sel_start);
}

void
TreeView::paste_node_as_prev_sibling ()
{
	GtkTreeIter cur_sel_start={0} ;
	enum MlViewStatus status=MLVIEW_OK ;

	status = mlview_tree_editor_get_cur_sel_start_iter
	         (m_priv->current_tree_editor, &cur_sel_start) ;
	THROW_IF_FAIL (status == MLVIEW_OK) ;
	status = mlview_tree_editor_paste_node_as_sibling
	         (m_priv->current_tree_editor, &cur_sel_start, TRUE) ;
}

void
TreeView::paste_node_as_next_sibling ()
{
	GtkTreeIter cur_sel_start={0} ;
	enum MlViewStatus status = MLVIEW_OK ;

	THROW_IF_FAIL (m_priv != NULL) ;

	status = mlview_tree_editor_get_cur_sel_start_iter
	         (m_priv->current_tree_editor, &cur_sel_start) ;
	THROW_IF_FAIL (status == MLVIEW_OK) ;
	mlview_tree_editor_paste_node_as_sibling (m_priv->current_tree_editor,
	        &cur_sel_start, FALSE) ;
}

void
TreeView::set_upper_paned1_proportions (const guint a_percentage)
{
	gint separator_position = 0;
	GtkWidget *top_level_widget = NULL;

	THROW_IF_FAIL (m_priv != NULL);
	THROW_IF_FAIL (m_priv->upper_paned1) ;

	top_level_widget =
	    gtk_widget_get_toplevel (GTK_WIDGET (get_view_widget ()->gobj ()));
	THROW_IF_FAIL (top_level_widget != NULL);

	separator_position =
	    top_level_widget->allocation.height *
	    a_percentage / 100;

	m_priv->upper_paned1->set_position (separator_position) ;

	get_view_widget ()->show_all () ;
}

void
TreeView::find_xml_node_that_contains_str_interactive ()
{
	MlViewTreeEditor *tree_editor = NULL;

	tree_editor = get_current_tree_editor () ;
	THROW_IF_FAIL (tree_editor != NULL);
	mlview_tree_editor_search_interactive (tree_editor) ;
}

void
TreeView::set_main_paned_proportions (const guint a_percentage)
{
	guint separator_position = 0;
	Gtk::Widget *top_level_widget = NULL;

	THROW_IF_FAIL (m_priv) ;
	top_level_widget = get_view_widget ()->get_toplevel () ;
	THROW_IF_FAIL (top_level_widget != NULL);

	m_priv->main_paned_percentage = a_percentage;

	top_level_widget->get_allocation ().set_width (a_percentage/100) ;

	m_priv->main_paned->set_position (separator_position) ;

	get_view_widget ()->show_all () ;
}

void
TreeView::set_all_paned_proportions (const guint a_top_down_percentage,
                                     const guint a_left_right_percentage)
{
	set_upper_paned1_proportions (a_left_right_percentage) ;
	set_main_paned_proportions (a_top_down_percentage);
}


void
TreeView::set_xml_document_path (const UString &a_file_path)
{
	THROW_IF_FAIL (m_priv) ;
	THROW_IF_FAIL (m_priv->tree_editors) ;

	if (m_priv->tree_editors) {
		MlViewTreeEditor *tree_editor = NULL;
		Gtk::Notebook_Helpers::PageList pages =
		    m_priv->tree_editors->pages () ;
		Gtk::Notebook_Helpers::PageList::iterator it ;
		for (it = pages.begin () ; it != pages.end () ; ++it) {
			tree_editor = MLVIEW_TREE_EDITOR
			              (it->get_child ()->gobj ()) ;
			THROW_IF_FAIL (tree_editor) ;
			mlview_tree_editor_set_xml_document_path
			(tree_editor, a_file_path.c_str ()) ;
			tree_editor = NULL ;
		}
	}
	set_view_name (UString (path_get_basename (a_file_path))) ;
}

void
TreeView::expand_tree_to_depth_interactive ()
{
	GtkDialog *dialog = NULL;
	GtkWidget *expand_to_leaves = NULL,
	                              *depth_entry = NULL;
	gint button = 0,
	              depth = 0;
	gchar *depth_str = NULL;

	THROW_IF_FAIL (m_priv) ;

	dialog = get_expand_tree_dialog ();
	THROW_IF_FAIL (dialog != NULL);

	button = gtk_dialog_run (dialog);

	expand_to_leaves = (GtkWidget*)
	                   gtk_object_get_data (GTK_OBJECT (dialog),
	                                        "expand-to-leaves");
	THROW_IF_FAIL (expand_to_leaves != NULL);

	depth_entry = (GtkWidget*) gtk_object_get_data (GTK_OBJECT (dialog),
	              "depth-entry");
	THROW_IF_FAIL (depth_entry != NULL);

	switch (button) {
	case GTK_RESPONSE_ACCEPT: /*user clicked OK */
		if (gtk_toggle_button_get_active
		        (GTK_TOGGLE_BUTTON (expand_to_leaves)) ==
		        TRUE) {
			mlview_tree_editor_expand_tree_to_depth
			(m_priv->current_tree_editor, -1);
		} else {
			depth_str = (gchar *)
			            gtk_entry_get_text (GTK_ENTRY (depth_entry));

			if (depth_str) {
				depth = atoi (depth_str);
				mlview_tree_editor_expand_tree_to_depth
				(m_priv->current_tree_editor, depth);
			}
		}
		break;
	case GTK_RESPONSE_REJECT: /*user clicked CANCEL */
	case GTK_RESPONSE_CLOSE: /*closed the dialog box */
	default:               /*the sky falls on our heads */
		break;
	}

	gtk_widget_hide (GTK_WIDGET (dialog));
}

enum MlViewStatus
TreeView::reload_xml_document ()
{
	return MLVIEW_OK ;
}

enum MlViewStatus
TreeView::get_contextual_menu (GtkWidget **a_menu_ptr)
{
	GtkUIManager *ui_manager = NULL ;
	GtkWidget *tmp_widget = NULL, *menu = NULL ;

	ui_manager = get_ui_manager () ;
	THROW_IF_FAIL (ui_manager) ;

	tmp_widget = gtk_ui_manager_get_widget
	             (ui_manager, "/TreeViewPopupEditMenu/CommentNodeMenuitem") ;
	if (!tmp_widget) {
		build_contextual_menu2 () ;

	}
	menu = gtk_ui_manager_get_widget (ui_manager,
	                                  "/TreeViewPopupEditMenu") ;
	THROW_IF_FAIL (menu) ;
	gtk_widget_show_all (menu) ;

	activate_or_deactivate_proper_menu_items2 ("/TreeViewPopupEditMenu") ;
	*a_menu_ptr = menu ;
	if (!*a_menu_ptr)
		return MLVIEW_ERROR ;

	return MLVIEW_OK ;
}

enum MlViewStatus
TreeView::get_edit_menu_for_application (GtkWidget **a_menu_ptr)
{
	GtkUIManager *ui_manager = NULL ;
	GtkWidget *tmp_widget = NULL, *menu = NULL ;

	ui_manager = get_ui_manager () ;
	THROW_IF_FAIL (ui_manager) ;

	tmp_widget = gtk_ui_manager_get_widget
	             (ui_manager, "/MainMenubar/EditMenu/CommentNodeMenuitem") ;
	if (!tmp_widget) {
		build_app_edit_menu () ;

	}
	menu = gtk_ui_manager_get_widget (ui_manager,
	                                  "/MainMenubar/EditMenu") ;
	THROW_IF_FAIL (menu) ;
	gtk_widget_show_all (menu) ;

	activate_or_deactivate_proper_menu_items2 ("/MainMenubar/EditMenu") ;
	*a_menu_ptr = menu ;
	if (!*a_menu_ptr)
		return MLVIEW_ERROR ;

	return MLVIEW_OK ;
}

void
update_contextual_menu (GtkMenu ** a_menu_ptr)
{}

enum MlViewStatus
TreeView::focus_on_node_editor ()
{
	THROW_IF_FAIL (m_priv) ;
	THROW_IF_FAIL (m_priv->node_editor) ;

	mlview_node_editor_grab_focus
	(m_priv->node_editor) ;
	return MLVIEW_OK ;
}

enum MlViewStatus
TreeView::focus_on_tree_editor ()
{
	return MLVIEW_OK ;
}

void
TreeView::select_parent_node ()
{
	MlViewTreeEditor *tree_editor = NULL ;

	tree_editor = get_current_tree_editor () ;
	THROW_IF_FAIL (tree_editor) ;
	mlview_tree_editor_select_parent_node (tree_editor) ;
}

void
TreeView::select_prev_sibling_node ()
{
	MlViewTreeEditor *tree_editor = NULL ;

	tree_editor = get_current_tree_editor () ;
	THROW_IF_FAIL (tree_editor) ;
	mlview_tree_editor_select_prev_sibling_node (tree_editor) ;
}

void
TreeView::select_next_sibling_node ()
{
	MlViewTreeEditor *tree_editor = NULL ;

	tree_editor = get_current_tree_editor () ;
	THROW_IF_FAIL (tree_editor) ;
	mlview_tree_editor_select_next_sibling_node (tree_editor) ;
}

GtkUIManager *
TreeView::get_ui_manager ()
{
	gchar *file_path = NULL ;
	GtkActionGroup *action_group = NULL ;

	AppContext *context = AppContext::get_instance () ;
	THROW_IF_FAIL (context) ;
	if (!m_priv->ui_manager) {
		m_priv->ui_manager = (GtkUIManager*) context->get_element
														("MlViewUIManager") ;
		THROW_IF_FAIL (m_priv->ui_manager) ;

		action_group = gtk_action_group_new ("TreeViewEditMenuActions") ;
		gtk_action_group_set_translation_domain (action_group, GETTEXT_PACKAGE) ;
		gtk_action_group_add_actions (action_group, gv_edit_menu_actions,
		                              sizeof (gv_edit_menu_actions)/sizeof (GtkActionEntry),
		                              this) ;
		m_priv->action_group = action_group ;
		gtk_ui_manager_insert_action_group (m_priv->ui_manager,
		                                    action_group, 0) ;
		file_path = mlview_utils_locate_file ("tree-view-edit-menu.xml") ;
		THROW_IF_FAIL (file_path) ;
		m_priv->tree_view_popup_edit_menu_merge_id =
		    gtk_ui_manager_add_ui_from_file (m_priv->ui_manager,
		                                     file_path, 0) ;
		if (file_path) {
			g_free (file_path)  ;
			file_path = NULL ;
		}
		THROW_IF_FAIL (m_priv->tree_view_popup_edit_menu_merge_id) ;
	}
	return m_priv->ui_manager ;
}

//********************
//signal handlers
//********************

sigc::signal0<void>&
TreeView::signal_document_changed ()
{
	return m_priv->signal_document_changed ;
}

void
TreeView::on_realized ()
{
	//nothing to do here for the moment
}

void
TreeView::on_is_swapped_in ()
{
	//put in here stuffs to edit the application menu to get
	//out stuffs in there
}

void
TreeView::on_is_swapped_out ()
{

	mlview::AppContext *context = AppContext::get_instance () ;
	THROW_IF_FAIL (context) ;

	GtkUIManager *ui_manager = (GtkUIManager*)context->get_element 
											("MlViewUIManager") ;
	THROW_IF_FAIL (ui_manager) ;

	if (m_priv->edit_menu_merge_id) {
		gtk_ui_manager_remove_ui (ui_manager,
		                          m_priv->edit_menu_merge_id)  ;
		m_priv->edit_menu_merge_id = 0;
	}
	clear_completion_popup_submenus () ;
}

void
TreeView::on_contextual_menu_requested (GtkWidget *a_source_widget,
										GdkEvent *a_event)
{
	THROW_IF_FAIL (a_source_widget) ;
	THROW_IF_FAIL (a_event) ;

	handle_contextual_menu_request (a_source_widget, a_event) ;
}

void
TreeView::on_view_swapped (mlview::Object *a_old_view, 
						   mlview::Object* a_new_view)
{
}

//************************
//protected methods
//************************

GtkDialog*
TreeView::get_expand_tree_dialog ()
{
	GtkWidget *table = NULL,
	                   *label = NULL,
	                            *depth_entry = NULL;
	GtkWidget *expand_to_leaves = NULL;

	THROW_IF_FAIL (m_priv);

	if (m_priv->expand_tree_dialog == NULL) {
		m_priv->expand_tree_dialog =
		    GTK_DIALOG
		    (gtk_dialog_new_with_buttons
		     (_("Choose the depth of the tree expansion"),
		      NULL, GTK_DIALOG_MODAL,
		      GTK_STOCK_CANCEL, GTK_RESPONSE_REJECT,
		      GTK_STOCK_OK,GTK_RESPONSE_ACCEPT,
		      NULL));
		THROW_IF_FAIL (m_priv->expand_tree_dialog) ;
		gtk_dialog_set_default_response
		(GTK_DIALOG
		 (m_priv->expand_tree_dialog),
		 GTK_RESPONSE_ACCEPT) ;
		expand_to_leaves =
		    gtk_check_button_new_with_label
		    (_("expand to leaves"));

		gtk_box_pack_start_defaults
		(GTK_BOX
		 (m_priv->expand_tree_dialog->vbox), expand_to_leaves);

		label = gtk_label_new (_("absolute expansion depth:"));
		depth_entry = gtk_entry_new ();
		gtk_entry_set_text (GTK_ENTRY (depth_entry),
		                    "2");

		table = gtk_table_new (1, 2, FALSE);
		gtk_table_attach_defaults (GTK_TABLE (table),
		                           label, 0, 1, 0, 1);
		gtk_table_attach_defaults (GTK_TABLE (table),
		                           depth_entry, 1, 2, 0,
		                           1);

		gtk_box_pack_start_defaults
		(GTK_BOX
		 (m_priv->expand_tree_dialog->vbox), table);

		g_signal_connect (G_OBJECT (expand_to_leaves),
		                  "toggled",
		                  G_CALLBACK
		                  (toggle_expand_to_leaves_cb),
		                  depth_entry);

		gtk_widget_show_all
		(m_priv->expand_tree_dialog->vbox);

		gtk_object_set_data
		(GTK_OBJECT
		 (m_priv->expand_tree_dialog),
		 "expand-to-leaves", expand_to_leaves);

		gtk_object_set_data
		(GTK_OBJECT (m_priv->expand_tree_dialog),
		 "depth-entry", depth_entry);
	}
	return m_priv->expand_tree_dialog ;
}
const UString
TreeView::build_edit_menu_root_path (bool a_popup)
{
	gchar *menu_root_path = NULL ;

	if (a_popup == TRUE) {
		menu_root_path = (gchar*)"/TreeViewPopupEditMenu" ;
	} else {
		menu_root_path = (gchar*)"/MainMenubar/EditMenu" ;
	}
	return menu_root_path ;
}

enum MlViewStatus
TreeView::build_edit_menu_body (const UString &a_menu_root_path)
{
	guint *merge_id = NULL ;

	gchar * parent_menu_path = NULL ;
	GtkUIManager *ui_manager = NULL ;

	ui_manager = get_ui_manager () ;
	THROW_IF_FAIL (ui_manager) ;

	if (a_menu_root_path == "/MainMenubar/EditMenu") {
		if (!m_priv->edit_menu_merge_id ) {
			m_priv->edit_menu_merge_id =
			    gtk_ui_manager_new_merge_id (ui_manager) ;
		}
		merge_id = &m_priv->edit_menu_merge_id ;
	} else if (a_menu_root_path == "/TreeViewPopupEditMenu") {
		merge_id = &m_priv->tree_view_popup_edit_menu_merge_id ;
	} else {
		mlview_utils_trace_debug ("Unknown menu root path:") ;
		mlview_utils_trace_debug (a_menu_root_path.c_str ()) ;
		return MLVIEW_ERROR ;
	}

	/*
	 *build the contextual menu in here !
	 */
	gtk_ui_manager_add_ui (ui_manager,
	                       *merge_id,
	                       a_menu_root_path.c_str (),
	                       "CommentNodeMenuitem",
	                       "CommentNodeAction",
	                       GTK_UI_MANAGER_AUTO,
	                       FALSE) ;

	gtk_ui_manager_add_ui (ui_manager,
	                       *merge_id,
	                       a_menu_root_path.c_str (),
	                       "UncommentNodeMenuitem",
	                       "UncommentNodeAction",
	                       GTK_UI_MANAGER_AUTO,
	                       FALSE) ;

	gtk_ui_manager_add_ui (ui_manager,
	                       *merge_id,
	                       a_menu_root_path.c_str (),
	                       "TreeViewEditMenuSeparator0",
	                       NULL,
	                       GTK_UI_MANAGER_SEPARATOR,
	                       FALSE) ;

	gtk_ui_manager_add_ui (ui_manager,
	                       *merge_id,
	                       a_menu_root_path.c_str (),
	                       "AddChildNodeMenu",
	                       "AddChildNodeMenuAction",
	                       GTK_UI_MANAGER_MENU,
	                       FALSE) ;

	parent_menu_path = g_strjoin ("/",
	                              a_menu_root_path.c_str (),
	                              "AddChildNodeMenu",
	                              NULL) ;
	THROW_IF_FAIL (parent_menu_path) ;

	gtk_ui_manager_add_ui (ui_manager,
	                       *merge_id,
	                       parent_menu_path,
	                       "AddChildNodeMenuitem",
	                       "AddChildNodeAction",
	                       GTK_UI_MANAGER_AUTO,
	                       FALSE) ;
	if (parent_menu_path) {
		g_free (parent_menu_path) ;
		parent_menu_path = NULL ;
	}

	gtk_ui_manager_add_ui (ui_manager,
	                       *merge_id,
	                       a_menu_root_path.c_str (),
	                       "InsertNextSiblingNodeMenu",
	                       "InsertNextSiblingNodeMenuAction",
	                       GTK_UI_MANAGER_MENU,
	                       FALSE) ;
	parent_menu_path = g_strjoin ("/",
	                              a_menu_root_path.c_str (),
	                              "InsertNextSiblingNodeMenu",
	                              NULL) ;
	THROW_IF_FAIL (parent_menu_path) ;

	gtk_ui_manager_add_ui (ui_manager,
	                       *merge_id,
	                       parent_menu_path,
	                       "InsertNextSiblingNodeMenuitem",
	                       "InsertNextSiblingNodeAction",
	                       GTK_UI_MANAGER_AUTO,
	                       FALSE) ;
	if (parent_menu_path) {
		g_free (parent_menu_path) ;
		parent_menu_path = NULL ;
	}


	gtk_ui_manager_add_ui (ui_manager,
	                       *merge_id,
	                       a_menu_root_path.c_str (),
	                       "InsertPrevSiblingNodeMenu",
	                       "InsertPrevSiblingNodeMenuAction",
	                       GTK_UI_MANAGER_MENU,
	                       FALSE) ;

	parent_menu_path = g_strjoin ("/",
	                              a_menu_root_path.c_str (),
	                              "InsertPrevSiblingNodeMenu",
	                              NULL) ;
	THROW_IF_FAIL (parent_menu_path) ;

	gtk_ui_manager_add_ui (ui_manager,
	                       *merge_id,
	                       parent_menu_path,
	                       "InsertPrevSiblingNodeMenuitem",
	                       "InsertPrevSiblingNodeAction",
	                       GTK_UI_MANAGER_AUTO,
	                       FALSE) ;
	if (parent_menu_path) {
		g_free (parent_menu_path) ;
		parent_menu_path = NULL ;
	}

	gtk_ui_manager_add_ui (ui_manager,
	                       *merge_id,
	                       a_menu_root_path.c_str (),
	                       "TreeViewEditMenuSeparator1",
	                       NULL,
	                       GTK_UI_MANAGER_SEPARATOR,
	                       FALSE) ;

	gtk_ui_manager_add_ui (ui_manager,
	                       *merge_id,
	                       a_menu_root_path.c_str (),
	                       "CopyNodeMenuitem",
	                       "CopyNodeAction",
	                       GTK_UI_MANAGER_AUTO,
	                       FALSE) ;


	gtk_ui_manager_add_ui (ui_manager,
	                       *merge_id,
	                       a_menu_root_path.c_str (),
	                       "CutNodeMenuitem",
	                       "CutNodeAction",
	                       GTK_UI_MANAGER_AUTO,
	                       FALSE) ;

	gtk_ui_manager_add_ui (ui_manager,
	                       *merge_id,
	                       a_menu_root_path.c_str (),
	                       "TreeViewEditMenuSeparator2",
	                       NULL,
	                       GTK_UI_MANAGER_SEPARATOR,
	                       FALSE) ;

	gtk_ui_manager_add_ui (ui_manager,
	                       *merge_id,
	                       a_menu_root_path.c_str (),
	                       "PasteNodeAsChildMenuitem",
	                       "PasteNodeAsChildAction",
	                       GTK_UI_MANAGER_AUTO,
	                       FALSE) ;

	gtk_ui_manager_add_ui (ui_manager,
	                       *merge_id,
	                       a_menu_root_path.c_str (),
	                       "PasteNodeAsPrevMenuitem",
	                       "PasteNodeAsPrevAction",
	                       GTK_UI_MANAGER_AUTO,
	                       FALSE) ;

	gtk_ui_manager_add_ui (ui_manager,
	                       *merge_id,
	                       a_menu_root_path.c_str (),
	                       "PasteNodeAsNextMenuitem",
	                       "PasteNodeAsNextAction",
	                       GTK_UI_MANAGER_AUTO,
	                       FALSE) ;

	gtk_ui_manager_add_ui (ui_manager,
	                       *merge_id,
	                       a_menu_root_path.c_str (),
	                       "TreeViewEditMenuSeparator3",
	                       NULL,
	                       GTK_UI_MANAGER_SEPARATOR,
	                       FALSE) ;

	gtk_ui_manager_add_ui (ui_manager,
	                       *merge_id,
	                       a_menu_root_path.c_str (),
	                       "SelectNextSiblingNodeMenuitem",
	                       "SelectNextSiblingAction",
	                       GTK_UI_MANAGER_AUTO,
	                       FALSE) ;

	gtk_ui_manager_add_ui (ui_manager,
	                       *merge_id,
	                       a_menu_root_path.c_str (),
	                       "SelectPrevSiblingNodeMenuitem",
	                       "SelectPrevSiblingAction",
	                       GTK_UI_MANAGER_AUTO,
	                       FALSE) ;

	gtk_ui_manager_add_ui (ui_manager,
	                       *merge_id,
	                       a_menu_root_path.c_str (),
	                       "TreeViewEditMenuSeparator4",
	                       NULL,
	                       GTK_UI_MANAGER_SEPARATOR,
	                       FALSE) ;

	gtk_ui_manager_add_ui (ui_manager,
	                       *merge_id,
	                       a_menu_root_path.c_str (),
	                       "SelectParentNodeMenuitem",
	                       "SelectParentNodeAction",
	                       GTK_UI_MANAGER_AUTO,
	                       FALSE) ;

	gtk_ui_manager_add_ui (ui_manager,
	                       *merge_id,
	                       a_menu_root_path.c_str (),
	                       "TreeViewEditMenuSeparator5",
	                       NULL,
	                       GTK_UI_MANAGER_SEPARATOR,
	                       FALSE) ;

	gtk_ui_manager_add_ui (ui_manager,
	                       *merge_id,
	                       a_menu_root_path.c_str (),
	                       "FindNodeMenuitem",
	                       "FindNodeAction",
	                       GTK_UI_MANAGER_AUTO,
	                       FALSE) ;

	gtk_ui_manager_ensure_update (ui_manager) ;
	return MLVIEW_OK ;
}

enum MlViewStatus
TreeView::build_contextual_menu2 ()
{
	enum MlViewStatus status = MLVIEW_OK ;

	UString menu_root_path = build_edit_menu_root_path (TRUE) ;
	status = build_edit_menu_body (menu_root_path) ;
	THROW_IF_FAIL (status == MLVIEW_OK) ;
	return MLVIEW_OK ;
}

enum MlViewStatus
TreeView::build_app_edit_menu ()
{
	UString menu_root_path = build_edit_menu_root_path (FALSE) ;
	THROW_IF_FAIL (menu_root_path != "") ;
	MlViewStatus status = build_edit_menu_body (menu_root_path) ;
	THROW_IF_FAIL (status == MLVIEW_OK) ;
	return MLVIEW_OK ;
}

void
TreeView::clear_completion_popup_submenus ()
{
	GtkUIManager *ui_manager = NULL ;

	ui_manager = get_ui_manager () ;
	THROW_IF_FAIL (ui_manager) ;

	gtk_ui_manager_remove_ui (ui_manager,
	                          m_priv->add_child_element_merge_id) ;
	m_priv->add_child_element_merge_id= 0 ;
	gtk_ui_manager_remove_ui (ui_manager,
	                          m_priv->insert_next_element_merge_id) ;
	m_priv->insert_next_element_merge_id = 0 ;
	gtk_ui_manager_remove_ui (ui_manager,
	                          m_priv->insert_prev_element_merge_id) ;
	m_priv->insert_prev_element_merge_id = 0 ;
	gtk_ui_manager_ensure_update (ui_manager) ;
}

void
TreeView::update_completion_popup_submenu2
				(const UString &a_menu_root_path,
				 xmlNodePtr a_node,
				 enum NODE_INSERTION_SCHEME a_insertion_scheme)
{
	GtkAction *action = NULL ;
	GtkUIManager *ui_manager = NULL ;
	GtkActionGroup *action_group = NULL ;
	GList *children_name_list = NULL ;
	gint nb_of_names = 0;
	gchar *cur_name = NULL ;
	GList *cur = NULL ;
	guint *merge_id_ptr = 0 ;
	gchar *menu_path = NULL, *action_name_prefix = NULL, *action_name = NULL ;

	THROW_IF_FAIL (a_node && a_node->doc && a_node->doc->extSubset) ;
	THROW_IF_FAIL (m_priv) ;

	nb_of_names =
	    mlview_parsing_utils_build_element_name_completion_list
			(a_insertion_scheme, a_node, &children_name_list);

	if (nb_of_names <= 0)
		return ;

	ui_manager = get_ui_manager () ;
	THROW_IF_FAIL (ui_manager) ;

	if (!m_priv->add_child_element_action_group) {
		m_priv->add_child_element_action_group =
		    gtk_action_group_new ("AddChildElementActionsGroup") ;
		gtk_action_group_set_translation_domain (m_priv->add_child_element_action_group,
		        GETTEXT_PACKAGE) ;
		THROW_IF_FAIL (m_priv->add_child_element_action_group) ;
		gtk_ui_manager_insert_action_group (ui_manager,
		                                    m_priv->add_child_element_action_group,
		                                    0) ;
	}
	if (!m_priv->add_child_element_merge_id) {
		m_priv->add_child_element_merge_id =
		    gtk_ui_manager_new_merge_id (ui_manager) ;
	}
	if (!m_priv->insert_next_element_merge_id) {
		m_priv->insert_next_element_merge_id =
		    gtk_ui_manager_new_merge_id (ui_manager) ;
	}
	if (!m_priv->insert_prev_element_merge_id) {
		m_priv->insert_prev_element_merge_id =
		    gtk_ui_manager_new_merge_id (ui_manager) ;
	}
	if (!m_priv->insert_prev_element_action_group) {
		m_priv->insert_prev_element_action_group =
		    gtk_action_group_new ("InsertNextElementActionsGroup") ;
		gtk_action_group_set_translation_domain (m_priv->insert_prev_element_action_group,
		        GETTEXT_PACKAGE) ;
		THROW_IF_FAIL (m_priv->insert_prev_element_action_group) ;
		gtk_ui_manager_insert_action_group (ui_manager,
		                                    m_priv->insert_prev_element_action_group,
		                                    0) ;
	}
	if (!m_priv->insert_next_element_action_group) {
		m_priv->insert_next_element_action_group =
		    gtk_action_group_new ("InsertNextElementActionsGroup") ;
		gtk_action_group_set_translation_domain (m_priv->insert_next_element_action_group,
		        GETTEXT_PACKAGE) ;
		THROW_IF_FAIL (m_priv->insert_prev_element_action_group) ;
		gtk_ui_manager_insert_action_group (ui_manager,
		                                    m_priv->insert_next_element_action_group,
		                                    0) ;
	}


	switch (a_insertion_scheme) {
	case ADD_CHILD:
		action_name_prefix = (gchar*) "AddElementAction" ;
		merge_id_ptr = &m_priv->add_child_element_merge_id ;
		menu_path = g_strjoin ("/",
		                       a_menu_root_path.c_str (),
		                       "AddChildNodeMenu",
		                       NULL) ;
		THROW_IF_FAIL (menu_path) ;
		action_group = m_priv->add_child_element_action_group ;
		break ;
	case INSERT_BEFORE:
		action_name_prefix = (gchar*) "InsertPrevSiblingElementAction" ;
		merge_id_ptr = &m_priv->insert_prev_element_merge_id ;
		menu_path = g_strjoin ("/",
		                       a_menu_root_path.c_str (),
		                       "InsertPrevSiblingNodeMenu",
		                       NULL) ;
		THROW_IF_FAIL (menu_path) ;
		action_group = m_priv->insert_prev_element_action_group ;
		break ;
	case INSERT_AFTER:
		action_name_prefix = (gchar*) "InsertNextSiblingElementAction" ;
		merge_id_ptr = &m_priv->insert_next_element_merge_id ;
		menu_path = g_strjoin ("/",
		                       a_menu_root_path.c_str (),
		                       "InsertNextSiblingNodeMenu",
		                       NULL) ;
		THROW_IF_FAIL (menu_path) ;
		action_group = m_priv->insert_next_element_action_group ;
		break ;
	case CHANGE_CUR_ELEMENT_NAME:
		g_assert_not_reached () ;
		break ;
	default:
		break ;
	}

	cur = children_name_list ;
	cur_name = (gchar*) cur->data ;
	while (cur_name) {
		action_name = mlview_utils_get_unique_string
		              (action_name_prefix) ;
		action = gtk_action_new (action_name,
		                         cur_name,
		                         NULL,
		                         NULL) ;
		g_signal_connect (G_OBJECT (action),
		                  "activate",
		                  G_CALLBACK (insert_element_action_cb),
		                  this) ;
		gtk_action_group_add_action (action_group,
		                             action) ;
		gtk_ui_manager_add_ui (ui_manager,
		                       *merge_id_ptr,
		                       menu_path,
		                       cur_name,
		                       action_name,
		                       GTK_UI_MANAGER_MENUITEM,
		                       FALSE) ;

		if (action_name) {
			g_free (action_name) ;
			action_name = NULL ;
		}
		cur = (cur->next) ? cur->next : NULL;
		cur_name = (cur) ? (gchar *) cur->data : NULL;
	}

	if (menu_path) {
		g_free (menu_path) ;
		menu_path = NULL ;
	}
}

enum MlViewStatus
TreeView::handle_contextual_menu_request (GtkWidget *a_source_widget,
        GdkEvent *a_event)
{
	//TODO: finish porting this "
	GtkWidget *menu = NULL ;
	enum MlViewStatus status = MLVIEW_OK ;
	GdkEventButton *event_button = NULL;

	THROW_IF_FAIL (a_source_widget && GTK_IS_WIDGET (a_source_widget)) ;
	THROW_IF_FAIL (a_event) ;

	if (a_source_widget
	        == GTK_WIDGET (m_priv->node_editor)
	        || a_source_widget
	        == GTK_WIDGET (m_priv->current_tree_editor)) {

		if (a_event->type == GDK_BUTTON_PRESS) {
			event_button = (GdkEventButton*)a_event ;

			status = get_contextual_menu (&menu) ;
			THROW_IF_FAIL (status == MLVIEW_OK
			               && menu
			               && GTK_IS_MENU (menu)) ;

			AppContext *ctxt = AppContext::get_instance () ;
			THROW_IF_FAIL (ctxt) ;

			gtk_menu_popup (GTK_MENU (menu), NULL, NULL, NULL,
			                ctxt, event_button->button,
			                event_button->time) ;
		}

	}
	return MLVIEW_OK ;
}

enum MlViewStatus
TreeView::execute_action (MlViewAction *a_action)
{
	THROW_IF_FAIL (m_priv) ;
	THROW_IF_FAIL (a_action) ;

	if (!strcmp
	        ((const char*)a_action->name,
	         "add-child-node-interactive")) {
		add_child_node_interactive () ;
	} else if (!strcmp
	           ((const char*)a_action->name,
	            "insert-prev-sibling-node-interactive") ) {
		insert_prev_sibling_node_interactive () ;
	} else if (!strcmp
	           ((const char*)a_action->name,
	            "insert-next-sibling-node-interactive") ) {
		insert_next_sibling_node_interactive () ;
	} else if (!strcmp ((const char*)a_action->name,
	                    "cut-node") ) {
		cut_node () ;
	} else if (!strcmp ((const char*)a_action->name,
	                    "copy-node") ) {
		copy_node () ;
	} else if (!strcmp ((const char*)a_action->name,
	                    "paste-node-as-child") ) {
		paste_node_as_child () ;
	} else if (!strcmp ((const char*)a_action->name,
	                    "paste-node-as-prev-sibling") ) {
		paste_node_as_prev_sibling () ;
	} else if (!strcmp ((const char*)a_action->name,
	                    "paste-node-as-next-sibling") ) {
		paste_node_as_next_sibling () ;
	} else if (!strcmp ((const char*)a_action->name,
	                    "expand-tree-to-depth-interactive") ) {
		expand_tree_to_depth_interactive () ;
	} else if (!strcmp ((const char*)a_action->name,
	                    "find-node-that-contains-str-interactive") ) {
		find_xml_node_that_contains_str_interactive () ;
	} else if (!strcmp ((const char*)a_action->name,
	                    "create-internal-subset-node-interactive")) {
		create_internal_subset_node_interactive () ;
	} else if (!strcmp ((const char*)a_action->name,
	                    "comment-current-node")) {
		comment_current_node () ;
	} else if (!strcmp ((const char*)a_action->name,
	                    "uncomment-current-node")) {
		uncomment_current_node () ;
	} else if (!strcmp ((const char*)a_action->name,
	                    "select-parent-node")) {
		select_parent_node () ;
	} else if (!strcmp ((const char*)a_action->name,
	                    "select-prev-sibling-node")) {
		select_prev_sibling_node () ;
	} else if (!strcmp ((const char*)a_action->name,
	                    "select-next-sibling-node")) {
		select_next_sibling_node () ;
	} else  {
		gchar *err_msg = NULL ;
		g_strconcat ("Unknown edition action: ",
		             a_action->name,
		             NULL) ;
		mlview_utils_trace_debug ((const gchar*)err_msg) ;
		if (err_msg) {
			g_free (err_msg) ;
			err_msg = NULL ;
		}

	}
	return MLVIEW_OK ;
}

bool
TreeView::get_must_rebuild_upon_document_reload ()
{
	return true ;
}

enum MlViewStatus
TreeView::connect_to_doc (MlViewXMLDocument *a_doc)
{
	MlViewTreeEditor *tree_editor = NULL ;

	THROW_IF_FAIL (a_doc && MLVIEW_IS_XML_DOCUMENT (a_doc)) ;

	Gtk::Notebook_Helpers::PageList pages = m_priv->tree_editors->pages () ;
	Gtk::Notebook_Helpers::PageList::iterator it ;
	for (it = pages.begin () ; it != pages.end (); ++it) {
		tree_editor = MLVIEW_TREE_EDITOR (it->get_child ()->gobj ()) ;
		THROW_IF_FAIL (tree_editor) ;
		mlview_tree_editor_connect_to_doc (tree_editor, a_doc);
	}
	mlview_node_editor_connect_to_doc (m_priv->node_editor, a_doc) ;
	return MLVIEW_OK ;
}

enum MlViewStatus
TreeView::disconnect_from_doc (MlViewXMLDocument *a_doc)

{
	MlViewTreeEditor *tree_editor = NULL;

	THROW_IF_FAIL (a_doc && MLVIEW_IS_XML_DOCUMENT (a_doc)) ;
	THROW_IF_FAIL (m_priv) ;
	THROW_IF_FAIL (m_priv->tree_editors) ;

	Gtk::Notebook_Helpers::PageList pages = m_priv->tree_editors->pages () ;
	Gtk::Notebook_Helpers::PageList::iterator it ;
	for (it = pages.begin () ; it != pages.end () ; ++it) {
		tree_editor = MLVIEW_TREE_EDITOR (it->get_child ()->gobj ()) ;
		THROW_IF_FAIL (tree_editor) ;
		mlview_tree_editor_disconnect_from_doc (tree_editor, a_doc);
	}

	mlview_node_editor_disconnect_from_doc (m_priv->node_editor, a_doc) ;

	g_signal_handlers_disconnect_by_func
	(G_OBJECT (a_doc),
	 (void*)update_completion_widget_cb,
	 m_priv->completion_widget);

	g_signal_handlers_disconnect_by_func (G_OBJECT (a_doc),
	                                      (void*)doc_path_changed_cb,
	                                      this);

	return MLVIEW_OK ;
}

enum MlViewStatus
TreeView::activate_or_deactivate_proper_menu_items2 (const gchar *a_menu_root_path)
{
	GtkUIManager *ui_manager = NULL ;
	GtkAction *action = NULL ;
	gchar *menu_root_path = NULL ;

	xmlNode *cur_node = NULL, *root_element = NULL ;

	THROW_IF_FAIL (m_priv) ;
	THROW_IF_FAIL (m_priv->current_tree_editor) ;
	AppContext *context = AppContext::get_instance () ;
	THROW_IF_FAIL (context) ;

	mlview::PrefsCategoryGeneral *prefs =
	    dynamic_cast<PrefsCategoryGeneral*> (
		mlview::Preferences::get_instance ()
		->get_category_by_id (mlview::PrefsCategoryGeneral::CATEGORY_ID));

	cur_node = mlview_tree_editor_get_cur_sel_xml_node
	    (m_priv->current_tree_editor) ;
	if (!cur_node)
	    return MLVIEW_ERROR ;

	ui_manager = get_ui_manager () ;
	THROW_IF_FAIL (ui_manager) ;

	mlview_xml_document_get_root_element (get_document (), &root_element) ;

	clear_completion_popup_submenus () ;
	if (cur_node->type == XML_ELEMENT_NODE &&
	    prefs->use_validation () &&
	    cur_node->doc->extSubset) {
		update_completion_popup_submenu2 (a_menu_root_path,
		                                  cur_node,
		                                  ADD_CHILD) ;

		update_completion_popup_submenu2 (a_menu_root_path,
		                                  cur_node,
		                                  INSERT_AFTER) ;

		update_completion_popup_submenu2 (a_menu_root_path,
		                                  cur_node,
		                                  INSERT_BEFORE) ;
	} else {}

	switch (cur_node->type) {
	case XML_DOCUMENT_NODE:

		menu_root_path = g_strjoin ("/",
		                            a_menu_root_path,
		                            "CommentNodeMenuitem",
		                            NULL) ;
		action = gtk_ui_manager_get_action
		         (ui_manager,
		          menu_root_path) ;
		THROW_IF_FAIL (action ) ;
		g_object_set (G_OBJECT (action),
		              "sensitive", FALSE,
		              NULL) ;
		if (menu_root_path) {
			g_free (menu_root_path) ;
			menu_root_path = NULL ;
		}

		menu_root_path = g_strjoin ("/",
		                            a_menu_root_path,
		                            "UncommentNodeMenuitem",
		                            NULL) ;
		action = gtk_ui_manager_get_action
		         (ui_manager,
		          menu_root_path) ;
		THROW_IF_FAIL (action) ;
		g_object_set (G_OBJECT (action),
		              "sensitive", FALSE,
		              NULL) ;
		if (menu_root_path) {
			g_free (menu_root_path) ;
			menu_root_path = NULL ;
		}

		menu_root_path = g_strjoin ("/",
		                            a_menu_root_path,
		                            "InsertNextSiblingNodeMenu",
		                            NULL) ;
		action = gtk_ui_manager_get_action
		         (ui_manager,
		          menu_root_path) ;
		THROW_IF_FAIL (action) ;
		g_object_set (G_OBJECT (action),
		              "sensitive", FALSE,
		              NULL) ;
		if (menu_root_path) {
			g_free (menu_root_path) ;
			menu_root_path = NULL ;
		}


		menu_root_path = g_strjoin ("/",
		                            a_menu_root_path,
		                            "InsertPrevSiblingNodeMenu",
		                            NULL) ;
		action = gtk_ui_manager_get_action
		         (ui_manager,
		          menu_root_path) ;
		THROW_IF_FAIL (action) ;
		g_object_set (G_OBJECT (action),
		              "sensitive", FALSE,
		              NULL) ;
		if (menu_root_path) {
			g_free (menu_root_path) ;
			menu_root_path = NULL ;
		}


		menu_root_path = g_strjoin ("/",
		                            a_menu_root_path,
		                            "CutNodeMenuitem",
		                            NULL) ;
		action = gtk_ui_manager_get_action
		         (ui_manager,
		          menu_root_path) ;
		if (menu_root_path) {
			g_free (menu_root_path) ;
			menu_root_path = NULL ;
		}
		THROW_IF_FAIL (action) ;

		g_object_set (G_OBJECT (action),
		              "sensitive", FALSE,
		              NULL) ;


		menu_root_path = g_strjoin ("/",
		                            a_menu_root_path,
		                            "PasteNodeAsChildMenuitem",
		                            NULL) ;
		action = gtk_ui_manager_get_action
		         (ui_manager,
		          menu_root_path) ;
		THROW_IF_FAIL (action) ;
		if (menu_root_path) {
			g_free (menu_root_path) ;
			menu_root_path = NULL ;
		}
		g_object_set (G_OBJECT (action),
		              "sensitive", FALSE,
		              NULL) ;

		menu_root_path = g_strjoin ("/",
		                            a_menu_root_path,
		                            "PasteNodeAsPrevMenuitem",
		                            NULL) ;
		action = gtk_ui_manager_get_action
		         (ui_manager,
		          menu_root_path) ;
		THROW_IF_FAIL (action) ;
		if (menu_root_path) {
			g_free (menu_root_path) ;
			menu_root_path = NULL ;
		}
		g_object_set (G_OBJECT (action),
		              "sensitive", FALSE,
		              NULL) ;

		menu_root_path = g_strjoin ("/",
		                            a_menu_root_path,
		                            "PasteNodeAsNextMenuitem",
		                            NULL) ;
		action = gtk_ui_manager_get_action
		         (ui_manager,
		          menu_root_path) ;
		THROW_IF_FAIL (action) ;
		if (menu_root_path) {
			g_free (menu_root_path) ;
			menu_root_path = NULL ;
		}
		g_object_set (G_OBJECT (action),
		              "sensitive", FALSE,
		              NULL) ;

		menu_root_path = g_strjoin ("/",
		                            a_menu_root_path,
		                            "CopyNodeMenuitem",
		                            NULL) ;
		action = gtk_ui_manager_get_action
		         (ui_manager,
		          menu_root_path) ;
		THROW_IF_FAIL (action) ;
		if (menu_root_path) {
			g_free (menu_root_path) ;
			menu_root_path = NULL ;
		}
		g_object_set (G_OBJECT (action),
		              "sensitive", FALSE,
		              NULL) ;


		menu_root_path = g_strjoin ("/",
		                            a_menu_root_path,
		                            "SelectNextSiblingNodeMenuitem",
		                            NULL) ;
		action = gtk_ui_manager_get_action
		         (ui_manager,
		          menu_root_path) ;
		THROW_IF_FAIL (action) ;
		if (menu_root_path) {
			g_free (menu_root_path) ;
			menu_root_path = NULL ;
		}
		g_object_set (G_OBJECT (action),
		              "sensitive", FALSE,
		              NULL) ;

		menu_root_path = g_strjoin ("/",
		                            a_menu_root_path,
		                            "SelectParentNodeMenuitem",
		                            NULL) ;
		action = gtk_ui_manager_get_action
		         (ui_manager,
		          menu_root_path) ;
		THROW_IF_FAIL (action) ;
		if (menu_root_path) {
			g_free (menu_root_path) ;
			menu_root_path = NULL ;
		}
		g_object_set (G_OBJECT (action),
		              "sensitive", FALSE,
		              NULL) ;

		menu_root_path = g_strjoin ("/",
		                            a_menu_root_path,
		                            "SelectPrevSiblingNodeMenuitem",
		                            NULL) ;
		action = gtk_ui_manager_get_action
		         (ui_manager,
		          menu_root_path) ;
		THROW_IF_FAIL (action) ;
		if (menu_root_path) {
			g_free (menu_root_path) ;
			menu_root_path = NULL ;
		}
		g_object_set (G_OBJECT (action),
		              "sensitive", FALSE,
		              NULL) ;

		break ;

	case XML_ELEMENT_NODE:
	case XML_DTD_NODE:


		if (cur_node->type == XML_ELEMENT_NODE) {

			menu_root_path = g_strjoin ("/",
			                            a_menu_root_path,
			                            "CommentNodeMenuitem",
			                            NULL) ;
			action = gtk_ui_manager_get_action
			         (ui_manager,
			          menu_root_path) ;
			if (menu_root_path) {
				g_free (menu_root_path) ;
				menu_root_path = NULL ;
			}

			THROW_IF_FAIL (action) ;

			if (cur_node == root_element) {
				g_object_set (G_OBJECT (action),
				              "sensitive", FALSE,
				              NULL) ;
			} else {
				g_object_set (G_OBJECT (action),
				              "sensitive", TRUE,
				              NULL) ;
			}

			menu_root_path = g_strjoin ("/",
			                            a_menu_root_path,
			                            "UncommentNodeMenuitem",
			                            NULL) ;
			action = gtk_ui_manager_get_action
			         (ui_manager,
			          menu_root_path) ;
			THROW_IF_FAIL (action) ;

			g_object_set (G_OBJECT (action),
			              "sensitive", FALSE,
			              NULL) ;

			if (menu_root_path) {
				g_free (menu_root_path) ;
				menu_root_path = NULL ;
			}
		}
		menu_root_path = g_strjoin ("/",
		                            a_menu_root_path,
		                            "InsertNextSiblingNodeMenu",
		                            NULL) ;
		action = gtk_ui_manager_get_action
		         (ui_manager,
		          menu_root_path) ;
		if (menu_root_path) {
			g_free (menu_root_path) ;
			menu_root_path = NULL ;
		}
		THROW_IF_FAIL (action) ;
		if (cur_node == root_element) {
			g_object_set (G_OBJECT (action),
			              "sensitive", FALSE,
			              NULL) ;
		} else {
			g_object_set (G_OBJECT (action),
			              "sensitive", TRUE,
			              NULL) ;
		}


		menu_root_path = g_strjoin ("/",
		                            a_menu_root_path,
		                            "InsertPrevSiblingNodeMenu",
		                            NULL) ;
		action = gtk_ui_manager_get_action
		         (ui_manager,
		          menu_root_path) ;
		if (menu_root_path) {
			g_free (menu_root_path) ;
			menu_root_path = NULL ;
		}
		THROW_IF_FAIL (action) ;
		if (cur_node == root_element) {
			g_object_set (G_OBJECT (action),
			              "sensitive", FALSE,
			              NULL) ;
		} else {
			g_object_set (G_OBJECT (action),
			              "sensitive", TRUE,
			              NULL) ;
		}

		menu_root_path = g_strjoin ("/",
		                            a_menu_root_path,
		                            "CopyNodeMenuitem",
		                            NULL) ;
		action = gtk_ui_manager_get_action
		         (ui_manager,
		          menu_root_path) ;
		if (menu_root_path) {
			g_free (menu_root_path) ;
			menu_root_path = NULL ;
		}
		THROW_IF_FAIL (action) ;
		g_object_set (G_OBJECT (action),
		              "sensitive", TRUE,
		              NULL) ;

		menu_root_path = g_strjoin ("/",
		                            a_menu_root_path,
		                            "AddChildNodeMenu",
		                            NULL) ;
		action = gtk_ui_manager_get_action
		         (ui_manager,
		          menu_root_path) ;
		if (menu_root_path) {
			g_free (menu_root_path) ;
			menu_root_path = NULL ;
		}
		THROW_IF_FAIL (action) ;
		g_object_set (G_OBJECT (action),
		              "sensitive", TRUE,
		              NULL) ;

		menu_root_path = g_strjoin ("/",
		                            a_menu_root_path,
		                            "PasteNodeAsChildMenuitem",
		                            NULL) ;
		action = gtk_ui_manager_get_action
		         (ui_manager,
		          menu_root_path) ;
		if (menu_root_path) {
			g_free (menu_root_path) ;
			menu_root_path = NULL ;
		}
		THROW_IF_FAIL (action) ;
		g_object_set (G_OBJECT (action),
		              "sensitive", TRUE,
		              NULL) ;


		menu_root_path = g_strjoin ("/",
		                            a_menu_root_path,
		                            "PasteNodeAsPrevMenuitem",
		                            NULL) ;
		action = gtk_ui_manager_get_action
		         (ui_manager,
		          menu_root_path) ;
		if (menu_root_path) {
			g_free (menu_root_path) ;
			menu_root_path = NULL ;
		}
		THROW_IF_FAIL (action) ;

		if (cur_node == root_element) {
			g_object_set (G_OBJECT (action),
			              "sensitive", FALSE,
			              NULL) ;
		} else {
			g_object_set (G_OBJECT (action),
			              "sensitive", TRUE,
			              NULL) ;
		}

		menu_root_path = g_strjoin ("/",
		                            a_menu_root_path,
		                            "PasteNodeAsNextMenuitem",
		                            NULL) ;
		action = gtk_ui_manager_get_action
		         (ui_manager,
		          menu_root_path) ;
		THROW_IF_FAIL (action) ;
		if (cur_node == root_element) {
			g_object_set (G_OBJECT (action),
			              "sensitive", FALSE,
			              NULL) ;

		} else {
			g_object_set (G_OBJECT (action),
			              "sensitive", TRUE,
			              NULL) ;
		}

		menu_root_path = g_strjoin ("/",
		                            a_menu_root_path,
		                            "CopyNodeMenuitem",
		                            NULL) ;
		action = gtk_ui_manager_get_action
		         (ui_manager,
		          menu_root_path) ;
		if (menu_root_path) {
			g_free (menu_root_path) ;
			menu_root_path = NULL ;
		}
		THROW_IF_FAIL (action) ;
		g_object_set (G_OBJECT (action),
		              "sensitive", TRUE,
		              NULL) ;

		menu_root_path = g_strjoin ("/",
		                            a_menu_root_path,
		                            "SelectNextSiblingNodeMenuitem",
		                            NULL) ;
		action = gtk_ui_manager_get_action
		         (ui_manager,
		          menu_root_path) ;
		if (menu_root_path) {
			g_free (menu_root_path) ;
			menu_root_path = NULL ;
		}
		THROW_IF_FAIL (action) ;
		if (cur_node->next) {
			g_object_set (G_OBJECT (action),
			              "sensitive", TRUE,
			              NULL) ;
		} else {
			g_object_set (G_OBJECT (action),
			              "sensitive", FALSE,
			              NULL) ;
		}

		menu_root_path = g_strjoin ("/",
		                            a_menu_root_path,
		                            "SelectPrevSiblingNodeMenuitem",
		                            NULL) ;
		action = gtk_ui_manager_get_action
		         (ui_manager,
		          menu_root_path) ;
		if (menu_root_path) {
			g_free (menu_root_path) ;
			menu_root_path = NULL ;
		}
		THROW_IF_FAIL (action) ;
		if (cur_node->prev) {
			g_object_set (G_OBJECT (action),
			              "sensitive", TRUE,
			              NULL) ;
		} else {
			g_object_set (G_OBJECT (action),
			              "sensitive", FALSE,
			              NULL) ;
		}


		menu_root_path = g_strjoin ("/",
		                            a_menu_root_path,
		                            "SelectParentNodeMenuitem",
		                            NULL) ;
		action = gtk_ui_manager_get_action
		         (ui_manager,
		          menu_root_path) ;
		if (menu_root_path) {
			g_free (menu_root_path) ;
			menu_root_path = NULL ;
		}
		THROW_IF_FAIL (action) ;
		if (cur_node->parent) {
			g_object_set (G_OBJECT (action),
			              "sensitive", TRUE,
			              NULL) ;
		} else {
			g_object_set (G_OBJECT (action),
			              "sensitive", FALSE,
			              NULL) ;
		}

		menu_root_path = g_strjoin ("/",
		                            a_menu_root_path,
		                            "CutNodeMenuitem",
		                            NULL) ;
		action = gtk_ui_manager_get_action
		         (ui_manager,
		          menu_root_path) ;
		if (menu_root_path) {
			g_free (menu_root_path) ;
			menu_root_path = NULL ;
		}
		THROW_IF_FAIL (action) ;

		if (cur_node->parent
		        && cur_node->parent->type == XML_DOCUMENT_NODE
		        && cur_node->next == NULL
		        && cur_node->prev == NULL) {
			g_object_set (G_OBJECT (action),
			              "sensitive", FALSE,
			              NULL) ;
		} else {
			g_object_set (G_OBJECT (action),
			              "sensitive", TRUE,
			              NULL) ;
		}
		break ;
	case XML_COMMENT_NODE:
		menu_root_path = g_strjoin ("/",
		                            a_menu_root_path,
		                            "CommentNodeMenuitem",
		                            NULL) ;
		action = gtk_ui_manager_get_action
		         (ui_manager,
		          menu_root_path) ;
		if (menu_root_path) {
			g_free (menu_root_path) ;
			menu_root_path = NULL ;
		}
		THROW_IF_FAIL (action) ;
		g_object_set (G_OBJECT (action),
		              "sensitive", FALSE,
		              NULL) ;

		menu_root_path = g_strjoin ("/",
		                            a_menu_root_path,
		                            "UncommentNodeMenuitem",
		                            NULL) ;
		action = gtk_ui_manager_get_action
		         (ui_manager,
		          menu_root_path) ;
		THROW_IF_FAIL (action) ;
		g_object_set (G_OBJECT (action),
		              "sensitive", TRUE,
		              NULL) ;
		if (menu_root_path) {
			g_free (menu_root_path) ;
			menu_root_path = NULL ;
		}
		break ;
	default:
		menu_root_path = g_strjoin ("/",
		                            a_menu_root_path,
		                            "CommentNodeMenuitem",
		                            NULL) ;
		action = gtk_ui_manager_get_action
		         (ui_manager,
		          menu_root_path) ;
		if (menu_root_path) {
			g_free (menu_root_path) ;
			menu_root_path = NULL ;
		}
		THROW_IF_FAIL (action) ;
		g_object_set (G_OBJECT (action),
		              "sensitive", TRUE,
		              NULL) ;

		menu_root_path = g_strjoin ("/",
		                            a_menu_root_path,
		                            "UncommentNodeMenuitem",
		                            NULL) ;
		action = gtk_ui_manager_get_action
		         (ui_manager,
		          menu_root_path) ;
		if (menu_root_path) {
			g_free (menu_root_path) ;
			menu_root_path = NULL ;
		}
		THROW_IF_FAIL (action) ;
		g_object_set (G_OBJECT (action),
		              "sensitive", FALSE ,
		              NULL) ;

		menu_root_path = g_strjoin ("/",
		                            a_menu_root_path,
		                            "CopyNodeMenuitem",
		                            NULL) ;
		action = gtk_ui_manager_get_action
		         (ui_manager,
		          menu_root_path) ;
		if (menu_root_path) {
			g_free (menu_root_path) ;
			menu_root_path = NULL ;
		}
		THROW_IF_FAIL (action) ;
		g_object_set (G_OBJECT (action),
		              "sensitive", TRUE,
		              NULL) ;


		menu_root_path = g_strjoin ("/",
		                            a_menu_root_path,
		                            "CutNodeMenuitem",
		                            NULL) ;
		action = gtk_ui_manager_get_action
		         (ui_manager,
		          menu_root_path) ;
		if (menu_root_path) {
			g_free (menu_root_path) ;
			menu_root_path = NULL ;
		}
		THROW_IF_FAIL (action) ;
		g_object_set (G_OBJECT (action),
		              "sensitive", TRUE,
		              NULL) ;


		menu_root_path = g_strjoin ("/",
		                            a_menu_root_path,
		                            "AddChildNodeMenu",
		                            NULL) ;
		action = gtk_ui_manager_get_action
		         (ui_manager,
		          menu_root_path) ;
		if (menu_root_path) {
			g_free (menu_root_path) ;
			menu_root_path = NULL ;
		}
		THROW_IF_FAIL (action) ;
		g_object_set (G_OBJECT (action),
		              "sensitive", FALSE,
		              NULL) ;

		menu_root_path = g_strjoin ("/",
		                            a_menu_root_path,
		                            "PasteNodeAsChildMenuitem",
		                            NULL) ;
		action = gtk_ui_manager_get_action
		         (ui_manager,
		          menu_root_path) ;
		THROW_IF_FAIL (action) ;
		g_object_set (G_OBJECT (action),
		              "sensitive", FALSE,
		              NULL) ;
		break ;
	}
	return MLVIEW_OK ;
}

enum MlViewStatus
TreeView::undo ()
{
	enum MlViewStatus status = MLVIEW_OK ;
	MlViewXMLDocument *doc = NULL ;

	THROW_IF_FAIL (m_priv) ;

	doc = get_document () ;
	if (!doc) {
		mlview_utils_trace_debug ("View contains no document") ;
		return MLVIEW_ERROR ;
	}
	status = mlview_xml_document_undo_mutation (doc, NULL) ;
	return status ;
}


enum MlViewStatus
TreeView::redo ()
{
	enum MlViewStatus status = MLVIEW_OK ;
	MlViewXMLDocument *doc = NULL ;

	THROW_IF_FAIL (m_priv) ;

	doc = get_document () ;
	if (!doc) {
		mlview_utils_trace_debug ("View contains no document") ;
		return MLVIEW_ERROR ;
	}
	status = mlview_xml_document_redo_mutation (doc, NULL) ;
	return status ;
}


bool
TreeView::can_undo ()
{
	gboolean can_undo = FALSE ;
	MlViewXMLDocument *doc = NULL ;

	THROW_IF_FAIL (m_priv) ;

	doc = get_document () ;
	can_undo = mlview_xml_document_can_undo_mutation (doc) ;
	return can_undo ;
}

bool
TreeView::can_redo ()
{
	gboolean can_redo = FALSE ;
	MlViewXMLDocument *doc = NULL ;

	THROW_IF_FAIL (m_priv) ;

	doc = get_document () ;
	can_redo = mlview_xml_document_can_redo_mutation (doc) ;
	return can_redo ;
}
//************************
//signal slot methods
//***********************


void
TreeView::on_tree_editor_selected (GtkNotebookPage *a_page,
                                   guint a_page_num)
{

	THROW_IF_FAIL (m_priv) ;
	THROW_IF_FAIL (m_priv->tree_editors) ;

	Gtk::Widget *widget = m_priv->tree_editors->get_nth_page (a_page_num) ;
	THROW_IF_FAIL (widget) ;
	MlViewTreeEditor *tree_editor = MLVIEW_TREE_EDITOR (widget->gobj ()) ;
	THROW_IF_FAIL (tree_editor);

	m_priv->current_tree_editor = tree_editor ;
}
}//namespace mlview

