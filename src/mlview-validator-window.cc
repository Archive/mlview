/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4-*- */

/*This file is part of GNU MlView
 *
 *GNU MlView is free software; 
 *you can redistribute it and/or modify it under the terms of 
 *the GNU General Public License as 
 *published by the Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope 
 *that it will be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */
#include <string.h>
#include <glade/glade.h>
#include "mlview-validator-window.h"
#include "mlview-validator.h"

struct _MlViewValidatorWindow
{
	MlViewXMLDocument *document;

	GtkWidget *validation_report;
	GtkWidget *document_label;
	GtkWidget *status_label;
	GtkWidget *status_icon;

	struct
	{
		GtkWidget *view;
		GtkListStore *store;
		mlview::ValidationOutput *validation_output;
		struct mlview::TypeIcons *icons;
	}
	output;

	struct
	{
		GtkWidget *combo;
		GtkListStore *store;
		GHashTable *references;
	}
	schemas;
};

typedef struct _MlViewValidatorWindow MlViewValidatorWindow;

enum SchemasModelColumns {
    URL_COLUMN = 0,
    TYPE_COLUMN,
    SCHEMA_COLUMN,
    SCHEMAS_MODEL_NB_COLUMNS
};

enum OutputModelColumns {
    ICON_COLUMN = 0,
    NODE_COLUMN,
    LEVEL_COLUMN,
    ERROR_COLUMN,
    MESSAGE_COLUMN,
    OUTPUT_MODEL_NB_COLUMNS
};

static void
add_schema_to_list_store_func (MlViewSchema *a_schema,
                               MlViewValidatorWindow *a_window)
{
	GtkTreeIter iter = { 0 };
	gchar *url = NULL, *type = NULL;
	GtkTreePath *path = NULL;
	GtkTreeRowReference *ref = NULL;
	enum MlViewSchemaType schema_type = SCHEMA_TYPE_UNDEF ;
	enum MlViewStatus status = MLVIEW_OK ;

	g_return_if_fail (a_schema);
	g_return_if_fail (a_window);
	g_return_if_fail (a_window->schemas.store);
	g_return_if_fail (a_window->schemas.references);

	url = mlview_schema_get_url (a_schema);

	g_return_if_fail (url);

	gtk_list_store_append (a_window->schemas.store, &iter);

	path = gtk_tree_model_get_path
	       (GTK_TREE_MODEL (a_window->schemas.store), &iter);

	if (!path) {
		gtk_list_store_remove (a_window->schemas.store, &iter);

		return;
	}

	ref = gtk_tree_row_reference_new
	      (GTK_TREE_MODEL (a_window->schemas.store), path);

	gtk_tree_path_free (path);
	path = NULL;

	if (!ref) {
		gtk_list_store_remove (a_window->schemas.store, &iter);

		return;
	}

	g_hash_table_insert (a_window->schemas.references, url, ref);

	status = mlview_schema_get_type (a_schema, &schema_type) ;
	g_return_if_fail (status == MLVIEW_OK
	                  && schema_type != SCHEMA_TYPE_UNDEF) ;
	switch (schema_type) {
	case SCHEMA_TYPE_DTD:
		type = (gchar*) "(DTD)";
		break;
	case SCHEMA_TYPE_RNG:
		type = (gchar*) "(RNG)";
		break;
	case SCHEMA_TYPE_XSD:
		type = (gchar*) "(XSD)";
		break;
	default:
		g_assert_not_reached () ;
	}

	gtk_list_store_set (a_window->schemas.store, &iter, URL_COLUMN, url,
	                    TYPE_COLUMN, type, SCHEMA_COLUMN, a_schema, -1);
}

static void
document_changed_cb (MlViewValidatorWindow *a_window)
{
	g_return_if_fail (a_window);
	g_return_if_fail (a_window->status_label);
	g_return_if_fail (a_window->status_icon);

	gtk_label_set_text (GTK_LABEL (a_window->status_label),
	                    _("Document changed; Re-run validation"));
	gtk_widget_set_sensitive (GTK_WIDGET (a_window->status_icon), FALSE);

}

static void
schema_associated_cb (MlViewSchemaList *a_list, MlViewSchema *a_schema,
                      MlViewValidatorWindow *a_window)
{
	gint active = -1;

	g_return_if_fail (a_schema);
	g_return_if_fail (a_window);
	g_return_if_fail (a_window->schemas.combo);

	add_schema_to_list_store_func (a_schema, a_window);

	active = gtk_combo_box_get_active (GTK_COMBO_BOX
	                                   (a_window->schemas.combo));

	if (active == -1)
		gtk_combo_box_set_active (GTK_COMBO_BOX
		                          (a_window->schemas.combo), 0);
}

static void
schema_unassociated_cb (MlViewSchemaList *a_list, MlViewSchema *a_schema,
                        MlViewValidatorWindow *a_window)
{
	GtkTreeRowReference *ref = NULL;
	gchar *url = NULL;
	GtkTreePath *path = NULL;
	gboolean res = FALSE;
	GtkTreeIter iter = { 0 };

	g_return_if_fail (a_schema);
	g_return_if_fail (a_window);
	g_return_if_fail (a_window->schemas.references);
	g_return_if_fail (a_window->schemas.store);

	url = mlview_schema_get_url (a_schema);

	g_return_if_fail (url);

	ref = (GtkTreeRowReference *) g_hash_table_lookup (a_window->schemas.references,
	        url);

	g_return_if_fail (ref);

	path = gtk_tree_row_reference_get_path (ref);

	g_return_if_fail (path);

	res = gtk_tree_model_get_iter
	      (GTK_TREE_MODEL (a_window->schemas.store), &iter, path);

	gtk_tree_path_free (path);
	path = NULL;

	g_return_if_fail (res);

	res = g_hash_table_remove (a_window->schemas.references, url);

	g_return_if_fail (res);

	gtk_list_store_remove (a_window->schemas.store, &iter);
}

static void
validation_report_destroy_cb (GtkWidget *a_validation_report,
                              MlViewValidatorWindow *a_window)
{
	MlViewSchemaList *list = NULL;
	mlview::AppContext *ctxt = NULL;

	THROW_IF_FAIL (a_window);
	if (a_window->document) {
		if (a_window->output.icons) {
			ctxt = mlview::AppContext::get_instance ();

			if (ctxt)
				ctxt->type_icons_unref ();
		}
		g_signal_handlers_disconnect_by_func
		(G_OBJECT (a_window->document),
		 (void *) (document_changed_cb),
		 a_window);

		list = mlview_xml_document_get_schema_list
		       (a_window->document);

		if (list) {
			g_signal_handlers_disconnect_by_func
			(G_OBJECT (list),
			 (void *) (schema_associated_cb),
			 a_window);

			g_signal_handlers_disconnect_by_func
			(G_OBJECT (list),
			 (void *) (schema_unassociated_cb),
			 a_window);
		}
	}

	if (a_window->schemas.references)
		g_hash_table_destroy (a_window->schemas.references);

	if (a_window->output.validation_output)
		delete (a_window->output.validation_output);

	memset (a_window, 0, sizeof (MlViewValidatorWindow));

	g_free (a_window);
	a_window = NULL;
}

static void
close_button_clicked_cb (GtkWidget *a_button,
                         MlViewValidatorWindow *a_window)
{
	g_return_if_fail (a_window);
	g_return_if_fail (a_window->validation_report);

	gtk_widget_destroy (a_window->validation_report);
}

static void
validate_button_clicked_cb (GtkWidget *a_button,
                            MlViewValidatorWindow *a_window)
{
	GtkTreeIter iter = { 0 };
	gboolean res = FALSE;
	GtkWidget *dialog = NULL,
	                    *label = NULL;
	MlViewSchema *schema = NULL;
	enum MlViewStatus status = MLVIEW_OK ;
	mlview::ValidationOutput::Message *message = NULL;
	mlview::AppContext *ctxt = NULL;
	GdkPixbuf *pix = NULL;
	gchar *priority = NULL;

	g_return_if_fail (a_window);
	g_return_if_fail (a_window->document);
	g_return_if_fail (a_window->schemas.combo);
	g_return_if_fail (a_window->schemas.store);
	g_return_if_fail (a_window->output.view);
	g_return_if_fail (a_window->validation_report);
	g_return_if_fail (a_window->status_label);
	g_return_if_fail (a_window->status_icon);

	res = gtk_combo_box_get_active_iter
	      (GTK_COMBO_BOX (a_window->schemas.combo), &iter);

	if (!res) {
		dialog = gtk_dialog_new_with_buttons (_("No schema selected"),
		                                      GTK_WINDOW (a_window->validation_report),
		                                      GTK_DIALOG_MODAL,
		                                      GTK_STOCK_OK,
		                                      GTK_RESPONSE_NONE,
		                                      NULL);

		THROW_IF_FAIL (dialog) ;

		label = gtk_label_new
		        (_("You must associate a schema with "
		           "your document in order to validate it."));

		THROW_IF_FAIL (label) ;

		gtk_container_add (GTK_CONTAINER (GTK_DIALOG (dialog)->vbox),
		                   label);

		g_signal_connect (G_OBJECT (dialog), "response",
		                  G_CALLBACK (gtk_widget_destroy), NULL);

		gtk_widget_show_all (dialog);

		dialog = NULL;
		label = NULL;

		return;
	}

	if (!a_window->output.icons) {
		ctxt = mlview::AppContext::get_instance ();
		THROW_IF_FAIL (ctxt);
		a_window->output.icons = ctxt->type_icons_ref ();
	}

	gtk_tree_model_get (GTK_TREE_MODEL (a_window->schemas.store), &iter,
	                    SCHEMA_COLUMN, &schema, -1);

	g_return_if_fail (schema);

	if (a_window->output.validation_output) {
		delete (a_window->output.validation_output);
		a_window->output.validation_output = NULL;
	}

	status = mlview::Validator::validate_with_schema
	         (a_window->document,
	          schema,
	          &a_window->output.validation_output) ;

	THROW_IF_FAIL (a_window->output.validation_output);

	switch (status) {
	case MLVIEW_OK:
		gtk_label_set_text (GTK_LABEL (a_window->status_label),
		                    _("Valid document"));

		gtk_image_set_from_stock (GTK_IMAGE (a_window->status_icon),
		                          GTK_STOCK_YES,
		                          GTK_ICON_SIZE_SMALL_TOOLBAR);

		gtk_widget_set_sensitive (GTK_WIDGET (a_window->status_icon),
		                          TRUE);
		break;

	case MLVIEW_DOC_NOT_VALID_ERROR:
		gtk_label_set_text (GTK_LABEL (a_window->status_label),
		                    _("Invalid document"));

		gtk_image_set_from_stock (GTK_IMAGE (a_window->status_icon),
		                          GTK_STOCK_NO,
		                          GTK_ICON_SIZE_SMALL_TOOLBAR);

		gtk_widget_set_sensitive (GTK_WIDGET (a_window->status_icon),
		                          TRUE);
		break;

	default:
		gtk_label_set_text (GTK_LABEL (a_window->status_label),
		                    _("Validation error")) ;

		gtk_image_set_from_stock (GTK_IMAGE (a_window->status_icon),
		                          GTK_STOCK_STOP,
		                          GTK_ICON_SIZE_SMALL_TOOLBAR) ;

		gtk_widget_set_sensitive (GTK_WIDGET (a_window->status_icon),
		                          TRUE);
	}

	a_window->output.store = gtk_list_store_new
	                         (OUTPUT_MODEL_NB_COLUMNS, GDK_TYPE_PIXBUF, G_TYPE_STRING,
	                          G_TYPE_STRING, G_TYPE_STRING, G_TYPE_POINTER);

	g_return_if_fail (a_window->output.store);

	vector<mlview::ValidationOutput::Message*>::iterator it ;
	for (it = a_window->output.validation_output->get_messages ().begin () ;
	        it != a_window->output.validation_output->get_messages ().end () ;
	        ++it) {
		message = *it ;
		if (!message) {
			g_object_unref (G_OBJECT (a_window->output.store));
			a_window->output.store = NULL;

			mlview_utils_trace_debug
			("validate_button_clicked_cb failed.");
			return;
		}

		gtk_list_store_append (a_window->output.store, &iter);

		if (a_window->output.icons)
			switch (message->get_node_type ()) {
			case XML_ELEMENT_NODE:
				pix = a_window->output.icons->element;
				break;
			case XML_TEXT_NODE:
				pix = a_window->output.icons->text;
				break;
			case XML_COMMENT_NODE:
				pix = a_window->output.icons->comment;
				break;
			case XML_PI_NODE:
				pix = a_window->output.icons->pi;
				break;
			case XML_ENTITY_REF_NODE:
				pix = a_window->output.icons->entity_ref;
				break;
			default:
				pix = NULL;
				break;
			}

		switch (message->get_priority ()) {
		case XML_ERR_NONE:
			priority = _("Message");
			break;
		case XML_ERR_WARNING:
			priority = _("Warning");
			break;
		case XML_ERR_ERROR:
			priority = _("Error");
			break;
		case XML_ERR_FATAL:
			priority = _("Fatal");
			break;
		default:
			priority = NULL;
			break;
		}

		xmlChar* name = NULL ;
		if (message->get_node ()) {
			name = const_cast<xmlChar*>(message->get_node ()->name) ;
		}
		gtk_list_store_set (a_window->output.store, &iter,
		                    ICON_COLUMN, pix,
		                    LEVEL_COLUMN, priority,
		                    NODE_COLUMN, name,
		                    ERROR_COLUMN, message->get_text().c_str (),
		                    MESSAGE_COLUMN, message, -1);
	}

	gtk_tree_view_set_model (GTK_TREE_VIEW (a_window->output.view),
	                         GTK_TREE_MODEL (a_window->output.store));

	return;
}

static void
row_activated_cb (GtkTreeView *a_view,
                  GtkTreePath *a_path,
                  GtkTreeViewColumn *a_column,
                  MlViewValidatorWindow *a_win)
{
	GtkTreeIter iter = { 0 };
	gboolean res = FALSE;
	mlview::ValidationOutput::Message *message = NULL;
	GtkWidget *dialog = NULL, *label = NULL;

	g_return_if_fail (a_win);
	g_return_if_fail (a_path);
	g_return_if_fail (a_win->output.store);
	g_return_if_fail (GTK_IS_TREE_MODEL (a_win->output.store));
	g_return_if_fail (a_win->document);
	g_return_if_fail (a_win->validation_report);

	res = gtk_tree_model_get_iter (GTK_TREE_MODEL (a_win->output.store),
	                               &iter, a_path);

	THROW_IF_FAIL (res);

	gtk_tree_model_get (GTK_TREE_MODEL (a_win->output.store), &iter,
	                    MESSAGE_COLUMN, &message, -1);

	THROW_IF_FAIL (message);

	if (message->get_node ())
		mlview_xml_document_select_node (a_win->document,
		                                 message->get_node ());
	else {
		dialog = gtk_dialog_new_with_buttons
		         (_("No node for message"),
		          GTK_WINDOW (a_win->validation_report),
		          GTK_DIALOG_MODAL,
		          GTK_STOCK_OK,
		          GTK_RESPONSE_ACCEPT,
		          NULL);

		g_return_if_fail (dialog);

		label = gtk_label_new
		        (_("No existing node is associated with this message."));

		if (!label)
			goto cleanup;

		gtk_container_add (GTK_CONTAINER (GTK_DIALOG (dialog)->vbox),
		                   label);

		gtk_widget_show (label);

		gtk_dialog_run (GTK_DIALOG (dialog));

cleanup:
		if (dialog) {
			gtk_widget_destroy (dialog);
			dialog = NULL;
		}
	}
}

GtkWidget *
mlview_validator_window_new (MlViewXMLDocument *a_doc)
{
	gchar *file = NULL, *uri = NULL;
	GladeXML *xml = NULL;
	MlViewValidatorWindow *win = NULL;
	MlViewSchemaList *list = NULL;
	GtkCellRenderer *renderer = NULL;
	GtkTreeIter iter = { 0 };
	gboolean res = FALSE;
	GtkTreeViewColumn *column = NULL;

	g_return_val_if_fail (a_doc && MLVIEW_IS_XML_DOCUMENT (a_doc), NULL);

	list = mlview_xml_document_get_schema_list (a_doc);

	g_return_val_if_fail (list, NULL);

	/* Allocation */

	win = (MlViewValidatorWindow *) g_try_malloc (sizeof (MlViewValidatorWindow));

	if (!win)
		goto cleanup;

	memset (win, 0, sizeof (MlViewValidatorWindow));

	win->document = a_doc;

	/* Glade */

	file = gnome_program_locate_file (NULL, GNOME_FILE_DOMAIN_APP_DATADIR,
	                                  PACKAGE "/mlview-validation-report.glade",
	                                  TRUE, NULL);

	if (!file)
		goto cleanup;

	xml = glade_xml_new (file, NULL, NULL);

	g_free (file);
	file = NULL;

	if (!xml)
		goto cleanup;

	/* Misc widgets */

	win->validation_report = glade_xml_get_widget (xml, "ValidationReport");

	if (!(win->validation_report && GTK_IS_WINDOW (win->validation_report)))
		goto cleanup;

	win->document_label = glade_xml_get_widget (xml, "DocumentLabel");

	if (!(win->document_label && GTK_IS_LABEL (win->document_label)))
		goto cleanup;

	uri = mlview_xml_document_get_uri (a_doc);

	if (uri) {
		gtk_label_set_text (GTK_LABEL (win->document_label), uri);

		uri = NULL;
	}

	win->schemas.combo = glade_xml_get_widget (xml, "SchemasCombo");

	if (!(win->schemas.combo && GTK_IS_COMBO_BOX (win->schemas.combo)))
		goto cleanup;

	win->status_label = glade_xml_get_widget (xml, "StatusLabel");

	if (!(win->status_label && GTK_IS_LABEL (win->status_label)))
		goto cleanup;

	win->status_icon = glade_xml_get_widget (xml, "StatusIcon");

	if (!(win->status_icon && GTK_IS_IMAGE (win->status_icon)))
		goto cleanup;

	/* Output view */

	win->output.view = glade_xml_get_widget (xml, "OutputView");

	if (!(win->output.view && GTK_IS_TREE_VIEW (win->output.view)))
		goto cleanup;

	g_signal_connect (G_OBJECT (win->output.view), "row-activated",
	                  G_CALLBACK (row_activated_cb), win);

	win->output.store = gtk_list_store_new
	                    (OUTPUT_MODEL_NB_COLUMNS, GDK_TYPE_PIXBUF, G_TYPE_STRING,
	                     G_TYPE_STRING, G_TYPE_STRING, G_TYPE_POINTER);

	if (!win->output.store)
		goto cleanup;

	gtk_tree_view_set_model (GTK_TREE_VIEW (win->output.view),
	                         GTK_TREE_MODEL (win->output.store));

	g_object_unref (G_OBJECT (win->output.store));

	column = gtk_tree_view_column_new ();

	if (!column)
		goto cleanup;

	gtk_tree_view_column_set_title (column, _("Node"));

	renderer = gtk_cell_renderer_pixbuf_new ();

	if (!renderer)
		goto cleanup;

	gtk_tree_view_column_pack_start (column, renderer, FALSE);

	gtk_tree_view_column_set_attributes (column, renderer, "pixbuf",
	                                     ICON_COLUMN, NULL);

	renderer = NULL;

	renderer = gtk_cell_renderer_text_new ();

	gtk_tree_view_column_pack_start (column, renderer, FALSE);

	gtk_tree_view_column_set_attributes (column, renderer, "text",
	                                     NODE_COLUMN, NULL);

	renderer = NULL;

	gtk_tree_view_append_column (GTK_TREE_VIEW (win->output.view),
	                             column);

	column = NULL;

	column = gtk_tree_view_column_new ();

	if (!column)
		goto cleanup;

	gtk_tree_view_column_set_title (column, _("Priority"));

	renderer = gtk_cell_renderer_text_new ();

	if (!renderer)
		goto cleanup;

	gtk_tree_view_column_pack_start (column, renderer, FALSE);

	gtk_tree_view_column_set_attributes (column, renderer, "text",
	                                     LEVEL_COLUMN, NULL);

	renderer = NULL;

	gtk_tree_view_append_column (GTK_TREE_VIEW (win->output.view),
	                             column);

	column = NULL;

	column = gtk_tree_view_column_new ();

	if (!column)
		goto cleanup;

	gtk_tree_view_column_set_title (column, _("Message"));

	renderer = gtk_cell_renderer_text_new ();

	if (!renderer)
		goto cleanup;

	gtk_tree_view_column_pack_start (column, renderer, FALSE);

	gtk_tree_view_column_set_attributes (column, renderer, "text",
	                                     ERROR_COLUMN, NULL);

	renderer = NULL;

	gtk_tree_view_append_column (GTK_TREE_VIEW (win->output.view),
	                             column);

	column = NULL;

	/* Signals */

	glade_xml_signal_connect_data (xml, "validate_button_clicked_cb",
	                               G_CALLBACK (validate_button_clicked_cb),
	                               win);

	glade_xml_signal_connect_data (xml, "close_button_clicked_cb",
	                               G_CALLBACK (close_button_clicked_cb),
	                               win);

	g_signal_connect (G_OBJECT (win->validation_report), "destroy",
	                  G_CALLBACK (validation_report_destroy_cb), win);

	/* Schema list */

	win->schemas.references = g_hash_table_new_full
	                          (g_str_hash, g_str_equal, NULL, (GDestroyNotify) gtk_tree_row_reference_free);

	if (!win->schemas.references)
		goto cleanup;

	win->schemas.store = gtk_list_store_new (SCHEMAS_MODEL_NB_COLUMNS, G_TYPE_STRING,
	                     G_TYPE_STRING, G_TYPE_POINTER);

	if (!win->schemas.store)
		goto cleanup;

	mlview_schema_list_foreach (list, (MlViewSchemaListFunc) add_schema_to_list_store_func,
	                            win);

	gtk_combo_box_set_model (GTK_COMBO_BOX (win->schemas.combo),
	                         GTK_TREE_MODEL (win->schemas.store));

	g_object_unref (G_OBJECT (win->schemas.store));

	res = gtk_tree_model_get_iter_first (GTK_TREE_MODEL (win->schemas.store), &iter);

	if (res)
		gtk_combo_box_set_active_iter (GTK_COMBO_BOX (win->schemas.combo), &iter);

	renderer = gtk_cell_renderer_text_new ();

	if (!renderer)
		goto cleanup;

	gtk_cell_layout_pack_start (GTK_CELL_LAYOUT (win->schemas.combo), renderer, TRUE);

	gtk_cell_layout_set_attributes (GTK_CELL_LAYOUT (win->schemas.combo), renderer,
	                                "text", URL_COLUMN, NULL);

	renderer = gtk_cell_renderer_text_new ();

	if (!renderer)
		goto cleanup;

	gtk_cell_layout_pack_start (GTK_CELL_LAYOUT (win->schemas.combo), renderer, FALSE);

	gtk_cell_layout_set_attributes (GTK_CELL_LAYOUT (win->schemas.combo), renderer,
	                                "text", TYPE_COLUMN, NULL);

	renderer = NULL;

	g_signal_connect (G_OBJECT (list), "schema-associated",
	                  G_CALLBACK (schema_associated_cb), win);

	g_signal_connect (G_OBJECT (list), "schema-unassociated",
	                  G_CALLBACK (schema_unassociated_cb), win);

	/* Document */

	g_signal_connect_swapped (G_OBJECT (a_doc), "node-changed",
	                          G_CALLBACK (document_changed_cb), win);

	g_signal_connect_swapped (G_OBJECT (a_doc), "document-changed",
	                          G_CALLBACK (document_changed_cb), win);

	/* Return, cleanup */

	return win->validation_report;

cleanup:
	if (renderer) {
		gtk_object_destroy (GTK_OBJECT (renderer));
		renderer = NULL;
	}

	if (column) {
		gtk_object_destroy (GTK_OBJECT (column));
		column = NULL;
	}

	if (file) {
		g_free (file);
		file = NULL;
	}

	if (xml) {
		g_object_unref (xml);
		xml = NULL;
	}

	if (win) {
		if (win->validation_report) {
			gtk_widget_destroy (win->validation_report);

			memset (win, 0, sizeof (MlViewValidatorWindow));
		}

		if (win->schemas.references) {
			g_hash_table_destroy (win->schemas.references);
			win->schemas.references = NULL;
		}

		g_free (win);
		win = NULL;
	}

	return NULL;
}
