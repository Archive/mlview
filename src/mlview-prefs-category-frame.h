/* -*- Mode: C++; indent-tabs-mode: true; c-basic-offset: 4 -*- */
/*
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute 
 *it and/or modify it under the terms of 
 *the GNU General Public License as published by the 
 *Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYING file for copyright information.
 */
#ifndef __PREFS_CATEGORY_FRAME_H__
#define __PREFS_CATEGORY_FRAME_H__

#include <gtkmm.h>
#include <libglademm.h>

#include "mlview-object.h"

namespace mlview
{

struct PrefsCategoryFramePriv;

///
/// This class is a custom frame used on the right side of the preferences
/// window.\n
/// The frame is builded from glade. Its content is created from a specified
/// widget who inherits from Gtk::VBox.\n
/// Children of PrefsCategoryFrame must first call the
/// PrefsCategoryFrame constructor as it holds the ui creation code
///
class PrefsCategoryFrame : public Object
{
	friend class PrefsCategoryFramePriv;
	PrefsCategoryFramePriv *m_priv;

	//forbid copy/assignation
	PrefsCategoryFrame (PrefsCategoryFrame const &) ;
	PrefsCategoryFrame& operator= (PrefsCategoryFrame const&) ;
	
public:
	///
	/// Create the frame and load its content from glade.
	///
	/// \param widget_name the name of the widget from which to load the frame
	/// content
	PrefsCategoryFrame (const Glib::ustring& widget_name);
	///
	/// Default destructor
	///
	virtual ~PrefsCategoryFrame ();
	///
	/// Return a reference to the underlying Gtk::Widget
	///
	/// \return a reference to the underlying Gtk::Widget
	///
	Gtk::Widget& widget_ref ();

protected:
	///
	/// Return a reference to the Glade::Xml object.\n
	/// This method is a convenient way for child classes to access the glade file
	///
	Glib::RefPtr<Gnome::Glade::Xml> get_gladexml_ref ();
};

} // namespace mlview
#endif
