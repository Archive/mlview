/* -*- Mode: C; indent-tabs-mode:nil; c-basic-offset: 8-*- */

/*
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute 
 *it and/or modify it under the terms of 
 *the GNU General Public License as published by the 
 *Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */

#include "mlview-idbc.h"

#ifdef MLVIEW_WITH_DBUS
#include <dbus/dbus-glib-lowlevel.h>

static void mlview_idbc_base_init (gpointer a_this) ;

enum MlViewStatus get_session_bus (MlViewIDBC *a_this,
                                   DBusConnection **a_con,
                                   GError **a_error) ;
static void
mlview_idbc_base_init (gpointer a_this)
{
	MlViewIDBC *thiz = NULL ;

	thiz = a_this ;
	g_return_if_fail (thiz) ;

	thiz->get_session_bus = get_session_bus ;
}


enum MlViewStatus
get_session_bus (MlViewIDBC *a_this,
                 DBusConnection **a_con,
                 GError **a_error)
{
	DBusError dbus_error = {0} ;
	DBusConnection *dbus_connection = NULL ;
	static gboolean initialized = FALSE ;

	g_return_val_if_fail (a_this && MLVIEW_IS_IDBC (a_this),
	                      MLVIEW_BAD_PARAM_ERROR) ;

	dbus_error_init (&dbus_error) ;
	dbus_connection =  dbus_bus_get
	                   (DBUS_BUS_SESSION, &dbus_error) ;
	if (!dbus_connection
	        || dbus_error_is_set (&dbus_error)) {
		if (a_error && dbus_error_is_set (&dbus_error)) {
			*a_error = g_error_new
			           (g_quark_from_string
			            ("MLVIEW_BUS_ERROR"),
			            MLVIEW_BUS_ERROR,
			            "%s\n",
			            dbus_error.message) ;

		} else if (a_error) {
			*a_error = g_error_new
			           (g_quark_from_string
			            ("MLVIEW_BUS_ERROR"),
			            MLVIEW_BUS_ERROR,
			            "Could not get session bus\n") ;
		}
		return MLVIEW_BUS_ERROR ;
	}
	*a_con = dbus_connection ;

	if (!initialized) {
		dbus_connection_setup_with_g_main (dbus_connection, NULL) ;
		initialized = TRUE ;
	}
	return MLVIEW_OK ;
}

GType
mlview_idbc_get_type (void)
{
	static GType type = 0 ;

	if (!type) {
		static const GTypeInfo info = {
		                                  sizeof (MlViewIDBC),/*class size*/
		                                  mlview_idbc_base_init, /*base_init*/
		                                  NULL, /*base_finalize*/
		                                  NULL,/*class init*/
		                                  NULL,/*class finalize*/
		                                  NULL,/*class data*/
		                                  0,/*instance size*/
		                                  0,/*n_prealloc*/
		                                  NULL,/*instance_init*/
		                              } ;
		type = g_type_register_static (G_TYPE_INTERFACE,
		                               "MlViewIDBC",
		                               &info, 0) ;
		g_type_interface_add_prerequisite (type, G_TYPE_OBJECT) ;
	}
	return type ;
}

enum MlViewStatus
mlview_idbc_get_session_bus (MlViewIDBC *a_this,
                             DBusConnection **a_con,
                             GError **a_error)
{
	g_return_val_if_fail (a_this && MLVIEW_IS_IDBC (a_this),
	                      MLVIEW_BAD_PARAM_ERROR) ;

	if (!MLVIEW_IDBC_GET_IFACE (a_this)->get_session_bus) {
		return MLVIEW_IFACE_NOT_DEFINED_ERROR ;
	}
	return MLVIEW_IDBC_GET_IFACE (a_this)->get_session_bus
	       (a_this, a_con, a_error) ;
}

#endif /*MLVIEW_WITH_DBUS*/
