/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 8-*- */

/*This file is part of MlView
 *
 *MlView is free software; you can redistribute it and/or modify it under the terms of 
 *the GNU General Public License as published by the Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *MlView is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License along with MlView; 
 *see the file COPYING. If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */

#ifndef __MLVIEW_ATTRS_EDITOR_H__
#define __MLVIEW_ATTRS_EDITOR_H__

#include <gtk/gtk.h>
#include <libxml/tree.h>
#include "mlview-utils.h"
#include "mlview-app-context.h"
#include "mlview-xml-document.h"

G_BEGIN_DECLS

/**
 *@file
 *The declaration of the #MlViewAttrsEditor class.
 */

/*
 *compulsory macro to comply with GTK type system.
 */
#define MLVIEW_TYPE_ATTRS_EDITOR (mlview_attrs_editor_get_type())
#define MLVIEW_ATTRS_EDITOR(object) (GTK_CHECK_CAST(object,MLVIEW_TYPE_ATTRS_EDITOR,MlViewAttrsEditor))
#define MLVIEW_ATTRS_EDITOR_CLASS(klass) (GTK_CHECK_CLASS_CAST(klass,MLVIEW_TYPE_ATTRS_EDITOR,MlViewAttrsEditorClass))
#define MLVIEW_IS_ATTRS_EDITOR(object) (GTK_CHECK_TYPE((object), MLVIEW_TYPE_ATTRS_EDITOR))
#define MLVIEW_IS_ATTRS_EDITOR_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), MLVIEW_TYPE_ATTRS_EDITOR))

enum {
    ATTRIBUTE_NAME_OFFSET,
    ATTRIBUTE_VALUE_OFFSET
};

typedef struct _MlViewAttrsEditor
			MlViewAttrsEditor;
typedef struct _MlViewAttrsEditorPrivate
			MlViewAttrsEditorPrivate;
typedef struct _MlViewAttrsEditorClass
			MlViewAttrsEditorClass;

/**
 *This class is an attributes list editor.
 *It graphically edits the attributes list hold by a given xmlNode.
 *That edition includes adding/deleting/modifying attributes from the list.
 */
struct _MlViewAttrsEditor
{
	/**parent class structure*/
	GtkVBox container;
	/**The private member variables*/
	MlViewAttrsEditorPrivate *priv;
};

/**
 *The vtable of the MlViewAttrsEditor class.
 */
struct _MlViewAttrsEditorClass
{
	GtkVBoxClass parent_class;
	/*This class signals */
	void (*attribute_changed) (MlViewAttrsEditor *a_attributes_list,
	                           gpointer a_data) ;
};

guint mlview_attrs_editor_get_type (void);

GtkWidget *mlview_attrs_editor_new (gchar * a_names_title,
                                    gchar * a_values_title);

GtkTreeModel * mlview_attrs_editor_get_model (MlViewAttrsEditor *a_this) ;

enum MlViewStatus mlview_attrs_editor_get_cur_sel_iter (MlViewAttrsEditor *a_this,
                                                        GtkTreeIter *a_iter) ;

GtkTreeView * mlview_attrs_editor_get_tree_view (MlViewAttrsEditor *a_this) ;

GtkTreeRowReference * mlview_attrs_editor_get_new_row_ref (MlViewAttrsEditor *a_this,
                                                           GtkTreeIter *a_iter) ;

xmlNode * mlview_attrs_editor_get_cur_xml_node (MlViewAttrsEditor *a_this) ;

xmlAttr * mlview_attrs_editor_get_xml_attr (MlViewAttrsEditor *a_this,
                                            GtkTreeIter *a_iter) ;

enum MlViewStatus mlview_attrs_editor_get_row_ref_from_xml_attr (MlViewAttrsEditor *a_this,
                                                                xmlAttr *a_xml_attr,
                                                                GtkTreeRowReference **a_result) ;

GtkTreeRowReference * mlview_attrs_editor_get_row_ref (MlViewAttrsEditor *a_this,
                                                       GtkTreeIter *a_iter) ;

enum MlViewStatus mlview_attrs_editor_associate_row_ref_to_xml_attr (MlViewAttrsEditor *a_this,
                                                                     GtkTreeRowReference *a_ref,
                                                                     xmlAttr *a_xml_attr) ;

enum MlViewStatus mlview_attrs_editor_get_attribute2 (MlViewAttrsEditor * a_this,
                                                      GtkTreeRowReference *a_row_ref,
                                                      xmlAttr **a_xml_attr_ptr) ;

void mlview_attrs_editor_set_titles (MlViewAttrsEditor * a_attributes,
                                     gchar * a_names_title,
                                     gchar * a_values_title);

gboolean mlview_attrs_editor_is_row_the_add_new_attr_row (MlViewAttrsEditor *a_this,
                                                          GtkTreeIter *a_iter) ;

enum MlViewStatus mlview_attrs_editor_insert_attribute (MlViewAttrsEditor *a_this,
                                                        GtkTreeIter *a_iter,
                                                        gint offset,
                                                        const xmlAttrPtr a_xml_attr) ;

enum MlViewStatus mlview_attrs_editor_create_attribute (MlViewAttrsEditor * a_this);

enum MlViewStatus mlview_attrs_editor_remove_attribute (MlViewAttrsEditor * a_attributes,
                                                        GtkTreeIter *a_iter);

enum MlViewStatus
mlview_attrs_editor_get_attribute (MlViewAttrsEditor * a_this,
                                   GtkTreeIter *a_iter,
                                   xmlAttr **a_xml_attr_ptr) ;

enum MlViewStatus mlview_attrs_editor_edit_xml_attributes (MlViewAttrsEditor* a_this,
        MlViewXMLDocument *a_mlview_xml_doc,
        const xmlNodePtr a_xml_node) ;

enum MlViewStatus mlview_attrs_editor_clear (MlViewAttrsEditor *a_this) ;

enum MlViewStatus mlview_attrs_editor_update_attribute (MlViewAttrsEditor *a_this,
        xmlAttr *a_attr) ;

enum MlViewStatus mlview_attrs_editor_update_attribute_removed (MlViewAttrsEditor *a_this,
        xmlAttr *a_attr) ;

enum MlViewStatus mlview_attrs_editor_update_attribute_removed2 (MlViewAttrsEditor *a_this,
        xmlNode *a_node,
        xmlChar *a_name) ;

enum MlViewStatus mlview_attrs_editor_connect_to_doc (MlViewAttrsEditor *a_this,
        MlViewXMLDocument *a_doc) ;

enum MlViewStatus mlview_attrs_editor_disconnect_from_doc (MlViewAttrsEditor *a_this,
        MlViewXMLDocument *a_doc) ;
G_END_DECLS

#endif
