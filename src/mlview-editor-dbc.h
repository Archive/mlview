/* -*- Mode: C; indent-tabs-mode:nil; c-basic-offset: 8-*- */

/*
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute 
 *it and/or modify it under the terms of 
 *the GNU General Public License as published by the 
 *Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */

#ifndef __MLVIEW_DBC_H__
#define __MLVIEW_DBC_H__

#include "mlview-utils.h"

#ifdef MLVIEW_WITH_DBUS

#define MLVIEW_TYPE_EDITOR_DBC (mlview_editor_dbc_get_type ())
#define MLVIEW_EDITOR_DBC(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), MLVIEW_TYPE_EDITOR_DBC, MlViewEditorDBC))
#define MLVIEW_IS_EDITOR_DBC(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), MLVIEW_TYPE_EDITOR_DBC))
#define MLVIEW_EDITOR_DBC_GET_IFACE(obj) (G_TYPE_INSTANCE_GET_INTERFACE ((obj), MLVIEW_TYPE_EDITOR_DBC, MlViewEditorDBC))

typedef struct _MlViewEditorDBC MlViewEditorDBC ;
typedef struct _MlViewEditorDBCPriv MlViewEditorDBCPriv ;
typedef struct _MlViewEditorDBCClass MlViewEditorDBCClass ;

struct _MlViewEditorDBC
{
	GObject parent_object ;
	MlViewEditorDBCPriv *priv ;
} ;

struct _MlViewEditorDBCClass
{
	GObjectClass parent_class ;
} ;

GType mlview_editor_dbc_get_type (void) ;

MlViewEditorDBC * mlview_editor_dbc_new (void) ;

enum MlViewStatus mlview_editor_dbc_load_xml_file_with_dtd
(MlViewEditorDBC *a_this,
 const gchar *a_service_name,
 const gchar *a_doc_uri,
 const gchar *a_dtd_uri) ;

#endif /*MLVIEW_WITH_DBUS*/

#endif /*__MLVIEW_DBC_H__*/
