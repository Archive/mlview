/* -*- Mode: C++; indent-tabs-mode:true ; c-basic-offset: 4-*- */

/*
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute 
 *it and/or modify it under the terms of 
 *the GNU General Public License as published by the 
 *Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */
#include <iostream>
#include "mlview-prefs.h"
#include "mlview-prefs-category-treeview.h"
#include "mlview-prefs-category-frame-treeview.h"

namespace mlview
{
class ModelColumns : public Gtk::TreeModel::ColumnRecord
{
public:
    ModelColumns() {
	add (m_col_id);
	add (m_col_label);
    }

    Gtk::TreeModelColumn<Glib::ustring> m_col_id;
    Gtk::TreeModelColumn<Glib::ustring> m_col_label;
};

struct PrefsCategoryFrameTreeviewPriv
{
	PrefsCategoryTreeview *m_prefs;
	Gtk::SpinButton  *m_tree_depth_button;
	Gtk::CheckButton *m_enable_cbox_check_button;
	Gtk::FontButton  *m_font_button;
	Gtk::ColorButton *m_fg_color_button;
	Gtk::ColorButton *m_bg_color_button;
	Gtk::Button      *m_reset_to_default_button;
	Gtk::TreeView    *m_colors_list;
	ModelColumns      m_tree_columns;
	Glib::RefPtr
	<Gtk::ListStore>  m_ref_treemodel;
	Glib::RefPtr
	<Gtk::TreeSelection> m_ref_treeselection;

	void setup_ui (Glib::RefPtr<Gnome::Glade::Xml> i_glade_xml);
	void setup_event_handlers ();

	// Callbacks
	void on_color_selection ();
	void on_tree_depth_button_value_changed ();
	void on_enable_cbox_check_button_clicked ();
	void on_font_button_font_set ();
	void on_fg_color_button_color_set ();
	void reset_to_default ();

	PrefsCategoryFrameTreeviewPriv ():
		m_prefs (NULL),
		m_tree_depth_button (NULL),
		m_enable_cbox_check_button (NULL),
		m_font_button (NULL),
		m_fg_color_button (NULL),
		m_bg_color_button (NULL),
		m_reset_to_default_button (NULL),
		m_colors_list (NULL)
	{}
};

PrefsCategoryFrameTreeview::PrefsCategoryFrameTreeview ()
    : PrefsCategoryFrame ("prefs_category_box_treeview")
{
    Glib::RefPtr<Gnome::Glade::Xml> l_refxml = this->get_gladexml_ref ();

    m_priv = new PrefsCategoryFrameTreeviewPriv ();
    m_priv->m_prefs = dynamic_cast<PrefsCategoryTreeview*>
	(Preferences::get_instance ()->get_category_by_id ("treeview"));
	THROW_IF_FAIL (m_priv->m_prefs) ;

    m_priv->setup_ui (l_refxml);
    m_priv->setup_event_handlers ();
}

PrefsCategoryFrameTreeview::~PrefsCategoryFrameTreeview ()
{
    if (m_priv != NULL) {
		delete m_priv;
		m_priv = NULL ;
	}
}

void
PrefsCategoryFrameTreeviewPriv::setup_ui (
    Glib::RefPtr<Gnome::Glade::Xml> a_glade_xml_ref)
{
	THROW_IF_FAIL (a_glade_xml_ref) ;

// Get widgets handle
    a_glade_xml_ref->get_widget ("tree_depth_button",
				 m_tree_depth_button);
	THROW_IF_FAIL (m_tree_depth_button) ;

    a_glade_xml_ref->get_widget ("enable_cbox_check_button",
				 m_enable_cbox_check_button);
	THROW_IF_FAIL (m_enable_cbox_check_button) ;

    a_glade_xml_ref->get_widget ("treeview_fontbutton",
				 m_font_button);
	THROW_IF_FAIL (m_font_button) ;

    a_glade_xml_ref->get_widget ("treeview_fg_colorbutton",
				 m_fg_color_button);
	THROW_IF_FAIL (m_fg_color_button) ;

    a_glade_xml_ref->get_widget ("treeview_bg_colorbutton",
				 m_bg_color_button);
	THROW_IF_FAIL (m_bg_color_button) ;

    a_glade_xml_ref->get_widget ("treeview_colors_list",
				 m_colors_list);
	THROW_IF_FAIL (m_colors_list) ;

    a_glade_xml_ref->get_widget ("prefs_category_treeview_reset_button",
				 m_reset_to_default_button);
	THROW_IF_FAIL (m_reset_to_default_button) ;

	THROW_IF_FAIL (m_prefs) ;

    m_tree_depth_button->set_value (
	(double)m_prefs->get_default_tree_expansion_depth ());

    m_enable_cbox_check_button->set_active (
	m_prefs->enable_completion_box ());

    m_font_button->set_font_name (
	m_prefs->get_font_name ());

// Create list model
    m_ref_treemodel =
	Gtk::ListStore::create (m_tree_columns);
    m_colors_list->set_model (m_ref_treemodel);
    m_colors_list->append_column
	("label", m_tree_columns.m_col_label);

// Define list selection mode
    m_ref_treeselection= m_colors_list->get_selection();
    m_ref_treeselection->set_mode (Gtk::SELECTION_SINGLE);

// Populate list model
    Gtk::TreeModel::Row row;

    row = *(m_ref_treemodel->append ());
    row[m_tree_columns.m_col_id]     = "xml-attribute-node";
    row[m_tree_columns.m_col_label]  = "Attributes names";
    m_ref_treeselection->select (row);

    row = *(m_ref_treemodel->append ());
    row[m_tree_columns.m_col_id]     = "xml-attribute-value-node";
    row[m_tree_columns.m_col_label]  = "Attributes values";

    row = *(m_ref_treemodel->append ());
    row[m_tree_columns.m_col_id]     = "xml-comment-node";
    row[m_tree_columns.m_col_label]  = "Comments";

    row = *(m_ref_treemodel->append ());
    row[m_tree_columns.m_col_id]     = "xml-document-node";
    row[m_tree_columns.m_col_label]  = "Document";

    row = *(m_ref_treemodel->append ());
    row[m_tree_columns.m_col_id]     = "xml-dtd-node";
    row[m_tree_columns.m_col_label]  = "DTD";

    row = *(m_ref_treemodel->append ());
    row[m_tree_columns.m_col_id]     = "xml-element-node";
    row[m_tree_columns.m_col_label]  = "Element";

    row = *(m_ref_treemodel->append ());
    row[m_tree_columns.m_col_id]     = "xml-entity-decl-node";
    row[m_tree_columns.m_col_label]  = "Entity Declaration";

    row = *(m_ref_treemodel->append ());
    row[m_tree_columns.m_col_id]     = "xml-pi-node";
    row[m_tree_columns.m_col_label]  = "Processing Instruction";

    row = *(m_ref_treemodel->append ());
    row[m_tree_columns.m_col_id]     = "xml-text-node";
    row[m_tree_columns.m_col_label]  = "Text";

// Set fg button color (ugly :)
    const Gtk::TreeModel::iterator selected =
	m_colors_list->get_selection()->get_selected();
    Glib::ustring l_type_id =
	(*selected)[m_tree_columns.m_col_id];

    Glib::ustring color_str =
	m_prefs->get_color_for_type (l_type_id);

    m_fg_color_button->set_color (Gdk::Color (color_str));

}

void
PrefsCategoryFrameTreeviewPriv::setup_event_handlers ()
{
    m_tree_depth_button->signal_value_changed ().connect
	(sigc::mem_fun (*this,
			&PrefsCategoryFrameTreeviewPriv
			::on_tree_depth_button_value_changed));

    m_enable_cbox_check_button->signal_clicked ().connect
	(sigc::mem_fun (*this,
			&PrefsCategoryFrameTreeviewPriv
			::on_enable_cbox_check_button_clicked));

    m_font_button->signal_font_set ().connect
	(sigc::mem_fun (*this,
			&PrefsCategoryFrameTreeviewPriv
			::on_font_button_font_set));

    m_fg_color_button->signal_color_set ().connect
	(sigc::mem_fun (*this,
			&PrefsCategoryFrameTreeviewPriv
			::on_fg_color_button_color_set));

    m_ref_treeselection->signal_changed ().connect
	(sigc::mem_fun (*this,
			&PrefsCategoryFrameTreeviewPriv
			::on_color_selection));

    m_reset_to_default_button->signal_clicked ().connect
	(sigc::mem_fun (*this,
			&PrefsCategoryFrameTreeviewPriv
			::reset_to_default));
}

void
PrefsCategoryFrameTreeviewPriv::on_tree_depth_button_value_changed ()
{
    m_prefs->set_default_tree_expansion_depth (
	static_cast<int>(m_tree_depth_button->get_value ()));
}

void
PrefsCategoryFrameTreeviewPriv::on_enable_cbox_check_button_clicked ()
{
    m_prefs->set_enable_completion_box (
	m_enable_cbox_check_button->get_active ());
}

void
PrefsCategoryFrameTreeviewPriv::on_font_button_font_set ()
{
    m_prefs->set_font_name (
	m_font_button->get_font_name ());
}

void
PrefsCategoryFrameTreeviewPriv::on_fg_color_button_color_set ()
{
    const Gtk::TreeModel::iterator selected =
	m_colors_list->get_selection()->get_selected();
    Glib::ustring l_type_id =
	(*selected)[m_tree_columns.m_col_id];

    m_prefs->set_color_for_type (l_type_id,
				 m_fg_color_button->get_color ());
}

void
PrefsCategoryFrameTreeviewPriv::on_color_selection ()
{
    const Gtk::TreeModel::iterator selected =
	m_colors_list->get_selection()->get_selected();
    Glib::ustring l_type_id =
	(*selected)[m_tree_columns.m_col_id];

    Glib::ustring color_str =
	m_prefs->get_color_for_type (l_type_id);

    m_fg_color_button->set_color (Gdk::Color (color_str));
}

void
PrefsCategoryFrameTreeviewPriv::reset_to_default ()
{
    m_tree_depth_button->set_value (
	(double)m_prefs->get_default_tree_expansion_depth_default ());

    m_enable_cbox_check_button->set_active (
	m_prefs->enable_completion_box_default ());

    m_font_button->set_font_name (
	m_prefs->get_default_font_name ());
// Need to update fontname pref manually
    m_prefs->set_font_name (
	m_font_button->get_font_name ());

    typedef Gtk::TreeModel::Children type_children; //minimise code length.
    type_children children = m_ref_treemodel->children();
    for(type_children::iterator iter = children.begin();
	iter != children.end(); ++iter)
    {
	Gtk::TreeModel::Row row = *iter;
	Glib::ustring l_type_id = row[m_tree_columns.m_col_id];
	m_prefs->set_color_for_type
	    (l_type_id,
	     m_prefs->get_default_color_for_type (l_type_id));
    }

// Set fg button color (ugly :)
    const Gtk::TreeModel::iterator selected =
	m_colors_list->get_selection()->get_selected();
    Glib::ustring l_type_id =
	(*selected)[m_tree_columns.m_col_id];

    Glib::ustring color_str =
	m_prefs->get_color_for_type (l_type_id);

    m_fg_color_button->set_color (Gdk::Color (color_str));

}

} // namespace mlview
