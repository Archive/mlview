/* -*- Mode: C; indent-tabs-mode:nil; c-basic-offset: 8-*- */

/*
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute 
 *it and/or modify it under the terms of 
 *the GNU General Public License as published by the 
 *Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */

#include <string.h>
#include <stdio.h>
#include "mlview-utils.h"

#ifdef MLVIEW_WITH_DBUS

#include "mlview-dbus-cli.h"


struct MlViewCtlOptions
{
	gboolean display_help ;
	gboolean display_version ;
} ;

static enum MlViewDBusCliStatus parse_command_line
(int a_argc, char **a_argv,
 struct MlViewCtlOptions *a_options,
 GError **a_error) ;

static enum MlViewDBusCliStatus display_help (int a_argc, char **a_argv) ;

static enum MlViewDBusCliStatus display_version (int a_argc, char** a_argv) ;

static enum MlViewDBusCliStatus
display_help (int a_argc, char **a_argv)
{
	printf ("usage: %s [options] [commands] [command specific options]\n",
	        a_argv[0]) ;
	printf ("where options can be:\n") ;
	printf ("--help                                                          :"
	        " display this help\n") ;
	printf ("--version                                                       :"
	        " display the version of this tool\n") ;

	printf ("******************************\n") ;
	printf ("******************************\n") ;

	printf ("available commands are:\n") ;

	printf ("list-services                                                   :"
	        "list the available mlview services\n") ;

	printf ("ping-service <service-name>                                     :"
	        " ping a given mlview service\n") ;

	printf ("load-xml-file <service-name> <xml-file-uri> [dtd-uri]           :"
	        " load a given xml document\n") ;

	printf ("close-application <service-name>                                :"
	        " close the application that hosts the service\n") ;
	return MLVIEW_DBUS_CLI_OK ;
}

static enum MlViewDBusCliStatus
display_version (int a_argc, char** a_argv)
{
	printf ("0.0.1\n") ;
	return MLVIEW_DBUS_CLI_OK ;
}

static enum MlViewDBusCliStatus
parse_command_line (int a_argc, char **a_argv,
                    struct MlViewCtlOptions *a_options,
                    GError **a_error)
{
	int i=0 ;

	enum MlViewDBusCliStatus status = MLVIEW_DBUS_CLI_OK ;
	g_return_val_if_fail (a_options, MLVIEW_DBUS_CLI_BAD_PARAM_ERROR) ;

	for (i=1 ; i < a_argc ; i++)
	{

		if (a_argv[i][0] != '-') {
			goto try_to_recon_command ;
		}

		if (!strcmp (a_argv[i], "--help")) {
			a_options->display_help = TRUE ;
			break ;
		} else if (!strcmp (a_argv[i], "--version")) {
			a_options->display_version = TRUE ;
			break ;
		} else {
			a_options->display_help = TRUE ;
			break ;
		}
	}
	a_options->display_help = TRUE ;
	status = MLVIEW_OK ;
	goto out ;

try_to_recon_command:
	if (   !strcmp (a_argv[i], "list-services")
	        || !strcmp (a_argv[i], "ping-service"))
	{}
	else
	{
		status = MLVIEW_DBUS_CLI_UNKNOWN_COMMAND_ERROR ;
		if (a_error) {
			g_set_error (a_error,
			             g_quark_from_string ("MLVIEW_CTL"),
			             status,
			             "unknown command:%s\n",
			             a_argv[i]) ;
		}
	}
out:
	return status ;
}

int
main (int argc, char** argv)
{
	MlViewDBusCli *cli = NULL ; /*a command line iface class*/
	enum MlViewDBusCliStatus status = MLVIEW_DBUS_CLI_OK ;
	struct MlViewCtlOptions options = {
		                                  0
	                                  } ;

	gtk_init (&argc, &argv) ;

	parse_command_line (argc, argv, &options, NULL) ;

	if (options.display_help) {
		display_help (argc, argv) ;
		goto cleanup ;
	}

	if (options.display_version) {
		display_version (argc, argv) ;
		goto cleanup ;
	}

	cli = mlview_dbus_cli_new () ;
	if (!cli) {
		mlview_utils_trace_debug ("failed to instanciate "
		                          "the command line interface "
		                          "object") ;
		status = MLVIEW_DBUS_CLI_ERROR ;
		goto cleanup ;
	}
	status = mlview_dbus_cli_parse_command_line (cli, argc, argv) ;
	if (status != MLVIEW_DBUS_CLI_OK) {
		display_help (argc, argv) ;
	}
	status = mlview_dbus_cli_execute_command (cli) ;

cleanup:
	if (cli) {
		g_object_unref (G_OBJECT (cli)) ;
		cli = NULL ;
	}
	return status ;
}
#else
int
main (int argc, char **argv)
{
	printf ("no dbus support enabled") ;
	return 0 ;
}
#endif /*MLVIEW_WITH_DBUS*/
