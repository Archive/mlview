/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 8-*- */

/*This file is part of GNU MlView
 *
 *GNU MlView is free software; 
 *you can redistribute it and/or modify it under the terms of 
 *the GNU General Public License as published by 
 *the Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope that it will be useful, 
 *but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General 
 *Public License along with MlView; 
 *see the file COPYING. If not, write to the 
 *Free Software Foundation, Inc., 59 
 *Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright informations.
 */

#include "string.h"
#include "mlview-dbus-cli.h"
#include "mlview-ping-dbc.h"
#include "mlview-editor-dbc.h"

#ifdef MLVIEW_WITH_DBUS

#define PRIVATE(obj) (obj)->priv


struct _MlViewDBusCliPriv
{
	struct MlViewDBusCliOptions options ;
	gboolean dispose_has_run ;
} ;

static void mlview_dbus_cli_init (MlViewDBusCli *a_this) ;
static void mlview_dbus_cli_class_init (MlViewDBusCliClass *a_klass) ;
static void mlview_dbus_cli_dispose (GObject *a_this) ;
static void mlview_dbus_cli_finalize (GObject *a_this) ;
static enum MlViewDBusCliStatus string_to_command
(const gchar *a_string,
 enum MlViewDBusCliCommand *a_command) ;

static GObjectClass *gv_parent_class = NULL ;

/*****************
 * private methods
 *****************/

static void
mlview_dbus_cli_class_init (MlViewDBusCliClass *a_klass)
{
	GObjectClass *gobject_class = NULL ;

	g_return_if_fail (a_klass != NULL) ;
	gv_parent_class = g_type_class_peek_parent (a_klass) ;
	g_return_if_fail (gv_parent_class) ;
	gobject_class = G_OBJECT_CLASS (a_klass) ;

	gobject_class->dispose = mlview_dbus_cli_dispose ;
	gobject_class->finalize = mlview_dbus_cli_finalize ;
}

static void
mlview_dbus_cli_init (MlViewDBusCli *a_this)
{
	g_return_if_fail (a_this && MLVIEW_IS_DBUS_CLI (a_this)) ;

	PRIVATE (a_this) = g_try_malloc (sizeof (MlViewDBusCliPriv));
	if (!PRIVATE (a_this)) {
		mlview_utils_trace_debug ("malloc failed") ;
		return ;
	}
	memset (PRIVATE (a_this), 0, sizeof (MlViewDBusCliPriv)) ;
}

static void
mlview_dbus_cli_dispose (GObject *a_this)
{
	MlViewDBusCli * thiz = NULL ;

	g_return_if_fail (a_this && MLVIEW_IS_DBUS_CLI (a_this)) ;
	thiz = MLVIEW_DBUS_CLI (a_this) ;
	g_return_if_fail (thiz) ;

	if (!PRIVATE (thiz)) {
		return ;
	}
	if (PRIVATE (thiz)->dispose_has_run) {
		return ;
	}

	if (gv_parent_class->dispose)  {
		gv_parent_class->dispose (a_this) ;
	}
	PRIVATE (thiz)->dispose_has_run = TRUE ;
}

static void
mlview_dbus_cli_finalize (GObject *a_this)
{
	MlViewDBusCli *thiz = NULL ;

	g_return_if_fail (a_this && MLVIEW_IS_DBUS_CLI (a_this)) ;
	thiz = MLVIEW_DBUS_CLI (a_this) ;
	g_return_if_fail (thiz) ;

	if (PRIVATE (thiz)) {
		g_free (PRIVATE (thiz)) ;
		PRIVATE (thiz) = NULL ;
	}
}

static enum MlViewDBusCliStatus
string_to_command (const gchar *a_string,
                   enum MlViewDBusCliCommand *a_command)
{
	enum MlViewDBusCliStatus status = MLVIEW_DBUS_CLI_UNKNOWN_COMMAND_ERROR ;

	g_return_val_if_fail (a_string && a_command,
	                      MLVIEW_DBUS_CLI_BAD_PARAM_ERROR) ;

	if (!strcmp (a_string, "list-services")) {

		*a_command = MLVIEW_DBUS_CLI_LIST_SERVICES ;

	} else if (!strcmp (a_string, "ping-service")) {

		*a_command = MLVIEW_DBUS_CLI_PING_SERVICE ;

	} else if (!strcmp (a_string, "load-xml-file")) {

		*a_command = MLVIEW_DBUS_CLI_LOAD_XML_FILE ;

	} else if (!strcmp (a_string, "close-application")) {

		*a_command = MLVIEW_DBUS_CLI_CLOSE_APPLICATION ;

	} else {

		status = MLVIEW_DBUS_CLI_UNKNOWN_COMMAND_ERROR ;
	}

	return status ;
}

/****************
 * public methods
 ****************/

GType
mlview_dbus_cli_get_type (void)
{
	static GType type = 0 ;

	if (!type) {
		static const GTypeInfo type_info = {
		                                       sizeof (MlViewDBusCliClass),
		                                       NULL, NULL,
		                                       (GClassInitFunc) mlview_dbus_cli_class_init,
		                                       NULL, NULL,
		                                       sizeof (MlViewDBusCliClass),
		                                       0,
		                                       (GInstanceInitFunc) mlview_dbus_cli_init
		                                   } ;
		type = g_type_register_static (G_TYPE_OBJECT,
		                               "MlViewDBusCli",
		                               &type_info, 0) ;
	}
	return type ;
}

MlViewDBusCli *
mlview_dbus_cli_new (void)
{
	MlViewDBusCli *result = NULL ;

	result = g_object_new (MLVIEW_TYPE_DBUS_CLI, NULL) ;
	return result ;
}

enum MlViewDBusCliStatus
mlview_dbus_cli_parse_command_line (MlViewDBusCli *a_this,
                                    gint a_argc,
                                    gchar **a_argv)
{
	gint i = 0 ;
	enum MlViewDBusCliCommand command = MLVIEW_DBUS_CLI_UNKNOWN_COMMAND ;

	if (a_argc < 2) {
		PRIVATE (a_this)->options.display_help  = TRUE ;
		return MLVIEW_DBUS_CLI_DISPLAY_HELP_ERROR ;
	}

	string_to_command (a_argv[1], &command) ;

	if (command == MLVIEW_DBUS_CLI_UNKNOWN_COMMAND) {
		return MLVIEW_DBUS_CLI_UNKNOWN_COMMAND_ERROR ;
	}
	PRIVATE (a_this)->options.command = command ;

	for (i = 2 ; i < a_argc ; i++) {
		if (a_argv[i][0] != '-') {
			break ;
		}

		switch (command) {
		case MLVIEW_DBUS_CLI_LIST_SERVICES:
			/*
			 * put here parsing of the options
			 * of the "list-service command
			 */
			break ;

		case MLVIEW_DBUS_CLI_PING_SERVICE:
			/*
			 * put here parsing of the "ping-service" command
			 */
			break ;

		case MLVIEW_DBUS_CLI_LOAD_XML_FILE:
			break ;

		case MLVIEW_DBUS_CLI_CLOSE_APPLICATION:
			break ;

		default:
			g_assert_not_reached () ;
		}
	}
	switch (command) {
	case MLVIEW_DBUS_CLI_LIST_SERVICES:
		/*
		 * put here parsing of the argument 
		 * of the "list-service command
		 */
		break ;

	case MLVIEW_DBUS_CLI_PING_SERVICE:
		/*
		 * put here parsing of the "ping-service" command
		 * arguments
		 */
		if (!a_argv[i]) {
			PRIVATE (a_this)->options.display_help = TRUE ;
			return MLVIEW_DBUS_CLI_DISPLAY_HELP_ERROR ;
		}
		PRIVATE (a_this)->options.service_name = a_argv[i] ;
		break ;

	case MLVIEW_DBUS_CLI_LOAD_XML_FILE:
		if (!a_argv[i]) {
			PRIVATE (a_this)->options.display_help = TRUE ;
			return MLVIEW_DBUS_CLI_DISPLAY_HELP_ERROR ;
		}
		PRIVATE (a_this)->options.service_name = a_argv[i] ;
		if (i + 1 >= a_argc || !a_argv[i + 1]) {
			PRIVATE (a_this)->options.display_help = TRUE ;
			return MLVIEW_DBUS_CLI_DISPLAY_HELP_ERROR ;
		}

		PRIVATE (a_this)->options.xml_file_uri =  a_argv[i + 1] ;
		if (i + 2 < a_argc) {
			PRIVATE (a_this)->options.dtd_uri = a_argv[i + 2] ;
		}
		break ;

	case MLVIEW_DBUS_CLI_CLOSE_APPLICATION:
		if (i >= a_argc || !a_argv[i]) {
			PRIVATE (a_this)->options.display_help = TRUE  ;
			return MLVIEW_DBUS_CLI_DISPLAY_HELP_ERROR ;
		}
		PRIVATE (a_this)->options.service_name = a_argv[i] ;
		break ;
	default:
		g_assert_not_reached () ;
	}
	return MLVIEW_DBUS_CLI_OK ;
}

enum MlViewDBusCliStatus
mlview_dbus_cli_execute_command (MlViewDBusCli *a_this)
{
	enum MlViewDBusCliStatus status = MLVIEW_DBUS_CLI_OK ;

	switch (PRIVATE (a_this)->options.command) {
	case MLVIEW_DBUS_CLI_LIST_SERVICES:
		mlview_dbus_cli_list_services (a_this) ;
		break ;

	case MLVIEW_DBUS_CLI_PING_SERVICE:
		mlview_dbus_cli_ping_service (a_this) ;
		break ;

	case MLVIEW_DBUS_CLI_LOAD_XML_FILE:
		mlview_dbus_cli_load_xml_file (a_this) ;
		break ;

	case MLVIEW_DBUS_CLI_CLOSE_APPLICATION:
		mlview_dbus_cli_close_application (a_this) ;
		break ;

	default:
		g_assert_not_reached () ;
	}
	return status ;
}

enum MlViewDBusCliStatus
mlview_dbus_cli_get_options (MlViewDBusCli *a_this,
                             struct MlViewDBusCliOptions **a_options)

{
	g_return_val_if_fail (a_this && MLVIEW_IS_DBUS_CLI (a_this)
	                      && PRIVATE (a_this),
	                      MLVIEW_DBUS_CLI_BAD_PARAM_ERROR) ;

	*a_options = &PRIVATE (a_this)->options ;
	return MLVIEW_DBUS_CLI_OK ;
}

enum MlViewDBusCliStatus
mlview_dbus_cli_list_services (MlViewDBusCli *a_this)
{
	enum MlViewStatus status = MLVIEW_OK ;
	MlViewPingDBC *ping_proxy = NULL ;
	gchar ** active_services = NULL ;
	guint len = 0 ;

	g_return_val_if_fail (a_this && MLVIEW_IS_DBUS_CLI (a_this),
	                      MLVIEW_DBUS_CLI_BAD_PARAM_ERROR) ;

	ping_proxy = mlview_ping_dbc_new () ;
	if (!ping_proxy) {
		mlview_utils_trace_debug ("could not instanciate ping client") ;
		return MLVIEW_DBUS_CLI_ERROR ;
	}
	status = mlview_ping_dbc_list_active_services (ping_proxy,
	         &active_services,
	         &len) ;
	if (status) {
		mlview_utils_trace_debug ("service invocation failed") ;
		return MLVIEW_DBUS_CLI_SERVICE_INVOCATION_FAILED_ERROR ;
	}

	if (len && active_services) {
		guint i = 0 ;
		for (i = 0 ; i < len; i++) {
			printf ("%s\n", active_services[i]) ;
		}
		if (active_services) {
			mlview_ping_dbc_free_list_of_service_names
			(active_services, len) ;
			active_services = NULL ;
		}
	}

	return MLVIEW_DBUS_CLI_OK ;
}

enum MlViewDBusCliStatus
mlview_dbus_cli_ping_service (MlViewDBusCli *a_this)
{
	enum MlViewDBusCliStatus status2 = MLVIEW_DBUS_CLI_OK ;
	enum MlViewStatus status = MLVIEW_OK ;
	MlViewPingDBC *ping_proxy = NULL ;

	g_return_val_if_fail (a_this && MLVIEW_IS_DBUS_CLI (a_this)
	                      && PRIVATE (a_this),
	                      MLVIEW_DBUS_CLI_BAD_PARAM_ERROR) ;

	ping_proxy = mlview_ping_dbc_new () ;
	g_return_val_if_fail (ping_proxy,
	                      MLVIEW_DBUS_CLI_ERROR) ;

	g_return_val_if_fail (PRIVATE (a_this)->options.service_name,
	                      MLVIEW_DBUS_CLI_ERROR) ;

	status = mlview_ping_dbc_ping (ping_proxy,
	                               PRIVATE (a_this)->options.service_name) ;
	if (status) {
		printf ("failed\n") ;
		status2 = MLVIEW_DBUS_CLI_PING_FAILED_ERROR ;
	} else {
		printf ("succeeded\n") ;
		status2 = MLVIEW_DBUS_CLI_OK ;
	}

	g_object_unref (ping_proxy) ;
	return status2 ;
}

enum MlViewDBusCliStatus
mlview_dbus_cli_load_xml_file (MlViewDBusCli *a_this)
{
	MlViewEditorDBC *editor_proxy = NULL ;
	enum MlViewDBusCliStatus status2 = MLVIEW_DBUS_CLI_OK ;
	enum MlViewStatus status = MLVIEW_OK ;

	g_return_val_if_fail (a_this && MLVIEW_IS_DBUS_CLI (a_this)
	                      && PRIVATE (a_this),
	                      MLVIEW_DBUS_CLI_BAD_PARAM_ERROR) ;

	g_return_val_if_fail (PRIVATE (a_this)->options.xml_file_uri,
	                      MLVIEW_DBUS_CLI_BAD_PARAM_ERROR) ;

	editor_proxy = mlview_editor_dbc_new () ;
	g_return_val_if_fail (editor_proxy, MLVIEW_DBUS_CLI_ERROR) ;

	status = mlview_editor_dbc_load_xml_file_with_dtd
	         (editor_proxy,
	          PRIVATE (a_this)->options.service_name,
	          PRIVATE (a_this)->options.xml_file_uri,
	          PRIVATE (a_this)->options.dtd_uri) ;
	if (status) {
		status2 = MLVIEW_DBUS_CLI_LOAD_XML_FILE_FAILED_ERROR ;
	} else {
		status2= MLVIEW_DBUS_CLI_OK ;
	}
	if (status2) {
		printf ("failed\n") ;
	} else {
		printf ("succeeded\n") ;
	}
	g_object_unref (editor_proxy) ;

	return status2 ;
}

enum MlViewDBusCliStatus
mlview_dbus_cli_close_application (MlViewDBusCli *a_this)
{
	MlViewPingDBC *ping_proxy = NULL ;
	enum MlViewStatus status = MLVIEW_OK ;
	enum MlViewDBusCliStatus status2 = MLVIEW_DBUS_CLI_OK ;

	g_return_val_if_fail (a_this && MLVIEW_IS_DBUS_CLI (a_this)
	                      && PRIVATE (a_this),
	                      MLVIEW_DBUS_CLI_BAD_PARAM_ERROR) ;

	ping_proxy = mlview_ping_dbc_new () ;
	g_return_val_if_fail (ping_proxy, MLVIEW_DBUS_CLI_BAD_PARAM_ERROR) ;
	g_return_val_if_fail (PRIVATE (a_this)->options.service_name,
	                      MLVIEW_DBUS_CLI_BAD_PARAM_ERROR) ;

	status = mlview_ping_dbc_close_application
	         (ping_proxy,
	          PRIVATE (a_this)->options.service_name) ;
	if (status) {
		status2 = MLVIEW_DBUS_CLI_CLOSE_APPLICATION_FAILED_ERROR ;
		printf ("failed\n") ;
	} else {
		status2 = MLVIEW_DBUS_CLI_OK ;
		printf ("suceeded\n") ;
	}
	if (ping_proxy) {
		g_object_unref (ping_proxy) ;
		ping_proxy = NULL ;
	}
	return status2 ;
}


#endif /*MLVIEW_WITH_DBUS*/

