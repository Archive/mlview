/* -*- Mode: C; indent-tabs-mode:nil; c-basic-offset: 8-*- */

/*
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute 
 *it and/or modify it under the terms of 
 *the GNU General Public License as published by the 
 *Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */

#ifndef __MLVIEW_NS_EDITOR_H__
#define __MLVIEW_NS_EDITOR_H__

#include <libxml/tree.h>
#include "mlview-xml-document.h"
#include "mlview-app-context.h"

/**
 *@file
 *The declaration of the #MlViewNSEditor class.
 */

G_BEGIN_DECLS
#define MLVIEW_TYPE_NS_EDITOR (mlview_ns_editor_get_type())
#define MLVIEW_NS_EDITOR(object) (GTK_CHECK_CAST(object, MLVIEW_TYPE_NS_EDITOR, MlViewNSEditor))
#define MLVIEW_NS_EDITOR_CLASS(klass) (GTK_CHECK_CLASS_CAST(klass, MLVIEW_TYPE_NS_EDITOR, MlViewNSEditorClass))
#define MLVIEW_IS_NS_EDITOR(object) (GTK_CHECK_TYPE(object, MLVIEW_TYPE_NS_EDITOR))
#define MLVIEW_IS_NS_EDITOR_CLASS(klass) (GTK_CHECK_CLASS_CAST(klass, MLVIEW_TYPE_NS_EDITOR))

typedef struct _MlViewNSEditor MlViewNSEditor;
typedef struct _MlViewNSEditorClass MlViewNSEditorClass;
typedef struct _MlViewNSEditorPrivate MlViewNSEditorPrivate;

/**
 *The MlViewNSEditor class is an editing widget that
 *nows how to edit the namespaces accessible from a given
 *xmlNode *. It lets the user add/remove/change namespaces
 *on a given xml element.
 *THis widget inherits the GtkVBox widget.
 */
struct _MlViewNSEditor
{
	GtkVBox container;      /*parent object */
	MlViewNSEditorPrivate *priv;
};

struct _MlViewNSEditorClass
{
	GtkVBoxClass parent_class;
	void (*namespace_added) (MlViewNSEditor *a_editor,
	                         xmlNs * a_namespace,
	                         gpointer a_data);
	void (*namespace_prefix_changed) (MlViewNSEditor *a_editor,
	                                  xmlNs * a_namespace,
	                                  gpointer a_data);
	void (*namespace_uri_changed) (MlViewNSEditor *a_editor,
	                               xmlNs * a_namespace,
	                               gpointer a_data);
	void (*namespace_changed) (MlViewNSEditor *a_editor,
	                           xmlNs * a_namespace,
	                           gpointer a_data);
	void (*namespace_deleted) (MlViewNSEditor *a_editor,
	                           xmlNs * a_namespace,
	                           gpointer a_data);
};

guint mlview_ns_editor_get_type (void);

GtkWidget * mlview_ns_editor_new (MlViewXMLDocument *a_doc) ;

enum MlViewStatus mlview_ns_editor_set_xml_doc (MlViewNSEditor *a_this,
        MlViewXMLDocument *a_doc) ;

GtkTreeModel * mlview_ns_editor_get_model (MlViewNSEditor *a_this) ;

GtkTreeRowReference * mlview_ns_editor_get_row_ref_from_iter (MlViewNSEditor *a_this,
        GtkTreeIter *a_iter,
        gboolean a_create_if_not_exists) ;

enum MlViewStatus mlview_ns_editor_edit_namespace (MlViewNSEditor * a_this,
        xmlNs * a_ns,
        gboolean a_selectable) ;

GtkTreeRowReference * mlview_ns_editor_get_row_ref_from_ns (MlViewNSEditor *a_this,
        xmlNs *a_ns) ;

GtkTreeRowReference * mlview_ns_editor_get_row_ref2 (MlViewNSEditor *a_this,
        GtkTreeIter *a_iter,
        gboolean a_create_if_not_exists) ;

enum MlViewStatus mlview_ns_editor_get_cur_sel_start (MlViewNSEditor *a_this,
        GtkTreeIter *a_iter) ;

xmlNs * mlview_ns_editor_add_namespace (MlViewNSEditor * a_this,
                                        gchar * a_ns_prefix,
                                        gchar * a_ns_uri) ;

enum MlViewStatus mlview_ns_editor_remove_namespace (MlViewNSEditor * a_this,
        xmlNs * a_ns) ;

enum MlViewStatus mlview_ns_editor_update_ns_removed (MlViewNSEditor * a_this,
        xmlNode *a_node,
        xmlNs * a_ns) ;

enum MlViewStatus mlview_ns_editor_set_current_selected_row (MlViewNSEditor *a_this,
        GtkTreeIter *a_iter) ;

enum MlViewStatus mlview_ns_editor_edit_node_visible_namespaces (MlViewNSEditor * a_editor,
        xmlNode * a_xml_node) ;

gboolean mlview_ns_editor_is_row_the_add_new_ns_row (MlViewNSEditor *a_this,
        GtkTreeIter *a_iter) ;

enum MlViewStatus mlview_ns_editor_clear (MlViewNSEditor *a_editor);

xmlNs *mlview_ns_editor_get_current_selected_ns (MlViewNSEditor * a_editor);

xmlNode *mlview_ns_editor_get_current_xml_node (MlViewNSEditor * a_editor);

xmlNs *mlview_ns_editor_add_namespace_def (MlViewNSEditor * a_editor, gchar * a_ns_prefix,
        gchar * a_ns_uri, xmlNode * a_xml_node);

enum MlViewStatus mlview_ns_editor_update_ns_added (MlViewNSEditor *a_this,
        xmlNode *a_node,
        xmlNs *a_ns) ;

enum MlViewStatus mlview_ns_editor_update_ns (MlViewNSEditor *a_this,
        xmlNode *a_node,
        xmlNs *a_ns) ;

enum MlViewStatus mlview_ns_editor_remove_namespace_def (MlViewNSEditor * a_editor, xmlNs * a_ns,
        xmlNode * a_xml_node);

enum MlViewStatus mlview_ns_editor_connect_to_doc (MlViewNSEditor *a_this,
        MlViewXMLDocument *a_doc) ;

enum MlViewStatus mlview_ns_editor_disconnect_from_doc (MlViewNSEditor *a_this,
        MlViewXMLDocument *a_doc) ;

enum MlViewStatus mlview_ns_editor_enable_node_alteration (MlViewNSEditor *a_this,
        gboolean a_enable) ;
G_END_DECLS

#endif /*__MLVIEW_NS_EDITOR_H__*/
