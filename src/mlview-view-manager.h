/* -*- Mode: C++; indent-tabs-mode: nil; c-basic-offset: 4-*- */

/*This file is part of GNU MlView
 *
 *GNU MlView is free software; 
 *you can redistribute it and/or modify it under the terms of 
 *the GNU General Public License as 
 *published by the Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope 
 *that it will be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */
#ifndef __MLVIEW_VIEW_MANAGER_H__
#define __MLVIEW_VIEW_MANAGER_H__

#include <list>
#include <gtkmm.h>
#include "mlview-object.h"
#include "mlview-ustring.h"
#include "mlview-app-context.h"
#include "mlview-iview.h"
#include "mlview-safe-ptr-utils.h"
#include "mlview-gvc-iface.h"

using namespace std ;

namespace mlview
{
struct ViewManagerPriv ;
class ViewManager : public Object {
	friend struct ViewManagerPriv ;
	ViewManagerPriv *m_priv ;

	//forbid copy and assignation
	ViewManager (const ViewManager &a_view_manager) ;
	ViewManager& operator= (const ViewManager &a_view_manager) ;

public:

	ViewManager
	(const UString &a_graphical_view_container_type_name="OldGVC") ;

	~ViewManager () ;

	Gtk::Widget* get_embeddable_container_widget () ;

	IView* create_new_view_for_document (MlViewXMLDocument *a_doc,
			const UString &a_view_type) ;

	enum MlViewStatus insert_view (IView *a_view,
	                               long a_index=-1/*append*/) ;

	list<IView*> get_views_of_document
	(const MlViewXMLDocument *a_this) const ;

	enum MlViewStatus remove_view (IView *a_view) ;

	list<IView*> get_all_views (void) const ;

	gint get_number_of_views_opened_with_doc (MlViewXMLDocument *a_doc) const;

	gint get_number_of_open_documents () const ;

	list<MlViewXMLDocument*> get_list_of_open_documents () const ;

	bool view_exists (IView *a_view) const ;

	IView* get_cur_view () ;

	void set_cur_view (IView *a_view) ;

	//******************
	//signals
	//******************

	sigc::signal0<void>& signal_last_view_removed () ;

	sigc::signal1<void, IView*>& signal_first_view_added () ;

protected:

	enum MlViewStatus set_graphical_view_container (GVCIface *a_gvc) ;

	ViewDescriptor* select_view_to_open (void) ;

	void on_view_rebuilded (IView *a_old_view, IView *a_new_view) ;

	void on_views_swapped (IView* a_swapped_in, IView* a_swapped_out) ;

}
; //class ViewManager
}// namespace mlview

#endif //__MLVIEW_VIEW_MANAGER_H__
