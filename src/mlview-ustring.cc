/* -*- C++ -*-; indent-tabs-mode:true ; c-basic-offset: 4-*- */
/*
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute 
 *it and/or modify it under the terms of 
 *the GNU General Public License as published by the 
 *Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYING file for copyright information.
 */

#include "mlview-ustring.h"

/**
 * This class actually inherits Glib::ustring to
 * change its behaviour w.r.t assignation to a NULL 
 * char*. Assigning a NULL char* to this class,
 * equals assigning a "".
 *
 */
namespace mlview
{

UString::UString ()
{}

UString::UString (const char *a_cstr)
{
	if (!a_cstr)
		*this = "" ;
	else
		*this = a_cstr ;
}

UString::UString (UString const &an_other_string): Glib::ustring (an_other_string)
{
	this->assign (an_other_string) ;
}

UString::UString (const Glib::ustring &an_other_string)
{
	this->assign (an_other_string.c_str ()) ;
}

UString::~UString ()
{}

UString::UString&
UString::operator= (const char *a_cstr)
{
	if (!a_cstr)
		this->assign ("") ;
	else
		this->assign (a_cstr) ;

	return *this ;

}

UString&
UString::operator= (UString const &a_cstr)
{
	if (this == &a_cstr)
		return *this ;
	Glib::ustring::operator= (a_cstr) ;
	return *this ;
}

UString::operator const gchar* () const
{
	return c_str () ;
}

UString ::operator const guchar* () const
{
	return reinterpret_cast<const guchar*> (c_str ()) ;
}

}//namespace mlview
