/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 8-*- */

/*
 *This file is part of MlView
 *
 *MlView is free software; you can redistribute it and/or 
 *modify it under the terms of 
 *the GNU General Public License as published 
 *by the Free Software Foundation; either version 2
 *or (at your option) any later version.
 *
 *MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView.
 *see the file COPYING. 
 *If not, write to the 
 *Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */

#ifndef __MLVIEW_PING_DBO_H__
#define __MLVIEW_PING_DBO_H__

#include "mlview-utils.h"

G_BEGIN_DECLS

#ifdef MLVIEW_WITH_DBUS

#define INTERFACE_ORG_MLVIEW_PING_OBJECT_IFACE "org.mlview.PingObjectIface"
#define PATH_ORG_MLVIEW_PING_OBJECT "/org/mlview/PingObject"

#define MLVIEW_TYPE_PING_DBO (mlview_ping_dbo_get_type ())
#define MLVIEW_PING_DBO(widget) (G_TYPE_CHECK_INSTANCE_CAST ((widget), MLVIEW_TYPE_PING_DBO, MlViewPingDBO))
#define MLVIEW_PING_DBO_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), MLVIEW_TYPE_PING_DBO, MlViewPingDBOClass))
#define MLVIEW_IS_PING_DBO(widget) (G_TYPE_CHECK_INSTANCE_TYPE ((widget), MLVIEW_TYPE_PING_DBO))
#define MLVIEW_IS_PING_DBO_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), MLVIEW_TYPE_PING_DBO))

typedef struct _MlViewPingDBO MlViewPingDBO ;
typedef struct _MlViewPingDBOPriv MlViewPingDBOPriv ;
typedef struct _MlViewPingDBOClass MlViewPingDBOClass ;

struct _MlViewPingDBO
{
	GObject parent_object ;
	MlViewPingDBOPriv *priv ;
} ;

struct _MlViewPingDBOClass
{
	GObjectClass parent_class ;
} ;

GType mlview_ping_dbo_get_type (void)  ;

MlViewPingDBO * mlview_ping_dbo_new (MlViewAppContext *a_this) ;

#endif /*MLVIEW_WITH_DBUS*/

G_END_DECLS

#endif /*MLVIEW_PIN_DBO_H__*/
