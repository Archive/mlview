/* -*- Mode: C++; indent-tabs-mode: nil; c-basic-offset: 4-*- */

/*
 *This file is part of MlView
 *
 *MlView is free software; you can redistribute it and/or 
 *modify it under the terms of 
 *the GNU General Public License as published 
 *by the Free Software Foundation; either version 2
 *or (at your option) any later version.
 *
 *MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView.
 *see the file COPYING. 
 *If not, write to the 
 *Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */

#define ENABLE_NLS 1

#include <stdio.h>
#include <gnome.h>
#include "config.h"
#include "mlview-app.h"
#include "mlview-editor.h"
#include "mlview-view-factory.h"
#include "mlview-app-context.h"
#include "mlview-prefs-window.h"
#include "mlview-prefs.h"
#include "mlview-prefs-category-sizes.h"
#include "mlview-drop-manager.h"
#include "mlview-exec-command-dialog.h"
#include "recent-files/egg-recent.h"
#include "recent-files/egg-recent-view-uimanager.h"
#include "mlview-service.h"
#include "mlview-exception.h"
#include "mlview-safe-ptr-utils.h"
#include "mlview-plugin-manager.h"

#define ENABLE_NLS 1

/**
 *@file
 *the mlview application class definition.
 *It instanciates mlview's instance of  GnomeApp,
 *fills it with an instance of MlViewEditor, builds the application menus
 *and toolbars and creates an instance of AppContext.
 */
using namespace std ;
namespace mlview
{

struct App::WidgetsHandlePriv
{
	GtkWidget *app_win;/*main app window*/
	GtkWidget *menu_bar_container ;
	GtkWidget *toolbar_container ;
	/****MENU BAR STUFFS*****/
	GtkWidget *main_menu_bar ;
	/****Editor*widget*/
	Editor *editor ;

	//************************
	//methods
	//***********************
	WidgetsHandlePriv ():
			app_win (NULL),
			menu_bar_container (NULL),
			toolbar_container (NULL),
			main_menu_bar (NULL),
			editor (NULL)
	{}
}
;
struct AppPriv
{
	/*actions that require a document to be open*/
	static GtkActionEntry gv_doc_required_actions [] ;
	static GtkActionEntry gv_doc_not_required_actions[] ;

	App::WidgetsHandle *widgets ;
	GtkUIManager *ui_manager ;
	EggRecentViewUIManager *recent_view;
	EggRecentModel *recent_model;
	GtkActionGroup *doc_required_action_group ;
	GtkActionGroup *doc_not_required_action_group ;
	guint main_menubar_merge_id ;
	guint main_toolbar_merge_id ;
	SafePtr<AppContext, ObjectRef, ObjectUnref> context_ptr ;
	PluginManager plugin_manager ;

	AppPriv () :
			widgets (NULL),
			ui_manager (NULL),
			recent_view (NULL),
			recent_model (NULL),
			doc_required_action_group (NULL),
			doc_not_required_action_group (NULL),
			main_menubar_merge_id (0),
			main_toolbar_merge_id (0)
	{
		widgets = new App::WidgetsHandle () ;
	}

	//signal callbacks. This has to be ported to the slot system.
	//we don't have time for that yet though.
	static gboolean delete_event_cb (GtkWidget *a_widget,
	                                 GdkEvent *a_event,
	                                 App *a_app) ;

	static void new_menuitem_action_cb (GtkAction *a_action,
	                                    App *a_user_data) ;

	static void open_menuitem_action_cb (GtkAction *a_action,
	                                     App *a_user_data) ;

	static void openlocation_menuitem_action_cb (GtkAction *a_action, 
												 App *a_user_data) ;

	static void save_menuitem_action_cb (GtkAction *a_action,
	                                     App *a_user_data) ;

	static void save_as_menuitem_action_cb (GtkAction *a_action,
	                                        App *a_user_data) ;

	static void close_menuitem_action_cb (GtkAction *a_action,
	                                      App *a_user_data) ;

	static void quit_menuitem_action_cb (GtkAction *a_action,
	                                     App *a_user_data) ;

	static void undo_menuitem_action_cb (GtkAction *a_action,
	                                     App *a_user_data) ;

	static void redo_menuitem_action_cb (GtkAction *a_action,
	                                     App *a_user_data) ;

	static void preferences_menuitem_action_cb (GtkAction *a_action,
												App *a_user_data);

	static void associated_schemas_menuitem_action_cb (GtkAction *a_action,
													   App *a_app) ;

	static void new_view_on_doc_menuitem_action_cb (GtkAction *a_action,
													App *a_user_data) ;

	static void rename_view_menuitem_action_cb (GtkAction *a_action,
												App *a_user_data) ;

	static void validated_dtd_menuitem_action_cb (GtkAction *a_action,
												  App *a_user_data) ;

	static void apply_xslt_menuitem_action_cb (GtkAction *a_action,
                                                   App *a_user_data) ;

    static void execute_command_menuitem_action_cb (GtkAction *a_action,
                                                    App *a_user_data);

	static void about_menuitem_action_cb (GtkAction *a_action,
	                                      App *a_user_data) ;

	static void edit_menuitem_action_cb (GtkAction *a_action,
	                                     App *a_user_data) ;

	static void reload_document_menuitem_action_cb (GtkAction *a_action,
													App *a_app) ;

	static void open_recent_cb (GtkAction *a_action,
	                            App *a_user_data);

	static void application_initialized_cb (App *a_app) ;

	static char* tooltip_cb (EggRecentItem *item, gpointer user_data) ;
} ;


/*
 *The current instance of the mlview application context.
 *Must be set by mlview_app. For the moment,
 *there must be one application
 *context per process. That should be changed in the
 *future to enforce
 *reentrency.
 */

/*common function declarations*/

/*callbacks ...*/
static void
display_about_dialog (void);


/*actions that require a document to be open*/
GtkActionEntry AppPriv::gv_doc_required_actions [] = {

            /*File Menu*/
            {
                "SaveMenuitemAction", GTK_STOCK_SAVE, NULL,
                "<control>S", N_("Save the XML Document"),
                G_CALLBACK (save_menuitem_action_cb)
            },
            {
                "SaveAsMenuitemAction", GTK_STOCK_SAVE_AS, NULL,
                "<shift><control>S", N_("Save the XML Document in an other file"),
                G_CALLBACK (save_as_menuitem_action_cb)
            },

            {
                "ReloadDocumentMenuitemAction", GTK_STOCK_REVERT_TO_SAVED, NULL,
                "<control>R", N_("Reload the XML Document saved on disk"),
                G_CALLBACK (reload_document_menuitem_action_cb)
            },
            {
                "CloseMenuitemAction", GTK_STOCK_CLOSE, NULL,
                "<control>W", N_("Close the XML Document"),
                G_CALLBACK (close_menuitem_action_cb)
            },

            {
                "UndoMenuitemAction", GTK_STOCK_UNDO, NULL,
                "<control>Z", N_("Undo the last action"),
                G_CALLBACK (undo_menuitem_action_cb)},

            {
                "RedoMenuitemAction", GTK_STOCK_REDO, NULL,
                "<shift><control>Z", N_("Undo the last action"),
                G_CALLBACK (redo_menuitem_action_cb)
            },

            /*Tools menu*/
            {"ToolsMenuAction", NULL, N_("_Tools"), NULL, NULL, NULL},

            {"NewViewOnDocMenuitemAction", NULL, N_("New _view"),
             NULL, N_("Creates a new view on the current document"),
             NULL, /*G_CALLBACK (new_view_on_doc_menuitem_action_cb)*/
            },

            {"RenameViewMenuitemAction", NULL, N_("Re_name view"),
             NULL, N_("Rename the view"),
             G_CALLBACK (rename_view_menuitem_action_cb)
            },

            {"AssociatedSchemasMenuitemAction", GTK_STOCK_DND, N_("Schemas"),
             NULL, N_("Manage the schemas/DTDs associated to the document"),
             G_CALLBACK (associated_schemas_menuitem_action_cb)
            },

            {"ValidateDocMenuitemAction", GTK_STOCK_APPLY, N_("Va_lidate document"),
             NULL, N_("Validate the document against the associated DTD"),
             G_CALLBACK (validated_dtd_menuitem_action_cb)
            },

            {"ApplyXSLTMenuitemAction", GTK_STOCK_CONVERT, N_("Apply an _XSL transform"),
             NULL, N_("Apply an XSL transform to the current document"),
             G_CALLBACK (apply_xslt_menuitem_action_cb)
            },

            {"ExecuteCommandMenuitemAction", GTK_STOCK_EXECUTE, N_("_Execute command on document"),
             NULL, N_("Execute a command on the current document"),
             G_CALLBACK (execute_command_menuitem_action_cb)
            },
        } ;


GtkActionEntry AppPriv::gv_doc_not_required_actions[] = {

            /*File Menu*/
            {"FileMenuAction", NULL, N_("_File"), NULL, NULL, NULL},

			{
			 "NewMenuitemAction", GTK_STOCK_NEW, NULL,
			 "<control>N", N_("Create a new XML Document"),
			 G_CALLBACK (new_menuitem_action_cb)
			},

			{
			 "OpenMenuitemAction", GTK_STOCK_OPEN, NULL,
			 "<control>O", N_("Open an XML Document"),
			 G_CALLBACK (open_menuitem_action_cb)
			},

        {
		 "OpenLocationMenuitemAction", GTK_STOCK_NETWORK, N_("Open _location"),
         "<control>L", N_("Open an XML Document from an URI"),
         G_CALLBACK (openlocation_menuitem_action_cb)
		},

        {
		 "QuitMenuitemAction", GTK_STOCK_QUIT, NULL,
         "<control>Q", N_("Quit the MlView application"),
         G_CALLBACK (quit_menuitem_action_cb)
		},

/* Edit Menu */
        {"EditMenuAction", NULL, N_("_Edit"), NULL, NULL, NULL
//G_CALLBACK (edit_menuitem_action_cb)
        },

        {
		"PrefsMenuItemAction", GTK_STOCK_PREFERENCES, NULL,
         "<control>P", N_("_Preferences"),
         G_CALLBACK (preferences_menuitem_action_cb)
		},

        /*Help Menu*/
        {
		"HelpMenuAction", NULL, N_("_Help"), NULL, NULL, NULL
		},

        {
		 "AboutMenuitemAction", GNOME_STOCK_ABOUT, NULL,
         NULL, N_("About MlView ..."),
         G_CALLBACK (about_menuitem_action_cb)
		}
} ;

/*
 *Displays the about dialog.
 */
static void
display_about_dialog ()
{
	static GtkWidget *about_dialog = NULL, **widget_ptr = NULL;
	gchar *path = NULL ;
	GdkPixbuf *pixbuf = NULL;

	const gchar *authors[] = {
	                             N_("Author and maintainer:"),
	                             "Dodji Seketeli <dodji@gnome.org>\n",
	                             N_("Substantial contributors:"),
	                             "Nicolas Centa <happypeng@free.fr>",
	                             "Philippe Mechai <pmechai@free.fr>",
	                             "Baptiste Mille-Mathias <bmm80@free.fr>\n",
	                             N_("Former contributors:"),
	                             "Gael Chamoulaud<strider@gnome.org>",
	                             "Stephane Bonhomme<s.bonhomme@wanadoo.fr>",
	                             NULL
	                         };

	const gchar *documenters[] = {"Dodji Seketeli<dodji@mlview.org>",
	                              NULL
	                             };

	const gchar *translator_credits = _("translator_credits");

	if (about_dialog) {
		gtk_window_present (GTK_WINDOW (about_dialog));
		return;
	}
	path = gnome_program_locate_file (NULL,
	                                  GNOME_FILE_DOMAIN_APP_DATADIR,
	                                  "mlview/mlview-app-icon.xpm",
	                                  TRUE, NULL) ;
	if (path) {
		pixbuf = gdk_pixbuf_new_from_file (path, NULL);
		g_free (path) ;
		path = NULL ;
	}
	about_dialog = gnome_about_new
	               (PACKAGE, VERSION,
	                "Copyright \xc2\xa9 2001-2004 Dodji Seketeli\n",
	                _("A simple xml editor for GNOME"),
	                (const char **) authors,
	                (const char **) documenters,
	                strcmp
	                (translator_credits,
	                 "translator_credits") !=
	                0 ? translator_credits :
	                "No translators, English by\n"
	                "Dodji Seketeli  <dodji@mlview.org>\n"
	                "Gael Chamoulaud <strider@mlview.org>",
	                pixbuf);

	if (pixbuf != NULL) {
		g_object_unref (pixbuf) ;
		pixbuf = NULL ;
	}

	widget_ptr = &about_dialog ;
	g_object_add_weak_pointer (G_OBJECT (about_dialog),
	                           (void **) widget_ptr);
	gtk_widget_show (about_dialog);
}

//*******************************
//class members (static methods)
//*******************************

App::WidgetsHandle::WidgetsHandle ()
{
	m_priv = new App::WidgetsHandlePriv () ;
}

App::WidgetsHandle::~WidgetsHandle ()
{
	if (m_priv) {
		delete m_priv ;
		m_priv = NULL ;
	}
}

GtkWidget*
App::WidgetsHandle::get_app_win ()
{
	return m_priv->app_win ;
}

void
App::WidgetsHandle::set_app_win (GtkWidget *a_widget)
{
	m_priv->app_win = a_widget ;
}

GtkWidget *
App::WidgetsHandle::get_menu_bar_container ()
{
	return m_priv->menu_bar_container ;
}

void
App::WidgetsHandle::set_menu_bar_container (GtkWidget *a_widget)
{
	m_priv->menu_bar_container = a_widget ;
}

GtkWidget*
App::WidgetsHandle::get_toolbar_container ()
{
	return m_priv->toolbar_container ;
}

void
App::WidgetsHandle::set_toolbar_container (GtkWidget *a_widget)
{
	m_priv->toolbar_container = a_widget ;
}

GtkWidget *
App::WidgetsHandle::get_main_menu_bar ()
{
	return m_priv->main_menu_bar ;
}

void
App::WidgetsHandle::set_main_menu_bar (GtkWidget *a_widget)
{
	m_priv->main_menu_bar = a_widget ;
}

Editor*
App::WidgetsHandle::get_editor ()
{
	return m_priv->editor ;
}

void
App::WidgetsHandle::set_editor (Editor *a_widget)
{
	m_priv->editor = a_widget ;
}

gboolean
AppPriv::delete_event_cb (GtkWidget *a_widget,
                          GdkEvent *a_event,
                          App *a_app)
{
	THROW_IF_FAIL (GTK_IS_WIDGET (a_widget) && a_app) ;

	a_app->close_application (TRUE) ;
	return TRUE ;
}


void
AppPriv::new_menuitem_action_cb (GtkAction *a_action, App *a_app)
{
	Editor *editor = NULL ;
	THROW_IF_FAIL (a_action && a_app) ;

	editor = a_app->get_editor () ;
	THROW_IF_FAIL (editor) ;

	editor->create_new_xml_document ();
}

void
AppPriv::open_menuitem_action_cb (GtkAction *a_action, App *a_app)
{
	Editor *editor = NULL ;
	THROW_IF_FAIL (a_action && a_app) ;

	editor = a_app->get_editor () ;

	THROW_IF_FAIL (editor) ;

	editor->open_local_xml_document_interactive () ;
}

void
AppPriv::openlocation_menuitem_action_cb (GtkAction *a_action, App *a_app)
{
	Editor *editor = NULL ;
	THROW_IF_FAIL (a_action && a_app) ;

	editor = a_app->get_editor () ;
	THROW_IF_FAIL (editor) ;

	editor->open_xml_document_interactive () ;
}

void
AppPriv::open_recent_cb (GtkAction *a_action, App *a_app)
{
	Editor *editor = NULL;
	const EggRecentItem *item;
	gchar *uri;

	THROW_IF_FAIL (a_action && a_app);

	item = egg_recent_view_uimanager_get_item
	       (a_app->m_priv->recent_view, a_action);
	uri = (gchar*)egg_recent_item_peek_uri (item);

	editor = a_app->get_editor ();
	THROW_IF_FAIL (editor) ;

        editor->load_xml_file (uri, TRUE);
}

char*
AppPriv::tooltip_cb (EggRecentItem *item, gpointer user_data)
{
	char *uri = NULL;

	uri = egg_recent_item_get_uri_for_display (item);

	return uri;
}


void
AppPriv::save_menuitem_action_cb (GtkAction *a_action,
                                  App *a_app)
{
	Editor *editor = NULL ;
	THROW_IF_FAIL (a_action && a_app) ;

	editor = a_app->get_editor () ;
	THROW_IF_FAIL (editor) ;

	editor->save_xml_document () ;
}

void
AppPriv::save_as_menuitem_action_cb (GtkAction *a_action,
                                     App *a_app)
{
	Editor *editor = NULL ;
	THROW_IF_FAIL (a_action && a_app) ;

	editor = a_app->get_editor () ;
	THROW_IF_FAIL (editor) ;

	editor->save_xml_document_as_interactive () ;
}

void
AppPriv::close_menuitem_action_cb (GtkAction *a_action,
                                   App *a_app)
{
	Editor *editor = NULL ;
	THROW_IF_FAIL (a_action && a_app);

	editor = a_app->get_editor () ;
	THROW_IF_FAIL (editor) ;

	editor->close_xml_document (TRUE) ;
}

void
AppPriv::quit_menuitem_action_cb (GtkAction *a_action, App *a_app)
{
	Editor *editor = NULL ;
	mlview::App::WidgetsHandle * handle = NULL ;

	THROW_IF_FAIL (a_action && a_app) ;

	handle = a_app->get_widgets_handle () ;

	THROW_IF_FAIL (handle && handle->get_app_win ()) ;
	editor = a_app->get_editor () ;
	THROW_IF_FAIL (editor) ;

	a_app->close_application (TRUE) ;
}

void
AppPriv::undo_menuitem_action_cb (GtkAction *a_action, App *a_app)
{
	Editor *editor = NULL ;
	THROW_IF_FAIL (a_action && a_app) ;

	editor = a_app->get_editor () ;
	THROW_IF_FAIL (editor) ;

	editor->undo () ;
}

void
AppPriv::redo_menuitem_action_cb (GtkAction *a_action, App *a_app)
{
	Editor *editor = NULL ;
	THROW_IF_FAIL (a_action && a_app) ;

	editor = a_app->get_editor () ;
	THROW_IF_FAIL (editor) ;

	editor->redo () ;
}

void
AppPriv::preferences_menuitem_action_cb (GtkAction *a_action,
										 App *a_app)
{
	AppContext *app_context = AppContext::get_instance () ;
	THROW_IF_FAIL (app_context) ;

	PrefsWindow *window =
	static_cast<PrefsWindow*> (app_context->get_element
								("PrefsWindow"));

	if (window == NULL) {
		window = new PrefsWindow ();
		app_context->set_element ("PrefsWindow", window);
	}

	window->show ();
}

void
AppPriv::new_view_on_doc_menuitem_action_cb (GtkAction *a_action, App *a_app)
{
	ViewDescriptor *view_desc = NULL ;
	Editor *editor = NULL ;
	IView *view = NULL;

	THROW_IF_FAIL (a_action && a_app) ;

	editor = a_app->get_editor () ;
	THROW_IF_FAIL (editor) ;
	view_desc = (ViewDescriptor*)g_object_get_data
	            (G_OBJECT (a_action), "view-desc") ;
	if (!view_desc) {
		mlview_utils_trace_debug ("Could not get the correct view desc") ;
		return ;
	}
	view = editor->create_new_view_on_current_document (view_desc) ;
	if (!view) {
		mlview_utils_trace_debug ("Could not create the view requested") ;
		return ;
	}
	editor->get_view_manager ()->insert_view (view) ;
}


void
AppPriv::rename_view_menuitem_action_cb (GtkAction *a_action, App *a_app)
{
	Editor *editor = NULL ;
	THROW_IF_FAIL (a_action && a_app) ;

	editor = a_app->get_editor () ;
	THROW_IF_FAIL (editor) ;

	editor->set_current_view_name_interactive () ;
}

void
AppPriv::associated_schemas_menuitem_action_cb (GtkAction *a_action, App *a_app)
{
	Editor *editor = NULL ;
	THROW_IF_FAIL (a_action && a_app) ;

	editor = a_app->get_editor () ;
	THROW_IF_FAIL (editor) ;

	editor->manage_associated_schemas () ;
}

void
AppPriv::validated_dtd_menuitem_action_cb (GtkAction *a_action, App *a_app)
{
	Editor *editor = NULL ;
	THROW_IF_FAIL (a_action && a_app) ;

	editor = a_app->get_editor () ;
	THROW_IF_FAIL (editor) ;
	editor->validate () ;
}

void
AppPriv::apply_xslt_menuitem_action_cb (GtkAction *a_action, App *a_app)
{
	Editor *editor = NULL ;
	THROW_IF_FAIL (a_action && a_app) ;

	editor = a_app->get_editor () ;
	THROW_IF_FAIL (editor) ;

	editor->xslt_transform_document_interactive () ;
}

void
AppPriv::execute_command_menuitem_action_cb (GtkAction *a_action, App *a_app)
{
    THROW_IF_FAIL (a_action && a_app);

    Editor *editor = a_app->get_editor ();
    THROW_IF_FAIL (editor);

    MlViewXMLDocument *doc = editor->get_current_document ();
    THROW_IF_FAIL (doc);

    MlViewFileDescriptor *fd = mlview_xml_document_get_file_descriptor (doc);
	if (!fd) {
		AppContext *ctxt = AppContext::get_instance () ;
		THROW_IF_FAIL (ctxt) ;
		ctxt->warning (_("You need to save the current document before"
					   " you can run a command on it")) ;
		return ;
	}
    gchar *document_filename = mlview_file_descriptor_get_file_path (fd);
    mlview::ExecCommandDialog *m_dialog =
        new mlview::ExecCommandDialog (document_filename);

    if (m_dialog != NULL) {
        delete m_dialog;
		m_dialog = NULL ;
	}
}

void
AppPriv::about_menuitem_action_cb (GtkAction *a_action, App *a_app)
{
	Editor *editor = NULL ;

	THROW_IF_FAIL (a_action && a_app) ;

	editor = a_app->get_editor () ;
	THROW_IF_FAIL (editor) ;

	display_about_dialog ();
}

void
AppPriv::edit_menuitem_action_cb (GtkAction *a_action, App *a_app)
{
	Editor *editor = NULL ;

	THROW_IF_FAIL (a_action && GTK_IS_ACTION (a_action)
	                  && a_app) ;

	editor = a_app->get_editor () ;
	THROW_IF_FAIL (editor) ;

	editor->make_current_view_populate_application_edit_menu () ;
}

void
AppPriv::reload_document_menuitem_action_cb (GtkAction *a_action, App *a_app)
{
	Editor *editor = NULL ;

	THROW_IF_FAIL (a_action && GTK_IS_ACTION (a_action)
	                  && a_app) ;

	editor = a_app->get_editor () ;
	THROW_IF_FAIL (editor) ;

	editor->reload_document (TRUE) ;
}

//***********************
//private member methods
// *********************

/**
 *Make sure all the signals of the menu items
 *the right function callbacks are correctly
 *connected to the menu items.
 *@param a_menu_bar
 */
enum MlViewStatus
App::init_menu_and_toolbar (GladeXML *a_glade_xml)
{
	enum MlViewStatus status = MLVIEW_OK ;

	THROW_IF_FAIL (a_glade_xml) ;
	return status ;
}

enum MlViewStatus
App::build_and_init_menus (GladeXML *a_glade_xml)
{
	THROW_IF_FAIL (m_priv->context_ptr) ;

	gchar *file_path = NULL ;
	WidgetsHandle *widgets_handle= NULL ;
	GtkWidget *menubar = NULL, *toolbar = NULL ;

	GtkUIManager *ui_manager = NULL ;
	GtkActionGroup *action_group = NULL ;


	widgets_handle = get_widgets_handle () ;
	THROW_IF_FAIL (widgets_handle) ;
	THROW_IF_FAIL (widgets_handle->get_menu_bar_container ()) ;
	THROW_IF_FAIL (widgets_handle->get_app_win ()) ;

	action_group = gtk_action_group_new ("DocRequiredActions") ;
	gtk_action_group_set_translation_domain (action_group, GETTEXT_PACKAGE) ;

	gtk_action_group_add_actions
	(action_group,
	 AppPriv::gv_doc_required_actions,
	 sizeof (AppPriv::gv_doc_required_actions)
	 /
	 sizeof (GtkActionEntry),
	 this) ;
	m_priv->doc_required_action_group = action_group ;

	ui_manager = gtk_ui_manager_new () ;
	m_priv->ui_manager = ui_manager ;
	gtk_ui_manager_insert_action_group (ui_manager, action_group, 0) ;

	action_group = gtk_action_group_new ("DocNotRequiredActions") ;
	gtk_action_group_set_translation_domain (action_group, GETTEXT_PACKAGE) ;

	gtk_action_group_add_actions
	(action_group,
	 AppPriv::gv_doc_not_required_actions,
	 sizeof (AppPriv::gv_doc_not_required_actions)
	 /
	 sizeof (GtkActionEntry),
	 this) ;
	gtk_ui_manager_insert_action_group (ui_manager, action_group, 1) ;
	m_priv->doc_not_required_action_group = action_group ;

	gtk_window_add_accel_group (GTK_WINDOW (widgets_handle->get_app_win ()),
	                            gtk_ui_manager_get_accel_group (ui_manager)) ;

	file_path = mlview_utils_locate_file ("main-menu-bar.xml") ;
	THROW_IF_FAIL (file_path) ;

	m_priv->main_menubar_merge_id =
	    gtk_ui_manager_add_ui_from_file (ui_manager,
	                                     file_path,
	                                     NULL) ;
	THROW_IF_FAIL (m_priv->main_menubar_merge_id) ;
	if (file_path) {
		g_free (file_path) ;
		file_path = NULL ;
	}

	file_path = mlview_utils_locate_file ("main-toolbar.xml") ;
	THROW_IF_FAIL (file_path) ;
	m_priv->main_toolbar_merge_id =
	    gtk_ui_manager_add_ui_from_file (ui_manager,
	                                     file_path,
	                                     NULL) ;
	if (file_path) {
		g_free (file_path) ;
		file_path = NULL ;
	}

	/*
	 * call the dynamic view types choice submenu building code
	 */
	build_view_types_choice_submenu () ;

	menubar = gtk_ui_manager_get_widget (ui_manager, "/MainMenubar") ;
	THROW_IF_FAIL (menubar) ;
	gtk_widget_show_all (menubar) ;
	gtk_box_pack_end_defaults
	(GTK_BOX (widgets_handle->get_menu_bar_container ()),
	 menubar) ;
	toolbar = gtk_ui_manager_get_widget (ui_manager, "/MainToolbar") ;
	THROW_IF_FAIL (toolbar) ;
	gtk_widget_show_all (toolbar) ;
	gtk_box_pack_end_defaults
	(GTK_BOX (widgets_handle->get_toolbar_container ()),
	 toolbar) ;
	m_priv->context_ptr->set_element ("MlViewUIManager", ui_manager) ;
	return MLVIEW_OK ;
}

enum MlViewStatus
App::init_from_glade (GladeXML *a_glade_xml)
{
	GtkWidget *editor_container = NULL ;
	gchar *mlview_icon_filename = NULL ;
	EggRecentModel *model = NULL;
	EggRecentViewUIManager *view = NULL;
	enum MlViewStatus status = MLVIEW_ERROR ;

	THROW_IF_FAIL (a_glade_xml && m_priv) ;

	m_priv->context_ptr = AppContext::get_instance () ;
	THROW_IF_FAIL (m_priv->context_ptr) ;

	/*
	 *Build the widget handle datastructure
	 */
	build_widgets_handle (a_glade_xml) ;

	/*
	 *init the editor widget
	 */
	status = init_editor (a_glade_xml) ;

	/*
	 *init the menubar and toolbar
	 *The 2 following functions must be merged.
	 */
	status = init_menu_and_toolbar (a_glade_xml) ;
	build_and_init_menus (a_glade_xml) ;

	THROW_IF_FAIL (status == MLVIEW_OK) ;
	THROW_IF_FAIL (m_priv->widgets->get_editor ()) ;

	/*
	 * Add recent file entries
	 */
	model = egg_recent_model_new (EGG_RECENT_MODEL_SORT_MRU);
	egg_recent_model_set_filter_mime_types (model,
	                                        "text/xml",
	                                        "text/html",
	                                        NULL);

	view = egg_recent_view_uimanager_new (
	           m_priv->ui_manager,
	           "/MainMenubar/FileMenu/OpenRecent",
	           NULL,
	           NULL);
	egg_recent_view_uimanager_set_action_func (
	    view,
	    G_CALLBACK (AppPriv::open_recent_cb),
	    this);

	egg_recent_view_uimanager_set_tooltip_func
	(view, AppPriv::tooltip_cb, NULL);
	egg_recent_view_uimanager_show_icons (view, FALSE);
	egg_recent_view_set_model (EGG_RECENT_VIEW (view), model);

	m_priv->recent_model = model;
	m_priv->recent_view = view;

	Editor *editor = m_priv->widgets->get_editor () ;
	THROW_IF_FAIL (editor) ;

	/*
	 * Add Drop Support to window
	 */
	mlview_drop_manager_register_target 
	(GTK_WIDGET (m_priv->widgets->get_app_win ()));

	/*
	 *set the main window  and other mlview component 
	*to the context. That way, components willing
	*to access other components can get them from
	*the context by calling AppContext::get_element()
	 */
	m_priv->context_ptr->set_element ("MlViewMainWindow",
									  m_priv->widgets->get_app_win ()) ;
	m_priv->context_ptr->set_element ("MlViewEditor",
									  m_priv->widgets->get_editor ()) ;
	m_priv->context_ptr->set_element ("AppMainMenuBar",
									  m_priv->widgets->get_main_menu_bar ()) ;
	m_priv->context_ptr->set_element ("App", this) ;
	m_priv->context_ptr->set_element ("MlViewUIManager", m_priv->ui_manager) ;
	m_priv->context_ptr->set_element ("MlViewRecentModel", m_priv->recent_model);

	mlview_icon_filename = g_build_filename (DATADIR, "pixmaps",
										     "mlview-app-icon.png",
										     NULL) ;
	THROW_IF_FAIL (mlview_icon_filename) ;

	if (g_file_test (mlview_icon_filename, G_FILE_TEST_EXISTS) != TRUE) {
		g_warning ("could not find file %s\n",
		           mlview_icon_filename) ;
		return MLVIEW_ERROR ;
	}

	gtk_window_set_icon_from_file
	(GTK_WINDOW (m_priv->widgets->get_app_win ()),
	 mlview_icon_filename, NULL) ;
	gtk_window_set_default_icon_from_file
	(mlview_icon_filename, NULL) ;

        if (mlview_icon_filename) {
                g_free (mlview_icon_filename) ;
                mlview_icon_filename = NULL ;
        }

        /*
	 * Restore window size
	 */
        mlview::PrefsCategorySizes *prefs =
            dynamic_cast<mlview::PrefsCategorySizes*> (
                mlview::Preferences::get_instance ()
                ->get_category_by_id (mlview::PrefsCategorySizes::CATEGORY_ID));

	if (prefs->get_main_window_width () > 0
		&&
		prefs->get_main_window_height () > 0) {

		gtk_window_resize (GTK_WINDOW (m_priv->widgets->get_app_win ()),
						   prefs->get_main_window_width (),
						   prefs->get_main_window_height ());
	}

	/**
	 * Connect to usefull signal to give "life" to the
	 * application
	 */
	g_signal_connect (G_OBJECT (m_priv->widgets->get_app_win ()),
	                  "delete-event",
	                  G_CALLBACK (AppPriv::delete_event_cb),
	                  this) ;

	m_priv->context_ptr->signal_application_initialized ().connect
				(sigc::mem_fun (*this, &App::on_application_initialized)) ;
	m_priv->context_ptr->signal_document_name_changed ().connect
				(sigc::mem_fun (*this, &App::on_document_name_changed)) ;	
	m_priv->context_ptr->signal_view_swapped ().connect
				(sigc::mem_fun (*this, &App::on_view_swapped)) ;
	m_priv->context_ptr->signal_view_undo_state_changed ().connect
				(sigc::mem_fun (*this, &App::on_view_undo_state_changed)) ;

	editor->get_view_manager ()->signal_first_view_added ().connect
	(sigc::mem_fun (this, &App::on_signal_first_view_added)) ;

	editor->get_view_manager ()->signal_last_view_removed ().connect
	(sigc::mem_fun (this, &App::on_signal_last_view_removed)) ;

	editor_container = NULL ;
	status = MLVIEW_OK ;
	/*
	 *notify whomever listens to the event "the application has
	 *been initialized".
	 */
	m_priv->context_ptr->notify_application_initialized () ;
	return status ;
}

/**
 *populate the main container with the menu bar
 *a MlViewEditor widget.
 *@param a_glade_xml the glade xml object that contains
 *the mlview app graphical layout.
 */
enum MlViewStatus
App::init_editor (GladeXML *a_glade_xml)
{
	Editor *mlview_editor = NULL ;
	GtkWidget *editor_container = NULL ;

	THROW_IF_FAIL (a_glade_xml && m_priv) ;

	editor_container = glade_xml_get_widget (a_glade_xml,
	                   "EditorContainer") ;
	THROW_IF_FAIL (editor_container) ;

	/*create and pack the MlViewEditor widget*/
	mlview_editor = new Editor ("mlview:empty") ;
	THROW_IF_FAIL (mlview_editor) ;

	/*
	 *Pack the editor widget into the editor container
	 */
	gtk_box_pack_start_defaults (GTK_BOX (editor_container),
	                             GTK_WIDGET (mlview_editor->gobj ())) ;
	m_priv->widgets->set_editor (mlview_editor) ;

	return MLVIEW_OK ;
}

enum MlViewStatus
App::build_widgets_handle (GladeXML *a_glade_xml)
{
	THROW_IF_FAIL (a_glade_xml && m_priv && m_priv->widgets) ;

	m_priv->widgets->set_app_win
	(glade_xml_get_widget (a_glade_xml, "AppWin")) ;

	THROW_IF_FAIL (m_priv->widgets->get_app_win ()) ;

	m_priv->widgets->set_menu_bar_container
	(glade_xml_get_widget (a_glade_xml,
	                       "MenuBarContainer")) ;
	THROW_IF_FAIL (m_priv->widgets->get_menu_bar_container ()) ;

	m_priv->widgets->set_toolbar_container
	(glade_xml_get_widget (a_glade_xml, "ToolbarContainer")) ;
	THROW_IF_FAIL (m_priv->widgets->get_toolbar_container ()) ;

	return MLVIEW_OK ;
}

void
App::set_editing_enabled (bool a_enable)
{
	GtkUIManager *ui_manager = NULL ;
	Editor *editor = NULL ;

	THROW_IF_FAIL (m_priv) ;
	THROW_IF_FAIL (m_priv->doc_required_action_group
	                  && m_priv->doc_not_required_action_group) ;

	ui_manager = get_ui_manager () ;
	THROW_IF_FAIL (ui_manager) ;

	if (a_enable == TRUE) {
		gtk_action_group_set_sensitive (m_priv->doc_required_action_group,
		                                TRUE) ;

	} else {
		gtk_action_group_set_sensitive (m_priv->doc_required_action_group,
		                                FALSE) ;
	}

	/*now update the undo/redo action sensitivity*/
	editor = get_editor () ;
	if (!editor)
		return ;
	m_priv->context_ptr->notify_view_undo_state_changed () ;
}

/**
 * Build the submenu that lets the user choose the types of views
 * she wants to use.
 * @param a_this the current instance of #App
 */
enum MlViewStatus
App::build_view_types_choice_submenu ()
{
	gint number_of_views = 0, i=0, merge_id= 0 ;
	Editor *editor = NULL ;
	ViewDescriptor *view_desc = NULL ;
	GtkUIManager *ui_manager = NULL ;

	THROW_IF_FAIL (m_priv) ;
	THROW_IF_FAIL (m_priv->doc_required_action_group) ;

	editor = get_editor () ;
	THROW_IF_FAIL (editor) ;
	number_of_views = ViewFactory::get_number_of_view_desc () ;
	if (!number_of_views) {
		mlview_utils_trace_debug ("Got 0 views") ;
		return MLVIEW_ERROR ;
	}
	ui_manager = get_ui_manager () ;
	THROW_IF_FAIL (ui_manager) ;

	merge_id = gtk_ui_manager_new_merge_id (ui_manager) ;
	for (i=0 ; i < number_of_views ;i++) {
		GtkAction *action = NULL ;
		view_desc = ViewFactory::get_view_descriptor_at (i) ;
		if (!view_desc) {
			break ;
		}
		action = gtk_action_new (view_desc->view_type_name,
		                         view_desc->translated_view_name,
		                         view_desc->view_description,
		                         NULL) ;
		gtk_action_group_add_action
		(m_priv->doc_required_action_group,
		 action) ;
		g_object_set_data (G_OBJECT (action), "view-desc", view_desc) ;
		g_signal_connect
		(G_OBJECT (action),
		 "activate",
		 G_CALLBACK (AppPriv::new_view_on_doc_menuitem_action_cb),
		 this) ;
		gtk_ui_manager_add_ui
		(ui_manager, merge_id,
		 "/MainMenubar/ToolsMenu/NewViewOnDocMenuitem",
		 view_desc->view_type_name, view_desc->view_type_name,
		 GTK_UI_MANAGER_AUTO, FALSE) ;
		gtk_ui_manager_ensure_update (ui_manager) ;
	}
	return MLVIEW_OK ;
}

//****************
// signal handlers
//****************

void
App::on_signal_first_view_added (IView *a_view)
{
	set_editing_enabled (true) ;
}

void
App::on_signal_last_view_removed ()
{

	set_editing_enabled (false) ;
}

/****************
 *public methods
 ***************/

App::App (const Glib::ustring &a_appname)
{
	gchar *glade_file = NULL ;
	GladeXML *glade_xml = NULL ;

	m_priv = new AppPriv () ;

	glade_file = gnome_program_locate_file
	             (NULL, GNOME_FILE_DOMAIN_APP_DATADIR,
	              PACKAGE "/mlview-main-app-win2.glade", TRUE,
	              NULL) ;
	g_assert (glade_file) ;

	glade_xml = glade_xml_new (glade_file,
	                           "AppWin",
	                           NULL) ;
	g_assert (glade_xml) ;
	init_from_glade (glade_xml) ;

	try {
		m_priv->plugin_manager.load_all_plugins_from_default_plugins_dir () ;
	} catch (exception &a_exception) {
		TRACE_EXCEPTION (a_exception) ;
	} catch (...) {
		LOG_TO_ERROR_STREAM ("caught unknown exception") ;
	}

	if (glade_xml) {
		g_object_unref (G_OBJECT (glade_xml)) ;
		glade_xml = NULL ;
	}
	if (glade_file) {
		g_free (glade_file) ;
		glade_file = NULL ;
	}
}

/**
 * Make the main window visible
 *
 * @param a_app the current mlview application
 */
void
App::set_visible ()
{
	THROW_IF_FAIL (m_priv);

	gtk_widget_show_all (GTK_WIDGET (m_priv->widgets->get_app_win ()));
}

/**
 *Getter of the instance of MlViewEditor associated
 *to the current instance of GnomeApp.
 *
 *@param a_app the instance of GnomeApp to consider.
 *@return the instance of MlViewEditor associated to a_app or
 *NULL if nothing is associated.
 */
Editor *
App::get_editor ()
{
	Editor *result = NULL;

	THROW_IF_FAIL (m_priv && m_priv->widgets);


	result = m_priv->widgets->get_editor () ;
	return result;
}

/**
 *Returns the instance of #MlViewWidgetsHandle associated
 *to the current instance of #App.
 *@param a_this the instance of #App to consider.
 *@return a pointer to the widgets handle, NULL in case of
 *an error.
 */
App::WidgetsHandle *
App::get_widgets_handle ()
{
	THROW_IF_FAIL (m_priv) ;

	return m_priv->widgets ;
}

/**
 *Sets the title of the main application window.
 *The title is built from the name of document being edited.
 *That document name must be passed as a parameter of this function.
 *This is a requirement of the GNOME HIG.
 *@param a_this the current instance of #App.
 *@param a_document_name the name of the document being
 *@return MLVIEW_OK upon successful completion.
 */
enum MlViewStatus
App::set_main_window_title (const Glib::ustring &a_title)
{
	Glib::ustring title_string ;

	THROW_IF_FAIL (m_priv && m_priv->widgets && m_priv->widgets->get_app_win ()) ;

	title_string = a_title ;
	title_string +=  " - MlView" ;

	gtk_window_set_title (GTK_WINDOW (m_priv->widgets->get_app_win ()),
	                      title_string.c_str ()) ;
	return MLVIEW_OK ;
}

GtkUIManager *
App::get_ui_manager ()
{
	THROW_IF_FAIL (m_priv) ;

	return m_priv->ui_manager ;
}

void
App::close_application (bool a_interactive)
{
	Editor *editor = NULL;
	WidgetsHandle *handle = NULL ;
	bool is_ok = FALSE;
	gint w, h;

	THROW_IF_FAIL (m_priv);

	editor = get_editor ();
	THROW_IF_FAIL (editor) ;

	handle = get_widgets_handle () ;
	THROW_IF_FAIL (handle && handle->get_app_win ()) ;

	if (editor)
		is_ok = editor->close_all_xml_documents (a_interactive);

	if (is_ok == FALSE) {
		gtk_widget_show (handle->get_app_win ()) ;
		return;
	}

	/*
	 * Save window size
	 */
	gtk_window_get_size (GTK_WINDOW (handle->get_app_win ()), &w, &h);
	m_priv->context_ptr->save_window_state ( w, h);
	/*mlview_service_stop (a_this, NULL) ; does not work very well*/

	gtk_widget_destroy (handle->get_app_win ());
	handle->set_app_win (NULL) ;
	gtk_main_quit ();
}

void
App::close_all_docs (bool a_interactive)
{
	Editor *editor = NULL;
	WidgetsHandle *handle = NULL ;
	gboolean is_ok = FALSE;
	gint w, h;

	THROW_IF_FAIL (m_priv);

	editor = get_editor ();
	THROW_IF_FAIL (editor) ;

	handle = get_widgets_handle () ;
	THROW_IF_FAIL (handle && handle->get_app_win ()) ;

	if (editor)
		is_ok = editor->close_all_xml_documents (a_interactive);

	if (is_ok == FALSE) {
		gtk_widget_show (handle->get_app_win ()) ;
		return;
	}

	/*
	 * Save window size
	 */
	gtk_window_get_size (GTK_WINDOW (handle->get_app_win ()), &w, &h) ;
	m_priv->context_ptr->save_window_state (w, h) ;
}

App::~App ()
{
	if (m_priv) {

		delete m_priv ;
		m_priv = NULL ;
	}
}

void
App::on_application_initialized ()
{
	THROW_IF_FAIL (m_priv) ;

	set_editing_enabled (false) ;
}

void
App::on_document_name_changed (gpointer a_doc)
{
	THROW_IF_FAIL (a_doc && MLVIEW_IS_XML_DOCUMENT (a_doc)) ;

	xmlDoc * doc = mlview_xml_document_get_native_document
									(MLVIEW_XML_DOCUMENT (a_doc)) ;
	if (!doc) {
		LOG_TO_ERROR_STREAM ("mlview_xml_document_get_xml_document() failed\n") ;
		return ;
	}
	gchar *doc_name = NULL ;
	if (doc->name) {
		doc_name = doc->name ;
	} else {
		doc_name = (gchar*)"untitled" ;
	}
	set_main_window_title (doc_name) ;
}

void
App::on_view_swapped (Object *a_old_view, Object *a_new_view)
{
	MlViewXMLDocument *mlview_doc = NULL ;
	xmlDoc *doc = NULL ;
	gchar *doc_name = NULL ;

	if (!a_new_view) ;
		return ;
	IView* new_view = dynamic_cast<IView*> (a_new_view) ;
	THROW_IF_FAIL (new_view) ;

	mlview_doc = new_view->get_document () ;
	if (mlview_doc)
		doc = mlview_xml_document_get_native_document (mlview_doc) ;
	if (!doc) {
		LOG_TO_ERROR_STREAM ("mlview_xml_document_get_xml_document() failed\n") ;
		return ;
	}
	if (doc->name) {
		doc_name = doc->name ;
	} else {
		doc_name = (gchar*)"untitled" ;
	}
	set_main_window_title (doc_name) ;
}

void
App::on_view_undo_state_changed ()
{
	Editor *editor = NULL ;
	GtkUIManager *ui_manager = NULL ;
	GtkAction *undo_action = NULL,
	                         *redo_action = NULL ;

	THROW_IF_FAIL (m_priv) ;
	THROW_IF_FAIL (m_priv->context_ptr) ;

	editor = (Editor*)m_priv->context_ptr->get_element ("MlViewEditor") ;
	if (!editor) {
		LOG_TO_ERROR_STREAM ("Could not find the editor") ;
		return ;
	}

	ui_manager = get_ui_manager ();
	THROW_IF_FAIL (ui_manager) ;

	undo_action = gtk_ui_manager_get_action (ui_manager,
	              "/MainToolbar/UndoToolitem") ;
	THROW_IF_FAIL (undo_action) ;
	redo_action = gtk_ui_manager_get_action (ui_manager,
	              "/MainToolbar/RedoToolitem") ;

	if (editor->can_undo () == TRUE) {
		g_object_set (G_OBJECT (undo_action),
		              "sensitive", TRUE, NULL) ;
	} else {
		g_object_set (G_OBJECT (undo_action),
		              "sensitive", FALSE, NULL) ;
	}
	if (editor->can_redo () == TRUE) {
		g_object_set (G_OBJECT (redo_action), "sensitive", TRUE, NULL) ;

	} else {
		g_object_set (G_OBJECT (redo_action), "sensitive", FALSE, NULL) ;
	}
}

}// namespace mlview
