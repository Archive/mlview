/*
  Various UI-oriented utility functions

  This file is part of MlView.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#ifndef __MLVIEW_UI_UTILS_H__
#define __MLVIEW_UI_UTILS_H__

#include <gtkmm.h>

namespace mlview
{

///
/// Return an html string representation of a Gdk::Color object\n
/// For instance blue would give #0000FF
///
/// \param color the Gdk::color to convert to a string
///
Glib::ustring
gdk_color_to_html_string (const Gdk::Color color);

///
/// Compatibility version of
/// mlview::gdk_color_to_html_string (const Gdk::Color color)
///
/// \see mlview::gdk_color_to_html_string ()
///
Glib::ustring
gdk_color_to_html_string (const GdkColor color);


}


#endif // __MLVIEW_UI_UTILS_H__


/* -*- Mode: C++; indent-tabs-mode: t; c-basic-offset: 4; -*- */
