/* -*- Mode: C++; indent-tabs-mode:true ; c-basic-offset: 4-*- */

/*
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute 
 *it and/or modify it under the terms of 
 *the GNU General Public License as published by the 
 *Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */
#ifndef __MLVIEW_UTILS_H__
#define __MLVIEW_UTILS_H__

#define ENABLE_NLS 1
#define DBUS_API_SUBJECT_TO_CHANGE

#include "assert.h"
#include <iostream>
#include "config.h"
#include <libgnome/libgnome.h>
#include <libgnomevfs/gnome-vfs.h>
#include <libxml/parser.h>
#include <gtk/gtk.h>


G_BEGIN_DECLS

#ifndef BEGIN_NAMESPACE_MLVIEW
#define BEGIN_NAMESPACE_MLVIEW namespace mlview {
#endif //BEGIN_NAMESPACE_MLVIEW

#ifndef END_NAMESPACE_MLVIEW
#define END_NAMESPACE_MLVIEW } //namespace mlview
#endif //END_NAMESPACE_MLVIEW

#define HERE "in " << __FILE__ << ":" << __LINE__ << " "
/**
 *@file
 *Misc usefull functions.
 *There are also some functions that could be small patches
 *gtk and libxml2.
 */
enum MlViewStatus
{
    MLVIEW_OK = 0,
    MLVIEW_BAD_PARAM_ERROR,
    MLVIEW_NULL_FILE_NAME_ERROR,
    MLVIEW_BAD_FILE_ERROR,
    MLVIEW_EMPTY_FILE_ERROR,
    MLVIEW_NO_SPACE_LEFT_ERROR,
    MLVIEW_UNKNOWN_ENCODING_ERROR,
    MLVIEW_ENCODING_ERROR,
    MLVIEW_NO_ENCODINGS_ERROR,
    MLVIEW_ENCODING_NOT_SUPPORTED_ERROR,
    MLVIEW_CHAR_TOO_LONG_ERROR,
    MLVIEW_OUT_OF_MEMORY_ERROR,
    MLVIEW_NODE_NOT_FOUND_ERROR,
    MLVIEW_OUT_OF_BOUNDS_ERROR,
    MLVIEW_NO_LEFT_ANGLE_BRACKET_ERROR,
    MLVIEW_NO_RIGHT_ANGLE_BRACKET_ERROR,
    MLVIEW_NO_ROW_SELECTED_ERROR,
    MLVIEW_PARSING_ERROR,
    MLVIEW_UNBALANCED_TAF_ERROR,
    MLVIEW_NO_DANGLING_OPEN_TAG_FOUND_ERROR ,
    MLVIEW_BAD_URI_ERROR,
    MLVIEW_INTERNAL_SUBSET_ALREADY_EXISTS_ERROR,
    MLVIEW_ENTITY_NAME_EXISTS_ERROR,
    MLVIEW_BAD_NODE_PARENT_ERROR,
    MLVIEW_DOC_NOT_VALID_ERROR,
    MLVIEW_BAD_CONTENT_MODEL_ERROR,
    MLVIEW_REACHED_NON_FINAL_STATE_ERROR,
    MLVIEW_SEQUENCE_NON_VALID_ERROR,
    MLVIEW_EOF_ERROR,
    MLVIEW_IFACE_NOT_DEFINED_ERROR,
    MLVIEW_OBJECT_NOT_FOUND_ERROR,
    MLVIEW_OBJECT_CONTAINER_NOT_EMPTY_ERROR,
    MLVIEW_PATH_TOO_DEEP_ERRROR,
    MLVIEW_NO_DOC_IN_VIEW_ERROR,
    MLVIEW_INT_SUBSET_ALREADY_PRESENT_ERROR,
    MLVIEW_KEY_INPUTS_FULL_ERROR,
    MLVIEW_KEY_BINDING_NOT_FOUND_ERROR,
    MLVIEW_NOT_A_KEY_BINDING_ERROR,
    MLVIEW_KEY_SEQUENCE_TOO_SHORT_ERROR,
    MLVIEW_STACK_TOO_SHORT_ERROR,
    MLVIEW_CLIPBOAD_EMPTY_ERROR,
    MLVIEW_ENTRY_EMPTY_ERROR,
    MLVIEW_STACK_EMPTY_ERROR,
    MLVIEW_CANT_UNDO_ERROR,
    MLVIEW_CANT_RELOAD_ERROR,
    MLVIEW_NULL_POINTER_ERROR,
    MLVIEW_NO_STAG_ERROR,
    MLVIEW_SERVICE_START_ERROR,
    MLVIEW_BUS_ERROR,
    MLVIEW_CANNOT_START_SERVICE_ERROR,
    MLVIEW_SERVICE_ALREADY_EXISTS_ERROR,
    MLVIEW_PING_FAILED_ERROR,
    MLVIEW_RESERVED_FOR_FUTURE_USE1_ERROR,
    MLVIEW_RESERVED_FOR_FUTURE_USE2_ERROR,
    MLVIEW_RESERVED_FOR_FUTURE_USE3_ERROR,
    MLVIEW_RESERVED_FOR_FUTURE_USE4_ERROR,
    MLVIEW_RESERVED_FOR_FUTURE_USE5_ERROR,
    MLVIEW_RESERVED_FOR_FUTURE_USE6_ERROR,
    MLVIEW_RESERVED_FOR_FUTURE_USE7_ERROR,
    MLVIEW_RESERVED_FOR_FUTURE_USE8_ERROR,
    MLVIEW_RESERVED_FOR_FUTURE_USE9_ERROR,
    MLVIEW_RESERVED_FOR_FUTURE_USE10_ERROR,
    MLVIEW_RESERVED_FOR_FUTURE_USE11_ERROR,
    /*must be the last one */
    MLVIEW_ERROR
};

enum MlViewEncoding
{
    UTF8,
    ISO8859_1,
    ASCII
};

struct NameValuePair
{
	GString *name ;
	GString *value ;
} ;

/*
 *Some miscellaneous macros definitions.
 */

#define MLVIEW_LOG_DOMAIN "MLVIEW"


#define mlview_utils_trace(message, ...) \
fprintf (stderr, "mlview-debug: %s: in file %s: line %d: (%s)\n",\
         message, __FILE__,__LINE__,__ASSERT_FUNCTION) ;

/**
 *Trace a debug message.
 * The file, line and enclosing function
 *of the message will be automatically
 *added to the message.
 *@param a_msg the msg to trace.
 */
#ifdef MLVIEW_DEBUG
#   define mlview_utils_trace_debug(message, ...) \
	mlview_utils_trace (message, __VA_ARGS__)
#else
#   define mlview_utils_trace_debug(message, ...)
#endif

#define ERROR_STREAM \
cerr << "mlview-debug: in " << __ASSERT_FUNCTION \
<< " : in file " << __FILE__ << " : " \
<< " line " << __LINE__ << " : "

#ifdef MLVIEW_DEBUG
#define LOG_TO_ERROR_STREAM(expr) \
ERROR_STREAM << expr << endl
#else //ifdef MLVIEW_DEBUG
#define LOG_TO_ERROR_STREAM(expr)
#endif

int mlview_utils_load_xml_document (gchar * a_xmlfile,
                                    xmlParserCtxtPtr
                                    a_parser_context);


void mlview_utils_display_message_dialog (const gchar * a_msg_format, ...) ;

void mlview_utils_display_warning_dialog (const gchar * a_msg_format, ...) ;

void mlview_utils_display_error_dialog (const gchar * a_msg_format, ...);

gboolean mlview_utils_is_base_char (gint a_c)  ;

gboolean mlview_utils_is_combining (gint a_c) ;

gboolean mlview_utils_is_extender (gint a_c) ;

gboolean mlview_utils_is_ideographic (gint a_c) ;

gboolean mlview_utils_is_letter (gint a_c) ;

gboolean mlview_utils_is_name_char (gint a_c) ;

gboolean mlview_utils_is_digit (gint a_c) ;

gboolean mlview_utils_is_space (gint a_c) ;

gboolean mlview_utils_is_pubidchar (gint a_c) ;

enum MlViewStatus mlview_utils_skip_spaces (gchar *a_raw_str,
        gchar **a_skiped_to) ;

enum MlViewStatus mlview_utils_skip_spaces2 (GtkTextIter *a_raw_str,
        GtkTextIter **a_skiped_to) ;

enum MlViewStatus mlview_utils_parse_element_name (gchar *row_str,
        gchar **a_name_end) ;

enum MlViewStatus mlview_utils_parse_element_name2 (GtkTextIter *a_from,
        GtkTextIter **a_name_start,
        GtkTextIter **a_name_end) ;

enum MlViewStatus mlview_utils_parse_reference (gchar *a_raw_str,
        gchar **a_ref_end) ;

enum MlViewStatus mlview_utils_parse_reference2 (GtkTextIter *a_from,
        GtkTextIter **a_ref_end) ;

enum MlViewStatus mlview_utils_parse_attribute (gchar *a_raw_str,
        gchar **a_name_end,
        gchar **a_val_start,
        gchar **a_val_end)  ;

enum MlViewStatus mlview_utils_parse_attribute2 (GtkTextIter *a_from,
        GtkTextIter **a_name_end,
        GtkTextIter **a_val_start,
        GtkTextIter **a_val_end) ;

enum MlViewStatus mlview_utils_parse_comment (gchar *a_raw_str,
        GString **a_comment) ;

enum MlViewStatus mlview_utils_parse_pi (gchar *a_raw_pi,
        GString **a_pi_target,
        GString **a_pi_param) ;

enum MlViewStatus mlview_utils_parse_start_tag (gchar *a_raw_str,
        GString **a_name,
        GList **a_name_value_pair_list) ;

enum MlViewStatus mlview_utils_parse_start_tag2 (GtkTextIter *a_from,
        GString **a_name,
        GList **a_name_value_pair_list,
        GtkTextIter **a_to,
        gboolean *a_is_empty_tag) ;

enum MlViewStatus mlview_utils_parse_closing_tag2 (GtkTextIter *a_from,
        GString **a_name) ;

enum MlViewStatus mlview_utils_parse_cdata_section (const gchar *a_raw_str,
        gchar **a_out_start,
        gchar **a_out_end) ;

enum MlViewStatus mlview_utils_parse_doctype_decl (gchar *a_instr,
        gchar **a_name_start,
        gchar **a_name_end,
        gchar **a_public_id_start,
        gchar **a_public_id_end,
        gchar **a_external_id_start,
        gchar **a_external_id_end) ;

enum MlViewStatus
mlview_utils_parse_external_id (gchar *a_instr,
                                gchar **a_public_id_start,
                                gchar **a_public_id_end,
                                gchar **a_system_id_start,
                                gchar **a_system_id_end,
                                gchar **a_end_ptr) ;

enum MlViewStatus mlview_utils_parse_entity_ref (gchar *a_instr,
        gchar **a_name_start,
        gchar **a_name_end) ;

enum MlViewStatus mlview_utils_parse_char_ref (gchar *a_instr,
        gchar **a_char_code_start,
        gchar **a_char_code_end,
        gboolean *a_is_hexa) ;

enum MlViewStatus mlview_utils_parse_pe_ref (gchar *a_instr,
        gchar **a_name_start,
        gchar **a_name_end) ;

enum MlViewStatus mlview_utils_parse_entity_value (gchar *a_instr,
        gchar **a_value_start,
        gchar **a_value_end) ;

enum MlViewStatus mlview_utils_parse_internal_general_entity (gchar *a_instr,
        gchar **a_name_start,
        gchar **a_name_end,
        gchar **a_value_start,
        gchar **a_value_end) ;

enum MlViewStatus mlview_utils_parse_external_general_parsed_entity (gchar *a_instr,
        gchar **a_name_start,
        gchar **a_name_end,
        gchar **a_public_id_start,
        gchar **a_public_id_end,
        gchar **a_system_id_start,
        gchar **a_system_id_end) ;

enum MlViewStatus mlview_utils_parse_external_general_unparsed_entity (gchar *a_instr,
        gchar **a_name_start,
        gchar **a_name_end,
        gchar **a_public_id_start,
        gchar **a_public_id_end,
        gchar **a_system_id_start,
        gchar **a_system_id_end,
        gchar **a_ndata_start,
        gchar **a_ndata_end) ;

enum MlViewStatus mlview_utils_parse_internal_parameter_entity (gchar *a_instr,
        gchar **a_name_start,
        gchar **a_name_end,
        gchar **a_value_start,
        gchar **a_value_end) ;

enum MlViewStatus mlview_utils_parse_external_parameter_entity (gchar *iptr,
        gchar **a_name_start,
        gchar **a_name_end,
        gchar **a_public_id_start,
        gchar **a_public_id_end,
        gchar **a_system_id_start,
        gchar **a_system_id_end) ;

enum MlViewStatus mlview_utils_mark_menu_object (GtkWidget *a_menu_object,
        gchar *a_mark_string) ;


enum MlViewStatus mlview_utils_get_menu_object (GtkWidget *a_menu_root,
        gchar *a_path,
        GtkWidget **a_menu_object) ;

struct NameValuePair * mlview_utils_name_value_pair_new (GString *a_name,
			        GString *a_value) ;

void mlview_utils_name_value_pair_free (struct NameValuePair *a_this,
                                        gboolean a_free_strings) ;


enum MlViewStatus mlview_utils_get_current_word_bounds (gchar *a_phrase,
        gint a_phrase_len,
        gint a_cur_index,
        gchar **a_word_start,
        gchar **a_word_end) ;

void  mlview_utils_name_value_pair_list_free (GList *a_nv_pair_list,
        gboolean a_free_strings) ;

struct NameValuePair * mlview_utils_name_value_pair_list_lookup (GList *a_nv_pair_list ,
			        const gchar *a_name) ;

gboolean mlview_utils_is_white_string (const gchar * a_str);

enum MlViewStatus mlview_utils_escape_predef_entities_in_str (gchar *a_instr,
        gchar **a_outstr,
        guint *a_outstrlen) ;

enum MlViewStatus mlview_utils_isolat1_str_len_as_utf8 (const gchar * a_str,
        gint * a_result_len);

enum MlViewStatus mlview_utils_utf8_str_len_as_isolat1 (const gchar * a_utf8_str,
        gint * a_len);

enum MlViewStatus mlview_utils_isolat1_str_to_utf8 (const gchar * a_input_str,
        gchar ** a_output_str);

enum MlViewStatus mlview_utils_utf8_str_to_isolat1 (const gchar * a_in_str,
        gchar ** a_out_str);
void mlview_utils_parse_full_name (xmlNode * a_node,
                                   const gchar * a_full_name,
                                   xmlNs ** a_ns,
                                   gchar ** a_local_name);

void mlview_utils_init_available_encodings_list (void);

void mlview_utils_ref_available_encodings (void);

void mlview_utils_unref_available_encodings (void);

GList *mlview_utils_get_available_encodings (void);

enum MlViewStatus mlview_utils_add_supported_encoding (const gchar * a_name);

enum MlViewStatus mlview_utils_del_supported_encoding (const gchar * a_name);

gboolean mlview_utils_is_encoding_supported (const gchar * a_encoding);

gboolean mlview_utils_str_equals_ignore_case (const gchar *a_str1,
        const gchar *a_str2) ;
gboolean mlview_utils_str_equals (const gchar *a_str1,
                                  const gchar *a_str2,
                                  gboolean a_ignore_case) ;
gboolean mlview_utils_strstr_ignore_case (const gchar *a_haystack,
        const gchar *a_needle) ;
gboolean mlview_utils_strstr (const gchar *a_haystack,
                              const gchar *a_needle,
                              gboolean a_ignore_case) ;

enum MlViewStatus mlview_utils_uri_is_relative (const gchar *a_uri,
        gboolean *a_is_relative) ;

gchar * mlview_utils_get_dir_name_from_uri (const gchar *a_uri) ;

enum MlViewStatus mlview_utils_relative_uri_to_absolute_uri (const gchar *a_relative_uri,
        const gchar* a_base_uri,
        gchar **a_absolute_uri) ;

void
xmlDictFreeMem (xmlDict *a_dict,
                xmlChar* a_mem) ;

xmlNs *xmlUnlinkNs (xmlNode * node, xmlNs * ns);

xmlNs *xmlUnlinkNsDef (xmlNode * node, xmlNs * ns);

int xmlValidGetValidElementsChildren (xmlNode * a_node,
                                      const xmlChar ** a_list,
                                      int a_max);

int xmlSetEntityNodeName (xmlDtd *aDtd, xmlEntity *anEntityNode,
                          xmlChar *aName) ;

int xmlNodeIsChildOf (xmlNodePtr aChild,
                      xmlNodePtr aParent);

int xmlGetValidElementsChildren2 (xmlNode *aNode,
                                  const xmlChar **aList,
                                  unsigned int aMax) ;

xmlNode * mlview_utils_get_last_child_element_node (xmlNode *a_ref_node) ;

gint gtk_clist_row_absolute_top_ypixel (GtkCList * a_clist,
                                        gint a_row);

gint gtk_ctree_node_absolute_top_ypixel (GtkCTree * a_tree,
        GtkCTreeNode * a_node);

gboolean mlview_utils_gtk_row_ref_2_iter (GtkTreeModel * a_model,
        GtkTreeRowReference * a_ref,
        GtkTreeIter * a_iter) ;

enum MlViewStatus mlview_utils_tree_path_string_to_iter (GtkTreeModel *a_model,
        gchar *a_tree_path_str,
        GtkTreeIter *a_iter);

enum MlViewStatus mlview_utils_gtk_tree_view_expand_row_to_depth (GtkTreeView *a_view,
        GtkTreePath *a_path,
        gint a_depth) ;

enum MlViewStatus mlview_utils_gtk_tree_view_expand_row_to_depth2 (GtkTreeView *a_view,
        GtkTreeIter *a_iter,
        gint a_depth) ;

gint gtk_clist_absolute_row_top_ypixel (GtkCList * a_clist, gint a_row);


void mlview_utils_init (void);

void mlview_utils_cleanup (void);

gchar *mlview_utils_normalize_text (const gchar *a_original,
                                    const gchar *a_ex_endline,
                                    const gchar *a_endline,
                                    const gchar *a_space,
                                    const guint a_line_length) ;

gchar * mlview_utils_locate_file (const gchar *a_file_name) ;

gchar *mlview_utils_get_unique_string (const gchar *a_prefix) ;

gchar * mlview_utils_escape_underscore_for_gtk_widgets (const gchar *a_in_string) ;

gchar * mlview_utils_replace_word (const gchar *a_input_string,
                                   const gchar *a_lookup_word,
                                   const gchar *a_replacement_word) ;

const gchar * mlview_utils_combo_box_get_active_text (GtkComboBox *combo_box) ;

GList * mlview_utils_push_on_stack (GList *a_stack, gpointer a_element) ;

GList * mlview_utils_pop_from_stack (GList *a_stack, gpointer *a_element) ;

GList *mlview_utils_peek_from_stack (GList *a_stack, gpointer *a_element) ;

GtkTextIter * mlview_utils_text_iter_forward_chars_dup (GtkTextIter *a_iter,
        guint a_count) ;

enum MlViewStatus mlview_utils_text_iter_get_char_at (GtkTextIter *a_iter,
        guint a_count,
        gunichar *a_char) ;

enum MlViewStatus mlview_utils_text_iter_get_iter_at (GtkTextIter *a_cur_iter,
        guint a_count,
        GtkTextIter **a_iter) ;

enum MlViewStatus mlview_utils_lookup_action_group (GtkUIManager *a_manager,
        const gchar * a_name,
        GtkActionGroup **a_action_group) ;

G_END_DECLS

#endif
