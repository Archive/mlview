/* -*- Mode: C++; indent-tabs-mode: true; c-basic-offset: 4-*- */

/*This file is part of GNU MlView
 *
 *GNU MlView is free software; 
 *you can redistribute it and/or modify it under the terms of 
 *the GNU General Public License as 
 *published by the Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope 
 *that it will be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */

#ifndef __MLVIEW_EDITOR_H__
#define __MLVIEW_EDITOR_H__

#include <list>
#include <gtkmm.h>
#include <libxml/parser.h>
#include "mlview-object.h"
#include "mlview-ustring.h"
#include "mlview-node-editor.h"
#include "mlview-file-selection.h"
#include "mlview-app-context.h"
#include "mlview-xml-document.h"
#include "mlview-iview.h"
#include "mlview-action.h"
#include "mlview-view-manager.h"

/**
 *@file
 *The declaration of the #Editor class.
 *
 */
namespace mlview {

/**
 *Data structure used for the New Document dialog
 */
typedef struct _MlViewNewDocumentDialogData {
        gchar *root_node_name;
        gchar *xml_version;
        gchar *encoding;
        MlViewSchema *schema;
} MlViewNewDocumentDialogData;

struct EditorPriv ;

class Editor : public Gtk::VBox, public mlview::Object {
	friend struct EditorPriv ;
	EditorPriv *m_priv ;

	public:

	Editor (const UString &a_title) ;

	virtual ~Editor () ;


	IView * create_new_view_on_document (MlViewXMLDocument *a_xml_doc) ;

	IView * create_new_view_on_document 
		(MlViewXMLDocument *a_doc,
	         const UString &a_view_desc_type_name) ;

	IView *create_new_view_on_document (MlViewXMLDocument *a_doc,
					    ViewDescriptor *a_desc);

	IView *create_new_view_on_current_document (ViewDescriptor*); 

	IView * create_new_view_on_current_document_interactive ();

	IView * get_cur_view () const ;

	MlViewXMLDocument *get_current_document () const ;

	ViewManager* get_view_manager () const ;


	void load_xml_file (const UString &a_file_path,
	                    bool a_interactive);

	void load_xml_file_with_dtd (const UString &a_file_path,
	                             const UString &a_dtd_path,
	                             bool a_disable_interaction);

	enum MlViewStatus reload_document (bool a_interactive) ;

	void open_xml_document_interactive ();

	void open_local_xml_document_interactive ();

	void set_current_view_name (const UString &a_name);

	void set_current_view_name_interactive ();

	void edit_xml_document (xmlDocPtr a_doc,
	                        const UString &a_doc_name);

	void save_xml_document ();

	void save_xml_document_as (const UString &a_file_path);

	void save_xml_document_as_interactive ();

	UString get_current_xml_doc_file_path () const ;

	void close_xml_document_without_saving ();

	void save_and_close_xml_document ();

	bool show_new_document_dialog (MlViewNewDocumentDialogData **data) ;

	void create_new_xml_document ();

	bool close_xml_document (gboolean a_interactive = true);

	bool close_all_xml_documents (gboolean a_interactive);

	void paste_node_as_prev_sibling ();

	void paste_node_as_child ();

	void paste_node_as_next_sibling ();

	void expand_tree_to_depth_interactive ();

	void edit_settings_interactive ();

	MlViewFileSelection* get_file_selector (UString &a_title);

	gint manage_associated_schemas ();

	void validate ();

	enum MlViewStatus execute_action (MlViewAction *a_action) ;

	MlViewXMLDocument* choose_and_open_stylesheet () ;

	MlViewXMLDocument* select_xsl_doc () ;

	void xslt_transform_document_interactive ();

	gint get_number_of_views_opened_with_doc (MlViewXMLDocument *a_doc) const;

	GtkWidget *show_schemas_window_for_doc (MlViewXMLDocument *a_doc);

	GtkWidget *show_validation_window_for_doc (MlViewXMLDocument *a_doc);

	enum MlViewStatus make_current_view_populate_application_edit_menu () ;

	bool is_document_opened_in_editor (const UString &a_doc_uri) const;

	enum MlViewStatus undo () ;

	enum MlViewStatus redo () ;

	bool can_undo () ;

	bool can_redo () ;

	//**************************
	// signals
	//**************************
	sigc::signal0<void> signal_document_changed () ;

protected:

	//***************************
	//forbid copy and assignation
	//***************************
	Editor (Editor &a_editor) ;
	Editor& operator= (Editor &an_other_editor) ;

	Gtk::Widget * create_tab_title (IView *a_view, UString &a_title) ;

	void connect_to_app_context (AppContext *a_context) ;

	void disconnect_from_app_context (AppContext *a_context) ;

	GtkWidget * build_reload_file_confirmation_dialog (void) ;

	ViewDescriptor const* select_view_to_open (void) ;

	bool is_view_added_to_editor (IView *a_view) ;

	bool confirm_close () ;

	void set_cur_view (IView *a_view) ;

}
;//end class mlview::Editor

} //end namespace mlview
#endif /*__MLVIEW_EDITOR_H__*/
