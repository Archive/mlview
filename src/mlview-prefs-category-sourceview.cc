/* -*- Mode: C++; indent-tabs-mode:true ; c-basic-offset: 4-*- */
/*
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute 
 *it and/or modify it under the terms of 
 *the GNU General Public License as published by the 
 *Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYING file for copyright information.
 */
#include "mlview-exception.h"
#include "mlview-prefs-category-sourceview.h"


namespace mlview
{

struct PrefsCategorySourceViewPriv
{
	// Preferences keys
	static const char *SHOW_LINE_NUMBERS_KEY;
	static const char *TABS_WIDTH_KEY;
	static const char *REPLACE_TABS_KEY;
	static const char *AUTO_INDENT_KEY;
	static const char *SHOW_MARGIN_KEY;
	static const char *MARGIN_POSITION_KEY;
	static const char *FONT_NAME_KEY;
	// Signals
	sigc::signal0<void> signal_show_line_number_changed;
	sigc::signal0<void> signal_tabs_width_changed;
	sigc::signal0<void> signal_replace_tabs_changed;
	sigc::signal0<void> signal_auto_indent_changed;
	sigc::signal0<void> signal_show_margin_changed;
	sigc::signal0<void> signal_margin_position_changed;
	sigc::signal0<void> signal_font_name_changed;
};

const char *PrefsCategorySourceViewPriv::SHOW_LINE_NUMBERS_KEY =
    "/apps/mlview/sourceview/show-line-numbers";
const char *PrefsCategorySourceViewPriv::TABS_WIDTH_KEY =
    "/apps/mlview/sourceview/tabs-width";
const char *PrefsCategorySourceViewPriv::REPLACE_TABS_KEY =
    "/apps/mlview/sourceview/replace-tabs";
const char *PrefsCategorySourceViewPriv::AUTO_INDENT_KEY =
    "/apps/mlview/sourceview/auto-indent";
const char *PrefsCategorySourceViewPriv::SHOW_MARGIN_KEY =
    "/apps/mlview/sourceview/show-margin";
const char *PrefsCategorySourceViewPriv::MARGIN_POSITION_KEY =
    "/apps/mlview/sourceview/margin-position";
const char *PrefsCategorySourceViewPriv::FONT_NAME_KEY =
    "/apps/mlview/sourceview/fontname";

PrefsCategorySourceView::PrefsCategorySourceView (PrefsStorageManager *manager)
		: PrefsCategory ("sourceview", manager)
{
	m_priv = new PrefsCategorySourceViewPriv ();
}

PrefsCategorySourceView::~PrefsCategorySourceView ()
{
	if (m_priv != NULL) {
		delete m_priv;
		m_priv = NULL;
	}
}

bool
PrefsCategorySourceView::show_line_numbers_default ()
{
	try {

		return get_storage_manager ().get_default_bool_value (
		           PrefsCategorySourceViewPriv::SHOW_LINE_NUMBERS_KEY);

	} catch (mlview::Exception& e) {
		TRACE_EXCEPTION (e);
	}

	return FALSE;
}

bool
PrefsCategorySourceView::show_line_numbers ()
{
	try {

		return get_storage_manager ().get_bool_value (
		           PrefsCategorySourceViewPriv::SHOW_LINE_NUMBERS_KEY);

	} catch (mlview::Exception& e) {
		TRACE_EXCEPTION (e);
	}

	return show_line_numbers_default ();
}


void
PrefsCategorySourceView::set_show_line_numbers (bool show)
{
	try {

		get_storage_manager ().set_bool_value (
		    PrefsCategorySourceViewPriv::SHOW_LINE_NUMBERS_KEY,
		    show);

		signal_show_line_number_changed ().emit ();

	} catch (mlview::Exception& e) {
		TRACE_EXCEPTION (e);
	}
}


int
PrefsCategorySourceView::get_tabs_width_default ()
{
	try {

		return get_storage_manager ().get_default_int_value (
		           PrefsCategorySourceViewPriv::TABS_WIDTH_KEY);

	} catch (mlview::Exception& e) {
		TRACE_EXCEPTION (e);
	}

	return 4;
}

int
PrefsCategorySourceView::get_tabs_width ()
{
	try {

		return get_storage_manager ().get_int_value (
		           PrefsCategorySourceViewPriv::TABS_WIDTH_KEY);

	} catch (mlview::Exception& e) {
		TRACE_EXCEPTION (e);
	}

	return get_tabs_width_default ();
}

void
PrefsCategorySourceView::set_tabs_width (int nchars)
{
	try {

		get_storage_manager ().set_int_value (
		    PrefsCategorySourceViewPriv::TABS_WIDTH_KEY,
		    nchars);

		signal_tabs_width_changed ().emit ();

	} catch (mlview::Exception &e) {
		TRACE_EXCEPTION (e);
	}
}

bool
PrefsCategorySourceView::replace_tabs_with_spaces_default ()
{
	try {

		return get_storage_manager ().get_default_bool_value (
		           PrefsCategorySourceViewPriv::REPLACE_TABS_KEY);

	} catch (mlview::Exception& e) {
		TRACE_EXCEPTION (e);
	}

	return FALSE;
}


bool
PrefsCategorySourceView::replace_tabs_with_spaces ()
{
	try {

		return get_storage_manager ().get_bool_value (
		           PrefsCategorySourceViewPriv::REPLACE_TABS_KEY);

	} catch (mlview::Exception& e) {
		TRACE_EXCEPTION (e);
	}

	return replace_tabs_with_spaces_default ();
}


void
PrefsCategorySourceView::set_replace_tabs_with_spaces (bool replace)
{
	try {

		get_storage_manager ().set_bool_value (
		    PrefsCategorySourceViewPriv::REPLACE_TABS_KEY,
		    replace);

		signal_replace_tabs_changed ().emit ();

	} catch (mlview::Exception &e) {
		TRACE_EXCEPTION (e);
	}
}

bool
PrefsCategorySourceView::auto_indent_default ()
{
	try {

		return get_storage_manager ().get_default_bool_value (
		           PrefsCategorySourceViewPriv::AUTO_INDENT_KEY);

	} catch (mlview::Exception& e) {
		TRACE_EXCEPTION (e);
	}

	return FALSE;
}


bool
PrefsCategorySourceView::auto_indent ()
{
	try {

		return get_storage_manager ().get_bool_value (
		           PrefsCategorySourceViewPriv::AUTO_INDENT_KEY);

	} catch (mlview::Exception& e) {
		TRACE_EXCEPTION (e);
	}

	return auto_indent_default ();
}


void
PrefsCategorySourceView::set_auto_indent (bool autoindent)
{
	try {

		get_storage_manager ().set_bool_value (
		    PrefsCategorySourceViewPriv::AUTO_INDENT_KEY,
		    autoindent);

		signal_auto_indent_changed ().emit ();

	} catch (mlview::Exception &e) {
		TRACE_EXCEPTION (e);
	}
}


bool
PrefsCategorySourceView::show_margin_default ()
{
	try {

		return get_storage_manager ().get_default_bool_value (
		           PrefsCategorySourceViewPriv::SHOW_MARGIN_KEY);

	} catch (mlview::Exception& e) {
		TRACE_EXCEPTION (e);
	}

	return FALSE;
}


bool
PrefsCategorySourceView::show_margin ()
{
	try {

		return get_storage_manager ().get_bool_value (
		           PrefsCategorySourceViewPriv::SHOW_MARGIN_KEY);

	} catch (mlview::Exception& e) {
		TRACE_EXCEPTION (e);
	}

	return replace_tabs_with_spaces_default ();
}


void
PrefsCategorySourceView::set_show_margin (bool show)
{
	try {

		get_storage_manager ().set_bool_value (
		    PrefsCategorySourceViewPriv::SHOW_MARGIN_KEY,
		    show);

		signal_show_margin_changed ().emit ();

	} catch (mlview::Exception &e) {
		TRACE_EXCEPTION (e);
	}
}


int
PrefsCategorySourceView::get_margin_position_default ()
{
	try {

		return get_storage_manager ().get_default_int_value (
		           PrefsCategorySourceViewPriv::MARGIN_POSITION_KEY);

	} catch (mlview::Exception& e) {
		TRACE_EXCEPTION (e);
	}

	return 80;
}


int
PrefsCategorySourceView::get_margin_position ()
{
	try {

		return get_storage_manager ().get_int_value (
		           PrefsCategorySourceViewPriv::MARGIN_POSITION_KEY);

	} catch (mlview::Exception& e) {
		TRACE_EXCEPTION (e);
	}

	return get_margin_position_default ();
}


void
PrefsCategorySourceView::set_margin_position (int position)
{
	try {

		get_storage_manager ().set_int_value (
		    PrefsCategorySourceViewPriv::MARGIN_POSITION_KEY,
		    position);

		signal_margin_position_changed ().emit ();

	} catch (mlview::Exception &e) {
		TRACE_EXCEPTION (e);
	}
}


UString
PrefsCategorySourceView::get_default_font_name ()
{
	try {

		return get_storage_manager ().get_default_string_value (
		           PrefsCategorySourceViewPriv::FONT_NAME_KEY);

	} catch (mlview::Exception& e) {
		TRACE_EXCEPTION (e);
	}

	return "Sans 10";
}


UString
PrefsCategorySourceView::get_font_name ()
{
	try {

		return get_storage_manager ().get_string_value (
		           PrefsCategorySourceViewPriv::FONT_NAME_KEY);

	} catch (mlview::Exception& e) {
		TRACE_EXCEPTION (e);
	}

	return get_default_font_name ();
}


void
PrefsCategorySourceView::set_font_name (const UString& fontname)
{
	try {

		get_storage_manager ().set_string_value (
		    PrefsCategorySourceViewPriv::FONT_NAME_KEY,
		    fontname);

		signal_font_name_changed ().emit ();

	} catch (mlview::Exception &e) {
		TRACE_EXCEPTION (e);
	}
}


sigc::signal0<void>&
PrefsCategorySourceView::signal_show_line_number_changed ()
{
	return m_priv->signal_show_line_number_changed;
}

sigc::signal0<void>&
PrefsCategorySourceView::signal_tabs_width_changed ()
{
	return m_priv->signal_tabs_width_changed;
}

sigc::signal0<void>&
PrefsCategorySourceView::signal_replace_tabs_changed ()
{
	return m_priv->signal_replace_tabs_changed;
}

sigc::signal0<void>&
PrefsCategorySourceView::signal_auto_indent_changed ()
{
	return m_priv->signal_auto_indent_changed;
}

sigc::signal0<void>&
PrefsCategorySourceView::signal_show_margin_changed ()
{
	return m_priv->signal_show_margin_changed;
}

sigc::signal0<void>&
PrefsCategorySourceView::signal_margin_position_changed ()
{
	return m_priv->signal_margin_position_changed;
}

sigc::signal0<void>&
PrefsCategorySourceView::signal_font_name_changed ()
{
	return m_priv->signal_font_name_changed;
}

} // namespace mlview
