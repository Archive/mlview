/* -*- Mode: C++; indent-tabs-mode: true; c-basic-offset: 4-*- */

/*
 *This file is part of MlView
 *
 *MlView is free software; you can redistribute it and/or 
 *modify it under the terms of 
 *the GNU General Public License as published 
 *by the Free Software Foundation; either version 2
 *or (at your option) any later version.
 *
 *MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView.
 *see the file COPYING. 
 *If not, write to the 
 *Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */
#include "mlview-object.h"

namespace mlview {

struct ObjectPriv {
	long refcount ;

	ObjectPriv ():
		refcount (0)
	{}

	ObjectPriv& operator= (ObjectPriv const &an_other)
	{
		if (this == &an_other)
			return *this;
		//don't copy refcount
		//put future member assignations down here ...
		return *this ;
	}
};

Object::MethodNotImplementedException::MethodNotImplementedException
         (const gchar *a_reason): Exception (a_reason)
{
}

Object::MethodNotImplementedException::MethodNotImplementedException
        (const MethodNotImplementedException &an_e) : Exception (an_e)
{
}

Object::MethodNotImplementedException::~MethodNotImplementedException
() throw ()
{
}

Object::Object ()
{
	m_priv = new ObjectPriv () ;
	ref () ;
}

Object::Object (Object const &an_object)
{
	m_priv = new ObjectPriv ();
	m_priv = an_object.m_priv ;
}

Object::~Object ()
{
	if (m_priv) {
		delete m_priv ;
		m_priv = NULL ;
	}
}

Object&
Object::operator= (Object const& an_other)
{
	if (this == &an_other)
		return *this ;
	m_priv = an_other.m_priv ;
	return *this ;
}

void
Object::ref () const
{
	++m_priv->refcount ;
}

void
Object::unref () const
{
	THROW_IF_FAIL (m_priv) ;
	if (m_priv->refcount <= 0)
		return ;
	--m_priv->refcount ;
	if (m_priv->refcount<= 0) {
		delete this ;
	}
}
} // namespace mlview

