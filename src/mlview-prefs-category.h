/* -*- Mode: C++; indent-tabs-mode: true; c-basic-offset: 4 -*- */
/*
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute 
 *it and/or modify it under the terms of 
 *the GNU General Public License as published by the 
 *Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYING file for copyright information.
 */
#ifndef MLVIEW_PREFS_CATEGORY_H
#define MLVIEW_PREFS_CATEGORY_H

#include "mlview-object.h"
#include "mlview-prefs-storage-manager.h"

namespace mlview
{
struct PrefsCategoryPriv;

///
/// \brief This class represents a functional subset of preferences.
///
/// Preferences can be divided into several categories. Each category being
/// a functional subset of the preferences.
/// For instance we could have a category for each kind of view, handling
/// specific preferences.
///
class PrefsCategory : public Object
{
	friend struct PrefsCategoryPriv;
	PrefsCategoryPriv *m_priv;

	//forbid copy/assignation
	PrefsCategory (PrefsCategory const&) ;
	PrefsCategory& operator= (PrefsCategory const&) ;

public:
	///
	/// Constructor that register the name of the category, and the associated
	/// storage manager.
	///
	/// \param category_id the category's unique identifier
	/// \param storage_manager a pointer to a storage manager instance
	///
	PrefsCategory (const UString& a_category_id,
	               PrefsStorageManager *a_storage_manager);
	virtual ~PrefsCategory ();

	///
	/// Returns the category's identifier. Categories are bound to
	/// Preferences by their id.
	///
	/// \return the category's id
	///
	const UString& get_id ();
protected:
	///
	/// Returns a pointer to the storage manager
	///
	/// \return the address of the storage manager
	///
	PrefsStorageManager& get_storage_manager ();
};

} // namespace mlview

#endif
