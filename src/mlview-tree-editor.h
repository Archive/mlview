/* -*- Mode: C++; indent-tabs-mode:nil; c-basic-offset: 4-*- */
/*
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute 
 *it and/or modify it under the terms of 
 *the GNU General Public License as published by the 
 *Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */

/**
 *This class is the abstraction of a visual tree editor.
 *Given an xmlNode (libxml2) based tree, 
 *it can visualize it, support cut/copy/paste operations
 *that modifies directly the underlying xmlNode based tree etc ...
 */
#ifndef __MLVIEW_TREE_EDITOR_H__
#define __MLVIEW_TREE_EDITOR_H__

#include <gtk/gtk.h>
#include <libxml/tree.h>
#include "mlview-node-type-picker.h"
#include "mlview-app-context.h"
#include "mlview-xml-document.h"


/**
 *@file
 *The declaration of the #MlViewTreeEditor widget. 
 */

G_BEGIN_DECLS
/*common macros to comply with the GtkObject typing system.*/
#define MLVIEW_TYPE_TREE_EDITOR (mlview_tree_editor_get_type())
#define MLVIEW_TREE_EDITOR(object) (G_TYPE_CHECK_INSTANCE_CAST((object),MLVIEW_TYPE_TREE_EDITOR,MlViewTreeEditor))
#define MLVIEW_TREE_EDITOR_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST((klass),MLVIEW_TYPE_TREE_EDITOR,MlViewTreeEditorClass))
#define MLVIEW_IS_TREE_EDITOR(object) (G_TYPE_CHECK_INSTANCE_TYPE((object),MLVIEW_TYPE_TREE_EDITOR))
#define MLVIEW_IS_TREE_EDITOR_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass),MLVIEW_TYPE_TREE_EDITOR))
/*common class and instance definitions*/
typedef struct _MlViewTreeEditor MlViewTreeEditor;
typedef struct _MlViewTreeEditorClass MlViewTreeEditorClass;
typedef struct _MlViewTreeEditorPrivate MlViewTreeEditorPrivate;


enum MlViewTreeInsertType {
    INSERT_TYPE_APPEND_CHILD_VISIT_NEXT,
    INSERT_TYPE_PREPEND_CHILD,
    INSERT_TYPE_INSERT_BEFORE,
    INSERT_TYPE_INSERT_AFTER
};

/**
 *The #MlViewTreeEditor widget.
 *It can load an xml tree,
 *visualize it as a tree and allow
 *the user to edit the tree.
 *Each time one of the nodes of the
 *tree is selected, cut, copied etc,
 *this widget emits signals
 *that can be caught and handler by the caller.
 */
struct _MlViewTreeEditor
{
	GtkVBox vbox;
	MlViewTreeEditorPrivate *priv;
};

/********************************************************
 *The structure representing the class of all the MlViewTreeEditor objects.
 ***********************************************************/
struct _MlViewTreeEditorClass
{
	GtkVBoxClass parent_class;

	/*============================
	 *signal handlers definitions
	 *===========================*/

	/**
	 *This signal is emitted whenever an
	 *action edition changes the tree.
	 *@param a_editor the instance of #MlViewTreeEditor
	 *that emitted the signal.
	 *@param a_data some custom user data
	 */
	void (*tree_changed) (MlViewTreeEditor * a_editor,
	                      gpointer a_data);
	/**
	 *Emitted when a node is cut.
	 *@param a_editor the instance of #MlViewTreeEditor that
	 *emitted the signal.
	 *@param a_node_cut the node that has been cut.
	 *@param a_user_data a custom user data.
	 */
	void (*node_cut) (MlViewTreeEditor * a_editor,
	                  xmlNode *a_node_cut,
	                  gpointer a_user_data);
	/**
	 *Emitted when a node is pasted.
	 *@param a_editor the instance of #MlViewTreeEditor that
	 *emitted the signal.
	 *@param a_ref the row reference of the newly pasted
	 *column
	 *@param a_user_data a custom user data.
	 */
	void (*node_pasted) (MlViewTreeEditor * a_editor,
	                     GtkTreeRowReference * a_ref,
	                     gpointer a_user_data);
	/**
	 *Emitted when a node is added.
	 *"Added" means that the node is 
	 *either "inserted" or "added" as the
	 *last child node of another node.
	 *@param a_editor the instance of #MlViewTreeEditor
	 *that emitted the signal.
	 *@param a_ref the row reference of the column
	 *that has been added.
	 *@param a_user_data some custom user data.
	 */
	void (*node_added) (MlViewTreeEditor * a_editor,
	                    GtkTreeRowReference * a_ref,
	                    gpointer a_user_data);
	/**
	 *Emitted when a node is selected.
	 *@param a_editor the instance of #MlViewTreeEditor that
	 *emitted the signal.
	 *@param a_ref the tree row reference of the column
	 *that emited the signal.
	 *@param a_user_data a custom user data.
	 */
	void (*node_selected) (MlViewTreeEditor * a_editor,
	                       GtkTreeRowReference *a_ref,
	                       gpointer a_user_data);

	GtkTreeView * (*build_tree_view_from_xml_doc) (MlViewTreeEditor * a_this,
	        xmlDoc * a_doc);

	enum MlViewStatus (*update_visual_node) (MlViewTreeEditor *a_this,
											 GtkTreeIter * a_iter,
											 gboolean a_selected);

	enum MlViewStatus (*build_tree_model_from_xml_tree)
				(MlViewTreeEditor * a_this, const xmlNode * a_node,
				 GtkTreeIter * a_ref_iter, enum MlViewTreeInsertType a_type,
				 GtkTreeModel ** a_model);

	void (*ungrab_focus_requested) (MlViewTreeEditor *a_this,
	                                gpointer a_user_data) ;
	enum MlViewStatus (*clear) (MlViewTreeEditor *a_this) ;

	enum MlViewStatus (*reload_from_doc) (MlViewTreeEditor *a_this) ;
};


guint mlview_tree_editor_get_type (void);

GtkWidget *mlview_tree_editor_new ();

void mlview_tree_editor_construct (MlViewTreeEditor *editor);

enum MlViewStatus mlview_tree_editor_internal_general_entity_to_string
				(MlViewTreeEditor *a_this,
				 xmlEntity *a_entity,
                                 bool selected,
				 gchar **a_string) ;

enum MlViewStatus mlview_tree_editor_external_general_parsed_entity_to_string
                                (MlViewTreeEditor *a_this,
                                 xmlEntity *a_entity,
                                 bool selected,
                                 gchar **a_string) ;

enum MlViewStatus mlview_tree_editor_external_general_unparsed_entity_to_string
                                (MlViewTreeEditor *a_this,
                                 xmlEntity *a_entity,
                                 bool selected,
                                 gchar **a_string) ;

enum MlViewStatus mlview_tree_editor_internal_parameter_entity_to_string
                                (MlViewTreeEditor *a_this,
                                 xmlEntity *a_entity,
                                 bool selected,
                                 gchar **a_string) ;

enum MlViewStatus mlview_tree_editor_external_parameter_entity_to_string
				(MlViewTreeEditor *a_this,
				 xmlEntity *a_entity,
                                 bool selected,
				 gchar **a_string) ;

enum MlViewStatus mlview_tree_editor_dtd_node_to_string
				(MlViewTreeEditor *a_this,
				 xmlDtd *a_dtd_node,
                                 bool selected,
				 gchar **a_string) ;

enum MlViewStatus mlview_tree_editor_entity_ref_to_string
				(MlViewTreeEditor *a_this,
				 xmlNode *a_node,
                                 bool selected,
				 gchar **a_string) ;

enum MlViewStatus mlview_tree_editor_document_node_to_string
				(MlViewTreeEditor *a_this,
				 xmlNode *a_node,
                                 bool selected,
				 gchar **a_string) ;

enum MlViewStatus mlview_tree_editor_cdata_section_to_string
				(MlViewTreeEditor *a_this,
				 xmlNode *a_node,
				 gchar **a_result) ;

GtkTreeModel *mlview_tree_editor_get_model (MlViewTreeEditor *a_this);

void mlview_tree_editor_destroy (GtkObject * a_object);

enum MlViewStatus mlview_tree_editor_edit_xml_doc (MlViewTreeEditor * a_editor,
												   MlViewXMLDocument * a_doc);

enum MlViewStatus mlview_tree_editor_clear (MlViewTreeEditor *a_this) ;

enum MlViewStatus mlview_tree_editor_reload_from_doc (MlViewTreeEditor *a_this) ;

GtkTreeView *mlview_tree_editor_get_tree_view (MlViewTreeEditor * a_this);

GtkStyle *mlview_tree_editor_get_style (MlViewTreeEditor * a_this);

void mlview_tree_editor_set_style (MlViewTreeEditor * a_this,
                                   GtkStyle *style);

enum MlViewStatus mlview_tree_editor_get_iter (MlViewTreeEditor * a_this,
											   xmlNode * a_node,
											   GtkTreeIter * a_iter);

GtkTreeRowReference* mlview_tree_editor_get_sel_start (MlViewTreeEditor * a_this);

enum MlViewStatus mlview_tree_editor_update_visual_node
							(MlViewTreeEditor * a_this,
							 GtkTreeIter * a_iter,
							 gboolean a_selected);

enum MlViewStatus mlview_tree_editor_update_visual_node2
							(MlViewTreeEditor * a_this,
							 xmlNode * a_node,
							 gboolean a_selected);

enum MlViewStatus mlview_tree_editor_add_child_node
							(MlViewTreeEditor * a_tree_editor,
							 GtkTreeIter * a_parent_iter,
							 xmlNodePtr a_xml_node);

enum MlViewStatus mlview_tree_editor_add_child_element_node
							(MlViewTreeEditor *a_this,
							 const gchar *a_element_name,
							 gboolean a_start_edit_element) ;

void mlview_tree_editor_add_child_element_interactive (MlViewTreeEditor *a_this) ;

void mlview_tree_editor_add_child_text_node
							(MlViewTreeEditor *a_this,
							 const gchar* a_text,
							 gboolean a_start_editing) ;

enum MlViewStatus mlview_tree_editor_insert_prev_text_node
							(MlViewTreeEditor *a_this,
							 const gchar *a_text,
							 gboolean a_start_editing) ;

enum MlViewStatus mlview_tree_editor_insert_next_text_node
							(MlViewTreeEditor *a_this,
							 const gchar *a_text,
							 gboolean a_start_editing) ;

void mlview_tree_editor_add_child_text_node_interactive
							(MlViewTreeEditor *a_this) ;

enum MlViewStatus mlview_tree_editor_insert_sibling_node (MlViewTreeEditor * a_this,
        GtkTreeIter *a_ref_iter,
        xmlNode * a_node,
        gboolean a_previous);

enum MlViewStatus mlview_tree_editor_cut_node (MlViewTreeEditor * a_this,
        GtkTreeIter * a_iter);

enum MlViewStatus mlview_tree_editor_cut_node2 (MlViewTreeEditor * a_this,
        GtkTreePath * a_path);

enum MlViewStatus mlview_tree_editor_cut_node3 (MlViewTreeEditor * a_this,
        xmlNode *a_node) ;

void mlview_tree_editor_cut_cur_node (MlViewTreeEditor *a_this) ;

enum MlViewStatus mlview_tree_editor_comment_current_node (MlViewTreeEditor *a_this) ;

enum MlViewStatus mlview_tree_editor_uncomment_current_node (MlViewTreeEditor *a_this) ;

enum MlViewStatus mlview_tree_editor_copy_node (MlViewTreeEditor * a_this,
        GtkTreeIter * a_iter) ;

enum MlViewStatus mlview_tree_editor_copy_node2 (MlViewTreeEditor * a_this,
        GtkTreePath * a_path) ;

enum MlViewStatus mlview_tree_editor_copy_current_node (MlViewTreeEditor *a_this) ;

void  mlview_tree_editor_create_new_xml_doc (MlViewTreeEditor * a_this,
        MlViewXMLDocument * a_doc);

enum MlViewStatus mlview_tree_editor_search_interactive (MlViewTreeEditor *a_this) ;

enum MlViewStatus mlview_tree_editor_search (MlViewTreeEditor *a_this,
        GtkTreeRowReference *a_from,
        struct SearchConfig *a_config,
        xmlNode **a_node_found) ;

void mlview_tree_editor_set_root_element (MlViewTreeEditor *a_this,
        xmlNode *a_node,
        gboolean a_emit_signals) ;

enum MlViewStatus mlview_tree_editor_paste_node_as_child (MlViewTreeEditor *a_this,
        GtkTreeIter *a_parent_iter) ;

enum MlViewStatus mlview_tree_editor_paste_node_as_child2 (MlViewTreeEditor *a_this) ;

enum MlViewStatus mlview_tree_editor_paste_node_as_sibling (MlViewTreeEditor *a_this,
        GtkTreeIter *a_ref_iter,
        gboolean a_previous) ;
enum MlViewStatus mlview_tree_editor_paste_node_as_sibling2 (MlViewTreeEditor *a_this,
        GtkTreePath *a_ref_path,
        gboolean a_previous) ;

enum MlViewStatus mlview_tree_editor_paste_node_as_prev_sibling (MlViewTreeEditor *a_this) ;

enum MlViewStatus mlview_tree_editor_paste_node_as_next_sibling (MlViewTreeEditor *a_this) ;

MlViewXMLDocument * mlview_tree_editor_get_mlview_xml_doc (MlViewTreeEditor *a_this) ;

enum MlViewStatus mlview_tree_editor_get_cur_sel_start_tree_path (MlViewTreeEditor *a_this,
        GtkTreePath **a_tree_path) ;

enum MlViewStatus mlview_tree_editor_get_cur_sel_start_iter (MlViewTreeEditor *a_this,
        GtkTreeIter *a_iter) ;
GtkTreeRowReference* mlview_tree_editor_get_cur_sel_start (MlViewTreeEditor *a_this) ;

xmlNode * mlview_tree_editor_get_cur_sel_xml_node (MlViewTreeEditor *a_this) ;

MlViewNodeTypePicker* mlview_tree_editor_get_node_type_picker (MlViewTreeEditor * a_tree_editor);

enum MlViewStatus mlview_tree_editor_set_node_type_picker (MlViewTreeEditor *a_this,
        MlViewNodeTypePicker *a_picker) ;

GtkTreeRowReference * mlview_tree_editor_iter_2_row_ref (MlViewTreeEditor *a_this,
        GtkTreeIter *a_iter) ;

GtkTreeRowReference * mlview_tree_editor_xml_node_2_row_reference (MlViewTreeEditor *a_this,
        xmlNode *a_node) ;


void mlview_tree_editor_add_child_node_interactive (MlViewTreeEditor * a_tree_editor);

void mlview_tree_editor_insert_sibling_node_interactive (MlViewTreeEditor * a_tree_editor);

void mlview_tree_editor_insert_prev_sibling_element_node (MlViewTreeEditor *a_this,
        const gchar *a_element_name,
        gboolean a_start_editing) ;

void mlview_tree_editor_insert_next_sibling_element_node (MlViewTreeEditor *a_this,
        const gchar *a_element_name,
        gboolean a_start_editing) ;

void mlview_tree_editor_insert_next_sibling_node_interactive  (MlViewTreeEditor * a_tree_editor);

void mlview_tree_editor_insert_next_sibling_node_interactive2  (MlViewTreeEditor * a_tree_editor);

void mlview_tree_editor_insert_prev_sibling_node_interactive  (MlViewTreeEditor * a_tree_editor);

void mlview_tree_editor_insert_prev_sibling_element_interactive (MlViewTreeEditor *a_this) ;

void mlview_tree_editor_insert_next_sibling_element_interactive (MlViewTreeEditor *a_this) ;


void mlview_tree_editor_set_xml_document_path  (MlViewTreeEditor * a_tree_editor,
        const gchar * a_file_path);

void mlview_tree_editor_set_to_modified (MlViewTreeEditor *a_this,
        gboolean a_is_modified) ;

xmlNode *mlview_tree_editor_get_xml_node  (MlViewTreeEditor * a_this,
        GtkTreeIter * a_iter);

xmlNode * mlview_tree_editor_get_xml_node3 (MlViewTreeEditor *a_this,
        GtkTreePath *a_path) ;

xmlNode * mlview_tree_editor_get_xml_node2  (MlViewTreeEditor * a_this,
        GtkTreeRowReference * a_row_ref) ;

void mlview_tree_editor_set_application_context  (MlViewTreeEditor * a_this);

void mlview_tree_editor_update_contextual_menu  (MlViewTreeEditor * a_tree_editor,
        GtkMenu **a_menu_ptr);

enum MlViewStatus mlview_tree_editor_update_child_node_added (MlViewTreeEditor * a_this,
        xmlNode * a_parent,
        xmlNode * a_node,
        gboolean a_emit_signals);

enum MlViewStatus mlview_tree_editor_update_internal_subset_added (MlViewTreeEditor *a_this,
        xmlDtd *a_subset_node) ;

enum MlViewStatus mlview_tree_editor_update_node_cut  (MlViewTreeEditor * a_this,
        xmlNode * a_parent_node,
        xmlNode * a_node_cut);

enum MlViewStatus mlview_tree_editor_update_node_pasted  (MlViewTreeEditor * a_this,
        xmlNode * a_parent_node,
        xmlNode * a_node,
        gboolean a_emit_signals);

enum MlViewStatus mlview_tree_editor_update_sibling_node_inserted (MlViewTreeEditor * a_this,
        xmlNode * a_ref_node,
        xmlNode * a_inserted_node,
        gboolean a_previous,
        gboolean a_emit_signals);

enum MlViewStatus mlview_tree_editor_update_node_commented (MlViewTreeEditor *a_this,
        xmlNode *a_old_node,
        xmlNode *a_new_node) ;


void mlview_tree_editor_expand_tree_to_depth (MlViewTreeEditor * a_this,
        gint a_depth) ;

void mlview_tree_editor_toggle_node_folding (MlViewTreeEditor *a_this) ;

void mlview_tree_editor_select_node (MlViewTreeEditor *a_this,
                                     xmlNode *a_node,
                                     gboolean a_select_issued_by_model,
                                     gboolean a_signal_model) ;

void mlview_tree_editor_start_editing_node (MlViewTreeEditor *a_this,
        xmlNode *a_node) ;

enum MlViewStatus mlview_tree_editor_connect_to_doc (MlViewTreeEditor *a_this,
        MlViewXMLDocument *a_doc) ;

enum MlViewStatus mlview_tree_editor_disconnect_from_doc (MlViewTreeEditor *a_this,
        MlViewXMLDocument *a_doc) ;

enum MlViewStatus mlview_tree_editor_edit_xml_entity_decl_node (MlViewTreeEditor *a_this,
        xmlEntity *a_entity_node,
        gchar *a_new_text) ;

enum MlViewStatus mlview_tree_editor_edit_dtd_node (MlViewTreeEditor *a_this,
        xmlDtd *a_dtd_node,
        gchar *a_new_text) ;

enum MlViewStatus mlview_tree_editor_edit_cdata_section_node (MlViewTreeEditor *a_this,
        xmlNode *a_cur_node,
        gchar *a_new_text) ;

gchar * mlview_tree_editor_build_attrs_list_str (MlViewTreeEditor *a_this,
                                                 xmlNode * a_node,
                                                 bool selected) ;

const gchar *mlview_tree_editor_get_colour_string (MlViewTreeEditor *a_this,
        xmlElementType a_type);

GHashTable * mlview_tree_editor_get_nodes_rows_hash (MlViewTreeEditor *a_this) ;

void mlview_tree_editor_set_nodes_rows_hash (MlViewTreeEditor *a_this,
        GHashTable *a_nodes_rows_hash) ;


enum MlViewStatus  mlview_tree_editor_build_tree_model_from_xml_tree (MlViewTreeEditor * a_this,
        const xmlNode * a_node,
        GtkTreeIter * a_ref_iter,
        enum MlViewTreeInsertType a_type,
        GtkTreeModel ** a_model) ;

GtkTreeView * mlview_tree_editor_build_tree_view_from_xml_doc (MlViewTreeEditor * a_this,
        xmlDoc * a_doc) ;

enum MlViewStatus mlview_tree_editor_request_ungrab_focus (MlViewTreeEditor *a_this) ;

enum MlViewStatus mlview_tree_editor_grab_focus (MlViewTreeEditor *a_this) ;

enum MlViewStatus mlview_tree_editor_scroll_to_cell (MlViewTreeEditor *a_this,
        GtkTreePath *a_tree_path) ;

enum MlViewStatus mlview_tree_editor_select_next_sibling_node (MlViewTreeEditor *a_this) ;

enum MlViewStatus mlview_tree_editor_select_next_sibling_node2 (MlViewTreeEditor *a_this,
        GtkTreeRowReference *a_cur_sel_node) ;

enum MlViewStatus mlview_tree_editor_select_prev_sibling_node (MlViewTreeEditor *a_this) ;

enum MlViewStatus mlview_tree_editor_select_prev_sibling_node2 (MlViewTreeEditor *a_this,
        GtkTreeRowReference *a_cur_sel_node) ;

enum MlViewStatus mlview_tree_editor_select_parent_node (MlViewTreeEditor *a_this) ;

enum MlViewStatus mlview_tree_editor_select_parent_node2 (MlViewTreeEditor *a_this,
        GtkTreeRowReference *a_cur_sel_node) ;

void mlview_tree_editor_select_node2 (MlViewTreeEditor *a_this,
                                      GtkTreePath *a_tree_path,
                                      gboolean a_issued_by_model,
                                      gboolean a_emit_signal) ;
G_END_DECLS

#endif /*__MLVIEW_TREE_EDITOR_H__*/
