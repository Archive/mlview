/* -*- Mode: C++; indent-tabs-mode: nil; c-basic-offset: 4-*- */

/*This file is part of GNU MlView
 *
 *GNU MlView is free software; 
 *you can redistribute it and/or modify it under the terms of 
 *the GNU General Public License as 
 *published by the Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope 
 *that it will be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */
#include "mlview-gvc-factory.h"
#include "mlview-old-gvc.h"

namespace mlview
{

GVCIface*
GVCFactory::create_gvc (const UString &gvc_type_name)
{
	GVCIface *result = NULL ;
	if (gvc_type_name == "OldGVC") {
		result = manage (new OldGVC ()) ;
	} else {
		THROW (UString ("graphical view type: ") + gvc_type_name
		       + UString (" is unknown")) ;
	}
	return result ;
}

}
