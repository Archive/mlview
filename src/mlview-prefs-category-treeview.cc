/* -*- Mode: C++; indent-tabs-mode:true ; c-basic-offset: 4-*- */

/*
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute 
 *it and/or modify it under the terms of 
 *the GNU General Public License as published by the 
 *Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */
#include <math.h>

#include "mlview-exception.h"
#include "mlview-prefs-category-treeview.h"
#include "mlview-ui-utils.h"

namespace mlview
{
struct PrefsCategoryTreeviewPriv
{
// Preferences keys
    static const char *TREE_DEPTH_KEY;
    static const char *ENABLE_CBOX_KEY;
    static const char *FONT_NAME_KEY;
    static const char *COLOR_KEYS_PATH;
// Signals
    sigc::signal0<void> signal_font_changed;
    sigc::signal0<void> signal_colour_changed;
// Functions
    const Glib::ustring
    tree_editor_node_colour_to_type (mlview::TreeEditorsNodeColour a_colour);
    unsigned int color_value (int color);
};

const char* PrefsCategoryTreeview::CATEGORY_ID = "treeview";

const char* PrefsCategoryTreeviewPriv::TREE_DEPTH_KEY  =
    "/apps/mlview/treeview/default-tree-expansion-depth";
const char* PrefsCategoryTreeviewPriv::ENABLE_CBOX_KEY =
    "/apps/mlview/treeview/enable-completion-box";
const char* PrefsCategoryTreeviewPriv::FONT_NAME_KEY =
    "/apps/mlview/treeview/fontname";
const char* PrefsCategoryTreeviewPriv::COLOR_KEYS_PATH =
    "/apps/mlview/treeview/colours/";

// Constructor
PrefsCategoryTreeview::PrefsCategoryTreeview
(PrefsStorageManager *manager_)
    : PrefsCategory (PrefsCategoryTreeview::CATEGORY_ID, manager_)
{
    m_priv = new PrefsCategoryTreeviewPriv ();
}
// Destructor
PrefsCategoryTreeview::~PrefsCategoryTreeview ()
{
    if (m_priv != NULL) {
	delete m_priv;
	m_priv = NULL;
    }
}

//
// Accessors
//
int
PrefsCategoryTreeview::get_default_tree_expansion_depth_default ()
{
    try {

	return get_storage_manager ().get_default_int_value (
		PrefsCategoryTreeviewPriv::TREE_DEPTH_KEY);

    } catch (mlview::Exception& e) {
	TRACE_EXCEPTION (e);
    }
    return 4;
}

int
PrefsCategoryTreeview::get_default_tree_expansion_depth ()
{
    try {

	return get_storage_manager ().get_int_value (
		PrefsCategoryTreeviewPriv::TREE_DEPTH_KEY);

    } catch (mlview::Exception& e) {
	TRACE_EXCEPTION (e);
    }

    return get_default_tree_expansion_depth_default ();
}

void
PrefsCategoryTreeview::set_default_tree_expansion_depth (int value)
{
    get_storage_manager ().set_int_value (
	    PrefsCategoryTreeviewPriv::TREE_DEPTH_KEY,
	    value);
}

bool
PrefsCategoryTreeview::enable_completion_box_default ()
{
    try {

	return get_storage_manager ().get_default_bool_value (
		PrefsCategoryTreeviewPriv::ENABLE_CBOX_KEY);

    } catch (mlview::Exception& e) {
	TRACE_EXCEPTION (e);
    }
    return TRUE;
}

bool
PrefsCategoryTreeview::enable_completion_box ()
{
    try {

	return get_storage_manager ().get_bool_value (
		PrefsCategoryTreeviewPriv::ENABLE_CBOX_KEY);

    } catch (mlview::Exception& e) {
	TRACE_EXCEPTION (e);
    }

    return enable_completion_box_default ();
}

void
PrefsCategoryTreeview::set_enable_completion_box (bool flag)
{
    get_storage_manager ().set_bool_value (
	    PrefsCategoryTreeviewPriv::ENABLE_CBOX_KEY,
	    flag);
}

Glib::ustring
PrefsCategoryTreeview::get_default_font_name ()
{
    try {

	return get_storage_manager ().get_default_string_value (
		PrefsCategoryTreeviewPriv::FONT_NAME_KEY);

    } catch (mlview::Exception& e) {
	TRACE_EXCEPTION (e);
    }
    return "Arial 10";
}

Glib::ustring
PrefsCategoryTreeview::get_font_name ()
{
    try {

	return get_storage_manager ().get_string_value (
		PrefsCategoryTreeviewPriv::FONT_NAME_KEY);

    } catch (mlview::Exception& e) {
	TRACE_EXCEPTION (e);
    }

    return get_default_font_name ();
}

void
PrefsCategoryTreeview::set_font_name (const Glib::ustring& name)
{
    get_storage_manager ().set_string_value (
	    PrefsCategoryTreeviewPriv::FONT_NAME_KEY,
	    name);

    signal_font_changed ().emit();
}

Glib::ustring
PrefsCategoryTreeview::get_default_color_for_type (const Glib::ustring& type_)
{
    try {
	Glib::ustring type_key =
	    PrefsCategoryTreeviewPriv::COLOR_KEYS_PATH;
	type_key += type_;

	return get_storage_manager ().get_default_string_value (type_key);

    } catch (mlview::Exception& e) {
	TRACE_EXCEPTION (e);
    }
    return "#000000";
}
Glib::ustring
PrefsCategoryTreeview::get_color_for_type (const Glib::ustring& type_)
{
    Glib::ustring type_key =
	PrefsCategoryTreeviewPriv::COLOR_KEYS_PATH;
    type_key += type_;

    try {

	return get_storage_manager ().get_string_value (type_key);

    } catch (mlview::Exception& e) {
	TRACE_EXCEPTION (e);
    }

    return get_default_color_for_type (type_key);
}
Glib::ustring
PrefsCategoryTreeview::get_default_color_for_type (mlview::TreeEditorsNodeColour colour_)
{
    try {
	const Glib::ustring &type =
	    m_priv->tree_editor_node_colour_to_type (colour_);

	Glib::ustring type_key =
	    PrefsCategoryTreeviewPriv::COLOR_KEYS_PATH;
	type_key += type;

	return get_storage_manager ().get_default_string_value (type_key);

    } catch (mlview::Exception& e) {
	TRACE_EXCEPTION (e);
    }
    return "#000000";
}
Glib::ustring
PrefsCategoryTreeview::get_color_for_type (mlview::TreeEditorsNodeColour colour_)
{

    const Glib::ustring& type =
	m_priv->tree_editor_node_colour_to_type (colour_);

    Glib::ustring type_key =
	PrefsCategoryTreeviewPriv::COLOR_KEYS_PATH;
    type_key += type;

    try {

	return get_storage_manager ().get_string_value (type_key);

    } catch (mlview::Exception& e) {
	TRACE_EXCEPTION (e);
    }

    return get_default_color_for_type (type_key);
}


void
PrefsCategoryTreeview::set_color_for_type (const Glib::ustring& type_,
					   const Glib::ustring& color)
{
    Glib::ustring type_key =
	PrefsCategoryTreeviewPriv::COLOR_KEYS_PATH;
    type_key += type_;

    get_storage_manager ().set_string_value (type_key, color);
}

void
PrefsCategoryTreeview::set_color_for_type (const Glib::ustring& type_,
					   const Gdk::Color color)
{
    try {

	set_color_for_type (type_,
			    mlview::gdk_color_to_html_string (color));

	signal_colour_changed ().emit ();

    } catch (mlview::Exception& e) {
	TRACE_EXCEPTION (e);
    }

}

sigc::signal0<void>&
PrefsCategoryTreeview::signal_font_changed ()
{
    return m_priv->signal_font_changed;
}

sigc::signal0<void>&
PrefsCategoryTreeview::signal_colour_changed ()
{
    return m_priv->signal_colour_changed;
}


const Glib::ustring
PrefsCategoryTreeviewPriv::tree_editor_node_colour_to_type (mlview::TreeEditorsNodeColour a_colour)
{
    const char* TreeEditorsNodeColourTypes[] = {
	"xml-element-node",
	"xml-attribute-node",
	"xml-attribute-value-node",
	"xml-text-node",
	"xml-comment-node",
	"xml-document-node",
	"xml-pi-node",
	"xml-dtd-node",
	"xml-entity-decl-node",
	NULL
    };

    return TreeEditorsNodeColourTypes[a_colour];
}

} // namespace mlview
