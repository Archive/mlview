/* -*- Mode: C++; indent-tabs-mode:true ; c-basic-offset: 4-*- */

/*
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute 
 *it and/or modify it under the terms of 
 *the GNU General Public License as published by the 
 *Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */
#include "mlview-prefs-category.h"
#include "mlview-exception.h"

namespace mlview
{

struct PrefsCategoryPriv
{
	UString id;
	PrefsStorageManager *storage_manager;
};

PrefsCategory::PrefsCategory (const UString &a_category_id,
                              PrefsStorageManager *a_mgr)
{
	THROW_IF_FAIL (a_mgr) ;

	m_priv = new PrefsCategoryPriv ();
	m_priv->id = UString (a_category_id) ;
	m_priv->storage_manager = a_mgr;
}

PrefsCategory::~PrefsCategory ()
{
	if (m_priv != NULL) {
		delete m_priv;
		m_priv = NULL ;
	} else {
		THROW ("double free") ;
	}
}

const UString&
PrefsCategory::get_id ()
{
	THROW_IF_FAIL (m_priv) ;
	return m_priv->id ;
}

PrefsStorageManager&
PrefsCategory::get_storage_manager ()
{
	THROW_IF_FAIL (m_priv) ;
	return *m_priv->storage_manager;
}

} // namespace mlview
