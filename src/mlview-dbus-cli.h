/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 8-*- */

/*This file is part of GNU MlView
 *
 *GNU MlView is free software; 
 *you can redistribute it and/or modify it under the terms of 
 *the GNU General Public License as published by 
 *the Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope that it will be useful, 
 *but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General 
 *Public License along with MlView; 
 *see the file COPYING. If not, write to the 
 *Free Software Foundation, Inc., 59 
 *Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright informations.
 */

#ifndef __MLVIEW_DBUS_CLI_H__
#define __MLVIEW_DBUS_CLI_H__

#include "mlview-utils.h"
#ifdef MLVIEW_WITH_DBUS

G_BEGIN_DECLS

#define MLVIEW_TYPE_DBUS_CLI (mlview_dbus_cli_get_type())
#define MLVIEW_DBUS_CLI(object) (G_TYPE_CHECK_INSTANCE_CAST((object), MLVIEW_TYPE_DBUS_CLI, MlViewDBusCli))
#define MLVIEW_DBUS_CLI_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST((klass), MLVIEW_TYPE_DBUS_CLI, MlViewContextClass))
#define MLVIEW_IS_DBUS_CLI(object) (G_TYPE_CHECK_INSTANCE_TYPE((object), MLVIEW_TYPE_DBUS_CLI))
#define MLVIEW_IS_DBUS_CLI_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass), MLVIEW_TYPE_DBUS_CLI))

typedef struct _MlViewDBusCli MlViewDBusCli ;
typedef struct _MlViewDBusCliClass MlViewDBusCliClass ;
typedef struct _MlViewDBusCliPriv MlViewDBusCliPriv ;

enum MlViewDBusCliStatus {

    MLVIEW_DBUS_CLI_OK,
    MLVIEW_DBUS_CLI_ERROR,
    MLVIEW_DBUS_CLI_BAD_PARAM_ERROR,

    /***reasons to display help*/
    MLVIEW_DBUS_CLI_DISPLAY_HELP_ERROR,
    MLVIEW_DBUS_CLI_UNKNOWN_COMMAND_ERROR,
    /*****************************/

    MLVIEW_DBUS_CLI_SERVICE_INVOCATION_FAILED_ERROR,
    MLVIEW_DBUS_CLI_PING_FAILED_ERROR,
    MLVIEW_DBUS_CLI_LOAD_XML_FILE_FAILED_ERROR,
    MLVIEW_DBUS_CLI_CLOSE_APPLICATION_FAILED_ERROR,
    MLVIEW_DBUS_CLI_BAD_USAGE_ERROR
} ;

enum MlViewDBusCliCommand {
    MLVIEW_DBUS_CLI_UNKNOWN_COMMAND,
    MLVIEW_DBUS_CLI_LIST_SERVICES,
    MLVIEW_DBUS_CLI_PING_SERVICE,
    MLVIEW_DBUS_CLI_LOAD_XML_FILE,
    MLVIEW_DBUS_CLI_CLOSE_APPLICATION
} ;

struct MlViewDBusCliOptions
{
	gboolean display_help ;

	enum MlViewDBusCliCommand command ;

	/*ping-service argument*/
	gchar * service_name ;

	/*load-xml-file arguments*/
	gchar *xml_file_uri ;
	gchar *dtd_uri ;
} ;

struct _MlViewDBusCli
{
	GObject parent_object ;
	MlViewDBusCliPriv *priv ;
} ;

struct _MlViewDBusCliClass
{
	GObjectClass parent_class ;
} ;

GType mlview_dbus_cli_get_type (void) ;

MlViewDBusCli * mlview_dbus_cli_new (void) ;


enum MlViewDBusCliStatus mlview_dbus_cli_parse_command_line
(MlViewDBusCli *a_this,
 gint a_argc,
 gchar **a_argv) ;

enum MlViewDBusCliStatus mlview_dbus_cli_get_options
(MlViewDBusCli *a_this,
 struct MlViewDBusCliOptions **a_options) ;

enum MlViewDBusCliStatus mlview_dbus_cli_execute_command (MlViewDBusCli *a_this) ;

enum MlViewDBusCliStatus mlview_dbus_cli_list_services (MlViewDBusCli *a_this) ;

enum MlViewDBusCliStatus mlview_dbus_cli_ping_service (MlViewDBusCli *a_this) ;

enum MlViewDBusCliStatus mlview_dbus_cli_load_xml_file (MlViewDBusCli *a_this) ;

enum MlViewDBusCliStatus mlview_dbus_cli_close_application
(MlViewDBusCli *a_this) ;
G_END_DECLS
#endif /*MLVIEW_WITH_DBUS*/
#endif /*__MLVIEW_DBUS_CLI_H__*/
