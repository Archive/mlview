/* -*- Mode: C++; indent-tabs-mode: nil; c-basic-offset: 4-*- */

/*
 *This file is part of MlView
 *
 *MlView is free software; you can redistribute it and/or 
 *modify it under the terms of 
 *the GNU General Public License as published 
 *by the Free Software Foundation; either version 2
 *or (at your option) any later version.
 *
 *MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView.
 *see the file COPYING. 
 *If not, write to the 
 *Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */


#include <string.h>
#include "mlview-app-context.h"
#include "mlview-service.h"
#include "mlview-idbo.h"
#include "mlview-editor-dbo.h"
#include "mlview-ping-dbo.h"

#ifdef MLVIEW_WITH_DBUS
#define DBUS_API_SUBJECT_TO_CHANGE
#include <dbus/dbus.h>
#include <dbus/dbus-glib-lowlevel.h>
#endif /*MLVIEW_WITH_DBUS*/


#define PRIVATE(obj) (obj)->priv

struct _MlViewServicePriv
{
#ifdef MLVIEW_WITH_DBUS
	DBusConnection *dbus_connection;
#endif /*MLVIEW_WITH_DBUS*/

	char * name ;
	gboolean dispose_has_run ;
} ;

static GObjectClass *gv_parent_class = NULL ;

MlViewService *gv_service = NULL ;

#ifdef MLVIEW_WITH_DBUS
struct MlViewDBusObjectDesc
{
	const char * name ;
	const char * path ;
	DBusObjectPathVTable dbus_vtable ;
	MlViewIDBOCreateInstanceFunc create_instance ;
	MlViewIDBO *instance;
} ;

struct MlViewDBusObjectDesc gv_dbus_objects[] =
    {

	    {
		    "MlViewEditorObject",
		    PATH_ORG_MLVIEW_EDITOR_OBJECT,
		    {0},
		    (MlViewIDBOCreateInstanceFunc) mlview_editor_dbo_new,
		    NULL/*must be initialized to NULL*/
	    },
	    {
	        "MlViewPingObject",
	        PATH_ORG_MLVIEW_PING_OBJECT,
	        {0},
	        (MlViewIDBOCreateInstanceFunc) mlview_ping_dbo_new,
	        NULL
	    }
    } ;
#endif /*MLVIEW_WITH_DBUS*/

static void mlview_service_class_init (MlViewServiceClass *a_klass) ;
static void mlview_service_init (MlViewService *a_this) ;
static void mlview_service_dispose (GObject *a_this) ;
static void mlview_service_finalize (GObject *a_this) ;
static enum MlViewStatus mlview_service_construct (MlViewService *a_this) ;
static MlViewService * mlview_service_new (void) ;

/*dbus stuffs*/
#ifdef MLVIEW_WITH_DBUS
static enum MlViewStatus get_bus (MlViewService *a_this,
                                  DBusConnection **a_connection,
                                  GError **a_error) ;
static enum MlViewStatus try_to_start_dbus_service (MlViewService *a_this,
        GError **an_error) ;

static enum MlViewStatus try_to_stop_dbus_service (MlViewService *a_this,
        GError **an_error) ;

static enum MlViewStatus register_dbus_objects (MlViewService *a_this) ;

static enum MlViewStatus unregister_dbus_objects (MlViewService *a_this) ;

/* not used yet
static enum MlViewStatus mlview_service_lookup_dbus_object_instance 
						(const char * a_object_path,
						 MlViewIDBO **a_object) ;
*/
#endif

static void
mlview_service_class_init (MlViewServiceClass *a_klass)
{
	GObjectClass *object_class = NULL ;

	gv_parent_class = (GObjectClass *) g_type_class_peek_parent (a_klass) ;
	object_class = G_OBJECT_CLASS (a_klass) ;
	g_return_if_fail (object_class) ;

	object_class->dispose = mlview_service_dispose ;
	object_class->finalize = mlview_service_finalize ;
}

static void
mlview_service_init (MlViewService *a_this)
{
	PRIVATE (a_this) = (MlViewServicePriv *) g_try_malloc (sizeof (MlViewServicePriv))  ;
	if (!PRIVATE (a_this)) {
		mlview_utils_trace_debug ("Out of memory error") ;
		return ;
	}
	memset (PRIVATE (a_this), 0, sizeof (MlViewServicePriv)) ;
}

static void
mlview_service_dispose (GObject *a_this)
{
	MlViewService *thiz = NULL ;

	g_return_if_fail (thiz && MLVIEW_IS_SERVICE (thiz)) ;

	thiz = MLVIEW_SERVICE (a_this) ;
	g_return_if_fail (thiz && PRIVATE (thiz)) ;

	if (PRIVATE (thiz)->dispose_has_run)
		return ;

#ifdef MLVIEW_WITH_DBUS

	if (PRIVATE (thiz)->dbus_connection) {
		dbus_connection_unref (PRIVATE (thiz)->dbus_connection) ;
		PRIVATE (thiz)->dbus_connection = NULL ;
	}
#endif

	if (PRIVATE (thiz)->name) {
		g_free ((void*)PRIVATE (thiz)->name) ;
		PRIVATE (thiz)->name = NULL ;
	}

	if (gv_parent_class->dispose) {
		gv_parent_class->dispose (a_this) ;
	}
	PRIVATE (thiz)->dispose_has_run  = TRUE ;
}

static void
mlview_service_finalize (GObject *a_this)
{
	MlViewService *thiz = NULL ;

	g_return_if_fail (a_this && MLVIEW_IS_SERVICE (a_this)) ;

	thiz = MLVIEW_SERVICE (a_this) ;

	if (!PRIVATE (thiz))
		return ;

	g_free (PRIVATE (thiz)) ;
	PRIVATE (thiz) = NULL ;
}

static enum MlViewStatus
mlview_service_construct (MlViewService *a_this)
{
	g_return_val_if_fail (a_this && MLVIEW_IS_SERVICE (a_this),
	                      MLVIEW_BAD_PARAM_ERROR) ;

	if (!PRIVATE (a_this)->name) {
		PRIVATE (a_this)->name = g_strdup (ORG_MLVIEW_SERVICE) ;
	}
	return MLVIEW_OK ;
}

static MlViewService *
mlview_service_new (void)
{
	MlViewService *service = NULL ;

	service = (MlViewService *) g_object_new (MLVIEW_TYPE_SERVICE, NULL) ;

	mlview_service_construct (service) ;

	return service ;
}

#ifdef MLVIEW_WITH_DBUS
static enum MlViewStatus
get_bus (MlViewService *a_this,
         DBusConnection **a_connection,
         GError **a_error)
{
	DBusError dbus_error = {0} ;

	g_return_val_if_fail (a_this && MLVIEW_IS_SERVICE (a_this),
	                      MLVIEW_BAD_PARAM_ERROR) ;

	if (!PRIVATE (a_this)->dbus_connection) {

		dbus_error_init (&dbus_error) ;
		PRIVATE (a_this)->dbus_connection= dbus_bus_get
		                                   (DBUS_BUS_SESSION, &dbus_error) ;

		if (!PRIVATE (a_this)->dbus_connection
		        || dbus_error_is_set (&dbus_error)) {

			if (a_error && dbus_error_is_set (&dbus_error)) {
				*a_error = g_error_new
				           (g_quark_from_string ("MLVIEW_BUS_ERROR"),
				            MLVIEW_BUS_ERROR,
				            "%s\n",
				            dbus_error.message) ;
			} else if (a_error) {
				*a_error = g_error_new
				           (g_quark_from_string ("MLVIEW_BUS_ERROR"),
				            MLVIEW_BUS_ERROR,
				            "could not get the session bus\n") ;
			}
			return MLVIEW_BUS_ERROR ;
		}
		dbus_connection_setup_with_g_main
		(PRIVATE (a_this)->dbus_connection, NULL);
	}

	*a_connection = PRIVATE (a_this)->dbus_connection ;
	return MLVIEW_OK ;
}
#endif /*MLVIEW_WITH_DBUS*/

#ifdef MLVIEW_WITH_DBUS
static enum MlViewStatus
try_to_start_dbus_service (MlViewService *a_this, GError **an_error)
{
	enum MlViewStatus status = MLVIEW_OK ;
	DBusConnection *dbus_connection = NULL ;
	DBusError dbus_error = {0} ;
	GError *error = NULL ;
	int service_result = 0 ;
	gchar *service_name = NULL ;
	gchar tab[2] = {0,0} ;

	g_return_val_if_fail (a_this && MLVIEW_IS_SERVICE (a_this),
	                      MLVIEW_BAD_PARAM_ERROR) ;

	status = get_bus (a_this, &dbus_connection, an_error) ;
	if (an_error || status != MLVIEW_OK || !dbus_connection) {
		return MLVIEW_CANNOT_START_SERVICE_ERROR ;
	}

	dbus_error_init (&dbus_error) ;

	tab[0]='a' ;

try_service_name:
	if (service_name) {
		g_free (service_name) ;
		service_name = NULL ;
	}
	service_name = g_strdup_printf ("%s.%s",
	                                PRIVATE (a_this)->name,
	                                tab) ;
	service_result = dbus_bus_acquire_service
	                 (dbus_connection,
	                  service_name, 0, &dbus_error) ;

	switch (service_result) {
	case DBUS_SERVICE_REPLY_PRIMARY_OWNER:
		/*we started the service, OK*/
		mlview_utils_trace_debug
		("Service name: %s registered\n",
		 service_name) ;
		status = register_dbus_objects (a_this) ;
		break ;
	case DBUS_SERVICE_REPLY_ALREADY_OWNER:
		/*we were already started as service. OK still*/
		status = MLVIEW_OK ;
		break ;
	case DBUS_SERVICE_REPLY_SERVICE_EXISTS:
		/*
		 * an other process has been registered under
		 * with the same service name. So we can't be registered
		 * with the same name. shit. KO.
		 */
		status = MLVIEW_SERVICE_ALREADY_EXISTS_ERROR ;
		tab[0]++ ;
		if (tab[0] > 'z') {
			g_error ("Sorry, you can't have more than 36 "
			         "instances of mlview running for the "
			         "moment") ;
		}
		goto try_service_name ;
	case DBUS_SERVICE_REPLY_IN_QUEUE:
		/*
		 * our request is queued. (bus overcrowded ?) KO
		 */
		status = MLVIEW_SERVICE_START_ERROR ;
		break ;
	default:
		g_assert_not_reached () ;
		break ;
	}

	if (error) {
		g_error_free (error) ;
		error = NULL ;
	}
	if (dbus_error_is_set (&dbus_error)) {
		dbus_error_init (&dbus_error) ;
	}
	if (service_name) {
		g_free (service_name) ;
		service_name = NULL ;
	}
	return status ;
}
#endif

#ifdef MLVIEW_WITH_DBUS
static enum MlViewStatus
unregister_dbus_objects (MlViewService *a_this)
{
	enum MlViewStatus status = MLVIEW_OK ;
	glong nb_dbus_objects = sizeof (gv_dbus_objects)
	                        /
	                        sizeof (struct MlViewDBusObjectDesc) ;
	int i = 0 ;

	g_return_val_if_fail (MLVIEW_IS_SERVICE (a_this)
	                      && PRIVATE (a_this)
	                      && PRIVATE (a_this)->dbus_connection,
	                      MLVIEW_BAD_PARAM_ERROR) ;

	for (i=0; i < nb_dbus_objects ;i++) {
		if (!dbus_connection_unregister_object_path
		        (PRIVATE (a_this)->dbus_connection,
		         gv_dbus_objects[i].path)) {

			mlview_utils_trace_debug ("could not unregister a dbus "
			                          "object") ;
			status = MLVIEW_ERROR ;
		}
	}
	return status ;
}
#endif /*MLVIEW_WITH_DBUS*/

#ifdef MLVIEW_WITH_DBUS
static enum MlViewStatus
try_to_stop_dbus_service (MlViewService *a_this,
                          GError **an_error)
{
	enum MlViewStatus status = MLVIEW_OK ;

	status = unregister_dbus_objects (a_this) ;
	return status ;
}
#endif /*MLVIEW_WITH_DBUS*/

#ifdef MLVIEW_WITH_DBUS
static enum MlViewStatus
register_dbus_objects (MlViewService *a_this)
{
	glong nb_dbus_objects = sizeof (gv_dbus_objects)
	                        /
	                        sizeof (struct MlViewDBusObjectDesc) ;
	int i = 0 ;

	g_return_val_if_fail (a_this && MLVIEW_IS_SERVICE (a_this)
	                      && PRIVATE (a_this),
	                      MLVIEW_BAD_PARAM_ERROR) ;

	for (i=0 ; i < nb_dbus_objects; i++) {
		gv_dbus_objects[i].instance = gv_dbus_objects[i].create_instance () ;
		gv_dbus_objects[i].dbus_vtable.unregister_function =
		    mlview_idbo_get_message_unregister_handler
		    (gv_dbus_objects[i].instance) ;
		gv_dbus_objects[i].dbus_vtable.message_function =
		    mlview_idbo_get_message_handler
		    (gv_dbus_objects[i].instance) ;

		/*
		 *  register the dbus object 
		 */
		if (!dbus_connection_register_object_path
		        (PRIVATE (a_this)->dbus_connection,
		         gv_dbus_objects[i].path,
		         &gv_dbus_objects[i].dbus_vtable,
		         gv_dbus_objects[i].instance)) {
			g_error ("object path registration failed") ;
		}
		mlview_utils_trace_debug ("registered object: %s\n",
		                          gv_dbus_objects[i].path) ;
	}
	return MLVIEW_OK ;
}

/*
 NOT USED YET
static enum MlViewStatus 
mlview_service_lookup_dbus_object_instance (const char * a_object_path,
					    MlViewIDBO **a_object)
{
	glong nb_dbus_objects = sizeof (gv_dbus_objects)
		                /
		                sizeof (enum MlViewDBusObjectDesc) ;
	int i = 0 ;
 
	g_return_val_if_fail (a_object_path && a_object,
			      MLVIEW_BAD_PARAM_ERROR) ;
 
	for (i=0 ; i < nb_dbus_objects ; i++) {
		if (gv_dbus_objects[i].path 
		    && !strcmp (gv_dbus_objects[i].path, a_object_path)) {
			*a_object = gv_dbus_objects[i].instance ;
			return MLVIEW_OK ;
		}
	}
	return MLVIEW_OBJECT_NOT_FOUND_ERROR ;
 
}
*/
#endif /*MLVIEW_WITH_DBUS*/

/*****************
 * public methods
 *****************/

GType
mlview_service_get_type (void)
{
	static GType type = 0 ;

	if (!type) {
		static const GTypeInfo type_info = {
		                                       sizeof (MlViewServiceClass),
		                                       NULL, NULL,
		                                       (GClassInitFunc) mlview_service_class_init,
		                                       NULL, NULL,
		                                       sizeof (MlViewService),
		                                       0,
		                                       (GInstanceInitFunc) mlview_service_init
		                                   } ;
		type = g_type_register_static (G_TYPE_OBJECT,
		                               "MlViewService",
		                               &type_info, (GTypeFlags) 0) ;
	}
	return type ;
}

enum MlViewStatus
mlview_service_start (mlview::App *a_app,
                      GError **an_error)
{
	enum MlViewStatus status = MLVIEW_OK ;

	if (!gv_service) {
		/*create the service, start it, and save a pointer to it*/
		gv_service = mlview_service_new () ;
		g_return_val_if_fail (gv_service, MLVIEW_ERROR) ;
	}

#ifdef MLVIEW_WITH_DBUS
	status = try_to_start_dbus_service (gv_service, an_error) ;
#else

	status = MLVIEW_OK ;
#endif /*MLVIEW_WITH_DBUS*/

	return status ;
}

enum MlViewStatus
mlview_service_stop (mlview::App *a_app,
                     GError **an_error)
{
	enum MlViewStatus status = MLVIEW_OK ;

	g_return_val_if_fail (gv_service, MLVIEW_BAD_PARAM_ERROR) ;

#ifdef MLVIEW_WITH_DBUS

	status = try_to_stop_dbus_service (gv_service, an_error) ;
#endif

	return status ;
}

