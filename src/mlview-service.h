/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 8-*- */

/*
 *This file is part of MlView
 *
 *MlView is free software; you can redistribute it and/or 
 *modify it under the terms of 
 *the GNU General Public License as published 
 *by the Free Software Foundation; either version 2
 *or (at your option) any later version.
 *
 *MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView.
 *see the file COPYING. 
 *If not, write to the 
 *Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */
#ifndef __MLVIEW_SERVICE_H__
#define __MLVIEW_SERVICE_H__

#include "mlview-utils.h"
#include "mlview-app-context.h"
#include "mlview-app.h"

G_BEGIN_DECLS

#define MLVIEW_TYPE_SERVICE (mlview_service_get_type ())
#define MLVIEW_SERVICE(widget) (G_TYPE_CHECK_INSTANCE_CAST ((widget), MLVIEW_TYPE_SERVICE, MlViewService))
#define MLVIEW_SERVICE_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), MLVIEW_TYPE_SERVICE, MlViewServiceClass))
#define MLVIEW_IS_SERVICE(widget) (G_TYPE_CHECK_INSTANCE_TYPE ((widget), MLVIEW_TYPE_SERVICE))
#define MLVIEW_IS_SERVICE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), MLVIEW_TYPE_SERVICE))

#define ORG_MLVIEW_SERVICE "org.mlview.Service"

typedef struct _MlViewService MlViewService ;
typedef struct _MlViewServiceClass MlViewServiceClass ;
typedef struct _MlViewServicePriv MlViewServicePriv ;

struct _MlViewService
{
	GObject parent_object ;
	MlViewServicePriv *priv ;
} ;

struct _MlViewServiceClass
{
	GObjectClass parent_class ;
} ;

GType mlview_service_get_type (void) ;

enum MlViewStatus mlview_service_start (mlview::App *a_app,
                                        GError **an_error) ;

enum MlViewStatus mlview_service_stop (mlview::App *a_app,
                                       GError **an_error) ;
G_END_DECLS
#endif /*__MLVIEW_SERVICE_H__*/
