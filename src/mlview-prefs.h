/* -*- Mode: C++; indent-tabs-mode: true; c-basic-offset: 4 -*- */
/*
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute 
 *it and/or modify it under the terms of 
 *the GNU General Public License as published by the 
 *Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYING file for copyright information.
 */
#ifndef MLVIEW_PREFS_H
#define MLVIEW_PREFS_H

#include "mlview-object.h"
#include "mlview-prefs-category.h"

namespace mlview
{
struct PreferencesPriv;

///
/// \brief This class manage application settings.
///
/// Preferences are settings available for the user to configure
/// specific MlView parts. Preferences are divided in categories which
/// handle a functionnaly coherent subset of the preferences.\n
/// To acces preferences, the user need to get a reference to a specific
/// category first. The category then giving access to the desired preferences.
/// \n\n
/// Preferences are implemented as a singleton.
///
/// \see mlview::PrefsCategory
///
class Preferences : public mlview::Object
{
	friend struct PreferencesPriv;
	PreferencesPriv *m_priv;

	Preferences (Preferences const&) ;
	Preferences& operator= (Preferences const&) ;

public:
	virtual ~Preferences ();

	///
	/// Return the singleton' instance
	///
	/// \return a pointer to an mlview::Preferences instance
	///
	static mlview::Preferences* get_instance ();

	///
	/// Return a given category of the preferences based on
	/// its id_
	///
	/// \param category_id The id of the category
	///
	/// \return the associated category or NULL
	///
	PrefsCategory* get_category_by_id
	(const Glib::ustring& category_id);

private:
	Preferences ();
	static mlview::Preferences *m_instance;
};

} // namespace mlview

#endif
