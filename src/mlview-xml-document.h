/* -*- Mode: C++; indent-tabs-mode:nil; c-basic-offset: 4-*- */
/*
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute 
 *it and/or modify it under the terms of 
 *the GNU General Public License as published by the 
 *Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the
 *GNU General Public License along with MlView;
 *see the file COPYING.
 *If not, write to the Free Software Foundation,
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYING file for copyright information.
 */

#ifndef __MLVIEW_XML_DOCUMENT_H__
#define __MLVIEW_XML_DOCUMENT_H__

#include <libxml/tree.h>

#include "mlview-app-context.h"
#include "mlview-file-descriptor.h"
#include "mlview-parsing-utils.h"
#include "mlview-utils.h"
#include "mlview-schema-list.h"

G_BEGIN_DECLS
/**
 *@file
 *The declaration of the #MlViewXMLDocument class.
 *See the definition file.
 */

#define MLVIEW_TYPE_XML_DOCUMENT (mlview_xml_document_get_type ())
#define MLVIEW_XML_DOCUMENT(object) (G_TYPE_CHECK_INSTANCE_CAST ((object), MLVIEW_TYPE_XML_DOCUMENT, MlViewXMLDocument))
#define MLVIEW_XML_DOCUMENT_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), MLVIEW_TYPE_XML_DOCUMENT, MlViewXMLDocumentClass))
#define MLVIEW_IS_XML_DOCUMENT(object) (G_TYPE_CHECK_INSTANCE_TYPE ((object), MLVIEW_TYPE_XML_DOCUMENT))
#define MLVIEW_IS_XML_DOCUMENT_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), MLVIEW_TYPE_XML_DOCUMENT))

/**
 *In the search api, this bitmap specifies 
 *which part of the xml node (content, name, attibute name etc ...) 
 *should be considered for the search. 
 */
enum WhereInTheNodeBitmap {
    NODE_NAME = 1,
    NODE_ATTRIBUTE_NAME = 1 << 1,
    NODE_ATTRIBUTE_VALUE = 1 << 2,
    NODE_CONTENT = 1 << 3,
    NODE_ALL_PARTS = NODE_NAME
                     | NODE_ATTRIBUTE_NAME
                     | NODE_ATTRIBUTE_VALUE | NODE_CONTENT
} ;

struct SearchConfig
{
	enum WhereInTheNodeBitmap where ;
	gboolean ignore_case ;
	gchar *search_string ;
	gboolean downward ;
} ;

typedef struct _MlViewXMLDocument MlViewXMLDocument;
typedef struct _MlViewXMLDocumentClass
			MlViewXMLDocumentClass;
typedef struct _MlViewXMLDocumentPrivate
			MlViewXMLDocumentPrivate;

struct _MlViewXMLDocument
{
	GObject object;
	MlViewXMLDocumentPrivate *priv;
};

struct _MlViewXMLDocumentClass
{
	GObjectClass parent_class;

	/*Tree Edition signals */

	/**
	 *This signal is emited when a structural change appears
	 *on a tree. A structural change is either adding or removing
	 *a node. Changing a node name/attribute/content is not
	 *considered as a structural change, and thus doesn't trigger
	 *the emission of this signal.
	 */
	void (*document_changed) (MlViewXMLDocument *a_xml_doc,
	                          gpointer a_user_data);

	void (*node_cut) (MlViewXMLDocument * a_xml_doc,
	                  xmlNode * a_parent_node,
	                  xmlNode * a_cut_node,
	                  gpointer a_user_data);

	void (*prev_sibling_node_inserted)(MlViewXMLDocument * a_xml_doc,
	                                   xmlNode * a_sibling_node,
	                                   xmlNode * a_inserted_node,
	                                   gpointer a_user_data);

	void (*next_sibling_node_inserted) (MlViewXMLDocument * a_xml_doc,
	                                    xmlNode * a_sibling_node,
	                                    xmlNode * a_inserted_node,
	                                    gpointer a_user_data);

	void (*child_node_added) (MlViewXMLDocument *
	                          a_xml_doc, xmlNode *
	                          a_parent_node,
	                          xmlNode * a_added_node,
	                          gpointer a_user_data);

	void (*content_changed) (MlViewXMLDocument *a_xml_doc,
	                         xmlNode * a_node,
	                         gpointer a_user_data);

	void (*name_changed) (MlViewXMLDocument *a_xml_doc,
	                      xmlNode * a_node,
	                      gpointer a_user_data);

	void (*node_attribute_name_changed) (MlViewXMLDocument *a_xml_doc,
	                                     xmlAttr *a_attr,
	                                     gpointer a_user_data) ;

	void (*node_attribute_value_changed) (MlViewXMLDocument *a_xml_doc,
	                                      xmlAttr *a_attr,
	                                      gpointer a_user_data) ;

	void (*node_attribute_removed) (MlViewXMLDocument *a_xml_doc,
	                                xmlNode *a_node, xmlChar *a_name,
	                                gpointer a_user_data) ;

	void (*node_attribute_added) (MlViewXMLDocument *a_xml_doc,
	                              xmlAttr *a_attr, gpointer a_user_data) ;

	void (*node_namespace_added) (MlViewXMLDocument *a_this,
	                              xmlNode *a_node,
	                              xmlNs *a_ns,
	                              gpointer a_user_data) ;

	void (*node_namespace_changed) (MlViewXMLDocument *a_this,
	                                xmlNode *a_node,
	                                xmlNs *a_ns,
	                                gpointer a_user_data) ;

	void (*node_namespace_removed) (MlViewXMLDocument *a_this,
	                                xmlNode *a_node,
	                                xmlNs *a_ns,
	                                gpointer a_user_data) ;

	void (*replace_node) (MlViewXMLDocument *a_this,
	                      xmlNode *old_node,
	                      xmlNode *new_node,
	                      gpointer a_user_data) ;

	void (*node_commented) (MlViewXMLDocument *a_this,
	                        xmlNode *a_node,
	                        xmlNode *a_new_node,
	                        gpointer a_user_data) ;

	void (*node_uncommented) (MlViewXMLDocument *a_this,
	                          xmlNode *a_node,
	                          xmlNode *a_new_node,
	                          gpointer a_user_data) ;

	void (*node_changed) (MlViewXMLDocument *a_xml_doc,
	                      xmlNode * a_node,
	                      gpointer a_user_data);

	void (*file_path_changed) (MlViewXMLDocument *a_xml_doc,
	                           gpointer a_user_data);

	void (*searched_node_found) (MlViewXMLDocument *a_xml_doc,
	                             xmlNode *a_node_found,
	                             gpointer a_user_data) ;

	void (*node_selected) (MlViewXMLDocument *a_xml_doc,
	                       xmlNode *a_node_found,
	                       gpointer a_user_data) ;

	void (*node_unselected) (MlViewXMLDocument *a_xml_doc,
	                         xmlNode *a_node,
	                         gpointer a_user_data) ;

	void (*dtd_node_system_id_changed) (MlViewXMLDocument *a_this,
	                                    xmlDtd *a_dtd_node,
	                                    gpointer a_user_data) ;

	void (*dtd_node_public_id_changed) (MlViewXMLDocument *a_this,
	                                    xmlDtd *a_dtd_node,
	                                    gpointer a_user_data) ;

	void (*dtd_node_created) (MlViewXMLDocument *a_this,
	                          xmlDtd *a_dtd_node,
	                          gpointer a_user_data) ;

	void (*entity_node_content_changed) (MlViewXMLDocument *a_this,
	                                     xmlEntity *a_entity_node,
	                                     gpointer a_user_data) ;

	void (*entity_node_public_id_changed) (MlViewXMLDocument *a_this,
	                                       xmlEntity *a_entity_node,
	                                       gpointer a_user_data) ;

	void (*entity_node_system_id_changed) (MlViewXMLDocument *a_this,
	                                       xmlEntity *a_entity_node,
	                                       gpointer *a_user_data) ;

	void (*ext_subset_changed) (MlViewXMLDocument *a_this,
	                            gpointer a_user_data);

	void (*document_closed) (MlViewXMLDocument *a_this,
	                         gpointer a_user_data);

	void (*document_reloaded) (MlViewXMLDocument *a_this,
	                           gpointer a_user_data);

	void (*going_to_save) (MlViewXMLDocument *a_this,
	                       gpointer a_user_data) ;

	void (*document_undo_state_changed) (MlViewXMLDocument *a_this,
	                                     gpointer a_user_data) ;
};

guint mlview_xml_document_get_type (void);

MlViewXMLDocument *mlview_xml_document_new (xmlDocPtr a_xml_doc);

void mlview_xml_document_ref (MlViewXMLDocument * a_this);

void mlview_xml_document_unref (MlViewXMLDocument * a_this);

MlViewXMLDocument *mlview_xml_document_open_with_dtd_interactive
										(const gchar * a_file_name);

xmlNode * mlview_xml_document_get_node_from_clipboard2 (xmlDoc * a_xml_doc) ;

MlViewXMLDocument * mlview_xml_document_clone (MlViewXMLDocument *a_original);

MlViewFileDescriptor * mlview_xml_document_get_file_descriptor
								(MlViewXMLDocument * a_xml_doc);

void mlview_xml_document_set_file_descriptor
		(MlViewXMLDocument * a_xml_doc, MlViewFileDescriptor * a_file_desc);

void mlview_xml_document_set_file_path (MlViewXMLDocument * a_xml_doc,
                                        const gchar * a_file_path);

gchar *mlview_xml_document_get_file_path (MlViewXMLDocument * a_xml_doc);

xmlDocPtr mlview_xml_document_get_native_document
									(const MlViewXMLDocument * a_xml_doc);

void mlview_xml_document_set_app_context (MlViewXMLDocument * a_doc);

void mlview_xml_document_validate (MlViewXMLDocument * a_doc);

gboolean mlview_xml_document_needs_saving (MlViewXMLDocument * a_doc);

gint mlview_xml_document_save (MlViewXMLDocument *a_doc,
                               const gchar * a_file_path,
                               gboolean a_check_overwrt);


gint mlview_xml_document_save_xml_doc (MlViewXMLDocument * a_xml_doc,
                                       const gchar * a_file_path)  ;

void mlview_xml_document_save_xml_doc2 (MlViewXMLDocument *a_xml_doc,
                                        gchar **a_buffer,
                                        gint *a_buffer_len) ;

enum MlViewStatus mlview_xml_document_reload_from_buffer
	(MlViewXMLDocument *a_this, const gchar *a_buffer, gboolean a_emit_signal) ;

/**************************************
 *Document Edition interfaces.
 ***************************************/

void mlview_xml_document_select_node (MlViewXMLDocument *a_this,
                                      xmlNode *a_node) ;

void mlview_xml_document_copy_node_to_clipboard2 (xmlNode * a_xml_node,
        xmlDoc * a_doc) ;

enum MlViewStatus mlview_xml_document_add_child_node
		(MlViewXMLDocument * a_xml_doc,
		 const gchar * a_parent_xml_node_path,
		 xmlNode * a_xml_node, gboolean a_subtree_required,
		 gboolean a_emit_signal);

enum MlViewStatus mlview_xml_document_cut_node
			(MlViewXMLDocument * a_xml_doc,
			 const gchar * a_xml_node_path,
			 gboolean a_emit_signal) ;

enum MlViewStatus mlview_xml_document_insert_prev_sibling_node
				(MlViewXMLDocument * a_xml_document,
				 const gchar* a_sibling_node_path,
				 xmlNode * a_xml_node,
				 gboolean a_subtree_required,
				 gboolean a_emit_signal);

enum MlViewStatus mlview_xml_document_insert_next_sibling_node
			(MlViewXMLDocument * a_xml_document,
			 const gchar * a_sibling_node,
			 xmlNode * a_xml_node,
			 gboolean a_subtree_required,
			 gboolean a_emit_signal);

void mlview_xml_document_paste_node_as_child
		(MlViewXMLDocument * a_xml_document,
         const gchar * a_parent_node_path,
         gboolean a_emit_signal);

void mlview_xml_document_paste_node_as_sibling
					(MlViewXMLDocument * a_xml_document,
					 const gchar * a_parent_node_path,
					 const gchar * a_sibling_node_path,
					 gboolean a_previous,
					 gboolean a_emit_signal);


enum MlViewStatus mlview_xml_document_set_node_content
							(MlViewXMLDocument * a_xml_document,
							 const gchar * a_node_path,
							 gchar * a_content,
							 gboolean a_emit_signal) ;

enum MlViewStatus mlview_xml_document_node_get_content
						(xmlNode * a_node,
						 enum MlViewEncoding a_enc,
						 gchar ** a_outbuf);

enum MlViewStatus mlview_xml_document_set_node_name
						(MlViewXMLDocument * a_xml_document,
						 const gchar * a_node_path,
						 gchar * a_name,
						 gboolean a_emit_signal);

enum MlViewStatus mlview_xml_document_set_node_name_without_xpath
					(MlViewXMLDocument *a_this,
					 xmlNode *a_node,
					 gchar *a_name,
					 gboolean a_emit_signal) ;

enum MlViewStatus mlview_xml_document_set_entity_node_name
		(MlViewXMLDocument *a_this,
         xmlEntity *a_entity,
         xmlDtd *a_dtd_node,
         gchar *a_name,
         gboolean a_emit_signal) ;

enum MlViewStatus mlview_xml_document_node_get_name (xmlNode * a_node,
													 enum MlViewEncoding a_enc,
													 gchar ** a_outbuf);

const gchar * mlview_xml_document_get_mime_type (MlViewXMLDocument *a_this) ;

enum MlViewStatus mlview_xml_document_set_attribute
						(MlViewXMLDocument *a_this,
						 const gchar *a_node_path,
						 const xmlChar* a_name,
						 const xmlChar *a_value,
						 gboolean a_emit_signal) ;

enum MlViewStatus mlview_xml_document_set_attribute_name (MlViewXMLDocument *a_this,
        xmlAttr *a_attr,
        const xmlChar *a_name,
        gboolean a_emit_signal) ;

enum MlViewStatus mlview_xml_document_set_attribute_value (MlViewXMLDocument *a_this,
        xmlAttr *a_attr,
        const xmlChar *a_value,
        gboolean a_emit_signal) ;

enum MlViewStatus mlview_xml_document_remove_attribute (MlViewXMLDocument *a_this,
        xmlNode *a_node,
        const xmlChar *a_name,
        gboolean a_emit_signal) ;

enum MlViewStatus  mlview_xml_document_synch_attributes (MlViewXMLDocument *a_doc,
        const gchar *a_node_path,
        GList *a_nv_pair_list) ;

enum MlViewStatus mlview_xml_document_create_internal_subset (MlViewXMLDocument *a_this,
        const xmlChar * a_name,
        const xmlChar *public_id,
        const xmlChar *system_id,
        gboolean a_emit_signal) ;

xmlNs * mlview_xml_document_create_ns (MlViewXMLDocument *a_this,
                                       xmlNode *a_node, gchar *uri,
                                       gchar *prefix,
                                       gboolean a_emit_signal) ;

enum MlViewStatus mlview_xml_document_set_ns (MlViewXMLDocument *a_this,
        xmlNode *a_node,
        xmlNs *a_ns,
        xmlChar *a_uri,
        xmlChar *a_prefix,
        gboolean a_emit_signal) ;

enum MlViewStatus mlview_xml_document_remove_ns (MlViewXMLDocument *a_this,
        xmlNs *a_ns,
        xmlNode *a_node,
        gboolean a_emit_signal) ;

enum MlViewStatus mlview_xml_document_set_dtd_node_system_id (MlViewXMLDocument *a_this,
        xmlDtd *a_dtd,
        xmlChar *a_system_id,
        gboolean a_emit_signal) ;

enum MlViewStatus mlview_xml_document_set_dtd_node_public_id (MlViewXMLDocument *a_this,
        xmlDtd *a_dtd,
        xmlChar *a_public_id,
        gboolean a_emit_signal) ;

enum MlViewStatus mlview_xml_document_set_entity_content (MlViewXMLDocument *a_this,
        xmlEntity *a_entity,
        xmlChar *a_content,
        gboolean a_emit_signal) ;

enum MlViewStatus mlview_xml_document_set_entity_public_id (MlViewXMLDocument *a_this,
        xmlEntity *a_entity,
        xmlChar *a_external_id,
        gboolean a_emit_signal) ;

enum MlViewStatus mlview_xml_document_set_entity_system_id (MlViewXMLDocument *a_this,
        xmlEntity *a_entity,
        xmlChar *a_system_id,
        gboolean a_emit_signal) ;

enum MlViewStatus mlview_xml_document_node_get_fqn_len_as_isolat1 (xmlNode * a_node, gint * a_len);

enum MlViewStatus mlview_xml_document_node_get_fqn (xmlNode * a_node,
        enum MlViewEncoding a_enc,
        gchar ** a_outbuf);

enum MlViewStatus mlview_xml_document_search (MlViewXMLDocument *a_this,
        const struct SearchConfig *a_conf,
        xmlNode *a_from,
        xmlNode **a_found,
        gboolean emit_signal) ;

enum MlViewStatus
mlview_xml_document_search2 (MlViewXMLDocument *a_this,
                             const struct SearchConfig *a_conf,
                             xmlNode *a_from,
                             xmlNode **a_found,
                             gboolean a_emit_signal) ;

enum MlViewStatus mlview_xml_document_search3 (MlViewXMLDocument *a_this,
        const struct SearchConfig *a_conf,
        xmlNode *a_from,
        xmlNode **a_found,
        gboolean a_emit_signal);

gboolean mlview_xml_document_is_completion_possible_global (MlViewXMLDocument *a_this);

gboolean mlview_xml_document_is_node_valid (MlViewXMLDocument *a_this,
        xmlNode *a_node);

const gchar *mlview_xml_document_get_ext_subset_name (MlViewXMLDocument *a_doc);

MlViewSchemaList *mlview_xml_document_get_schema_list (MlViewXMLDocument *a_doc);

gboolean mlview_xml_document_set_ext_subset_with_url (MlViewXMLDocument *a_doc,
        const gchar *a_url);

MlViewXMLDocument *mlview_xml_document_open_with_dtd (const gchar * a_file_name,
        const gchar * a_dtd_name);


enum MlViewStatus mlview_xml_document_replace_node (MlViewXMLDocument *a_this,
        gchar *a_node_path,
        xmlNode *a_replacement,
        gboolean a_emit_signal) ;

enum MlViewStatus mlview_xml_document_comment_node (MlViewXMLDocument *a_this,
        const gchar *a_node_path,
        gboolean a_emit_signal) ;

enum MlViewStatus mlview_xml_document_uncomment_node (MlViewXMLDocument *a_this,
        const gchar *a_node,
        gboolean a_emit_signal) ;

gchar *mlview_xml_document_get_uri (MlViewXMLDocument *a_this);

gboolean mlview_xml_document_can_undo_mutation (MlViewXMLDocument *a_this) ;

gboolean mlview_xml_document_can_redo_mutation (MlViewXMLDocument *a_this) ;

enum MlViewStatus mlview_xml_document_undo_mutation (MlViewXMLDocument *a_this,
        gpointer a_user_data) ;

enum MlViewStatus mlview_xml_document_redo_mutation (MlViewXMLDocument *a_this,
        gpointer a_user_data) ;

xmlNode * mlview_xml_document_get_node_from_xpath (MlViewXMLDocument *a_this,
        const gchar *an_xpath_expr) ;

enum MlViewStatus mlview_xml_document_get_node_path (MlViewXMLDocument *a_this,
        xmlNode *a_node,
        gchar **a_path) ;

enum MlViewStatus mlview_xml_document_get_root_element (MlViewXMLDocument *a_this,
        xmlNode **a_root_element) ;

gboolean mlview_xml_document_is_standalone (MlViewXMLDocument *a_this);

void mlview_xml_document_set_standalone (MlViewXMLDocument *a_this, gboolean standalone);

enum MlViewStatus mlview_xml_document_notify_undo_state_changed
(MlViewXMLDocument *a_this) ;
G_END_DECLS
#endif
