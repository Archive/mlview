/* -*- Mode: C++; indent-tabs-mode:true; c-basic-offset: 4-*- */

/*
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute 
 *it and/or modify it under the terms of 
 *the GNU General Public License as published by the 
 *Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */

#include <unistd.h>
#include <stdlib.h>
#include <gnome.h>
#include <gtkmm.h>
#include <vte/vte.h>
#include <glade/glade.h>
#include <libglademm.h>
#include "mlview-exec-command-dialog.h"

namespace mlview
{

struct ExecCommandDialogPriv
{
	UString m_filename;
	Glib::RefPtr <Gnome::Glade::Xml> m_refxml;
	Gtk::Dialog *m_dialog;
	Gtk::Button *m_exec_button;
	Gtk::Button *m_close_button;
	GnomeEntry *m_command_entry;
	Gtk::Expander *m_output_expander;
	Gtk::VBox *m_output_vbox;
	GtkWidget *m_output_terminal;

	// Constructor
	/*
	  ExecCommandDialogPriv (const char *filename) :
	  m_dialog (NULL),
	  m_exec_button (NULL),
	  m_close_button (NULL),
	  m_cancel_button (NULL),
	  m_command_entry (NULL),
	  m_output_expander (NULL),
	  m_output_vbox (NULL),
	  m_filename (0)
	  {}
	*/
	// Methods
	void set_filename (const gchar *filename);
	void setup_ui ();
	void setup_event_handlers ();
	void execute ();

	// Callbacks
	void on_exec_button_clicked ();
};


ExecCommandDialog::ExecCommandDialog (const gchar *filename)
{
	m_priv = new ExecCommandDialogPriv ();
	m_priv->set_filename (filename);
	m_priv->setup_ui ();
	m_priv->setup_event_handlers ();

	m_priv->m_dialog->run ();
}

ExecCommandDialog::~ExecCommandDialog ()
{
	if (m_priv != NULL) {
		m_priv->m_dialog->hide ();

		delete m_priv;
		m_priv = NULL;
	}
}

void
ExecCommandDialogPriv::set_filename (const gchar *filename)
{
	m_filename = UString(Glib::ustring (filename));
}

void
ExecCommandDialogPriv::setup_ui ()
{
	gchar *glade_file =
	    gnome_program_locate_file (NULL,
	                               GNOME_FILE_DOMAIN_APP_DATADIR,
	                               PACKAGE "/mlview-exec-command.glade",
	                               TRUE,
	                               NULL) ;
	m_refxml = Gnome::Glade::Xml::create (glade_file,
	                                      "ExecCommandDialog");

	m_refxml->get_widget ("ExecCommandDialog",
	                      m_dialog);
	m_refxml->get_widget ("exec_button",
	                      m_exec_button);
	m_refxml->get_widget ("close_button",
	                      m_close_button);

	m_command_entry = GNOME_ENTRY (glade_xml_get_widget (m_refxml->gobj (),
	                               "command_entry"));

	m_refxml->get_widget ("output_expander",
	                      m_output_expander);

	m_refxml->get_widget ("output_box",
	                      m_output_vbox);

	m_output_terminal = vte_terminal_new ();
	gtk_widget_show (m_output_terminal);
	gtk_box_pack_start_defaults (GTK_BOX (m_output_vbox->gobj ()),
	                             m_output_terminal);
}

void
ExecCommandDialogPriv::setup_event_handlers ()
{
	m_exec_button->signal_clicked ().connect (
	    sigc::mem_fun (*this,
	                   &ExecCommandDialogPriv::on_exec_button_clicked));
}

void
ExecCommandDialogPriv::on_exec_button_clicked ()
{
	// Reset terminal
	vte_terminal_reset ((VteTerminal*)m_output_terminal,
	                    TRUE,  // full reset
	                    TRUE); // reset scrollback history

	// Get command text from entry
	const gchar *command_pattern =
	gtk_entry_get_text
	(GTK_ENTRY (gnome_entry_gtk_entry (GNOME_ENTRY (m_command_entry))));

	if (command_pattern == NULL)
		return;

	// Replace %s with filename
	gchar *command = g_strdup_printf (command_pattern,
	                                  m_filename.c_str ());

	// Parse command
	gchar **argv = NULL;
	int   argc;

	gboolean res = g_shell_parse_argv (command,
	                                   &argc,
	                                   &argv,
	                                   NULL);

	if (!res)
		return;

	// Execute command in terminal window
	vte_terminal_fork_command ((VteTerminal*)m_output_terminal,
	                           (char*)argv[0],
	                           (char**)argv,
	                           (char**)environ,
	                           (char*)getenv("HOME"),
	                           TRUE,
	                           FALSE,
	                           FALSE);

	free (command);
}

} // namespace mlview
