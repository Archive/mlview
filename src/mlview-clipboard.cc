/* -*- Mode: C++; indent-tabs-mode:true; c-basic-offset: 4-*- */
/*
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute 
 *it and/or modify it under the terms of 
 *the GNU General Public License as published by the 
 *Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */

#include "mlview-clipboard.h"

namespace mlview
{

typedef map<UString, UString> StringToStringMap ;
struct ClipboardPriv
{
	//a map with keys are
	//buffer names, and
	//the values are buffer contents.
	StringToStringMap buffers ;

	ClipboardPriv () {}
	ClipboardPriv& operator= (ClipboardPriv const& an_other)
	{
		buffers = an_other.buffers ;
		return *this ;
	}
}
;//end struct ClipboardPriv

Clipboard::Clipboard ()
{
	m_priv = new ClipboardPriv () ;
}


Clipboard::Clipboard (Clipboard const &a_clipboard) : Object (a_clipboard)
{
	m_priv = new ClipboardPriv () ;
	m_priv->buffers = a_clipboard.m_priv->buffers ;
}

Clipboard&
Clipboard::operator= (Clipboard const &a_clipboard)
{
	if (this == &a_clipboard)
		return *this ;
	THROW_IF_FAIL (m_priv) ;

	//call the mother class' assignment operator
	Object::operator= (a_clipboard) ;
	m_priv->buffers = a_clipboard.m_priv->buffers ;
	return *this ;
}

Clipboard::~Clipboard ()
{
	if (m_priv) {
		delete (m_priv) ;
		m_priv = NULL ;
	}
}

enum MlViewStatus
Clipboard::put (const UString &a_content,
			    const UString &a_buffer_name)
{
	THROW_IF_FAIL (m_priv) ;

	if (a_buffer_name == DEFAULT_CLIPBOARD_BUFFER_NAME) {
		put_text_in_default_native_clipboard (a_content) ;
	} else {
		pair<StringToStringMap::iterator, bool> insertion_result;
		insertion_result = m_priv->buffers.insert
		(StringToStringMap::value_type (a_buffer_name, a_content)) ;

		if (!insertion_result.second) {
			m_priv->buffers.erase (a_buffer_name) ;
			insertion_result = m_priv->buffers.insert
			(StringToStringMap::value_type (a_buffer_name, a_content));
			if (!insertion_result.second)
				return MLVIEW_ERROR ;
		}
	}
	return MLVIEW_OK ;
}

enum MlViewStatus
Clipboard::put (const UString &a_content)
{
	return put (a_content, DEFAULT_CLIPBOARD_BUFFER_NAME) ;
}

enum MlViewStatus
Clipboard::put (const xmlNode *a_node,
		        const UString &a_buffer_name)
{
	THROW_IF_FAIL (m_priv) ;
	THROW_IF_FAIL (a_node) ;
	gchar *buf = NULL ;
	enum MlViewStatus status = MLVIEW_OK ;

	status = mlview_parsing_utils_serialize_node_to_buf (a_node, &buf) ;
	if (status != MLVIEW_OK || !buf) {
		LOG_TO_ERROR_STREAM ("node serialization failed") ;
		return MLVIEW_ERROR ;
	}
	UString serialized_node = buf ;
	g_free (buf) ; buf = NULL ;
	return put (serialized_node, a_buffer_name) ;
}

enum MlViewStatus
Clipboard::put (const xmlNode *a_xml_node)
{
	return put (a_xml_node, DEFAULT_CLIPBOARD_BUFFER_NAME) ;
}

xmlNode*
Clipboard::get (const xmlDoc *a_doc)
{
	THROW_IF_FAIL (a_doc) ;

	return get (a_doc, DEFAULT_CLIPBOARD_BUFFER_NAME) ;
}

xmlNode*
Clipboard::get (const MlViewXMLDocument *a_doc)
{
	THROW_IF_FAIL (a_doc) ;
	return get (a_doc, DEFAULT_CLIPBOARD_BUFFER_NAME) ;
}

xmlNode*
Clipboard::get (const xmlDoc *a_doc, const UString &a_buf_name)
{
	THROW_IF_FAIL (a_doc) ;

	xmlNode *node = NULL ;
	UString serialized_fragment = get (a_buf_name) ;
	if (serialized_fragment != "") {
		mlview_parsing_utils_parse_fragment
			(a_doc, (xmlChar*)serialized_fragment.c_str (), &node) ;
	}
	return node ;
}

xmlNode*
Clipboard::get (const MlViewXMLDocument*a_doc, const UString &a_buf_name)
{
	THROW_IF_FAIL (a_doc) ;

	xmlDoc *doc = mlview_xml_document_get_native_document (a_doc) ;
	THROW_IF_FAIL (doc) ;
	return get (doc, a_buf_name) ;
}

UString
Clipboard::get (const UString &a_buffer_name)
{
	UString result ("") ;
	if (a_buffer_name == DEFAULT_CLIPBOARD_BUFFER_NAME) {
		result = get_text_from_default_native_clipboard () ;
	} else {
		StringToStringMap::iterator it ;
		it = m_priv->buffers.find (a_buffer_name);
		if (it == m_priv->buffers.end ())
			result = "" ;
		result = it->second ;
	}
	return result ;
}

UString
Clipboard::get (void)
{
	return get (DEFAULT_CLIPBOARD_BUFFER_NAME) ;
}

list<UString>
Clipboard::get_list_of_buffer_names ()
{
	THROW_IF_FAIL (m_priv) ;
	list<UString> result ;

	StringToStringMap::iterator it ;
	for (it = m_priv->buffers.begin ();
		 it != m_priv->buffers.end ();
		 ++it) {
		result.push_back (it->first) ;
	}
	result.push_back (DEFAULT_CLIPBOARD_BUFFER_NAME) ;
	return result ;
}

bool
Clipboard::has_buffer (const UString &a_buffer_name)
{
	THROW_IF_FAIL (m_priv) ;

	if (a_buffer_name == DEFAULT_CLIPBOARD_BUFFER_NAME)
		return true ;

	StringToStringMap::iterator it ;

	it = m_priv->buffers.find (a_buffer_name) ;
	if (it == m_priv->buffers.end ())
		return false ;
	return true ;

}

//********************
//protected methods
//*********************

void
Clipboard::put_text_in_default_native_clipboard (const UString &a_text)
{
	THROW_IF_FAIL (m_priv) ;

	GdkDisplay * default_display = gdk_display_get_default () ;
	THROW_IF_FAIL (default_display) ;
	GtkClipboard* native_clipboard = gtk_clipboard_get_for_display
		(default_display, GDK_SELECTION_CLIPBOARD) ;
	THROW_IF_FAIL (native_clipboard) ;
	gtk_clipboard_set_text (native_clipboard, a_text.c_str (), a_text.size ()) ;
}

UString
Clipboard::get_text_from_default_native_clipboard (void)
{
	THROW_IF_FAIL (m_priv) ;

	GdkDisplay *default_display = gdk_display_get_default () ;
	THROW_IF_FAIL (default_display) ;
	GtkClipboard* native_clipboard = gtk_clipboard_get_for_display
				(default_display, GDK_SELECTION_CLIPBOARD) ;
	THROW_IF_FAIL (native_clipboard) ;
	gchar *text = gtk_clipboard_wait_for_text (native_clipboard) ;

	UString result (text) ;
	if (text) {
		g_free (text) ;
		text = NULL ;
	}
	return result ;
}

}//namespace mlview
