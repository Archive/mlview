/* -*- Mode: C; indent-tabs-mode:nil; c-basic-offset: 8-*- */
/*
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute 
 *it and/or modify it under the terms of 
 *the GNU General Public License as published by the 
 *Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */

#include "mlview-view-adapter.h"
#include "mlview-tree-editor.h"

#ifndef __MLVIEW_STYLED_VIEW_H__
#define __MLVIEW_STYLED_VIEW_H__

#ifdef MLVIEW_WITH_STYLE
#include <sewfox/sx-box-view.h>

G_BEGIN_DECLS
#define MLVIEW_TYPE_STYLED_VIEW (mlview_styled_view_get_type ())
#define MLVIEW_STYLED_VIEW(widget) (G_TYPE_CHECK_INSTANCE_CAST ((widget), MLVIEW_TYPE_STYLED_VIEW, MlViewStyledView))
#define MLVIEW_STYLED_VIEW_CLASS(widget) (G_TYPE_CHECK_CLASS_CAST ((widget), MLVIEW_TYPE_STYLED_VIEW, MlViewStyledViewClass))
#define MLVIEW_IS_STYLED_VIEW(widget) (G_TYPE_CHECK_INSTANCE_TYPE ((widget),MLVIEW_TYPE_STYLED_VIEW))
#define MLVIEW_IS_STYLED_VIEW_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), MLVIEW_TYPE_STYLED_VIEW))

typedef struct _MlViewStyledView MlViewStyledView ;
typedef struct _MlViewStyledViewClass MlViewStyledViewClass ;
typedef struct _MlViewStyledViewPriv MlViewStyledViewPriv ;

struct _MlViewStyledView
{
	MlViewViewAdapter view ;
	MlViewStyledViewPriv *priv ;
} ;

struct _MlViewStyledViewClass
{
	MlViewViewAdapterClass parent_class ;
	/*signal default handlers*/
} ;

GType mlview_styled_view_get_type (void) ;
GtkWidget *mlview_styled_view_new (MlViewXMLDocument *a_doc,
                                   const gchar *a_name,
                                   MlViewAppContext *a_app_ctxt) ;

MlViewTreeEditor *mlview_styled_view_get_tree_editor (MlViewStyledView *a_this) ;

enum MlViewStatus mlview_styled_view_construct (MlViewStyledView *a_this,
        MlViewXMLDocument *a_doc,
        const gchar *a_view_name,
        MlViewAppContext *a_app_context) ;

enum MlViewStatus mlview_styled_view_create_internal_subset_node_interactive (MlViewStyledView *a_this) ;

enum MlViewStatus mlview_styled_view_execute_action (MlViewIView *a_this,
        MlViewAction *a_action) ;

enum MlViewStatus mlview_styled_view_connect_to_doc (MlViewIView *a_this,
        MlViewXMLDocument *a_doc) ;

enum MlViewStatus mlview_styled_view_disconnect_from_doc (MlViewIView *a_this,
        MlViewXMLDocument *a_doc) ;

enum MlViewStatus mlview_styled_view_update_from_css_paths (MlViewStyledView *a_this,
        gchar *a_author_css_path,
        gchar *a_user_css_path,
        gchar *a_ua_css_path,
        enum CREncoding a_enc) ;

enum MlViewStatus mlview_styled_view_updatde_from_cascade (MlViewStyledView *a_this,
        CRCascade *a_cascade) ;

enum MlViewStatus mlview_styled_view_update_from_css_paths_interactive (MlViewStyledView *a_this) ;


enum MlViewStatus mlview_styled_view_set_app_context (MlViewStyledView *a_this,
        MlViewAppContext *a_app_context) ;

MlViewAppContext * mlview_styled_view_get_app_context (MlViewStyledView *a_this) ;

enum MlViewStatus mlview_styled_view_add_child_node_interactive (MlViewStyledView *a_this) ;

enum MlViewStatus mlview_styled_view_insert_prev_sibling_node_interactive (MlViewStyledView *a_this) ;

enum MlViewStatus mlview_styled_view_insert_next_sibling_node_interactive (MlViewStyledView * a_this)  ;

enum MlViewStatus mlview_styled_view_cut_node (MlViewStyledView *a_this) ;

enum MlViewStatus mlview_styled_view_copy_node (MlViewStyledView *a_this) ;

enum MlViewStatus mlview_styled_view_paste_node_as_child (MlViewStyledView * a_this)  ;

enum MlViewStatus mlview_styled_view_paste_node_as_prev_sibling (MlViewStyledView * a_this) ;

enum MlViewStatus mlview_styled_view_paste_node_as_next_sibling (MlViewStyledView * a_this) ;

G_END_DECLS

#endif /*MLVIEW_WITH_STYLE*/
#endif /*__MLVIEW_STYLED_VIEW_H__*/

