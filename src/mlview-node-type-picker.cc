/* -*- Mode: C++; indent-tabs-mode:nil; c-basic-offset: 4-*- */
/*
 *This file is part of MlView.
 *
 *MlView is free software; you can redistribute 
 *it and/or modify it under the terms of 
 *the GNU General Public License as published by the 
 *Free Software Foundation; either version 2, 
 *or (at your option) any later version.
 *
 *GNU MlView is distributed in the hope that it will 
 *be useful, but WITHOUT ANY WARRANTY; 
 *without even the implied warranty of 
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *See the GNU General Public License for more details.
 *
 *You should have received a copy of the 
 *GNU General Public License along with MlView; 
 *see the file COPYING. 
 *If not, write to the Free Software Foundation, 
 *Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *See COPYRIGHT file for copyright information.
 */

#include <string.h>
#include <libxml/parser.h>
#include <libxml/valid.h>
#include "mlview-utils.h"
#include "mlview-node-type-picker.h"
#include "mlview-prefs.h"
#include "mlview-prefs-category-general.h"

/**
 *@file
 *The definition of the #MlViewNodeTypePicker class.
 */

/*a pointer to the parent class structure of MlViewNodeTypePickerClass*/
static GtkDialogClass *gv_parent_class = NULL;
static gboolean gv_on_going_validation = TRUE;

/*the data structure of the private part of MlViewNodeType*/
struct _MlViewNodeTypePickerPrivate
{
	NodeTypeDefinition *selected_node_type_def;

	/*a combo box that lets the user choose a type of node */
	GtkCombo *node_types_combo;

	/*
	 *If the user chooses an element node, 
	 *this entry contains the name. 
	 *If it chooses a content node, contains the content etc ...
	 */
	GtkCombo *node_name_or_content;

	/*
	 *The label of the entry above. 
	 *Will be dynamically changed depending on 
	 *what "node_name_or_content" contains
	 */
	GtkLabel *node_name_or_content_label;

	/*
	 *the element names completion list.
	 *Whenever validation is on, this 
	 *list should be built 
	 *(according to the document validation constraints)
	 *each time the user has to enter an element name.
	 */
	GList *element_names_choice_list;

	/*The node types presented to the user */
	GList *node_type_names;
	gboolean dispose_has_run ;
};


#define PRIVATE(node_type_picker) (node_type_picker->priv)
static const int NO_SELECTED_NODE_TYPE = -1;

/*=============================================
 *This is the list of all the types of xml node 
 *that the editor should ne able to handle
 *The types come from the file libxml/tree.h of 
 *Daniel Veillard's libxml2
 *=============================================*/
static NodeTypeDefinition gv_xml_node_types[] = {
            {"ELEMENT NODE", XML_ELEMENT_NODE, (xmlEntityType)0}, /*0 */
            {"TEXT NODE", XML_TEXT_NODE, (xmlEntityType)0}, /*1 */
            {"PI NODE", XML_PI_NODE, (xmlEntityType)0}, /*2 */
            {"COMMENT NODE", XML_COMMENT_NODE, (xmlEntityType)0}, /*3 */
            {"CDATA SECTION NODE", XML_CDATA_SECTION_NODE, (xmlEntityType)0},
            {"XML INTERNAL GENERAL ENTITY DECL",XML_ENTITY_DECL,
             XML_INTERNAL_GENERAL_ENTITY},
            {"XML EXTERNAL GENERAL PARSED ENTITY DECL",XML_ENTITY_DECL,
             XML_EXTERNAL_GENERAL_PARSED_ENTITY},
            {"XML EXTERNAL GENERAL UNPARSED ENTITY DECL",XML_ENTITY_DECL,
             XML_EXTERNAL_GENERAL_UNPARSED_ENTITY},
            {"XML INTERNAL PARAMETER ENTITY DECL",XML_ENTITY_DECL,
             XML_INTERNAL_PARAMETER_ENTITY},
            {"XML EXTERNAL PARAMETER ENTITY DECL",XML_ENTITY_DECL,
             XML_EXTERNAL_PARAMETER_ENTITY},

            /*the line below must be the last one... */
            {NULL}
        };
/*the offset of the element types defined in gv_xml_node_types*/
enum MlViewElementDescOffsets {
    ELEMENT_NODE = 0,
    TEXT_NODE,
    PI_NODE,
    COMMENT_NODE,
    CDATA_SECTION_NODE,
    INTERNAL_GENERAL_ENTITY_NODE,
    EXTERNAL_GENERAL_PARSED_ENTITY_NODE,
    EXTERNAL_GENERAL_UNPARSED_ENTITY_NODE,
    INTERNAL_PARAMETER_ENTITY_NODE,
    EXTERNAL_PARAMETER_ENTITY_NODE
} ;

/*
 *An index to find a node type def by the node type name. 
 *This index if followed by its ref count.
 *When the ref count falls to zero, the index is freed.
 */
static GHashTable *gv_xml_node_types_by_names = NULL;

/*private functions declarations*/
static void node_type_selected_cb (GtkEditable * a_entry,
                                   MlViewNodeTypePicker * a_this);

static void mlview_node_type_picker_clear_element_name_choice_list (MlViewNodeTypePicker * a_this,
        gboolean a_clear_element_name_entry);

static void mlview_node_type_picker_init_node_type_list (MlViewNodeTypePicker * a_this);

static void mlview_node_type_picker_clear_node_type_choice_list (MlViewNodeTypePicker * a_this,
        gboolean a_clear_node_type_entry);

static void mlview_node_type_picker_update_node_type_list_and_elements_list (MlViewNodeTypePicker * a_this);

/*private gtk framework methods*/

/**
 *The GObject finalize method.
 *Free/unreferences the external allocated/referenced resources
 *hold by the this object.
 *@param a_this the instance of #MlViewNodeTypePicker to dispose.
 */
static void
mlview_node_type_picker_dispose (GObject *a_this)
{
	MlViewNodeTypePicker *picker = NULL ;
	THROW_IF_FAIL (a_this && MLVIEW_NODE_TYPE_PICKER (a_this));

	picker = MLVIEW_NODE_TYPE_PICKER (a_this) ;
	THROW_IF_FAIL (picker) ;

	if (PRIVATE (picker)->dispose_has_run == TRUE) {
		return ;
	}
	if (gv_xml_node_types_by_names) {
		g_hash_table_destroy
		(gv_xml_node_types_by_names);
		gv_xml_node_types_by_names = NULL;
	}
	PRIVATE (picker)->dispose_has_run = TRUE ;
	if (gv_parent_class && G_OBJECT_CLASS (gv_parent_class)->dispose) {
		G_OBJECT_CLASS (gv_parent_class)->dispose (a_this) ;
	}
}

static void
mlview_node_type_picker_finalize (GObject *a_this)
{
	MlViewNodeTypePicker *picker=NULL ;

	THROW_IF_FAIL (a_this
	                  && MLVIEW_IS_NODE_TYPE_PICKER (a_this)) ;
	picker = MLVIEW_NODE_TYPE_PICKER (a_this) ;
	THROW_IF_FAIL (picker) ;
	if (PRIVATE (picker)) {
		g_free (PRIVATE (picker)) ;
		PRIVATE (picker) = NULL ;
	}
	if (gv_parent_class && G_OBJECT_CLASS (gv_parent_class)->finalize) {
		G_OBJECT_CLASS (gv_parent_class)->finalize (a_this) ;
	}
}

/**
 *Class initialyzer.
 */
static void
mlview_node_type_picker_class_init (MlViewNodeTypePickerClass *a_klass)
{
	GObjectClass *gobject_class;

	THROW_IF_FAIL (a_klass != NULL);

	gv_parent_class = (GtkDialogClass*) g_type_class_peek_parent (a_klass);
	THROW_IF_FAIL (gv_parent_class) ;
	gobject_class = G_OBJECT_CLASS (a_klass);
	THROW_IF_FAIL (gobject_class) ;

	gobject_class->dispose = mlview_node_type_picker_dispose;
	gobject_class->finalize = mlview_node_type_picker_finalize;
	/*signal definition should come below */
}


/**
 *The instance initialyzer of the MlViewNodeTypePicker. 
 *
 */
static void
mlview_node_type_picker_init (MlViewNodeTypePicker * a_this)
{
	GtkWidget *label = NULL,
	                   *table = NULL;

	THROW_IF_FAIL (a_this != NULL);

	if (PRIVATE (a_this) == NULL) {
		PRIVATE (a_this) =
		    (MlViewNodeTypePickerPrivate*) g_malloc0
		    (sizeof (MlViewNodeTypePickerPrivate));
	}

	/*init the private attributes */
	PRIVATE (a_this)->selected_node_type_def = NULL ;

	/*build the gui */
	PRIVATE (a_this)->node_types_combo =
	    GTK_COMBO (gtk_combo_new ());

	gtk_entry_set_editable
	(GTK_ENTRY
	 (PRIVATE (a_this)->node_types_combo->entry),
	 FALSE);
	gtk_entry_set_activates_default
	(GTK_ENTRY
	 (PRIVATE (a_this)->node_types_combo->entry),
	 TRUE) ;
	PRIVATE (a_this)->node_name_or_content =
	    GTK_COMBO (gtk_combo_new ());
	gtk_combo_disable_activate
	(PRIVATE (a_this)->node_name_or_content) ;
	gtk_entry_set_activates_default
	(GTK_ENTRY
	 (PRIVATE (a_this)->node_name_or_content->entry),
	 TRUE) ;
	PRIVATE (a_this)->node_name_or_content_label =
	    GTK_LABEL (gtk_label_new (_("Element name")));
	mlview_node_type_picker_init_node_type_list
	(a_this);

	PRIVATE (a_this)->selected_node_type_def =
	    &gv_xml_node_types[ELEMENT_NODE] ;
	g_signal_connect
	(G_OBJECT
	 (PRIVATE (a_this)->node_types_combo->entry),
	 "changed",
	 G_CALLBACK (node_type_selected_cb), a_this);

	label = gtk_label_new (_("Node type"));

	table = gtk_table_new (1, 2, FALSE);

	gtk_table_attach_defaults (GTK_TABLE (table), label,
	                           0, 1, 0, 1);

	gtk_table_attach_defaults (GTK_TABLE (table),
	                           GTK_WIDGET
	                           (PRIVATE (a_this)->
	                            node_types_combo), 1, 2, 0,
	                           1);

	gtk_box_pack_start (GTK_BOX
	                    (GTK_DIALOG (a_this)->vbox),
	                    table, FALSE, TRUE, 0);

	gtk_widget_show (table);
	gtk_widget_show (GTK_WIDGET
	                 (PRIVATE (a_this)->node_types_combo));
	gtk_widget_show (label);

	table = gtk_table_new (1, 2, FALSE);
	gtk_table_attach_defaults
	(GTK_TABLE (table),
	 GTK_WIDGET (PRIVATE
	             (a_this)->node_name_or_content_label),
	 0, 1, 0, 1);
	gtk_table_attach_defaults
	(GTK_TABLE (table),
	 GTK_WIDGET (PRIVATE (a_this)->node_name_or_content),
	 1, 2, 0, 1);
	gtk_box_pack_start (GTK_BOX
	                    (GTK_DIALOG (a_this)->vbox),
	                    table, FALSE, TRUE, 0);
	gtk_widget_show (table);
	gtk_widget_show (GTK_WIDGET
	                 (PRIVATE (a_this)->node_name_or_content));
	gtk_widget_show (GTK_WIDGET
	                 (PRIVATE
	                  (a_this)->node_name_or_content_label));
}


static void
mlview_node_type_picker_update_node_type_list_and_elements_list (MlViewNodeTypePicker * a_this)
{
	GHashTable *present_element_types = NULL;
	GList *list_elem = NULL,
	                   *elements_to_remove = NULL;

	THROW_IF_FAIL (a_this != NULL);
	THROW_IF_FAIL (MLVIEW_IS_NODE_TYPE_PICKER
	                  (a_this));
	THROW_IF_FAIL (PRIVATE (a_this) != NULL);

	mlview_node_type_picker_clear_node_type_choice_list
	(a_this, TRUE);

	present_element_types =
	    g_hash_table_new (g_str_hash, g_str_equal);

	for (list_elem =
	            PRIVATE (a_this)->element_names_choice_list;
	        list_elem; list_elem = g_list_next (list_elem)) {
		gchar *element_name = NULL;

		element_name = (gchar*)list_elem->data;

		if (element_name
		        && !strcmp (element_name, "#PCDATA")
		        &&
		        !g_hash_table_lookup (present_element_types,
		                              "TEXT_NODE")) {

			PRIVATE (a_this)->node_type_names =
			    g_list_append
			    (PRIVATE (a_this)->node_type_names,
			     (gpointer)gv_xml_node_types[TEXT_NODE].
			     node_type_name);

			g_hash_table_insert
			(present_element_types,
			 (gpointer)"TEXT_NODE",
			 (gpointer)"TEXT_NODE");
			elements_to_remove =
			    g_list_append
			    (elements_to_remove,
			     list_elem->data);

		} else if (element_name
		           &&
		           !g_hash_table_lookup
		           (present_element_types,
		            "ELEMENT_NODE")) {

			PRIVATE (a_this)->node_type_names =
			    g_list_append
			    (PRIVATE (a_this)->node_type_names,
			     (gpointer)gv_xml_node_types[ELEMENT_NODE].
			     node_type_name);

			g_hash_table_insert
			(present_element_types,
			 (gpointer)"ELEMENT_NODE",
			 (gpointer)"ELEMENT_NODE");
		}
	}

	list_elem = elements_to_remove;

	while (list_elem && list_elem->data) {

		PRIVATE (a_this)->
		element_names_choice_list =
		    g_list_remove (PRIVATE (a_this)->
		                   element_names_choice_list,
		                   list_elem->data);
		list_elem = g_list_next (list_elem);

	}

	if (g_list_length
	        (PRIVATE
	         (a_this)->element_names_choice_list) <= 0) {
		PRIVATE (a_this)->element_names_choice_list = 0;
	}
	PRIVATE (a_this)->node_type_names =
	    g_list_append (PRIVATE (a_this)->node_type_names,
	                   (gpointer)gv_xml_node_types[PI_NODE].
	                   node_type_name);

	PRIVATE (a_this)->node_type_names =
	    g_list_append (PRIVATE (a_this)->node_type_names,
	                   (gpointer)gv_xml_node_types[COMMENT_NODE].
	                   node_type_name);
	gtk_combo_set_popdown_strings
	(PRIVATE (a_this)->node_types_combo,
	 PRIVATE (a_this)->node_type_names);
	gtk_combo_set_popdown_strings
	(PRIVATE (a_this)->node_name_or_content,
	 PRIVATE (a_this)->element_names_choice_list);
	if (present_element_types) {
		g_hash_table_destroy (present_element_types);
		present_element_types = NULL;
	}
}


/**
 *Initialize the node type list 
 *(also the visual list of the node types names combo)
 *with the default values defined in the table gv_xml_node_types. 
 *
 */
static void
mlview_node_type_picker_init_node_type_list (MlViewNodeTypePicker* a_this)
{
	int i = 0;

	THROW_IF_FAIL (a_this != NULL);
	THROW_IF_FAIL (MLVIEW_IS_NODE_TYPE_PICKER
	                  (a_this));
	THROW_IF_FAIL (PRIVATE (a_this) != NULL);

	/*walk thru the table of node types to  build the combo popdown list */
	if (gv_xml_node_types_by_names == NULL) {
		gv_xml_node_types_by_names =
		    g_hash_table_new (g_str_hash,
		                      g_str_equal);
	}

	for (i = 0; gv_xml_node_types[i].node_type_name; i++) {

		PRIVATE (a_this)->node_type_names =
		    g_list_append
		    (PRIVATE (a_this)->node_type_names,
		     (gpointer)gv_xml_node_types[i].
		     node_type_name);

		g_hash_table_insert
		(gv_xml_node_types_by_names,
		 (gpointer)gv_xml_node_types[i].node_type_name,
		 (gpointer)&gv_xml_node_types[i]);
	}

	gtk_combo_set_popdown_strings
	(PRIVATE (a_this)->node_types_combo,
	 PRIVATE (a_this)->node_type_names);
}


/**
 *Helper function. Frees the element name list build to
 *help the user during addition of a new element. 
 *
 */
static void
mlview_node_type_picker_clear_element_name_choice_list (MlViewNodeTypePicker * a_this,
        gboolean a_clear_element_name_entry)
{
	THROW_IF_FAIL (a_this != NULL);
	THROW_IF_FAIL (PRIVATE (a_this) != NULL);

	if (PRIVATE (a_this)->element_names_choice_list) {
		g_list_free (PRIVATE (a_this)->
		             element_names_choice_list);
		PRIVATE (a_this)->element_names_choice_list =
		    NULL;

		gtk_list_clear_items
		(GTK_LIST
		 (PRIVATE (a_this)->
		  node_name_or_content->list), 0, -1);

		if (a_clear_element_name_entry == TRUE
		        && PRIVATE (a_this)->node_name_or_content
		        && PRIVATE
		        (a_this)->node_name_or_content->entry) {
			gtk_editable_delete_text
			(GTK_EDITABLE
			 (PRIVATE
			  (a_this)->node_name_or_content->entry),
			 0, -1);
		}
	}
}


/**
 *
 */
static void
mlview_node_type_picker_clear_node_type_choice_list (MlViewNodeTypePicker * a_this,
        gboolean a_clear_node_type_entry)
{
	THROW_IF_FAIL (a_this != NULL);
	THROW_IF_FAIL (MLVIEW_IS_NODE_TYPE_PICKER
	                  (a_this));
	THROW_IF_FAIL (PRIVATE (a_this) != NULL);

	if (PRIVATE (a_this)->node_type_names) {
		g_list_free (PRIVATE (a_this)->node_type_names);
		PRIVATE (a_this)->node_type_names = NULL;

		gtk_list_clear_items
		(GTK_LIST
		 (PRIVATE (a_this)->node_types_combo->list),
		 0, -1);

		if (a_clear_node_type_entry == TRUE
		        && PRIVATE (a_this)->node_types_combo
		        && PRIVATE (a_this)->node_types_combo->entry) {
			g_signal_handlers_block_by_func
			(GTK_OBJECT
			 (PRIVATE
			  (a_this)->node_types_combo->entry),
			 (void*)node_type_selected_cb,
			 a_this);

			gtk_editable_delete_text
			(GTK_EDITABLE
			 (PRIVATE
			  (a_this)->node_types_combo->entry),
			 0, -1);

			g_signal_handlers_unblock_by_func
			(GTK_OBJECT
			 (PRIVATE
			  (a_this)->node_types_combo->entry),
			 (void*) node_type_selected_cb, a_this);
		}
	}
}

/*===============================================================
 *Private signals callback methods
 *===============================================================*/

/**
 *Callback function connected to the "changed" signal
 *emited by the GtkEntry MlViewNodeTypePicker::private->node_types_combo->entry.
 *Gets the name of the node type selected and sets the selected node type
 *to the proper node_type id. 
 *
 */
static void
node_type_selected_cb (GtkEditable * a_entry,
                       MlViewNodeTypePicker * a_this)
{
	gchar *node_type_name = NULL;
	NodeTypeDefinition *node_type_def = NULL;

	THROW_IF_FAIL (a_entry != NULL);
	THROW_IF_FAIL (a_this != NULL);

	node_type_name = (gchar *)
	                 gtk_entry_get_text (GTK_ENTRY (a_entry));

	g_assert (node_type_name != NULL);
	g_assert (gv_xml_node_types_by_names != NULL);

	if (!strcmp (node_type_name, ""))
		return ;
	node_type_def = (NodeTypeDefinition*)
	                g_hash_table_lookup (gv_xml_node_types_by_names,
	                                     node_type_name);
	if (!node_type_def) {
		g_warning ("found no node_type_def associated to: %s",
		           node_type_name) ;
		return ;
	}
	THROW_IF_FAIL (node_type_def);

	mlview_node_type_picker_set_selected_node_type
	(a_this, node_type_def->node_type,
	 node_type_def->entity_type);
}


/*===============================================================
 *Private helper functions
 *===============================================================*/

/*================================================================
 *Public methods of the NodeTypePicker class.
 *=================================================================*/


/**
 *The default constructor.
 *@return the newly created node type picker.
 */
GtkWidget *
mlview_node_type_picker_new ()
{
	GtkWidget *result;

	result = GTK_WIDGET (gtk_type_new
	                     (MLVIEW_TYPE_NODE_TYPE_PICKER));
	g_return_val_if_fail (result != NULL, NULL);
	g_return_val_if_fail (MLVIEW_IS_NODE_TYPE_PICKER
	                      (result), NULL);

	gtk_dialog_add_buttons (GTK_DIALOG (result),
	                        GTK_STOCK_CANCEL, GTK_RESPONSE_REJECT,
	                        GTK_STOCK_OK, GTK_RESPONSE_ACCEPT,
	                        NULL);
	gtk_dialog_set_default_response (GTK_DIALOG (result),
	                                 GTK_RESPONSE_ACCEPT) ;
	return result;
}



/**
 *Builds the element name choice list to initialyze 
 *the element name combo list.
 *This function first re-initialyzes the picker as if 
 *there were no validation.
 *Then, if validation is switched on, 
 *it builds the element names list. 
 *@param a_this the current instance of #MlViewNodeTypePicker.
 *@param a_insertion_scheme wether the choice list is for a node
 *to inserted before, after or as child node of the node to consider.
 *@param a_current_xml_node the xml node to consider while building
 *the element name choice list.
 */
void
mlview_node_type_picker_build_element_name_choice_list (MlViewNodeTypePicker * a_this,
        enum NODE_INSERTION_SCHEME a_insertion_scheme,
        xmlNode * a_current_xml_node)
{
	gint nb_of_names = 0;

	THROW_IF_FAIL (a_this != NULL);
	THROW_IF_FAIL (MLVIEW_IS_NODE_TYPE_PICKER (a_this));

	mlview_node_type_picker_clear_element_name_choice_list
	(a_this, TRUE);
	mlview_node_type_picker_clear_node_type_choice_list
	(a_this, TRUE);
	mlview_node_type_picker_init_node_type_list (a_this) ;


	mlview::PrefsCategoryGeneral *prefs =
	    dynamic_cast<mlview::PrefsCategoryGeneral*> (
		mlview::Preferences::get_instance ()
		->get_category_by_id (mlview::PrefsCategoryGeneral::CATEGORY_ID));
	THROW_IF_FAIL (prefs);

	if ((prefs->use_validation () != TRUE)
	        || (a_current_xml_node == NULL)
	        || (a_current_xml_node->doc == NULL))
		return;

	if ((a_current_xml_node == NULL)
	        || (a_current_xml_node->doc == NULL))
		return;
	if (a_current_xml_node->type == XML_ELEMENT_NODE) {
		nb_of_names =
		    mlview_parsing_utils_build_element_name_completion_list
		    (a_insertion_scheme, a_current_xml_node,
		     &PRIVATE (a_this)->element_names_choice_list);
		if (nb_of_names > 0) {  /*found some names to propose to the user */
			mlview_node_type_picker_update_node_type_list_and_elements_list
			(a_this);
		}
	}
}

/**
 *The instance builder of the MlViewNodeTypePicker class. 
 *@param a_title the new of the node type picker window.
 *@return the newly built node type picker or NULL if an error arises.
 */
GtkWidget *
mlview_node_type_picker_new_with_title (gchar * a_title)
{
	GtkWidget *result;

	result = GTK_WIDGET (gtk_type_new
	                     (MLVIEW_TYPE_NODE_TYPE_PICKER));
	g_return_val_if_fail (result != NULL, NULL);
	g_return_val_if_fail (MLVIEW_IS_NODE_TYPE_PICKER
	                      (result), NULL);

	gtk_dialog_add_buttons (GTK_DIALOG (result),
	                        GTK_STOCK_CANCEL, GTK_RESPONSE_REJECT,
	                        GTK_STOCK_OK, GTK_RESPONSE_ACCEPT,
	                        NULL) ;
	gtk_dialog_set_default_response (GTK_DIALOG (result),
	                                 GTK_RESPONSE_ACCEPT) ;
	return result;
}


/**
 *Sets the title of the node type picker dialog window.
 *@param a_this the current instance of #MlViewNodeTypePicker
 *@param a_title the new title of the node type picker.
 */
void
mlview_node_type_picker_set_title (MlViewNodeTypePicker *a_this,
                                   const gchar * a_title)
{
	THROW_IF_FAIL (a_this != NULL);
	THROW_IF_FAIL (a_title != NULL);

	gtk_window_set_title (GTK_WINDOW (a_this), a_title);
}


/**
 *Getter of the type of node selected by the user.
 *@param a_this the current instance of #MlViewNodeTypePicker.
 *@return a pointer to the #NodeTypeDefinition datastructure.
 *Must *NOT* be freed by the caller !!!
 */
NodeTypeDefinition *
mlview_node_type_picker_get_selected_node_type (MlViewNodeTypePicker * a_this)
{
	g_return_val_if_fail (a_this != NULL, NULL);
	g_return_val_if_fail (PRIVATE (a_this) != NULL, NULL);

	return PRIVATE (a_this)->selected_node_type_def ;
}


/**
 *If the element type selected is either 
 *XML_ELEMENT_NODE or XML_PI_NODE, this method returns
 *the name of the node that the user entered. 
 *For other types of nodes, this method returns
 *the content of the node. 
 *Note that if node names completion is on, this function frees
 *the element names list build during 
 *the call of mvliew_node_type_picker_build_element_name_choice_list (). 
 *@param a_this the current instance of #MlViewNodeTypePicker.
 *@return NULL if the argument given if NULL or 
 *if the instance has not been correctly built. 
 */
gchar *
mlview_node_type_picker_get_node_name_or_content (MlViewNodeTypePicker * a_this)
{
	g_return_val_if_fail (a_this != NULL, NULL);
	g_return_val_if_fail (PRIVATE (a_this) != NULL,
	                      NULL);

	if (PRIVATE (a_this)->node_name_or_content) {
		/*if node name completion is on,
		 *validation is on, free the element names
		 *choice list proposed to 
		 *the user and build during the last
		 *invocation of 
		 *mvliew_node_type_picker_build_element_name_choice_list ()
		 */
		return (gchar *) gtk_entry_get_text
		       (GTK_ENTRY
		        (PRIVATE (a_this)->
		         node_name_or_content->entry));
	}
	return NULL;
}


/**
 *Sets the node type selected by the picker.
 *@param a_this the current instance of #MlViewNodeTypePicker.
 *@param a_node_type the new type of node 
 *(must be of type enum xmlElementType defined in libxml2)
 */
void
mlview_node_type_picker_set_selected_node_type (MlViewNodeTypePicker * a_this,
        xmlElementType a_node_type,
        xmlEntityType a_entity_type/*in case a_node_type is an entity decl*/)
{
	THROW_IF_FAIL (a_this != NULL);
	THROW_IF_FAIL (PRIVATE (a_this));
	THROW_IF_FAIL (PRIVATE (a_this)->
	                  node_name_or_content_label != NULL);

	switch (a_node_type) {
	case XML_ELEMENT_NODE:
		gtk_label_set_text
		(PRIVATE (a_this)->node_name_or_content_label,
		 _("Element node name"));

		if (gv_on_going_validation == TRUE
		        && PRIVATE (a_this)->element_names_choice_list) {
			gtk_combo_set_popdown_strings
			(PRIVATE
			 (a_this)->node_name_or_content,
			 PRIVATE
			 (a_this)->element_names_choice_list);
		}
		PRIVATE (a_this)->selected_node_type_def =
		    &gv_xml_node_types[ELEMENT_NODE] ;
		gtk_widget_show
		(GTK_WIDGET (PRIVATE (a_this)->node_name_or_content));
		gtk_widget_show
		(GTK_WIDGET (PRIVATE (a_this)->node_name_or_content_label));
		break;
	case XML_COMMENT_NODE:
		gtk_label_set_text
		(PRIVATE (a_this)->node_name_or_content_label,
		 _("Comment node content"));
		gtk_widget_show
		(GTK_WIDGET (PRIVATE (a_this)->node_name_or_content_label));
		if (PRIVATE (a_this)->node_name_or_content) {
			gtk_list_clear_items
			(GTK_LIST
			 (PRIVATE
			  (a_this)->node_name_or_content->list),
			 0, -1);

			gtk_editable_delete_text
			(GTK_EDITABLE
			 (PRIVATE (a_this)->node_name_or_content->entry),
			 0, -1);

			gtk_widget_show
			(GTK_WIDGET (PRIVATE (a_this)->node_name_or_content));
		}
		PRIVATE (a_this)->selected_node_type_def =
		    &gv_xml_node_types[COMMENT_NODE];
		break;

	case XML_TEXT_NODE:
		gtk_label_set_text
		(PRIVATE (a_this)->node_name_or_content_label,
		 _("Text node content"));
		gtk_widget_hide
		(GTK_WIDGET (PRIVATE (a_this)->node_name_or_content_label));
		if (PRIVATE (a_this)->node_name_or_content) {
			gtk_list_clear_items
			(GTK_LIST
			 (PRIVATE (a_this)->
			  node_name_or_content->list), 0,
			 -1);
			gtk_editable_delete_text
			(GTK_EDITABLE
			 (PRIVATE
			  (a_this)->node_name_or_content->entry), 0,
			 -1);
			gtk_widget_hide
			(GTK_WIDGET (PRIVATE (a_this)->node_name_or_content));
		}
		PRIVATE (a_this)->selected_node_type_def =
		    &gv_xml_node_types[TEXT_NODE] ;
		break;

	case XML_PI_NODE:
		gtk_label_set_text
		(PRIVATE
		 (a_this)->node_name_or_content_label,
		 _("PI node name"));
		gtk_widget_show
		(GTK_WIDGET (PRIVATE (a_this)->node_name_or_content_label));
		if (PRIVATE (a_this)->node_name_or_content) {
			gtk_list_clear_items
			(GTK_LIST
			 (PRIVATE (a_this)->node_name_or_content->list),
			 0, -1);

			gtk_editable_delete_text
			(GTK_EDITABLE
			 (PRIVATE (a_this)->node_name_or_content->entry),
			 0, -1);
			gtk_widget_show
			(GTK_WIDGET (PRIVATE (a_this)->node_name_or_content));
		}
		PRIVATE (a_this)->selected_node_type_def =
		    &gv_xml_node_types[PI_NODE] ;
		break;

	case XML_CDATA_SECTION_NODE:
		gtk_label_set_text
		(PRIVATE (a_this)->node_name_or_content_label,
		 _("CDATA section node content"));
		gtk_widget_show
		(GTK_WIDGET (PRIVATE (a_this)->node_name_or_content_label));
		if (PRIVATE (a_this)->node_name_or_content) {
			gtk_list_clear_items
			(GTK_LIST
			 (PRIVATE (a_this)->
			  node_name_or_content->list), 0,
			 -1);
			gtk_editable_delete_text
			(GTK_EDITABLE
			 (PRIVATE
			  (a_this)->node_name_or_content->entry), 0,
			 -1);
			gtk_widget_show
			(GTK_WIDGET (PRIVATE (a_this)->node_name_or_content));
		}
		PRIVATE (a_this)->selected_node_type_def =
		    &gv_xml_node_types[CDATA_SECTION_NODE] ;
		break;

	case XML_ENTITY_DECL:
		switch (a_entity_type) {
		case XML_INTERNAL_GENERAL_ENTITY:
			gtk_label_set_text
			(PRIVATE (a_this)->node_name_or_content_label,
			 _("INTERNAL GENERAL ENTITY node name"));
			PRIVATE (a_this)->selected_node_type_def =
			    &gv_xml_node_types[INTERNAL_GENERAL_ENTITY_NODE] ;
			break ;
		case XML_EXTERNAL_GENERAL_PARSED_ENTITY:
			gtk_label_set_text
			(PRIVATE (a_this)->node_name_or_content_label,
			 _("EXTERNAL GENERAL PARSED ENTITY node name"));
			PRIVATE (a_this)->selected_node_type_def =
			    &gv_xml_node_types[EXTERNAL_GENERAL_PARSED_ENTITY_NODE] ;
			break ;
		case XML_EXTERNAL_GENERAL_UNPARSED_ENTITY:
			gtk_label_set_text
			(PRIVATE (a_this)->node_name_or_content_label,
			 _("EXTERNAL GENERAL UNPARSED ENTITY node name"));
			PRIVATE (a_this)->selected_node_type_def =
			    &gv_xml_node_types[EXTERNAL_GENERAL_UNPARSED_ENTITY_NODE] ;
			break ;
		case XML_INTERNAL_PARAMETER_ENTITY:
			gtk_label_set_text
			(PRIVATE (a_this)->node_name_or_content_label,
			 _("INTERNAL PARAMETER ENTITY node name"));
			PRIVATE (a_this)->selected_node_type_def =
			    &gv_xml_node_types[INTERNAL_PARAMETER_ENTITY_NODE] ;
			break ;
		case XML_EXTERNAL_PARAMETER_ENTITY:
			gtk_label_set_text
			(PRIVATE (a_this)->node_name_or_content_label,
			 _("EXTERNAL PARAMETER ENTITY node name"));
			PRIVATE (a_this)->selected_node_type_def =
			    &gv_xml_node_types[EXTERNAL_PARAMETER_ENTITY_NODE] ;
			break ;
		default:
			mlview_utils_trace_debug
			("should not reach this case") ;
			break ;
		}
		gtk_widget_show
		(GTK_WIDGET (PRIVATE (a_this)->node_name_or_content_label));
		if (PRIVATE (a_this)->node_name_or_content) {
			gtk_list_clear_items
			(GTK_LIST
			 (PRIVATE (a_this)->
			  node_name_or_content->list), 0,
			 -1);
			gtk_editable_delete_text
			(GTK_EDITABLE
			 (PRIVATE
			  (a_this)->node_name_or_content->entry), 0,
			 -1);
			gtk_widget_show
			(GTK_WIDGET (PRIVATE (a_this)->node_name_or_content));
		}
		break ;
	default:
		gtk_label_set_text
		(PRIVATE (a_this)->node_name_or_content_label,
		 _("Element node name"));
		gtk_widget_show
		(GTK_WIDGET (PRIVATE (a_this)->node_name_or_content_label));
		PRIVATE (a_this)->selected_node_type_def = NULL ;
		if (PRIVATE (a_this)->node_name_or_content) {
			gtk_widget_show
			(GTK_WIDGET (PRIVATE (a_this)->node_name_or_content));
		}
		break;
	}
}

/**
 *Turns on/off the on going validation. 
 *@param a_on if TRUE, turn on the on going validation.
 */
void
mlview_node_type_picker_set_on_going_validation (gboolean a_on)
{
	gv_on_going_validation = a_on;
}


/**
 *Checks if on going validation is on/off
 *@return TRUE if on going validation is turned on and false if not. 
 */
gboolean
mlview_node_type_picker_on_going_validation_is_on (void)
{
	return gv_on_going_validation;
}


/**
 *The type identifier builder of this object. 
 *@return the gobject type getter/builder of
 *the #MlViewNodeTypePicker class.
 */
guint
mlview_node_type_picker_get_type (void)
{
	static guint mlview_node_type_picker_type = 0;

	if (!mlview_node_type_picker_type) {

		static const GTypeInfo t_info = {
		                                    sizeof (MlViewNodeTypePickerClass),
		                                    NULL,   /* base_init */
		                                    NULL,   /* base_finalize */
		                                    (GClassInitFunc)
		                                    mlview_node_type_picker_class_init,
		                                    NULL,   /* class_finalize */
		                                    NULL,   /* class_data */
		                                    sizeof (MlViewNodeTypePicker),
		                                    0,
		                                    (GInstanceInitFunc)
		                                    mlview_node_type_picker_init,
		                                };

		mlview_node_type_picker_type =
		    g_type_register_static (GTK_TYPE_DIALOG,
		                            "MlViewNodeTypePicker",
		                            &t_info, (GTypeFlags)0);
	}
	return mlview_node_type_picker_type;
}


/**
 *Sets the focus to the node name entry of
 *the node type picker.
 *@param a_this the current instance of #MlViewNodeTypePicker.
 */
void
mlview_node_type_picker_set_focus_to_node_name_or_content_entry (MlViewNodeTypePicker * a_this)
{
	THROW_IF_FAIL (a_this != NULL);
	THROW_IF_FAIL (PRIVATE (a_this) != NULL);

	if (PRIVATE (a_this)->node_name_or_content)
		gtk_widget_grab_focus (GTK_WIDGET
		                       (PRIVATE (a_this)->
		                        node_name_or_content->
		                        entry));
}


/**
 *Selects the content the of node name entry
 *presented in the node type picker.
 *@param a_this the current instance of #MlViewNodeTypePicker.
 */
void
mlview_node_type_picker_select_node_name_or_content_entry_text (MlViewNodeTypePicker * a_this)
{
	gchar *tmp_str = NULL;

	THROW_IF_FAIL (a_this != NULL);
	THROW_IF_FAIL (PRIVATE (a_this) != NULL);

	mlview_node_type_picker_set_focus_to_node_name_or_content_entry
	(a_this);

	if (PRIVATE (a_this)->node_name_or_content)
		tmp_str = (gchar *)
		          gtk_entry_get_text
		          (GTK_ENTRY
		           (PRIVATE
		            (a_this)->node_name_or_content->entry));

	if (tmp_str != NULL) {
		gtk_entry_select_region
		(GTK_ENTRY
		 (PRIVATE
		  (a_this)->node_name_or_content->entry),
		 0, -1);
	}
}
