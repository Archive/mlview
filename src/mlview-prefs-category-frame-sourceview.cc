/*
  Implementation of the SourceView preferences category frame

  This file is part of MlView

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include "mlview-prefs.h"
#include "mlview-prefs-category-sourceview.h"
#include "mlview-prefs-category-frame-sourceview.h"


namespace mlview
{

struct PrefsCategoryFrameSourceViewPriv
{
    PrefsCategorySourceView *m_prefs;
// Widgets
    Gtk::CheckButton *m_show_line_numbers_check_button;
    Gtk::SpinButton  *m_tabs_width_spin_button;
    Gtk::CheckButton *m_replace_tabs_check_button;
    Gtk::CheckButton *m_auto_indent_check_button;
    Gtk::CheckButton *m_show_margin_check_button;
    Gtk::SpinButton  *m_margin_position_spin_button;
    Gtk::FontButton  *m_font_button;
    Gtk::Button      *m_reset_to_default_button;
// Methods
    void setup_ui (Glib::RefPtr<Gnome::Glade::Xml> a_glade_xml_ref);
    void setup_event_handlers ();
// Callbacks
    void on_show_line_numbers_check_button_clicked ();
    void on_tabs_width_spin_button_value_changed ();
    void on_replace_tabs_check_button_clicked ();
    void on_auto_indent_check_button_clicked ();
    void on_show_margin_check_button_clicked ();
    void on_margin_position_value_changed ();
    void on_font_button_font_set ();
    void reset_to_default ();
};


PrefsCategoryFrameSourceView::PrefsCategoryFrameSourceView ()
    : PrefsCategoryFrame ("prefs_category_box_srcview")
{
    Glib::RefPtr<Gnome::Glade::Xml> l_refxml = this->get_gladexml_ref ();

    m_priv = new PrefsCategoryFrameSourceViewPriv ();
    m_priv->m_prefs = dynamic_cast<PrefsCategorySourceView*>
	(Preferences::get_instance ()
	 ->get_category_by_id ("sourceview"));
    m_priv->setup_ui (l_refxml);
    m_priv->setup_event_handlers ();
}

PrefsCategoryFrameSourceView::~PrefsCategoryFrameSourceView ()
{
    if (m_priv != NULL) {
	delete m_priv;
	m_priv = NULL;
    }
}

void
PrefsCategoryFrameSourceViewPriv::setup_ui (
    Glib::RefPtr<Gnome::Glade::Xml> a_glade_xml_ref)
{
// Get widgets handle
    a_glade_xml_ref->get_widget ("show_line_numbers_checkbutton",
				 m_show_line_numbers_check_button);
    a_glade_xml_ref->get_widget ("tabs_width_spinbutton",
				 m_tabs_width_spin_button);
    a_glade_xml_ref->get_widget ("replace_tabs_checkbutton",
				 m_replace_tabs_check_button);
    a_glade_xml_ref->get_widget ("autoindent_check_button",
				 m_auto_indent_check_button);
    a_glade_xml_ref->get_widget ("show_margin_checkbutton",
				 m_show_margin_check_button);
    a_glade_xml_ref->get_widget ("margin_position_spinbutton",
				 m_margin_position_spin_button);
    a_glade_xml_ref->get_widget ("srcview_fontbutton",
				 m_font_button);
    a_glade_xml_ref->get_widget ("prefs_category_srcview_reset_button",
				 m_reset_to_default_button);

// Init. widgets from preferences
    m_show_line_numbers_check_button->set_active (
	m_prefs->show_line_numbers ());

    m_tabs_width_spin_button->set_value (
	m_prefs->get_tabs_width ());

    m_replace_tabs_check_button->set_active (
	m_prefs->replace_tabs_with_spaces ());

    m_auto_indent_check_button->set_active (
	m_prefs->auto_indent ());

    m_show_margin_check_button->set_active (
	m_prefs->show_margin ());

    m_margin_position_spin_button->set_value (
	m_prefs->get_margin_position ());

    m_font_button->set_font_name (
	m_prefs->get_font_name ());
}

void
PrefsCategoryFrameSourceViewPriv::setup_event_handlers ()
{
    m_show_line_numbers_check_button->signal_clicked ().connect (
	sigc::mem_fun (*this,
		       &PrefsCategoryFrameSourceViewPriv::
		       on_show_line_numbers_check_button_clicked));

    m_tabs_width_spin_button->signal_value_changed ().connect (
	sigc::mem_fun (*this,
		       &PrefsCategoryFrameSourceViewPriv::
		       on_tabs_width_spin_button_value_changed));

    m_replace_tabs_check_button->signal_clicked ().connect (
	sigc::mem_fun (*this,
		       &PrefsCategoryFrameSourceViewPriv::
		       on_replace_tabs_check_button_clicked));

    m_auto_indent_check_button->signal_clicked ().connect (
	sigc::mem_fun (*this,
		       &PrefsCategoryFrameSourceViewPriv::
		       on_auto_indent_check_button_clicked));

    m_show_margin_check_button->signal_clicked ().connect (
	sigc::mem_fun (*this,
		       &PrefsCategoryFrameSourceViewPriv::
		       on_show_margin_check_button_clicked));

    m_margin_position_spin_button->signal_value_changed ().connect (
	sigc::mem_fun (*this,
		       &PrefsCategoryFrameSourceViewPriv::
		       on_margin_position_value_changed));

    m_font_button->signal_font_set ().connect (
	sigc::mem_fun (*this,
		       &PrefsCategoryFrameSourceViewPriv::
		       on_font_button_font_set));

    m_reset_to_default_button->signal_clicked ().connect (
	sigc::mem_fun (*this,
		       &PrefsCategoryFrameSourceViewPriv::
		       reset_to_default));
}

void
PrefsCategoryFrameSourceViewPriv::on_show_line_numbers_check_button_clicked ()
{
    m_prefs->set_show_line_numbers (
	m_show_line_numbers_check_button->get_active ());
}

void
PrefsCategoryFrameSourceViewPriv::on_tabs_width_spin_button_value_changed ()
{
    m_prefs->set_tabs_width (
	static_cast<int>(m_tabs_width_spin_button->get_value ()));
}

void
PrefsCategoryFrameSourceViewPriv::on_replace_tabs_check_button_clicked ()
{
    m_prefs->set_replace_tabs_with_spaces (
	m_replace_tabs_check_button->get_active ());
}

void
PrefsCategoryFrameSourceViewPriv::on_auto_indent_check_button_clicked ()
{
    m_prefs->set_auto_indent (
	m_auto_indent_check_button->get_active ());
}

void
PrefsCategoryFrameSourceViewPriv::on_show_margin_check_button_clicked ()
{
    m_prefs->set_show_margin (
	m_show_margin_check_button->get_active ());
}

void
PrefsCategoryFrameSourceViewPriv::on_margin_position_value_changed ()
{
    m_prefs->set_margin_position (
	static_cast<int>(m_margin_position_spin_button->get_value ()));
}

void
PrefsCategoryFrameSourceViewPriv::on_font_button_font_set ()
{
    m_prefs->set_font_name (
	m_font_button->get_font_name ());
}

void
PrefsCategoryFrameSourceViewPriv::reset_to_default ()
{
    m_show_line_numbers_check_button->set_active (
	m_prefs->show_line_numbers_default ());

    m_tabs_width_spin_button->set_value (
	m_prefs->get_tabs_width_default ());

    m_replace_tabs_check_button->set_active (
	m_prefs->replace_tabs_with_spaces_default ());

    m_auto_indent_check_button->set_active (
	m_prefs->auto_indent_default ());

    m_show_margin_check_button->set_active (
	m_prefs->show_margin_default ());

    m_margin_position_spin_button->set_value (
	m_prefs->get_margin_position_default ());

    m_font_button->set_font_name (
	m_prefs->get_default_font_name ());

// Need to update fontname property manually
// Maybe a bug in GTK ?
    m_prefs->set_font_name (
	m_font_button->get_font_name ());
}

} // namespace mlview
