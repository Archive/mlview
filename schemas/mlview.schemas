<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<gconfschemafile>
  <schemalist>
    <schema>
      <key>/schemas/apps/mlview/validation-is-on</key>
      <applyto>/apps/mlview/validation-is-on</applyto>
      <owner>mlview</owner>
      <type>bool</type>
      <default>true</default>
      <locale name="C">
	<short>The validation activation state</short>
	<long>If set to true, the validation (and the elements/attributes names completion) 
	feature is activated.</long>
      </locale>
    </schema>
    <schema>
      <key>/schemas/apps/mlview/sizes/main-window-width</key>
      <applyto>/apps/mlview/sizes/main-window-width</applyto>
      <owner>mlview</owner>
      <type>int</type>
      <default>800</default>
      <locale name="C">
	<short>MlView main window width</short>
	<long>Width of MlView's main window</long>
      </locale>
    </schema>
    <schema>
      <key>/schemas/apps/mlview/sizes/main-window-height</key>
      <applyto>/apps/mlview/sizes/main-window-height</applyto>
      <owner>mlview</owner>
      <type>int</type>
      <default>600</default>
      <locale name="C">
	<short>MlView main window height</short>
	<long>Height of MlView's main window</long>
      </locale>
    </schema>
    <schema>
      <key>/schemas/apps/mlview/sizes/tree-editor-size</key>
      <applyto>/apps/mlview/sizes/tree-editor-size</applyto>
      <owner>mlview</owner>
      <type>int</type>
      <default>300</default>
      <locale name="C">
	<short>Tree Editor Size</short>
	<long>Size of the tree editor's paned position.</long>
      </locale>
    </schema>
    <schema>
      <key>/schemas/apps/mlview/sizes/completion-size</key>
      <applyto>/apps/mlview/sizes/completion-size</applyto>
      <owner>mlview</owner>
      <type>int</type>
      <default>200</default>
      <locale name="C">
	<short>Completion box size</short>
	<long>Size of the tree view's completion box.</long>
      </locale>
    </schema>

    <!-- SourceView -->
    <schema>
      <key>/schemas/apps/mlview/sourceview/show-line-numbers</key>
      <applyto>/apps/mlview/sourceview/show-line-numbers</applyto>
      <owner>mlview</owner>
      <type>bool</type>
      <default>FALSE</default>
      <locale name="C">
	<short>Show line numbers</short>
	<long></long>
      </locale>
    </schema>
    <schema>
      <key>/schemas/apps/mlview/sourceview/tabs-width</key>
      <applyto>/apps/mlview/sourceview/tabs-width</applyto>
      <owner>mlview</owner>
      <type>int</type>
      <default>4</default>
      <locale name="C">
	<short>Tabulation width in number of characters</short>
	<long></long>
      </locale>
    </schema>
    <schema>
      <key>/schemas/apps/mlview/sourceview/replace-tabs</key>
      <applyto>/apps/mlview/sourceview/replace-tabs</applyto>
      <owner>mlview</owner>
      <type>bool</type>
      <default>FALSE</default>
      <locale name="C">
	<short>Replace tabulations with spaces</short>
	<long></long>
      </locale>
    </schema>
    <schema>
      <key>/schemas/apps/mlview/sourceview/auto-indent</key>
      <applyto>/apps/mlview/sourceview/auto-indent</applyto>
      <owner>mlview</owner>
      <type>bool</type>
      <default>FALSE</default>
      <locale name="C">
	<short>Auto indentation of text</short>
	<long></long>
      </locale>
    </schema>
    <schema>
      <key>/schemas/apps/mlview/sourceview/show-margin</key>
      <applyto>/apps/mlview/sourceview/show-margin</applyto>
      <owner>mlview</owner>
      <type>bool</type>
      <default>FALSE</default>
      <locale name="C">
	<short>Show margin rule</short>
	<long></long>
      </locale>
    </schema>
    <schema>
      <key>/schemas/apps/mlview/sourceview/margin-position</key>
      <applyto>/apps/mlview/sourceview/margin-position</applyto>
      <owner>mlview</owner>
      <type>int</type>
      <default>80</default>
      <locale name="C">
	<short></short>
	<long></long>
      </locale>
    </schema>
    <schema>
      <key>/schemas/apps/mlview/sourceview/fontname</key>
      <applyto>/apps/mlview/sourceview/fontname</applyto>
      <owner>mlview</owner>
      <type>string</type>
      <default>Sans 10</default>
      <locale name="C">
	<short>The font to use</short>
	<long></long>
      </locale>
    </schema>

    <!-- Treeview -->
    <schema>
      <key>/schemas/apps/mlview/treeview/default-tree-expansion-depth</key>
      <applyto>/apps/mlview/treeview/default-tree-expansion-depth</applyto>
      <owner>mlview</owner>
      <type>int</type>
      <default>4</default>
      <locale name="C">
	<short>The default tree expansion depth</short>
	<long>The depth to which the nodes of the xml document should be expanded.
	This applies at document load time.</long>
      </locale>
    </schema>
    <schema>
      <key>/schemas/apps/mlview/treeview/enable-completion-box</key>
      <applyto>/apps/mlview/treeview/enable-completion-box</applyto>
      <owner>mlview</owner>
      <type>bool</type>
      <default>true</default>
      <locale name="C">
	<short>The completion box state</short>
	<long>If set to true, the completion box will be shown.</long>
      </locale>
    </schema>
    <schema>
      <key>/schemas/apps/mlview/treeview/fontname</key>
      <applyto>/apps/mlview/treeview/fontname</applyto>
      <owner>mlview</owner>
      <type>string</type>
      <default>Sans 10</default>
      <locale name="C">
	<short>The font used by the treeview widget</short>
	<long>Defines the font which is used for the text in the treeview widget</long>
      </locale>
    </schema>
    <schema>
      <key>/schemas/apps/mlview/treeview/colours/xml-element-node</key>
      <applyto>/apps/mlview/treeview/colours/xml-element-node</applyto>
      <owner>mlview</owner>
      <type>string</type>
      <default>#0080ff</default>
      <locale name="C">
	<short>Element node colour in mlview</short>
	<long>Element node colour in mlview's trees.</long>
      </locale>
    </schema>
    <schema>
      <key>/schemas/apps/mlview/treeview/colours/xml-text-node</key>
      <applyto>/apps/mlview/treeview/colours/xml-text-node</applyto>
      <owner>mlview</owner>
      <type>string</type>
      <default>#ff0000</default>
      <locale name="C">
        <short>Text node colour in mlview</short>
        <long>Text node colour in mlview's trees.</long>
      </locale>
    </schema>
    <schema>
      <key>/schemas/apps/mlview/treeview/colours/xml-comment-node</key>
      <applyto>/apps/mlview/treeview/colours/xml-comment-node</applyto>
      <owner>mlview</owner>
      <type>string</type>
      <default>#0000FF</default>
      <locale name="C">
        <short>Comment node colour in mlview</short>
        <long>Comment node colour in mlview's trees.</long>
      </locale>
    </schema>
    <schema>
      <key>/schemas/apps/mlview/treeview/colours/xml-document-node</key>
      <applyto>/apps/mlview/treeview/colours/xml-document-node</applyto>
      <owner>mlview</owner>
      <type>string</type>
      <default>#0080ff</default>
      <locale name="C">
        <short>Document node colour in mlview</short>
        <long>Document node colour in mlview's trees.</long>
      </locale>
    </schema>
    <schema>
      <key>/schemas/apps/mlview/treeview/colours/xml-dtd-node</key>
      <applyto>/apps/mlview/treeview/colours/xml-dtd-node</applyto>
      <owner>mlview</owner>
      <type>string</type>
      <default>#ff00ff</default>
      <locale name="C">
        <short>DTD node colour in mlview</short>
        <long>DTD node colour in mlview's trees.</long>
      </locale>
    </schema>
    <schema>
      <key>/schemas/apps/mlview/treeview/colours/xml-attribute-node</key>
      <applyto>/apps/mlview/treeview/colours/xml-attribute-node</applyto>
      <owner>mlview</owner>
      <type>string</type>
      <default>#00ff00</default>
      <locale name="C">
        <short>Attribute name colour in mlview</short>
        <long>Attribute name colour in mlview's trees.</long>
      </locale>
    </schema>
    <schema>
      <key>/schemas/apps/mlview/treeview/colours/xml-attribute-value--node</key>
      <applyto>/apps/mlview/treeview/colours/xml-attribute-value-node</applyto>
      <owner>mlview</owner>
      <type>string</type>
      <default>#ff0000</default>
      <locale name="C">
        <short>Attribute value colour in mlview</short>
        <long>Attribute value colour in mlview's trees.</long>
      </locale>
    </schema>
    <schema>
      <key>/schemas/apps/mlview/treeview/colours/xml-pi-node</key>
      <applyto>/apps/mlview/treeview/colours/xml-pi-node</applyto>
      <owner>mlview</owner>
      <type>string</type>
      <default>#0A7FDE</default>
      <locale name="C">
        <short>XML Processing instruction node colour in mlview</short>
        <long>XML Processing instruction node colour in mlview</long>
      </locale>
    </schema>
    <schema>
      <key>/schemas/apps/mlview/treeview/colours/xml-entity-decl-node</key>
      <applyto>/apps/mlview/treeview/colours/xml-entity-decl-node</applyto>
      <owner>mlview</owner>
      <type>string</type>
      <default>#FF00FF</default>
      <locale name="C">
        <short>XML Entity Declaration node colour in mlview</short>
        <long>XML Entity Declaration node colour in mlview</long>
      </locale>
    </schema>
    <schema>
      <key>/schemas/apps/mlview/default-editing-view-type</key>
      <applyto>/apps/mlview/default-editing-view-type</applyto>
      <owner>mlview</owner>
      <type>string</type>
      <default>source-view</default>
      <locale name="C">
        <short>The default tree oriented editing view</short>
        <long>This view is made of tree oriented editing widgets, a node editor and an element name completion widget. This view is well suited for tree oriented editing</long>
      </locale>
    </schema>
  </schemalist>
</gconfschemafile>
